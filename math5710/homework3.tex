% Created 2019-09-27 Fri 12:18
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\textheight=9.25in \textwidth=7in \topmargin=-0.75in \oddsidemargin=-0.25in \evensidemargin=-0.25in \usepackage{mathtools} \usepackage[binary-units]{siunitx}
\author{Abraham White}
\date{\today}
\title{Homework 3}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Homework 3},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{2}
\label{sec:org1bf318b}
An experiment consists of throwing a fair coin four times. Find the frequency function and cumulative
distribution function of the following random variables:

\begin{quote}
The following table represents the possible outcomes of throwing a fair coin four times, and the
values of the discrete random variables \(X_b\), the number of heads following the first tail, and
\(X_c\), the number of heads minus the number of tails.

\begin{center}
\begin{tabular}{lrr}
Outcome & \(X_b\) & \(X_c\)\\
\hline
HHHH & 0 & 4\\
HHHT & 0 & 2\\
HHTH & 1 & 2\\
HHTT & 0 & 0\\
HTHH & 2 & 2\\
HTHT & 1 & 0\\
HTTH & 1 & 0\\
HTTT & 0 & -2\\
THHH & 3 & 2\\
THHT & 2 & 0\\
THTH & 2 & 0\\
THTT & 1 & -2\\
TTHH & 2 & 0\\
TTHT & 1 & -2\\
TTTH & 1 & -2\\
TTTT & 0 & -4\\
\end{tabular}
\end{center}
\end{quote}

\subsection*{(b)}
\label{sec:orgd6c8b1f}
The number of heads following the first tail

\begin{quote}
The frequency function can be determined by examination of the above table, and is
\[
p(x_b) = 
\begin{cases}
5/16 & x_b = 0\\
6/16 & x_b = 1\\
4/16 & x_b = 2\\
1/16 & x_b = 3\\
\end{cases}
\]
Remember that the definition of a cdf is $F(x) = P(X \leq x)$ for some random random variable $X$ and
number $x$. We can then determine $F(x_b)$ by examining the table above, looking for values of $X_b$
that are less than or equal to some number $x_b$. Because we have a finite range of values for $X_b$,
we can represent $F(x_b)$ as a summation of frequency functions: $F(x_b) = \sum_{i = 0}^{x_b} p(i)$.
\[
F(x_b) =
\begin{cases}
5/16 & x_b = 0\\
11/16 & x_b = 1\\
15/16 & x_b = 2\\
1 & x_b = 3\\
\end{cases}
\]
\end{quote}

\subsection*{(c)}
\label{sec:orgab00e59}
The number of heads minus the number of tails

\begin{quote}
The frequency function can be determined by examination of the above table, and is
\[
p(x_c) = 
\begin{cases}
1/16 & x_c = -4 \\
4/16 & x_c = -2 \\
6/16 & x_c = 0 \\
4/16 & x_c = 2 \\
1/16 & x_c = 4 \\
\end{cases}
\]
We can then determine $F(x_c)$ by examining the table above, looking for values of $X_c$
that are less than or equal to some number $x_c$.
\[
F(x_c) =
\begin{cases}
1/16 & x_c = -4 \\
5/16 & x_c = -2 \\
11/16 & x_c = 0 \\
15/16 & x_c = 2 \\
1 & x_c = 4 \\
\end{cases}
\]
We could also generically represent the cumulative distribution function for these kind of variables
using the following method: Let $D$ be the domain of the frequency function $p$ on some discrete random
variable $X$. Sort $D$ in increasing order, such that the $i$th element of the set is less than or
equal to element $i+1$. We can say that the cumulative distribution function is
\[
F(x) = \sum_{i = 1}^{k} p(D_i), \text{ where $k$ is the position of $x$ in the set $D$}
\]
\end{quote}

\section*{4}
\label{sec:org50f3725}
If \(X\) is an integer-valued random variable, show that the frequency function is related to the cdf
by \(p(k) = F(k) - F(k - 1)\).

\begin{quote}
First recall that for a cdf \(F(k), F(k) = P(X \leq k)\). Recall that the frequency function represents
the probability that a discrete random variable is exactly equal to some value, i.e. \(p(k) = P(K = k)\),
for a discrete random variable \(K\). Then the probability that \(X \leq x\) for some integer \(x\) is the same as
\(P(X = x) + P(X = x - 1) + \cdots + P(X = 0)\). This means that \(F(k) = P(X \leq k) = \sum_{x \leq k} p(x)\), and
\(F(k - 1) = P(X \leq k - 1) = \sum_{x \leq k - 1} p(x)\).

Now we can solve algebraically to prove that \(p(k) = F(k) - F(k - 1)\):
\begin{align*}
p(k) &= F(k) - F(k - 1)\\
&= P(X \leq k) - P(X \leq k - 1)\\
&= (p(k) + p(k - 1) + \cdots + p(0)) - (p(k - 1) + p(k - 2) + \cdots + p(0))\\
&= p(k) + p(k - 1) - p(k - 1) + p(k - 2) - p(k - 2) + \cdots + p(0) - p(0)\\
&= p(k)
\end{align*}
\end{quote}

\section*{9}
\label{sec:orge933369}
For what values of \(p\) is a two-out-of-three majority decoder better than transmission of the message
once?

\begin{quote}
Assume we are transmitting a single bit over a noisy channel. The bit may have either the value 0, or
the value 1. There is a probability \(p\) of the bit being flipped during transmission. A message is sent
three times, with the decoder determining the correct message to be that which two of the three bits
has.

We have a binomial probability distribution. This means that we can calculate the probability that
some number out of three bits flipped using the binomial frequency function
\(f(k,n,p) = \binom{n}{k}p^k(1 - p)^{n - k}\), for \(n = 3\), \(p\) is the probability that a bit flipped, and
\(k\) is the number of flipped bits.

To calculate the probability that a single transmission sent correctly, we can let \(n = 1\) and \(k = 0\).
Therefore, the probability \(P_1\) that a single transmission sent correctly is
\begin{align*}
P_1 &= f(0,1,p)\\
&= \binom{1}{0}p^0(1 - p)^{1 - 0}\\
&= 1 - p
\end{align*}

The probability that two out of three transmissions sent correctly is \(P_3 = f(0,3,p) + f(1,3,p)\), since
we can have a maximum of one flipped bit, represented by \(k\). This gives us:
\begin{align*}
P_3 &= f(0,3,p) + f(1,3,p)\\
&= \binom{3}{0}p^0(1 - p)^{3 - 0} + \binom{3}{1}p^1(1 - p)^{3 - 1}\\
&= (1 - p)^3 + 3p(1 - p)^2\\
&= (-p^3 + 3p^2 - 3p + 1)  + 3p(p^2 - 2p + 1)\\
&= (-p^3 + 3p^2 - 3p + 1)  + (3p^3 - 6p^2 + 3p)\\
&= 2p^3 - 3p^2 + 1\\
\end{align*}

For the two-out-of-three majority decoder to be better than a single transmission, \(P_3\) must be greater
than \(P_1\).

\begin{align*}
P_3 &> P_1\\
2p^3 - 3p^2 + 1 &> 1 - p\\
2p^3 - 3p^2 + p &> 0\\
\end{align*}

We must now solve this inequality to find the values of \(p\) where the majority decoder is better.
First we find the roots of the polynomial.
\begin{align*}
&2p^3 - 3p^2 + p\\
&p(p - 1)(2p - 1)\\
&p = \{0,\frac{1}{2},1\}
\end{align*}
Next, we test the behavior of the polynomial near these roots. \\
For the range \(p < 0\), we test \(p = -1\). The polynomial is \(-1(-1 - 1)(-2 - 1) = -1(-2)(-3)\), a negative value. \\
For the range \(0 < p < 1/2\), we test \(p = 1/4\). The polynomial is \(1/4(1/4 - 1)(2/4 - 1) = 1/4(-3/4)(-1/2)\), a positive value. \\
For the range \(1/2 < p < 1\), we test \(p = 3/4\). The polynomial is \(3/4(3/4 - 1)(6/4 - 1) = 3/4(-1/4)(2/4)\), a negative value. \\
For the range \(p > 1\), we test \(p = 2\). The polynomial is \(2(2 - 1)(4 - 1)\), a positive value. \\

This means the inequality is true for values of \(p\) greater than 0 and less than \(\frac{1}{2}\), or greater
than 1. Since a probability cannot be less than zero or greater than 1, the final value of \(p\) is the range
\((0,\frac{1}{2})\).
\end{quote}

\section*{12}
\label{sec:org1097bee}
Which is more likely: 9 heads in 10 tosses of a fair coin, or 18 heads in 20 tosses?

\begin{quote}
This is another binomial distribution. Let \(A\) be the event that 9 out of 10 tosses resulted in a head,
and \(B\) be the event that 18 out of 20 tosses resulted in a head.

To solve \(P(A)\), we use the binomial frequency function \(f(k,n,p) = \binom{n}{k}p^k(1 - p)^{n - k}\). In this case,
\(p = 0.5\) since the coin is fair, \(k = 9\), and \(n = 10\).
\begin{align*}
P(A) &= \binom{10}{9}0.5^9(1 - 0.5)^{10 - 9}\\
&= 10 \times 0.5^9 \times 0.5\\
&= 10 \times 0.5^{10}\\
&= 0.00976563
\end{align*}

To solve \(P(B)\), we use the binomial frequency function with \(p = 0.5\), \(k = 18\), and \(n = 20\).
\begin{align*}
P(B) &= \binom{20}{18}0.5^{18}(1 - 0.5)^{20 - 18}\\
&= 190 \times 0.5^{18} \times 0.5^{2}\\
&= 190 \times 0.5^{20}\\
&= 0.000181198
\end{align*}

Therefore, the event \(A\), 9 heads in 10 tosses, is more likely since \(P(A)\) is greater than \(P(B)\).
\end{quote}

\section*{14}
\label{sec:org4e4d35e}
Two boys play basketball in the following way. They take turns shooting and stop when a basket is made.
Player A goes first and has probability \(p_1\) of making a basket on any throw. Player B, who shoots second,
has probability \(p_2\) of making a basket. The outcomes of the successive trials are assumed to be independent.

\subsection*{(a)}
\label{sec:orga4d578f}
Find the frequency function for the total number of attempts

\begin{quote}
First we begin by enumerating some of the possible values of the frequency function \(f\). Let \(X\) be the
discrete random variable for the total number of attempts, \(q_1 = 1 - p_1\), the probability that player A
doesn't make a basket, and \(q_2 = 1 - p_2\), the probability that player B doesn't make a basket.

\[
f(x) =
\begin{cases}
f(1) = P(X = 1) = p_1 \\
f(2) = P(X = 2) = q_1 p_2 \\
f(3) = P(X = 3) = q_1 q_2 p_1 \\
f(4) = P(X = 4) = q_1 q_2 q_1 p_2 \\
f(5) = P(X = 5) = q_1 q_2 q_1 q_2 p_1 \\
f(6) = P(X = 6) = q_1 q_2 q_1 q_2 q_1 p_2 \\
\vdots 
\end{cases}
\]

Notice that if $x$ is odd, i.e. $x = 2n - 1$, then player A made $n - 1$ failed attempts, and 1 successful
basket, for a total of $n$ shots. Player B made $n - 1$ unsuccessful shots.

If $x$ is even, i.e. $x = 2n$, then player B made $n - 1$ failed attempts, and 1 successful basket, for a
total of $n$ shots. Player A made $n$ unsuccessful shots.

Therefore the frequency function is
\[
f(x) =
\begin{cases}
p_1 q_1^{n - 1} q_2^{n - 1} & \text{where $x = 2n - 1$} \\
p_2 q_1^n q_2^{n - 1} & \text{where $x = 2n$} \\
\end{cases}
\]
\end{quote}

\subsection*{(b)}
\label{sec:orgc192e9f}
What is the probability that player A wins?

\begin{quote}
Notice in the enumerated cases for the frequency function above that for every odd \(x = 2n - 1\),
there is one successful shot for player A, with probability \(p_1\), and \(n - 1\) unsuccessful attempts
for both player A and player B, with probabilities \(q_1\) and \(q_2\) respectively.

Let \(A\) be the event that player A won.

Remember that player A can only win on odd numbered turns. Therefore the probability that player A wins
is the probability that player A won the first round, plus the probability that player A won the third
round, and so on to infinity. As we noted earlier, for the nth throw by player A there will be \(n - 1\)
throws that were missed by player A and player B, and one throw that was successful. This gives us the
following formula:
\[
   P(A) = p_1 \sum_{n = 1}^\infty (q_1 q_2)^{n - 1} = \frac{p_1}{1 - q_1 q_2} = \frac{p_1}{1 - (1 - p_2 - p_1 + p_1p_2)} = \frac{p_1}{p_2 + p_1 - p_1 p_2}
   \]
\end{quote}

\section*{15}
\label{sec:org83ada27}
Two teams, A and B, play a series of games. If team A has probability .4 of winning each game, is it to its
advantage to play the best three out of five games or the best four out of seven? Assume the outcomes of
successive games are independent.

\begin{quote}
This is a binomial distribution. We can use the frequency function to determine the probability of winning a
number of games. For three out of five games, we have the expression \(\binom{5}{3}0.4^{3}(1 - 0.4)^{5 - 3} = 0.2304\).
For four out of seven, we have expression \(\binom{7}{4}0.4^{4}(1 - 0.4)^{7 - 4} = 0.193536\).

Therefore it is better to play to best three out of five.
\end{quote}

\section*{17}
\label{sec:orga85c084}
Suppose that in a sequence of independent Bernoulli trials, each with probability of success \(p\), the number
of failures up to the first success is counted. What is the frequency function for this random variable?

\begin{quote}
Let \(K\) be the number of failures up to the first success. Let \(q\) be the probability of failure, \(q = 1 - p\).
The frequency function is \(p(k) = q^k p\), where \(k\) is the number of failures.
\end{quote}

\section*{20}
\label{sec:org534521c}
If \(X\) is a geometric random variable with \(p = .5\), for what value of \(k\) is \(P(X \leq k) \approx .99\)?

\begin{quote}
This is asking for a \(k\) such that the cdf \(F(k) \approx .99\)?

We could just state that the cdf for a geometric distribution is: \(F(k) = 1 - (1 - p)^k\), but it is more likely
that it is expected for us to derive this.

Recall that by the definition of a geometric random variable
\begin{align*}
P(X \leq k) &= \sum_{i = 1}^k p (1 - p)^{i - 1} \\
&= \sum_{i = 1}^\infty p (1 - p)^{i - 1} - \sum_{k+1}^\infty p (1 - p)^{j - 1}
\shortintertext{Recall that the first summation simply adds to 1}
&= 1 - \sum_{k+1}^\infty p (1 - p)^{j - 1}
\shortintertext{Let us pull a factor out of the summation to simplify}
&= 1 - p(1-p)^{k} + p(1-p)^{k + 1} + \cdots \\
&= 1 - p(1-p)^{k} (1 + (1-p)^{1} + \cdots) \\
\shortintertext{This is a well-known series: $\sum_{i = 0}^{\infty} x^i = \frac{1}{1 - x}$}
&= 1 - p(1-p)^{k} \frac{1}{1 - (1 - p)} \\
&= 1 - (1 - p)^k
\end{align*}

Now we solve for a \(k\).

\begin{align*}
1 - (1 - p)^k \approx 0.99\\
1 - 0.99 \approx (1 - p)^k\\
0.01 \approx (1 - p)^k\\
\log(0.01)/\log(1 - p) \approx k
\shortintertext{Substitute our value for $p$}
\log(0.01)/\log(1 - 0.5) \approx k\\
\log(0.01)/\log(0.5) \approx k\\
6.64 \approx k
\end{align*}

We check the two integers closest to this approximation: \(F(6) = 1 - 0.5^6 = 0.984\), \(F(7) = 1 - 0.5^7 = 0.992\).

Therefore we choose 7 as the \(k\) that gives us the closest approximation to 0.99.
\end{quote}
\end{document}