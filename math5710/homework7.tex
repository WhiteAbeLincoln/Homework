% Created 2019-11-08 Fri 21:51
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\input{../preamble.tex}
\author{Abraham White}
\date{\today}
\title{Homework 7}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Homework 7},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{1}
\label{sec:org44f828d}
Show that if a random variable is bounded---that is, \(|X| \leq M \leq \infty\) ---then \(E(X)\) exists.
\begin{answer}
Recall the definition of the expected value for a discrete random variable 
\[ E(X) = \sum_i x_i p(x_i) \]
and a continuous random variable
\[ E(X) = \int_{-\infty}^{\infty} x f(x)\,dx \]

Both of these definitions are contingent on the fact that the summation or integral does not diverge, because
then the expected value would be undefined. \\

If \(X\) is a discrete random variable and is bounded, then every possible value \(x_i\) will be finite.
\(p(x_i)\) is a probability, and so is bound between 0 and 1. Therefore the summation of every value \(x_i\)
will also be finite. \\

For the continuous random variable, we know that the integral of a probability density function over the entire
space is 1. Because \(X\) is bounded by \(M\), we know that the space is between \(-M\) and \(M\), and therefore
finite. We also know that every value of \(X\), given by \(x\), must be less than \(M\). Therefore the definition
becomes
\begin{align*}
E(X) &= \int_{-\infty}^{\infty} x f(x)\,dx = \int_{-M}^{M} x f(x)\,dx \\
E(X) &\leq \int_{-M}^{M} M f(x)\,dx \\
&\leq M \int_{-M}^{M} f(x)\,dx
\shortintertext{This integral is 1, since the pdf over the entire space is 1}
E(X) &\leq M
\end{align*}

\(E(X)\) is bounded by \(M\) and \(M\) is finite, therefore \(E(X)\) is finite and exists.
\end{answer}
\section*{2}
\label{sec:orgfd371c7}
If \(X\) is a discrete uniform random variable---that is, \(P(X=k) = 1/n\) for \(k=1,2,\ldots,n\) ---find \(E(X)\)
and \(\operatorname{Var}(X)\).
\begin{answer}
The expected value is 
\begin{align*}
E(X) &= \sum_{k=1}^n \frac{k}{n} \\
     &= \frac{1}{n} \sum_{k=1}^n k \\
     \shortintertext{This is an arithmetic progression}
     &= \frac{n(n+1)}{2n} \\
     &= \frac{n+1}{2} \\
\end{align*}
\(\operatorname{Var}(X)\) is found by
\begin{align*}
\operatorname{Var}(X) &= E(X^2) - E(X)^2 \\
                      &= E(X^2) - \left(\frac{n+1}{2}\right)^2 \\
                      &= \sum_{k=1}^n \frac{k^2}{n} - \left(\frac{n+1}{2}\right)^2 \\
                      &= \frac{1}{n} \sum_{k=1}^n k^2 - \left(\frac{n+1}{2}\right)^2 \\
                      &= \frac{(n+1)(2n+1)}{6} - \left(\frac{n+1}{2}\right)^2 \\
                      &= \frac{(n+1)(2n+1)}{6} - \frac{(n+1)^2}{4} \\
                      &= \frac{2(n+1)(2n+1) - 3(n+1)^2}{12} \\
                      &= \frac{4n^2 + 6n + 2 - 3n^2 - 6n - 3}{12} \\
                      &= \frac{n^2 - 1}{12} \\
\end{align*}
\end{answer}
\section*{4}
\label{sec:orgd5ea1fa}
Let \(X\) have the cdf \(F(x) = 1 - x^{-\alpha},~ x \geq 1\).
\subsection*{a}
\label{sec:orgf6b835d}
Find \(E(X)\) for those values of \(\alpha\) for which \(E(X)\) exists.
\begin{answer}
First we need to find the pdf from the cdf. 
\begin{align*}
f(x) &= \frac{d}{dx} F(x) \\
     &= \frac{d}{dx} 1 - x^{-\alpha} \\
     &= \alpha x^{-\alpha - 1}
\end{align*}

Next we can find the expected value using the formula. Note that \(\alpha\) must be greater than 1.
\begin{align*}
E(X) &= \int_{-\infty}^{\infty} x f(x)\,dx \\
&= \int_{1}^{\infty} x \alpha x^{-\alpha - 1}\,dx \\
&= \int_{1}^{\infty} \alpha x^{-\alpha}\,dx \\
&= \alpha \int_{1}^{\infty} x^{-\alpha}\,dx \\
&= \alpha \left[\frac{x^{1-\alpha}}{1-\alpha}\right]_{1}^{\infty}  \\
&= \alpha \left( \lim_{x\to\infty} \frac{x^{1-\alpha}}{1-\alpha} - \frac{1^{1-\alpha}}{1-\alpha} \right) \\
&= \alpha \left(0 - \frac{1}{1-\alpha}\right) \\
&= \frac{\alpha}{\alpha-1}
\end{align*}
\end{answer}
\subsection*{b}
\label{sec:orga855458}
Find \(\operatorname{Var}(X)\) for those values of \(\alpha\) for which it exists.
\begin{answer}
We first need to find \(E(X^2)\).
\begin{align*}
E(X^2) &= \int_{-\infty}^{\infty} x^2 f(x)\,dx \\
       &= \int_{1}^{\infty} x^2 f(x)\,dx \\
       &= \int_{1}^{\infty} x^2 \alpha x^{-\alpha - 1}\,dx \\
       &= \int_{1}^{\infty} x^2 \alpha x^{-\alpha - 1}\,dx \\
       &= \alpha \int_{1}^{\infty} x^2 x^{-\alpha - 1}\,dx \\
       &= \alpha \int_{1}^{\infty} x^{-\alpha+1}\,dx \\
       &= \alpha \left[\frac{x^{-\alpha+2}}{-\alpha + 2}\right]_{1}^{\infty} \\
       &= \alpha \left(\lim_{x\to\infty} \frac{x^{-\alpha+2}}{-\alpha + 2} - \frac{1}{-\alpha + 2}\right) \\
       &= \alpha \left(0 - \frac{1}{-\alpha + 2}\right) \\
       &= \frac{\alpha}{2 -\alpha}
\end{align*}
Next we calculate \(\operatorname{Var}(X)\).
\begin{align*}
\operatorname{Var}(X) &= E(X^2) - E(X)^2 \\
                      &= \frac{\alpha}{2 -\alpha} - \left(\frac{\alpha}{\alpha-1}\right)^2 \\
                      &= \frac{\alpha}{2 -\alpha} - \frac{\alpha^2}{(\alpha-1)^2}
\end{align*}
\end{answer}
\section*{5}
\label{sec:org753cc18}
Let \(X\) have the density
\[ f(x)=\frac{1+\alpha x}{2}, ~~~ -1 \leq x \leq 1, ~~~ -1 \leq \alpha \leq 1 \]
Find \(E(x)\) and \(\operatorname{Var}(X)\).
\begin{answer}
\begin{align*}
E(X) &= \int_{-\infty}^{\infty} x f(x)\,dx \\
     &= \int_{-1}^{1} x \frac{1 + \alpha x}{2} \,dx \\
     &= \frac{1}{2} \int_{-1}^{1} x + \alpha x^2 \,dx \\
     &= \frac{1}{2} \left( \int_{-1}^{1} x \,dx + \alpha \int_{-1}^{1} x^2 \,dx \right) \\
     &= \frac{1}{2} \left( \left[\frac{x^2}{2}\right]_{-1}^{1} + \alpha \left[\frac{x^3}{3}\right]_{-1}^{1} \right) \\
     &= \frac{1}{2} \left( \left(\frac{1}{2} - \frac{1}{2}\right) + \alpha \left(\frac{1}{3} - \frac{-1}{3}\right) \right) \\
     &= \frac{1}{2} \left(\alpha \frac{2}{3} \right) \\
     &= \frac{\alpha}{3}
\end{align*}
We must find \(E(X^2)\).
\begin{align*}
E(X^2) &= \int_{-\infty}^{\infty} x^2 f(x^2)\,dx \\
       &= \int_{-\infty}^{\infty} x^2 f(x)\,dx \\
       &= \int_{-1}^{1} x^2 \frac{1+\alpha x}{2} \,dx \\
       &= \frac{1}{2} \int_{-1}^{1} x^2+\alpha x^3 \,dx \\
       &= \frac{1}{2} \left(\int_{-1}^{1} x^2 \,dx + \alpha \int_{-1}^{1} x^3 \,dx\right) \\
       &= \frac{1}{2} \left(\left[\frac{x^3}{3}\right]_{-1}^{1} + \alpha \left[\frac{x^4}{4}\right]_{-1}^{1} \right) \\
       &= \frac{1}{2} \left(\left(\frac{1}{3} - \frac{-1}{3} \right) + 0 \right) \\
       &= \frac{1}{2} \left(\frac{2}{3}\right) \\
       &= \frac{1}{3}
\end{align*}

Now we find \(\operatorname{Var}(X)\).
\begin{align*}
\operatorname{Var}(X) &= E(X^2) - E(X)^2 \\
                      &= \frac{1}{3} - \left(\frac{\alpha}{3}\right)^2 \\
                      &= \frac{1}{3} - \frac{\alpha^2}{9} \\
                      &= \frac{3 - \alpha^2}{9}
\end{align*}
\end{answer}
\section*{14}
\label{sec:org022467a}
Let \(X\) be a continuous random variable with the density function
\[ f(x) = 2x, ~~~ 0 \leq x \leq 1 \]
\subsection*{a}
\label{sec:org1f0e46f}
Find \(E(X)\).
\begin{answer}
\begin{align*}
E(X) &= \int_{-\infty}^{\infty} x f(x)\,dx \\
     &= \int_{0}^{1} x 2x \,dx \\
     &= 2 \int_{0}^{1} x^2 \,dx \\
     &= 2 \left[\frac{x^3}{3}\right]_{0}^{1} \\
     &= 2 \left(\frac{1}{3} - \frac{0}{3}\right) \\
     &= \frac{2}{3}
\end{align*}
\end{answer}
\subsection*{b}
\label{sec:org8657397}
Find \(E(X^2)\) and \(\operatorname{Var}(X)\)
\begin{answer}
\begin{align*}
E(X^2) &= \int_{-\infty}^{\infty} x^2 f(x)\,dx \\
     &= \int_{0}^{1} x^2 2x \,dx \\
     &= 2 \int_{0}^{1} x^3 \,dx \\
     &= 2 \left[\frac{x^4}{4}\right]_{0}^{1} \\
     &= 2 \frac{1}{4}  \\
     &= \frac{1}{2} \\\\
\operatorname{Var}(X) &= E(X^2) - E(X)^2 \\
                      &= \frac{1}{2} - \left(\frac{2}{3}\right)^2 \\
                      &= \frac{1}{2} - \frac{4}{9} \\
                      &= \frac{1}{18}
\end{align*}
\end{answer}
\end{document}