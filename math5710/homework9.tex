% Created 2019-11-22 Fri 23:38
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\input{../preamble.tex}
\author{Abraham White}
\date{\today}
\title{Homework 8}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Homework 8},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{80}
\label{sec:org71a7903}
Let \(X\) be a continuous random variable with density function \(f(x) = 2x\), \(0 \leq x \leq 1\). Find the
moment-generating function of \(X\), \(M(t)\), and verify that \(E(X) = M'(0)\) and that \(E(X^2) = M''(0)\).
\begin{answer}
The moment-generating function for any continuous random variable is
\[ M(t) = \int_{-\infty}^{\infty} e^{tx} f(x)\,dx \]
\begin{align*}
M(t) &= \int_{-\infty}^{\infty} e^{tx} f(x)\,dx \\
     &= \int_{0}^{1} e^{tx} 2x\,dx \\
     &= 2 \int_{0}^{1} e^{tx} x\,dx
     \shortintertext{Integrate by parts: $f = x,~dg = e^{tx}\,dx,~df = dx,~g=e^{tx}/t$}
     &= \frac{2x e^{tx}}{t} \evald_0^1 - 2\int_{0}^{1} \frac{e^{tx}}{t}\,dx \\
     &= \frac{2x e^{tx}}{t} \evald_0^1 - \frac{2}{t}\int_{0}^{1} e^{tx}\,dx \\
     &= \frac{2x e^{tx}}{t} \evald_0^1 - \frac{2}{t} \left( \frac{e^{tx}}{t} \evald_0^1 \right) \\
     &= \frac{2}{t} \left(x e^{tx} \evald_0^1 - \left( \frac{e^{tx}}{t} \evald_0^1 \right)\right) \\
     &= \frac{2}{t} \left(e^{t} - \left( \frac{e^{t}}{t} - \frac{1}{t} \right)\right) \\
     &= - \frac{2e^{t}}{t^2} + \frac{2}{t^2} + \frac{2e^{t}}{t}  
\end{align*}
Now we need to find \(E(X)\) and \(E(X^2)\).
\begin{align*}
E(X) &= \int_{-\infty}^{\infty} x f(x) \, dx \\
     &= \int_{0}^{1} x 2x \, dx \\
     &= \int_{0}^{1} 2x^2 \, dx \\
     &= \frac{2x^3}{3} \evald_{0}^{1} \\
     &= \frac{2}{3} \\
E(X^2) &= \int_{-\infty}^{\infty} x^2 f(x) \, dx \\
       &= \int_{0}^{1} x^2 2x \, dx \\
       &= \int_{0}^{1} 2x^3 \, dx \\
       &= \frac{2x^4}{4} \evald^1_0 \\
       &= \frac{1}{2}
\end{align*}
Finally, we find \(M'(0), M''(0)\) and compare.
\begin{align*}
M' &= \frac{d}{dt} \left(- \frac{2e^{t}}{t^2} + \frac{2}{t^2} + \frac{2e^{t}}{t} \right) \\
   &= \frac{2e^t(t^2 - 2t + 2)-4}{t^3} \\
M'' &= \frac{d}{dt} \left(\frac{2e^t(t^2 - 2t + 2)-4}{t^3}\right) \\
    &= \frac{2e^t(t^3 - 3t^2 + 6t - 6) + 12}{t^4}
\end{align*}
It's obvious that both \(M'(0)\) and \(M''(0)\) are undefined, but we can find \(\lim_{t \to 0} M'\) and
\(\lim_{t \to 0} M''\) through multiple applications of l'Hoptial's rule. This gives us
\(\lim_{t \to 0} M' = \frac{2}{3} = E(X)\) and \(\lim_{t \to 0} M'' = \frac{1}{2} = E(X^2)\).
\end{answer}
\section*{81}
\label{sec:org4755da0}
Find the moment-generating function of a Bernoulli random variable, and use it to find the mean, variance,
and third moment.
\begin{answer}
The Bernoulli distribution is discrete, so to find the moment-generating function we will use the discrete
variant: \[ M(t) = \sum_x e^{tx}p(x) \]
The probability mass function for a Bernoulli random variable is
\[ p(x) = \begin{cases}p & x = 1\\1-p & x=0\end{cases} \]
Using this we can find the mgf for a Bernoulli random variable.
\begin{align*}
M(t) &= \sum_x e^{tx}p(x) \\
     &= e^{t \cdot 0}p(0) + e^{t \cdot 1}p(1) \\
     &= 1-p + e^t p \\
     &= pe^t - p + 1
\end{align*}
For a given random variable with moment \(M(t)\), the mean is \(E(X) = M'(0)\), the variance is
\(\Var(X) = E(X^2) - E(X)^2 = M''(0) - M'(0)^2\), and the third moment is \(M'''(0)\).
We must find \(M', M''\) and \(M'''\).
\begin{align*}
M'(t) &= \frac{d}{dt} (pe^t - p + 1) \\
      &= \frac{d}{dt}pe^t - \frac{d}{dt}p + \frac{d}{dt}1 \\
      &= pe^t \\
M''(t) &= M'''(t) = \frac{d}{dt} pe^t = pe^t
\end{align*}
The mean
\[ E(X) = M'(0) = p \]
The variance
\begin{align*}
\Var(X) &= M''(0) - M'(0)^2 \\
        &= p - p^2 \\
        &= p(1 - p)
\end{align*}
The third moment
\[ M'''(0) = p \]
\end{answer}
\section*{82}
\label{sec:org9e50691}
Use the result of Problem 81 to find the mgf of a binomial random variable and its mean and variance.
\begin{answer}
We can express a binomial random variable as a sum of independent Bernoulli trials. Let
\(Y \sim \operatorname{B}(n,p)\) be a Binomial random variable with probability \(p\) and \(n\) trials.
We can say \(Y = X_1 + \cdots + X_n\) where \(X\) is an independent Bernoulli random variable with
probability \(p\).
We can use Property D from the book to find \(M_Y\), the moment-generating function for \(Y\).
\begin{corollary*}{D}
If $X$ and $Y$ are independent random variables with mgf's $M_X$ and $M_Y$, and $Z = X + Y$, then
$M_Z(t) = M_X(t) + M_Y(t)$ on the common interval where both mgf's exist.
\end{corollary*}
There are \(n\) such random variables in \(Y\), so \(M_Y(t) = M_X(t)^n = (pe^t - p + 1)^n\).

Now we must find \(M_Y'\) and \(M_Y''\).
\begin{align*}
M_Y'(t) &= \frac{d}{dt} (pe^t - p + 1)^n
        \shortintertext{Use chain rule with $f = x^n$ and $g = pe^t - p + 1$}
        &= npe^t(pe^t - p + 1)^{n-1} \\
M_Y''(t) &= \frac{d}{dt} npe^t(pe^t - p + 1)^{n-1} \\
         &= np \frac{d}{dt} e^t(pe^t - p + 1)^{n-1}
         \shortintertext{Use product rule}
         &= np \left(\frac{d}{dt} e^t \cdot (pe^t - p + 1)^{n-1} + e^t \cdot \frac{d}{dt} (pe^t - p + 1)^{n-1} \right) \\
         &= np \left(e^t \cdot (pe^t - p + 1)^{n-1} + e^t \cdot (n-1)pe^t(pe^t - p + 1)^{n-2} \right) \\
         &= npe^t (pe^t - p + 1)^{n-1} + n(n-1)p^2e^{2t}(pe^t - p + 1)^{n-2}
\end{align*}
The mean
\begin{align*}
E(Y) &= M_Y'(0) = npe^0(pe^0 - p + 1)^{n-1} \\
     &= np(p - p + 1)^{n-1} \\
     &= np
\end{align*}
The second moment
\begin{align*}
M_Y''(0) &= npe^0 (pe^0 - p + 1)^{n-1} + n(n-1)p^2e^{2 \cdot 0}(pe^0 - p + 1)^{n-2} \\
         &= np (p - p + 1)^{n-1} + n(n-1)p^2(p - p + 1)^{n-2} \\
         &= np 1^{n-1} + n(n-1)p^2 1^{n-2} \\
         &= n(n-1)p^2 + np
\end{align*}
The variance
\begin{align*}
\Var(Y) &= M_Y''(0) - M_Y'(0)^2 \\
        &= n(n-1)p^2 + np - n^2p^2 \\
        &= n^2p^2 - np^2 + np - n^2p^2 \\
        &= -np^2 + np \\
        &= np(1 - p)
\end{align*}
\end{answer}
\section*{91}
\label{sec:orgf08b6de}
Use the mgf to show that if \(X\) follows an exponential distribution, \(cX~(c > 0)\) does also.
\begin{answer}
 Two variables will follow the
same distribution if their moment generating functions are equal. We have an exponential random variable 
\(X \sim \operatorname{Exp}(\lambda)\). Let \(Y = cX\).

First we must find \(M_X\).
\begin{align*}
M_X(t) &= \int_{-\infty}^{\infty} e^{tx} f(x) \, dx \\
       &= \int_{0}^{\infty} e^{tx} \lambda e^{-\lambda x} \, dx \\
       &= \lambda \int_{0}^{\infty} e^{x(t - \lambda)} \, dx
       \shortintertext{Let $u = x(t - \lambda)$ and $du = (t-\lambda)dx$}
       &= \lambda \int_{x=0}^{\infty} \frac{e^u}{t-\lambda} \, du \\
       &= \frac{\lambda}{t-\lambda} \int_{x=0}^{\infty} e^u \, du \\
       &= \frac{\lambda}{t-\lambda} \left(e^{x(t-\lambda)} \evald_{0}^{\infty}\right) \\
       &= \frac{\lambda}{t-\lambda} \left(\lim_{x\to\infty} e^{x(t-\lambda)} - 1\right)
       \shortintertext{Assuming $t < \lambda$ this goes to 0}
       &= \frac{\lambda}{t-\lambda} (0-1) \\
       &= \frac{\lambda}{\lambda - t}
\end{align*}
Next we must find \(M_Y\). The moment generating function of a random variable \(X\) is \(M(t) = E(e^tX)\). We
can use this property to derive \(M_Y\) by substituting \(Y = cX\).
\begin{align*}
M_Y(t) &= E(e^{tY}) = E(e^{tcX}) = M_X(ct) \\
       &= \frac{\lambda}{\lambda - ct} \\
       &= \frac{\lambda/c}{\lambda/c - t}
\end{align*}
This is the moment-generating function for an exponential random variable with parameter \(\lambda/c\).
\end{answer}
\section*{5}
\label{sec:org93dcc80}
Using moment-generating functions, show that as \(n \to \infty,p \to 0\), and \(np \to \lambda\), the Binomial
distribution with parameters \(n\) and \(p\) tends to the Poisson distribution.
\begin{answer}
The moment-generating function for a binomial random variable is
\[ (pe^t - p + 1)^n \]
We will manipulate this mgf until we can find that as \(n \to \infty, p \to 0\), and \(np \to \lambda\), this
becomes the mgf for the Poisson distribution. Let \(M_X\) be a binomial mgf and \(M_Y\) be a Poisson mgf.
\begin{align*}
\lim_{n\to\infty,p\to 0,np\to\lambda} M_X &= \lim_{n\to\infty,p\to 0,np\to\lambda} (pe^t - p + 1)^n \\
    &= \lim_{n\to\infty,p\to 0,np\to\lambda}(p(e^t - 1) + 1)^n \\
    &= \lim_{n\to\infty,p\to 0,np\to\lambda} (\frac{np}{n}(e^t - 1) + 1)^n \\
    &= \lim_{n\to\infty} (\lambda(e^t - 1) + 1)^n \\
    \shortintertext{Notice that this is the definition of the exponential $e^x$, where $x = \lambda(e^t - 1)$}
    &= e^{\lambda(e^t - 1)}
\end{align*}
This is the mgf for the Poisson distribution with parameter \(\lambda\), so the Binomial distribution tends to
the Poisson distribution.
\end{answer}
\end{document}