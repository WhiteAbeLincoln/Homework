% Created 2019-09-09 Mon 12:19
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\textheight=9.25in \textwidth=7in \topmargin=-0.75in \oddsidemargin=-0.25in \evensidemargin=-0.25in \usepackage{mathtools} \usepackage{pgfplots}
\author{Abraham White}
\date{\today}
\title{Math 5710}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Math 5710},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Intro to Probability}
\label{sec:org94d6af6}
\subsection*{Definitions}
\label{sec:orge8d1e43}
\begin{description}
\item[{Random Experiment}] An experiment which may generate more than one outcome
\begin{itemize}
\item Coin toss, Dice roll
\item Rolling two d6 dice: 36 possible outcomes, \(6^n\) for \(n\) dice
\end{itemize}
\item[{Sample Space}] The set of all possible outcomes for a random experiment
\begin{itemize}
\item Denoted by \(S\) or \(\Omega\)
\end{itemize}
\item[{Event}] An event is a subset of a sample space
\begin{itemize}
\item Note: Generally we use uppercase english letters near the beginning of the
alphabet to represent events: Event A, Event B
\item Roll two dice: Identify the event that the addition of the dice is 5
\begin{itemize}
\item \(A\) = Addition of two d6 dice is 5
\item \(A = \{(1,4),(4,1),(2,3),(3,2)\}\)
\item Probability of \(A\): 4/36
\end{itemize}
\end{itemize}
\item[{Probability Function}] A probability function on \(\Omega\) is a function that
satisfies the following three axioms:
\begin{enumerate}
\item For any event \(A\), \(P(A) \geq 0\) (non-negative)
\item If \(\Omega\) is a sample space, then \(P(\Omega) = 1\) (certainty)
Probability for the whole sample space must add to one. We know some
item in \(\Omega\) must always occur.
\item If \(A_1, A_2 \ldots\) is an infinite sequence of pairwise, disjoint events
(\(A_i \cap A_j \neq \varnothing\) when \(i \neq j\)), then
\(P(\cup_{i=1}^{\infty} A_i) = \sum_{i=1}^{\infty} P(A_i)\) or
\(P(A \cup B) = P(A) + P(B)\)
\end{enumerate}
\end{description}
\subsection*{Review of Set Theory}
\label{sec:orgeed4082}
\begin{itemize}
\item \(\varnothing\) is the empty set.
\item \(\{\}\) is another notation for the empty set.
\item \(A \cup B\) is set union - at least \(A\) or \(B\) occurs
\item \(A \cap B\) is set intersection - both \(A\) and \(B\) occurs
\end{itemize}
\subsection*{Basic Properties of \(P(A)\)}
\label{sec:org679aa5c}
\begin{enumerate}
\item Complement Rule\\
Let \(A^c\) be the complement of \(A\), that is \(A^c\) is not \(A\).
\begin{align*}
1 = P(\Omega) &= P(A \cup A^c) = P(A) + P(A^c) \\
P(A^c) &= 1 - P(A)
\end{align*}
\item General addition rule\\
For any two events \(A, B\): \(P(A \cup B) = P(A) + P(B) - P(A \cap B)\)
\begin{align*}
P(A) &= P((A \cap B) \cup (A \cap B^c))
\end{align*}
\end{enumerate}
\subsection*{Examples}
\label{sec:org4a14629}
\subsubsection*{Pick a letter at random from the word TENNESSEE}
\label{sec:orgb715e7e}
\begin{enumerate}
\item Identify \(\Omega\) \\
\(\Omega = \{T, E, N, S\}\) \\
Events: \\
  \(A := \text{The letter T is picked}\) \\
  \(B := \text{The letter E is picked}\) \\
  \(C := \text{The letter N is picked}\) \\
  \(D := \text{The letter S is picked}\)
\item What probabilities should be assigned to the outcomes? \\
\(P(A) = 1/9\) \\
\(P(B) = 4/9\) \\
\(P(C) = 2/9\) \\
\(P(D) = 2/9\)
\end{enumerate}

\section*{Conditional Probability}
\label{sec:org9bac93a}
\emph{Given two events \(A\) and \(B\), the probability that \(A\) occurs, given \(B\) has occured is}:
\[
  P(A \mid B) = \frac{P(A \cap B)}{P(B)}
  \]
We can use this definition to write \(P(A \cap B)\) in terms of conditional probabilities:
\[
  P(A \cap B) = P(B) * P(A \mid B)
  \]
and
\[
  P(B \cap A) = P(A) * P(B \mid A)
  \]
Though \(P(A \cap B) = P(B \cap A)\), the right hand side of the two equations aren't the same.
In general \(P(A \mid B) \ne P(B \mid A)\).
\subsection*{Example 1}
\label{sec:org5d76abc}
Role a tetrahedral die (d4) twice. Consider the events
\begin{itemize}
\item \(B = min(X,Y) \text{ is } 2\)
\item \(M = max(X,Y)\)
\end{itemize}
Compute the following probabilities:
\begin{itemize}
\item \(P(M = 1 \mid B) = 0\)
\item \(P(M = 2 \mid B) = 1/5\)
\end{itemize}
\subsection*{Example 2}
\label{sec:org84d4b22}
Suppose there is a plane overhead 5\% of the time. You can report a plane overhead with 99\%
accuracy when present using a radar gun. When no plane is present the radar will report a plane
overhead 10\% of the time (false positive). Let \(A\) be the event that there is a plane overhead
and \(B\) is the event that the radar reports a plane overhead. Compute the following:

\begin{itemize}
\item \(P(A \cup B)\)
\item \(P(B)\)
\item \(P(B \mid A)\)
\item \(P(A \mid B)\)
\end{itemize}
\begin{quote}
We know: \\
\(P(A) = .05\) \\
\(P(B \mid A) = .99\) \\
\(P(B \mid A^C) = .1\)

\begin{align*}
P(A \cap B) &= P(A) * P(B \mid B) \\
&= .05 * .99 \\
&= .0495
\end{align*}
\begin{align*}
P(A^C \mid B) &= P(A^C) * P(B \mid A^C) \\
&= .095
\end{align*}
\begin{align*}
P(B) &= P(A \cap B) + P(A^C \cap B) \\
     &= .1445
\end{align*}
\begin{align*}
P(A \mid B) &= \frac{P(A \cap B)}{P(B)} \\
&= \frac{.0495}{.1445}
\end{align*}
\end{quote}
\subsection*{Law of Total Probability}
\label{sec:org65ee159}
Let \(B_1, B_2, \ldots, B_n\) be such that \(\bigcup^n_{i=1}B_i = \Omega\) and \(B_i, B_j = \empty\) for \(i \ne j\),
with \(P(B_i) > 0\) for all \(i\). Then, for any event \(A\),
\[
   P(A) = \sum^n_{i=1} P(A \mid B_i) P(B_i) = \sum^n_{i = 1} P(A \cap B_i)
   \]
\subsubsection*{Proof}
\label{sec:org707677c}
\begin{align*}
P(A \cap \Omega) \\
&= P\left(A \cap \left(\sum^n_{i=1} B_i \right)\right) \\
&= P\left(\sum^n_{i=1} (A \cap B_i)\right) \\
&= \sum^n_{i=1} P(A \cap B_i) \\
&= \sum^n_{i=1} P(A \mid B_i) P(B_i)
\end{align*}
\end{document}