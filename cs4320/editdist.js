const editDistanceMat = (s1,s2) => {
  const m = Array.from({ length: s1.length + 1 }, () => Array.from({ length: s2.length + 1  }, () => 0))

  for (let i = 1; i <= s1.length; ++i) {
    m[i][0] = i
  }

  for (let j = 1; j <= s2.length; ++j) {
    m[0][j] = j
  }

  for (let i = 1; i <= s1.length; ++i) {
    for (let j = 1; j <= s2.length; ++j) {
      m[i][j] = Math.min(
        m[i - 1][j - 1] + (s1.charAt(i) === s2.charAt(j) ? 0 : 1), // substitution
        m[i - 1][j] + 1, // deletion
        m[i][j - 1] + 1, // insertion
      )
    }
  }

  return m
}

const editDistance = (s1,s2, m = editDistanceMat(s1,s2)) => m[s1.length][s2.length]

const stringMat = m => m.map(r => r.join('|')).join('\n')

const displayEditMat = (s1,s2, m) => stringMat([[" ", "ε", ...s2], ...m.map((v, i) => ([i === 0 ? "ε" : s1.charAt(i - 1), ...v]))])

const fn = (s1, s2) => {
  const m = editDistanceMat(s1,s2)
  const s = displayEditMat(s1,s2,m)
  const e = editDistance(s1,s2, m)

  return `${e}\n${s}`
}

const [,,s1 = "paris",s2 = "alice"] = process.argv

console.log(fn(s1,s2))
