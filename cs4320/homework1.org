#+OPTIONS: toc:nil num:nil
#+LATEX_CLASS_OPTIONS: [10pt,AMS Euler]
#+LATEX_HEADER: \textheight=9.25in \textwidth=7in \topmargin=-0.75in \oddsidemargin=-0.25in \evensidemargin=-0.25in \usepackage{mathtools} \usepackage{pgfplots}
#+AUTHOR: Abraham White
#+TITLE: Homework 1
* Manning 1.2
  Consider these documents:\\
  
  \noindent *Doc 1* breakthrough drug for schizophrenia \\
  *Doc 2* new schizophrenia drug \\
  *Doc 3* new approach for treatment of schizophrenia \\
  *Doc 4* new hopes for schizophrenia patients \\
  
  1. Draw the term-document incidence matrix for this document collection.

  2. Draw the inverted index representation for this collection.

* Manning 1.3
  For the document collection shown in Excersise 1.2, what are the returned results
  for these queries:
  
  1. =schizophrenia AND drug=
  2. =for AND NOT(drug OR approach)=
   
* Manning 1.4
  For the queries below, can we still run through the intersection in time $O(x + y)$,
  where $x$ and $y$ are the lengths of the postings lists for =Brutus= and =Caesar=? If not,
  what can we achieve?
  
  1. =Brutus AND NOT Caesar=
  2. =Brutus OR NOT Caesar=

* Manning 1.7
  Given the following postings list sizes, recommend a query processing order.
  
  | Term         | Postings size |
  |--------------+---------------|
  | eyes         |        213312 |
  | kaleidoscope |         87009 |
  | marmalade    |        107913 |
  | skies        |        271658 |
  | tangerine    |         46653 |
  | trees        |        316812 |

  1. =(tangerine OR trees) AND (marmalade OR skies) AND (kaleidoscope OR eyes)=
  
* Manning 1.8
  If the query is: \\
  \\
  =friends AND romans AND (NOT countrymen)= \\
  \\
  \noindent how could we use the frequency of countrymen in evaluating the best query evaluation
  order? In particular, propose a way of handling negation in determining the order of
  query processing.
  
* Manning 1.13
  Try using the Boolean search features on a couple of major web search engines. For instance,
  choose a word, such as =burglar=, and submit the queries (i) =burglar=, (ii) =burglar AND burglar=,
  and (iii) =burglar OR burglar=. Look at the estimated number of results and top hits. Do they make
  sense in terms of Boolean logic? Often they haven't for major search engines. Can you make sense of
  what is going on? What about if you try different words? For example, query (i) =knight=, (ii) =conquer=,
  and then (iii) =knight OR conquer=. What bound should the number ofr results from the first two queries
  place on the third query? Is this bound observed?
  
* Manning 2.2
  Suggest what normalized form should be used for these words (including the word itself as a possibility):
  
  1. 'Cos
  2. Shi'ite
  3. cont'd
  4. Hawai'i
  5. O'Rourke
    
* Manning 2.3
  The following pairs of words are stemmed to the same form by the Porter stemmer.
  Which pairs would you argue shouldn't be conflated. Give your reasoning.
  
  1. abandon/abandoned\\
     This is correct.
  2. absorbency/absorbent\\
     This is correct.
  3. marketing/markets\\
     This is incorrect. Marketing refers to the act of advertising or exchanging products, while markets
     can refer to physical or virtual spaces for selling goods.
  4. university/universe\\
     This is incorrect. University refers to a place for higher learning, whereas universe refers to
     an overarching collection or space that contains all things under consideration.
  5. volume/volumes\\
     This is correct
  
* Manning 2.4
  For the porter stemmer rule group shown in (2.1):
  
  1. What is the purpose of including an identity rule such as $SS \rightarrow SS$
  2. Applying just this rule group, what will the following words be stemmed to? /circus/ /canaries/ /boss/
  3. What rule should be added to correctly stem /pony/?
  4. The stemming for /ponies/ and /pony/ might seem strange. Does it have a deleterios effect on retrieval?
     Why or why not?
* Manning 2.5
  Why are skip pointers not useful for queries of the form =x OR y=?
* Manning 2.6
  We have a two-word query. For one term the postings list consists of the following 16 entries:
  $$[4,6,10,12,14,16,18,20,22,32,47,81,120,122,157,180]$$
  and for the other it is the one entry postings list:
  $$[47]$$
  Work out how many comparisons would be done to intersect the two postings lists with the following two
  strategies. Briefly justify your answers:
  1. Using standard postings lists
  2. Using postings list s stored with skip pointers, with a skip length of $\sqrt{P}$, as suggested
     in Section 2.3.
     
* Manning 2.8
  Assume a biword index. Give an example of a document which will be returned for a query of
  =New York University= but is actually a false positive which should not be returned.
  
* Manning 2.9
  Shown below is a portion of a positional index in the format:
  term: doc1: <position1,position2,...>;doc2: <position1,position2,...>; etc.
  #+BEGIN_EXAMPLE
  angels : 2: <36,174,252,651>   ; 4: <12,22,102,432>     ; 7: <17>              ;
  fools  : 2: <1,17,74,222>      ; 4: <8,78,108,458>      ; 7: <3,13,23,193>     ;
  fear   : 2: <87,704,722,901>   ; 4: <13,43,113,433>     ; 7: <18,328,528>      ;
  in     : 2: <3,37,76,444,851>  ; 4: <10,20,110,470,500> ; 7: <5,15,25,195>     ;
  rush   : 2: <2,66,194,321,702> ; 4: <9,69,149,429,569>  ; 7: <4,14,404>        ;
  to     : 2: <47,86,234,999>    ; 4: <14,24,774,944>     ; 7: <199,319,599,709> ;
  tread  : 2: <57,94,333>        ; 4: <15,35,155>         ; 7: <20,320>          ;
  where  : 2: <67,124,393,1001>  ; 4: <11,41,101,421,431> ; 7: <16,36,736>       ;
  #+END_EXAMPLE
  Which document(s) if any match each of the following queries, where each expression within
  quotes is a phrase query?
  
  1. ="fools rush in"=
  2. ="fools rush in" AND "angels fear to tread"=
  
* Manning 2.10
  Consider the following fragment of a positional index:
  #+BEGIN_EXAMPLE
  Gates     : 1: <3> ; 2: <6>    ; 3: <2,17> ; 4: <1>        ;
  IBM       : 4: <3> ; 7: <14>   ;
  Microsoft : 1: <1> ; 2: <1,21> ; 3: <3>    ; 5: <16,22,51> ;
  #+END_EXAMPLE
  The $/k$ operator, =word1 /k word2= finds occurrences of =word1= within $k$ words of =word2=
  (on either side), where $k$ is a positive integer argument. Thus $k = 1$ demands that =word1=
  be adjacent to =word2=.
  1. Describe the set of documents that satisify the query =Gates /2 Microsoft=.
  2. Describe each set of values for $k$ for which the query =Gates /k Microsoft= returns a
     different set of documents as the answer.
     
* Manning Preface & CH1 vs Bengfort Preface & CH1
* Bengfort gender analysis results
* COMMENT Local Variables
  # Local Variables:
  # eval: (add-hook 'after-save-hook 'org-latex-export-to-pdf nil t)
  # End:
