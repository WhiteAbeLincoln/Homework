% Abraham White A02178955

% Prints a cell of the maze
p_space(Maze, Row, Column) :- maze(Maze, Row, Column, open), write('.').
p_space(Maze, Row, Column) :- maze(Maze, Row, Column, barrier), write('x').

% prints a cell of the maze or a * if the current location is in the list
printCell(_, Row, Column, L) :- member([Row, Column], L), write('*').
printCell(Maze, Row, Column, _) :- p_space(Maze, Row, Column).

% prints all the cells of a row Row in a maze Maze, starting at column Column
printCells(Maze, Row, Column, L) :- printCell(Maze, Row, Column, L), C1 is Column + 1, printCells(Maze, Row, C1, L); true.

% prints a row of dash characters from Count to Max inclusive
printDash(Count, Max) :- Count =< Max, write('-'), printDash(Count + 1, Max); true.

% prints a row of a maze (including borders)
printRow(Maze, 0, _) :- write('+'), mazeSize(Maze, _, Column), printDash(1, Column), write('+'), nl.
printRow(Maze, R, _) :- R1 is R - 1, mazeSize(Maze, R1, C), write('+'), printDash(1, C), write('+'), nl.
printRow(Maze, R, L) :- maze(Maze, R, _, _), write('|'), printCells(Maze, R, 1, L), write('|'), nl.

% prints all the rows of a maze (including borders)
printRows(Maze, R, Max, L) :- R =< Max, printRow(Maze, R, L), R1 is R + 1, printRows(Maze, R1, Max, L); true.

% prints a maze
printMaze(Maze, L) :- mazeSize(Maze, Row, _), printRows(Maze, 0, Row + 1, L).

printList([]) .
printList([H|T]) :- write(H), nl, printList(T).

% Try a move in an "Up" direction, assumes Row and Column are bound
try(Row, Column, NextRow, NextColumn) :- NextRow is Row, NextColumn is Column - 1.
% Try a move in an "Down" direction, assumes Row and Column are bound
try(Row, Column, NextRow, NextColumn) :- NextRow is Row, NextColumn is Column + 1.
% Try a move in an "Left" direction, assumes Row and Column are bound
try(Row, Column, NextRow, NextColumn) :- NextRow is Row - 1, NextColumn is Column.
% Try a move in an "Right" direction, assumes Row and Column are bound
try(Row, Column, NextRow, NextColumn) :- NextRow is Row + 1, NextColumn is Column.

% Attempts to move from a row and column to a goal row and goal column
% keeping track in a list
move(_, List, NL, R, C, R, C) :- append(List, [[R, C]], NL). % append the final position and emit the new list
move(Maze, List, NewList, Row, Column, GoalRow, GoalColumn) :-
    try(Row, Column, NR, NC), % gets all the possible next positions
    not(member([NR, NC], List)), % if we haven't already tried the next position 
    maze(Maze, NR, NC, open), % and the position is open 
    append(List, [[Row, Column]], NL), % add our current position to the tracked list
    move(Maze, NL, NewList, NR, NC, GoalRow, GoalColumn). % move to that position

% solves a path through the maze from 1,1 to the opposite corner, and prints the solution
solve(Maze) :- mazeSize(Maze, R, C), move(Maze, [], L, 1, 1, R, C), printMaze(Maze, L).

% vim: ft=prolog
