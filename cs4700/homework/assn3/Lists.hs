module Lists where
  -- AUTHOR: Abraham White A02178955

  -- | An infinite list of counting numbers
  countingNumbers :: [Int]
  countingNumbers = [1..] -- infinite range

  -- | An infinite list of even numbers
  evenNumbers :: [Int]
  evenNumbers = [2,4..] -- uses range syntax with a step

  -- | An infinite list of prime numbers
  primeNumbers :: [Int]
  primeNumbers = [ x | x <- countingNumbers, isPrime x ]

  -- | Evaluates whether an integer is prime
  isPrime :: Int -> Bool
  isPrime 1 = False
  isPrime x = all (\y -> gcd x y == 1) [y | y <- [2..x-1]]

  -- | Merges two lists using a predicate function for comparison
  mergeP :: (t -> t -> Bool) -> [t] -> [t] -> [t]
  mergeP p [] a = a
  mergeP p a [] = a
  mergeP p a@(x:xs) b@(y:ys) = if p x y
                                  then x:(mergeP p xs b)
                                  else y:(mergeP p a ys)

  -- | Merges two lists of orderable items
  merge :: Ord t => [t] -> [t] -> [t]
  merge = mergeP (<=) -- use our mergeP function

  -- | Puts the elements 1..k at the end of the list xs 
  wrap :: Int -> [t] -> [t]
  wrap k [] = []
  wrap k xs = drop k $ take (k + length xs) $ cycle xs

  -- | Extracts the sublist from `start` to `end` (inclusive) of an array
  -- Note that start and end are 0 based.
  --
  -- ==== __Examples__
  --
  -- Usage:
  --
  -- >>> let list = [1,2,3]
  -- >>> slice (0,1) list
  -- [1,2]
  --
  slice :: (Int, Int) -> [t] -> [t]
  slice (start, end) = take ((end+1) - start) . drop start -- we use the point-free style

  -- | Creates a list of all the possible contiguous sublists of a list
  subLists :: [t] -> [[t]]
  subLists xs = [take n xs | n <- [1..length xs]]

  -- | Gives the number of elements in a list of lists
  -- I.E, counts the number of elements in each sublist and sums the results
  countElements :: [[t]] -> Int
  countElements = sum . map length

  -- | Sorts a list of sublists based on the sum of the sublists elements 
  -- Uses a merge sort function with a custom predicate
  sortSubLists :: [[Int]] -> [[Int]]
  sortSubLists [] = []
  sortSubLists [x] = [x]
  sortSubLists xs = mergeP (\x y -> (sum x) <= (sum y)) left right -- merge the sorted sublists
                    where
                        -- splits an array into half, yielding (first, second)
                        splitHalf l = splitAt (length l `div` 2) l 
                        -- call sortSublists on the left half
                        left = sortSubLists (fst $ splitHalf xs)
                        -- call sortSublists on the right half
                        right = sortSubLists (snd $ splitHalf xs)

  -- | Applies a binary function to a list of items
  applyFunc :: (a -> a -> a) -> [a] -> a
  applyFunc f [x] = x
  applyFunc f xs = foldr f (last xs) (init xs)

  -- | Applies a function to every list in a list of lists
  listApply :: (b -> b -> b) -> [[b]] -> [b]
  listApply = map . applyFunc -- another point free style

  -- | Composes a list of unary functions into a single unary function
  composeList :: [(a -> a)] -> (a -> a)
  composeList [] = id
  composeList [f] = f
  composeList (f:xs) = f . composeList xs
