(define (goofy result x total)
  (if (>= x total)
    result
    (goofy (+ result x) (+ x 1) total)))
