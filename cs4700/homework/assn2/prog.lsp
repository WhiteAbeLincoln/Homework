(define (right lst)
  (let ((half (/ (length lst) 2)))
       (0 half lst)
       ))

(define (left lst)
  (let ((half (/ (length lst) 2)))
       (half lst)
       ))

(define (merge-pred a b p)
  (if (empty? a) b
    (empty? b) a
    (p (first a) (first b))
    (cons (first a) (merge (rest a) b))
    (cons (first b) (merge a (rest b)))
    ))

(define (merge a b)
  (merge-pred a b (lambda (x y) (<= x y)))
  )

(define (sort-list lst)
  (if (empty? lst) lst
      (= 1 (length lst)) lst
      (merge (sort-list (left lst)) (sort-list (right lst)))
      ))

(define (sort-functions lst)
  (if (empty? lst) lst
    (= 1 (length lst)) lst
    (merge-pred (sort-functions (left lst)) (sort-functions (right lst))
                (lambda (f g)
                  (let (i 0)
                    (do-until (or (< (f i) (g i)) (> (f i) (g i)))
                              (inc i)
                              (< (f i) (g i))
                              ))))))

(define (compose f g) (expand (lambda (x) (f (g x))) 'f 'g))

(define (compose-list lst)
  (if (empty? lst) (expand (lambda (x) x))
    (= 1 (length lst)) (compose (lst 0) (lambda (x) x))
    (compose (lst 0) (compose-list (rest lst)))
    ))

(define (squares filename n)
  (if (not (ends-with filename ".ps"))
      (begin
       (module "canvas.lsp")
       (cv:canvas "CanvasOne" 1000 1000)
       (set 'ps cv))
    (module "postscript.lsp")
      )

  ;; we set dimensions of 1000x1000, and so our squares will be 96x96 px to fit 10 with margin
  (let (i 0)
    (ps:goto 0 96)
    (ps:turn 90)
    (do-until (= i n)
              (inc i)
              (if (odd? i)
                  (ps:fill-color 0.0 0.0 0.0)
                (ps:fill-color 0.8 0.8 0.8)
                )
              (ps:rectangle 96 96 true)
              (ps:move 96)
              )
    )
  (ps:render filename)
)

(define (deg-to-rad deg)
  (mul deg 3.141592654 (div 1.0 180.0)))

(define (draw-tree ps x y angle depth)
  (if (not (= depth 0))
      (begin
       (let ((x2 (+ x (round (mul depth 10 (cos (deg-to-rad angle))))))
             (y2 (+ y (round (mul depth 10 (sin (deg-to-rad angle)))))))
       (ps:goto x y)
       (ps:drawto x2 y2)
       (draw-tree ps x2 y2 (- angle 45) (- depth 1))
       (draw-tree ps x2 y2 angle (- depth 1))
       (draw-tree ps x2 y2 (+ angle 45) (- depth 1))
       )
      )
  ))

(define (fractal filename dimension)
  (if (not (ends-with filename ".ps"))
      (begin
       (module "canvas.lsp")
       (cv:canvas "CanvasOne" 1000 1000)
       (set 'ps cv))
    (module "postscript.lsp")
    )
  (draw-tree ps 300 500 90 dimension)
  (ps:render filename)
  )
