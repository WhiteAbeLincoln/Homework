#include "compute-pi.h"
#include "util.h"
#include <cmath>

double pi_number(int n) {
  // Uses Chudnovsky_algorithm https://en.wikipedia.org/wiki/Chudnovsky_algorithm
  double over_pi = 12*Sum([](int k) {
      return (std::pow(-1, k) * Factorial(6*k) * (545140134*k + 13591409)) / (Factorial(3*k)*std::pow(Factorial(k),3)*std::pow((640320), (3*k+(3.0/2))));
    }, n);

  return 1/over_pi;
}
