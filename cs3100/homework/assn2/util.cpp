#include "util.h"
// Helper math functions

// Computes the factorial to nth term
int Factorial(int n) {
  if (n == 0) {
    return 1;
  }

  return n*Factorial(n-1);
}

// Computes the summation (∑) of fun from start to end
double Sum(std::function<double(int)> fun, int end, int start, int step) {
  double sum = 0;

  for (int i = start; i < end; i+= step) {
    sum += fun(i);
  }

  return sum;
}
