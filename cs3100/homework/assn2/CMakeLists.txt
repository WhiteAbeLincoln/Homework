cmake_minimum_required(VERSION 3.0)
project(Assign2)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror")

set(SOURCE_FILES main.cpp util.cpp compute-e.cpp compute-pi.cpp compute-fib.cpp)
add_executable(Assign2 ${SOURCE_FILES})
