#include <cstring>
#include <stdio.h>
#include <string>
#include "compute-pi.h"
#include "compute-e.h"
#include "compute-fib.h"

const char* help() {
  return
    "--- Assign 1 Help ---\n"
    "-fib [n] : Compute the fibonacci of [n]\n"
    "-e [n] : Compute the value of 'e' using [n] iterations\n"
    "-pi [n] : Compute Pi to [n] digits";
}

int main(int argc, char** argv) {
  if (argc != 3) {
    printf("%s", help());
    return -1;
  }

  int n = -1;
  try {
    n = std::stoi(argv[2]);
  } catch (...) {
    printf("parameter n could not be converted to number");
    return -1;
  }

  if (strcmp(argv[1], "-fib") == 0) {
    if (n < 0 || n > 40) {
      printf("n must be in range [0, 40]");
      return -1;
    }
    printf("fibb: %d\n", fib_number(n));
    return 0;
  }

  if (strcmp(argv[1], "-e") == 0) {
    if (n < 1 || n > 30) {
      printf("n must be in range [1, 30]");
      return -1;
    }
    printf("e: %.*f\n", 30, e_number(n));
    return 0;
  }

  if (strcmp(argv[1], "-pi") == 0) {
    if (n < 1 || n > 10) {
      printf("n must be in range [1, 10]");
      return -1;
    }
    printf("pi: %.*f\n", n-1, pi_number(n));
    return 0;
  }

  printf("%s", help());
  return -1;
}
