#include "compute-e.h"
#include "util.h"

double e_number(int n) {
  return Sum([](int k) { return 1.0/Factorial(k); }, n);
}
