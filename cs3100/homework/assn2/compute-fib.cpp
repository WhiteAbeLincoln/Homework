#include "compute-fib.h"

int fib_number(int n) {
  if (n <= 2) {
    return 1;
  }
  return fib_number(n-1) + fib_number(n-2);
}
