#ifndef ASSN2_UTIL_H_
#define ASSN2_UTIL_H_
#include <functional>
double Sum(std::function<double(int)> fun, int end, int start=0, int step=1);
int Factorial(int);
#endif  // ASSN2_UTIL_H_
