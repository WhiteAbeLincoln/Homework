# Mandelbrot


## Functions
double x double -> int
int -> color
int x int -> double x double

## Linear interpolation
t(b-a) + a

## timer function
generic
void f(void)

template <typename Fn>
auto timeFunc(Fn f)
    auto start = std::chrono::stead_clock::now();
    f();
    auto end = std::chrono::stead_clock::now();
    return end - start;
