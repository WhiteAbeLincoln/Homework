/************************
* A#: A02214512
* Course: CS1400
* Section: 7
* HW#: 8
***********************/

#include <cmath>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct Color
{
    int red;
    int green;
    int blue;
};

struct MandelbrotConfig
{
    int width;
    int height;
    double xComplexMin;
    double xComplexMax;
    double yComplexMin;
    double yComplexMax;
    int maxIterations;
    Color colorOne;
    Color colorTwo;
    string outputFileName;
};

MandelbrotConfig readConfig(string configFileLocation);
void drawMandelbrot(MandelbrotConfig config);
Color getPixelColor(Color color1, Color color2, int iterations, int maxIterations);

int main(int argc, char** argv)
{

    MandelbrotConfig configFile;
    string fileName;
    if (argc > 1) {
        fileName = argv[1];
    } else {
        // Read in config file location from user
        cout << "Mandelbrot Config File: " << endl;
        cin >> fileName;
    }
    // Read config file contents into MandelbrotConfig struct instance
    configFile = readConfig(fileName);

    // Compute and write specified mandelbrot image to PPM file
    drawMandelbrot(configFile);

    return 0;
}

MandelbrotConfig readConfig(string configFileLocation) {
    MandelbrotConfig configFile;
    ifstream fin(configFileLocation);
    fin >> configFile.width >> configFile.height >> configFile.xComplexMin >> configFile.xComplexMax
        >> configFile.yComplexMin >> configFile.yComplexMax >> configFile.maxIterations
        >> configFile.colorOne.red >> configFile.colorOne.green >> configFile.colorOne.blue
        >> configFile.colorTwo.red >> configFile.colorTwo.green >> configFile.colorTwo.blue >> configFile.outputFileName;
    return configFile;
}

void drawMandelbrot(MandelbrotConfig config) {
    double pixelWidth = (config.xComplexMax - config.xComplexMin)/config.width;
    double pixelHeight = (config.yComplexMax - config.yComplexMin)/config.height;

    ofstream outputFile(config.outputFileName);
    outputFile << "P3" << endl;
    outputFile << config.width << " " << config.height << endl;
    outputFile << 255 << endl;

    for (double Py = 0; Py < config.width; Py++) {
        for (double Px = 0; Px < config.height; Px++) {
            // Scale current pixel coordinates to lie inside the complex Mandelbrot X/Y scale (use pixelWidth/Height)
            double x0 = config.xComplexMin + Px * pixelWidth;
            double y0 = config.yComplexMin + Py * pixelHeight;

            double x = 0.0;
            double y = 0.0;
            int iteration = 0;
            while ((x * x) + (y * y) < (2 * 2) && iteration < config.maxIterations) {
                double xtemp = (x * x) - (y * y) + x0;
                y = 2 * x * y + y0;
                x = xtemp;
                iteration = iteration + 1;
            }

            Color color = getPixelColor(config.colorOne, config.colorTwo, iteration, config.maxIterations);

            outputFile << setw(4) << color.red << setw(4) << color.green << setw(4) << color.blue << setw(2) << " ";
        }
        outputFile << endl;
    }
    outputFile.close();
}

Color getPixelColor(Color color1, Color color2, int iterations, int maxIterations) {
    Color colorOfPixel;
    colorOfPixel.red = round(color1.red - (((color1.red - color2.red) / static_cast<float>(maxIterations)) * iterations));
    colorOfPixel.green = round(color1.green - (((color1.green - color2.green) / static_cast<float>(maxIterations)) * iterations));
    colorOfPixel.blue = round(color1.blue - (((color1.blue - color2.blue) / static_cast<float>(maxIterations)) * iterations));

    return colorOfPixel;
}