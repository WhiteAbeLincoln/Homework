// Abe White A02178955
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include "Mandelbrot.hpp"

// argument parsing code adapted from http://stackoverflow.com/questions/865668/how-to-parse-command-line-arguments-in-c#868894
char* getOption(char** begin, char** end, const std::string& option) {
    char** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end) {
        return *itr;
    }

    return 0;
}

bool optionExists(char** begin, char** end, const std::string& option) {
    return std::find(begin, end, option) != end;
}

// prints help
void help(char* name) {
    std::cout << "Mandelbrot" << std::endl;
    std::cout << "Usage:" << std::endl;
    std::cout << '\t' << name << " [-i <iterations>] [-c <config> | -o <outfile>]" << std::endl;
    std::cout << '\t' << name << " -h|--help" << std::endl;
    std::cout << '\t' << name << " --version" << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << '\t' << "-h --help"       << '\t' << "Show this screen"     << std::endl;
    std::cout << '\t' << "--version"       << '\t' << "Show version"         << std::endl;
    std::cout << '\t' << "-i <iterations>" << '\t' << "Number of iterations" << std::endl;
    std::cout << '\t' << "-c <config>"     << '\t' << "Config filename"      << std::endl;
    std::cout << '\t' << "-o <outfile>"    << '\t' << "Output filename"      << std::endl;
}

// reads a config file
void readConfig(char* configFile) {
    std::ifstream config(configFile);
    int width = 0;
    int height = 0;
    double xcompmin = 0.0;
    double xcompmax = 0.0;
    double ycompmin = 0.0;
    double ycompmax = 0.0;

    int sr,sg,sb;
    int er,eg,eb;

    config >> width >> height;
    config >> xcompmin >> xcompmax;
    config >> ycompmin >> ycompmax;
    config >> mandelbrotIterMax;
    config >> sr >> sg >> sb;
    config >> er >> eg >> eb;
    config >> outfile;

    startColor = {sr, sg, sb};
    endColor = {er, eg, eb};

    image = Plane<int>(0, width, 0, height);
    complexPlane = Plane<double>(xcompmin, xcompmax, ycompmin, ycompmax);
}

// runs the mandelbrot as well as timing and std devation, avg calculations
void run() {
    // run once for caching
    [](){mandelbrot(false);};

    int i = 0;
    std::vector<double> times;
    while (i < totalIterations) {
        times.push_back(
            timeFunc([](){
                mandelbrot(false);
            })
        );
        i++;
    }
    // compute average and standard deviation
    auto avg = compAverage(times);
    auto stdDev = compStdDev(times);

    std::cout << "Average: " << avg << std::endl;
    std::cout << "Standard Deviation: " << stdDev << std::endl;

    // print mandelbrot
    mandelbrot(true);
}

int main(int argc, char** argv) {
    if (optionExists(argv, argv+argc, "-h") || optionExists(argv, argv+argc, "--help")) {
        help(argv[0]);
        return 0;
    }

    char* iter = getOption(argv, argv+argc, "-i");
    char* config = getOption(argv, argv+argc, "-c");
    char* of = getOption(argv, argv+argc, "-o");

    if (config && of) {
        help(argv[0]);
        return 1;
    }

    if (config) {
        readConfig(config);
    }

    if (iter) {
        totalIterations = std::stoi(iter);
    }

    if (of) {
        outfile = of;
    }

    /* add forward slash to beginning to toggle printing debug config file information
    std::cout << image.width() << " " << image.height() << std::endl;
    std::cout << complexPlane.getXmin() << " " << complexPlane.getXmax() << std::endl;
    std::cout << complexPlane.getYmin() << " " << complexPlane.getYmax() << std::endl;
    std::cout << mandelbrotIterMax << std::endl;
    std::cout << startColor.r << " " << startColor.g << " " << startColor.b << std::endl;
    std::cout << endColor.r << " " << endColor.g << " " << endColor.b << std::endl;
    std::cout << outfile << std::endl;
    //*/

    run();

    return 0;
}
