#include <vector>
#include <random>
#include <deque>
#include <algorithm>
#include <utility>
#include <iostream>

bool is_page_fault(std::vector<bool> memory, int page_request) {
  return !memory[page_request - 1];
}

class memory {
private:
  unsigned long frame_size;
  std::vector<bool> in_memory;
  std::deque<int> fifo;
public:
  memory(unsigned long max_size) {
    frame_size = max_size;
    // initializes indexes 0 - 249 with false, since they aren't in memory
    for (int i = 0; i < 250; ++i) {
      in_memory.push_back(false);
    }
  }

  // only works with values from 1-250, due to in_memory vector
  bool add(int i) {
    bool fault = is_page_fault(in_memory, i);

    if (fault) {
      if (fifo.size() >= frame_size) {
        auto end = fifo.back();
        fifo.pop_back();
        in_memory[end - 1] = false;
      }
      fifo.push_front(i);
      in_memory[i - 1] = true;
    }

    return fault;
  }

  int add_sequence(std::vector<int> sequence) {
    int num_faults = 0;
    for (auto page : sequence) {
      bool faulted = add(page);
      if (faulted) {
        num_faults++;
      }
    }
    return num_faults;
  }

  void clear() {
    fifo.clear();
  }

};

int getRandom() {
  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<int> uni(1,250);
  return uni(rng);
}


std::vector<std::vector<int>> make_page_sequences() {
  std::vector<std::vector<int>> vec;
  for (int i = 0; i < 100; ++i) {
    std::vector<int> v2;
    for (int j = 0; j < 1000; ++j) {
      v2.push_back(getRandom());
    }
    vec.push_back(v2);
  }

  return vec;
}



int main() {
  // results is a vector of size 100
  std::vector<std::vector<int>> results;
  auto sequences = make_page_sequences();

  for (auto sequence : sequences) {
    std::vector<int> res;
    for (int i = 1; i <= 100; ++i) {
      // creates a new memory with page size of i
      memory m(i);
      res.push_back(m.add_sequence(sequence));
    }
    results.push_back(res);
  }

  int num_anomaly = 0;

  std::cout << "Length of memory reference string: 1000\n";
  std::cout << "Frames of physical memory: 100\n";

  for (size_t sequence = 0; sequence < results.size(); ++sequence) {
    for (size_t i = 1; i < results.size(); i++) {
      if (results[sequence][i] > results[sequence][i-1]) {
        std::cout << "Anomaly Discovered!" << std::endl;
        num_anomaly++;
        std::cout << "\tSequence: " << sequence << std::endl;
        std::cout << "\tPage Faults: " << results[sequence][i-1] << " @ Frame Size: " << i-1 << std::endl;
        std::cout << "\tPage Faults: " << results[sequence][i] << " @ Frame Size: " << i << std::endl;
      }
    }
  }

  std::cout << "Anomaly Detected " << num_anomaly << " times\n";
}
