//
// Created by abe on 2/12/17.
//

#include <random>
#include <algorithm>
#include "ThreadPool.h"


void quickSort(std::vector<int>& a, int l, int r, ThreadPool* tp) {
    if (a.size() > 1) {
        int i=l, j=r;
        int pivot = (a)[(l+r)/2];

        while (i <= j) {
            while((a)[i] < pivot) i++;
            while((a)[j] > pivot) j--;
            if (i <= j) {
                std::swap((a)[i], (a)[j]);
                i++; j--;
            }
        }

        //if (l < j) (*tp).enqueue(quickSort, a, l, j, tp);
        //if (i < r) (*tp).enqueue(quickSort, a, i, r, tp);

        if (l < j) (*tp).enqueue([&a,l,j,tp]{ quickSort(a, l, j, tp); });
        if (i < r) (*tp).enqueue([&a,i,r,tp]{ quickSort(a, i, r, tp); });
        //if (l < j) quickSort(a, l, j, tp);
        //if (i < r) quickSort(a, i, r, tp);
    }
}

void Quicksort(std::vector<int> a, ThreadPool* tp) {
    quickSort(a, 0, a.size()-1, tp);
}

void printArray(std::vector<int> a) {
    for (int& i : a) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

std::vector<int> buildArray(int size) {
    int i=0;

    // build a vector of random numbers
    std::vector<int> data;
    data.reserve(size);
    std::generate_n(std::back_inserter(data), data.capacity(), [&](){ return ++i; });
    std::random_shuffle(data.begin(), data.end());

    return data;
}

void work_proc(std::vector<int> data, ThreadPool* tp)
{

   /*
    if (data == nullptr) {
        std::vector<int> v = buildArray(10000);
        data = &v;
    }
    */

    Quicksort(data, tp);
}

int main()
{
    ThreadPool tp;

    // works
    std::vector<int> data = buildArray(10000);
    printArray(data);
    Quicksort(data, &tp);
    //*
    std::vector<int> data2 = buildArray(10000);
    std::vector<int> data3 = buildArray(10000);
    std::vector<int> data4 = buildArray(10000);
    std::vector<int> data5 = buildArray(10000);
    std::vector<int> data6 = buildArray(10000);

    printArray(data6);

    tp.enqueue(work_proc, data2, &tp);
    tp.enqueue(work_proc, data3, &tp);
    tp.enqueue(work_proc, data4, &tp);
    tp.enqueue(work_proc, data5, &tp);
    tp.enqueue(work_proc, data6, &tp);
    /*/

    // doesn't work (d goes out of scope and gets destroyed before quickSort is finished with it)
    for (int i = 0; i < 5; i++) {
        std::vector<int> d = buildArray(10000);
        tp.enqueue(work_proc, &d, &tp);
        // printArray(d);
    }
    //*/

    tp.waitFinished();
    std::cout << tp.getProcessed() << '\n';

    printArray(data);
    /*
    // run five batches of 100 items
    for (int x=0; x<5; ++x)
    {
        // queue 100 work tasks
        for (int i=0; i<100; ++i) {
            std::vector<int> t = buildArray(10000);
            tp.enqueue(work_proc, &t, &tp);
        }

        tp.waitFinished();
        std::cout << tp.getProcessed() << '\n';
    }
     */

    // destructor will close down thread pool
    return EXIT_SUCCESS;
}