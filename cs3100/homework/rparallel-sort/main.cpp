#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include "ThreadPool.h"

// generic function to time arbitrary function
template <typename Fn>
double timeFunc(Fn f) {
    auto start = std::chrono::steady_clock::now();
    f();
    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> diff = end - start;
    return diff.count();
}

void quickSort(std::vector<int>* a, int l, int r, ThreadPool* tp) {
    if (a->size() > 1) {
        int i=l, j=r;
        int pivot = (*a)[(l+r)/2];

        while (i <= j) {
            while((*a)[i] < pivot) i++;
            while((*a)[j] > pivot) j--;
            if (i <= j) {
                std::swap((*a)[i], (*a)[j]);
                i++; j--;
            }
        }

        if (l < j) (*tp).enqueue(quickSort, a, l, j, tp);
        if (i < r) (*tp).enqueue(quickSort, a, i, r, tp);
        //if (l < j) quickSort(a, l, j, tp);
        //if (i < r) quickSort(a, i, r, tp);
    }
}

void Quicksort(std::vector<int>* a, ThreadPool* tp) {
    quickSort(a, 0, a->size()-1, tp);
}

int linearSearch(std::vector<int>* a, int l, int r, int value) {
    for (int i = l; i < r; i++) {
        if ((*a)[i] == value) {
            return i;
        }
    }

    return -1;
}

int LinearSearch(std::vector<int>* a, int value, ThreadPool* tp) {
    std::vector<std::future<int>> results;
    for (unsigned long i = 0; i < tp->availableThreads(); i++) {
        results.push_back((*tp).enqueue(linearSearch, a, a->size()*i, a->size()*(i+1), value));
    }

    for(auto&& result: results) {
        auto i = result.get();
        if (i != -1) return i;
    }

    return -1;
}

std::vector<int> createArray(int size) {
    std::vector<int> a;

    for(int i = 0; i < size; i++)
        a.push_back(i);
    std::random_shuffle(a.begin(),a.end());

    return a;
}

void printVector(const std::vector<int>& a) {
   for (const int& i : a) {
       std::cout << i << " ";
   }

   std::cout << std::endl;
}

template <typename Fn, typename... Args>
std::vector<double> testFunction(int totalIterations, std::vector<int> arr, Fn f, Args... args) {
    std::vector<double> times;
    for (int i = 0; i < totalIterations; i++) {
        std::vector<int> test(arr);
        times.push_back(
                timeFunc(f(test, args...))
        );
    }

    return times;
}

std::function<void()> testStdSort(std::vector<int> a) {
    return [&] { std::sort(a.begin(), a.end()); /*printVector(a);*/ };
}

std::function<void()> testStdFind(std::vector<int> a, int findValue) {
    return [&] {
        std::vector<int>::iterator it = std::find(a.begin(), a.end(), findValue);
        if (it != a.end())
            std::cout << *it << " found in a at: " << it-a.begin() << std::endl;
    };
}

std::function<void()> testQuickSort(std::vector<int> a, ThreadPool* p) {
    return [&] { Quicksort(&a, p); printVector(a); };
}

std::function<void()> testLinearFind(std::vector<int> a, int findValue, ThreadPool* p) {
    return [&] {
        int it = LinearSearch(&a, findValue, p);
        if (it != -1)
            std::cout << "Linear " << a[it] << " found in a at: " << it << std::endl;
    };
}

double compAverage(std::vector<double> arr) {
    double total = 0;

    for(double &elem : arr) {
        total += elem;
    };

    return total / arr.size();
}

// compute standard deviation of array of doubles
double compStdDev(std::vector<double> arr) {
    double sum = 0;
    double avg = compAverage(arr);

    for(double &elem : arr) {
        sum += ((elem - avg)*(elem - avg));
    };

    double stdDev = std::sqrt((1/(float)arr.size()) * sum);

    return stdDev;
}

int main() {
    int totalIterations = 10;
    std::vector<int> v100 = createArray(100);
    std::vector<int> vMill = createArray(1000000);

    ThreadPool pool(8);
    int findValue = 5;

    std::vector<double> v100StdSortTimes;
    std::vector<double> v100StdFindTimes;
    std::vector<double> v100QuickSortTimes;
    std::vector<double> v100LinearSearchTimes;

    v100StdSortTimes = testFunction(totalIterations, v100, testStdSort);
    v100StdFindTimes = testFunction(totalIterations, v100, testStdFind, findValue);

    v100QuickSortTimes = testFunction(1, v100, testQuickSort, &pool);
    pool.waitFinished();
    std::cout << pool.getProcessed() << std::endl;
    // v100LinearSearchTimes = testFunction(totalIterations, v100, testLinearFind, findValue, &pool);

    std::cout << "std::sort average: " << compAverage(v100StdFindTimes) << std::endl;


    return 0;
}