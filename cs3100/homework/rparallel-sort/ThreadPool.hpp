//
// Created by abe on 2/4/17.
//

#ifndef MANDELBROT_THREADPOOL_THREADPOOL_H
#define MANDELBROT_THREADPOOL_THREADPOOL_H

#include <vector>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <future>

// threadpool implementation adapted from https://github.com/progschj/ThreadPool/blob/master/ThreadPool.h 

class ThreadPool {
public:
    ThreadPool();
    ThreadPool(int);

    template<typename F, typename... Args>
    auto enqueue(F&& f, Args&&... args)
        -> std::future<typename std::result_of<F(Args...)>::type>;
    unsigned long availableThreads();
    void waitFinished();
    void join();
    unsigned int getProcessed() const { return processed; }
    std::queue<std::function<void()>> getTasks();
    ~ThreadPool();

private:
    std::vector<std::thread> workers;
    std::queue<std::function<void()>> tasks;

    std::mutex queue_mtx;
    std::condition_variable cv_task;
    std::condition_variable cv_finished;
    std::atomic_uint processed;
    unsigned int busy;


    bool stop;
    bool finishBeforeExit = true;
};

unsigned long ThreadPool::availableThreads() {
    return this->workers.size();
}

std::queue<std::function<void()>> ThreadPool::getTasks() {
    return this->tasks;
}

ThreadPool::ThreadPool(int threads) : stop(false) {
    for (int i = 0; i < threads; i++) {
        workers.emplace_back(
                // this is our consumer thread
                // loops while there are tasks to complete
                [this] {
                    for (;;) {
                        // create blank task
                        std::function<void()> task;
                        // lock thread, since we are going to be modifying the queue
                        std::unique_lock<std::mutex> lck(this->queue_mtx);
                        // block until predicate is true
                        this->cv_task.wait(lck, [this]() {return this->stop || !this->tasks.empty();});

                        if (this->stop && this->tasks.empty()) return;
                        // use std::move, since we are going to delete the value with tasks.pop() anyway
                        // std::move is more efficient than calling the copy constructor of the task

                        if (!this->tasks.empty()) {
                            ++busy;

                            task = std::move(this->tasks.front());
                            this->tasks.pop();

                            lck.unlock();

                            task();
                            ++processed;

                            lck.lock();
                            --busy;
                            cv_finished.notify_one();

                        } else if (this->stop) break;

                    }
                }
        );
    }
}

ThreadPool::ThreadPool() : ThreadPool(1) {}

void ThreadPool::join() {
    {
        std::unique_lock<std::mutex> lck(queue_mtx);
        stop = true;
    }
    cv_task.notify_all();

    cv_task.notify_all();
    for (std::thread& w : workers) {
        if (w.joinable()) {
            std::cout << "joining thread " << w.get_id() << std::endl;
            w.join();
        }
    }
}

void ThreadPool::waitFinished() {
    std::unique_lock<std::mutex> lock(queue_mtx);
    cv_finished.wait(lock, [this]() { return tasks.empty() && busy == 0; });
}

template<typename F, typename... Args>
auto ThreadPool::enqueue(F&& f, Args&&... args)
-> std::future<typename std::result_of<F(Args...)>::type> {

    using return_type = typename std::result_of<F(Args...)>::type;
    // use a shared pointer to better handle destruction of threads and tasks
    auto task = std::make_shared<std::packaged_task<return_type()>>(
            // use std::forward to handle cases where bare rvalues are passed
            // (for instance, passing a lambda or calling enqueue with raw values
            // e.g. enqueue([i]{ cout<<"in thread" << i << endl; }, 1)
            // both the lambda and 1 are rvalues, and so won't handle references correctly
            // without the std::forward call
            std::bind(std::forward<F>(f), std::forward<Args>(args)...)
    );


    // we return a promise to the return value from the task
    // the value can be retrieved with enqueue(blah).get()
    std::future<return_type> res = task->get_future();

    {
        std::unique_lock<std::mutex> lck(queue_mtx);

        if (stop)
            throw std::runtime_error("Attempted to enqueue task on stopped ThreadPool");

        tasks.emplace([task](){ (*task)(); });
    }

    cv_task.notify_one();

    return res;
}

// our destructor joins threads, so we don't have to manually call join() on the pool
ThreadPool::~ThreadPool() {
    if (finishBeforeExit) {
        std::unique_lock<std::mutex> lk(queue_mtx);
        cv_finished.wait(lk, [this](){
            std::cout << "remaining tasks: " << tasks.size() << std::endl;
            return tasks.empty() && busy == 0;
        });
    }

    {
        std::unique_lock<std::mutex> lck(queue_mtx);
        stop = true;
        cv_task.notify_all();
        lck.unlock();
    }

    // joins threads to main once destroyed
    for (std::thread& w : workers) {
        if (w.joinable())
            w.join();
    }
}

#endif //MANDELBROT_THREADPOOL_THREADPOOL_H
