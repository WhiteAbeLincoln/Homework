#!/usr/bin/env bash

for i in {1..18..2}; do
    ./mandelbrot -i 10 -t $i -e >> report-odd.csv
done

for i in {2..18..2}; do 
    ./mandelbrot -i 10 -t $i -e >> report-even.csv
done
