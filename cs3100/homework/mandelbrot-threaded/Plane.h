// Abe White A02178955
#ifndef MANDELBROT_PLANE_H
#define MANDELBROT_PLANE_H

template <typename T>
class Plane {
    private:
        T xmin;
        T xmax;
        T ymin;
        T ymax;
    public:
        Plane(T xmin, T xmax, T ymin, T ymax);
        T width();
        T height();
        T area();
        T getXmin();
        T getXmax();
        T getYmin();
        T getYmax();
};

#endif //MANDELBROT_PLANE_H
