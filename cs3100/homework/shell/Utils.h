//
// Created by abe on 2/25/17.
//

#ifndef SHELL_UTILS_H
#define SHELL_UTILS_H

namespace utils {
    std::string ltrim(const std::string& s);
    std::string rtrim(const std::string &s);
    std::string trim(const std::string &s);
    std::vector<std::string> keepsplit(std::string s, const std::string &delim);
    std::vector<std::string> split(std::string s, char c=' ', bool removeEmpty=true);
    std::vector<std::string> tokenize(std::string s, char delim=' ', char quote='"');
    void strvec_to_char(const std::vector<std::string> &strvec, int &argc, char **args);
    std::string int_to_nth(const int &i);

    // implemented in header because of the template. Compiler doesn't know what the template parameter type is unless
    // the function is defined in the same file as it's used
    template<typename A>
    std::string join(const A &begin, const A &end, const std::string &t) {
        std::string result;
        bool added = false;
        for (A it=begin; it!=end; it++) {
            result.append(*it);
            added = true;
            if (added && it+1 != end)
                result.append(t);
        }
        return result;
    };
};

#endif //SHELL_UTILS_H
