//
// Created by abe on 2/24/17.
//

#ifndef SHELL_COMMAND_H
#define SHELL_COMMAND_H

#include <string>
#include <vector>
#include "History.h"

enum redirect_type {
    INPUT,
    OUTPUT,
    APPEND,
    NONE
};

class Command {
private:
    std::vector<std::string> args;
    std::string file = "";
    redirect_type redirect = redirect_type::NONE;
    bool background = false;
    const int CHILD_PID = 0;
    std::chrono::duration<double>& elapsed;
    History& history;
    bool run_builtins();
    void run_command(char**, int, int);
    void redirect_command(char**, int, int);
    bool loop = true;
public:
    const std::vector<std::string>& getArgs() const;
    void setArgs(const std::vector<std::string>&);
    const std::string& getFile() const;
    redirect_type getRedirect() const;
    void setRedirect(redirect_type, const std::string&);
    bool isBackground() const;
    void setBackground(bool);
    bool execute(int, int);
    Command(std::string, std::chrono::duration<double, std::ratio<1, 1>>&, History&);
    Command(std::vector<std::string>, std::chrono::duration<double, std::ratio<1, 1>>&, History&);
};


#endif //SHELL_COMMAND_H
