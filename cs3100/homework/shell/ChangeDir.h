//
// Created by abe on 2/16/17.
//

#ifndef SHELL_CHANGEDIR_H
#define SHELL_CHANGEDIR_H

#include <stdlib.h>
#include <zconf.h>
#include <regex>
#include <iostream>
#include <sys/stat.h>
#include "Utils.h"
#include "DirUtils.h"

using namespace utils;

std::string recurse_stat(const std::vector<std::string>& parts, const std::string& dir, size_t i, std::string append) {
    if (i == parts.size())
        return dir;

    std::string test = append + parts[i];
    if (parts[i][parts[i].size()-1] != '/')
        test = test + '/';
    test = test+dir;

    struct stat statbuf;

    if (stat(test.c_str(), &statbuf) != -1) {
        if (S_ISDIR(statbuf.st_mode)) {
            return test;
        } else if (append.empty()) {
            return recurse_stat(parts, dir, i, "./");
        } else {
            return recurse_stat(parts, dir, i+1, "");
        }
    } else return recurse_stat(parts, dir, i+1, "");
}

std::string check_cdpath(const std::string& directory) {
    char* cdpath = getenv("CDPATH");
    if (cdpath == NULL) {
        return directory;
    } else {
        std::vector<std::string> parts = split(cdpath, ':');

        return recurse_stat(parts, directory, 0, "");
    }
}

// implementation taken from description given in "man 1p cd"
void change_dir(std::vector<std::string> args) {
    DirUtils dirUtils;

    bool p = false;
    // bool l = false; l is assumed to be !p
    std::string curpath = "";
    std::string oldcurpath = "";

    std::vector<std::string>::iterator p_iter = std::find(args.begin(), args.end(), "-P");
    std::vector<std::string>::iterator l_iter = std::find(args.begin(), args.end(), "-L");

    if (p_iter != args.end()) {
        p = true;
        args.erase(p_iter, p_iter + 1);
    }

    if (l_iter != args.end()) {
        p = false;
        args.erase(l_iter, l_iter + 1);
    }

    // this is when I like javascript
    // std::string directory = args[1] || "";

    std::string directory = "";
    if (args.size() > 1)
        directory = args[1];

    if (args.size() == 1) { // no other arguments, use HOME
        char *home = getenv("HOME");
        if (home == NULL) {
            std::cout << "crash: cannot change directory; HOME isn't defined";
            return;
        }

        directory = home;

    } else if (directory[0] == '~') {
        char *home = getenv("HOME");
        if (home == NULL) {
            std::cout << "crash: cannot change directory; HOME isn't defined";
            return;
        }

        // erase the ~ character
        directory.erase(0, 1);

        directory = home + directory;
    }


    if (directory[0] == '/') {
        curpath = directory;
    } else {
        std::vector <std::string> comps = split(directory, '/', false);

        if (comps[0] == "." || comps[0] == "..") {
            curpath = directory;
        } else {
            curpath = check_cdpath(directory);
        }
    }

    // if -P is specified, continue to 10
    if (!p) {
        if (curpath[0] != '/') {
            char *pwd = getenv("PWD");
            if (pwd == NULL) {
                std::cout << "crash: cannot change directory; PWD isn't defined";
                return;
            }

            if (pwd[strlen(pwd) - 1] != '/')
                curpath = strcat(pwd, "/") + curpath;
            else
                curpath = pwd + curpath;
        }

        curpath = dirUtils.make_canonical(curpath);
        oldcurpath = curpath;

        // if curpath is longer than {PATH_MAX} bytes
        if (curpath.size() > PATH_MAX) {
            // then convert curpath from absolute to relative pathname if possible

            char *pwd = getenv("PWD");
            if (pwd == NULL) {
                std::cout << "crash: cannot change directory; PWD isn't defined";
                return;
            }

            std::regex r(pwd);

            if (pwd[strlen(pwd) - 1] != '/')
                curpath = std::regex_replace(curpath, r, ".");
            else
                curpath = std::regex_replace(curpath, r, "./");
        }
    }

    errno = 0;
    if (chdir(curpath.c_str()) != 0) {
        std::printf("crash: cd: %s: %s\n", join(args.begin()+1, args.end(), " ").c_str(), strerror(errno));
    }

    // if the -P option is not in effect, the PWD env var shall be set to the value that curpath
    // had before conversion to a relative pathname
    /*if (!p)*/ {
        char *pwd = getenv("PWD");
        setenv("OLDPWD", pwd, 1);
        setenv("PWD", oldcurpath.c_str(), 1);

        // if the -P option is in effect, the PWD env var shall be set to the string that would be output by pwd -P
        // } else {
        // how do we know what pwd -P would output?
        // -P option means physical, avoid all symlinks
        // see https://ubuntuforums.org/showthread.php?t=1126617
    }
}
#endif //SHELL_CHANGEDIR_H
