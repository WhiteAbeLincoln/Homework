//
// Created by abe on 2/24/17.
//

#include <chrono>
#include <zconf.h>
#include <cstring>
#include <wait.h>
#include <iostream>
#include <fcntl.h>
#include "Command.h"
#include "Utils.h"
#include "ChangeDir.h"

Command::Command(std::vector<std::string> args, std::chrono::duration<double, std::ratio<1, 1>> &elapsed,
                 History &history)
        : args(args), elapsed(elapsed), history(history) {}

Command::Command(std::string s, std::chrono::duration<double, std::ratio<1, 1>> &elapsed, History &history)
        : elapsed(elapsed), history(history) {
    Command::args = utils::tokenize(s);
}

const std::vector<std::string>& Command::getArgs() const {
    return args;
}

void Command::setArgs(const std::vector<std::string>& args) {
    Command::args = args;
}

const std::string& Command::getFile() const {
    return file;
}

redirect_type Command::getRedirect() const {
    return redirect;
}

void Command::setRedirect(redirect_type ftype, const std::string& f) {
    Command::redirect = ftype;
    Command::file = f;
}

bool Command::isBackground() const {
    return background;
}

void Command::setBackground(bool background) {
    Command::background = background;
}

void Command::run_command(char** args, int in, int out) {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    pid_t pid = fork();

    if (pid < CHILD_PID) { // error, couldn't create
        std::cerr << "fork() failed\n";
    } else if (pid == CHILD_PID) { // child
        if (in != STDIN_FILENO) {
            dup2(in, STDIN_FILENO);
            close(in);
        }

        if (out != STDOUT_FILENO) {
            dup2(out, STDOUT_FILENO);
            close(out);
        }

        if (execvp(args[0], args) < 0) { // execvp failed (file not found)
            std::printf("crash: %s: %s\n", args[0], std::strerror(errno));
            exit(errno);
        }
    } else {// Parent
        start = std::chrono::system_clock::now();
        waitpid(pid, NULL, 0);
        end = std::chrono::system_clock::now();
        elapsed = elapsed + (end-start);
    }
}

// lots of duplication between this and run_command.
// perhaps I can just open the file and pass the file descriptor to a call of run_command?
void Command::redirect_command(char** args, int in, int out) {
    // std::printf("calling redirect for %s\n", file.c_str());
    std::chrono::time_point<std::chrono::system_clock> start, end;
    int fd;
    pid_t pid = fork();

    if (pid < CHILD_PID) {
        std::cerr << "fork() failed\n";
    } else if (pid == CHILD_PID) {
        if (redirect == redirect_type::OUTPUT || redirect == redirect_type::APPEND) {
            if (redirect == redirect_type::OUTPUT)
                fd = open(file.c_str(), O_RDWR|O_CREAT, 0644);
            else
                fd = open(file.c_str(), O_RDWR|O_CREAT|O_APPEND, 0644);


            if (fd < 0) {
                printf("crash: %s\n", strerror(errno));
                exit(errno);
            }

            // std::printf("redirect output to %s: %d\n", file.c_str(), fd);

            if (in != STDIN_FILENO) {
                dup2(in, STDIN_FILENO);
                close(in);
            }

            dup2(fd, STDOUT_FILENO);
            close(fd);
        }

        if (redirect == redirect_type::INPUT) {
            fd = open(file.c_str(), O_RDONLY, 0644);

            if (fd < 0) {
                printf("crash: %s\n", strerror(errno));
                exit(errno);
            }

            // std::printf("redirect input to %s: %d\n", file.c_str(), fd);

            if (out != STDOUT_FILENO) {
                dup2(out, STDOUT_FILENO);
                close(out);
            }

            dup2(fd, STDIN_FILENO);
            close(fd);
        }

        if (execvp(args[0], args) < 0) { // execvp failed (file not found)
            std::printf("crash: %s: %s\n", args[0], std::strerror(errno));
            exit(errno);
        }
    } else {
        start = std::chrono::system_clock::now();
        waitpid(pid, NULL, 0);
        end = std::chrono::system_clock::now();
        elapsed = elapsed + (end-start);
    }
}

bool Command::execute(int in, int out) {
    if (!run_builtins()) {
        char **argv = new char *[args.size()];
        int argc = 0;
        utils::strvec_to_char(args, argc, argv);

        if (redirect != redirect_type::NONE)
            redirect_command(argv, in, out);
        else
            run_command(argv, in, out);
    }

    return loop;
}

bool Command::run_builtins() {
    bool cont = false;

    // make sure commands modifying history and args go first

    if (args[0] == "!!") {
        // convert !! to ^ command
        args[0] = "^";
        if (args.size() > 1)
            args[1] = std::to_string(history.size()-1);
        else
            args.push_back(std::to_string(history.size()-1));
    }

    if (args[0] == "^" || args[0][0] == '^') {
        // if the user didn't include a space between ^ and n
        // convert to valid args array
        if (args[0].size() > 1) {
            if (args.size() > 1)
                args[1] = args[0].substr(1);
            else
                args.push_back(args[0].substr(1));
            args[0] = "^";
        }

        int idx = std::stoi(args[1]);

        if (idx-1 < history.size() && idx-1 > 0) {
            std::printf("%s\n", history[idx-1].c_str());
            args = utils::split(history[idx - 1]);
            cont = false;
            history[history.size()-1] = history[idx-1];
        } else {
            std::printf("crash: Cannot execute the %s history element\n", utils::int_to_nth(idx).c_str());
            history.remove(history.size()-1);
            cont = true;
        }
    }

    if (args[0] == "exit") {
        loop = false;
        cont = true;
    }

    if (args[0] == "cd") {
        change_dir(args);
        cont = true;
    }

    if (args[0] == "history") {
        if (args.size() > 1) {
            int num = 0;
            try {
                num = std::stoi(args[1]);
                history.print(num);
            } catch (std::invalid_argument e) {
                printf("crash: history: %s: numeric argument required\n", args[1].c_str());
            }
        } else history.print();

        cont = true;
    }

    if (args[0] == "ptime") {
        std::printf("Time spent executing child processes: "
                            "%.0f seconds "
                            "%.0f milliseconds and "
                            "%.0f microseconds\n",
                    elapsed.count(), (elapsed.count()*1000), (elapsed.count()*1000000));
        cont = true;
    }

    return cont;
}
