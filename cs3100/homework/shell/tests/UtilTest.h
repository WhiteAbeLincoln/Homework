//
// Created by abe on 2/16/17.
//

#ifndef SHELL_UTILTEST_H
#define SHELL_UTILTEST_H

#include <gtest/gtest.h>
#include "../Utils.cpp"

namespace {
    using namespace utils;

    TEST(UtilTest, ltrimTest) {
        const std::string test1 = " this/package/testdata/myinputfile.dat";
        const std::string test2 = "  this/package/testdata/myinputfile.dat";
        const char* expected = "this/package/testdata/myinputfile.dat";

        ASSERT_STREQ(ltrim(test1).c_str(), expected) << "ltrim didn't remove single leading whitespace";
        ASSERT_STREQ(ltrim(test2).c_str(), expected) << "ltrim didn't remove multiple leading whitespace";
    }

// Tests that Foo does Xyz.
    TEST(UtilTest, rtrimTest) {
        const std::string test1 = "this/package/testdata/myinputfile.dat ";
        const std::string test2 = "this/package/testdata/myinputfile.dat  ";
        const char* expected = "this/package/testdata/myinputfile.dat";

        ASSERT_STREQ(rtrim(test1).c_str(), expected) << "rtrim didn't remove trailing whitespace";
        ASSERT_STREQ(rtrim(test2).c_str(), expected) << "rtrim didn't remove multiple trailing whitespace";
    }

    TEST(UtilTest, trimTest) {
        const std::string test1 = " this/package/testdata/myinputfile.dat ";
        const std::string test2 = "  this/package/testdata/myinputfile.dat  ";
        const char* expected = "this/package/testdata/myinputfile.dat";

        ASSERT_STREQ(trim(test1).c_str(), expected) << "trim didn't remove surrounding whitespace";
        ASSERT_STREQ(trim(test2).c_str(), expected) << "trim didn't remove multiple surrounding whitespace";
    }

    TEST(UtilTest, splitTest) {
        const std::string test1 = "a,b,c,d";
        const std::string test2 = "a,b,,c,d";
        const std::string test3 = "/";

        ASSERT_EQ(split(test1, ',').size(), 4) << "split didn't return the expected number of elements";
        ASSERT_EQ(split(test2, ',', false).size(), 5) << "split didn't preserve empty strings";
        ASSERT_EQ(split(test2, ',', true).size(), 4) << "split mistakenly preserved empty strings";
        ASSERT_EQ(split(test3, '/', false).size(), 1) << "split didn't preserve empty strings";
    }

    TEST(UtilTest, keepsplitTest) {
        const std::string test1 = "ab&cd";
        const char* expected = "ab&";
        const char* expected1 = "cd";

        std::vector<std::string> res = keepsplit(test1, "&");
        ASSERT_EQ(res.size(), 2) << "keepsplit didn't return the expected number of elements";
        ASSERT_STREQ(res[0].c_str(), expected) << "keepsplit didn't keep the delimiter token";
        ASSERT_STREQ(res[1].c_str(), expected1) << "keepsplit didn't return the correct strings";
    }

    TEST(UtilTest, tokenizeTest) {
        const std::string test = "this is a 'test string' with 'examples'";
        const std::string test1 = "'example string' is an example";
        const std::string test2 = "'example string'";
        const std::string test3 = "this does not have any quotes";

        std::vector<std::string> res  = tokenize(test,  ' ', '\'');
        std::vector<std::string> res1 = tokenize(test1, ' ', '\'');
        std::vector<std::string> res2 = tokenize(test2, ' ', '\'');
        std::vector<std::string> res3 = tokenize(test3, ' ', '\'');

        std::vector<std::string> ex = {"this", "is", "a", "test string", "with", "examples"};
        std::vector<std::string> ex1 = {"example string", "is", "an", "example"};
        std::vector<std::string> ex2 = {"example string"};
        std::vector<std::string> ex3 = {"this", "does", "not", "have", "any", "quotes"};

        ASSERT_EQ(res.size(), 6);
        ASSERT_EQ(res, ex);
        ASSERT_EQ(res1, ex1);
        ASSERT_EQ(res2, ex2);
        ASSERT_EQ(res3, ex3);
    }

    TEST(UtilTest, joinTest) {
        std::vector<std::string> v = {"a","b","c","d"};
        std::vector<std::string> v2 = {"a","b","","d"};
        std::vector<std::string> v3 = {"","a","b","c","d"};
        std::vector<std::string> v4 = {""};

        ASSERT_STREQ(join(v.begin(),v.end(), ",").c_str(), "a,b,c,d") << "join didn't combine elements correctly";
        ASSERT_STREQ(join(v.begin()+1,v.end()-1, " ").c_str(), "b c") << "join didn't handle relative iterators correctly";
        ASSERT_STREQ(join(v.rbegin(),v.rend(), ",").c_str(), "d,c,b,a") << "join didn't handle revers iterators correctly";
        ASSERT_STREQ(join(v2.begin(),v2.end(), "/").c_str(), "a/b//d") << "join didn't preserve empty strings";
        ASSERT_STREQ(join(v3.begin(),v3.end(), "/").c_str(), "/a/b/c/d") << "join didn't preserve beginning empty strings";
        ASSERT_STREQ(join(v4.begin(),v4.end(), "/").c_str(), "") << "join didn't handle empty string";
    }

    TEST(UtilTest, strvecToCharTest) {
        std::vector<std::string> v = {"aa","bb","cc","dd"};
        char** arg = new char*[v.size()];
        int argc;

        strvec_to_char(v, argc, arg);

        ASSERT_EQ(argc, v.size()) << "size differed after vector to char** conversion";
        ASSERT_STREQ(arg[0], v[0].c_str()) << "first elements differed after vector to char** conversion";
    }

}  // namespace

#endif //SHELL_UTILTEST_H
