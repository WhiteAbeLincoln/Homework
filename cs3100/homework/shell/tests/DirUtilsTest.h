//
// Created by abe on 2/16/17.
//

#ifndef SHELL_CHANGEDIRTEST_H
#define SHELL_CHANGEDIRTEST_H

#include <gtest/gtest.h>
#include <string>
#include "../DirUtils.h"

namespace {
    class MockDirUtils : public DirUtils {
    public:
        virtual bool is_dir(const std::string&, int&) { return true; };
    };

    class TestableDirUtils : public  MockDirUtils {
    public:
        using MockDirUtils::remove_dot_components;
        using MockDirUtils::remove_dotdot_components;
        using MockDirUtils::remove_slashes;
    };

    TEST(DirUtils, makeCanonicalTest) {
        std::string path = "/home/abe/documents/../downloads/./files";
        std::string path2 = "/home/abe/documents/../../../usr/./bin";
        const char* expected = "/home/abe/downloads/files";
        const char* expected2 = "/usr/bin";

        MockDirUtils dirUtils;

        ASSERT_STREQ(dirUtils.make_canonical(path2).c_str(), expected2);
        ASSERT_STREQ(dirUtils.make_canonical(path).c_str(), expected);
    }

    TEST(DirUtils, dirExistsTest) {
        // we can be reasonably sure that /usr/bin/ exists on most distros
        std::string path = "/usr/bin";
        std::string notpath = "/foo/bar";
        int err;
        int err2;
        DirUtils dirUtils;

        ASSERT_TRUE(dirUtils.is_dir(path, err)) << "is_dir incorrectly said path doesn't exist " << err;
        ASSERT_FALSE(dirUtils.is_dir(notpath, err2));
        ASSERT_EQ(ENOENT, err2) << "is_dir didn't return the correct error code. expected ENOENT, got: " << err;
    }

    TEST(DirUtils, removeDotTest) {
        std::string path = "/home/abe/documents/./files";
        std::string path2 = "/./home/abe/documents/./files";
        std::vector<std::string> v = split(path, '/', false);
        std::vector<std::string> v2 = split(path2, '/', false);

        std::vector<std::string> ex = split("/home/abe/documents/files", '/', false);
        TestableDirUtils dirUtils;

        ASSERT_EQ(dirUtils.remove_dot_components(v), ex);
        ASSERT_EQ(dirUtils.remove_dot_components(v2), ex);
    }

    TEST(DirUtils, removeDotDotTest) {
        std::string path = "/home/abe/documents/../files";
        std::string path2 = "/home/abe/documents/foo/bar/../../../files";
        std::vector<std::string> v = split(path, '/', false);
        std::vector<std::string> v2 = split(path2, '/', false);

        const char* expected = "/home/abe/files";

        std::vector<std::string> ex = split(expected, '/', false);

        TestableDirUtils dirUtils;

        ASSERT_EQ(dirUtils.remove_dotdot_components(v), ex);
        ASSERT_EQ(dirUtils.remove_dotdot_components(v2), ex);
    }

    TEST(DirUtils, removeSlashTest) {
        std::string s1 = "/a/b/c/";
        const char* e1 = "/a/b/c";

        std::string s2 = "/a////b/c";

        TestableDirUtils dirUtils;

        ASSERT_STREQ(dirUtils.remove_slashes(s1).c_str(), e1);
        ASSERT_STREQ(dirUtils.remove_slashes(s2).c_str(), e1);
    }
}
#endif //SHELL_CHANGEDIRTEST_H
