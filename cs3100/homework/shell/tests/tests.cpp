//
// Created by abe on 2/15/17.
//
#include <gtest/gtest.h>
#include "UtilTest.h"
#include "DirUtilsTest.h"

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
