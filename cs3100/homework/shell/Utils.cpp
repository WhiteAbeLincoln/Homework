//
// Created by abe on 2/15/17.
//

#include <stdio.h>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <cstring>
#include "Utils.h"

std::string utils::ltrim(const std::string& s) {
    // returns iterator to first element that returns false for predicate
    auto iter = std::find_if_not(s.begin(), s.end(), [](char c){ return std::isspace(c); });
    // return a new string starting at our first non space element
    return std::string(iter, s.end());
}

std::string utils::rtrim(const std::string& s) {
    auto iter = std::find_if_not(s.rbegin(), s.rend(), [](char c){ return std::isspace(c); }).base();
    return std::string(s.begin(), iter);
}

std::string utils::trim(const std::string &s) {
    return ltrim(rtrim(s));
}

std::vector<std::string> utils::keepsplit(std::string s, const std::string& delim) {
    size_t pos = 0;
    std::string temp;
    std::vector<std::string> elems;
    std::string curr_token;

    pos = s.find(delim);
    while (pos != std::string::npos) {
        curr_token = s.substr(0, pos+delim.length());
        elems.push_back(curr_token);
        s.erase(0, pos+delim.length());
        pos = s.find(delim);
    }
    elems.push_back(s);

    return elems;
}

std::vector<std::string> utils::split(std::string s, char c, bool removeEmpty) {
    std::stringstream ss(s);
    std::vector<std::string> v;
    std::string temp;

    while (getline(ss, temp, c))
        v.push_back(temp); // can we use std::inserter in the getline call instead?

    if (removeEmpty) {
        auto iter = std::remove_if(v.begin(), v.end(), [](std::string t) {return t.empty();});
        v = std::vector<std::string>(v.begin(), iter);
    }

    return v;
}

// for now, only allow a single type of quote (double)
// this will break on nested quotes " ' ' "
std::vector<std::string> utils::tokenize(std::string s, char delim, char quote) {
    std::vector<std::string> res;
    // make sure to keep empty
    std::vector<std::string> vec_quote = split(s, quote, false);
    for (size_t i = 0; i < vec_quote.size(); i++) {
        // on even tokens
        if (i % 2 == 0) {
            // split at delim, remove empty
            std::vector<std::string> vec_delim = split(vec_quote[i], delim);
            // append the new tokens to the result
            for (auto elem:vec_delim)
                res.push_back(elem);

            // using the following results in a SIGABRT when echo 1 2 is called.
            // not sure why (I'm guessing the elements are passed by reference instead of copied,
            // and the last element gets deleted, and results in the abort when the program attempts to delete it again
            // res.insert(res.end(), vec_delim.begin(), vec_delim.end());
        } else {
            // otherwise, insert the entire quoted token
            res.push_back(vec_quote[i]);
        }
    }

    // remove any remaining empty tokens
    auto iter = std::remove_if(res.begin(), res.end(), [](std::string t) {return t.empty();});
    res = std::vector<std::string>(res.begin(), iter);

    return res;
}

/*template<typename T>
std::string join(const T& begin, const T& end, const std::string c) {
    std::stringstream ss;

    std::copy(begin, end, std::ostream_iterator<int>(ss, c.c_str()));

    return ss.str();
}*/

// from http://stackoverflow.com/a/1430893
/*
template <typename A>
std::string utils::join(const A &begin, const A &end, const std::string &t) {
    std::string result;
    bool added = false;
    for (A it=begin; it!=end; it++) {
        result.append(*it);
        added = true;
        if (added && it+1 != end)
            result.append(t);
    }
    return result;
}
 */

void utils::strvec_to_char(const std::vector<std::string>& strvec, int& argc, char** args) {
    argc = strvec.size();
    for (int i = 0; i < argc; i++) {
        size_t size = strvec[i].size();
        args[i] = new char[size+1]();
        strncpy(args[i], strvec[i].c_str(), size);
    }

    // terminate with null pointer, since execvp wants it
    args[argc] = NULL;
}

std::string utils::int_to_nth(const int& i) {
    std::string s = std::to_string(i);
    // we know last character must be between 0 and 9
    // so use this trick to convert to int again
    int last = s[s.size()-1] - '0';

    if (i < 10 || i > 20) {
        if (last == 0 || last > 3)
            return std::to_string(i) + "th";
        else if (last == 1)
            return std::to_string(i) + "st";
        else if (last == 2)
            return std::to_string(i) + "nd";
        else if (last == 3)
            return std::to_string(i) + "rd";
    } else {
        return std::to_string(i) + "th";
    }

    return std::to_string(i)+"th";
}
