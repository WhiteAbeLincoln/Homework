//
// Created by abe on 2/16/17.
//

#ifndef SHELL_HISTORY_H
#define SHELL_HISTORY_H

#include <string>
#include <vector>

class History {
private:
    std::string location = ".crash_history";
    std::vector<std::string> entries;
    void print_entry(const int&, const std::string&);
public:
    History(const std::string& location, bool home_relative=false);
    History() : History(location, true) {};
    ~History();
    void append(const std::string&);
    const std::string& get(const int&) const;
    void set(const int&, const std::string&);
    void remove(const int&);
    int size();
    void print(const int& n);
    void print();
    std::string& operator[](int i);
    const std::string& operator[](int i) const;
    // void set(const std::string& s):append(s) {};
};


#endif //SHELL_HISTORY_H
