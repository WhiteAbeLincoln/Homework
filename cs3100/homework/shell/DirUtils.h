//
// Created by abe on 2/16/17.
//

#ifndef SHELL_DIRUTILS_H
#define SHELL_DIRUTILS_H

#include <sys/stat.h>
#include <cerrno>
#include <regex>
#include "Utils.h"

using namespace utils;

class DirUtils {
public:
    // can't make these static because I need virtuals to override
    // is_dir in my testing function
    // c++ doesn't have static virtuals, since static lookup is done at compile time
    // this might also allow for overriding based on platform (windows?)
    virtual bool is_dir(const std::string&, int& err);
    std::string make_canonical(const std::string&);
protected:
    std::vector<std::string> remove_dot_components(std::vector<std::string>);
    std::vector<std::string> remove_dotdot_components(std::vector<std::string>);
    std::string remove_slashes(std::string);
};

bool DirUtils::is_dir(const std::string& s, int& err) {
    struct stat statbuf;

    if (stat(s.c_str(), &statbuf) != -1) {
        return S_ISDIR(statbuf.st_mode);
    } else {
        err = errno;
        return false;
    }
}

std::vector<std::string> DirUtils::remove_dot_components(std::vector<std::string> v) {
    // dot components and any slash characters separating them from the next component shall be deleted
    std::vector<std::string>::iterator iter = std::remove(v.begin(), v.end(), ".");
    return std::vector<std::string>(v.begin(), iter);
}

std::vector<std::string> DirUtils::remove_dotdot_components(std::vector<std::string> v) {
    for (size_t i=0; i < v.size(); i++) {
        // for each dot-dot component
        // if there is a preceding component
        if (v.at(i) == ".." && i > 0) {

            // and it is neither root nor dot-dot
            // (v.at(0) will always be root (or "" in this case), because of the PWD stuff earlier)
            if (v.at(i - 1) != v.at(0) || v.at(i - 1) != "..") {
                // then

                int err;

                // if the preceding component doesn't refer to a directory
                if (!is_dir(join(v.begin(), v.begin() + (i - 1), "/"), err)) {
                    // stop trying to cd, invalid path
                    // TODO: return error value to change_dir and stop trying to cd
                    return std::vector<std::string>({""});
                } else {
                    // remove preceding component, /, .., /
                    v.erase(v.begin()+(i-1), v.begin()+(i+1));
                    if (i-2 > 0)
                        i=i-2;
                    else i=0;
                    // i=i-2; // adjust index to compensate for removed elements
                }
            }
        }
    }

    // do we need to do this?
    // it should be a copy anyway, since we don't pass by reference
    return std::vector<std::string>(v);
}

std::string DirUtils::remove_slashes(std::string s) {
    // removing any trailing / characters that aren't leading slash characters
    if (s[s.size()-1] == '/' && s.size() > 1)
        s.erase(s.size()-1, 1);

    // replacing 3 or more leading slash characters with a single slash
    std::regex r("[/]{3,}");
    s = std::regex_replace(s, r, "/");

    return s;
}

std::string DirUtils::make_canonical(const std::string& dir) {
    std::vector<std::string> v = split(dir, '/', false);

    v = remove_dot_components(v);
    v = remove_dotdot_components(v);

    std::string final = join(v.begin(), v.end(), "/");

    // handle the case where v was {""}
    if (final.empty())
        final = "/";

    // an implementation may further simplify curpath by:

    final = remove_slashes(final);

    return final;
}
#endif //SHELL_DIRUTILS_H
