#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <zconf.h>
#include "Utils.h"
#include "History.h"
#include "Command.h"

// positive this won't cause any issues
// only reason utils is namespaced is for possible future conflicts,
// and because I was getting multiple definition errors when Utils was a single .h file
using namespace utils;

std::string prompt(std::string& line) {
    std::printf("[cmd:] ");
    getline(std::cin, line);
    // split discards empty elements by default, so we don't have to worry about input like:
    // ls___-lA
    // the trim handles cases like __ls_
    return trim(line);
}

std::string get_redirect(std::string& input, const std::string& dir) {
    size_t itr = input.find(dir);
    std::string output = "";

    while (itr != std::string::npos) {
        if (itr+dir.length() != input.length() && input[itr+dir.length()] == ' ') {
            // get next space, starting at the current
            size_t begin_word = itr+dir.length()+1;
            size_t next = input.find(' ', begin_word);
            if (next != std::string::npos)
                // std::printf("next: %d, curr: %d\n", (int) next, (int) (itr + dir.length()));
                // we already trimmed the string, so we know there exists a
                // non whitespace character following itr+1 if itr+1 was a space
                output = input.substr(begin_word, next - (begin_word));
            else
                // get all characters to end of line
                output = input.substr(begin_word, std::string::npos);
            input.replace(itr, output.length()+dir.length()+2, "");
        } else if (itr+dir.length() != input.length()) {
            size_t next = input.find(' ', itr);
            if (next != std::string::npos)
                output = input.substr(itr+dir.length(), next-(itr+dir.length()));
            else
                output = input.substr(itr+dir.length(), std::string::npos);
            input.replace(itr, output.length()+dir.length()+1, "");
        } else {
            std::printf("crash: syntax error near unexpected token newline\n");
            errno = 2;
            break;
        }

        itr = input.find(dir);
    }

    return output;
}



int main() {
    History history;
    std::chrono::duration<double> elapsed;
    bool loop = true;

    std::string line = "";
    std::printf("Welcome to crash:\n");
    std::printf("\tthe unstable c++ shell\n");

    while (loop) {
        // split a sequence of commands
        //std::vector<std::string> commands = split(prompt(line), ';');

        std::string input = prompt(line);
        history.append(line);

        // first extract redirection. e.g <foo and >bar
        std::string in = get_redirect(input, "<");
        std::string out_append = get_redirect(input, ">>");
        std::string out = get_redirect(input, ">");
        if (errno == 2) {
            errno = 0;
            continue;
        }

        // then split at pipes
        std::vector<std::string> command_strings = split(input, '|');
        std::vector<Command> commands;

        for (auto& elem : command_strings) {
            commands.push_back(Command(elem, elapsed, history));
        }

        if (commands.size() == 0)
            continue;

        // attach the redirection to first and last elements
        if (!in.empty()) {
            //std::printf("found input from: %s\n", in.c_str());
            commands[0].setRedirect(redirect_type::INPUT, in);
        }
        if (!out.empty()) {
            //std::printf("found output to: %s\n", out.c_str());
            commands[commands.size() - 1].setRedirect(redirect_type::OUTPUT, out);
        }
        if (!out_append.empty()) {
            //std::printf("found append to: %s\n", out_append.c_str());
            commands[commands.size() - 1].setRedirect(redirect_type::APPEND, out_append);
        }

        int fd[2];
        int in_fd = STDIN_FILENO;
        size_t i;

        // std::printf("%d commands\n", (int)commands.size());
        // method for chaining pipes adapted from http://stackoverflow.com/a/8092270
        if (commands.size() > 1) {
            for (i = 0; i < commands.size() - 1; i++) {
                // std::printf("calling %d of %d commands\n", (int) i, (int) commands.size());
                Command &cmd = commands.at(i);
                pipe(fd);

                loop = cmd.execute(in_fd, fd[1]);
                close(fd[1]);
                in_fd = fd[0];

                if (!loop) break;
            }

            if (in_fd != STDIN_FILENO) {
                // std::printf("calling %d of %d commands\n", (int) i, (int) commands.size());
                loop = commands[i].execute(in_fd, STDOUT_FILENO);
            }
        } else {
            loop = commands[0].execute(STDIN_FILENO, STDOUT_FILENO);
        }
    }

    return 0;
}