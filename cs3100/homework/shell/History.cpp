//
// Created by abe on 2/16/17.
//

#include "History.h"
#include <fstream>
#include <string>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <cstring>
#include <cmath>

History::History(const std::string& location, bool home_relative) {
    this->location = location;
    if (home_relative) {
        char* home = std::getenv("HOME");
        std::string temp(home);
        if (home != NULL) {
            if (home[std::strlen(home)-1] != '/')
                this->location = temp + ("/"+location);
            else
                this->location = temp + location;
        }
    }
    struct stat buff;

    // file exists, read number of lines
    if (stat(location.c_str(), &buff) == 0) {
        std::ifstream f(location);
        std::string temp;
        while(std::getline(f, temp))
            entries.push_back(temp);

        f.close();
    } else {
        // file doesn't exist, create it
        mode_t mode = S_IRUSR | S_IWUSR | S_IWGRP | S_IROTH;
        creat(location.c_str(), mode);
    }
}

History::~History() {
    std::ofstream f(this->location);
    if (f.is_open())
        for (auto& h : entries)
            f << h << std::endl;
    f.close();
}

const std::string& History::get(const int& i) const {
    return entries[i];
}

void History::set(const int& i, const std::string& history) {
    entries[i] = history;
}

void History::append(const std::string& s) {
    entries.push_back(s);
}

int History::size() {
    // should we do this every time?
    // or store in num_entries?
    //return read_num_lines(this->location);
    return entries.size();
}

void History::remove(const int& i) {
    this->entries.erase(this->entries.begin()+i);
}

void History::print_entry(const int& i, const std::string& s) {
    int width = 1 + (int)std::log10(this->entries.size());
    std::printf("  %*i  %s\n", width, i, s.c_str());
}

void History::print() {
    int i = 1;
    // gets the number of digits in needed for the width
    for (auto& e : entries) {
        print_entry(i, e);
        i++;
    }
}

void History::print(const int& n) {
    std::size_t i = entries.size()-n;
    if (((int)entries.size())-n < 0)
        i = 0;
    for (; i < entries.size(); i++)
        print_entry(i+1, this->entries[i]);
}

std::string& History::operator[](int i) {
    // return get(i);
    return entries[i];
}

const std::string& History::operator[](int i) const {
    return get(i);
}
