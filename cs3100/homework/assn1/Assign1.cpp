/* A02178955
   Assignment 1

   NOTE: This program requires C++11
   Compile with: `gcc -std=c++11 Assign1.cpp`
*/
#include <cstring>
#include <iostream>
#include <stdio.h>
#include <functional>
#include <cmath>

const char* help() {
  return
    "--- Assign 1 Help ---\n"
    "-fib [n] : Compute the fibonacci of [n]\n"
    "-e [n] : Compute the value of 'e' using [n] iterations\n"
    "-pi [n] : Compute Pi to [n] digits";
}

// Helper math functions

// Computes the factorial to nth term
int Factorial(int n) {
  if (n == 0) {
    return 1;
  }

  return n*Factorial(n-1);
}

// Computes the summation (∑) of fun from start to end
double Sum(std::function<double(int)> fun, int end, int start=0, int step=1) {
  double sum = 0;

  for (int i = start; i < end; i+= step) {
    sum += fun(i);
  }

  return sum;
}

int fibb(int n) {
  if (n <= 2) {
    return 1;
  }
  return fibb(n-1) + fibb(n-2);
}

double e_number(int n) {
  return Sum([](int k) { return 1.0/Factorial(k); }, n);
}

double pi_number(int n) {
  // Uses Chudnovsky_algorithm https://en.wikipedia.org/wiki/Chudnovsky_algorithm

  // 1/pi
    double over_pi = 12*Sum([](int k) {
        return (std::pow(-1, k) * Factorial(6*k) * (545140134*k + 13591409)) / (Factorial(3*k)*std::pow(Factorial(k),3)*std::pow((640320), (3*k+(3.0/2))));
      }, n);

  // pi
  return 1/over_pi;
}

int main(int argc, char** argv) {
  if (argc != 3) {
    printf("%s", help());
    return -1;
  }

  int n = -1;
  try {
    n = std::stoi(argv[2]);
  } catch (...) {
    printf("%s", help());
    return -1;
  }

  if (strcmp(argv[1], "-fib") == 0) {
    if (n < 0 || n > 40) {
      printf("n must be in range [0, 40]");
      return -1;
    }
    printf("fibb: %d\n", fibb(n));
    return 0;
  }

  if (strcmp(argv[1], "-e") == 0) {
    if (n < 1 || n > 30) {
      printf("n must be in range [1, 30]");
      return -1;
    }
    printf("e: %.*f\n", 30, e_number(n));
    return 0;
  }

  if (strcmp(argv[1], "-pi") == 0) {
    if (n < 1 || n > 10) {
      printf("n must be in range [1, 10]");
      return -1;
    }
    printf("pi: %.*f\n", n-1, pi_number(n));
    return 0;
  }

  printf("%s", help());
  return -1;
}
