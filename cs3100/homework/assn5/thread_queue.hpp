#ifndef ASSN5_THREAD_QUEUE
#define ASSN5_THREAD_QUEUE

#include <mutex>
#include <deque>
#include <queue>
#include <utility>
#include <condition_variable>

/**
   A thread safe implementation of the queue container adaptor
   see http://en.cppreference.com/w/cpp/container/queue for documentation
   I would like to implement all of the methods in the future, but that may
   not be possible, since some are not thread friendly (like the separate front and pop methods)
 */
template<class T, class Container = std::deque<T>>
class thread_queue {
private:
  mutable std::mutex m;
  std::condition_variable cv;
protected:
  std::queue<T, Container> c;
public:
  typedef Container container_type;
  typedef typename Container::value_type value_type;
  typedef typename Container::size_type size_type;

  explicit thread_queue( const Container& cont ) : c(cont) {}
  explicit thread_queue( Container&& cont = Container() ) : c(cont) {}
  // copy constructor
  thread_queue( const thread_queue& other ) : c(other.c) {}
  // move constructor
  thread_queue( thread_queue&& other ) : c(other.c) {}

  bool empty() const {
    std::lock_guard<std::mutex> lock(m);
    return c.empty();
  }

  size_type size() const {
    std::lock_guard<std::mutex> lock(m);
    return c.size();
  }

  void push(const value_type& value) {
    std::lock_guard<std::mutex> lock(m);
    c.push(value);
    cv.notify_one();
  }

  void push(value_type&& value) {
    std::lock_guard<std::mutex> lock(m);
    c.push(std::move(value));
    cv.notify_one();
  }

  template<class... Args>
  void emplace(Args&&... args) {
    std::lock_guard<std::mutex> lock(m);
    c.push(T(std::forward<Args>(args)...));
    cv.notify_one();
  }

  // Retrieves and removes the head of the queue, waiting if necessary until element becomes available
  T pop() {
    std::unique_lock<std::mutex> lock(m);
    while (c.empty()) {
      cv.wait(lock);
    }
    auto item = c.front();
    c.pop();
    lock.unlock();
    return item;
  }
};

#endif // ASSN5_THREAD_QUEUE
