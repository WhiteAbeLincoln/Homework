#ifndef ASSN5_THREAD_UNORDERED_MAP
#define ASSN5_THREAD_UNORDERED_MAP

#include <mutex>
#include <unordered_map>
#include <condition_variable>
#include <utility>
#include <algorithm>

/** Wrapper arround the unordered_map container
    Supports most constructor methods
 */
template<
  class Key,
  class T,
  class Hash = std::hash<Key>,
  class KeyEqual = std::equal_to<Key>,
  class Allocator = std::allocator< std::pair<const Key, T> >
  >
class thread_unordered_map {
private:
  typedef std::unordered_map<Key, T, Hash, KeyEqual, Allocator> Container;
  mutable std::mutex m;
  // I don't think need this, since I'm never waiting on any of these functions
  // I'll keep it for possible future changes though
  std::condition_variable cv;
protected:
  Container c;
public:
  // forward most of the member types
  typedef typename Container::key_type key_type;
  typedef typename Container::mapped_type mapped_type;
  typedef typename Container::value_type value_type;
  typedef typename Container::size_type size_type;
  typedef typename Container::difference_type difference_type;
  typedef typename Container::hasher hasher;
  typedef typename Container::key_equal key_equal;
  typedef typename Container::allocator_type allocator_type;
  typedef value_type& reference;
  typedef const value_type& const_reference;

  // constructs empty container
  thread_unordered_map() : c( size_type(/*implementation-defined*/) ) {}
  explicit thread_unordered_map( size_type bucket_count,
                                 const Hash& hash = Hash(),
                                 const KeyEqual& equal = KeyEqual(),
                                 const Allocator& alloc = Allocator() ) : c(bucket_count,
                                                                            hash, equal, alloc) {}
  thread_unordered_map( size_type bucket_count,
                        const Allocator& alloc ) : c(bucket_count, Hash(), KeyEqual(), alloc) {}
  thread_unordered_map( size_type bucket_count,
                        const Hash& hash,
                        const Allocator& alloc ) : c(bucket_count, hash, KeyEqual(), alloc) {}
  explicit thread_unordered_map( const Allocator& alloc ) : c(alloc) {}
  // range constructors
  template< class InputIt >
  thread_unordered_map( InputIt first, InputIt last,
                        size_type bucket_count,
                        const Allocator& alloc ) : c(first, last,
                                                     bucket_count, Hash(), KeyEqual(), alloc) {}
  template< class InputIt >
  thread_unordered_map( InputIt first, InputIt last,
                        size_type bucket_count,
                        const Hash& hash,
                        const Allocator& alloc ): c(first, last,
                                                    bucket_count, hash, KeyEqual(), alloc) {}
  // Not sure how safe this is when dealing with threads
  // for now we don't care since I won't be using any of these constructors anyway

  // copy constructors
  thread_unordered_map(const thread_unordered_map& o) : c(o.c) {}
  thread_unordered_map(const thread_unordered_map& o, const Allocator& a) : c(o.c, a) {};
  // move constructors
  thread_unordered_map(thread_unordered_map&& o) noexcept : c(o.c) {}
  thread_unordered_map(thread_unordered_map&& o, const Allocator& a) noexcept : c(o.c, a) {}
  thread_unordered_map(std::initializer_list<value_type> i) : c(i) {}
  // initializer list constructor
  thread_unordered_map( std::initializer_list<value_type> init,
                        size_type bucket_count,
                        const Allocator& alloc ) : c(init, bucket_count,
                                                     Hash(), KeyEqual(), alloc) {}
  thread_unordered_map( std::initializer_list<value_type> init,
                        size_type bucket_count,
                        const Hash& hash,
                        const Allocator& alloc ) : c(init, bucket_count,
                                                     hash, KeyEqual(), alloc) {}

  void clear() noexcept {
    std::lock_guard<std::mutex> lock(m);
    c.clear();
  }

  bool empty() const noexcept {
    std::lock_guard<std::mutex> lock(m);
    return c.empty();
  }

  size_type size() const noexcept {
    std::lock_guard<std::mutex> lock(m);
    return c.size();
  }

  size_type max_size() const noexcept {
    std::lock_guard<std::mutex> lock(m);
    return c.max_size();
  }

  bool insert(const value_type& value) {
    std::lock_guard<std::mutex> lock(m);
    auto r = c.insert(value);
    cv.notify_one();
    return r.second;
  }

  bool insert(value_type&& value) {
    std::lock_guard<std::mutex> lock(m);
    auto r = c.insert(std::forward<value_type>(value));
    cv.notify_one();
    return r.second;
  }

  template< class InputIt >
  void insert(InputIt first, InputIt last) {
    std::lock_guard<std::mutex> lock(m);
    c.insert(first, last);
    cv.notify_one();
  }

  void insert(std::initializer_list<value_type> ilist) {
    std::lock_guard<std::mutex> lock(m);
    c.insert(ilist);
    cv.notify_one();
  }

  template<class UnaryFunction>
  void for_each(UnaryFunction f) {
    std::lock_guard<std::mutex> lock(m);
    std::for_each(c.begin(), c.end(), f);
  }

  const T& at(const Key& key) const {
    std::lock_guard<std::mutex> lock(m);
    return c.at(key);
  }
};

#endif // ASSN5_THREAD_UNORDERED_MAP
