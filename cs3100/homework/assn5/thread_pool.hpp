#ifndef ASSN5_THREADPOOL
#define ASSN5_THREADPOOL

#include <vector>
#include <thread>
#include "thread_queue.hpp"
#include <functional>

class thread_pool {
private:
  std::vector<std::thread> workers;
  thread_queue<std::function<void()>> tasks;
public:
  // default constructor uses the number of hardware threads
  thread_pool() : thread_pool(std::thread::hardware_concurrency()) {}
  thread_pool(int threads) {
    // adds a new worker for each thread
    // a worker is a std::thread object that
    // executes items from the tasks queue until empty
    for (int i = 0; i < threads; ++i) {
      workers.push_back(
                        std::thread([this] {
                            // loop until we have no remaining tasks
                            for (;;) {
                              std::function<void()> task;
                              task = this->tasks.pop();

                              task();

                              // this is a possibly blocking call for eash iteration
                              // of the loop. Is there any better solution?
                              if (this->tasks.empty()) {
                                return;
                              }
                            }
                          })
                        );
    }
  }

  ~thread_pool() {
    this->join();
  }

  void join() {
    for (auto& w : workers) {
      if (w.joinable()) {
        w.join();
      }
    }
  }

  // provides pretty much the same interface as std::thread constructor
  // a function to run, and parameters to pass
  // TODO: look into using std::future to support return values
  template<class F, class... Args>
  void add(F&& f, Args&&... args) {
    auto task = std::bind(f, args...);

    // once the thread runs, we'll have triple nested lambdas yayy!
    tasks.emplace([task]() {
        (task)();
      });
  }
};

#endif //ASSN5_THREADPOOL
