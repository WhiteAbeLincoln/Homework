#include <vector>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "thread_pool.hpp"
#include "thread_unordered_map.hpp"
#include "compute_pi.h"

void thread_task(unsigned int task, thread_unordered_map<unsigned int, unsigned int>& map) {
    std::cout << ".";
    std::cout.flush();
    // TODO: use https://en.wikipedia.org/wiki/Bailey%E2%80%93Borwein%E2%80%93Plouffe_formula
    // Wikipedia says it is O(n*log(n)), which should be faster than O(n^2)
    auto digit = computePiDigit(task+1);

    map.insert({task, digit});
}

int main() {
  // the thread pool abstracts all of the thread creation and task management for us
  thread_pool tp;
  thread_unordered_map<unsigned int, unsigned int> map;

  for (unsigned int i = 0; i < 1000; ++i) {
    // we have to use std::ref to pass our reference through std::bind and the std::thread constructor
    // I could have a lambda that captures map by reference, but I prefered having the separate function
    tp.add(thread_task, i, std::ref(map));
  }

  tp.join();

  std::cout << std::endl
            << std::endl
            << "3.";
  for (int i = 0; i < 1000; ++i) {
    std::cout << map.at(i);
  }

  return 0;
}
