#ifndef ASSN5_COMPUTE_PI
#define ASSN5_COMPUTE_PI
unsigned int computePiDigit(int n);
unsigned long long piDigitHex(unsigned long long n);
#endif //ASSN5_COMPUTE_PI
