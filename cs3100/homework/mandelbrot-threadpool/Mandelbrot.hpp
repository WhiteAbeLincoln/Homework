// Abe White A02178955
#ifndef MANDELBROT_MANDELBROT_H
#define MANDELBROT_MANDELBROT_H

#include <string>
#include <complex>
#include <vector>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <functional>
#include <thread>
#include "ThreadPool.hpp"
#include "Plane.h"

struct Color {
    int r;
    int g;
    int b;
};

// program config defaults
Plane<int> image(0,512,0,512);
Plane<double> complexPlane(-2,0.5,-1.12,1.12);
int mandelbrotIterMax = 1000;
Color startColor = {42, 10, 200};
Color endColor = {200, 125, 0};
std::string outfile = "mandelbrot.ppm";
int totalIterations = 1000;
int numThreads = 2;

// defines the mandelbrot function
std::complex<double> mandelbrotFunc(std::complex<double> z, std::complex<double> c) {
    return z*z + c;
}

// scales one plane to another
std::complex<double> scaleToPlane(Plane<int> real, Plane<double> cPlane, std::complex<double> comp) {
    // scaledX = complexPlane.xmin + xpixel * (complexPlane width / image width)
    double realPart = cPlane.getXmin() + comp.real() * (cPlane.width() / real.width());
    double imaginary = cPlane.getYmin() + comp.imag() * (cPlane.height() / real.height());

    return std::complex<double>(realPart, imaginary);
}

// measures the number of iterations before function hits escape condition
int escapeFunc(std::complex<double> c, int maxIterations) {
    std::complex<double> z(0,0);
    int i = 0;

    while (abs(z) < 2 && i < maxIterations) {
        z = mandelbrotFunc(z, c);
        i++;
    }

    return i;
}

// gets the color based on iteration and beginning and ending colors
Color getColorFromIteration(Color c1, Color c2, int iter, int maxIter) {
    // linear interpolation between color 1 and color 2
    Color newColor;
    newColor.r = static_cast<int>(round(c1.r - (((c1.r - c2.r) / static_cast<float>(maxIter)) * iter)));
    newColor.g = static_cast<int>(round(c1.g - (((c1.g - c2.g) / static_cast<float>(maxIter)) * iter)));
    newColor.b = static_cast<int>(round(c1.b - (((c1.b - c2.b) / static_cast<float>(maxIter)) * iter)));

    return newColor;
}

// gets the color for a singular pixel
// escape time algorithm code adapted from wikipedia page on Mandelbrot Set to work with std::complex
Color getColorForPixel(int x, int y) {
    std::complex<double> complex1((double)x, (double)y);
    complex1 = scaleToPlane(image, complexPlane, complex1);
    int iterations = escapeFunc(complex1, mandelbrotIterMax);
    return getColorFromIteration(startColor, endColor, iterations, mandelbrotIterMax);
}

// generic function to time arbitrary function
template <typename Fn>
double timeFunc(Fn f) {
    auto start = std::chrono::steady_clock::now();
    f();
    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> diff = end - start;
    return diff.count();
}

// compute average of array of doubles
double compAverage(std::vector<double> arr) {
    double total = 0;

    for(double &elem : arr) {
        total += elem;
    };

    return total / arr.size();
}

// compute standard deviation of array of doubles
double compStdDev(std::vector<double> arr) {
    double sum = 0;
    double avg = compAverage(arr);

    for(double &elem : arr) {
        sum += ((elem - avg)*(elem - avg));
    };

    double stdDev = std::sqrt((1/(float)arr.size()) * sum);

    return stdDev;
}

// prints out the color array to a ppm file
void print(std::string outfile, int width, int height, std::vector<Color> colors) {
    std::ofstream of(outfile);
    of << "P3" << std::endl;
    of << width << " " << height << std::endl;
    of << 255 << std::endl;

    int rows = 0;

    for(Color &c : colors) {
        of << std::setw(4) << c.r << std::setw(4) << c.g << std::setw(4) << c.b << std::setw(2) << " ";
        rows++;
        if (rows == width) {
            of << std::endl;
            rows = 0;
        }

    };

    of.close();
}
// this helper just applies all the parameters to the curried version
void calcSection(int startx, int endx, int starty, int endy, std::vector<Color>* colorMatrix) {
    for (int row = starty; row < endy; row++) {
        for (int col = startx; col < endx; col++) {
            Color c = getColorForPixel(col, row);
            // as long as the start and end x and y values don't overlap with any other threads, we don't have to worry
            // about concurrent writes to the same memory location
            // TODO: Determine if std::vector::at is thread safe
            colorMatrix->at(image.width()*row + col) = c;
        }
    }
}

// this a really ugly curried version of calcSection for better use by the producer
auto _calcSection = ([](int startx /* int endx, int starty, int endy, std::vector<Color>* colorMatrix */) {
    return [startx](int endx) {
        return [startx, endx](int starty) {
            return [startx, endx, starty](int endy) {
                return [startx, endx, starty, endy](std::vector<Color>* colorMatrix) {

                    for (int row = starty; row < endy; row++) {
                        for (int col = startx; col < endx; col++) {
                            Color c = getColorForPixel(col, row);
                            // as long as the start and end x and y values don't overlap with any other threads, we don't have to worry
                            // about concurrent writes to the same memory location
                            // TODO: Determine if std::vector::at is thread safe
                            colorMatrix->at(image.width()*row + col) = c;
                        }
                    }


                };
            };
        };
    };
});

// producer function
void produceWork(int type, ThreadPool* pool, std::vector<Color>* colorMatrix) {
    switch (type) {
        // 1 pixel per thread
        case 0:
            for (int j = image.getYmin(); j < image.getYmax(); j++) {
                for (int i = image.getXmin(); i < image.getXmax(); i++) {
                    pool->add(calcSection, i, i+1, j, j+1, colorMatrix);
                }
            }
            break;

        // multiple pixels, less than one row per thread
        case 1:
            // divide row in two
            for (int j = image.getYmin(); j < image.getYmax(); j++) {
                pool->add(calcSection, image.getXmin(), image.getXmax()/2, j, j+1, colorMatrix);
                pool->add(calcSection, image.getXmax()/2, image.getXmax(), j, j+1, colorMatrix);
            }
            break;

        // 1 row
        case 2:
            for (int j = image.getYmin(); j < image.getYmax(); j++) {
                pool->add(calcSection, image.getXmin(), image.getXmax(), j, j+1, colorMatrix);
            }
            break;

        // multiple rows, but not even chunks
        case 3:
            pool->add(calcSection, image.getXmin(), image.getXmax(), image.getYmin(), image.getYmax()/2, colorMatrix);
            pool->add(calcSection, image.getXmin(), image.getXmax(), image.getYmax()/2, (image.getYmax()/3)*2, colorMatrix);
            pool->add(calcSection, image.getXmin(), image.getXmax(), (image.getYmax()/3)*2, image.getYmax(), colorMatrix);
            break;

        // rows/n size chunks
        case 4:
            for (int i = 0; i < numThreads; i++) {
                int y = (image.getYmax()/numThreads);
                pool->add(calcSection, image.getXmin(), image.getXmax(), y*i, y*(i+1), colorMatrix);
            }
            break;

        default:
            break;
    }
}

// consumer function
/*
void consumeWork(std::queue<auto>* funcQueue, std::vector<Color>* colorMatrix) {
    std::unique_lock<std::mutex> lck(mtx);
    // while (funcQueue->size() == 0) cv.wait(lck);
    for (;;) {
        cv.wait(lck, [funcQueue]{ return funcQueue->size() != 0;});
        if (funcQueue->size() == 0) return;
        std::function<void(std::vector<Color>*)> f = &funcQueue->front();
        f(colorMatrix);
        funcQueue->pop();
    }
}
*/


// runs the mandelbrot sequence
void mandelbrot(bool printImg, int type) {
    // std::vector<std::vector<Color>> colorMatrix(image.width(), std::vector<Color>(image.height()));
    std::vector<Color> colorMatrix(image.width()*image.height());
    ThreadPool pool(numThreads);

    // we can't really split a single pixel computation over multiple threads, so limit maximum number to the number of rows
    // performance increase is minimal, and I don't want to figure out how to divide up an image row right now
    if (numThreads > image.getYmax()) {
        numThreads = image.getYmax();
    }

    // for a threadpool, instead of tuning the task to the number of threads, we have a set task e.g. calculate a single pixel
    // and we have a set number of threads. When a thread completes its task, we assign a new one to it.
    // that's where the producer/consumer pattern comes in. The producer creates a set of tasks, and the consumer threads
    // get tasks from the set until it's empty
    std::thread producer(produceWork, type, &pool, &colorMatrix);
    producer.join();
    pool.join();

    if (printImg) {
        print(outfile, image.width(), image.height(), colorMatrix);
    }

    colorMatrix.clear();
}

#endif // MANDELBROT_MANDELBROT_H
