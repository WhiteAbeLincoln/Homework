//
// Created by abe on 2/4/17.
//

#ifndef MANDELBROT_THREADPOOL_THREADPOOL_H
#define MANDELBROT_THREADPOOL_THREADPOOL_H

#include <vector>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>

// threadpool implementation adapted from https://github.com/progschj/ThreadPool/blob/master/ThreadPool.h 

class ThreadPool {
public:
    ThreadPool(int);
    template<typename F, typename... Args>
    void add(F&&, Args&&...);
    void join();
    ~ThreadPool();

private:
    std::vector<std::thread> workers;
    std::queue<std::function<void()>> tasks;

    int i = 0;
    std::mutex mtx;
    std::condition_variable cv;
    bool stop;
};

ThreadPool::ThreadPool(int threads) {
    stop = false;
    for (int i = 0; i < threads; i++) {
        workers.push_back(
                // this is our consumer thread
                // loops while there are tasks to complete
                std::thread([this]{
                    for (;;) {//while (!this->tasks.empty()) {
                        std::function<void()> task;

                        {
                            std::unique_lock<std::mutex> lck(this->mtx);
                            this->cv.wait(lck, [this] {return this->stop || !this->tasks.empty();});
                            if (this->stop && this->tasks.empty()) return;
                            task = this->tasks.front();
                            this->tasks.pop();
                            this->i++;
                        }

                        task();

                        if (this->tasks.size() == 0) {
                            return;
                        }
                    }
                })
        );
    }
}

void ThreadPool::join() {
    for (std::thread& w : workers) {
        w.join();
    }
}

// very basic for now, we won't return anything from the worker functions
template<typename F, typename... Args>
void ThreadPool::add(F&& f, Args&&... args) {
    auto task = std::bind(f, args...);

    {
        std::unique_lock<std::mutex> lck(mtx);
        tasks.emplace([task](){
            (task)();
        });
    }
    cv.notify_one();
}

ThreadPool::~ThreadPool() {

    {
        std::unique_lock<std::mutex> lck(mtx);
        stop = true;
    }
    cv.notify_all();

    // joins threads to main once destroyed
    for (std::thread& w : workers) {
        if (w.joinable())
            w.join();
    }
}

#endif //MANDELBROT_THREADPOOL_THREADPOOL_H
