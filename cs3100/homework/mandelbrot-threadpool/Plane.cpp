// Abe White A02178955
#include "Plane.h"

template <typename T>
Plane<T>::Plane(T xmin, T xmax, T ymin, T ymax) {
    this->xmin = xmin;
    this->xmax = xmax;
    this->ymin = ymin;
    this->ymax = ymax;
}

template <typename T>
T Plane<T>::width() {
    return xmax - xmin;
}

template <typename T>
T Plane<T>::height() {
    return ymax - ymin;
}

template <typename T>
T Plane<T>::area() {
    return width()*height();
}

template <typename T>
T Plane<T>::getXmin() {
    return xmin;
}

template <typename T>
T Plane<T>::getXmax() {
    return xmax;
}

template <typename T>
T Plane<T>::getYmin() {
    return ymin;
}

template <typename T>
T Plane<T>::getYmax() {
    return ymax;
}

// explicitly instantiate the template classes
template class Plane<int>;
template class Plane<double>;
