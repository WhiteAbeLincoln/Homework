#ifndef CPSL_VALUE_H
#define CPSL_VALUE_H
#include <variant>
#include <type_safe/types.hpp>

namespace AST {
  /**
   * A tagged union of our primitive AST values
   * includes strings, integers, characters
   *
   * Written more properly:
   * type Value =
   *   | { type: STR; value: char* }
   *   | { type: INT; value: int }
   *   | { type: CHAR; value: char }
   *   | { type: BOOL; value: bool }
   * or
   * data Value = Str String | Int Integer | Chr Char | Boolean Bool
   */
  using Value = std::variant<
    long int,
    std::string,
    char,
    type_safe::bool_t
  >;
};

#endif // CPSL_VALUE_H
