#ifndef CPSL_TABLE_H
#define CPSL_TABLE_H
#include <unordered_map>
#include <vector>

template <typename A>
class Table {
private:
  static std::size_t& getCounter() {
    // initializing static counter in function avoids the
    // ISO C++ forbids in-class initialization error
    static std::size_t counter = 0;
    return counter;
  }

  int id;

  Table<A>* parent;
  Table<A>* child;

public:
  Table() : id(getCounter()++)
          , parent(nullptr)
          , child(nullptr) {}

  const Table<A>* getParent() const { return this->parent; }
  const Table<A>* getChild() const { return this->child; }
  int getId() const { return id; }


  // used to keep track of insertion order for printing
  // since printing needs to be in the same order as the input
  // code or it won't have the same semantics
  // or we could not use references in the printed code and just fully
  // expand symbol and type definitions, but that greatly increases the
  // verbosity of the output and wouldn't work with recursive types
  std::vector<std::string> insertion_order;
  std::unordered_map<std::string, A> table;

  Table<A>* push() {
    auto newTable = new Table();
    newTable->parent = this;
    this->child = newTable;
    return newTable;
  }

  Table<A>* pop() const {
    auto table = this->parent;
    if (table == nullptr) {
      throw std::runtime_error("Cannot pop a table with no parent");
    }

    return table;
  }

  A lookup(std::string name) const {
    auto pointer = this;
    // it would be cool to define an iterator for this, but they're really complicated in c++
    while (pointer != nullptr) {
      auto table = pointer->table;
      auto found = table.find(name);
      if (found != table.end())
        return found->second;
      pointer = pointer->parent;
    }

    return nullptr;
  }

  bool insert(std::string name, A val) {
    auto existing = this->table.find(name);
    if (existing != this->table.end()) {
      return false;
    }
    this->insertion_order.push_back(name);
    this->table[name] = val;
    return true;
  }

  const Table<A>* root() const {
    auto pointer = this;
    while (pointer->parent != nullptr) {
      pointer = pointer->parent;
    }

    return pointer;
  }
};

#endif // CPSL_TABLE_H
