#ifndef CPSL_OPTIMIZE_H
#define CPSL_OPTIMIZE_H
#include "ast.hpp"

namespace optim {

  AST::Value evaluate_const_op(
    const AST::Operation&,
    const AST::SymbolTable*
  );
  AST::Value evaluate_const_expr(
    const AST::Expression&,
    const AST::SymbolTable*
  );
  AST::Expression* try_evaluate_const_expr(
    AST::Expression*,
    const AST::SymbolTable*
  );
}
#endif // CPSL_OPTIMIZE_H
