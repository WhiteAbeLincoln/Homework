#ifndef CPSL_UTIL_H
#define CPSL_UTIL_H
#include <cstdarg>
#include <vector>
#include <string>
#include <algorithm>
#include <functional>
#include <optional>

namespace util {
  struct Config {
    bool verbose;
    bool print_graph;
    bool print_ast;
  };
  extern Config CpslConfig;

  int print_verbose1(const char* fmt, ...);
  int print_verbose(const char* fmt, ...);
  std::string print_char(const char chr);
  std::string escape_string(std::string);
  std::string join(const std::vector<std::string>, const std::string = ",");
  template <typename T>
  // inserts elements of the second vector into the first, mutating it
  void insert_flat(std::vector<T>& old, std::vector<T> inserted)  {
    old.insert(
      std::end(old)
    , std::make_move_iterator(inserted.begin())
    , std::make_move_iterator(inserted.end())
    );
  }
  template <typename T>
  // concatenates two vectors and returns a copy
  std::vector<T> concat(std::vector<T> old, std::vector<T> inserted)  {
    old.insert(
      std::end(old)
    , std::make_move_iterator(inserted.begin())
    , std::make_move_iterator(inserted.end())
    );
    return old;
  }
  template <typename T>
  std::vector<T> push_back(std::vector<T> arr, T val) {
    arr.push_back(val);
    return arr;
  }

  template <typename A, typename B>
  std::vector<B> map_vec(std::vector<A> as, std::function<B(const A&)> f) {
    std::vector<B> ret;
    ret.reserve(as.size());
    std::transform(as.begin(), as.end(), std::back_inserter(ret), f);
    return ret;
  }

  template <typename A, typename B>
  A match_optional(std::optional<A> o, std::function<B(const A&)> v, std::function<B()> nv) {
    if (o.has_value()) return v(*o);
    return nv();
  }

  // see example code for https://en.cppreference.com/w/cpp/utility/variant/visit
  // helper type for the visitor #4

  template<class... Ts> struct match : Ts... { using Ts::operator()...; };
  template<class... Ts> match(Ts...) -> match<Ts...>;
}

#ifdef ALLOW_VERBOSE
#define PRINT_VERBOSE(fmt, ...) util::print_verbose1(fmt __VA_OPT__(,) __VA_ARGS__)
#define PRINT_VERBOSE1(fmt, ...) util::print_verbose1(fmt __VA_OPT__(,) __VA_ARGS__)
#else
#define PRINT_VERBOSE(fmt, ...)
#define PRINT_VERBOSE1(fmt, ...)
#endif

#endif // CPSL_UTIL_H
