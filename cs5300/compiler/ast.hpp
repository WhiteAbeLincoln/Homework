#ifndef CPSL_AST_H
#define CPSL_AST_H
#include <vector>
#include <string>
#include <utility>
#include <unordered_map>
#include "table.tpp"
#include <type_safe/types.hpp>
#include <variant>
#include <valuable/value-ptr.hpp>
#include <optional>
#include "value.hpp"
#include "mips.hpp"

namespace AST {
// we need partial declaration
struct Array;
struct Record;

struct Type {
  enum class Kind;
  Type(std::string name, Kind tag);
  Type(std::string name, Record* record);
  Type(std::string name, Array* array);
  Type(Array* array);
  Type(Record* record);
  std::string name;
  enum class Kind {
    STR
  , INT
  , CHAR
  , BOOL
  , ARRAY
  , RECORD
  , VOID
  } tag;
  union {
    Array* array;
    Record* record;
  };
};

extern Type* type_string;
extern Type* type_char;
extern Type* type_int;
extern Type* type_bool;
extern Type* type_void;

typedef std::vector<std::string> IdentList;
typedef std::pair<IdentList, const Type*> Declaration;
typedef std::vector<Declaration> DeclarationList;

std::string print_decl(const Declaration&);
std::string print_decllist(const DeclarationList&);
// I don't like putting the implementation in the header
// but it seems to be required for template functions
// I could manually specialize but thats almost worse than this
template <typename A>
std::vector<A>* add_and_own(A* val, std::vector<A>* list = new std::vector<A>()) {
  list->push_back(*val);
  delete val;
  // explicitly set to nullptr so we can catch errors more easily
  val = nullptr;
  return list;
}

IdentList* add_ident(const char*, IdentList*);

struct Symbol {
  Symbol(std::string, const Type*);
  Symbol(std::string, Value);
  /* It turns out that C (and by extension C++) have different namespaces
    for structs, enums, and standard variables. In C, you must explicitly
    specify that you are passing a struct to a function, not just some type
    in the global namespace:
      struct X {};
      void f(X x) {}; // Error X is not defined
      void f(struct X x) {}; // valid
    A common pattern is to use typedef with an anonymous struct, placing
    that name in the global namespace:
      typedef struct { } X; // OR
      typedef struct X { } X;
    The above also applies to enums
    C++ does not have this same restriction - if a name is not found in
    the struct and enum namespaces are searched as well.
      struct X {};
      void f(X x) {}; // valid b/c C++ searches other namespaces
  */
  std::string name;
  const Type* type;
  std::optional<Value> const_val;
  long size;
  long offset;
  mips::Register offset_reg = mips::global_ptr;
  bool ref = false;
  bool param = false;
};

extern Value* val_true;
extern Value* val_false;
extern Symbol* sym_true;
extern Symbol* sym_false;

using TypeTable = Table<const Type*>;
using SymbolTable = Table<Symbol*>;

TypeTable* initialize(TypeTable*);
SymbolTable* initialize(SymbolTable*);
void insert(TypeTable&, std::string, const Type*);
void insert(SymbolTable&, std::string, Symbol*);
Symbol* lookup(const SymbolTable&, std::string);
const Type* lookup(const TypeTable&, std::string);

struct Param {
  bool ref = false;
  std::string name;
  const Type* type;
};

struct Signature {
  std::string name;
  std::vector<Param> params;
  const Type* return_type = type_void;
  std::string label = "";
};

std::vector<Param>* create_params(bool, AST::IdentList&, const AST::Type*);
void insert_params(SymbolTable*, const Signature&);

struct Operation;
struct LValue;
struct Call;

using OpPtr = valuable::value_ptr<Operation>;
using LValPtr = valuable::value_ptr<LValue>;
using CallPtr = valuable::value_ptr<Call>;
using Expression = std::variant<
  OpPtr
, LValPtr
, Value
, CallPtr
>;

struct Call {
  Signature sig;
  std::vector<Expression> args;
};

bool is_constant(const Expression&);

using ConstDecl = std::pair<const char*, const Expression*>;
using ConstDeclList = std::vector<ConstDecl>;

void free_value(Value*);
void free_operation(Operation*);

// ideally this would be a variant
// but I can't figure out recursive variants for c++17
// without using a third-party library like boost
//
// struct LValueSym { Symbol sym_val; };
// struct LValueRec { Symbol sym_val; LValue parent; };
// struct LValueArr { Expression idx; LValue parent; };
// using LValue = std::variant<LValueSym, LValueRec, LValueArr>;
struct LValue {
  enum class Kind {
    SYMBOL
  , ARRAY
  , RECORD
  } tag;

  union {
    Symbol* sym_val;
    // we need pointers because symbols can be mutated later to add size information
    // todo: determine if unique_ptr or shared_ptr is appropriate
    std::pair<LValue*, Expression>* array_val;
    std::pair<LValue*, Symbol*>* rec_val;
  };

  // record
  LValue(LValue*, std::string);
  // array
  LValue(LValue*, const Expression*);
  // symbol
  LValue(std::string, const SymbolTable&);
};

using ReadStmt = std::vector<LValue>;
ReadStmt* add_read(LValue*, ReadStmt* = new ReadStmt());

using WriteStmt = std::vector<Expression>;
WriteStmt* add_write(Expression*, WriteStmt* = new WriteStmt());

struct Stop {};
struct Assignment {
  // does type checking
  Assignment(LValue, Expression);
  LValue lval;
  Expression expr;
};

struct Return {
  std::optional<Expression> value;
};

struct IfStatement;
struct While;
struct Repeat;
struct For;
// we need a pointer/container type to allow IfStatement to be
// heap allocated, since it contains recursive references to
// the Statement variant (which means Statement's size cannot be
// statically determined). We use a copyable unique-ptr like
// container to achieve this
using IfStmtPtr = valuable::value_ptr<IfStatement>;
using WhilePtr = valuable::value_ptr<While>;
using RepeatPtr = valuable::value_ptr<Repeat>;
using ForPtr = valuable::value_ptr<For>;
using Statement = std::variant<
  Assignment
, Stop
, WriteStmt
, ReadStmt
, IfStmtPtr
, WhilePtr
, RepeatPtr
, ForPtr
, Return
, Call
>;

using StatementList = std::vector<AST::Statement>;

struct ElseIf {
  ElseIf(
    Expression
  , StatementList
  );
  Expression pred;
  StatementList body;
};

struct IfStatement {
  IfStatement(
    Expression
  , StatementList
  , std::vector<ElseIf> = std::vector<ElseIf>{}
  , StatementList = StatementList{}
  );
  Expression pred;
  StatementList body;
  std::vector<ElseIf> elseifs;
  StatementList else_body;
};

struct While {
  While(
    Expression
  , StatementList
  );
  Expression pred;
  StatementList body;
};

struct Repeat {
  Repeat(
    Expression
  , StatementList
  );
  Expression pred;
  StatementList body;
};

enum class ForDir {
  PRED
, SUCC
};

struct ForHead {
  ForDir dir;
  Expression end;
};

struct For {
  For(
    Assignment,
    ForDir,
    Expression,
    StatementList
  );
  Assignment init;
  ForDir dir;
  Expression end;
  StatementList body;
};

struct Operation {
  // forward declare
  // the operation kind (add,subtract,multiply)
  enum class Kind;

  Operation(Kind, Expression);
  Operation(Kind, Expression, Expression);
  Operation(Kind, std::vector<Expression>);

  /*
  I can't find any way to combine enums
  in typescript or a ML-like language I would do:
    type ArithOps = 'div' | 'mult' | 'sub' | 'add' | 'mod'
    type RelOps = 'lt' | 'gt' | 'lte' | 'gte' | 'eq'
    type LogicalOps = 'and' | 'or' | 'not' // 'disjunction' | 'conjunction' | 'complement'
    type Operations = ArithOps | RelOps | LogicalOps
  There is a trick with terminating names and relying on the compiler
  assigning values sequentially, but with -Wall the compiler complains
  that the enums are not the same
  E.g:
  enum ArithOp {
    DIV = 0,
    MULT,
    SUB,
    ADD,
    MODULUS,
    arithEnd
  };
  enum LogicalOp {
    AND = ArithOp::relEnd,
    OR,
    NOT,
    logEnd
  };

  Solution: constructor functions and runtime checks :(
    we would have had to check anyway though
  */
  enum class Kind {
  // arithmetic
    DIV = 0
  , MULT
  , SUB
  , ADD
  , MOD
  // relational
  , LT
  , GT
  , LTE
  , GTE
  , EQ
  , NEQ
  // logical
  , AND
  , OR
  , NOT
  // intrinsic functions
  , CHR
  , ORD
  , PRED
  , SUCC
  } op;
  const Type* type;
  // if we had c++17 I would use std::optional on the right
  // as it is now I have to just use a vector, which may actually be better
  // (we could easily support operations with more than 2 operands)
  std::vector<Expression> operands;
};

struct Array {
  Array(Expression*, Expression*, const Type*);
  // array bounds must be constant, which can be determined at compile time
  long int startI;
  long int endI;
  const AST::Type* of;
};

struct Record {
  AST::SymbolTable table;
};

struct Body {
  SymbolTable table;
  TypeTable type_table;
  StatementList statements;
};

using Function = std::tuple<Signature, std::optional<Body>>;
class FunctionTable {
public:
  std::unordered_map<std::string, Function> table;
  std::optional<Signature> lookup(const std::string&) const;
  std::optional<Function> lookup(const Signature&) const;
  std::tuple<bool, std::tuple<Signature, std::optional<Body>>>
    insert(Signature sig, std::optional<Body>);
  std::tuple<Signature, std::optional<AST::Body>> force_insert(Signature, std::optional<Body>);
  std::tuple<Signature, std::optional<AST::Body>> force_insert(Signature, std::optional<Body>, std::string);
};

Signature lookup(const FunctionTable&, const std::string&, const std::vector<Expression>&);
void insert(FunctionTable&, const Signature&, const std::optional<Body>& = std::nullopt);

struct Program {
  StatementList statements;
  SymbolTable table;
  TypeTable type_table;
  FunctionTable ftable;
};

std::string opkind_str(const Operation::Kind);
std::string typekind_str(const AST::Type::Kind);

// prints using a lisp-like syntax
// for ast validation. the goal is for a program
// to be mostly reconstructable from its printed form
std::string print(const Program&);
std::string print(const Body&);
std::string print(const TypeTable&);
std::string print(const SymbolTable&);
std::string print(const StatementList&);
std::string print(const Statement&);
std::string print(const WriteStmt&);
std::string print(const ReadStmt&);
std::string print(const Assignment&);
std::string print(const IfStatement&);
std::string print(const While&);
std::string print(const Repeat&);
std::string print(const For&);
std::string print(const std::vector<ElseIf>&);
std::string print(const Expression&);
std::string print(const Operation&);
std::string print(const Type&, bool = true);
std::string print(const Record&);
std::string print(const Array&);
std::string print(const LValue&);
std::string print(const Symbol&, bool = true);
std::string print(const Value&);
std::string print(const Signature&);
std::string print(const Call& call);
std::string print(const FunctionTable&);
std::string print(const std::vector<AST::Param>&);

std::string hash_signature(const Signature&);
std::string hash_call(const std::string&, const std::vector<Expression>&);
} // namespace AST
#endif // CPSL_AST_H
