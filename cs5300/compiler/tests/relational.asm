.data
str_11: .asciiz "1<>2="
str_10: .asciiz "2=2="
str_21: .asciiz "\nboolean\n\n"
str_15: .asciiz "a<b="
str_2: .asciiz "\n\n"
str_36: .asciiz "e<>f="
str_1: .asciiz "2<2="
str_0: .asciiz "1<2="
str_3: .asciiz "1<=2="
str_4: .asciiz "2<=2="
str_14: .asciiz "a<1="
str_6: .asciiz "2>2="
str_8: .asciiz "2>=2="
str_26: .asciiz "c>=d="
str_13: .asciiz "\ninteger\n\n"
str_5: .asciiz "1>2="
str_16: .asciiz "a<=b="
str_17: .asciiz "a>b="
str_18: .asciiz "a>=b="
str_9: .asciiz "1=2="
str_19: .asciiz "a=b="
str_20: .asciiz "a<>b="
str_29: .asciiz "\nchar\n\n"
str_34: .asciiz "e>=f="
str_35: .asciiz "e=f="
str_27: .asciiz "c=d="
str_7: .asciiz "1>=2="
str_23: .asciiz "c<d="
str_24: .asciiz "c<=d="
str_12: .asciiz "2<>2="
str_25: .asciiz "c>d="
str_30: .asciiz "e<'B'="
str_28: .asciiz "c<>d="
str_31: .asciiz "e<f="
str_22: .asciiz "c<true="
str_32: .asciiz "e<=f="
str_33: .asciiz "e>f="
.text
.globl main
main:
la $a0, str_0
li $v0, 4
syscall
li $a0, 1
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_1
li $v0, 4
syscall
li $a0, 0
li $v0, 1
syscall
la $a0, str_2
li $v0, 4
syscall
la $a0, str_3
li $v0, 4
syscall
li $a0, 1
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_4
li $v0, 4
syscall
li $a0, 1
li $v0, 1
syscall
la $a0, str_2
li $v0, 4
syscall
la $a0, str_5
li $v0, 4
syscall
li $a0, 0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_6
li $v0, 4
syscall
li $a0, 0
li $v0, 1
syscall
la $a0, str_2
li $v0, 4
syscall
la $a0, str_7
li $v0, 4
syscall
li $a0, 0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_8
li $v0, 4
syscall
li $a0, 1
li $v0, 1
syscall
la $a0, str_2
li $v0, 4
syscall
la $a0, str_9
li $v0, 4
syscall
li $a0, 0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_10
li $v0, 4
syscall
li $a0, 1
li $v0, 1
syscall
la $a0, str_2
li $v0, 4
syscall
la $a0, str_11
li $v0, 4
syscall
li $a0, 1
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_12
li $v0, 4
syscall
li $a0, 0
li $v0, 1
syscall
la $a0, str_2
li $v0, 4
syscall
la $a0, str_13
li $v0, 4
syscall
li $s0, 4
sw $s0, 12($gp)
li $s0, 5
sw $s0, 20($gp)
la $a0, str_14
li $v0, 4
syscall
lw $s0, 12($gp)
slti $s0, $s0, 1
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_15
li $v0, 4
syscall
lw $s0, 12($gp)
lw $s5, 20($gp)
slt $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_16
li $v0, 4
syscall
lw $s0, 12($gp)
lw $s5, 20($gp)
sle $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_17
li $v0, 4
syscall
lw $s0, 12($gp)
lw $s5, 20($gp)
sgt $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_18
li $v0, 4
syscall
lw $s0, 12($gp)
lw $s5, 20($gp)
sge $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_19
li $v0, 4
syscall
lw $s0, 12($gp)
lw $s5, 20($gp)
seq $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_20
li $v0, 4
syscall
lw $s0, 12($gp)
lw $s5, 20($gp)
sne $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_21
li $v0, 4
syscall
li $s0, 0
sw $s0, 4($gp)
li $s0, 1
sw $s0, 16($gp)
la $a0, str_22
li $v0, 4
syscall
lw $s0, 4($gp)
slti $s0, $s0, 1
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_23
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 16($gp)
slt $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_24
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 16($gp)
sle $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_25
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 16($gp)
sgt $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_26
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 16($gp)
sge $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_27
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 16($gp)
seq $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_28
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 16($gp)
sne $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_29
li $v0, 4
syscall
li $s0, 'A'
sw $s0, 0($gp)
li $s0, 'B'
sw $s0, 8($gp)
la $a0, str_30
li $v0, 4
syscall
lw $s0, 0($gp)
slti $s0, $s0, 'B'
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_31
li $v0, 4
syscall
lw $s0, 0($gp)
lw $s5, 8($gp)
slt $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_32
li $v0, 4
syscall
lw $s0, 0($gp)
lw $s5, 8($gp)
sle $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_33
li $v0, 4
syscall
lw $s0, 0($gp)
lw $s5, 8($gp)
sgt $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_34
li $v0, 4
syscall
lw $s0, 0($gp)
lw $s5, 8($gp)
sge $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_35
li $v0, 4
syscall
lw $s0, 0($gp)
lw $s5, 8($gp)
seq $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_36
li $v0, 4
syscall
lw $s0, 0($gp)
lw $s5, 8($gp)
sne $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $v0, 10
syscall