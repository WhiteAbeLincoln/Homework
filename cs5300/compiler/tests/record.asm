.data
.text
.globl main
main:
li $s0, 1
sw $s0, 0($gp)
la $s0, 4($gp)
addi $s0, $s0, 0
lw $s5, 0($gp)
sw $s5, 0($s0)
la $s4, 4($gp)
addi $s4, $s4, 0
lw $s5, 0($s4)
addi $s5, $s5, 0
li $s4, 8
mult $s5, $s4
mflo $s5
la $s4, 12($gp)
addi $s4, $s4, 4
la $t5, 0($s4)
add $s5, $s5, $t5
la $s0, 0($s5)
addi $s0, $s0, 0
li $s4, 42
sw $s4, 0($s0)
lw $s4, 176($gp)
addi $s4, $s4, 0
li $t5, 8
mult $s4, $t5
mflo $s4
la $t5, 96($gp)
add $s4, $s4, $t5
la $s0, 0($s4)
addi $s0, $s0, 4
lw $t2, 0($gp)
addi $t2, $t2, 0
li $t3, 8
mult $t2, $t3
mflo $t2
la $t3, 12($gp)
addi $t3, $t3, 4
la $s2, 0($t3)
add $t2, $t2, $s2
la $s3, 0($t2)
addi $s3, $s3, 0
lw $t5, 0($s3)
sw $t5, 0($s0)
li $a0, 42
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $v0, 10
syscall