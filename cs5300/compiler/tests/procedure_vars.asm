.data
str_0: .asciiz "Hi "
.text
.globl main
main:
li $s0, 0
la $s5, 0($gp)
add $s0, $s0, $s5
li $s5, 1
sw $s5, 0($s0)
li $s0, 4
la $s5, 0($gp)
add $s0, $s0, $s5
li $s5, 2
sw $s5, 0($s0)
addi $sp, $sp, -16
li $s0, 'A'
sw $s0, 0($sp)
li $s0, 1
sw $s0, 4($sp)
la $s0, 8($sp)
la $s5, 0($gp)
lw $s4, 0($s5)
sw $s4, 0($s0)
lw $s4, 4($s5)
sw $s4, 4($s0)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __sayHi0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 16
li $s0, 'B'
sw $s0, 8($gp)
addi $sp, $sp, -16
lw $s0, 8($gp)
sw $s0, 0($sp)
li $s0, 5
sw $s0, 4($sp)
la $s0, 8($sp)
la $s5, 0($gp)
lw $s4, 0($s5)
sw $s4, 0($s0)
lw $s4, 4($s5)
sw $s4, 4($s0)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __sayHi0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 16
li $v0, 10
syscall
__sayHi0__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -8
addi $sp, $sp, -4
sw $s4, 0($sp)
addi $sp, $sp, -4
sw $s5, 0($sp)
addi $sp, $sp, -4
sw $s0, 0($sp)
li $s0, 1
sw $s0, -4($fp)
li $s0, 1
sw $s0, -8($fp)
la $a0, str_0
li $v0, 4
syscall
lw $s0, 12($fp)
move $a0, $s0
li $v0, 1
syscall
li $a0, ' '
li $v0, 11
syscall
lw $s0, 8($fp)
move $a0, $s0
li $v0, 11
syscall
li $a0, ' '
li $v0, 11
syscall
lw $s0, -4($fp)
move $a0, $s0
li $v0, 1
syscall
li $s5, 0
la $s4, 16($fp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $s5, 4
la $s4, 16($fp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
__sayHi0__ret:
lw $s0, 0($sp)
addi $sp, $sp, 4
lw $s5, 0($sp)
addi $sp, $sp, 4
lw $s4, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 8
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra