'(Program (TypeTable 0 (Child (TypeTable 1 (Entries))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Entries (int (Symbol int (PType integer))) (char (Symbol char (PType char))) (bool (Symbol bool (PType boolean)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (FunctionTable (Entries)) (Scope 1 1 (StatementList (Statement (Assign (LVal (PType integer) (Symbol int)) (ConstExpr (Value 1)))) (Statement (Assign (LVal (PType char) (Symbol char)) (ConstExpr (Value #\G)))) (Statement (Assign (LVal (PType boolean) (Symbol bool)) (ConstExpr (Value #t)))) (Statement (Write (ConstExpr (Value "int: ")) (Expr (LVal (PType integer) (Symbol int))) (ConstExpr (Value #\space)) (ConstExpr (Value "char: ")) (Expr (LVal (PType char) (Symbol char))) (ConstExpr (Value #\space)) (ConstExpr (Value "bool: ")) (Expr (LVal (PType boolean) (Symbol bool))) (ConstExpr (Value #\newline)))) (Statement (Write (ConstExpr (Value "(succ . pred) ")) (ConstExpr (Value "int: ")) (Expr (Op SUCC (PType integer) (Expr (Op PRED (PType integer) (Expr (LVal (PType integer) (Symbol int))))))) (ConstExpr (Value #\newline)))) (Statement (Write (ConstExpr (Value "(succ . pred) ")) (ConstExpr (Value "char: ")) (Expr (Op SUCC (PType char) (Expr (Op PRED (PType char) (Expr (LVal (PType char) (Symbol char))))))) (ConstExpr (Value #\newline)))) (Statement (Write (ConstExpr (Value "(succ . pred) ")) (ConstExpr (Value "bool: ")) (Expr (Op SUCC (PType boolean) (Expr (Op PRED (PType boolean) (Expr (LVal (PType boolean) (Symbol bool))))))) (ConstExpr (Value #\newline)))))))
'(Program
  (TypeTable
   0
   (Child
    (TypeTable
     1
     (Entries)))
   (Entries
    (integer
     (PType
      integer))
    (INTEGER
     (PType
      integer))
    (boolean
     (PType
      boolean))
    (BOOLEAN
     (PType
      boolean))
    (char
     (PType char))
    (CHAR
     (PType char))
    (string
     (PType string))
    (STRING
     (PType
      string))))
  (SymbolTable
   0
   (Child
    (SymbolTable
     1
     (Entries
      (int
       (Symbol
        int
        (PType
         integer)))
      (char
       (Symbol
        char
        (PType
         char)))
      (bool
       (Symbol
        bool
        (PType
         boolean))))))
   (Entries
    (false
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (FALSE
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (true
     (Symbol
      true
      (PType
       boolean)
      (Value #t)))))
  (FunctionTable
   (Entries))
  (Scope
   1
   1
   (StatementList
    (Statement
     (Assign
      (LVal
       (PType
        integer)
       (Symbol int))
      (ConstExpr
       (Value 1))))
    (Statement
     (Assign
      (LVal
       (PType char)
       (Symbol
        char))
      (ConstExpr
       (Value
        #\G))))
    (Statement
     (Assign
      (LVal
       (PType
        boolean)
       (Symbol
        bool))
      (ConstExpr
       (Value #t))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "int: "))
      (Expr
       (LVal
        (PType
         integer)
        (Symbol
         int)))
      (ConstExpr
       (Value
        #\space))
      (ConstExpr
       (Value
        "char: "))
      (Expr
       (LVal
        (PType char)
        (Symbol
         char)))
      (ConstExpr
       (Value
        #\space))
      (ConstExpr
       (Value
        "bool: "))
      (Expr
       (LVal
        (PType
         boolean)
        (Symbol
         bool)))
      (ConstExpr
       (Value
        #\newline))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "(succ . pred) "))
      (ConstExpr
       (Value
        "int: "))
      (Expr
       (Op
        SUCC
        (PType
         integer)
        (Expr
         (Op
          PRED
          (PType
           integer)
          (Expr
           (LVal
            (PType
             integer)
            (Symbol
             int)))))))
      (ConstExpr
       (Value
        #\newline))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "(succ . pred) "))
      (ConstExpr
       (Value
        "char: "))
      (Expr
       (Op
        SUCC
        (PType char)
        (Expr
         (Op
          PRED
          (PType
           char)
          (Expr
           (LVal
            (PType
             char)
            (Symbol
             char)))))))
      (ConstExpr
       (Value
        #\newline))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "(succ . pred) "))
      (ConstExpr
       (Value
        "bool: "))
      (Expr
       (Op
        SUCC
        (PType
         boolean)
        (Expr
         (Op
          PRED
          (PType
           boolean)
          (Expr
           (LVal
            (PType
             boolean)
            (Symbol
             bool)))))))
      (ConstExpr
       (Value
        #\newline)))))))
