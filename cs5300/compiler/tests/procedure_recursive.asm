.data
.text
.globl main
main:
addi $sp, $sp, -4
li $s0, 10
sw $s0, 0($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __countdown0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 4
li $v0, 10
syscall
__countdown0__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
sw $s5, 0($sp)
addi $sp, $sp, -4
sw $s0, 0($sp)
lw $s0, 8($fp)
li $s5, 0
sle $s0, $s0, $s5
beq $s0, $zero, b_else1
j __countdown0__ret
j b_end0
b_else1:
b_end0:
lw $s0, 8($fp)
move $a0, $s0
li $v0, 1
syscall
li $a0, ' '
li $v0, 11
syscall
addi $sp, $sp, -4
lw $s0, 8($fp)
addi $s0, $s0, -1
sw $s0, 0($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __countdown0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 4
__countdown0__ret:
lw $s0, 0($sp)
addi $sp, $sp, 4
lw $s5, 0($sp)
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra