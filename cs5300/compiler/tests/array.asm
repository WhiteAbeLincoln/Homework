.data
.text
.globl main
main:
li $s0, 4
la $s5, 12($gp)
add $s0, $s0, $s5
li $s5, 42
sw $s5, 0($s0)
li $s5, 4
la $s4, 12($gp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $s0, 4
sw $s0, 36($gp)
lw $s5, 36($gp)
addi $s5, $s5, 1
addi $s5, $s5, -4
li $s4, 4
mult $s5, $s4
mflo $s5
la $s4, 12($gp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $s0, 'A'
sw $s0, 40($gp)
lw $s0, 40($gp)
addi $s0, $s0, -65
li $s5, 4
mult $s0, $s5
mflo $s0
la $s5, 0($gp)
add $s0, $s0, $s5
li $s5, 42
sw $s5, 0($s0)
li $s5, 0
la $s4, 0($gp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
lw $s5, 40($gp)
addi $s5, $s5, -65
li $s4, 4
mult $s5, $s4
mflo $s5
la $s4, 0($gp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $v0, 10
syscall