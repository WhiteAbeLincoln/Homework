'(Program (TypeTable 0 (Child (TypeTable 1 (Entries))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Entries (a (Symbol a (PType integer))) (b (Symbol b (PType char)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (FunctionTable (Entries)) (Scope 1 1 (StatementList (Statement (Write (ConstExpr (Value "provide int a: ")))) (Statement (Read (LVal (PType integer) (Symbol a)))) (Statement (Write (ConstExpr (Value "\ngot int a: ")) (Expr (LVal (PType integer) (Symbol a))))) (Statement (Write (ConstExpr (Value "\nprovide char b: ")))) (Statement (Read (LVal (PType char) (Symbol b)))) (Statement (Write (ConstExpr (Value "\ngot char b: ")) (Expr (LVal (PType char) (Symbol b))))))))
'(Program
  (TypeTable
   0
   (Child
    (TypeTable
     1
     (Entries)))
   (Entries
    (integer
     (PType
      integer))
    (INTEGER
     (PType
      integer))
    (boolean
     (PType
      boolean))
    (BOOLEAN
     (PType
      boolean))
    (char
     (PType char))
    (CHAR
     (PType char))
    (string
     (PType string))
    (STRING
     (PType
      string))))
  (SymbolTable
   0
   (Child
    (SymbolTable
     1
     (Entries
      (a
       (Symbol
        a
        (PType
         integer)))
      (b
       (Symbol
        b
        (PType
         char))))))
   (Entries
    (false
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (FALSE
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (true
     (Symbol
      true
      (PType
       boolean)
      (Value #t)))))
  (FunctionTable
   (Entries))
  (Scope
   1
   1
   (StatementList
    (Statement
     (Write
      (ConstExpr
       (Value
        "provide int a: "))))
    (Statement
     (Read
      (LVal
       (PType
        integer)
       (Symbol a))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "\ngot int a: "))
      (Expr
       (LVal
        (PType
         integer)
        (Symbol
         a)))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "\nprovide char b: "))))
    (Statement
     (Read
      (LVal
       (PType char)
       (Symbol b))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "\ngot char b: "))
      (Expr
       (LVal
        (PType char)
        (Symbol
         b))))))))
