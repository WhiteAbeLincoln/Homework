.data
str_0: .asciiz "Testing This"
.text
.globl main
main:
li $s0, 42
sw $s0, 20($gp)
li $s0, 'A'
sw $s0, 16($gp)
li $s0, 1
sw $s0, 28($gp)
la $s0, str_0
sw $s0, 36($gp)
li $s0, 0
la $s5, 40($gp)
add $s0, $s0, $s5
lw $s5, 20($gp)
sw $s5, 0($s0)
li $s0, 0
la $s5, 44($gp)
add $s0, $s0, $s5
lw $s5, 16($gp)
sw $s5, 0($s0)
li $s0, 0
la $s5, 12($gp)
add $s0, $s0, $s5
lw $s5, 28($gp)
sw $s5, 0($s0)
li $s0, 0
la $s5, 8($gp)
add $s0, $s0, $s5
lw $s5, 36($gp)
sw $s5, 0($s0)
la $s0, 4($gp)
addi $s0, $s0, 0
li $s4, 0
la $t5, 40($gp)
add $s4, $s4, $t5
lw $s5, 0($s4)
sw $s5, 0($s0)
la $s0, 24($gp)
addi $s0, $s0, 0
li $s4, 0
la $t5, 44($gp)
add $s4, $s4, $t5
lw $s5, 0($s4)
sw $s5, 0($s0)
la $s0, 0($gp)
addi $s0, $s0, 0
li $s4, 0
la $t5, 12($gp)
add $s4, $s4, $t5
lw $s5, 0($s4)
sw $s5, 0($s0)
la $s0, 32($gp)
addi $s0, $s0, 0
li $s4, 0
la $t5, 8($gp)
add $s4, $s4, $t5
lw $s5, 0($s4)
sw $s5, 0($s0)
lw $s0, 20($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, ' '
li $v0, 11
syscall
li $s5, 0
la $s4, 40($gp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, ' '
li $v0, 11
syscall
la $s5, 4($gp)
addi $s5, $s5, 0
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
lw $s0, 16($gp)
move $a0, $s0
li $v0, 11
syscall
li $a0, ' '
li $v0, 11
syscall
li $s5, 0
la $s4, 44($gp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 11
syscall
li $a0, ' '
li $v0, 11
syscall
la $s5, 24($gp)
addi $s5, $s5, 0
lw $s0, 0($s5)
move $a0, $s0
li $v0, 11
syscall
li $a0, '\n'
li $v0, 11
syscall
lw $s0, 28($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, ' '
li $v0, 11
syscall
li $s5, 0
la $s4, 12($gp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, ' '
li $v0, 11
syscall
la $s5, 0($gp)
addi $s5, $s5, 0
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
lw $s0, 36($gp)
move $a0, $s0
li $v0, 4
syscall
li $a0, ' '
li $v0, 11
syscall
li $s5, 0
la $s4, 8($gp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 4
syscall
li $a0, ' '
li $v0, 11
syscall
la $s5, 32($gp)
addi $s5, $s5, 0
lw $s0, 0($s5)
move $a0, $s0
li $v0, 4
syscall
li $a0, '\n'
li $v0, 11
syscall
li $v0, 10
syscall