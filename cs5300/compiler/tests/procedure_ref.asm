.data
str_1: .asciiz "Before "
str_0: .asciiz "After "
.text
.globl main
main:
li $s0, 0
la $s5, 0($gp)
add $s0, $s0, $s5
li $s5, 1
sw $s5, 0($s0)
addi $sp, $sp, -4
la $s0, 0($gp)
sw $s0, 0($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __sayHi0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 4
la $a0, str_0
li $v0, 4
syscall
li $s5, 0
la $s4, 0($gp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $v0, 10
syscall
__sayHi0__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
sw $s4, 0($sp)
addi $sp, $sp, -4
sw $s5, 0($sp)
addi $sp, $sp, -4
sw $s0, 0($sp)
la $a0, str_1
li $v0, 4
syscall
li $s5, 0
lw $s4, 8($fp)
la $t5, 0($s4)
add $s5, $s5, $t5
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $s0, 0
lw $s5, 8($fp)
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 5
sw $s5, 0($s0)
__sayHi0__ret:
lw $s0, 0($sp)
addi $sp, $sp, 4
lw $s5, 0($sp)
addi $sp, $sp, 4
lw $s4, 0($sp)
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra