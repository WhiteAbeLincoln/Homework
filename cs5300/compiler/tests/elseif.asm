.data
str_7: .asciiz "else\n"
str_6: .asciiz "elseif\n"
str_1: .asciiz "you mean _if_ he gets out\n"
str_3: .asciiz "not reached "
str_0: .asciiz "hes not going to be happy when he gets out of there\n"
str_2: .asciiz "if, if is good\n"
str_4: .asciiz "else const\n"
str_5: .asciiz "non-const\n"
.text
.globl main
main:
la $a0, str_0
li $v0, 4
syscall
la $a0, str_1
li $v0, 4
syscall
la $a0, str_2
li $v0, 4
syscall
j branch1
la $a0, str_3
li $v0, 4
syscall
la $a0, str_4
li $v0, 4
syscall
branch1:
li $s0, 1
sw $s0, 0($gp)
lw $s0, 0($gp)
not $s0, $s0
andi $s0, $s0, 1
beq $s0, $zero, b_else3
la $a0, str_3
li $v0, 4
syscall
la $a0, str_5
li $v0, 4
syscall
j b_end2
b_else3:
lw $s0, 0($gp)
not $s0, $s0
andi $s0, $s0, 1
beq $s0, $zero, b_elif4
la $a0, str_3
li $v0, 4
syscall
la $a0, str_6
li $v0, 4
syscall
j b_end2
b_elif4:
lw $s0, 0($gp)
beq $s0, $zero, b_elif5
la $a0, str_0
li $v0, 4
syscall
la $a0, str_1
li $v0, 4
syscall
la $a0, str_2
li $v0, 4
syscall
j b_end2
b_elif5:
la $a0, str_3
li $v0, 4
syscall
la $a0, str_7
li $v0, 4
syscall
b_end2:
li $v0, 10
syscall