.data
str_0: .asciiz "The sum of the array is: "
.text
.globl main
main:
li $s0, 1
sw $s0, 48($gp)
loop_start0:
lw $s0, 48($gp)
li $s5, 12
sle $s0, $s0, $s5
beq $s0, $zero, loop_end1
lw $s0, 48($gp)
addi $s0, $s0, -1
li $s5, 4
mult $s0, $s5
mflo $s0
la $s5, 0($gp)
add $s0, $s0, $s5
lw $s5, 48($gp)
addi $s5, $s5, 5
sw $s5, 0($s0)
lw $s0, 48($gp)
addi $s0, $s0, 1
sw $s0, 48($gp)
j loop_start0
loop_end1:
la $a0, str_0
li $v0, 4
syscall
addi $sp, $sp, -48
la $s0, 0($sp)
la $s5, 0($gp)
lw $s4, 0($s5)
sw $s4, 0($s0)
lw $s4, 4($s5)
sw $s4, 4($s0)
lw $s4, 8($s5)
sw $s4, 8($s0)
lw $s4, 12($s5)
sw $s4, 12($s0)
lw $s4, 16($s5)
sw $s4, 16($s0)
lw $s4, 20($s5)
sw $s4, 20($s0)
lw $s4, 24($s5)
sw $s4, 24($s0)
lw $s4, 28($s5)
sw $s4, 28($s0)
lw $s4, 32($s5)
sw $s4, 32($s0)
lw $s4, 36($s5)
sw $s4, 36($s0)
lw $s4, 40($s5)
sw $s4, 40($s0)
lw $s4, 44($s5)
sw $s4, 44($s0)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __sum0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 48
move $a0, $v0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $v0, 10
syscall
__sum0__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -8
addi $sp, $sp, -4
sw $s4, 0($sp)
addi $sp, $sp, -4
sw $s5, 0($sp)
addi $sp, $sp, -4
sw $s0, 0($sp)
li $s0, 0
sw $s0, -8($fp)
li $s0, 1
sw $s0, -4($fp)
loop_start2:
lw $s0, -4($fp)
li $s5, 12
sle $s0, $s0, $s5
beq $s0, $zero, loop_end3
lw $s0, -8($fp)
lw $s4, -4($fp)
addi $s4, $s4, -1
li $t5, 4
mult $s4, $t5
mflo $s4
la $t5, 8($fp)
add $s4, $s4, $t5
lw $s5, 0($s4)
add $s0, $s0, $s5
sw $s0, -8($fp)
lw $s0, -4($fp)
addi $s0, $s0, 1
sw $s0, -4($fp)
j loop_start2
loop_end3:
lw $s0, -8($fp)
move $v0, $s0
j __sum0__ret
__sum0__ret:
lw $s0, 0($sp)
addi $sp, $sp, 4
lw $s5, 0($sp)
addi $sp, $sp, 4
lw $s4, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 8
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra