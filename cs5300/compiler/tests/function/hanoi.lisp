'(Program (TypeTable 0 (Child (TypeTable 1 (Child (TypeTable 3 (Entries))) (Entries))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Child (SymbolTable 3 (Child (SymbolTable 5 (Entries))) (Entries (disk (Symbol disk (PType integer))) (source (Symbol source (PType char))) (dest (Symbol dest (PType char))) (spare (Symbol spare (PType char)))))) (Entries (disks (Symbol disks (PType integer)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (FunctionTable (Entries ((Sig moveTower (Type void) (Param disk REF (PType integer)) (Param source REF (PType char)) (Param dest REF (PType char)) (Param spare REF (PType char))) (Body (TypeTable 0 (Child (TypeTable 1 (Child (TypeTable 3 (Entries))) (Entries))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Child (SymbolTable 3 (Child (SymbolTable 5 (Entries))) (Entries (disk (Symbol disk (PType integer))) (source (Symbol source (PType char))) (dest (Symbol dest (PType char))) (spare (Symbol spare (PType char)))))) (Entries (disks (Symbol disks (PType integer)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (Scope 3 3 (StatementList (Statement (If (Expr (Op EQ (PType boolean) (Expr (LVal (PType integer) (Symbol disk))) (ConstExpr (Value 1)))) (StatementList (Statement (Write (ConstExpr (Value "Move ")) (Expr (LVal (PType integer) (Symbol disk))) (ConstExpr (Value " from ")) (Expr (LVal (PType char) (Symbol source))) (ConstExpr (Value " to ")) (Expr (LVal (PType char) (Symbol dest))) (ConstExpr (Value #\newline))))) (ElseIfs) (StatementList (Statement (Call moveTower (Expr (Op SUB (PType integer) (Expr (LVal (PType integer) (Symbol disk))) (ConstExpr (Value 1)))) (Expr (LVal (PType char) (Symbol source))) (Expr (LVal (PType char) (Symbol spare))) (Expr (LVal (PType char) (Symbol dest))))) (Statement (Write (ConstExpr (Value "Move ")) (Expr (LVal (PType integer) (Symbol disk))) (ConstExpr (Value " from ")) (Expr (LVal (PType char) (Symbol source))) (ConstExpr (Value " to ")) (Expr (LVal (PType char) (Symbol dest))) (ConstExpr (Value #\newline)))) (Statement (Call moveTower (Expr (Op SUB (PType integer) (Expr (LVal (PType integer) (Symbol disk))) (ConstExpr (Value 1)))) (Expr (LVal (PType char) (Symbol spare))) (Expr (LVal (PType char) (Symbol dest))) (Expr (LVal (PType char) (Symbol source)))))))))))))) (Scope 1 1 (StatementList (Statement (Write (ConstExpr (Value "Enter number of disks:")))) (Statement (Read (LVal (PType integer) (Symbol disks)))) (Statement (Write (ConstExpr (Value "Moving ")) (Expr (LVal (PType integer) (Symbol disks))) (ConstExpr (Value " disks from tower A to tower B with tower C helping\n")))) (Statement (Call moveTower (Expr (LVal (PType integer) (Symbol disks))) (ConstExpr (Value #\A)) (ConstExpr (Value #\B)) (ConstExpr (Value #\C)))) (Statement (Write (ConstExpr (Value "Done\n")))))))
'(Program
  (TypeTable
   0
   (Child
    (TypeTable
     1
     (Child
      (TypeTable
       3
       (Entries)))
     (Entries)))
   (Entries
    (integer
     (PType
      integer))
    (INTEGER
     (PType
      integer))
    (boolean
     (PType
      boolean))
    (BOOLEAN
     (PType
      boolean))
    (char
     (PType char))
    (CHAR
     (PType char))
    (string
     (PType string))
    (STRING
     (PType
      string))))
  (SymbolTable
   0
   (Child
    (SymbolTable
     1
     (Child
      (SymbolTable
       3
       (Child
        (SymbolTable
         5
         (Entries)))
       (Entries
        (disk
         (Symbol
          disk
          (PType
           integer)))
        (source
         (Symbol
          source
          (PType
           char)))
        (dest
         (Symbol
          dest
          (PType
           char)))
        (spare
         (Symbol
          spare
          (PType
           char))))))
     (Entries
      (disks
       (Symbol
        disks
        (PType
         integer))))))
   (Entries
    (false
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (FALSE
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (true
     (Symbol
      true
      (PType
       boolean)
      (Value #t)))))
  (FunctionTable
   (Entries
    ((Sig
      moveTower
      (Type void)
      (Param
       disk
       REF
       (PType
        integer))
      (Param
       source
       REF
       (PType char))
      (Param
       dest
       REF
       (PType char))
      (Param
       spare
       REF
       (PType
        char)))
     (Body
      (TypeTable
       0
       (Child
        (TypeTable
         1
         (Child
          (TypeTable
           3
           (Entries)))
         (Entries)))
       (Entries
        (integer
         (PType
          integer))
        (INTEGER
         (PType
          integer))
        (boolean
         (PType
          boolean))
        (BOOLEAN
         (PType
          boolean))
        (char
         (PType
          char))
        (CHAR
         (PType
          char))
        (string
         (PType
          string))
        (STRING
         (PType
          string))))
      (SymbolTable
       0
       (Child
        (SymbolTable
         1
         (Child
          (SymbolTable
           3
           (Child
            (SymbolTable
             5
             (Entries)))
           (Entries
            (disk
             (Symbol
              disk
              (PType
               integer)))
            (source
             (Symbol
              source
              (PType
               char)))
            (dest
             (Symbol
              dest
              (PType
               char)))
            (spare
             (Symbol
              spare
              (PType
               char))))))
         (Entries
          (disks
           (Symbol
            disks
            (PType
             integer))))))
       (Entries
        (false
         (Symbol
          false
          (PType
           boolean)
          (Value
           #f)))
        (FALSE
         (Symbol
          false
          (PType
           boolean)
          (Value
           #f)))
        (true
         (Symbol
          true
          (PType
           boolean)
          (Value
           #t)))))
      (Scope
       3
       3
       (StatementList
        (Statement
         (If
          (Expr
           (Op
            EQ
            (PType
             boolean)
            (Expr
             (LVal
              (PType
               integer)
              (Symbol
               disk)))
            (ConstExpr
             (Value
              1))))
          (StatementList
           (Statement
            (Write
             (ConstExpr
              (Value
               "Move "))
             (Expr
              (LVal
               (PType
                integer)
               (Symbol
                disk)))
             (ConstExpr
              (Value
               " from "))
             (Expr
              (LVal
               (PType
                char)
               (Symbol
                source)))
             (ConstExpr
              (Value
               " to "))
             (Expr
              (LVal
               (PType
                char)
               (Symbol
                dest)))
             (ConstExpr
              (Value
               #\newline)))))
          (ElseIfs)
          (StatementList
           (Statement
            (Call
             moveTower
             (Expr
              (Op
               SUB
               (PType
                integer)
               (Expr
                (LVal
                 (PType
                  integer)
                 (Symbol
                  disk)))
               (ConstExpr
                (Value
                 1))))
             (Expr
              (LVal
               (PType
                char)
               (Symbol
                source)))
             (Expr
              (LVal
               (PType
                char)
               (Symbol
                spare)))
             (Expr
              (LVal
               (PType
                char)
               (Symbol
                dest)))))
           (Statement
            (Write
             (ConstExpr
              (Value
               "Move "))
             (Expr
              (LVal
               (PType
                integer)
               (Symbol
                disk)))
             (ConstExpr
              (Value
               " from "))
             (Expr
              (LVal
               (PType
                char)
               (Symbol
                source)))
             (ConstExpr
              (Value
               " to "))
             (Expr
              (LVal
               (PType
                char)
               (Symbol
                dest)))
             (ConstExpr
              (Value
               #\newline))))
           (Statement
            (Call
             moveTower
             (Expr
              (Op
               SUB
               (PType
                integer)
               (Expr
                (LVal
                 (PType
                  integer)
                 (Symbol
                  disk)))
               (ConstExpr
                (Value
                 1))))
             (Expr
              (LVal
               (PType
                char)
               (Symbol
                spare)))
             (Expr
              (LVal
               (PType
                char)
               (Symbol
                dest)))
             (Expr
              (LVal
               (PType
                char)
               (Symbol
                source))))))))))))))
  (Scope
   1
   1
   (StatementList
    (Statement
     (Write
      (ConstExpr
       (Value
        "Enter number of disks:"))))
    (Statement
     (Read
      (LVal
       (PType
        integer)
       (Symbol
        disks))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "Moving "))
      (Expr
       (LVal
        (PType
         integer)
        (Symbol
         disks)))
      (ConstExpr
       (Value
        " disks from tower A to tower B with tower C helping\n"))))
    (Statement
     (Call
      moveTower
      (Expr
       (LVal
        (PType
         integer)
        (Symbol
         disks)))
      (ConstExpr
       (Value #\A))
      (ConstExpr
       (Value #\B))
      (ConstExpr
       (Value
        #\C))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "Done\n")))))))
