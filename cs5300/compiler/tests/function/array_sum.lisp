'(Program (TypeTable 0 (Child (TypeTable 1 (Child (TypeTable 3 (Entries))) (Entries (nums (Type (Array 1 12 (PType integer))))))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Child (SymbolTable 6 (Child (SymbolTable 7 (Entries))) (Entries))) (Entries (NUMS_LENGTH (Symbol NUMS_LENGTH (PType integer) (Value 12))) (x (Symbol x (PType integer))) (arr (Symbol arr (Type nums)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (FunctionTable (Entries ((Sig sum (PType integer) (Param a REF (Type nums))) (Body (TypeTable 0 (Child (TypeTable 1 (Child (TypeTable 3 (Entries))) (Entries (nums (Type (Array 1 12 (PType integer))))))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Child (SymbolTable 6 (Child (SymbolTable 7 (Entries))) (Entries))) (Entries (NUMS_LENGTH (Symbol NUMS_LENGTH (PType integer) (Value 12))) (x (Symbol x (PType integer))) (arr (Symbol arr (Type nums)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (Scope 3 3 (StatementList (Statement (Assign (LVal (PType integer) (Symbol total)) (ConstExpr (Value 0)))) (Statement (For (Assign (LVal (PType integer) (Symbol i)) (ConstExpr (Value 1))) TO (ConstExpr (Value 12)) (StatementList (Statement (Assign (LVal (PType integer) (Symbol total)) (Expr (Op ADD (PType integer) (Expr (LVal (PType integer) (Symbol total))) (Expr (LVal (PType integer) (LVal (Type nums) (Symbol a)) (At (Expr (LVal (PType integer) (Symbol i))))))))))))) (Statement (Return (Expr (LVal (PType integer) (Symbol total))))))))))) (Scope 1 1 (StatementList (Statement (For (Assign (LVal (PType integer) (Symbol x)) (ConstExpr (Value 1))) TO (ConstExpr (Value 12)) (StatementList (Statement (Assign (LVal (PType integer) (LVal (Type nums) (Symbol arr)) (At (Expr (LVal (PType integer) (Symbol x))))) (Expr (Op ADD (PType integer) (ConstExpr (Value 5)) (Expr (LVal (PType integer) (Symbol x)))))))))) (Statement (Write (ConstExpr (Value "The sum of the array is: ")) (Expr (Call sum (Expr (LVal (Type nums) (Symbol arr))))) (ConstExpr (Value #\newline)))))))
'(Program
  (TypeTable
   0
   (Child
    (TypeTable
     1
     (Child
      (TypeTable
       3
       (Entries)))
     (Entries
      (nums
       (Type
        (Array
         1
         12
         (PType
          integer)))))))
   (Entries
    (integer
     (PType
      integer))
    (INTEGER
     (PType
      integer))
    (boolean
     (PType
      boolean))
    (BOOLEAN
     (PType
      boolean))
    (char
     (PType char))
    (CHAR
     (PType char))
    (string
     (PType string))
    (STRING
     (PType
      string))))
  (SymbolTable
   0
   (Child
    (SymbolTable
     1
     (Child
      (SymbolTable
       6
       (Child
        (SymbolTable
         7
         (Entries)))
       (Entries)))
     (Entries
      (NUMS_LENGTH
       (Symbol
        NUMS_LENGTH
        (PType
         integer)
        (Value 12)))
      (x
       (Symbol
        x
        (PType
         integer)))
      (arr
       (Symbol
        arr
        (Type
         nums))))))
   (Entries
    (false
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (FALSE
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (true
     (Symbol
      true
      (PType
       boolean)
      (Value #t)))))
  (FunctionTable
   (Entries
    ((Sig
      sum
      (PType
       integer)
      (Param
       a
       REF
       (Type nums)))
     (Body
      (TypeTable
       0
       (Child
        (TypeTable
         1
         (Child
          (TypeTable
           3
           (Entries)))
         (Entries
          (nums
           (Type
            (Array
             1
             12
             (PType
              integer)))))))
       (Entries
        (integer
         (PType
          integer))
        (INTEGER
         (PType
          integer))
        (boolean
         (PType
          boolean))
        (BOOLEAN
         (PType
          boolean))
        (char
         (PType
          char))
        (CHAR
         (PType
          char))
        (string
         (PType
          string))
        (STRING
         (PType
          string))))
      (SymbolTable
       0
       (Child
        (SymbolTable
         1
         (Child
          (SymbolTable
           6
           (Child
            (SymbolTable
             7
             (Entries)))
           (Entries)))
         (Entries
          (NUMS_LENGTH
           (Symbol
            NUMS_LENGTH
            (PType
             integer)
            (Value
             12)))
          (x
           (Symbol
            x
            (PType
             integer)))
          (arr
           (Symbol
            arr
            (Type
             nums))))))
       (Entries
        (false
         (Symbol
          false
          (PType
           boolean)
          (Value
           #f)))
        (FALSE
         (Symbol
          false
          (PType
           boolean)
          (Value
           #f)))
        (true
         (Symbol
          true
          (PType
           boolean)
          (Value
           #t)))))
      (Scope
       3
       3
       (StatementList
        (Statement
         (Assign
          (LVal
           (PType
            integer)
           (Symbol
            total))
          (ConstExpr
           (Value
            0))))
        (Statement
         (For
          (Assign
           (LVal
            (PType
             integer)
            (Symbol
             i))
           (ConstExpr
            (Value
             1)))
          TO
          (ConstExpr
           (Value
            12))
          (StatementList
           (Statement
            (Assign
             (LVal
              (PType
               integer)
              (Symbol
               total))
             (Expr
              (Op
               ADD
               (PType
                integer)
               (Expr
                (LVal
                 (PType
                  integer)
                 (Symbol
                  total)))
               (Expr
                (LVal
                 (PType
                  integer)
                 (LVal
                  (Type
                   nums)
                  (Symbol
                   a))
                 (At
                  (Expr
                   (LVal
                    (PType
                     integer)
                    (Symbol
                     i)))))))))))))
        (Statement
         (Return
          (Expr
           (LVal
            (PType
             integer)
            (Symbol
             total)))))))))))
  (Scope
   1
   1
   (StatementList
    (Statement
     (For
      (Assign
       (LVal
        (PType
         integer)
        (Symbol x))
       (ConstExpr
        (Value 1)))
      TO
      (ConstExpr
       (Value 12))
      (StatementList
       (Statement
        (Assign
         (LVal
          (PType
           integer)
          (LVal
           (Type
            nums)
           (Symbol
            arr))
          (At
           (Expr
            (LVal
             (PType
              integer)
             (Symbol
              x)))))
         (Expr
          (Op
           ADD
           (PType
            integer)
           (ConstExpr
            (Value
             5))
           (Expr
            (LVal
             (PType
              integer)
             (Symbol
              x))))))))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "The sum of the array is: "))
      (Expr
       (Call
        sum
        (Expr
         (LVal
          (Type
           nums)
          (Symbol
           arr)))))
      (ConstExpr
       (Value
        #\newline)))))))
