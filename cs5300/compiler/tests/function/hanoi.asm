.data
str_1: .asciiz "Moving "
str_6: .asciiz " to "
str_4: .asciiz "Move "
str_0: .asciiz "Enter number of disks:"
str_2: .asciiz " disks from tower A to tower B with tower C helping\n"
str_3: .asciiz "Done\n"
str_5: .asciiz " from "
.text
.globl main
main:
la $a0, str_0
li $v0, 4
syscall
li $v0, 5
syscall
sw $v0, 0($gp)
la $a0, str_1
li $v0, 4
syscall
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
la $a0, str_2
li $v0, 4
syscall
addi $sp, $sp, -16
lw $s0, 0($gp)
sw $s0, 0($sp)
li $s0, 'A'
sw $s0, 4($sp)
li $s0, 'B'
sw $s0, 8($sp)
li $s0, 'C'
sw $s0, 12($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __moveTower0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 16
la $a0, str_3
li $v0, 4
syscall
li $v0, 10
syscall
__moveTower0__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
sw $s5, 0($sp)
addi $sp, $sp, -4
sw $s0, 0($sp)
lw $s0, 8($fp)
li $s5, 1
seq $s0, $s0, $s5
beq $s0, $zero, b_else1
la $a0, str_4
li $v0, 4
syscall
lw $s0, 8($fp)
move $a0, $s0
li $v0, 1
syscall
la $a0, str_5
li $v0, 4
syscall
lw $s0, 12($fp)
move $a0, $s0
li $v0, 11
syscall
la $a0, str_6
li $v0, 4
syscall
lw $s0, 16($fp)
move $a0, $s0
li $v0, 11
syscall
li $a0, '\n'
li $v0, 11
syscall
j b_end0
b_else1:
addi $sp, $sp, -16
lw $s0, 8($fp)
addi $s0, $s0, -1
sw $s0, 0($sp)
lw $s0, 12($fp)
sw $s0, 4($sp)
lw $s0, 20($fp)
sw $s0, 8($sp)
lw $s0, 16($fp)
sw $s0, 12($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __moveTower0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 16
la $a0, str_4
li $v0, 4
syscall
lw $s0, 8($fp)
move $a0, $s0
li $v0, 1
syscall
la $a0, str_5
li $v0, 4
syscall
lw $s0, 12($fp)
move $a0, $s0
li $v0, 11
syscall
la $a0, str_6
li $v0, 4
syscall
lw $s0, 16($fp)
move $a0, $s0
li $v0, 11
syscall
li $a0, '\n'
li $v0, 11
syscall
addi $sp, $sp, -16
lw $s0, 8($fp)
addi $s0, $s0, -1
sw $s0, 0($sp)
lw $s0, 20($fp)
sw $s0, 4($sp)
lw $s0, 16($fp)
sw $s0, 8($sp)
lw $s0, 12($fp)
sw $s0, 12($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __moveTower0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 16
b_end0:
__moveTower0__ret:
lw $s0, 0($sp)
addi $sp, $sp, 4
lw $s5, 0($sp)
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra