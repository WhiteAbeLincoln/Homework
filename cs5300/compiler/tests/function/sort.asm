.data
str_1: .asciiz "\nSorted: "
str_0: .asciiz "Random: "
.text
.globl main
main:
addi $sp, $sp, -404
la $s0, 0($gp)
sw $s0, 0($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __rfill2__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 404
la $a0, str_0
li $v0, 4
syscall
addi $sp, $sp, -404
la $s0, 0($gp)
sw $s0, 0($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __print1__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 404
li $a0, '\n'
li $v0, 11
syscall
addi $sp, $sp, -412
la $s0, 0($gp)
sw $s0, 0($sp)
li $s0, 0
sw $s0, 404($sp)
li $s0, 100
sw $s0, 408($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __sort4__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 412
la $a0, str_1
li $v0, 4
syscall
addi $sp, $sp, -404
la $s0, 0($gp)
sw $s0, 0($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __print1__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 404
li $a0, '\n'
li $v0, 11
syscall
li $v0, 10
syscall
__print1__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
addi $sp, $sp, -4
sw $s4, 0($sp)
addi $sp, $sp, -4
sw $s5, 0($sp)
addi $sp, $sp, -4
sw $s0, 0($sp)
li $s0, 0
sw $s0, -4($fp)
loop_start0:
lw $s0, -4($fp)
li $s5, 99
sle $s0, $s0, $s5
beq $s0, $zero, loop_end1
lw $s5, -4($fp)
addi $s5, $s5, 0
li $s4, 4
mult $s5, $s4
mflo $s5
lw $s4, 8($fp)
la $t5, 0($s4)
add $s5, $s5, $t5
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, ' '
li $v0, 11
syscall
lw $s0, -4($fp)
addi $s0, $s0, 1
sw $s0, -4($fp)
j loop_start0
loop_end1:
li $s5, 400
lw $s4, 8($fp)
la $t5, 0($s4)
add $s5, $s5, $t5
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
__print1__ret:
lw $s0, 0($sp)
addi $sp, $sp, 4
lw $s5, 0($sp)
addi $sp, $sp, 4
lw $s4, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra
__rand0__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
addi $sp, $sp, -4
sw $s3, 0($sp)
li $t5, 8121
lw $s3, 404($gp)
mult $t5, $s3
mflo $t5
addi $t5, $t5, 28411
sw $t5, 404($gp)
lw $t5, 404($gp)
li $s3, 134456
div $t5, $s3
mflo $t5
sw $t5, -4($fp)
lw $t5, 404($gp)
li $s3, 134456
div $t5, $s3
mfhi $t5
sw $t5, 404($gp)
lw $t5, -4($fp)
move $v0, $t5
j __rand0__ret
__rand0__ret:
lw $s3, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra
__rfill2__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
li $t5, 0
sw $t5, -4($fp)
loop_start2:
lw $t5, -4($fp)
li $t2, 100
sle $t5, $t5, $t2
beq $t5, $zero, loop_end3
lw $t5, -4($fp)
addi $t5, $t5, 0
li $t2, 4
mult $t5, $t2
mflo $t5
lw $t2, 8($fp)
la $t3, 0($t2)
add $t5, $t5, $t3
addi $sp, $sp, -4
sw $t5, 0($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __rand0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 0
lw $t5, 0($sp)
addi $sp, $sp, 4
li $t2, 1000
div $v0, $t2
mfhi $v0
sw $v0, 0($t5)
lw $t5, -4($fp)
addi $t5, $t5, 1
sw $t5, -4($fp)
j loop_start2
loop_end3:
__rfill2__ret:
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra
__sort4__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -8
addi $sp, $sp, -4
sw $s2, 0($sp)
li $t5, 0
sw $t5, -4($fp)
loop_start4:
lw $t5, -4($fp)
li $t2, 100
sle $t5, $t5, $t2
beq $t5, $zero, loop_end5
lw $t5, -4($fp)
addi $t5, $t5, 1
sw $t5, -8($fp)
loop_start6:
lw $t5, -8($fp)
li $t2, 100
sle $t5, $t5, $t2
beq $t5, $zero, loop_end7
lw $t2, -4($fp)
addi $t2, $t2, 0
li $t3, 4
mult $t2, $t3
mflo $t2
lw $t3, 8($fp)
la $s2, 0($t3)
add $t2, $t2, $s2
lw $t5, 0($t2)
lw $t3, -8($fp)
addi $t3, $t3, 0
li $s2, 4
mult $t3, $s2
mflo $t3
lw $s2, 8($fp)
la $t0, 0($s2)
add $t3, $t3, $t0
lw $t2, 0($t3)
sgt $t5, $t5, $t2
beq $t5, $zero, b_else9
addi $sp, $sp, -8
lw $t5, -4($fp)
addi $t5, $t5, 0
li $t2, 4
mult $t5, $t2
mflo $t5
lw $t2, 8($fp)
la $t3, 0($t2)
add $t5, $t5, $t3
la $t2, 0($t5)
sw $t2, 0($sp)
lw $t5, -8($fp)
addi $t5, $t5, 0
li $t2, 4
mult $t5, $t2
mflo $t5
lw $t2, 8($fp)
la $t3, 0($t2)
add $t5, $t5, $t3
la $t2, 0($t5)
sw $t2, 4($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __swap3__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 8
j b_end8
b_else9:
b_end8:
lw $t5, -8($fp)
addi $t5, $t5, 1
sw $t5, -8($fp)
j loop_start6
loop_end7:
lw $t5, -4($fp)
addi $t5, $t5, 1
sw $t5, -4($fp)
j loop_start4
loop_end5:
__sort4__ret:
lw $s2, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 8
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra
__swap3__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
lw $t2, 8($fp)
lw $t5, 0($t2)
sw $t5, -4($fp)
lw $t5, 8($fp)
lw $t2, 12($fp)
lw $t3, 0($t2)
sw $t3, 0($t5)
lw $t5, 12($fp)
lw $t2, -4($fp)
sw $t2, 0($t5)
__swap3__ret:
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra