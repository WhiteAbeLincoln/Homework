.data
.text
.globl main
main:
li $s0, 1
sw $s0, 4($gp)
li $s0, 0
sw $s0, 0($gp)
lw $s0, 4($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, ' '
li $v0, 11
syscall
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
addi $sp, $sp, -8
la $s0, 4($gp)
sw $s0, 0($sp)
la $s0, 0($gp)
sw $s0, 4($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __swap0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 8
lw $s0, 4($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, ' '
li $v0, 11
syscall
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $v0, 10
syscall
__swap0__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
addi $sp, $sp, -4
sw $s4, 0($sp)
addi $sp, $sp, -4
sw $s5, 0($sp)
addi $sp, $sp, -4
sw $s0, 0($sp)
lw $s5, 8($fp)
lw $s0, 0($s5)
sw $s0, -4($fp)
lw $s0, 8($fp)
lw $s5, 12($fp)
lw $s4, 0($s5)
sw $s4, 0($s0)
lw $s0, 12($fp)
lw $s5, -4($fp)
sw $s5, 0($s0)
__swap0__ret:
lw $s0, 0($sp)
addi $sp, $sp, 4
lw $s5, 0($sp)
addi $sp, $sp, 4
lw $s4, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra