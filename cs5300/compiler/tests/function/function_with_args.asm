.data
str_3: .asciiz "4 + 5: "
str_2: .asciiz "a + b: "
str_0: .asciiz "a: "
str_1: .asciiz "b: "
.text
.globl main
main:
li $s0, 1
sw $s0, 4($gp)
li $s0, 2
sw $s0, 0($gp)
la $a0, str_0
li $v0, 4
syscall
lw $s0, 4($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_1
li $v0, 4
syscall
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_2
li $v0, 4
syscall
addi $sp, $sp, -8
lw $s0, 4($gp)
sw $s0, 0($sp)
lw $s0, 0($gp)
sw $s0, 4($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __add0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 8
move $a0, $v0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_3
li $v0, 4
syscall
addi $sp, $sp, -8
li $s0, 4
sw $s0, 0($sp)
li $s0, 5
sw $s0, 4($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __add0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 8
move $a0, $v0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_0
li $v0, 4
syscall
lw $s0, 4($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_1
li $v0, 4
syscall
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $v0, 10
syscall
__add0__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
sw $s5, 0($sp)
addi $sp, $sp, -4
sw $s0, 0($sp)
lw $s0, 8($fp)
lw $s5, 12($fp)
add $s0, $s0, $s5
move $v0, $s0
j __add0__ret
__add0__ret:
lw $s0, 0($sp)
addi $sp, $sp, 4
lw $s5, 0($sp)
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra