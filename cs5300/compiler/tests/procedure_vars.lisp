'(Program (TypeTable 0 (Child (TypeTable 1 (Child (TypeTable 3 (Entries))) (Entries (arr (Type (Array 0 1 (PType integer))))))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Child (SymbolTable 3 (Entries (c (Symbol c (PType char))) (d (Symbol d (PType integer))) (a (Symbol a (Type arr))) (x (Symbol x (PType integer))) (y (Symbol y (PType integer)))))) (Entries (c (Symbol c (PType char))) (arr (Symbol arr (Type arr)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (FunctionTable (Entries ((Sig sayHi (Type void) (Param c REF (PType char)) (Param d REF (PType integer)) (Param a REF (Type arr))) (Body (TypeTable 0 (Child (TypeTable 1 (Child (TypeTable 3 (Entries))) (Entries (arr (Type (Array 0 1 (PType integer))))))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Child (SymbolTable 3 (Entries (c (Symbol c (PType char))) (d (Symbol d (PType integer))) (a (Symbol a (Type arr))) (x (Symbol x (PType integer))) (y (Symbol y (PType integer)))))) (Entries (c (Symbol c (PType char))) (arr (Symbol arr (Type arr)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (Scope 3 3 (StatementList (Statement (Assign (LVal (PType integer) (Symbol x)) (ConstExpr (Value 1)))) (Statement (Assign (LVal (PType integer) (Symbol y)) (ConstExpr (Value 1)))) (Statement (Write (ConstExpr (Value "Hi ")) (Expr (LVal (PType integer) (Symbol d))) (ConstExpr (Value #\space)) (Expr (LVal (PType char) (Symbol c))) (ConstExpr (Value #\space)) (Expr (LVal (PType integer) (Symbol x))) (Expr (LVal (PType integer) (LVal (Type arr) (Symbol a)) (At (ConstExpr (Value 0))))) (Expr (LVal (PType integer) (LVal (Type arr) (Symbol a)) (At (ConstExpr (Value 1))))) (ConstExpr (Value #\newline)))))))))) (Scope 1 1 (StatementList (Statement (Assign (LVal (PType integer) (LVal (Type arr) (Symbol arr)) (At (ConstExpr (Value 0)))) (ConstExpr (Value 1)))) (Statement (Assign (LVal (PType integer) (LVal (Type arr) (Symbol arr)) (At (ConstExpr (Value 1)))) (ConstExpr (Value 2)))) (Statement (Call sayHi (ConstExpr (Value #\A)) (ConstExpr (Value 1)) (Expr (LVal (Type arr) (Symbol arr))))) (Statement (Assign (LVal (PType char) (Symbol c)) (ConstExpr (Value #\B)))) (Statement (Call sayHi (Expr (LVal (PType char) (Symbol c))) (ConstExpr (Value 5)) (Expr (LVal (Type arr) (Symbol arr))))))))
'(Program
  (TypeTable
   0
   (Child
    (TypeTable
     1
     (Child
      (TypeTable
       3
       (Entries)))
     (Entries
      (arr
       (Type
        (Array
         0
         1
         (PType
          integer)))))))
   (Entries
    (integer
     (PType
      integer))
    (INTEGER
     (PType
      integer))
    (boolean
     (PType
      boolean))
    (BOOLEAN
     (PType
      boolean))
    (char
     (PType char))
    (CHAR
     (PType char))
    (string
     (PType string))
    (STRING
     (PType
      string))))
  (SymbolTable
   0
   (Child
    (SymbolTable
     1
     (Child
      (SymbolTable
       3
       (Entries
        (c
         (Symbol
          c
          (PType
           char)))
        (d
         (Symbol
          d
          (PType
           integer)))
        (a
         (Symbol
          a
          (Type
           arr)))
        (x
         (Symbol
          x
          (PType
           integer)))
        (y
         (Symbol
          y
          (PType
           integer))))))
     (Entries
      (c
       (Symbol
        c
        (PType
         char)))
      (arr
       (Symbol
        arr
        (Type
         arr))))))
   (Entries
    (false
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (FALSE
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (true
     (Symbol
      true
      (PType
       boolean)
      (Value #t)))))
  (FunctionTable
   (Entries
    ((Sig
      sayHi
      (Type void)
      (Param
       c
       REF
       (PType char))
      (Param
       d
       REF
       (PType
        integer))
      (Param
       a
       REF
       (Type arr)))
     (Body
      (TypeTable
       0
       (Child
        (TypeTable
         1
         (Child
          (TypeTable
           3
           (Entries)))
         (Entries
          (arr
           (Type
            (Array
             0
             1
             (PType
              integer)))))))
       (Entries
        (integer
         (PType
          integer))
        (INTEGER
         (PType
          integer))
        (boolean
         (PType
          boolean))
        (BOOLEAN
         (PType
          boolean))
        (char
         (PType
          char))
        (CHAR
         (PType
          char))
        (string
         (PType
          string))
        (STRING
         (PType
          string))))
      (SymbolTable
       0
       (Child
        (SymbolTable
         1
         (Child
          (SymbolTable
           3
           (Entries
            (c
             (Symbol
              c
              (PType
               char)))
            (d
             (Symbol
              d
              (PType
               integer)))
            (a
             (Symbol
              a
              (Type
               arr)))
            (x
             (Symbol
              x
              (PType
               integer)))
            (y
             (Symbol
              y
              (PType
               integer))))))
         (Entries
          (c
           (Symbol
            c
            (PType
             char)))
          (arr
           (Symbol
            arr
            (Type
             arr))))))
       (Entries
        (false
         (Symbol
          false
          (PType
           boolean)
          (Value
           #f)))
        (FALSE
         (Symbol
          false
          (PType
           boolean)
          (Value
           #f)))
        (true
         (Symbol
          true
          (PType
           boolean)
          (Value
           #t)))))
      (Scope
       3
       3
       (StatementList
        (Statement
         (Assign
          (LVal
           (PType
            integer)
           (Symbol
            x))
          (ConstExpr
           (Value
            1))))
        (Statement
         (Assign
          (LVal
           (PType
            integer)
           (Symbol
            y))
          (ConstExpr
           (Value
            1))))
        (Statement
         (Write
          (ConstExpr
           (Value
            "Hi "))
          (Expr
           (LVal
            (PType
             integer)
            (Symbol
             d)))
          (ConstExpr
           (Value
            #\space))
          (Expr
           (LVal
            (PType
             char)
            (Symbol
             c)))
          (ConstExpr
           (Value
            #\space))
          (Expr
           (LVal
            (PType
             integer)
            (Symbol
             x)))
          (Expr
           (LVal
            (PType
             integer)
            (LVal
             (Type
              arr)
             (Symbol
              a))
            (At
             (ConstExpr
              (Value
               0)))))
          (Expr
           (LVal
            (PType
             integer)
            (LVal
             (Type
              arr)
             (Symbol
              a))
            (At
             (ConstExpr
              (Value
               1)))))
          (ConstExpr
           (Value
            #\newline))))))))))
  (Scope
   1
   1
   (StatementList
    (Statement
     (Assign
      (LVal
       (PType
        integer)
       (LVal
        (Type arr)
        (Symbol
         arr))
       (At
        (ConstExpr
         (Value
          0))))
      (ConstExpr
       (Value 1))))
    (Statement
     (Assign
      (LVal
       (PType
        integer)
       (LVal
        (Type arr)
        (Symbol
         arr))
       (At
        (ConstExpr
         (Value
          1))))
      (ConstExpr
       (Value 2))))
    (Statement
     (Call
      sayHi
      (ConstExpr
       (Value #\A))
      (ConstExpr
       (Value 1))
      (Expr
       (LVal
        (Type arr)
        (Symbol
         arr)))))
    (Statement
     (Assign
      (LVal
       (PType char)
       (Symbol c))
      (ConstExpr
       (Value
        #\B))))
    (Statement
     (Call
      sayHi
      (Expr
       (LVal
        (PType char)
        (Symbol c)))
      (ConstExpr
       (Value 5))
      (Expr
       (LVal
        (Type arr)
        (Symbol
         arr))))))))
