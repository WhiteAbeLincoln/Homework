'(Program (TypeTable 0 (Child (TypeTable 1 (Entries))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Entries (True (Symbol True (PType boolean))) (False (Symbol False (PType boolean)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (FunctionTable (Entries)) (Scope 1 1 (StatementList (Statement (Assign (LVal (PType boolean) (Symbol True)) (ConstExpr (Value #f)))) (Statement (Assign (LVal (PType boolean) (Symbol False)) (ConstExpr (Value #t)))) (Statement (Write (ConstExpr (Value "True = ")) (Expr (LVal (PType boolean) (Symbol True))) (ConstExpr (Value #\newline)))) (Statement (Write (ConstExpr (Value "False = ")) (Expr (LVal (PType boolean) (Symbol False))) (ConstExpr (Value #\newline)))))))
'(Program
  (TypeTable
   0
   (Child
    (TypeTable
     1
     (Entries)))
   (Entries
    (integer
     (PType
      integer))
    (INTEGER
     (PType
      integer))
    (boolean
     (PType
      boolean))
    (BOOLEAN
     (PType
      boolean))
    (char
     (PType char))
    (CHAR
     (PType char))
    (string
     (PType string))
    (STRING
     (PType
      string))))
  (SymbolTable
   0
   (Child
    (SymbolTable
     1
     (Entries
      (True
       (Symbol
        True
        (PType
         boolean)))
      (False
       (Symbol
        False
        (PType
         boolean))))))
   (Entries
    (false
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (FALSE
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (true
     (Symbol
      true
      (PType
       boolean)
      (Value #t)))))
  (FunctionTable
   (Entries))
  (Scope
   1
   1
   (StatementList
    (Statement
     (Assign
      (LVal
       (PType
        boolean)
       (Symbol
        True))
      (ConstExpr
       (Value #f))))
    (Statement
     (Assign
      (LVal
       (PType
        boolean)
       (Symbol
        False))
      (ConstExpr
       (Value #t))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "True = "))
      (Expr
       (LVal
        (PType
         boolean)
        (Symbol
         True)))
      (ConstExpr
       (Value
        #\newline))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "False = "))
      (Expr
       (LVal
        (PType
         boolean)
        (Symbol
         False)))
      (ConstExpr
       (Value
        #\newline)))))))
