'(Program (TypeTable 0 (Child (TypeTable 1 (Entries))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Child (SymbolTable 9 (Entries))) (Entries (x (Symbol x (PType boolean)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (FunctionTable (Entries)) (Scope 1 1 (StatementList (Statement (If (ConstExpr (Value #f)) (StatementList (Statement (Write (ConstExpr (Value "not reached ")) (ConstExpr (Value "const\n"))))) (ElseIfs (If (ConstExpr (Value #t)) (StatementList (Statement (Write (ConstExpr (Value "hes not going to be happy when he gets out of there\n")))) (Statement (Write (ConstExpr (Value "you mean _if_ he gets out\n")))) (Statement (Write (ConstExpr (Value "if, if is good\n"))))))) (StatementList (Statement (Write (ConstExpr (Value "not reached ")) (ConstExpr (Value "else const\n"))))))) (Statement (Assign (LVal (PType boolean) (Symbol x)) (ConstExpr (Value #t)))) (Statement (If (Expr (Op NOT (PType boolean) (Expr (LVal (PType boolean) (Symbol x))))) (StatementList (Statement (Write (ConstExpr (Value "not reached ")) (ConstExpr (Value "non-const\n"))))) (ElseIfs (If (Expr (Op NOT (PType boolean) (Expr (LVal (PType boolean) (Symbol x))))) (StatementList (Statement (Write (ConstExpr (Value "not reached ")) (ConstExpr (Value "elseif\n")))))) (If (Expr (LVal (PType boolean) (Symbol x))) (StatementList (Statement (Write (ConstExpr (Value "hes not going to be happy when he gets out of there\n")))) (Statement (Write (ConstExpr (Value "you mean _if_ he gets out\n")))) (Statement (Write (ConstExpr (Value "if, if is good\n"))))))) (StatementList (Statement (Write (ConstExpr (Value "not reached ")) (ConstExpr (Value "else\n"))))))))))
'(Program
  (TypeTable
   0
   (Child
    (TypeTable
     1
     (Entries)))
   (Entries
    (integer
     (PType
      integer))
    (INTEGER
     (PType
      integer))
    (boolean
     (PType
      boolean))
    (BOOLEAN
     (PType
      boolean))
    (char
     (PType char))
    (CHAR
     (PType char))
    (string
     (PType string))
    (STRING
     (PType
      string))))
  (SymbolTable
   0
   (Child
    (SymbolTable
     1
     (Child
      (SymbolTable
       9
       (Entries)))
     (Entries
      (x
       (Symbol
        x
        (PType
         boolean))))))
   (Entries
    (false
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (FALSE
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (true
     (Symbol
      true
      (PType
       boolean)
      (Value #t)))))
  (FunctionTable
   (Entries))
  (Scope
   1
   1
   (StatementList
    (Statement
     (If
      (ConstExpr
       (Value #f))
      (StatementList
       (Statement
        (Write
         (ConstExpr
          (Value
           "not reached "))
         (ConstExpr
          (Value
           "const\n")))))
      (ElseIfs
       (If
        (ConstExpr
         (Value #t))
        (StatementList
         (Statement
          (Write
           (ConstExpr
            (Value
             "hes not going to be happy when he gets out of there\n"))))
         (Statement
          (Write
           (ConstExpr
            (Value
             "you mean _if_ he gets out\n"))))
         (Statement
          (Write
           (ConstExpr
            (Value
             "if, if is good\n")))))))
      (StatementList
       (Statement
        (Write
         (ConstExpr
          (Value
           "not reached "))
         (ConstExpr
          (Value
           "else const\n")))))))
    (Statement
     (Assign
      (LVal
       (PType
        boolean)
       (Symbol x))
      (ConstExpr
       (Value #t))))
    (Statement
     (If
      (Expr
       (Op
        NOT
        (PType
         boolean)
        (Expr
         (LVal
          (PType
           boolean)
          (Symbol
           x)))))
      (StatementList
       (Statement
        (Write
         (ConstExpr
          (Value
           "not reached "))
         (ConstExpr
          (Value
           "non-const\n")))))
      (ElseIfs
       (If
        (Expr
         (Op
          NOT
          (PType
           boolean)
          (Expr
           (LVal
            (PType
             boolean)
            (Symbol
             x)))))
        (StatementList
         (Statement
          (Write
           (ConstExpr
            (Value
             "not reached "))
           (ConstExpr
            (Value
             "elseif\n"))))))
       (If
        (Expr
         (LVal
          (PType
           boolean)
          (Symbol
           x)))
        (StatementList
         (Statement
          (Write
           (ConstExpr
            (Value
             "hes not going to be happy when he gets out of there\n"))))
         (Statement
          (Write
           (ConstExpr
            (Value
             "you mean _if_ he gets out\n"))))
         (Statement
          (Write
           (ConstExpr
            (Value
             "if, if is good\n")))))))
      (StatementList
       (Statement
        (Write
         (ConstExpr
          (Value
           "not reached "))
         (ConstExpr
          (Value
           "else\n"))))))))))
