.data
str_1: .asciiz ") "
str_0: .asciiz "fact("
.text
.globl main
main:
li $s0, 5
sw $s0, 0($gp)
la $a0, str_0
li $v0, 4
syscall
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
la $a0, str_1
li $v0, 4
syscall
addi $sp, $sp, -4
lw $s0, 0($gp)
sw $s0, 0($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __fact0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 4
move $a0, $v0
li $v0, 1
syscall
li $v0, 10
syscall
__fact0__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
sw $s5, 0($sp)
addi $sp, $sp, -4
sw $s0, 0($sp)
lw $s0, 8($fp)
li $s5, 1
sle $s0, $s0, $s5
beq $s0, $zero, b_else1
lw $s0, 8($fp)
move $v0, $s0
j __fact0__ret
j b_end0
b_else1:
b_end0:
lw $s0, 8($fp)
addi $sp, $sp, -4
lw $s5, 8($fp)
addi $s5, $s5, -1
sw $s5, 0($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __fact0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 4
mult $s0, $v0
mflo $s0
move $v0, $s0
j __fact0__ret
__fact0__ret:
lw $s0, 0($sp)
addi $sp, $sp, 4
lw $s5, 0($sp)
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra