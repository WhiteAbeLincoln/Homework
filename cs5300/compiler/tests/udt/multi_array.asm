.data
.text
.globl main
main:
li $s0, 0
li $s5, 0
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 0
sw $s5, 0($s0)
li $s0, 4
li $s5, 0
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 0
sw $s5, 0($s0)
li $s0, 8
li $s5, 0
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 0
sw $s5, 0($s0)
li $s0, 12
li $s5, 0
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 0
sw $s5, 0($s0)
li $s0, 0
li $s5, 16
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 0
sw $s5, 0($s0)
li $s0, 4
li $s5, 16
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 1
sw $s5, 0($s0)
li $s0, 8
li $s5, 16
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 2
sw $s5, 0($s0)
li $s0, 12
li $s5, 16
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 3
sw $s5, 0($s0)
li $s0, 0
li $s5, 32
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 0
sw $s5, 0($s0)
li $s0, 4
li $s5, 32
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 2
sw $s5, 0($s0)
li $s0, 8
li $s5, 32
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 4
sw $s5, 0($s0)
li $s0, 12
li $s5, 32
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 6
sw $s5, 0($s0)
li $s0, 0
li $s5, 48
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 0
sw $s5, 0($s0)
li $s0, 4
li $s5, 48
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 3
sw $s5, 0($s0)
li $s0, 8
li $s5, 48
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 6
sw $s5, 0($s0)
li $s0, 12
li $s5, 48
la $s4, 24($gp)
add $s5, $s5, $s4
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 9
sw $s5, 0($s0)
li $s0, 2
sw $s0, 4($gp)
lw $s0, 4($gp)
addi $s0, $s0, 0
li $s5, 16
mult $s0, $s5
mflo $s0
la $s5, 24($gp)
add $s0, $s0, $s5
la $s5, 8($gp)
la $s4, 0($s0)
lw $t5, 0($s4)
sw $t5, 0($s5)
lw $t5, 4($s4)
sw $t5, 4($s5)
lw $t5, 8($s4)
sw $t5, 8($s5)
lw $t5, 12($s4)
sw $t5, 12($s5)
lw $s5, 4($gp)
addi $s5, $s5, 0
li $s4, 4
mult $s5, $s4
mflo $s5
la $s4, 8($gp)
add $s5, $s5, $s4
lw $s0, 0($s5)
sw $s0, 0($gp)
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
li $v0, 10
syscall