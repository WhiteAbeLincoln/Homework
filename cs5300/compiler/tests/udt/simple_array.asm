.data
.text
.globl main
main:
li $s0, 0
la $s5, 4($gp)
add $s0, $s0, $s5
li $s5, 40
sw $s5, 0($s0)
li $s0, 2
sw $s0, 0($gp)
lw $s0, 0($gp)
addi $s0, $s0, -1
li $s5, 4
mult $s0, $s5
mflo $s0
la $s5, 4($gp)
add $s0, $s0, $s5
lw $s5, 0($gp)
sw $s5, 0($s0)
li $s0, 8
la $s5, 4($gp)
add $s0, $s0, $s5
li $s4, 0
la $t5, 4($gp)
add $s4, $s4, $t5
lw $s5, 0($s4)
li $t5, 4
la $s3, 4($gp)
add $t5, $t5, $s3
lw $s4, 0($t5)
add $s5, $s5, $s4
sw $s5, 0($s0)
li $s5, 8
la $s4, 4($gp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $v0, 10
syscall