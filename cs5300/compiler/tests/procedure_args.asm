.data
str_0: .asciiz "Hi "
.text
.globl main
main:
addi $sp, $sp, -4
li $s0, 'A'
sw $s0, 0($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __sayHi0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 4
li $s0, 'B'
sw $s0, 0($gp)
addi $sp, $sp, -4
lw $s0, 0($gp)
sw $s0, 0($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __sayHi0__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 4
li $v0, 10
syscall
__sayHi0__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
sw $s0, 0($sp)
la $a0, str_0
li $v0, 4
syscall
lw $s0, 8($fp)
move $a0, $s0
li $v0, 11
syscall
li $a0, '\n'
li $v0, 11
syscall
__sayHi0__ret:
lw $s0, 0($sp)
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra