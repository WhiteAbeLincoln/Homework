.data
str_2: .asciiz "Hi1 "
str_0: .asciiz "After "
str_1: .asciiz "Hi2 "
.text
.globl main
main:
li $s0, 0
la $s5, 0($gp)
add $s0, $s0, $s5
li $s5, 1
sw $s5, 0($s0)
li $s0, 'B'
sw $s0, 4($gp)
addi $sp, $sp, -8
lw $s0, 4($gp)
sw $s0, 0($sp)
la $s0, 0($gp)
sw $s0, 4($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __hi11__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 8
la $a0, str_0
li $v0, 4
syscall
li $s5, 0
la $s4, 0($gp)
add $s5, $s5, $s4
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $v0, 10
syscall
__hi20__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
sw $s4, 0($sp)
addi $sp, $sp, -4
sw $s5, 0($sp)
addi $sp, $sp, -4
sw $s0, 0($sp)
la $a0, str_1
li $v0, 4
syscall
lw $s0, 8($fp)
move $a0, $s0
li $v0, 11
syscall
li $a0, ' '
li $v0, 11
syscall
li $s5, 0
lw $s4, 12($fp)
la $t5, 0($s4)
add $s5, $s5, $t5
lw $s0, 0($s5)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $s0, 0
lw $s5, 12($fp)
la $s4, 0($s5)
add $s0, $s0, $s4
li $s5, 3
sw $s5, 0($s0)
__hi20__ret:
lw $s0, 0($sp)
addi $sp, $sp, 4
lw $s5, 0($sp)
addi $sp, $sp, 4
lw $s4, 0($sp)
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra
__hi11__:
addi $sp, $sp, -4
sw $fp, 0($sp)
move $fp, $sp
addi $sp, $sp, -4
sw $s3, 0($sp)
la $a0, str_2
li $v0, 4
syscall
lw $t5, 8($fp)
move $a0, $t5
li $v0, 11
syscall
li $a0, ' '
li $v0, 11
syscall
li $s3, 0
lw $t2, 12($fp)
la $t3, 0($t2)
add $s3, $s3, $t3
lw $t5, 0($s3)
move $a0, $t5
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $t5, 0
lw $s3, 12($fp)
la $t2, 0($s3)
add $t5, $t5, $t2
li $s3, 2
sw $s3, 0($t5)
addi $sp, $sp, -8
lw $t5, 8($fp)
sw $t5, 0($sp)
lw $t5, 12($fp)
la $s3, 0($t5)
sw $s3, 4($sp)
addi $sp, $sp, -4
sw $ra, 0($sp)
jal __hi20__
lw $ra, 0($sp)
addi $sp, $sp, 4
addi $sp, $sp, 8
__hi11__ret:
lw $s3, 0($sp)
addi $sp, $sp, 4
move $sp, $fp
lw $fp, 0($sp)
addi $sp, $sp, 4
jr $ra