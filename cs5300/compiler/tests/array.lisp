'(Program (TypeTable 0 (Child (TypeTable 1 (Entries))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Entries (int (Symbol int (PType integer))) (c (Symbol c (PType char))) (arr (Symbol arr (Type (Array 4 9 (PType integer))))) (arr2 (Symbol arr2 (Type (Array 65 67 (PType integer)))))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (FunctionTable (Entries)) (Scope 1 1 (StatementList (Statement (Assign (LVal (PType integer) (LVal (Type (Array 4 9 (PType integer))) (Symbol arr)) (At (ConstExpr (Value 5)))) (ConstExpr (Value 42)))) (Statement (Write (Expr (LVal (PType integer) (LVal (Type (Array 4 9 (PType integer))) (Symbol arr)) (At (ConstExpr (Value 5))))) (ConstExpr (Value #\newline)))) (Statement (Assign (LVal (PType integer) (Symbol int)) (ConstExpr (Value 4)))) (Statement (Write (Expr (LVal (PType integer) (LVal (Type (Array 4 9 (PType integer))) (Symbol arr)) (At (Expr (Op ADD (PType integer) (Expr (LVal (PType integer) (Symbol int))) (ConstExpr (Value 1))))))) (ConstExpr (Value #\newline)))) (Statement (Assign (LVal (PType char) (Symbol c)) (ConstExpr (Value #\A)))) (Statement (Assign (LVal (PType integer) (LVal (Type (Array 65 67 (PType integer))) (Symbol arr2)) (At (Expr (LVal (PType char) (Symbol c))))) (ConstExpr (Value 42)))) (Statement (Write (Expr (LVal (PType integer) (LVal (Type (Array 65 67 (PType integer))) (Symbol arr2)) (At (ConstExpr (Value #\A))))) (Expr (LVal (PType integer) (LVal (Type (Array 65 67 (PType integer))) (Symbol arr2)) (At (Expr (LVal (PType char) (Symbol c)))))) (ConstExpr (Value #\newline)))))))
'(Program
  (TypeTable
   0
   (Child
    (TypeTable
     1
     (Entries)))
   (Entries
    (integer
     (PType
      integer))
    (INTEGER
     (PType
      integer))
    (boolean
     (PType
      boolean))
    (BOOLEAN
     (PType
      boolean))
    (char
     (PType char))
    (CHAR
     (PType char))
    (string
     (PType string))
    (STRING
     (PType
      string))))
  (SymbolTable
   0
   (Child
    (SymbolTable
     1
     (Entries
      (int
       (Symbol
        int
        (PType
         integer)))
      (c
       (Symbol
        c
        (PType
         char)))
      (arr
       (Symbol
        arr
        (Type
         (Array
          4
          9
          (PType
           integer)))))
      (arr2
       (Symbol
        arr2
        (Type
         (Array
          65
          67
          (PType
           integer))))))))
   (Entries
    (false
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (FALSE
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (true
     (Symbol
      true
      (PType
       boolean)
      (Value #t)))))
  (FunctionTable
   (Entries))
  (Scope
   1
   1
   (StatementList
    (Statement
     (Assign
      (LVal
       (PType
        integer)
       (LVal
        (Type
         (Array
          4
          9
          (PType
           integer)))
        (Symbol
         arr))
       (At
        (ConstExpr
         (Value
          5))))
      (ConstExpr
       (Value 42))))
    (Statement
     (Write
      (Expr
       (LVal
        (PType
         integer)
        (LVal
         (Type
          (Array
           4
           9
           (PType
            integer)))
         (Symbol
          arr))
        (At
         (ConstExpr
          (Value
           5)))))
      (ConstExpr
       (Value
        #\newline))))
    (Statement
     (Assign
      (LVal
       (PType
        integer)
       (Symbol int))
      (ConstExpr
       (Value 4))))
    (Statement
     (Write
      (Expr
       (LVal
        (PType
         integer)
        (LVal
         (Type
          (Array
           4
           9
           (PType
            integer)))
         (Symbol
          arr))
        (At
         (Expr
          (Op
           ADD
           (PType
            integer)
           (Expr
            (LVal
             (PType
              integer)
             (Symbol
              int)))
           (ConstExpr
            (Value
             1)))))))
      (ConstExpr
       (Value
        #\newline))))
    (Statement
     (Assign
      (LVal
       (PType char)
       (Symbol c))
      (ConstExpr
       (Value
        #\A))))
    (Statement
     (Assign
      (LVal
       (PType
        integer)
       (LVal
        (Type
         (Array
          65
          67
          (PType
           integer)))
        (Symbol
         arr2))
       (At
        (Expr
         (LVal
          (PType
           char)
          (Symbol
           c)))))
      (ConstExpr
       (Value 42))))
    (Statement
     (Write
      (Expr
       (LVal
        (PType
         integer)
        (LVal
         (Type
          (Array
           65
           67
           (PType
            integer)))
         (Symbol
          arr2))
        (At
         (ConstExpr
          (Value
           #\A)))))
      (Expr
       (LVal
        (PType
         integer)
        (LVal
         (Type
          (Array
           65
           67
           (PType
            integer)))
         (Symbol
          arr2))
        (At
         (Expr
          (LVal
           (PType
            char)
           (Symbol
            c))))))
      (ConstExpr
       (Value
        #\newline)))))))
