'(Program (TypeTable 0 (Child (TypeTable 1 (Entries (point (Type (Record (y (Symbol y)) (x (Symbol x))))) (a (Type (Array 0 9 (Type point)))) (foo (Type (Record (x (Symbol x)) (a (Symbol a)))))))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Entries (p (Symbol p (Type point))) (x (Symbol x (PType integer))) (y (Symbol y (PType integer))) (a (Symbol a (Type a))) (f (Symbol f (Type foo)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (FunctionTable (Entries)) (Scope 1 1 (StatementList (Statement (Assign (LVal (PType integer) (Symbol y)) (ConstExpr (Value 1)))) (Statement (Assign (LVal (PType integer) (LVal (Type point) (Symbol p))) (Expr (LVal (PType integer) (Symbol y))))) (Statement (Assign (LVal (PType integer) (LVal (Type point) (LVal (Type a) (LVal (Type foo) (Symbol f))) (At (Expr (LVal (PType integer) (LVal (Type point) (Symbol p))))))) (ConstExpr (Value 42)))) (Statement (Assign (LVal (PType integer) (LVal (Type point) (LVal (Type a) (Symbol a)) (At (Expr (LVal (PType integer) (Symbol x)))))) (Expr (LVal (PType integer) (LVal (Type point) (LVal (Type a) (LVal (Type foo) (Symbol f))) (At (Expr (LVal (PType integer) (Symbol y))))))))) (Statement (Write (ConstExpr (Value 42)) (ConstExpr (Value #\newline)))))))
'(Program
  (TypeTable
   0
   (Child
    (TypeTable
     1
     (Entries
      (point
       (Type
        (Record
         (y
          (Symbol
           y))
         (x
          (Symbol
           x)))))
      (a
       (Type
        (Array
         0
         9
         (Type
          point))))
      (foo
       (Type
        (Record
         (x
          (Symbol
           x))
         (a
          (Symbol
           a))))))))
   (Entries
    (integer
     (PType
      integer))
    (INTEGER
     (PType
      integer))
    (boolean
     (PType
      boolean))
    (BOOLEAN
     (PType
      boolean))
    (char
     (PType char))
    (CHAR
     (PType char))
    (string
     (PType string))
    (STRING
     (PType
      string))))
  (SymbolTable
   0
   (Child
    (SymbolTable
     1
     (Entries
      (p
       (Symbol
        p
        (Type
         point)))
      (x
       (Symbol
        x
        (PType
         integer)))
      (y
       (Symbol
        y
        (PType
         integer)))
      (a
       (Symbol
        a
        (Type a)))
      (f
       (Symbol
        f
        (Type
         foo))))))
   (Entries
    (false
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (FALSE
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (true
     (Symbol
      true
      (PType
       boolean)
      (Value #t)))))
  (FunctionTable
   (Entries))
  (Scope
   1
   1
   (StatementList
    (Statement
     (Assign
      (LVal
       (PType
        integer)
       (Symbol y))
      (ConstExpr
       (Value 1))))
    (Statement
     (Assign
      (LVal
       (PType
        integer)
       (LVal
        (Type point)
        (Symbol p)))
      (Expr
       (LVal
        (PType
         integer)
        (Symbol
         y)))))
    (Statement
     (Assign
      (LVal
       (PType
        integer)
       (LVal
        (Type point)
        (LVal
         (Type a)
         (LVal
          (Type foo)
          (Symbol
           f)))
        (At
         (Expr
          (LVal
           (PType
            integer)
           (LVal
            (Type
             point)
            (Symbol
             p)))))))
      (ConstExpr
       (Value 42))))
    (Statement
     (Assign
      (LVal
       (PType
        integer)
       (LVal
        (Type point)
        (LVal
         (Type a)
         (Symbol a))
        (At
         (Expr
          (LVal
           (PType
            integer)
           (Symbol
            x))))))
      (Expr
       (LVal
        (PType
         integer)
        (LVal
         (Type
          point)
         (LVal
          (Type a)
          (LVal
           (Type
            foo)
           (Symbol
            f)))
         (At
          (Expr
           (LVal
            (PType
             integer)
            (Symbol
             y)))))))))
    (Statement
     (Write
      (ConstExpr
       (Value 42))
      (ConstExpr
       (Value
        #\newline)))))))
