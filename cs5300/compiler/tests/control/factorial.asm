.data
.text
.globl main
main:
li $s0, 1
sw $s0, 0($gp)
li $s0, 1
sw $s0, 4($gp)
loop_start0:
lw $s0, 4($gp)
li $s5, 8
sle $s0, $s0, $s5
beq $s0, $zero, loop_end1
lw $s0, 0($gp)
lw $s5, 4($gp)
mult $s0, $s5
mflo $s0
sw $s0, 0($gp)
lw $s0, 4($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\t'
li $v0, 11
syscall
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
lw $s0, 4($gp)
addi $s0, $s0, 1
sw $s0, 4($gp)
j loop_start0
loop_end1:
li $v0, 10
syscall