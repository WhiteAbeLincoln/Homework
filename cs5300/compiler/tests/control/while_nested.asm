.data
str_0: .asciiz ":"
.text
.globl main
main:
li $s0, 0
sw $s0, 4($gp)
li $s0, 0
sw $s0, 0($gp)
loop_start0:
lw $s0, 4($gp)
li $s5, 6
sle $s0, $s0, $s5
beq $s0, $zero, loop_end1
loop_start2:
lw $s0, 0($gp)
lw $s5, 4($gp)
sle $s0, $s0, $s5
beq $s0, $zero, loop_end3
lw $s0, 4($gp)
move $a0, $s0
li $v0, 1
syscall
la $a0, str_0
li $v0, 4
syscall
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
li $a0, ' '
li $v0, 11
syscall
lw $s0, 0($gp)
addi $s0, $s0, 1
sw $s0, 0($gp)
j loop_start2
loop_end3:
li $a0, '\n'
li $v0, 11
syscall
lw $s0, 4($gp)
addi $s0, $s0, 1
sw $s0, 4($gp)
li $s0, 0
sw $s0, 0($gp)
j loop_start0
loop_end1:
li $v0, 10
syscall