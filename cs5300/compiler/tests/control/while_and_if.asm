.data
.text
.globl main
main:
li $s0, 0
sw $s0, 0($gp)
loop_start0:
lw $s0, 0($gp)
slti $s0, $s0, 10
beq $s0, $zero, loop_end1
lw $s0, 0($gp)
li $s5, 2
div $s0, $s5
mfhi $s0
li $s5, 0
seq $s0, $s0, $s5
beq $s0, $zero, b_else3
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
j b_end2
b_else3:
b_end2:
lw $s0, 0($gp)
addi $s0, $s0, 1
sw $s0, 0($gp)
j loop_start0
loop_end1:
li $v0, 10
syscall