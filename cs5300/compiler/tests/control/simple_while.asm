.data
.text
.globl main
main:
li $s0, 0
sw $s0, 0($gp)
loop_start0:
lw $s0, 0($gp)
slti $s0, $s0, 10
beq $s0, $zero, loop_end1
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
lw $s0, 0($gp)
addi $s0, $s0, 1
sw $s0, 0($gp)
j loop_start0
loop_end1:
li $v0, 10
syscall