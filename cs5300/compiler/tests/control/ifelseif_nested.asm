.data
str_2: .asciiz "i is 5!"
str_3: .asciiz "i is greater than 5!"
str_0: .asciiz "i less than 5!\n"
str_1: .asciiz "j is 1\n"
.text
.globl main
main:
li $s0, 5
sw $s0, 4($gp)
li $s0, 1
sw $s0, 0($gp)
lw $s0, 4($gp)
slti $s0, $s0, 5
beq $s0, $zero, b_else1
la $a0, str_0
li $v0, 4
syscall
lw $s0, 0($gp)
li $s5, 1
seq $s0, $s0, $s5
beq $s0, $zero, b_else3
la $a0, str_1
li $v0, 4
syscall
j b_end2
b_else3:
b_end2:
j b_end0
b_else1:
lw $s0, 4($gp)
li $s5, 5
seq $s0, $s0, $s5
beq $s0, $zero, b_elif4
la $a0, str_2
li $v0, 4
syscall
j b_end0
b_elif4:
la $a0, str_3
li $v0, 4
syscall
b_end0:
li $v0, 10
syscall