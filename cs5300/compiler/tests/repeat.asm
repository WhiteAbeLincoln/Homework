.data
str_0: .asciiz "i: "
str_2: .asciiz "should only happen once "
str_1: .asciiz " \n"
.text
.globl main
main:
li $s0, 0
sw $s0, 0($gp)
branch0:
la $a0, str_0
li $v0, 4
syscall
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
la $a0, str_1
li $v0, 4
syscall
lw $s0, 0($gp)
addi $s0, $s0, 1
sw $s0, 0($gp)
lw $s0, 0($gp)
li $s5, 5
sgt $s0, $s0, $s5
beq $s0, $zero, branch0
branch1:
la $a0, str_2
li $v0, 4
syscall
lw $s0, 0($gp)
move $a0, $s0
li $v0, 1
syscall
la $a0, str_1
li $v0, 4
syscall
lw $s0, 0($gp)
addi $s0, $s0, 1
sw $s0, 0($gp)
li $v0, 10
syscall