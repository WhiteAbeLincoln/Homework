.data
str_16: .asciiz "a>=b="
str_4: .asciiz "Enter A:"
str_5: .asciiz "\nEnter b:"
str_0: .asciiz "2+2="
str_6: .asciiz "a+b="
str_2: .asciiz "2/2="
str_3: .asciiz "2-2="
str_1: .asciiz "2*2="
str_7: .asciiz "a-b="
str_8: .asciiz "a*b="
str_9: .asciiz "a/b="
str_10: .asciiz "a%b="
str_11: .asciiz "a=b="
str_12: .asciiz "a<b="
str_13: .asciiz "a>b="
str_14: .asciiz "a<>b="
str_15: .asciiz "a<=b="
.text
.globl main
main:
la $a0, str_0
li $v0, 4
syscall
li $a0, 4
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_1
li $v0, 4
syscall
li $a0, 4
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_2
li $v0, 4
syscall
li $a0, 1
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_3
li $v0, 4
syscall
li $a0, 0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_4
li $v0, 4
syscall
li $v0, 5
syscall
sw $v0, 4($gp)
la $a0, str_5
li $v0, 4
syscall
li $v0, 5
syscall
sw $v0, 0($gp)
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_6
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 0($gp)
add $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_7
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 0($gp)
sub $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_8
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 0($gp)
mult $s0, $s5
mflo $s0
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_9
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 0($gp)
div $s0, $s5
mflo $s0
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_10
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 0($gp)
div $s0, $s5
mfhi $s0
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_11
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 0($gp)
seq $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_12
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 0($gp)
slt $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_13
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 0($gp)
sgt $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_14
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 0($gp)
sne $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_15
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 0($gp)
sle $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
la $a0, str_16
li $v0, 4
syscall
lw $s0, 4($gp)
lw $s5, 0($gp)
sge $s0, $s0, $s5
move $a0, $s0
li $v0, 1
syscall
li $a0, '\n'
li $v0, 11
syscall
li $v0, 10
syscall