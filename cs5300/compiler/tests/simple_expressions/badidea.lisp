'(Program (TypeTable 0 (Child (TypeTable 1 (Entries (i (PType integer)) (c (PType char)) (integer (PType char)) (char (PType char))))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Entries (true (Symbol true (PType boolean) (Value #f))) (false (Symbol false (PType boolean) (Value #t))) (i (Symbol i (PType integer))) (c (Symbol c (PType char)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (FunctionTable (Entries)) (Scope 1 1 (StatementList (Statement (Write (ConstExpr (Value "false")) (ConstExpr (Value #t)) (ConstExpr (Value #f)) (ConstExpr (Value #\newline)))) (Statement (Write (ConstExpr (Value "true")) (ConstExpr (Value #f)) (ConstExpr (Value #t)) (ConstExpr (Value #\newline)))) (Statement (Assign (LVal (PType integer) (Symbol i)) (ConstExpr (Value 65)))) (Statement (Assign (LVal (PType char) (Symbol c)) (Expr (Op CHR (PType char) (Expr (LVal (PType integer) (Symbol i))))))) (Statement (Write (Expr (LVal (PType integer) (Symbol i))) (Expr (LVal (PType char) (Symbol c))) (ConstExpr (Value #\newline)))))))
'(Program
  (TypeTable
   0
   (Child
    (TypeTable
     1
     (Entries
      (i
       (PType
        integer))
      (c
       (PType char))
      (integer
       (PType char))
      (char
       (PType
        char)))))
   (Entries
    (integer
     (PType
      integer))
    (INTEGER
     (PType
      integer))
    (boolean
     (PType
      boolean))
    (BOOLEAN
     (PType
      boolean))
    (char
     (PType char))
    (CHAR
     (PType char))
    (string
     (PType string))
    (STRING
     (PType
      string))))
  (SymbolTable
   0
   (Child
    (SymbolTable
     1
     (Entries
      (true
       (Symbol
        true
        (PType
         boolean)
        (Value #f)))
      (false
       (Symbol
        false
        (PType
         boolean)
        (Value #t)))
      (i
       (Symbol
        i
        (PType
         integer)))
      (c
       (Symbol
        c
        (PType
         char))))))
   (Entries
    (false
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (FALSE
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (true
     (Symbol
      true
      (PType
       boolean)
      (Value #t)))))
  (FunctionTable
   (Entries))
  (Scope
   1
   1
   (StatementList
    (Statement
     (Write
      (ConstExpr
       (Value
        "false"))
      (ConstExpr
       (Value #t))
      (ConstExpr
       (Value #f))
      (ConstExpr
       (Value
        #\newline))))
    (Statement
     (Write
      (ConstExpr
       (Value
        "true"))
      (ConstExpr
       (Value #f))
      (ConstExpr
       (Value #t))
      (ConstExpr
       (Value
        #\newline))))
    (Statement
     (Assign
      (LVal
       (PType
        integer)
       (Symbol i))
      (ConstExpr
       (Value 65))))
    (Statement
     (Assign
      (LVal
       (PType char)
       (Symbol c))
      (Expr
       (Op
        CHR
        (PType char)
        (Expr
         (LVal
          (PType
           integer)
          (Symbol
           i)))))))
    (Statement
     (Write
      (Expr
       (LVal
        (PType
         integer)
        (Symbol i)))
      (Expr
       (LVal
        (PType char)
        (Symbol c)))
      (ConstExpr
       (Value
        #\newline)))))))
