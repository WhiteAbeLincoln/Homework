'(Program (TypeTable 0 (Child (TypeTable 1 (Entries))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Entries (i (Symbol i (PType integer))) (c (Symbol c (PType char)))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (FunctionTable (Entries)) (Scope 1 1 (StatementList (Statement (Read (LVal (PType integer) (Symbol i)) (LVal (PType char) (Symbol c)))) (Statement (Write (Expr (LVal (PType integer) (Symbol i))) (Expr (LVal (PType char) (Symbol c))))) (Statement (Write (ConstExpr (Value #t)) (ConstExpr (Value #f)))))))
'(Program
  (TypeTable
   0
   (Child
    (TypeTable
     1
     (Entries)))
   (Entries
    (integer
     (PType
      integer))
    (INTEGER
     (PType
      integer))
    (boolean
     (PType
      boolean))
    (BOOLEAN
     (PType
      boolean))
    (char
     (PType char))
    (CHAR
     (PType char))
    (string
     (PType string))
    (STRING
     (PType
      string))))
  (SymbolTable
   0
   (Child
    (SymbolTable
     1
     (Entries
      (i
       (Symbol
        i
        (PType
         integer)))
      (c
       (Symbol
        c
        (PType
         char))))))
   (Entries
    (false
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (FALSE
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (true
     (Symbol
      true
      (PType
       boolean)
      (Value #t)))))
  (FunctionTable
   (Entries))
  (Scope
   1
   1
   (StatementList
    (Statement
     (Read
      (LVal
       (PType
        integer)
       (Symbol i))
      (LVal
       (PType char)
       (Symbol c))))
    (Statement
     (Write
      (Expr
       (LVal
        (PType
         integer)
        (Symbol i)))
      (Expr
       (LVal
        (PType char)
        (Symbol
         c)))))
    (Statement
     (Write
      (ConstExpr
       (Value #t))
      (ConstExpr
       (Value
        #f)))))))
