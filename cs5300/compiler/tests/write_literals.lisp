'(Program (TypeTable 0 (Child (TypeTable 1 (Entries))) (Entries (integer (PType integer)) (INTEGER (PType integer)) (boolean (PType boolean)) (BOOLEAN (PType boolean)) (char (PType char)) (CHAR (PType char)) (string (PType string)) (STRING (PType string)))) (SymbolTable 0 (Child (SymbolTable 1 (Entries (int (Symbol int (PType integer) (Value 0))) (bool (Symbol bool (PType boolean) (Value #t))) (char (Symbol char (PType char) (Value #\A))) (str (Symbol str (PType string) (Value "hello")))))) (Entries (false (Symbol false (PType boolean) (Value #f))) (FALSE (Symbol false (PType boolean) (Value #f))) (true (Symbol true (PType boolean) (Value #t))))) (FunctionTable (Entries)) (Scope 1 1 (StatementList (Statement (Write (ConstExpr (Value 0)) (ConstExpr (Value #t)) (ConstExpr (Value #\A)) (ConstExpr (Value "hello")) (ConstExpr (Value #\newline)))) (Statement (Write (ConstExpr (Value 0)) (ConstExpr (Value #t)) (ConstExpr (Value #\A)) (ConstExpr (Value "hello")) (ConstExpr (Value #\newline)))))))
'(Program
  (TypeTable
   0
   (Child
    (TypeTable
     1
     (Entries)))
   (Entries
    (integer
     (PType
      integer))
    (INTEGER
     (PType
      integer))
    (boolean
     (PType
      boolean))
    (BOOLEAN
     (PType
      boolean))
    (char
     (PType char))
    (CHAR
     (PType char))
    (string
     (PType string))
    (STRING
     (PType
      string))))
  (SymbolTable
   0
   (Child
    (SymbolTable
     1
     (Entries
      (int
       (Symbol
        int
        (PType
         integer)
        (Value 0)))
      (bool
       (Symbol
        bool
        (PType
         boolean)
        (Value #t)))
      (char
       (Symbol
        char
        (PType char)
        (Value
         #\A)))
      (str
       (Symbol
        str
        (PType
         string)
        (Value
         "hello"))))))
   (Entries
    (false
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (FALSE
     (Symbol
      false
      (PType
       boolean)
      (Value #f)))
    (true
     (Symbol
      true
      (PType
       boolean)
      (Value #t)))))
  (FunctionTable
   (Entries))
  (Scope
   1
   1
   (StatementList
    (Statement
     (Write
      (ConstExpr
       (Value 0))
      (ConstExpr
       (Value #t))
      (ConstExpr
       (Value #\A))
      (ConstExpr
       (Value
        "hello"))
      (ConstExpr
       (Value
        #\newline))))
    (Statement
     (Write
      (ConstExpr
       (Value 0))
      (ConstExpr
       (Value #t))
      (ConstExpr
       (Value #\A))
      (ConstExpr
       (Value
        "hello"))
      (ConstExpr
       (Value
        #\newline)))))))
