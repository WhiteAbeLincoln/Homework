#include "optimize.hpp"
#include "types.hpp"
#include "util.hpp"
#include <type_safe/types.hpp>

namespace ts = type_safe;

AST::Value optim::evaluate_const_op(
  const AST::Operation& op,
  const AST::SymbolTable* table
) {
  if (!types::is_const(op)) {
    throw std::runtime_error(
      "Cannot evaluate a non-const expression"
    );
  }
  switch (op.op) {
    case AST::Operation::Kind::ADD: {
      // the expressions should have already been type-checked
      auto n1 = evaluate_const_expr(op.operands[0], table);
      auto n2 = evaluate_const_expr(op.operands[1], table);
      return AST::Value(std::get<long int>(n1) + std::get<long int>(n2));
    }
    case AST::Operation::Kind::SUB: {
      auto n1 = evaluate_const_expr(op.operands[0], table);
      auto n2 = evaluate_const_expr(op.operands[1], table);
      return AST::Value(std::get<long int>(n1) - std::get<long int>(n2));
    }
    case AST::Operation::Kind::MULT: {
      auto n1 = evaluate_const_expr(op.operands[0], table);
      auto n2 = evaluate_const_expr(op.operands[1], table);
      return AST::Value(std::get<long int>(n1) * std::get<long int>(n2));
    }
    case AST::Operation::Kind::DIV: {
      auto n1 = evaluate_const_expr(op.operands[0], table);
      auto n2 = evaluate_const_expr(op.operands[1], table);
      return AST::Value(std::get<long int>(n1) / std::get<long int>(n2));
    }
    case AST::Operation::Kind::MOD: {
      auto n1 = evaluate_const_expr(op.operands[0], table);
      auto n2 = evaluate_const_expr(op.operands[1], table);
      return AST::Value(std::get<long int>(n1) % std::get<long int>(n2));
    }

    case AST::Operation::Kind::GTE: {
      auto n1 = evaluate_const_expr(op.operands[0], table);
      auto n2 = evaluate_const_expr(op.operands[1], table);
      return std::visit(util::match{
        [&](long int int_val) {
          return AST::Value(ts::bool_t(int_val >= std::get<long int>(n2)));
        },
        [&](type_safe::bool_t bool_val) {
          return AST::Value(ts::bool_t(static_cast<bool>(bool_val) >= static_cast<bool>(std::get<type_safe::bool_t>(n2))));
        },
        [&](std::string string_val) {
          return AST::Value(ts::bool_t(string_val >= std::get<std::string>(n2)));
        },
        [&](char char_val) {
          return AST::Value(ts::bool_t(char_val >= std::get<char>(n2)));
        }
      }, n1);
    }
    case AST::Operation::Kind::LTE: {
      auto n1 = evaluate_const_expr(op.operands[0], table);
      auto n2 = evaluate_const_expr(op.operands[1], table);
      return std::visit(util::match{
        [&](long int int_val) {
          return AST::Value(ts::bool_t(int_val <= std::get<long int>(n2)));
        },
        [&](type_safe::bool_t bool_val) {
          return AST::Value(ts::bool_t(static_cast<bool>(bool_val) <= static_cast<bool>(std::get<type_safe::bool_t>(n2))));
        },
        [&](std::string string_val) {
          return AST::Value(ts::bool_t(string_val <= std::get<std::string>(n2)));
        },
        [&](char char_val) {
          return AST::Value(ts::bool_t(char_val <= std::get<char>(n2)));
        }
      }, n1);
    }
    case AST::Operation::Kind::GT: {
      auto n1 = evaluate_const_expr(op.operands[0], table);
      auto n2 = evaluate_const_expr(op.operands[1], table);
      return std::visit(util::match{
        [&](long int int_val) {
          return AST::Value(ts::bool_t(int_val > std::get<long int>(n2)));
        },
        [&](type_safe::bool_t bool_val) {
          return AST::Value(ts::bool_t(static_cast<bool>(bool_val) > static_cast<bool>(std::get<type_safe::bool_t>(n2))));
        },
        [&](std::string string_val) {
          return AST::Value(ts::bool_t(string_val > std::get<std::string>(n2)));
        },
        [&](char char_val) {
          return AST::Value(ts::bool_t(char_val > std::get<char>(n2)));
        }
      }, n1);
    }
    case AST::Operation::Kind::LT: {
      auto n1 = evaluate_const_expr(op.operands[0], table);
      auto n2 = evaluate_const_expr(op.operands[1], table);
      return std::visit(util::match{
        [&](long int int_val) {
          return AST::Value(ts::bool_t(int_val < std::get<long int>(n2)));
        },
        [&](type_safe::bool_t bool_val) {
          return AST::Value(ts::bool_t(static_cast<bool>(bool_val) < static_cast<bool>(std::get<type_safe::bool_t>(n2))));
        },
        [&](std::string string_val) {
          return AST::Value(ts::bool_t(string_val < std::get<std::string>(n2)));
        },
        [&](char char_val) {
          return AST::Value(ts::bool_t(char_val < std::get<char>(n2)));
        }
      }, n1);
    }
    case AST::Operation::Kind::EQ: {
      auto n1 = evaluate_const_expr(op.operands[0], table);
      auto n2 = evaluate_const_expr(op.operands[1], table);
      return std::visit(util::match{
        [&](long int int_val) {
          return AST::Value(ts::bool_t(int_val == std::get<long int>(n2)));
        },
        [&](type_safe::bool_t bool_val) {
          return AST::Value(ts::bool_t(static_cast<bool>(bool_val) == static_cast<bool>(std::get<type_safe::bool_t>(n2))));
        },
        [&](std::string string_val) {
          return AST::Value(ts::bool_t(string_val == std::get<std::string>(n2)));
        },
        [&](char char_val) {
          return AST::Value(ts::bool_t(char_val == std::get<char>(n2)));
        }
      }, n1);
    }
    case AST::Operation::Kind::NEQ: {
      auto n1 = evaluate_const_expr(op.operands[0], table);
      auto n2 = evaluate_const_expr(op.operands[1], table);
      return std::visit(util::match{
        [&](long int int_val) {
          return AST::Value(ts::bool_t(int_val != std::get<long int>(n2)));
        },
        [&](type_safe::bool_t bool_val) {
          return AST::Value(ts::bool_t(static_cast<bool>(bool_val) != static_cast<bool>(std::get<type_safe::bool_t>(n2))));
        },
        [&](std::string string_val) {
          return AST::Value(ts::bool_t(string_val != std::get<std::string>(n2)));
        },
        [&](char char_val) {
          return AST::Value(ts::bool_t(char_val != std::get<char>(n2)));
        }
      }, n1);
    }

    case AST::Operation::Kind::AND: {
      auto b1 = evaluate_const_expr(op.operands[0], table);
      auto b2 = evaluate_const_expr(op.operands[1], table);
      return AST::Value(ts::bool_t(std::get<type_safe::bool_t>(b1) && std::get<type_safe::bool_t>(b2)));
    }
    case AST::Operation::Kind::OR: {
      auto b1 = evaluate_const_expr(op.operands[0], table);
      auto b2 = evaluate_const_expr(op.operands[1], table);
      return AST::Value(ts::bool_t(std::get<type_safe::bool_t>(b1) || std::get<type_safe::bool_t>(b2)));
    }
    case AST::Operation::Kind::NOT: {
      auto b1 = evaluate_const_expr(op.operands[0], table);
      return AST::Value(ts::bool_t(!std::get<type_safe::bool_t>(b1)));
    }

    // though the design document does not say intrinsic functions are allowed in constexpr
    // it is possible to evaluate
    case AST::Operation::Kind::ORD: {
      auto c1 = evaluate_const_expr(op.operands[0], table);
      return std::visit(util::match {
        [](const type_safe::bool_t& b) {
          return b ? AST::Value((long int)1) : AST::Value((long int)0);
        }
      , [](const char c) {
          return AST::Value((long int)c);
        }
      , [](const std::string&) -> AST::Value {
          throw std::runtime_error("Invalid type STRING given to intrinsic function ORD");
        }
      , [](const long int i) {
          return AST::Value(i);
        }
      }, c1);
    }
    case AST::Operation::Kind::CHR: {
      auto n1 = evaluate_const_expr(op.operands[0], table);
      return std::visit(util::match {
        [](const type_safe::bool_t& b) {
          return b ? AST::Value((char)1) : AST::Value((char)0);
        }
      , [](const char c) {
          return AST::Value(c);
        }
      , [](const std::string&) -> AST::Value {
          throw std::runtime_error("Invalid type STRING given to intrinsic function ORD");
        }
      , [](const long int i) {
          return AST::Value((char)i);
        }
      }, n1);
    }
    case AST::Operation::Kind::PRED: {
      auto any1 = evaluate_const_expr(op.operands[0], table);
      return std::visit(util::match{
        [](long int int_val) {
          return AST::Value(int_val - 1);
        },
        [](type_safe::bool_t bool_val) {
          return AST::Value(ts::bool_t(!bool_val));
        },
        [](char char_val) {
          return AST::Value((char)(char_val - 1));
        },
        [](std::string) -> AST::Value {
          throw std::runtime_error("Operation PRED is not defined for string type");
        }
      }, any1);
    }
    case AST::Operation::Kind::SUCC: {
      auto any1 = evaluate_const_expr(op.operands[0], table);
      return std::visit(util::match{
        [](long int int_val) {
          return AST::Value(int_val + 1);
        },
        [](type_safe::bool_t bool_val) {
          return AST::Value();
          return AST::Value(ts::bool_t(!bool_val));
        },
        [](char char_val) {
          return AST::Value((char)(char_val + 1));
        },
        [](std::string) -> AST::Value {
          throw std::runtime_error("Operation SUCC is not defined for string type");
        }
      }, any1);
    }
  }

  throw std::runtime_error(
    "evaluate_const_op not all cases handled"
  );
}

AST::Value optim::evaluate_const_expr(
  const AST::Expression& expr,
  const AST::SymbolTable* table
) {
  if (!types::is_const(expr)) {
    throw std::runtime_error(
      "Cannot evaluate a non-const expression"
    );
  }
  return std::visit(util::match{
    [](const AST::Value& val) { return val; }
  , [&](const AST::OpPtr& ptr) {
      return evaluate_const_op(*ptr, table);
    }
  , [&](const AST::LValPtr& lval) {
      if (lval->tag != AST::LValue::Kind::SYMBOL) {
        throw std::runtime_error(
          "A constant expression cannot reference a non-const lvalue"
        );
      }

      auto sym_name = lval->sym_val->name;
      auto sym = table->lookup(sym_name);
      if (!sym->const_val) {
        throw std::runtime_error(
          "A constant expression cannot reference a non-const symbol"
        );
      }
      return *sym->const_val;
    }
  , [&](const AST::CallPtr&) -> AST::Value {
      throw std::runtime_error(
        "Function calls are not const"
      );
    }
  }, expr);
}

AST::Expression* optim::try_evaluate_const_expr(
  AST::Expression* expr,
  const AST::SymbolTable* table
) {
  if (types::is_const(*expr)) {
    return new AST::Expression(evaluate_const_expr(*expr, table));
  }

  PRINT_VERBOSE("Expression could not be evaluated\n");

  return expr;
}
