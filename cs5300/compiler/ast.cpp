#include <stdexcept>
#include "ast.hpp"
#include <sstream>
#include <algorithm>
#include "util.hpp"
#include "types.hpp"
#include <exception>
#include <cstdio>
#include <numeric>

using namespace AST;
using namespace types;

// we can't use the templated version because we convert to string
AST::IdentList* AST::add_ident(const char* ident, IdentList* list) {
  list->push_back(std::string(ident));
  delete ident;
  // explicitly set to nullptr so we can catch errors more easily
  ident = nullptr;
  return list;
}

Type::Type(std::string name, Type::Kind tag) : name(name)
                                                , tag(tag) {}

Type::Type(Array* array) : Type(print(*array), array) {}
Type::Type(std::string name, Array* arr) : name(name)
                                         , tag(Type::Kind::ARRAY)
                                         , array(arr) {}

Type::Type(Record* record) : Type(print(*record), record) {}
Type::Type(std::string name, Record* record) : name(name)
                                             , tag(Type::Kind::RECORD)
                                             , record(record) {}


Type* AST::type_int = new Type("integer", Type::Kind::INT);
Type* AST::type_char = new Type("char", Type::Kind::CHAR);
Type* AST::type_bool = new Type("boolean", Type::Kind::BOOL);
Type* AST::type_string = new Type("string", Type::Kind::STR);
Type* AST::type_void = new Type("void", Type::Kind::VOID);
Value* AST::val_false = new Value(type_safe::bool_t(false));
Value* AST::val_true = new Value(type_safe::bool_t(true));
Symbol* AST::sym_true = new Symbol("true", *AST::val_true);
Symbol* AST::sym_false = new Symbol("false", *AST::val_false);

bool is_intrinsic_symbol(const AST::Symbol* val) {
  return val == AST::sym_true || val == AST::sym_false;
}

std::string AST::print_decl(const Declaration& decl) {
  std::string str("(Decl");
  auto decls = util::join(decl.first);
  auto type = print(*decl.second);
  return str + "," + type + "," + decls + ")";
}

std::string AST::print_decllist(const DeclarationList& dlist) {
  std::string str("(DList");

  for (auto decl : dlist) {
    str += "," + print_decl(decl);
  }

  return str + ")";
}

AST::Symbol* AST::lookup(const SymbolTable& table, std::string name) {
  auto result = table.lookup(name);
  if (result == nullptr) {
    throw std::runtime_error("Symbol not defined: " + name);
  }
  return result;
}

const AST::Type* AST::lookup(const TypeTable& table, std::string name) {
  auto result = table.lookup(name);
  if (result == nullptr) {
    throw std::runtime_error("Type not defined: " + name);
  }
  return result;
}

TypeTable* AST::initialize(TypeTable* table) {
  table->insert(AST::type_int->name, AST::type_int);
  table->insert("INTEGER", AST::type_int);
  table->insert(AST::type_bool->name, AST::type_bool);
  table->insert("BOOLEAN", AST::type_bool);
  table->insert(AST::type_char->name, AST::type_char);
  table->insert("CHAR", AST::type_char);
  table->insert(AST::type_string->name, AST::type_string);
  table->insert("STRING", AST::type_string);
  return table->push();
}

// initializes a default symbol scope with true and false defined
// and returns a child scope (so that true and false can be redefined)
SymbolTable* AST::initialize(SymbolTable* table) {
  table->insert(AST::sym_false->name, AST::sym_false);
  table->insert("FALSE", AST::sym_false);
  table->insert(AST::sym_true->name, AST::sym_true);
  table->insert("true", AST::sym_false);
  return table->push();
}

void AST::insert(SymbolTable& table, std::string name, AST::Symbol* sym) {
  if (sym == nullptr) {
    throw std::runtime_error("Attempted to insert a null symbol");
  }
  auto success = table.insert(name, sym);

  if (!success) {
    throw new std::runtime_error("Cannot redeclare symbol " + name);
  }
}

void AST::insert(TypeTable& table, std::string name, const Type* type) {
  if (type == nullptr) {
    throw std::runtime_error("Attempted to insert a null type");
  }
  auto success = false;
  switch (type->tag) {
    // if the type is not a record, it will not have a name
    case Type::Kind::RECORD: {
      success = table.insert(name, new Type(name, type->record));
      // can we delete the type?
      // delete type;
      break;
    }
    case Type::Kind::ARRAY: {
      success = table.insert(name, new Type(name, type->array));
      break;
    }
    default:
      success = table.insert(name, type);
  }

  if (!success) {
    throw new std::runtime_error("Cannot redeclare type " + name);
  }
}

long int get_array_bound(Expression* bound) {
  auto boundT = get_type(*bound);
  if (!is_ordered_type(boundT)) {
    throw std::invalid_argument("Array bounds must be an ordered type (int, char, bool)");
  }
  if (!is_const(*bound)) {
    throw std::invalid_argument("Array bounds must be const expressions");
  }

  auto val = std::get<AST::Value>(*bound);
  auto ret = std::visit(util::match{
    [&](long int b) -> long int { return b; },
    [&](char c) -> long int { return (long int)c; },
    [&](std::string) -> long int { throw std::invalid_argument("Invalid bound"); },
    [&](type_safe::bool_t b) -> long int { return b ? (long int)1 : (long int)0; },
  }, val);
  return ret;
}

Array::Array(
  Expression* start,
  Expression* end,
  const Type* type
) : startI(get_array_bound(start))
  , endI(get_array_bound(end))
  , of(type) {
    if (this->startI > this->endI)
      throw std::runtime_error("Array bounds must be in increasing order");
  }

// initializer list must be in the same order as definitions in the struct
Symbol::Symbol(std::string name, const Type* type) : name(name)
                                                   , type(type)
                                                   , const_val(std::nullopt) {
                                                     if (type == type_void) {
                                                       throw std::runtime_error("Symbol type cannot be void");
                                                     }
                                                   }

Symbol::Symbol(std::string name, Value val) : name(name)
                                            , type(types::get_type(val))
                                            , const_val({val}) {}

WriteStmt* AST::add_write(Expression* expr, WriteStmt* args) {
  types::verify_nonUser(expr);
  AST::add_and_own(expr, args);
  return args;
}

ReadStmt* AST::add_read(LValue* lval, ReadStmt* args) {
  auto lvalT = get_type(*lval);
  if (!has_type(lvalT, AST::Type::Kind::INT) && !has_type(lvalT, AST::Type::Kind::CHAR)) {
    throw std::runtime_error(
      "Cannot read into a variable with non-integer/char type "
      + typekind_str(lvalT->tag)
    );
  }
  AST::add_and_own(lval, args);
  return args;
}

std::string AST::typekind_str(const AST::Type::Kind tag) {
  switch (tag) {
    case AST::Type::Kind::ARRAY:
      return "ARRAY";
    case AST::Type::Kind::BOOL:
      return "BOOL";
    case AST::Type::Kind::CHAR:
      return "CHAR";
    case AST::Type::Kind::INT:
      return "INT";
    case AST::Type::Kind::RECORD:
      return "RECORD";
    case AST::Type::Kind::STR:
      return "STR";
    case AST::Type::Kind::VOID:
      return "VOID";
  }
  throw std::runtime_error("typekind_str: not all cases handled");
}

std::string AST::opkind_str(const Operation::Kind kind) {
  switch (kind) {
    case Operation::Kind::DIV:
      return "DIV";
    case Operation::Kind::MULT:
      return "MULT";
    case Operation::Kind::SUB:
      return "SUB";
    case Operation::Kind::ADD:
      return "ADD";
    case Operation::Kind::MOD:
      return "MOD";
    case Operation::Kind::LT:
      return "LT";
    case Operation::Kind::GT:
      return "GT";
    case Operation::Kind::LTE:
      return "LTE";
    case Operation::Kind::GTE:
      return "GTE";
    case Operation::Kind::EQ:
      return "EQ";
    case Operation::Kind::NEQ:
      return "NEQ";
    case Operation::Kind::AND:
      return "AND";
    case Operation::Kind::OR:
      return "OR";
    case Operation::Kind::NOT:
      return "NOT";
    case Operation::Kind::CHR:
      return "CHR";
    case Operation::Kind::ORD:
      return "ORD";
    case Operation::Kind::PRED:
      return "PRED";
    case Operation::Kind::SUCC:
      return "SUCC";
  }
  throw std::runtime_error("opkind_str: not all cases handled");
}

Assignment::Assignment(LValue lval, Expression expr) : lval(lval), expr(expr) {
  PRINT_VERBOSE("Constructing Assignment: %s := %s\n", print(lval).c_str(), print(expr).c_str());
  auto lvalT = get_type(lval);
  PRINT_VERBOSE("lval type %s", print(*lvalT).c_str());
  auto exprT = get_type(expr);
  PRINT_VERBOSE1(", expr type %s\n", print(*exprT).c_str());
  if (lvalT != exprT) {
    throw std::runtime_error(
      "Cannot assign expression of type "
      + typekind_str(exprT->tag)
      + " to lvalue of type "
      + typekind_str(lvalT->tag)
      );
  }
}

IfStatement::IfStatement(
  Expression pred
, StatementList body
, std::vector<ElseIf> elseifs
, StatementList else_body
) : pred(pred)
  , body(body)
  , elseifs(elseifs)
  , else_body(else_body)
{
  if (!types::has_type(this->pred, AST::Type::Kind::BOOL)) {
    throw std::runtime_error("If predicate expression must have type BOOL");
  }
}

ElseIf::ElseIf(
  Expression pred
, StatementList body
) : pred(pred)
  , body(body)
{
  if (!types::has_type(this->pred, AST::Type::Kind::BOOL)) {
    throw std::runtime_error("If predicate expression must have type BOOL");
  }
}

While::While(
  Expression pred
, StatementList body
) : pred(pred)
  , body(body)
{
  if (!types::has_type(this->pred, AST::Type::Kind::BOOL)) {
    throw std::runtime_error("If predicate expression must have type BOOL");
  }
}

Repeat::Repeat(
  Expression pred
, StatementList body
) : pred(pred)
  , body(body)
{
  if (!types::has_type(this->pred, AST::Type::Kind::BOOL)) {
    throw std::runtime_error("If predicate expression must have type BOOL");
  }
}

For::For(
  Assignment init,
  ForDir dir,
  Expression end,
  StatementList body
) : init(init), dir(dir), end(end), body(body)
{
  auto initT = types::get_type(this->init.expr);
  auto endT = types::get_type(this->end);
  if (!types::is_ordered_type(initT)) {
    throw std::runtime_error("For loop variable must be an ordered type");
  }
  if (initT != endT) {
    throw std::runtime_error("For loop start and end must have the same type");
  }
}

Operation::Operation(Operation::Kind op, Expression left) : Operation(op, std::vector<Expression>{left}) {}
Operation::Operation(Operation::Kind op, Expression left, Expression right) : Operation(op, std::vector<Expression>{left, right}) {}

Operation::Operation(
  Operation::Kind op,
  std::vector<Expression> operands
) : op(op)
  , operands(operands)
  {
    if (op <= Operation::Kind::MOD && op >= Operation::Kind::DIV) {
      this->type = check_arith_op(op, this->operands[0], this->operands[1]);
    } else if (op <= Operation::Kind::NOT && op >= Operation::Kind::AND) {
      this->type = check_logical_op(op, this->operands[0], op == Operation::Kind::NOT ? nullptr : &this->operands[1]);
    } else if (op <= Operation::Kind::NEQ && op >= Operation::Kind::LT) {
      this->type = check_relational_op(op, this->operands[0], this->operands[1]);
    } else if (op <= Operation::Kind::SUCC && op >= Operation::Kind::CHR) {
      this->type = check_intrinsic_func(op, this->operands[0]);
    } else {
      throw std::invalid_argument("Operation type is out of range");
    }
  }

AST::Symbol* get_record_entry(const LValue& lval, std::string key) {
  auto type = get_type(lval);
  if (!has_type(type, AST::Type::Kind::RECORD)) {
    throw std::runtime_error(
      "Cannot access key "
      + key
      + " on non-record type "
      + typekind_str(get_type(lval)->tag)
    );
  }

  auto record = type->record;
  return lookup(record->table, key);
}

LValue::LValue(LValue* lval, std::string key) : tag(LValue::Kind::RECORD)
                                                    , rec_val(
                                                        new std::pair<LValue*, Symbol*>(
                                                          lval
                                                        , get_record_entry(*lval, key)
                                                        )
                                                      )
                                                    {}

LValue::LValue(LValue* lval, const Expression* idx) : tag(LValue::Kind::ARRAY)
                                                    , array_val(
                                                        new std::pair<LValue*, Expression>(
                                                          lval
                                                        , *idx
                                                        )
                                                      )
                                                    {
                                                      auto idxT = types::get_type(*idx);
                                                      if (!types::is_ordered_type(idxT)) {
                                                        throw std::invalid_argument("Array bounds must be an ordered type (int, char, bool)");
                                                      }
                                                      delete idx;
                                                    }

LValue::LValue(std::string sym, const SymbolTable& table) : tag(LValue::Kind::SYMBOL)
                                                          , sym_val(lookup(table, sym))
                                                          {}

AST::Signature AST::lookup(
  const AST::FunctionTable& table,
  const std::string& name,
  const std::vector<Expression>& params
) {
  auto result = table.lookup(hash_call(name, params));
  if (!result) {
    throw std::runtime_error("Function not defined: " + name);
  }
  return *result;
}

std::tuple<bool, std::tuple<Signature, std::optional<AST::Body>>>
AST::FunctionTable::insert(
  Signature sig,
  std::optional<AST::Body> body
) {
  // not the fastest, but much easier than defining a hashing and equality function
  auto key = hash_signature(sig);
  auto existing = this->table.find(key);
  if (existing != this->table.end()) {
    return std::make_tuple(false, existing->second);
  }
  return std::make_tuple(true, this->force_insert(sig, body, key));
}

std::optional<AST::Signature> AST::FunctionTable::lookup(const std::string& key) const {
  auto found = this->table.find(key);
  if (found != this->table.end())
    return {std::get<0>(found->second)};

  return std::nullopt;
}

std::optional<AST::Function> AST::FunctionTable::lookup(const AST::Signature& sig) const {
  auto key = hash_signature(sig);
  auto found = this->table.find(key);
  if (found != this->table.end())
    return {found->second};

  return std::nullopt;
}

std::tuple<Signature, std::optional<AST::Body>>
AST::FunctionTable::force_insert(Signature sig, std::optional<AST::Body> body) {
  auto key = hash_signature(sig);
  return force_insert(sig, body, key);
}
std::tuple<Signature, std::optional<AST::Body>>
AST::FunctionTable::force_insert(Signature sig, std::optional<AST::Body> body, std::string key) {
  sig.label = sig.label == "" ? "__" + sig.name + std::to_string(this->table.size()) + "__" : sig.label;
  auto val = std::make_tuple(sig, body);
  this->table[key] = val;
  return val;
}

void AST::insert(AST::FunctionTable& table, const AST::Signature& sig, const std::optional<AST::Body>& body) {
  auto [inserted, result] = table.insert(sig, body);
  // if insertion failed because forward declaration exists
  if (!inserted && !std::get<1>(result)) {
    table.force_insert(std::get<0>(result), body);
  } else if (!inserted) {
    throw std::runtime_error("Cannot redeclare function " + sig.name);
  }
}

std::vector<Param>* AST::create_params(bool ref, AST::IdentList& idents, const AST::Type* type) {
  auto ps = new std::vector<Param>{};

  for (auto n : idents) {
    ps->push_back(AST::Param{ ref, n, type });
  }

  return ps;
}

void AST::insert_params(AST::SymbolTable* table, const AST::Signature& sig) {
  for (auto p : sig.params) {
    auto type = p.type;
    auto ref = p.ref;
    auto name = p.name;

    auto sym = new AST::Symbol(name, type);
    sym->ref = ref;
    sym->param = true;
    table->insert(name, sym);
  }
}

std::string AST::print(const LValue& lval) {
  std::string str("(LVal ");
  auto type = get_type(lval);
  str += print(*type) + " ";
  if (lval.tag == AST::LValue::Kind::ARRAY) {
    auto p = lval.array_val;
    str += print(*p->first) + " ";
  }
  if (lval.tag == AST::LValue::Kind::RECORD) {
    auto p = lval.rec_val;
    str += print(*p->first) + " ";
  }
  switch (lval.tag) {
    case LValue::Kind::ARRAY:
      str += "(At " + print(lval.array_val->second) + ")";
      break;
    case LValue::Kind::SYMBOL:
      str += print(*lval.sym_val);
      break;
    case LValue::Kind::RECORD:
      break;
  }

  return str + ")";
}

std::string AST::print(const Expression& val) {
  std::string str(types::is_const(val) ? "(ConstExpr " : "(Expr ");
  return str + std::visit(util::match{
    [](const Value& v) {
      return print(v);
    }
  , [](const OpPtr& ptr) {
      return print(*ptr);
    }
  , [](const LValPtr& ptr) {
      return print(*ptr);
    }
  , [](const CallPtr& ptr) {
      return print(*ptr);
    }
  }, val) + ")";
}

std::string AST::print(const Call& call) {
  std::string str("(Call " + call.sig.name);
  for (auto expr : call.args) {
    str += " " + print(expr);
  }
  return str + ")";
}

std::string AST::print(const Operation& val) {
  std::string str("(Op ");

  str += opkind_str(val.op);

  // the operation should always have a type
  // this is a defensive check
  if (val.type == nullptr) {
    throw std::runtime_error("Operation has uninitialized type");
  }
  str += " " + print(*val.type);

  for (auto v : val.operands) {
    str += " " + print(v);
  }

  return str + ")";
}

std::string AST::print(const Body& val) {
  return (
    "(Body "
    + print(*val.type_table.root())
    + " "
    + print(*val.table.root())
    + " "
    + "(Scope "
    + std::to_string(val.type_table.getId())
    + " "
    + std::to_string(val.table.getId())
    + " "
    + print(val.statements)
    + "))"
  );
}

std::string AST::print(const FunctionTable& val) {
  std::string str = "(FunctionTable (Entries";
  for (auto name : val.table) {
    auto [sig, body] = name.second;
    str += " (" + print(sig) + (body ? " " + print(*body) : "") + ")";
  }
  return str + "))";
}

std::string AST::print(const Program& val) {
  return (
      "(Program "
    // a Program is normally given a table one-removed from the root
    // yet we want to print the whole tree from the root
    + print(*val.type_table.root())
    + " "
    + print(*val.table.root())
    + " "
    + print(val.ftable)
    + " "
    // wrap in enclosing scope
    + "(Scope "
    + std::to_string(val.type_table.getId())
    + " "
    + std::to_string(val.table.getId())
    + " "
    + print(val.statements)
    + "))"
  );
}

std::string AST::print(const StatementList& stmts) {
  std::string str("(StatementList");
  for (auto stmt : stmts) {
    str += " " + print(stmt);
  }
  return str + ")";
}

std::string AST::print(const Statement& val) {
  std::string str("(Statement ");
  return str + std::visit(util::match{
    [](const Assignment& assn) {
      return print(assn);
    },
    [](const WriteStmt& write) {
      return print(write);
    },
    [](const Stop&) -> std::string {
      return "STOP";
    },
    [](const ReadStmt& read) {
      return print(read);
    },
    [](const IfStmtPtr& ptr) {
      return print(*ptr);
    },
    [](const WhilePtr& ptr) {
      return print(*ptr);
    },
    [](const RepeatPtr& ptr) {
      return print(*ptr);
    },
    [](const ForPtr& ptr) {
      return print(*ptr);
    },
    [](const Return& ret) {
      return "(Return" + (ret.value ? " " + print(*ret.value) : "") + ")";
    },
    [](const CallPtr& ptr) {
      return print(*ptr);
    }
  }, val) + ")";
}

std::string AST::print(const IfStatement& stmt) {
  return "(If "
  + print(stmt.pred)
  + " "
  + print(stmt.body)
  + " "
  + print(stmt.elseifs)
  + " "
  + print(stmt.else_body)
  + ")";
}

std::string AST::print(const While& stmt) {
  return "(While "
  + print(stmt.pred)
  + " "
  + print(stmt.body)
  + ")";
}

std::string AST::print(const Repeat& stmt) {
  return "(Repeat "
  + print(stmt.pred)
  + " "
  + print(stmt.body)
  + ")";
}

std::string AST::print(const For& stmt) {
  return "(For "
  + print(stmt.init)
  + (stmt.dir == ForDir::PRED ? " DOWNTO " : " TO ")
  + print(stmt.end)
  + " "
  + print(stmt.body)
  + ")";
}

std::string AST::print(const std::vector<ElseIf>& stmts) {
  std::string str = "(ElseIfs";
  for (auto stmt : stmts) {
    str += (" (If " + print(stmt.pred) + " " + print(stmt.body) + ")");
  }
  return str + ")";
}

std::string AST::print(const Type& val, bool name_only) {
  std::string str("(");
  auto intrinsic = is_intrinsic_type(&val);
  str += (intrinsic ? "PType " : "Type ");

  if (name_only && !intrinsic) {
    return str + val.name + ")";
  }

  switch (val.tag) {
    case Type::Kind::STR:
    case Type::Kind::INT:
    case Type::Kind::CHAR:
    case Type::Kind::BOOL:
    case Type::Kind::VOID:
      return str + val.name + ")";
    case Type::Kind::RECORD:
      return str + print(*val.record) + ")";
    case Type::Kind::ARRAY:
      return str + print(*val.array) + ")";
  }
  throw std::runtime_error("Type print not all cases handled");
}

std::string AST::print(const Assignment& val) {
  return std::string("(Assign ") + print(val.lval) + " " + print(val.expr) + ")";
}

std::string AST::print(const WriteStmt& args) {
  std::string str("(Write");
  for (auto w : args) {
    str += " " + print(w);
  }
  return str + ")";
}

std::string AST::print(const ReadStmt& args) {
  std::string str("(Read");
  for (auto w : args) {
    str += " " + print(w);
  }
  return str + ")";
}

std::string AST::print(const Symbol& val, bool name_only) {
  return std::string("(Symbol ")
    + val.name
    + (name_only
        ? ")"
        : (" " + print(*val.type)
          + (val.const_val
              ? " " + print(*val.const_val) + ")"
              : ")"
            )
          )
      );
}

// from https://docs.racket-lang.org/reference/reader.html#%28part._parse-character%29
std::string print_char_racket(const char chr) {
  switch (chr) {
    case '\0':
      return "null";
    case 8:
      return "backspace";
    case '\t':
      return "tab";
    case '\n':
      return "newline";
    case 11:
      return "vtab";
    case 12:
      return "page";
    case '\r':
      return "return";
    case ' ':
      return "space";
    case 127:
      return "rubout";
    default:
      return std::string("") + chr;
  }
}

std::string AST::print(const Value& val) {
  return (
    std::string("(Value ")
  + std::visit(util::match {
      [](long int int_val) -> std::string {
        return std::to_string(int_val);
      },
      [](std::string str_val) -> std::string {
        // TODO: Re-escape string
        return "\"" + str_val + "\"";
      },
      [](char char_val) -> std::string {
        // we'll use the racket syntax for character literals
        return "#\\" + print_char_racket(char_val);
      },
      [](type_safe::bool_t bool_val) -> std::string {
        return bool_val ? "#t" : "#f";
      }
    }, val)
  + ")"
  );
}

std::string AST::print(const TypeTable& val) {
  std::string str = "(TypeTable " + std::to_string(val.getId());
  if (val.getChild() != nullptr) {
    str += " (Child " + print(*val.getChild()) + ")";
  }
  str += " (Entries";
  for (auto name : val.insertion_order) {
    auto entry = val.table.at(name);
    // we expand the entries in the table and refer to them only by name
    // in all other places (including nested type references such as in records)
    str += " (" + name + " " + print(*entry, false) + ")";
  }
  return str + "))";
}

std::string AST::print(const SymbolTable& val) {
  std::string str = "(SymbolTable " + std::to_string(val.getId());
  if (val.getChild() != nullptr) {
    str += " (Child " + print(*val.getChild()) + ")";
  }
  str += " (Entries";
  for (auto name : val.insertion_order) {
    auto entry = val.table.at(name);
    str += " (" + name + " " + print(*entry, false) + ")";
  }
  return str + "))";
}

std::string AST::print(const Array& val) {
  std::string str("(Array");
  return str + " " + std::to_string(val.startI) + " " + std::to_string(val.endI) + " " + print(*val.of) + ")";
}

std::string AST::print(const Record& val) {
  std::string str("(Record");
  for (auto entry : val.table.table) {
    str += " (" + entry.first + " " + print(*entry.second) + ")";
  }
  return str + ")";
}

std::string AST::print(const std::vector<AST::Param>& params) {
  std::string str;
  for (auto entry : params) {
    str += " (Param "
        + entry.name
        + (entry.ref ? " VAR " : " REF ")
        + AST::print(*entry.type)
        + ")";
  }
  return str;
}

std::string AST::print(const AST::Signature& sig) {
  return "(Sig "
    + sig.name
    + print(*sig.return_type)
    + print(sig.params)
    + ")";
}

std::string AST::hash_signature(const AST::Signature& sig) {
  auto params = std::accumulate(sig.params.begin(), sig.params.end(), sig.name, [](auto& acc, auto& val) {
    auto type = print(*val.type);
    return acc + " " + type;
  });

  PRINT_VERBOSE("Hashing for signature: %s | %s\n", print(sig.params).c_str(), params.c_str());
  return params;
}

std::string AST::hash_call(const std::string& name, const std::vector<AST::Expression>& params) {
  auto paramHash = std::accumulate(params.begin(), params.end(), name, [](auto& acc, auto& val) {
    return acc + " " + print(*types::get_type(val));
  });
  PRINT_VERBOSE("Hashing for call: %s | %s\n", print(params).c_str(), paramHash.c_str());

  return paramHash;
}
