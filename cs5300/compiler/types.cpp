#include "types.hpp"
#include <algorithm>
#include "util.hpp"

template <typename T>
T* check_null_ptr(T* pointer) {
  if (pointer == nullptr) {
    throw std::runtime_error("received a null pointer");
  }
  return pointer;
}

bool types::is_intrinsic_type(const AST::Type* type) {
  return (
       type == AST::type_int
    || type == AST::type_char
    || type == AST::type_bool
    || type == AST::type_string
  );
}

bool types::is_ordered_type(const AST::Type* type) {
  return (
       type == AST::type_int
    || type == AST::type_char
    || type == AST::type_bool
  );
}


bool types::has_type(const AST::Type* type, AST::Type::Kind type_tag) {
  if (type == nullptr) {
    throw std::runtime_error("has_type: got null type");
  }

  return type->tag == type_tag;
}

bool types::has_type(const AST::Value& val, AST::Type::Kind type_tag) {
  return has_type(get_type(val), type_tag);
}

bool types::has_type(const AST::Expression& val, AST::Type::Kind type_tag) {
  return has_type(get_type(val), type_tag);
}

bool types::has_type(const AST::LValue& lval, AST::Type::Kind type_tag) {
  return has_type(get_type(lval), type_tag);
}

bool types::is_const(const AST::Operation& op) {
  return std::all_of(
    op.operands.begin(),
    op.operands.end(),
    [](const AST::Expression& expr) {
      return is_const(expr);
    }
  );
}

bool types::is_const(const AST::Expression& expr) {
  if (std::holds_alternative<AST::Value>(expr)) return true;
  if (std::holds_alternative<AST::CallPtr>(expr)) return false;
  if (std::holds_alternative<AST::LValPtr>(expr)) {
    auto lval = *std::get<AST::LValPtr>(expr);
    return is_const(lval);
  }
  if (std::holds_alternative<AST::OpPtr>(expr)) {
    auto op = *std::get<AST::OpPtr>(expr);
    return is_const(op);
  }
  throw std::runtime_error("is_const not all cases handled");
}

// technically some other symbol references could be constant
// however without a full-program analysis, we can only determine
// that its constant if the symbol has the constant flag set
// i.e. it was evaluated using evaluate_const_expr
// note that this should still be fine if a constant symbol
// gets redeclared later to a non-constant, because any new
// references will use the non-const version
// having a VAR block makes this easy to determine:
// A symbol is const if it is declared in the CONST block,
// and not declared in the VAR block. Symbols declared in the
// VAR block will overwrite the symbol in the CONST block, and
// any later lookups will use the new non-const symbol.
// It is an error to assign to a const symbol outside of the
// CONST block
// TODO: we should actually test that though
bool types::is_const(const AST::LValue& sym) {
  return sym.tag == AST::LValue::Kind::SYMBOL && sym.sym_val->const_val;
}

const AST::Type* types::get_type(const AST::Expression& expr) {
  return std::visit(util::match{
    [](const AST::Value& v) {
      return get_type(v);
    }
  , [](const AST::OpPtr ptr) {
      return check_null_ptr(ptr->type);
    }
  , [](const AST::LValPtr ptr) {
      return get_type(*ptr);
    }
  , [](const AST::CallPtr ptr) {
      return get_type(*ptr);
    }
  }, expr);
}

const AST::Type* types::get_type(const AST::Value& val) {
  return std::visit(util::match{
    [](long int) { return AST::type_int; },
    [](std::string) { return AST::type_string; },
    [](char) { return AST::type_char; },
    [](type_safe::bool_t) { return AST::type_bool; }
  }, val);
}

const AST::Type* types::get_type(const AST::LValue& lval) {
  switch (lval.tag) {
    case AST::LValue::Kind::SYMBOL:
      return get_type(*lval.sym_val);
    case AST::LValue::Kind::RECORD:
      return get_type(*lval.rec_val->second);
    case AST::LValue::Kind::ARRAY: {
      auto parent_type = get_type(*lval.array_val->first);
      if (!has_type(parent_type, AST::Type::Kind::ARRAY)) {
        throw std::runtime_error("somehow array lvalue initialized without array parent");
      }
      return parent_type->array->of;
    }
  }
  throw std::runtime_error("get_type(lvalue): not all cases handled");
}

const AST::Type* types::get_type(const AST::Symbol& sym) {
  return check_null_ptr(sym.type);
}

const AST::Type* types::get_type(const AST::Call& call) {
  return check_null_ptr(call.sig.return_type);
}

const AST::Type* types::check_arith_op(
  AST::Operation::Kind opkind,
  const AST::Expression& left,
  const AST::Expression& right
) {
  // if opkind is outside the range of arithmetic values, throw error
  if (  opkind > AST::Operation::Kind::MOD
     || opkind < AST::Operation::Kind::DIV) {
    throw std::invalid_argument(
      "received a non-arithmetic operation " + AST::opkind_str(opkind)
    );
  }
  if (  !has_type(left, AST::Type::Kind::INT)
     || !has_type(right, AST::Type::Kind::INT)) {
    throw std::invalid_argument(
      "received a non-integer expression for operation "
      + AST::opkind_str(opkind)
    );
  }
  return AST::type_int;
}

const AST::Type* types::check_logical_op(
  AST::Operation::Kind opkind,
  const AST::Expression& left,
  const AST::Expression* right
) {
  if (  opkind > AST::Operation::Kind::NOT
     || opkind < AST::Operation::Kind::AND) {
    throw std::invalid_argument(
      "received a non-logical operation "
      + AST::opkind_str(opkind)
    );
  }

  // some code duplication here, but I feel that it's simpler than
  // litering the other code with checks whether opkind is NOT
  if (opkind == AST::Operation::Kind::NOT) {
    if (!has_type(left, AST::Type::Kind::BOOL)) {
      throw std::invalid_argument(
        "received a non-boolean expression for operation "
        + AST::opkind_str(opkind)
      );
    }
    return AST::type_bool;
  }

  if (right == nullptr) {
    throw std::invalid_argument(
      "received a null expression for operation " +
      AST::opkind_str(opkind)
    );
  }
  if (  !has_type(left, AST::Type::Kind::BOOL)
     || !has_type(*right, AST::Type::Kind::BOOL)) {
    throw std::invalid_argument(
      "received a non-boolean value for operation "
      + AST::opkind_str(opkind)
    );
  }
  return AST::type_bool;
}

const AST::Type* types::check_relational_op(
  AST::Operation::Kind opkind,
  const AST::Expression& left,
  const AST::Expression& right
) {
  if (  opkind > AST::Operation::Kind::NEQ
     || opkind < AST::Operation::Kind::LT) {
    throw std::invalid_argument(
      "received a non-relational operation "
      + AST::opkind_str(opkind)
    );
  }
  auto ltype = get_type(left);
  auto rtype = get_type(right);
  if (ltype != rtype) {
    throw std::invalid_argument(
      "received a mixed type expression for operation "
      + AST::opkind_str(opkind)
    );
  }
  if (!is_ordered_type(ltype)) {
    throw std::invalid_argument(
      "received an invalid expression for operation "
      + AST::opkind_str(opkind)
    );
  }
  return AST::type_bool;
}

const AST::Type* types::check_intrinsic_func(
  AST::Operation::Kind opkind,
  const AST::Expression& left
) {
  auto type = get_type(left);
  switch (opkind) {
    case AST::Operation::Kind::CHR: {
      if (!is_ordered_type(type)) {
        throw std::invalid_argument(
          "received non-ordered expression for operation "
          + AST::opkind_str(opkind)
        );
      }
      return AST::type_char;
    }
    case AST::Operation::Kind::ORD: {
      if (!is_ordered_type(type)) {
        throw std::invalid_argument(
          "received non-ordered expression for operation "
          + AST::opkind_str(opkind)
        );
      }
      return AST::type_int;
    }
    case AST::Operation::Kind::PRED:
    case AST::Operation::Kind::SUCC: {
      if (!is_ordered_type(type)) {
        throw std::invalid_argument(
          "received non-ordered expression for operation "
          + AST::opkind_str(opkind)
        );
      }
      return type;
    }
    default:
      throw std::invalid_argument(
        "received a non-intrinsic operation "
        + AST::opkind_str(opkind)
      );
  }
}

void types::verify_nonUser(AST::Expression* expr) {
  auto exp = *expr;
  if (  types::has_type(exp, AST::Type::Kind::ARRAY)
     || types::has_type(exp, AST::Type::Kind::RECORD)) {
    throw std::invalid_argument(
      "recieved user-defined type when primitive was expected"
    );
  }
}

bool types::type_any_of(const AST::Type* type, std::vector<AST::Type::Kind> valid) {
  return std::any_of(valid.cbegin(), valid.cend(), [&](AST::Type::Kind kind) {
    return has_type(type, kind);
  });
}

bool types::type_any_of(const AST::Expression& exp, std::vector<AST::Type::Kind> valid) {
  return type_any_of(get_type(exp), valid);
}

void types::check_return(std::optional<AST::Signature> sig, std::optional<AST::Expression> expr) {
  if (!sig) { throw std::runtime_error("Cannot return outside function");  }

  auto ret = sig->return_type;
  if (expr && get_type(*expr) != ret) {
    throw std::runtime_error("Return value type does not match the function type");
  }

  if (!expr && ret != AST::type_void) {
    throw std::runtime_error("Cannot return void from a non-void function");
  }
}

void types::check_call(AST::Call& call) {
  auto size = call.sig.params.size();
  if (size != call.args.size()) {
    throw std::runtime_error("Invalid number of parameters in call");
  }

  for (size_t i = 0; i < size; i++) {
    if (call.sig.params[i].type != get_type(call.args[i]))
      throw std::runtime_error("Invalid parameter type for call");
  }
}
