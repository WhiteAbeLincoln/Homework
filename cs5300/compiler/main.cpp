#include "ast.hpp"
#include "util.hpp"
#include "codegen.hpp"
#include <cstdio>
#include <cstring>

extern int yyparse();
extern AST::Program root;

const char* HELP_TEXT = R"HELP(
Usage: cpsl [-h|-g|-v|-a]
  Compiles source code in the CPSL language, read on STDIN, to MIPS assembly, output on STDOUT
  Flags:
    -h  print this help text
    -g  output a graph representation of the AST in DOT format on STDOUT (does not emit mips code)
    -v  verbose output on STDERR (can be disabled by clearing the ALLOW_VERBOSE make flag)
    -a return the AST in lisp-like format on STDOUT
)HELP";

int main(int argc, char* argv[]) {
  for (int i = 1; i < argc; ++i) {
    auto value = argv[i];
    if (strcmp("-h", value) == 0) {
      printf("%s\n", HELP_TEXT);
      return 0;
    }
    auto matchGraph = strcmp("-g", value);
    auto matchVerbose = strcmp("-v", value);
    auto matchAST = strcmp("-a", value);
    if (matchGraph == 0) {
      util::CpslConfig.print_graph = true;
    } else if (matchVerbose == 0) {
      util::CpslConfig.verbose = true;
    } else if (matchAST == 0) {
      util::CpslConfig.print_ast = true;
    } else {
      printf("%s\n", HELP_TEXT);
      return 1;
    }
  }

  PRINT_VERBOSE(
    "Config: { verbose: %u, print_graph: %u, print_ast: %u }\n",
    util::CpslConfig.verbose,
    util::CpslConfig.print_graph,
    util::CpslConfig.print_ast
  );
  PRINT_VERBOSE("Beginning Parse\n");

  yyparse();

  // traversing the root could be expensive
  // we don't want to do it unless necessary
  // and because c++ is eager, if we didn't have this
  // if statement we would always traverse, even if we
  // didn't output anything
  if (util::CpslConfig.verbose) {
    PRINT_VERBOSE("Finished Parse\n");
    PRINT_VERBOSE("Final AST: %s\n", AST::print(root).c_str());
  }

  // the AST::Program root should now be defined

  if (util::CpslConfig.print_ast) {
    // if ast printing was requested, output lisp-like ast representation
    printf("%s\n", AST::print(root).c_str());
    return 0;
  }

  // output the final assembly
  printf("%s", gen::print(root).c_str());
  return 0;
}
