#include "codegen.hpp"
#include "types.hpp"
#include "util.hpp"
#include <utility>
#include <exception>
#include <numeric>
#include <cstring>

using namespace gen;

State::State() {
  this->init_regpool();
}

void State::init_regpool() {
  this->regpool.insert({mips::Register("$t0"), true});
  this->regpool.insert({mips::Register("$t1"), true});
  this->regpool.insert({mips::Register("$t2"), true});
  this->regpool.insert({mips::Register("$t3"), true});
  this->regpool.insert({mips::Register("$t4"), true});
  this->regpool.insert({mips::Register("$t5"), true});
  this->regpool.insert({mips::Register("$t6"), true});
  this->regpool.insert({mips::Register("$t7"), true});
  this->regpool.insert({mips::Register("$t8"), true});
  this->regpool.insert({mips::Register("$t9"), true});

  for (auto r : mips::saved_registers) {
    this->regpool.insert({r, true});
  }

  this->regpool.insert({mips::return_v0, true});
  this->regpool.insert({mips::return_v1, true});
  this->regpool.insert({mips::reg_a0, true});
  this->regpool.insert({mips::reg_a1, true});
  this->regpool.insert({mips::reg_a2, true});
  this->regpool.insert({mips::reg_a3, true});
}

// $t0-$t9 $a0-$a3 $v0-$v1
bool usable_reg(mips::Register reg) {
  if (reg == mips::stack_ptr) return false;
  if (reg == mips::global_ptr) return false;
  if (reg == mips::frame_ptr) return false;
  auto str_reg = static_cast<std::string>(reg);
  return (
     strncmp(str_reg.c_str(), "$t", 2) == 0
  || strncmp(str_reg.c_str(), "$s", 2) == 0
  );
}

mips::Register State::request_reg(mips::Register reg) {
  auto val = this->regpool[reg];
  if (val) {
    this->regpool[reg] = false;
    return reg;
  }
  throw std::runtime_error("Requested register " + static_cast<std::string>(reg) + " not available");
}

mips::Register State::request_reg() {
  for (auto pair : this->regpool) {
    if (pair.second && usable_reg(pair.first)) {
      auto ret = pair.first;
      // mark register as taken
      this->regpool[ret] = false;

      if (this->listening) {
        this->reg_listen.insert(ret);
      }
      return ret;
    }
  }
  // what do we do if a register is unavailable?
  throw std::runtime_error("No registers available FIXME");
}

void State::return_reg(mips::Register reg) {
  if (reg == mips::global_ptr || reg == mips::frame_ptr || reg == mips::stack_ptr)
    return;

  this->regpool[reg] = true;
}

void State::start_listen() {
  this->listening = true;
}

std::unordered_set<mips::Register> State::stop_listen() {
  auto v = this->reg_listen;
  this->reg_listen = std::unordered_set<mips::Register>{};
  return v;
}

// adds or finds a string in the data section and returns the label
mips::Label State::add_string(std::string str) {
  auto found = this->stringmap.find(str);
  if (found != this->stringmap.end()) {
    return mips::Label(found->second);
  }

  auto key = "str_" + std::to_string(this->stringmap.size());
  this->stringmap[str] = key;
  return mips::Label(key);
}

std::vector<mips::Declaration> State::string_declarations() {
  std::vector<mips::Declaration> vec;

  for (auto pair : this->stringmap) {
    auto name = mips::Label(pair.second);
    auto value = pair.first;
    auto type = mips::StorageType(".asciiz");

    vec.push_back(mips::Declaration{
      name
    , type
    , {AST::Value(value.c_str())}
    });
  }

  return vec;
}

mips::Label State::get_branch(std::string prefix) {
  return mips::Label(prefix + std::to_string(this->branchNum++));
}

void comment(mips::InstructionList& ilist, std::string c) {
  if (util::CpslConfig.verbose) {
    ilist.push_back(mips::comment(c));
  }
}

auto get_array_length(const AST::Array* array) {
  // arrays are inclusive - array[0:1] is an array of two elements
  return (array->endI + 1) - array->startI;
}

long get_size(const AST::Type& type) {
  switch (type.tag) {
    case AST::Type::Kind::VOID:
      return 0;
    case AST::Type::Kind::BOOL:
    case AST::Type::Kind::CHAR:
    case AST::Type::Kind::INT:
      return 4;
    case AST::Type::Kind::STR:
      // really just one since it's a reference to a constant memory location
      // we return 4 to keep it word-aligned
      return 4;
    case AST::Type::Kind::ARRAY: {
      auto range = get_array_length(type.array);
      auto cell_size = get_size(*type.array->of);
      return range * cell_size;
    }
    case AST::Type::Kind::RECORD: {
      auto table = type.record->table;
      long sum = 0;

      for (auto e : table.table) {
        sum += get_size(*types::get_type(*e.second));
      }

      return sum;
    }
  }
  throw std::runtime_error("get_size: did not handle all cases");
}

std::tuple<long, long, long, long> get_array_info(const AST::Type* type) {
  if (!types::has_type(type, AST::Type::Kind::ARRAY)) {
    throw std::runtime_error("Expected array access to have array type");
  }

  return {
    type->array->startI
  , type->array->endI
  , get_size(*type->array->of)
  , get_array_length(type->array)
  };
}

std::tuple<long, long, long, long> get_array_info(const AST::LValue& lval) {
  switch (lval.tag) {
    case AST::LValue::Kind::SYMBOL:
      return get_array_info(types::get_type(*lval.sym_val));
    case AST::LValue::Kind::RECORD:
      return get_array_info(types::get_type(*lval.rec_val->second));
    case AST::LValue::Kind::ARRAY:
      return get_array_info(types::get_type(*lval.array_val->first));
  }
  throw std::runtime_error("get_array_bounds: not all cases handled");
}

void return_address(const mips::Address& addr, State& state) {
  std::visit(util::match{
    // do nothing
    [](mips::Label) {},
    [&](const mips::OffsetAddress& a) {
        state.return_reg(a.loc);
    },
  }, addr);
}

std::tuple<mips::Register, mips::InstructionList> loadi(AST::Value val, State& state) {
  auto reg = state.request_reg();
  if (std::holds_alternative<std::string>(val)) {
    auto str_addr = state.add_string(std::get<std::string>(val));
    return {reg, {mips::loadaddr(reg, str_addr)}};
  }
  return {reg, {mips::loadi(reg, val)}};
}

std::tuple<mips::Register, mips::InstructionList> get_array_offset(
  const AST::LValue& lval,
  long idx,
  State& state
) {
  auto [low, hi, size, _len] = get_array_info(lval);
  if (idx > hi || idx < low)
    throw std::runtime_error(
      std::string("Array index out of bounds ")
    + std::to_string(low) + ":" + std::to_string(hi)
    + " - " + std::to_string(idx)
    );
  idx = idx - low;
  idx = idx * size;

  return loadi(AST::Value(idx), state);
}

std::vector<long> generate_array_offsets(const AST::Type* type) {
  auto [low, hi, size, len] = get_array_info(type);
  std::vector<long> vec;
  vec.reserve(len);

  for (auto idx = 0; idx < len; idx += size) {
    vec.push_back(idx);
  }

  return vec;
}

long int get_array_bound(const AST::Value& val) {
  return std::visit(util::match{
    [&](long int b) -> long int { return b; },
    [&](char c) -> long int { return (long int)c; },
    [&](std::string) -> long int { throw std::invalid_argument("Invalid bound"); },
    [&](type_safe::bool_t) -> long int { throw std::invalid_argument("Invalid bound"); },
  }, val);
}

std::tuple<mips::Register, mips::InstructionList> get_array_offset(
  const AST::LValue& lval,
  const AST::Expression& expr,
  State& state
) {
  if (std::holds_alternative<AST::Value>(expr))
    return get_array_offset(lval, get_array_bound(std::get<AST::Value>(expr)), state);

  auto [reg, ilist] = eval_expression(expr, state);
  auto [low, hi, size, _len] = get_array_info(lval);
  auto r2 = state.request_reg();
  util::insert_flat(ilist, {
    mips::addi(reg, reg, -low)
  , mips::loadi(r2, AST::Value(size))
  , mips::mult(reg, r2)
  , mips::mflo(reg)
  });
  state.return_reg(r2);

  return {reg, ilist};
}

std::tuple<mips::Address, mips::InstructionList> get_address(const AST::Symbol* sym, State& state) {
  PRINT_VERBOSE("Getting address offset at %d\n", sym->offset);
  auto realAddr = mips::OffsetAddress{ sym->offset, sym->offset_reg };
  // if symbol is reference, symbol's value will be an indirection to another address
  if (sym->ref) {
    auto reg = state.request_reg();
    auto i = mips::loadword(reg, realAddr);
    return {mips::OffsetAddress{ 0, reg }, {i}};
  }
  return {realAddr, {}};
}

std::tuple<mips::Address, mips::InstructionList> get_address(const AST::LValue& lval, State& state) {
  // TODO: fix this
  switch (lval.tag) {
    case AST::LValue::Kind::SYMBOL: {
      return get_address(lval.sym_val, state);
    }
    case AST::LValue::Kind::ARRAY: {
      auto [parent, expr] = *lval.array_val;
      auto [reg, ilist] = get_array_offset(lval, expr, state);
      auto [base_addr, ilist2] = get_address(*parent, state);
      util::insert_flat(ilist, ilist2);
      auto r2 = state.request_reg();
      util::insert_flat(ilist, {
        mips::loadaddr(r2, base_addr)
      , mips::add(reg, reg, r2)
      });
      // address has now been loaded into r2, can release its register if exists
      return_address(base_addr, state);
      state.return_reg(r2);
      return { mips::OffsetAddress{ 0, reg }, ilist };
    }
    case AST::LValue::Kind::RECORD: {
      auto [parent, sym] = *lval.rec_val;
      auto offset = sym->offset;
      auto reg = state.request_reg();
      auto [base_addr, ilist] = get_address(*parent, state);
      util::insert_flat(ilist, { mips::loadaddr(reg, base_addr), mips::addi(reg, reg, offset) });
      return { mips::OffsetAddress{ 0, reg }, ilist };
    }
  }
  throw std::runtime_error("store_word not all cases handled");
}

mips::InstructionList store_symbol(mips::Register reg, const AST::LValue& lval, State& state) {
  switch (lval.tag) {
    case AST::LValue::Kind::RECORD:
    case AST::LValue::Kind::ARRAY: {
      auto [address, ilist] = get_address(lval, state);
      ilist.push_back(mips::storeword(reg, address));
      return_address(address, state);
      return ilist;
    }
    case AST::LValue::Kind::SYMBOL: {
      auto [addr, ilist] = get_address(lval.sym_val, state);
      ilist.push_back(mips::storeword(reg, addr));
      return_address(addr, state);
      return ilist;
    }
  }
  throw std::runtime_error("store_word not all cases handled");
}

mips::InstructionList load_symbol(mips::Register reg, const AST::LValue& lval, State& state) {
  switch (lval.tag) {
    case AST::LValue::Kind::RECORD:
    case AST::LValue::Kind::ARRAY: {
      auto [address, ilist] = get_address(lval, state);
      ilist.push_back(mips::loadword(reg, address));
      return_address(address, state);
      return ilist;
    }
    case AST::LValue::Kind::SYMBOL: {
      auto [addr, ilist] = get_address(lval.sym_val, state);
      ilist.push_back(mips::loadword(reg, addr));
      return_address(addr, state);
      return ilist;
    }
  }
  throw std::runtime_error("load_word not all cases handled");
}

long init_array_of(const AST::Type* type) {
  if (types::is_intrinsic_type(type)) return get_size(*type);
  if (types::has_type(type, AST::Type::Kind::ARRAY)) {
    auto range = get_array_length(type->array);
    return range * init_array_of(type->array->of);
  }
  return gen::init_memory(type->record->table, 0);
}

long gen::init_sym_mem(long parent_offset, AST::Symbol* sym, mips::Register off_reg) {
  auto type = types::get_type(*sym);
  PRINT_VERBOSE("Initializing memory for symbol %s\n", sym->name.c_str());
  // set the symbols size to the calculated value
  // set the symbols offset to the current offset from the global pointer
  // TODO: this is probably not correct for all types
  // sym->offset = state.gpOffset;
  PRINT_VERBOSE("Symbol has size %d, offset %d\n", sym->size, sym->offset);
  sym->offset_reg = off_reg;
  sym->offset = parent_offset;
  if (sym->ref) {
    sym->size = 4;
    return 4;
  }

  // update global offset
  switch (type->tag) {
    case AST::Type::Kind::VOID:
      return 0;
    case AST::Type::Kind::BOOL:
    case AST::Type::Kind::CHAR:
    case AST::Type::Kind::INT:
    case AST::Type::Kind::STR: {
      sym->size = get_size(*type);
      return sym->size;
    }
    case AST::Type::Kind::ARRAY: {
      auto range = get_array_length(type->array);
      sym->size = range * init_array_of(type->array->of);
      return sym->size;
    }
    case AST::Type::Kind::RECORD: {
      sym->size = init_memory(sym->type->record->table, 0);
      // should now be set to size of whole record
      return sym->size;
    }
  }
  throw std::runtime_error("init_sym_mem: not all type cases handled");
}

long gen::init_memory(const AST::SymbolTable& table, long parent_offset, mips::Register off_reg) {
  return std::accumulate(table.table.begin(), table.table.end(), parent_offset, [&](auto accum, auto pair) {
    auto sym = pair.second;
    if (sym->const_val) return accum;
    return accum + init_sym_mem(accum, sym, off_reg);
  });
}

void print_symbol_mem(const std::string name, const AST::Symbol*);
void print_memory(const AST::SymbolTable& table) {
  PRINT_VERBOSE("%-15s%-25s%-10s%-10s\n", "Name", "Type", "Size", "Offset");
  for (auto [name, sym] : table.table) {
    print_symbol_mem(name, sym);
  }
}

void print_symbol_mem(const std::string name, const AST::Symbol* sym) {
  auto type = types::get_type(*sym);
  switch (type->tag) {
    case AST::Type::Kind::VOID: break;
    case AST::Type::Kind::BOOL:
    case AST::Type::Kind::CHAR:
    case AST::Type::Kind::INT:
    case AST::Type::Kind::STR: {
      PRINT_VERBOSE("%-15s%-25s%-10d%-10d\n", name.c_str(), AST::print(*type).c_str(), sym->size, sym->offset);
      break;
    }
    case AST::Type::Kind::ARRAY: {
      PRINT_VERBOSE("%-15s%-25s%-10d%-10d\n", name.c_str(), AST::print(*type).c_str(), sym->size, sym->offset);
      auto str = "ARR[" + std::to_string(get_array_length(type->array)) + "] OF:";
      PRINT_VERBOSE("%-15s%-25s%-10d\n", str.c_str(), AST::print(*type->array->of).c_str(), get_size(*type->array->of));
      if (types::has_type(type->array->of, AST::Type::Kind::RECORD)) {
        print_memory(type->array->of->record->table);
      }
      PRINT_VERBOSE("\n\n");
      break;
    }
    case AST::Type::Kind::RECORD: {
      PRINT_VERBOSE("%-15s%-25s%-10d%-10d\n", name.c_str(), AST::print(*type).c_str(), sym->size, sym->offset);
      PRINT_VERBOSE("RECORD FIELDS:\n\n");
      print_memory(type->record->table);
      PRINT_VERBOSE("\n\n");
      break;
    }
  }
}

std::string gen::print(const AST::Program& prog) {
  State state;
  state.gpOffset = init_memory(prog.table, 0);
  if (util::CpslConfig.verbose) {
    // print memory offsets
    PRINT_VERBOSE("final GP offset: %d\n", state.gpOffset);
    print_memory(prog.table);
  }

  for (auto [key, fn] : prog.ftable.table) {
    auto [sig, body] = fn;
    if (!body) {
      throw std::runtime_error("Function " + sig.name + " has been declared but not implemented");
    }

    // initialize memory for functions

    /*
      Argument N      <--- N*4+4($fp)
      ...
      Argument 2      <--- 12 ($fp)
      Argument 1      <--- 8($fp)
      Return address  <--- 4($fp)
      Old $fp        <--- (%esp) and ($fp)
      $s0            <--- -4($fp)
      Local variable1 <--- -8($fp)
      Local variable2 <--- -12($fp) and (%esp)
    */

    // initially set to 8 : offset 4 from saved frame pointer + offset 4 for saved return address
    auto param_size = std::accumulate(
      sig.params.begin(), sig.params.end(),
      8, [&](int param_size, auto& p) {
        auto name = p.name;
        auto sym = AST::lookup(body->table, name);
        return param_size + init_sym_mem(param_size, sym, mips::frame_ptr);
      }
    );

    state.fntable[key] = { sig, *body, param_size - 8, get_stmts_call(body->statements) };
  }

  auto statements = gen::eval_stmts(prog.statements, state);
  // add main label
  statements.insert(statements.begin(), mips::from_label(mips::Label("main")));
  // ensure we properly terminate
  auto last = statements[statements.size() - 1];
  if (!std::holds_alternative<mips::Instruction>(last)
   || std::get<mips::Instruction>(last) != mips::stop) {
    statements.push_back(mips::stop);
  }
  auto fns = gen::eval_functions(state);
  util::insert_flat(statements, fns);

  auto declarations = state.string_declarations();
  // TODO: insert the .global main declaration

  return mips::print(mips::Program{ { declarations }, { statements } });
}

mips::InstructionList gen::eval_functions(State& state) {
  mips::InstructionList ilist;
  for (auto [_key, fn] : state.fntable) {
    util::insert_flat(ilist, gen::eval_fn(fn, state));
  }

  return ilist;
}

std::vector<AST::Call> gen::get_expr_call(const AST::Expression& e) {
  return std::visit(util::match {
    [](const AST::OpPtr& ptr) {
      return std::accumulate(ptr->operands.begin(), ptr->operands.end(), std::vector<AST::Call>{}, [](auto& a, auto c) {
        util::insert_flat(a, get_expr_call(c));
        return a;
      });
    }
  , [](const AST::LValPtr& ptr) {
      if (ptr->tag == AST::LValue::Kind::SYMBOL) return std::vector<AST::Call>{};
      if (ptr->tag == AST::LValue::Kind::ARRAY) {
        return util::concat(get_expr_call(*ptr->array_val->first), get_expr_call(ptr->array_val->second));
      }
      if (ptr->tag == AST::LValue::Kind::RECORD) {
        return get_expr_call(*ptr->array_val->first);
      }
      throw std::runtime_error("Not all cases handled lval");
    }
  , [](const AST::Value) {return std::vector<AST::Call>{};}
  , [](const AST::CallPtr& call) {
      auto call_expr_calls = std::accumulate(
        call->args.begin()
      , call->args.end()
      , std::vector<AST::Call>{}
      , [](auto& accum, auto c) {
          util::insert_flat(accum, get_expr_call(c));
          return accum;
        }
      );
      call_expr_calls.push_back(*call);
      return call_expr_calls;
    }
  }, e);
}

std::vector<AST::Call> gen::get_stmt_call(const AST::Statement& stmt) {
  return std::visit(util::match {
    [](const AST::Assignment& assn) {
      return util::concat(get_expr_call(assn.lval), get_expr_call(assn.expr));
    }
  , [](const AST::Stop) -> std::vector<AST::Call> {return {};}
  , [](const AST::WriteStmt& write) {
      return std::accumulate(write.begin(), write.end(), std::vector<AST::Call>{}, [](auto& a, auto c) {
        util::insert_flat(a, get_expr_call(c));
        return a;
      });
    }
  , [](const AST::ReadStmt& read) {
      return std::accumulate(read.begin(), read.end(), std::vector<AST::Call>{}, [](auto& a, auto c) {
        util::insert_flat(a, get_expr_call(c));
        return a;
      });
    }
  , [](const AST::IfStmtPtr& ptr) {
      return (
        util::concat(
          util::concat(
            util::concat(
              get_expr_call(ptr->pred)
            , get_stmts_call(ptr->body)
            )
          , get_stmts_call(ptr->else_body)
          )
        , std::accumulate(
            ptr->elseifs.begin()
          , ptr->elseifs.end()
          , std::vector<AST::Call>{}
          , [](auto& a, auto c) {
            util::insert_flat(a, get_expr_call(c.pred));
            util::insert_flat(a, get_stmts_call(c.body));
            return a;
          })
        )
      );
    }
  , [](const AST::WhilePtr& ptr) {
      return util::concat(get_expr_call(ptr->pred), get_stmts_call(ptr->body));
    }
  , [](const AST::RepeatPtr& ptr) {
      return util::concat(get_expr_call(ptr->pred), get_stmts_call(ptr->body));
    }
  , [](const AST::ForPtr& ptr) {
      return (
        util::concat(
          util::concat(
            get_stmt_call(ptr->init)
          , get_expr_call(ptr->end)
          )
        , get_stmts_call(ptr->body)
        )
      );
    }
  , [](const AST::Return& ret) {
      if (ret.value.has_value()) return get_expr_call(*ret.value);
      return std::vector<AST::Call>{};
    }
  , [](const AST::Call& call) {
      auto call_expr_calls = std::accumulate(
        call.args.begin()
      , call.args.end()
      , std::vector<AST::Call>{}
      , [](auto& accum, auto c) {
          util::insert_flat(accum, get_expr_call(c));
          return accum;
        }
      );
      call_expr_calls.push_back(call);
      return call_expr_calls;
    }

  }, stmt);
}

std::vector<AST::Call> gen::get_stmts_call(const AST::StatementList& stmts) {
  return std::accumulate(stmts.begin(), stmts.end(), std::vector<AST::Call>{}, [](auto& a, auto c) {
    util::insert_flat(a, get_stmt_call(c));
    return a;
  });
}

bool find_reg(std::string i, mips::Register reg) {
  return i.find(static_cast<std::string>(reg)) != std::string::npos;
}

std::tuple<mips::InstructionList, mips::InstructionList, std::unordered_set<mips::Register>>
save_callee(std::unordered_set<mips::Register> all_used, State& state) {
  mips::InstructionList save;
  mips::InstructionList restore;
  std::unordered_set<mips::Register> used;

  for (auto r : mips::saved_registers) {
    if (all_used.find(r) != all_used.end()) {
      used.insert(r);
      util::insert_flat(save, mips::pushl(r));
      // do it in reverse order
      restore = util::concat(mips::popl(r), restore);
      // allow the register to be used
      state.return_reg(r);
    }
  }

  return {save, restore, used};
}

mips::InstructionList gen::eval_fn(const Fn& fn, State& state) {
  mips::InstructionList ilist;
  auto sig = fn.sig;
  auto body = fn.body;
  // auto is_leaf = fn.calls.size() == 0;
  state.currFn = fn;

  ilist.push_back(mips::Label(sig.label));

  /*
  Setup

  During the setup, the following two instructions are carried out immediately:

  pushl %ebp
  movl %esp %ebp
  */
  // saves the old frame pointer, sets the frame pointer to the current
  // stack pointer
  comment(ilist, "saving frame pointer");
  util::insert_flat(ilist, mips::pushl(mips::frame_ptr));
  ilist.push_back(mips::move(mips::frame_ptr, mips::stack_ptr));

  // initialize memory for local variables
  std::unordered_map<std::string, AST::Symbol*> table;

  auto t = body.table;
  // only initialize non-parameters (local vars)
  for (auto& pair : t.table) {
    if (!pair.second->param) {
      table.emplace(pair);
    }
  }

  long local_size = 0;
  // old frame pointer is at 0($fp), so we offset by 4
  std::accumulate(table.begin(), table.end(), (long)-4, [&](auto accum, auto pair) {
    auto sym = pair.second;
    if (sym->const_val) return accum;
    auto size = init_sym_mem(accum, sym, mips::frame_ptr);
    local_size += size;
    return accum - size;
  });

  // allocate local variable stack space (base_offset has already been allocated in save_calle)
  if (local_size) {
    comment(ilist, "allocate stack for local variables");
    ilist.push_back(mips::push_stack(local_size));
  }

  // get callee saved registers
  state.start_listen();
  auto body_ilist = eval_stmts(body.statements, state);
  auto all_used = state.stop_listen();
  auto [save, restore, used] = save_callee(all_used, state);

  // save callee saved registers
  comment(ilist, "save used registers");
  util::insert_flat(ilist, save);

  // execute body
  comment(ilist, "function body");
  util::insert_flat(ilist, body_ilist);

  // label for return
  ilist.push_back(mips::Label(sig.label + "ret"));

  // restore saved registers
  comment(ilist, "restore saved registers");
  util::insert_flat(ilist, restore);

  // deallocate local stack space
  if (local_size) {
    comment(ilist, "deallocate local stack space");
    ilist.push_back(mips::pop_stack(local_size));
  }

  for (auto reg : used) { state.request_reg(reg); }

  // restore frame pointer
  comment(ilist, "restore frame pointer");
  ilist.push_back(mips::move(mips::stack_ptr, mips::frame_ptr));
  util::insert_flat(ilist, mips::popl(mips::frame_ptr));

  // caller restores $ra after return
  comment(ilist, "return to call site");
  ilist.push_back(mips::jump_reg(mips::return_addr));

  state.currFn = std::nullopt;
  return ilist;
}

mips::InstructionList gen::eval_stmts(const AST::StatementList& stmts, State& state) {
  mips::InstructionList accum;
  for (auto stmt : stmts) {
    auto res = gen::eval_stmt(stmt, state);
    PRINT_VERBOSE("%s -> %s\n\n", AST::print(stmt).c_str(), mips::print(res).c_str());
    util::insert_flat(accum, res);
    // special case for stop statement - we don't need to continue generation
    // I'm not sure how this works - won't it need runtime type information?
    // no, because this is just checking a tag value in a tagged union - which already
    // exists at runtime
    if (res.size() == 1) {
      auto last = res[0];
      if (std::holds_alternative<mips::Instruction>(last)
       && std::get<mips::Instruction>(last) == mips::stop) {
        return accum;
      }
    }
  }

  return accum;
}

mips::InstructionList gen::eval_stmt(const AST::Statement& stmt, State& state) {
  auto list = std::visit(util::match{
    [&](const AST::WriteStmt& write) {
      return gen::eval_write(write, state);
    }
  , [&](const AST::ReadStmt& read) {
      return gen::eval_read(read, state);
    }
  , [&](const AST::Assignment& assn) {
      return gen::eval_assn(assn, state);
    }
  , [](const AST::Stop&) -> mips::InstructionList {
      return {mips::stop};
    }
  , [&](const AST::IfStmtPtr& ptr) {
      return eval_if(*ptr, state);
    }
  , [&](const AST::WhilePtr& ptr) {
      return eval_while(*ptr, state);
    }
  , [&](const AST::RepeatPtr& ptr) {
      return eval_repeat(*ptr, state);
    }
  , [&](const AST::ForPtr& ptr) {
      return eval_for(*ptr, state);
    }
  , [&](const AST::Return& r) {
      return eval_return(r, state);
    }
  , [&](const AST::Call call) {
      return eval_call(call, state);
    }
  }, stmt);

  if (util::CpslConfig.verbose) {
    list.insert(list.begin(), mips::comment(AST::print(stmt)));
  }

  return list;
}

mips::InstructionList gen::eval_return(const AST::Return& ret, State& state) {
  auto fn = state.currFn;
  if (!fn) throw std::runtime_error("Cannot return outside of function");
  auto sig = fn->sig;
  auto jump = mips::jump(mips::Label(sig.label + "ret"));
  if (!ret.value) return {jump};

  auto [reg, ilist] = eval_expression(*ret.value, state);
  ilist.push_back(mips::move(mips::return_v0, reg));
  ilist.push_back(jump);
  state.return_reg(reg);
  return ilist;
}

mips::Instruction write_for_type(const AST::Type& type) {
  switch (type.tag) {
    case AST::Type::Kind::INT: {
      return mips::writeint;
    }
    case AST::Type::Kind::CHAR: {
      return mips::writechar;
    }
    case AST::Type::Kind::BOOL: {
      return mips::writeint;
    }
    case AST::Type::Kind::STR: {
      return mips::writestr;
    }
    default:
      throw std::runtime_error("Values cannot be user types");
  }
  throw std::runtime_error("write_for_type: not all cases handled");
}

mips::InstructionList write_from(
  const mips::Register reg,
  const AST::Type& type,
  State& state
) {
  state.return_reg(reg);
  return {mips::move(mips::Register("$a0"), reg), write_for_type(type)};
}

mips::InstructionList write_const(const AST::Value& val, State& state) {
  return {
    types::has_type(val, AST::Type::Kind::STR)
      ? mips::Instruction(mips::loadaddr(mips::Register("$a0"), state.add_string(std::get<std::string>(val))))
      : mips::Instruction(mips::loadi(mips::Register("$a0"), val)),
    write_for_type(*types::get_type(val))
  };
}

mips::InstructionList gen::eval_write(const AST::WriteStmt& stmt, State& state) {
  mips::InstructionList accum;
  for (auto expr : stmt) {
    if (types::is_const(expr)) {
      if (!std::holds_alternative<AST::Value>(expr)) {
        throw std::runtime_error(
          "Somehow expression marked constant without being value"
        );
      }
      util::insert_flat(accum, write_const(std::get<AST::Value>(expr), state));
    } else {
      // expression needs to return assembly for the expression, and what register
      // it was stored in
      auto [reg, assm] = eval_expression(expr, state);
      util::insert_flat(accum, assm);
      util::insert_flat(accum, write_from(reg, *types::get_type(expr), state));
    }
  }

  return accum;
}

mips::InstructionList read(const AST::LValue& lval, State& state) {
  mips::Instruction readi;
  auto type = types::get_type(lval);
  switch (type->tag) {
    case AST::Type::Kind::CHAR:
      readi = mips::readchar;
      break;
    case AST::Type::Kind::INT:
      readi = mips::readint;
      break;
    default:
      throw std::runtime_error("Attempted to read invalid type " + AST::typekind_str(types::get_type(lval)->tag));
  }

  auto [addr, ilist] = get_address(lval, state);
  util::insert_flat(ilist, {
    readi
  , mips::storeword(mips::return_v0, addr)
  });
  return_address(addr, state);
  return ilist;
}

mips::InstructionList gen::eval_read(const AST::ReadStmt& stmt, State& state) {
  mips::InstructionList accum;
  for (auto lval : stmt) {
    util::insert_flat(accum, read(lval, state));
  }
  return accum;
}

// returns pair (register, asm instruction)
std::tuple<mips::Register, mips::InstructionList> gen::eval_expression(
  const AST::Expression& expr,
  State& state
) {
  return std::visit(util::match{
    [&](const AST::Value& val) {
      return loadi(val, state);
    }
  , [&](const AST::LValPtr& ptr) -> std::tuple<mips::Register, mips::InstructionList> {
      auto reg = state.request_reg();
      return {reg, load_symbol(reg, *ptr, state)};
    }
  , [&](const AST::OpPtr& ptr) {
      return gen::eval_op(*ptr, state);
    }
  , [&](const AST::CallPtr& ptr) -> std::tuple<mips::Register, mips::InstructionList> {
      return { mips::return_v0, gen::eval_call(*ptr, state) };
    }
  }, expr);
}

// $t0-$t9 $a0-$a3 $v0-$v1
bool caller_saved(mips::Register reg) {
  auto str_reg = static_cast<std::string>(reg);
  return (
     std::strncmp(str_reg.c_str(), "$t", 2) == 0
  || std::strncmp(str_reg.c_str(), "$a", 2) == 0
  || std::strncmp(str_reg.c_str(), "$v", 2) == 0
  );
}

std::unordered_set<mips::Register> get_used_caller(State& state) {
  std::unordered_set<mips::Register> used;
  for (auto pair : state.regpool) {
    // if the register is in use, and is a caller-saved register
    if (!pair.second && caller_saved(pair.first)) {
      used.insert(pair.first);
    }
  }

  return used;
}

std::tuple<mips::InstructionList, mips::InstructionList, std::unordered_set<mips::Register>>
save_caller(State& state) {
  mips::InstructionList save;
  mips::InstructionList restore;
  std::unordered_set<mips::Register> used = get_used_caller(state);

  for (auto r : used) {
    util::insert_flat(save, mips::pushl(r));
    // do it in reverse order
    restore = util::concat(mips::popl(r), restore);
    state.return_reg(r);
  }

  return {save, restore, used};
}

mips::InstructionList gen::eval_call(const AST::Call& call, State& state) {
  // save any used registers
  auto [ilist, restore, used] = save_caller(state);

  // add parameters
  auto param_size = 0;
  mips::InstructionList add_params;
  for (size_t i = 0; i < call.args.size(); ++i) {
    auto e = call.args[i];
    auto name = call.sig.params[i].name;
    auto ref = call.sig.params[i].ref;
    auto type = types::get_type(e);
    auto size = get_size(*type);
    auto addr = mips::OffsetAddress{ param_size, mips::stack_ptr };
    comment(add_params, "add parameter: " + name);
    util::insert_flat(add_params, save_location(addr, e, ref, true, state));
    param_size += size;
  }

  if (param_size > 0) {
    comment(ilist, "add parameters to stack");
    ilist.emplace_back(mips::push_stack(param_size));
    util::insert_flat(ilist, add_params);
  }

  // saves the current return address
  comment(ilist, "call function " + call.sig.name);
  util::insert_flat(ilist, mips::call(mips::Label(call.sig.label)));

  // restore $r0
  comment(ilist, "restore return address");
  util::insert_flat(ilist, mips::popl(mips::return_addr));

  // restore stack from parameters
  comment(ilist, "restore parameter stack space");
  ilist.push_back(mips::pop_stack(param_size));

  // restore saved registers
  if (used.size() > 0) {
    comment(ilist, "restore saved registers");
    util::insert_flat(ilist, restore);
    for (auto reg : used) { state.request_reg(reg); }
  }

  return ilist;
}

std::tuple<mips::Register, mips::InstructionList> eval_two(
  const AST::Expression& left,
  const AST::Expression& right,
  std::function<mips::InstructionList(mips::Register, mips::Register, mips::Register)> f,
  State& state
) {
  auto [left_reg, lefti] = eval_expression(left, state);
  auto [right_reg, righti] = eval_expression(right, state);
  auto ret = util::concat(
    util::concat(lefti, righti),
    f(left_reg, left_reg, right_reg)
  );
  state.return_reg(right_reg);
  return { left_reg, ret };
}

std::tuple<mips::Register, mips::InstructionList> eval_two(
  const AST::Expression& left,
  const AST::Expression& right,
  std::function<mips::Instruction(mips::Register, mips::Register, mips::Register)> f,
  State& state
) {
  auto [left_reg, lefti] = eval_expression(left, state);
  auto [right_reg, righti] = eval_expression(right, state);
  auto combined = util::concat(lefti, righti);
  combined.push_back(f(left_reg, left_reg, right_reg));
  state.return_reg(right_reg);
  return { left_reg, combined };
}

// only use with commutative operations - order is not preserved
// std::pair<AST::Value&, AST::Expression&> get_valued

std::tuple<mips::Register, mips::InstructionList> add_value_expr(
  const AST::Value& val,
  const AST::Expression& right,
  State& state
) {
  auto [reg, ilist] = gen::eval_expression(right, state);
  auto n = std::get<long int>(val);
  // no point in adding zero
  if (n != 0) {
    // we should be able to addi and re-store in the same register
    ilist.push_back(mips::addi(reg, reg, n));
  }
  return {reg, ilist};
}

std::tuple<mips::Register, mips::InstructionList> add(
  const AST::Expression& left,
  const AST::Expression& right,
  State& state
)  {
  // if one of the expressions is a Value, we can addi, otherwise
  if (std::holds_alternative<AST::Value>(left)) {
    return add_value_expr(std::get<AST::Value>(left), right, state);
  } else if (std::holds_alternative<AST::Value>(right)) {
    // addition is commutative, so order does not matter
    return add_value_expr(std::get<AST::Value>(right), left, state);
  }
  return eval_two(left, right, mips::add, state);
}

std::tuple<mips::Register, mips::InstructionList> sub_value_expr(
  const AST::Expression& left,
  const AST::Value& right,
  State& state
) {
  auto [reg, ilist] = gen::eval_expression(left, state);
  auto n = std::get<long int>(right);
  // no point in subtracting zero
  if (n != 0) {
    ilist.push_back(mips::subi(reg, reg, n));
  }
  return {reg, ilist};
}

std::tuple<mips::Register, mips::InstructionList> sub_value_expr(
  const AST::Value& left,
  const AST::Expression& right,
  State& state
) {
  auto [reg, ilist] = gen::eval_expression(right, state);
  auto left_val = std::get<long int>(left);
  // special case for subtracting from zero
  if (left_val == 0) {
    ilist.push_back(mips::negate(reg, reg));
    return {reg, ilist};
  }
  auto ireg = state.request_reg();
  // since SubI can only be used in cases of Reg - constant
  // we must load our value into a register and use a normal subtraction
  ilist.push_back(mips::loadi(ireg, left_val));
  ilist.push_back(mips::sub(reg, ireg, reg));
  state.return_reg(ireg);
  return {reg, ilist};
}

std::tuple<mips::Register, mips::InstructionList> sub(
  const AST::Expression& left,
  const AST::Expression& right,
  State& state
) {
  if (std::holds_alternative<AST::Value>(left)) {
    return sub_value_expr(std::get<AST::Value>(left), right, state);
  } else if (std::holds_alternative<AST::Value>(right)) {
    return sub_value_expr(left, std::get<AST::Value>(right), state);
  } else {
    return eval_two(left, right, mips::sub, state);
  }
}

std::tuple<mips::Register, mips::InstructionList> mult(
  const AST::Expression& left,
  const AST::Expression& right,
  State& state
) {
  // special case for the multiplicative identity
  if (std::holds_alternative<AST::Value>(right) && std::get<long int>(std::get<AST::Value>(right)) == 1) {
    return eval_expression(left, state);
  } else if (std::holds_alternative<AST::Value>(left) && std::get<long int>(std::get<AST::Value>(left)) == 1) {
    return eval_expression(right, state);
  }

  // we don't have any sort of mult immediate, so constants (other than 1) must be loaded first
  // this is done by our eval_expression function
  return eval_two(left, right, mips::multload, state);
}

std::tuple<mips::Register, mips::InstructionList> div(
  const AST::Expression& left,
  const AST::Expression& right,
  State& state
) {
  // special case for identity
  if (std::holds_alternative<AST::Value>(right) && std::get<long int>(std::get<AST::Value>(right)) == 1) {
    return eval_expression(left, state);
  }
  // we don't have any sort of div immediate, so constants must be loaded first
  // this is done by our eval_expression function
  return eval_two(left, right, mips::divload, state);
}

std::tuple<mips::Register, mips::InstructionList> mod(
  const AST::Expression& left,
  const AST::Expression& right,
  State& state
) {
  // every integer is divisible by one
  if (std::holds_alternative<AST::Value>(right) && std::get<long int>(std::get<AST::Value>(right)) == 1) {
    auto reg = state.request_reg();
    return { reg, { mips::loadi(reg, AST::Value((long int)0)) } };
  }
  // we don't have any sort of div immediate, so constants must be loaded first
  // this is done by our eval_expression function
  return eval_two(left, right, mips::modulus, state);
}

std::tuple<mips::Register, mips::InstructionList> lt(
  const AST::Expression& left,
  const AST::Expression& right,
  State& state
) {
  if (std::holds_alternative<AST::Value>(right)) {
    auto [reg, ilist] = eval_expression(left, state);
    ilist.push_back(mips::setlti(reg, reg, std::get<AST::Value>(right)));
    return {reg, ilist};
  }

  return eval_two(left, right, mips::setlt, state);
}

std::tuple<mips::Register, mips::InstructionList> pred(
  const AST::Expression& exp,
  State& state
) {
  auto type = types::get_type(exp);
  if (!types::is_ordered_type(type)) {
    throw std::runtime_error("Pred called with invalid type" + AST::typekind_str(type->tag));
  }

  if (type->tag == AST::Type::Kind::INT || type->tag == AST::Type::Kind::CHAR) {
    return add_value_expr(AST::Value((long int)-1), exp, state);
  }

  auto [reg, ilist] = eval_expression(exp, state);
  util::insert_flat(ilist, mips::bool_not(reg, reg));
  return { reg, ilist };
}

std::tuple<mips::Register, mips::InstructionList> succ(
  const AST::Expression& exp,
  State& state
) {
  auto type = types::get_type(exp);
  if (!types::is_ordered_type(type)) {
    throw std::runtime_error("Succ called with invalid type" + AST::typekind_str(type->tag));
  }

  if (type->tag == AST::Type::Kind::INT || type->tag == AST::Type::Kind::CHAR) {
    return add_value_expr(AST::Value((long int)1), exp, state);
  }

  auto [reg, ilist] = eval_expression(exp, state);
  util::insert_flat(ilist, mips::bool_not(reg, reg));
  return { reg, ilist };
}

std::tuple<mips::Register, mips::InstructionList> gen::eval_op(
  const AST::Operation& op,
  State& state
) {
  switch (op.op) {
    case AST::Operation::Kind::ADD:
      return add(op.operands[0], op.operands[1], state);
    case AST::Operation::Kind::SUB:
      return sub(op.operands[0], op.operands[1], state);
    case AST::Operation::Kind::MULT:
      return mult(op.operands[0], op.operands[1], state);
    case AST::Operation::Kind::DIV:
      return div(op.operands[0], op.operands[1], state);
    case AST::Operation::Kind::MOD:
      return mod(op.operands[0], op.operands[1], state);
    case AST::Operation::Kind::LT:
      return lt(op.operands[0], op.operands[1], state);
    case AST::Operation::Kind::LTE:
      return eval_two(op.operands[0], op.operands[1], mips::setlte, state);
    case AST::Operation::Kind::GT:
      return eval_two(op.operands[0], op.operands[1], mips::setgt, state);
    case AST::Operation::Kind::GTE:
      return eval_two(op.operands[0], op.operands[1], mips::setgte, state);
    case AST::Operation::Kind::EQ:
      return eval_two(op.operands[0], op.operands[1], mips::seteq, state);
    case AST::Operation::Kind::NEQ:
      return eval_two(op.operands[0], op.operands[1], mips::setneq, state);
    case AST::Operation::Kind::CHR:
    case AST::Operation::Kind::ORD:
      return eval_expression(op.operands[0], state);
    case AST::Operation::Kind::PRED:
      return pred(op.operands[0], state);
    case AST::Operation::Kind::SUCC:
      return succ(op.operands[0], state);
    case AST::Operation::Kind::AND:
      return eval_two(op.operands[0], op.operands[1], mips::bool_and, state);
    case AST::Operation::Kind::OR:
      return eval_two(op.operands[0], op.operands[1], mips::bool_or, state);
    case AST::Operation::Kind::NOT: {
      auto [reg, ilist] = eval_expression(op.operands[0], state);
      util::insert_flat(ilist, mips::bool_not(reg, reg));
      return { reg, ilist };
    }
  }
  throw std::runtime_error("eval_op: not all cases handled");
}

// in the case of assignment of UDTs (objects)
// we must either allow references, or implicitly copy on assignment
// copying is the simpler method
mips::InstructionList gen::copy_store(const mips::Address& toaddr, const mips::Address& fromaddr, const AST::Type* type, State& state) {
  mips::InstructionList ilist;

  if (types::has_type(type, AST::Type::Kind::ARRAY)) {
    auto toReg = state.request_reg();
    auto fromReg = state.request_reg();

    ilist.push_back(mips::loadaddr(toReg, toaddr));
    ilist.push_back(mips::loadaddr(fromReg, fromaddr));

    auto [low, hi, size, len] = get_array_info(type);

    for (auto offset = 0; offset < len * size; offset += size) {
      auto toaddr = mips::OffsetAddress{ offset, toReg };
      auto fromaddr = mips::OffsetAddress{ offset, fromReg };
      util::insert_flat(ilist, copy_store(toaddr, fromaddr, type->array->of, state));
    }

    state.return_reg(toReg);
    state.return_reg(fromReg);
  } else if (types::has_type(type, AST::Type::Kind::RECORD)) {
    auto toReg = state.request_reg();
    auto fromReg = state.request_reg();

    ilist.push_back(mips::loadaddr(toReg, toaddr));
    ilist.push_back(mips::loadaddr(fromReg, fromaddr));

    auto table = type->record->table;
    for (auto [name, sym] : table.table) {
      auto offset = sym->offset;
      auto toaddr = mips::OffsetAddress{ offset, toReg };
      auto fromaddr = mips::OffsetAddress{ offset, fromReg };
      util::insert_flat(ilist, copy_store(toaddr, fromaddr, sym->type, state));
    }

    state.return_reg(toReg);
    state.return_reg(fromReg);
  } else {
    auto temp = state.request_reg();
    ilist.push_back(mips::loadword(temp, fromaddr));
    ilist.push_back(mips::storeword(temp, toaddr));
    state.return_reg(temp);
  }

  return ilist;
}

mips::InstructionList gen::save_location(const AST::LValue& lval, const AST::Expression& expr, State& state) {
  // do nothing in the case of assignment to self
  if (std::holds_alternative<AST::LValPtr>(expr)) {
    auto elval = *std::get<AST::LValPtr>(expr);
    if (elval.tag == AST::LValue::Kind::SYMBOL && lval.sym_val == elval.sym_val)
      return {};

  }
  auto ref = lval.tag == AST::LValue::Kind::SYMBOL && lval.sym_val->ref;

  auto [toaddr, ilist] = get_address(lval, state);
  util::insert_flat(ilist, save_location(toaddr, expr, ref, false, state));
  return_address(toaddr, state);
  return ilist;
}

mips::InstructionList save_address_to_mem(
  const mips::Address& to,
  const mips::Address& from,
  State& state
) {
  auto reg = state.request_reg();
  state.return_reg(reg);
  return {mips::loadaddr(reg, from), mips::storeword(reg, to)};
}

mips::InstructionList save_value_to_mem(
  const mips::Address& to,
  const mips::Address& from,
  State& state
) {
  auto reg = state.request_reg();
  state.return_reg(reg);
  return {mips::loadword(reg, from), mips::storeword(reg, to)};
}

mips::InstructionList gen::save_location(const mips::Address& to, const AST::Expression& expr, bool ref, bool param, State& state) {
  auto type = types::get_type(expr);
  if (ref) {
    if (!std::holds_alternative<AST::LValPtr>(expr))
      throw std::runtime_error("Cannot pass a non-lvalue to a reference parameter");

    // store the memory address instead
    auto elval = *std::get<AST::LValPtr>(expr);
    auto [addr, ilist] = get_address(elval, state);
    if (param) {
      util::insert_flat(ilist, save_address_to_mem(to, addr, state));
    } else {
      util::insert_flat(ilist, save_value_to_mem(to, addr, state));
    }
    return_address(addr, state);
    return ilist;
  }

  if (!types::is_intrinsic_type(type)) {
    // we have a copy. Must be assigning a udt to another udt
    // what do we do?
    // treat the expression as an lvalue
    // (once functions are implemented we should also be able to copy a returnvalue)
    if (!std::holds_alternative<AST::LValPtr>(expr)) {
      throw std::runtime_error("Cannot copy-assign a non-lvalue at this time");
    }

    auto elval = *std::get<AST::LValPtr>(expr);
    auto [fromaddr, ilist] = get_address(elval, state);
    util::insert_flat(ilist, copy_store(to, fromaddr, type, state));
    return_address(fromaddr, state);
    return ilist;
  }

  auto [reg, assm] = eval_expression(expr, state);
  assm.push_back(mips::storeword(reg, to));
  state.return_reg(reg);
  return assm;
}

mips::InstructionList gen::eval_assn(const AST::Assignment& assn, State& state) {
  auto lval = assn.lval;
  auto expr = assn.expr;
  return save_location(lval, expr, state);
}

mips::InstructionList eval_elseif_const(
  const AST::Value& pred
, const AST::StatementList& body
, const mips::Label& end_label
, State& state
) {
  auto boolval = std::get<type_safe::bool_t>(pred);
  if (boolval) {
    return util::concat(gen::eval_stmts(body, state), {mips::jump(end_label)});
  }

  return {};
}

mips::InstructionList eval_elseif(const AST::ElseIf& stmt, const mips::Label& end_label, State& state) {
  auto pred = stmt.pred;
  auto body = stmt.body;
  if (types::is_const(pred)) {
    if (!std::holds_alternative<AST::Value>(pred)) {
      throw std::runtime_error(
        "Somehow expression marked constant without being value"
      );
    }
    return eval_elseif_const(std::get<AST::Value>(pred), body, end_label, state);
  }
  // evaluate predicate
  auto [reg, ilist] = eval_expression(pred, state);

  // branch if predicate false
  auto elseif_lbl = state.get_branch("b_elif");
  ilist.push_back(mips::bfalse(reg, elseif_lbl));
  state.return_reg(reg);

  // evaluate body
  util::insert_flat(ilist, eval_stmts(body, state));
  // unconditional jump to skip else only taken if body evaluated
  ilist.emplace_back(mips::jump(end_label));
  // end with elseif label
  ilist.emplace_back(elseif_lbl);
  return ilist;
}

mips::InstructionList eval_if_const(const AST::Value& pred, const AST::IfStatement& ifstmt, State& state) {
  auto boolval = std::get<type_safe::bool_t>(pred);
  if (boolval) {
    return gen::eval_stmts(ifstmt.body, state);
  }

  mips::InstructionList ilist;
  auto end_label = state.get_branch();
  // evaluate else ifs
  for (auto stmt : ifstmt.elseifs) {
    util::insert_flat(ilist, eval_elseif(stmt, end_label, state));
  }
  // evaluate else
  util::insert_flat(ilist, gen::eval_stmts(ifstmt.else_body, state));
  ilist.emplace_back(end_label);
  return ilist;
}

mips::InstructionList gen::eval_if(const AST::IfStatement& ifstmt, State& state) {
  auto end_label = state.get_branch("b_end");
  if (types::is_const(ifstmt.pred)) {
    if (!std::holds_alternative<AST::Value>(ifstmt.pred)) {
      throw std::runtime_error(
        "Somehow expression marked constant without being value"
      );
    }
    return eval_if_const(std::get<AST::Value>(ifstmt.pred), ifstmt, state);
  }
  // evaluate predicate
  auto [reg, ilist] = eval_expression(ifstmt.pred, state);

  // branch if predicate false
  auto else_label = state.get_branch("b_else");
  ilist.push_back(mips::bfalse(reg, else_label));
  state.return_reg(reg);

  // evaluate body
  util::insert_flat(ilist, eval_stmts(ifstmt.body, state));
  // unconditional jump to skip else only taken if body evaluated
  ilist.emplace_back(mips::jump(end_label));
  // insert else label
  ilist.emplace_back(else_label);

  // evaluate else ifs
  for (auto stmt : ifstmt.elseifs) {
    util::insert_flat(ilist, eval_elseif(stmt, end_label, state));
  }

  // evaluate else
  util::insert_flat(ilist, eval_stmts(ifstmt.else_body, state));
  // end with end label
  ilist.emplace_back(end_label);
  return ilist;
}

mips::InstructionList eval_while_const(
  const AST::Value& pred
, const AST::StatementList& stmts
, State& state
) {
  auto boolval = std::get<type_safe::bool_t>(pred);
  if (!boolval) {
    return {};
  }

  auto start_label = state.get_branch();
  mips::InstructionList ilist = {start_label};
  util::insert_flat(ilist, eval_stmts(stmts, state));
  ilist.emplace_back(mips::jump(start_label));

  return ilist;
}

mips::InstructionList gen::eval_while(const AST::While& stmt, State& state) {
  if (types::is_const(stmt.pred)) {
    if (!std::holds_alternative<AST::Value>(stmt.pred)) {
      throw std::runtime_error(
        "Somehow expression marked constant without being value"
      );
    }
    return eval_while_const(std::get<AST::Value>(stmt.pred), stmt.body, state);
  }

  auto start_label = state.get_branch("loop_start");
  auto end_label = state.get_branch("loop_end");
  mips::InstructionList ilist = { start_label };
  // evaluate predicate
  auto [reg, pred_ilist] = eval_expression(stmt.pred, state);
  util::insert_flat(ilist, pred_ilist);
  ilist.push_back(mips::bfalse(reg, end_label));
  state.return_reg(reg);
  util::insert_flat(ilist, eval_stmts(stmt.body, state));
  ilist.emplace_back(mips::jump(start_label));
  ilist.emplace_back(end_label);

  return ilist;
}

mips::InstructionList eval_repeat_const(
  const AST::Value& pred
, const AST::StatementList& stmts
, State& state
) {
  auto start_label = state.get_branch();
  mips::InstructionList ilist = {start_label};
  util::insert_flat(ilist, eval_stmts(stmts, state));

  auto boolval = std::get<type_safe::bool_t>(pred);
  if (!boolval) {
    ilist.emplace_back(mips::jump(start_label));
  }

  return ilist;
}

mips::InstructionList gen::eval_repeat(const AST::Repeat& stmt, State& state) {
  if (types::is_const(stmt.pred)) {
    if (!std::holds_alternative<AST::Value>(stmt.pred)) {
      throw std::runtime_error(
        "Somehow expression marked constant without being value"
      );
    }
    return eval_repeat_const(std::get<AST::Value>(stmt.pred), stmt.body, state);
  }

  auto start_label = state.get_branch();
  mips::InstructionList ilist = { start_label };
  util::insert_flat(ilist, eval_stmts(stmt.body, state));

  // evaluate predicate
  auto [reg, pred_ilist] = eval_expression(stmt.pred, state);
  util::insert_flat(ilist, pred_ilist);
  ilist.push_back(mips::bfalse(reg, start_label));
  state.return_reg(reg);

  return ilist;
}

mips::InstructionList gen::eval_for(const AST::For& stmt, State& state) {
  // translate into the equivalent while loop
  /*
    for INIT to|downto TERM do
      BLOCK;
    end;

    INIT;
    while ${to ? INIT.var <= TERM : INIT.var >= TERM} do
      BLOCK;
      ${to ? INIT.var := succ(INIT.var) : INIT.var := pred(INIT.var);}
    end;
  */

  // the incrementer statement: reassigns the init lval to its predecessor or successor
  auto incrementer = (
    AST::Assignment(
      stmt.init.lval
    , AST::Expression(
        AST::Operation(
          stmt.dir == AST::ForDir::SUCC
            ? AST::Operation::Kind::SUCC
            : AST::Operation::Kind::PRED
        , AST::Expression({ stmt.init.lval })
        )
      )
    )
  );

  auto condition = (
    AST::Expression(
      AST::Operation(
        stmt.dir == AST::ForDir::SUCC
        ? AST::Operation::Kind::LTE
        : AST::Operation::Kind::GTE
      , AST::Expression({ stmt.init.lval })
      , stmt.end
      )
    )
  );

  auto whilebody = stmt.body;
  whilebody.emplace_back(incrementer);

  auto whilestmt = AST::While(condition, whilebody);

  // first evaluate the initial loop variable
  auto ilist = eval_assn(stmt.init, state);
  // asm for the while loop
  util::insert_flat(ilist, eval_while(whilestmt, state));

  return ilist;
}
