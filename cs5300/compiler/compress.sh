#!/usr/bin/bash

make clean
cd ..
rm -f compiler.zip
zip -r compiler.zip compiler -x@compiler/.exclude.lst
cd -
