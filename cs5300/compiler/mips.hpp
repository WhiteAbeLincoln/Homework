#ifndef CPSL_MIPS_H
#define CPSL_MIPS_H
#include "value.hpp"
#include <string>
#include <type_safe/strong_typedef.hpp>
#include <variant>
#include <vector>
#include <tuple>
#include <unordered_set>

namespace mips {
  namespace ts = type_safe;

  struct Register : ts::strong_typedef<Register, std::string>
                  , ts::strong_typedef_op::equality_comparison<Register>
                  , ts::strong_typedef_op::relational_comparison<Register>
                  { using strong_typedef::strong_typedef; };

  extern Register global_ptr;
  extern Register stack_ptr;
  extern Register frame_ptr;
  extern Register return_addr;
  extern Register return_v0;
  extern Register return_v1;
  extern Register reg_a0;
  extern Register reg_a1;
  extern Register reg_a2;
  extern Register reg_a3;
  extern Register reg_s0;
  extern Register reg_s1;
  extern Register reg_s2;
  extern Register reg_s3;
  extern Register reg_s4;
  extern Register reg_s5;
  extern Register reg_s6;
  extern Register reg_s7;
  extern std::unordered_set<Register> saved_registers;
  extern Register zero;

  struct Label : ts::strong_typedef<Label, std::string>
               , ts::strong_typedef_op::equality_comparison<Label>
               , ts::strong_typedef_op::relational_comparison<Label>
               { using strong_typedef::strong_typedef; };

  struct StorageType : ts::strong_typedef<StorageType, std::string>
                     , ts::strong_typedef_op::equality_comparison<StorageType>
                     , ts::strong_typedef_op::relational_comparison<StorageType>
                     { using strong_typedef::strong_typedef; };

  struct Declaration {
    Label name;
    StorageType type;
    std::vector<AST::Value> values;
  };

  struct Data {
    std::vector<Declaration> declarations;
  };

  struct OffsetAddress {
    long offset;
    Register loc;
  };

  using Address = std::variant<Label, OffsetAddress>;

  struct Instruction : ts::strong_typedef<Instruction, std::string>
                     , ts::strong_typedef_op::equality_comparison<Instruction>
                     , ts::strong_typedef_op::relational_comparison<Instruction>
                     { using strong_typedef::strong_typedef; };

  using Line = std::variant<Instruction, Label>;
  using InstructionList = std::vector<Line>;

  Instruction addi(Register, Register, long int);
  Instruction add(Register, Register, Register);
  Instruction sub(Register, Register, Register);
  Instruction mult(Register, Register);
  Instruction div(Register, Register);
  Instruction move(Register, Register);
  Instruction mfhi(Register);
  Instruction mflo(Register);
  Instruction storeword(Register, Address);
  Instruction loadaddr(Register, Address);
  Instruction loadword(Register, Address);
  Instruction loadi(Register, AST::Value);
  extern Instruction syscall;
  Instruction seteq(Register, Register, Register);
  Instruction setgte(Register, Register, Register);
  Instruction setgt(Register, Register, Register);
  Instruction setlte(Register, Register, Register);
  Instruction setlt(Register, Register, Register);
  Instruction setlti(Register, Register, AST::Value);
  Instruction setneq(Register, Register, Register);
  // not, and, or are reserved identifiers, can be used in place of !,&&,||
  Instruction nott(Register, Register);
  Instruction andd(Register, Register, Register);
  Instruction orr(Register, Register, Register);
  Instruction andi(Register, Register, AST::Value);
  Instruction ori(Register, Register, AST::Value);
  Instruction from_label(Label);
  Instruction comment(std::string);
  Instruction beq(Register, Register, Label);
  Instruction bne(Register, Register, Label);
  Instruction jump(Label);
  Instruction jump_link(Label);
  Instruction jump_reg(Register);

  // Virtual Instructions

  Instruction push_stack(long int);
  Instruction pop_stack(long int);
  InstructionList pushl(Register&);
  InstructionList popl(Register&);
  InstructionList call(mips::Label);

  /**
   * Virtual Instruction
   *
   * not a real instruction, but we can fake it using addi and a negative
   *
   * Can only be used in cases of X - constant
   */
  Instruction subi(Register, Register, long int);

  /**
   * Virtual Instruction
   *
   * We can negate a number stored in
   * a register by subtracting from $zero
   */
  Instruction negate(Register, Register);

  // branch when register is 0
  Instruction bfalse(Register, Label);
  // branch when register is not 0
  Instruction btrue(Register, Label);

  extern Instruction stop;
  extern Instruction writeint;
  extern Instruction writechar;
  extern Instruction writestr;
  extern Instruction readint;
  extern Instruction readchar;

  // Combination of mflo and mult
  InstructionList multload(Register, Register, Register);
  InstructionList divload(Register, Register, Register);
  InstructionList modulus(Register, Register, Register);

  InstructionList bool_not(Register, Register);
  InstructionList bool_and(Register, Register, Register);
  InstructionList bool_or(Register, Register, Register);

  struct Text {
    InstructionList instructions;
  };

  struct Program {
    Data data;
    Text text;
  };

  std::string print(const Address&);
  std::string print(const Instruction&);
  std::string print(const AST::Value&);
  std::string print(const InstructionList&);
  std::string print(const Declaration&);
  std::string print(const Data&);
  std::string print(const Text&);
  std::string print(const mips::Program&);
  std::string print(const Line&);
  std::string print(const Label&);
} // namespace mips

namespace std {
  template <> struct hash<mips::Register> : type_safe::hashable<mips::Register> {};
} // namespace std

#endif // CPSL_MIPS_H
