#ifndef CPSL_TYPES_H
#define CPSL_TYPES_H
#include "ast.hpp"
#include <vector>
#include <optional>

// Functions relating to type-checking
namespace types {
  const AST::Type* get_type(const AST::Expression&);
  const AST::Type* get_type(const AST::Value&);
  const AST::Type* get_type(const AST::LValue&);
  const AST::Type* get_type(const AST::Symbol&);
  const AST::Type* get_type(const AST::Call&);
  bool has_type(const AST::Type*, AST::Type::Kind);
  bool has_type(const AST::Value&, AST::Type::Kind);
  bool has_type(const AST::Expression&, AST::Type::Kind);
  bool has_type(const AST::LValue&, AST::Type::Kind);
  bool type_any_of(const AST::Type*, std::vector<AST::Type::Kind>);
  bool type_any_of(const AST::Expression&, std::vector<AST::Type::Kind>);

  bool is_const(const AST::Expression&);
  bool is_const(const AST::Operation&);
  bool is_const(const AST::LValue&);

  bool is_intrinsic_type(const AST::Type*);
  // types that can be used with succ,pred,chr,ord, convertable to integers
  bool is_ordered_type(const AST::Type*);

  const AST::Type* check_arith_op(
    AST::Operation::Kind,
    const AST::Expression&,
    const AST::Expression&
  );
  const AST::Type* check_logical_op(
    AST::Operation::Kind,
    const AST::Expression&,
    const AST::Expression*
  );
  const AST::Type* check_relational_op(
    AST::Operation::Kind,
    const AST::Expression&,
    const AST::Expression&
  );
  const AST::Type* check_intrinsic_func(
    AST::Operation::Kind,
    const AST::Expression&
  );

  void verify_nonUser(AST::Expression*);
  void check_return(std::optional<AST::Signature>, std::optional<AST::Expression>);
  void check_call(AST::Call&);
}

#endif // CPSL_TYPES_H
