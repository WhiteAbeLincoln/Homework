%{
#include <cstring>
#include <cstdlib>

#include "ast.hpp"
#include "parser.tab.hpp"
%}

%option nounput
%option noyywrap
%option yylineno

%%

array|ARRAY         {return ARRAYSY;}
begin|BEGIN         {return BEGINSY;}
chr|CHR             {return CHRSY;}
const|CONST         {return CONSTSY;}
downto|DOWNTO       {return DOWNTOSY;}
do|DO               {return DOSY;}
else|ELSE           {return ELSESY;}
elseif|ELSEIF       {return ELSEIFSY;}
end|END             {return ENDSY;}
forward|FORWARD     {return FORWARDSY;}
for|FOR             {return FORSY;}
function|FUNCTION   {return FUNCTIONSY;}
if|IF               {return IFSY;}
of|OF               {return OFSY;}
ord|ORD             {return ORDSY;}
pred|PRED           {return PREDSY;}
procedure|PROCEDURE {return PROCEDURESY;}
read|READ           {return READSY;}
record|RECORD       {return RECORDSY;}
repeat|REPEAT       {return REPEATSY;}
return|RETURN       {return RETURNSY;}
stop|STOP           {return STOPSY;}
succ|SUCC           {return SUCCSY;}
then|THEN           {return THENSY;}
to|TO               {return TOSY;}
type|TYPE           {return TYPESY;}
until|UNTIL         {return UNTILSY;}
var|VAR             {return VARSY;}
ref|REF             {return REFSY;}
while|WHILE         {return WHILESY;}
write|WRITE         {return WRITESY;}

"%"  {return MODSY;}
"&"  {return ANDSY;}
"("  {return LPARENSY;}
")"  {return RPARENSY;}
"*"  {return MULTSY;}
"+"  {return PLUSSY;}
","  {return COMMASY;}
"-"  {return MINUSSY;}
"."  {return DOTSY;}
"/"  {return DIVSY;}
":"  {return COLONSY;}
":=" {return ASSIGNSY;}
";"  {return SCOLONSY;}
"<"  {return LTSY;}
"<=" {return LTESY;}
"<>" {return NEQSY;}
"="  {return EQSY;}
">"  {return GTSY;}
">=" {return GTESY;}
"["  {return LBRACKETSY;}
"]"  {return RBRACKETSY;}
"|"  {return ORSY;}
"~"  {return NOTSY;}

[a-zA-Z][a-zA-Z0-9_]* {yylval.ident = strdup(yytext); return IDENTSY;}

[0][0-7]+          {yylval.val = new AST::Value(strtol(yytext,nullptr,0)); return INTSY;}
[0][x][0-9a-fA-F]+ {yylval.val = new AST::Value(strtol(yytext,nullptr,0)); return INTSY;}
[0-9]+[0-9]*       {yylval.val = new AST::Value(strtol(yytext,nullptr,0)); return INTSY;}

'\\n'               {yylval.val = new AST::Value('\n'); return CHARCONSTSY;}
'\\t'               {yylval.val = new AST::Value('\t'); return CHARCONSTSY;}
'\\r'               {yylval.val = new AST::Value('\r'); return CHARCONSTSY;}
'\\?.'             {
  int size = strlen(yytext);
  const char v = size == 3 ? yytext[1] : yytext[2];
  yylval.val = new AST::Value(v);
  return CHARCONSTSY;
}

\"[a-zA-Z0-9~`!@'#$%^&*()_+=\-\[\]{}\\\/><,.:;| ]*\" {
  char* str = strdup(yytext);
  str++; // strip the opening quote
  str[strlen(str) - 1] = '\0'; // strip the closing quote
  // TODO: we should probably unescape
  // actually I'm not even sure if this string supports escaped characters
  yylval.val = new AST::Value(str); return STRINGSY;
}

\$.*$ {}
\n {}
[ \t]+ {}
. {}

%%
