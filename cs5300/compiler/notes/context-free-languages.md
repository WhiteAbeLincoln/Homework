Context-Free Languages
===========================

How do we write a Finite Automaton that accepts the language
`a`<sup>`n`</sup>`b`<sup>`n`</sup>?

We can't.

## Proof: pumping lemma

+ Given an infinte language, a word in that language cannot have a fixed length.
+ A finite state machine that accepts a word with length `n` must have `n` states.
+ Because there exists a word `w` in an infinite language with length > `n`, the automaton must have a loop.
+ Next we show that `a`<sup>`n`</sup>`b`<sup>`n`</sup> cannot have a loop

    Possible locations for  a loop given input `---aaabbb---`
    1. at the beginning among the `a`. Cannot be valid because we could have traversed the loop multiple times, giving more `a` than `b`
    2. at the end among the `b`. Cannot be valid because we could have traversed the loop multiple times, giving more `b` than `a`
    3. in the middle among the `ab`. Cannot be valid because we could have traversed the loop multiple times, giving the invalid text `abab`

    Therefore `a`<sup>`n`</sup>`b`<sup>`n`</sup> cannot be represented by a Regular language

## Context-Free languages
+ alphabet: a set of terminal symbols
+ set of non-terminal symbols
+ set of replacement rules

`S -> aSb`
`S -> ε`

Useful Site: [Grammophone](http://mdaines.github.io/grammophone/#)

Languages that permit multiple parse trees are called ambigous.

## Parsing
### LL Parser (Recursive Descent)
Scans from left to right, doing a left-most derivation.

Write a function for every non-terminal:
```
  bool S(in) {
    if (in == a) {
      consume(A)
      match(S)
      consume(B)
    }
  }
```

This fails for left recursion: `S -> S a`

There are methods for transforming left recursive grammars to languages that can be parsed by LL or you can use a different kind of parser.
