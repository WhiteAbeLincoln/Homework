Regular Expressions to State Machines
=====================================

RE -> NDFA -> DFA -> Minimal DFA

## Nondeterministic Finite Automata
+ Finite :: Finite number of states
+ Nondeterministic :: Multiple branches with the same accepting input, or empty string ε branch

We can convert a NDFA to a DFA

## Thompson's Construction: RE -> NDFA
1. Exactly 1 start and 1 accept state
    `[ start ] -> { } -> [ accept ]`
### Patterns
1. Literals: `[ start ] -a-> [ accept ]`
2. Concatenation (A + B):
```
  [ start ] -> { A } -> [ accept ]
  [ start ] -> { B } -> [ accept ]
  TO: [ start ] -> { A } -> [ ] -ε-> [ ] -> { B } -> [ accept ]
```
3. Alternation (A | B):
```
  [ start ] -> { A } -> [ accept ]
  [ start ] -> { B } -> [ accept ]
  TO: [ start ] -ε-> [ ] -> { A } -> [ ] -ε┐
        └ε-> [ ] -> { B } -> [ ] -ε-> [ accept ]
```
4. Kleine Closure (A*):
```
 [ start | -> { A } -> [ accept |
 TO: [ start ] -ε-> [ accept ]
        |ε             |ε
       [  ] - { A } - [  ]
        └-------ε------┘
```

## NDFA to DFA
Defer choice. Create a table of all possible resulting states.
We create new states for every indeterminate state
Example: Ambiguous transition on character 'a' which could lead to either state B or state D. We create a new state BD

Every new combination state containing an accept state is also accepting

Number of possible resulting states: Power Set - 2^n, given n NDFA states

# DFA Minimization

Minimum number of steps to determine if a word is part of the language is the number of characters in the word. Only benefit to minimization is optimizing cpu cache behavior.
1. Are any columns in the table identitcal? If true they can be merged
2. If any rows are identitcal, they can be merged
