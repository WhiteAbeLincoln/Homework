# Optimization
## Basic Blocks
Format: Label, statements without label or jmp, branch or jump
OR: sequence of statements between a label and a jump or branch
## Common Subexpression-elimination
### Example
  `dist12 := (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2)`
  where dist12 offset 16, x1 offset 0, x2 offset 4, y1 offset 8, y2 offset 12
#### Non-optimizing Compiler
  ```
  lw $t0, 0($gp)
  lw $t1, 4($gp)
  sub $t3, $t0, $t1
  lw $t0, 0($gp)
  lw $t1, 4($gp)
  sub $t4, $t0, $t1
  mult $t3, $t4
  mflo $t0
  lw $t1, 8($gp)
  lw $t2, 12($gp)
  sub $t3, $t1, #t2
  lw $t1, 8($gp)
  lw $t2, 12($gp)
  sub $t4, $t1, $t2
  mult $t3, $t4
  mflo $t1
  add $t2, $t0, $t1
  sw $t, 16($gp)
  ```
#### Optimizing Compiler
  uses a lot of registers

  keeps track of operations in table and recognizes
  when a subexpression occurs again, and re-uses those
  stored registers
  ```
  lw $t0, 0($gp)
  lw $t1, 4($gp)
  sub $t3, $t0, $t1
  mult $t3, $t3
  mflo $t4
  lw $t5, 8($gp)
  lw $t6, 12($gp)
  sub $t7, $t5, $t6
  mult $t7, $t7
  mflo $t8
  add ...
  sw ...
  ```
