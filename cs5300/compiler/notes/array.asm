# var arr: array[4:9] of integer;
# begin
#   arr[5] = 9;
#   write(arr[5]);
# end.

# evaluate the expression 9
li $t0, 9
# evaluate the index 5
li $t1, 5
# offset the index by the lower bound to get a zero-based index
addi $t1, $t1, -4
# load the size of the internal type (integer)
li $t2, 4
# multiply the offset index by the size
mult $t1, $t2
mflo $t1
# load the base address of the parent symbol (arr)
la $t2, 0($gp)
# add the address and the index to get an offset address
add $t1, $t1, $t2
# store our expression at the address
sw $t0, 0($t1)
# load the address
li $t1, 5
addi $t1, $t1, -4
li $t2, 4
mult $t1, $t2
mflo $t1
la $t2, 0($gp)
add $t1, $t1, $t2
# load the word at address
lw $t2, 0($t1)
# write the expression
move $a0, $t2
li $v0, 1
syscall
