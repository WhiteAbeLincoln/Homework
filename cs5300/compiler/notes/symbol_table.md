# Symbol Tables / Codegen

1. We need a register pool
2. An expression can be an identifier: E -> TOK_ID
    - Therefore we need to track this variable by its identifier in a table
      to learn its type, value, stored register
3. Types need to know their size so that we know where/how to store them - primitives should all have size 4
4. Global variables are stored at some offset from the global pointer
    ```
    i: int -> $gp @ 0
    j: int -> $gp @ 4
    ```

Sample Program
  ```
    var
      i: integer
      j: integer
    begin
      read(i, j);
      i := i + 2;
      write(i);
    end.
  ```
  ```asm
  # read int
  li $v0 5
  syscall
  # save read int in variable i
  sw $a0, 0($gp)
  # read int
  li $v0, 5
  syscall
  # save to variable j
  sw $a0, 4($gp)
  # load for addition
  lw $t0, 0($gp)
  li $t1, 2
  # add i + 2
  add $t2, t0, t1
  # assignment
  sw $t2, 0($gp)
  # load i for writing
  lw $t0, 0($gp)
  # move t0 to a0 - why are we using ori?
  ori $a0, $t0, 0
  # write integer i
  li $v0, 1
  syscall
  ```
