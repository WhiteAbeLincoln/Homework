#include "mips.hpp"
#include "types.hpp"
#include "util.hpp"
#include <exception>
#include <algorithm>

using namespace mips;

Register mips::global_ptr("$gp");
Register mips::stack_ptr("$sp");
Register mips::frame_ptr("$fp");
Register mips::return_addr("$ra");
Register mips::return_v0("$v0");
Register mips::return_v1("$v1");
Register mips::reg_a0("$a0");
Register mips::reg_a1("$a1");
Register mips::reg_a2("$a2");
Register mips::reg_a3("$a3");
Register mips::zero("$zero");
mips::Register mips::reg_s0("$s0");
mips::Register mips::reg_s1("$s1");
mips::Register mips::reg_s2("$s2");
mips::Register mips::reg_s3("$s3");
mips::Register mips::reg_s4("$s4");
mips::Register mips::reg_s5("$s5");
mips::Register mips::reg_s6("$s6");
mips::Register mips::reg_s7("$s7");
std::unordered_set<Register> mips::saved_registers{
  reg_s0, reg_s1, reg_s2, reg_s3, reg_s4, reg_s5, reg_s6, reg_s7
};

Instruction three_reg(std::string op, mips::Register r1, mips::Register r2, mips::Register r3) {
  return Instruction(
    op + " "
    + static_cast<std::string>(r1)
    + ", "
    + static_cast<std::string>(r2)
    + ", "
    + static_cast<std::string>(r3)
  );
}

Instruction two_reg(std::string op, mips::Register r1, mips::Register r2) {
  return Instruction(
    op + " "
    + static_cast<std::string>(r1)
    + ", "
    + static_cast<std::string>(r2)
  );
}

InstructionList mips::multload(Register to, Register left, Register right) {
  return {mult(left, right), mflo(to)};
}

InstructionList mips::divload(Register to, Register left, Register right) {
  return {div(left, right), mflo(to)};
}

InstructionList mips::modulus(Register to, Register left, Register right) {
  return {div(left, right), mfhi(to)};
}

InstructionList mips::bool_not(Register to, Register r) {
  return {
    nott(to, r)
    // select the first bit
  , andi(to, to, AST::Value((long int)1))
  };
}
InstructionList mips::bool_and(Register to, Register left, Register right) {
  return { andd(to, left, right) };
};
InstructionList mips::bool_or(Register to, Register left, Register right) {
  return {
    orr(to, left, right)
  , andi(to, to, AST::Value((long int)1))
  };
};

Instruction mips::addi(Register to, Register left, long int val) {
  return Instruction(
    "addi "
    + static_cast<std::string>(to)
    + ", "
    + static_cast<std::string>(left)
    + ", "
    + std::to_string(val)
  );
}

Instruction mips::add(Register to, Register left, Register right) {
  return three_reg("add", to, left, right);
}

Instruction mips::sub(Register to, Register left, Register right) {
  return three_reg("sub", to, left, right);
}

Instruction mips::subi(Register to, Register left, long int val) {
  return addi(to, left, -1 * val);
}

Instruction mips::negate(Register to, Register val) {
  return sub(to, mips::Register("$zero"), val);
}

Instruction mips::mult(Register left, Register right) {
  return two_reg("mult", left, right);
}

Instruction mips::div(Register left, Register right) {
  return two_reg("div", left, right);
}

Instruction mips::move(Register left, Register right) {
  return two_reg("move", left, right);
}

Instruction mips::mfhi(Register to) {
  return Instruction("mfhi " + static_cast<std::string>(to));
}

Instruction mips::mflo(Register to) {
  return Instruction("mflo " + static_cast<std::string>(to));
}

Instruction mips::storeword(Register from, Address to) {
  return Instruction(
    "sw "
    + static_cast<std::string>(from)
    + ", "
    + print(to)
  );
}

Instruction mips::loadaddr(Register to, Address from) {
  return Instruction(
    "la "
    + static_cast<std::string>(to)
    + ", "
    + print(from)
  );
}

Instruction mips::loadword(Register to, Address from) {
  return Instruction(
    "lw "
    + static_cast<std::string>(to)
    + ", "
    + print(from)
  );
}

Instruction mips::loadi(Register to, AST::Value val) {
  return Instruction(
    "li "
    + static_cast<std::string>(to)
    + ", "
    + mips::print(val)
  );
}

Instruction mips::comment(std::string cmt) {
  return Instruction("# " + cmt);
}

Instruction mips::syscall("syscall");
Instruction mips::writechar("li $v0, 11\nsyscall");
Instruction mips::writeint("li $v0, 1\nsyscall");
Instruction mips::stop("li $v0, 10\nsyscall");
Instruction mips::writestr("li $v0, 4\nsyscall");
Instruction mips::readint("li $v0, 5\nsyscall");
Instruction mips::readchar("li $v0, 12\nsyscall");

// labels used as an instruction should always be followed by a colon
// labels used in an instruction (such as load address, jump, etc) will not be followed by a colon
Instruction mips::from_label(Label l) {
  return Instruction(static_cast<std::string>(l) + ":");
}

Instruction mips::seteq(Register to, Register left, Register right) {
  return three_reg("seq", to, left, right);
}

Instruction mips::setgte(Register to, Register left, Register right) {
  return three_reg("sge", to, left, right);
}

Instruction mips::setgt(Register to, Register left, Register right) {
  return three_reg("sgt", to, left, right);
}

Instruction mips::setlte(Register to, Register left, Register right) {
  return three_reg("sle", to, left, right);
}

Instruction mips::setlt(Register to, Register left, Register right) {
  return three_reg("slt", to, left, right);
}

Instruction mips::setneq(Register to, Register left, Register right) {
  return three_reg("sne", to, left, right);
}

Instruction mips::setlti(Register to, Register left, AST::Value right) {
  return Instruction(
    "slti "
    + static_cast<std::string>(to)
    + ", "
    + static_cast<std::string>(left)
    + ", "
    + mips::print(right)
  );
}

Instruction mips::nott(Register to, Register val) {
  return two_reg("not", to, val);
}

Instruction mips::andd(Register to, Register left, Register right) {
  return three_reg("and", to, left, right);
}

Instruction mips::orr(Register to, Register left, Register right) {
  return three_reg("or", to, left, right);
}

Instruction mips::andi(Register to, Register left, AST::Value val) {
  return Instruction(
    "andi "
    + static_cast<std::string>(to)
    + ", "
    + static_cast<std::string>(left)
    + ", "
    + mips::print(val)
  );
}

Instruction mips::ori(Register to, Register left, AST::Value val) {
  return Instruction(
    "ori "
    + static_cast<std::string>(to)
    + ", "
    + static_cast<std::string>(left)
    + ", "
    + mips::print(val)
  );
}

Instruction mips::beq(Register r1, Register r2, Label lbl) {
  return Instruction(
    "beq "
    + static_cast<std::string>(r1)
    + ", "
    + static_cast<std::string>(r2)
    + ", "
    + static_cast<std::string>(lbl)
  );
}

Instruction mips::bne(Register r1, Register r2, Label lbl) {
  return Instruction(
    "bne "
    + static_cast<std::string>(r1)
    + ", "
    + static_cast<std::string>(r2)
    + ", "
    + static_cast<std::string>(lbl)
  );
}

Instruction mips::bfalse(Register test, Label lbl) {
  return beq(test, mips::Register("$zero"), lbl);
}

Instruction mips::btrue(Register test, Label lbl) {
  return bne(test, mips::Register("$zero"), lbl);
}

Instruction mips::jump(Label lbl) {
  return Instruction("j " + static_cast<std::string>(lbl));
}

Instruction mips::jump_link(Label lbl) {
  return Instruction("jal " + static_cast<std::string>(lbl));
}

Instruction mips::jump_reg(Register reg) {
  return Instruction("jr " + static_cast<std::string>(reg));
}

Instruction mips::push_stack(long int size) {
  return subi(mips::stack_ptr, mips::stack_ptr, size);
}

Instruction mips::pop_stack(long int size) {
  return addi(mips::stack_ptr, mips::stack_ptr, size);
}

InstructionList mips::call(mips::Label lbl) {
  return util::concat(
    mips::pushl(mips::return_addr)
  , { mips::jump_link(lbl) }
  );
}

InstructionList mips::pushl(Register& reg) {
  // we push a single word - 4 bytes
  return {
    mips::push_stack(4),
    mips::storeword(reg, mips::OffsetAddress{0, mips::stack_ptr})
  };
}

InstructionList mips::popl(Register& reg) {
  return {
    mips::loadword(reg, mips::OffsetAddress{0, mips::stack_ptr}),
    mips::pop_stack(4)
  };
}

std::string mips::print(const Instruction& instruction) {
  return static_cast<std::string>(instruction);
}

std::string mips::print(const Label& label) {
  return static_cast<std::string>(label) + ":";
}

std::string mips::print(const Line& line) {
  return std::visit(util::match{
    [](const Instruction& i) { return mips::print(i); }
  , [](const Label& i) { return mips::print(i); }
  }, line);
}

std::string mips::print(const InstructionList& vec) {
  return util::join(util::map_vec<Line, std::string>(vec, [](auto i) {
    return mips::print(i);
  }), "\n");
}

std::string mips::print(const Address& addr) {
  return std::visit(util::match{
    [](const Label& label) {
      return static_cast<std::string>(label);
    }
  , [](const OffsetAddress& oa) {
      return
        std::to_string(oa.offset)
        + "("
        + static_cast<std::string>(oa.loc)
        + ")";
    }
  }, addr);
}

std::string mips::print(const AST::Value& val) {
  PRINT_VERBOSE("Printing value %s\n", AST::print(val).c_str());
  return std::visit(util::match{
    [](std::string str_val) {
      return "\"" + util::escape_string(str_val) + "\"";
    },
    [](long int int_val) {
      return std::to_string(int_val);
    },
    [](const char char_val) {
      return "'" + util::print_char(char_val) + "'";
    },
    [](type_safe::bool_t bool_val) {
      return std::to_string(bool_val ? 1 : 0);
    }
  }, val);
}

std::string mips::print(const Declaration& decl) {
  auto accum =
      static_cast<std::string>(decl.name)
    + ": "
    + static_cast<std::string>(decl.type)
    + " "
    // we must have at least one value
    + mips::print(decl.values[0]);


  for (auto i = decl.values.begin() + 1; i != decl.values.end(); ++i) {
    accum += "," + mips::print(*i);
  }

  return accum;
}

std::string mips::print(const Data& data) {
  std::string accum = ".data";
  for (auto d : data.declarations) {
    accum += "\n" + mips::print(d);
  }

  return accum;
}

std::string mips::print(const Text& text) {
  std::string accum = ".text\n.globl main";
  for (auto i : text.instructions) {
    accum += "\n" + mips::print(i);
  }

  return accum;
}

std::string mips::print(const mips::Program& p) {
  return mips::print(p.data) + "\n" + mips::print(p.text);
}
