#ifndef CPSL_CODEGEN_H
#define CPSL_CODEGEN_H
#include <vector>
#include <string>
#include "ast.hpp"
#include "table.tpp"
#include "mips.hpp"
#include <vector>
#include <tuple>
#include <functional>
#include <optional>
#include <unordered_set>

namespace gen {
  using RegPool = std::unordered_map<mips::Register, bool>;
  using StringMap = std::unordered_map<std::string, std::string>;
  struct Fn {
    AST::Signature sig;
    AST::Body body;
    // positive offset from the frame pointer (towards bottom of stack)
    long int argumentOffset;
    std::vector<AST::Call> calls;
  };
  using FnTable = std::unordered_map<std::string, Fn>;
  class State {
  private:
    bool listening = false;
    std::unordered_set<mips::Register> reg_listen;
    void init_regpool();
    long branchNum = 0;
  public:
    RegPool regpool;
    long gpOffset;
    std::optional<Fn> currFn = std::nullopt;
    StringMap stringmap;
    FnTable fntable;
    std::vector<mips::Declaration> string_declarations();
    void start_listen();
    std::unordered_set<mips::Register> stop_listen();
    // requests a register from the pool
    mips::Register request_reg(mips::Register);
    mips::Register request_reg();
    // returns a register to the pool
    void return_reg(mips::Register);
    // adds a string to the state's string list
    mips::Label add_string(std::string);
    mips::Label get_branch(std::string = "branch");
    State();
  };
  long init_sym_mem(long, AST::Symbol*, mips::Register = mips::global_ptr);
  long init_memory(const AST::SymbolTable&, long, mips::Register = mips::global_ptr);

  std::tuple<mips::Register, mips::InstructionList> eval_expression(const AST::Expression&, State&);

  std::string print(const AST::Program&);
  // prints a statement list - ends with newline
  mips::InstructionList eval_stmts(const AST::StatementList&, State&);
  mips::InstructionList eval_stmt(const AST::Statement&, State&);
  mips::InstructionList eval_read(const AST::ReadStmt&, State&);
  // prints a write statement - ends with newline
  mips::InstructionList eval_write(const AST::WriteStmt&, State&);
  mips::InstructionList eval_assn(const AST::Assignment&, State&);
  mips::InstructionList eval_if(const AST::IfStatement&, State&);
  mips::InstructionList eval_while(const AST::While&, State&);
  mips::InstructionList eval_repeat(const AST::Repeat&, State&);
  mips::InstructionList eval_for(const AST::For&, State&);
  std::tuple<mips::Register, mips::InstructionList> eval_op(const AST::Operation&, State&);
  mips::InstructionList eval_functions(State&);
  mips::InstructionList eval_fn(const Fn&, State&);
  mips::InstructionList eval_call(const AST::Call&, State&);
  mips::InstructionList eval_return(const AST::Return&, State&);
  mips::InstructionList copy_store(const mips::Address&, const mips::Address&, const AST::Type*, State&);
  mips::InstructionList save_location(const mips::Address&, const AST::Expression&, bool, bool, State&);
  mips::InstructionList save_location(const AST::LValue&, const AST::Expression&, State&);

  std::vector<AST::Call> get_expr_call(const AST::Expression&);
  std::vector<AST::Call> get_stmts_call(const AST::StatementList&);
  std::vector<AST::Call> get_stmt_call(const AST::Statement&);
}
#endif // CPSL_CODEGEN_H
