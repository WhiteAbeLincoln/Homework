#include "util.hpp"
#include <cstdio>
#include <sstream>
#include <iostream>

// initialize our global shared config
util::Config util::CpslConfig = {false, false, false};

std::string util::print_char(const char chr) {
  switch (chr) {
    case '\n': return "\\n";
    case '\t': return "\\t";
    case '\r': return "\\r";
    default:
      return std::string("") + chr;
  }
}

std::string util::escape_string(std::string str) {
  std::string v = "";

  for (auto c : str) {
    // we hit an escape sequence
    switch (c) {
      case '\n':
        v += "\\n";
        break;
      case '\t':
        v += "\\t";
        break;
      case '\r':
        v += "\\r";
        break;
      default:
        v += c;
    }
  }

  return v;
}

// again some code duplication, but it doesn't seem
// worth completely figuring out C-style variadic
// arguments at this point
/**
 * Wrapper around printf that prints only when verbose
 * and does not prefixe the string "VERBOSE: "
 */
int util::print_verbose1(const char* fmt, ...) {
  int result;
  va_list args;
  va_start(args, fmt);
  if (util::CpslConfig.verbose) {
    result = vfprintf(stderr, fmt, args);
  }
  va_end(args);
  return result;
}

/**
 * Wrapper around printf that prints only when verbose
 * and prefixes the string "VERBOSE: "
 */
int util::print_verbose(const char* fmt, ...) {
  int result;
  va_list args;
  va_start(args, fmt);
  if (util::CpslConfig.verbose) {
    fputs("VERBOSE: ", stderr);
    result = vfprintf(stderr, fmt, args);
  }
  va_end(args);
  return result;
}

std::string util::join(const std::vector<std::string> vec, const std::string delim) {
  std::string s;
  for (auto it = vec.begin(); it != vec.end(); ++it) {
    s += *it;
    if (it+1 != vec.end()) s += delim;
  }
  return s;
}
