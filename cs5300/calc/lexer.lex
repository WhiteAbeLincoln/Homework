%{
#include <cstdlib>

#include "parser.tab.hpp"
%}

%option nounput
%option noyywrap

%%

"+" {return ADD;}
"-" {return SUB;}
"*" {return MULT;}
"/" {return DIV;}
"(" {return OPEN;}
")" {return CLOSE;}
"\n" {return DONE;}
";" {return DONE;}
"==" {return EQUAL;}
"=" {return EQUAL;}
":=" {return EQUAL;}
"let" {return LET;}
[a-zA-Z][_0-9a-zA-Z]* {yylval.id = strdup(yytext); return ID;}
[ \t] {}

[0-9]*\.[0-9]* {yylval.val = atof(yytext);return NUMBER;}
[0-9]+ {yylval.val = atof(yytext);return NUMBER;}

%%
