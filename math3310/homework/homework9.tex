% Created 2017-12-04 Mon 15:06
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\textheight=9.25in \textwidth=7in \topmargin=-0.75in \oddsidemargin=-0.25in \evensidemargin=-0.25in \DeclareMathOperator{\guard}{g} \usepackage{siunitx} \usepackage{mathtools} \DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\author{Abraham White}
\date{\today}
\title{}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.3.1 (Org mode 9.1.2)}, 
 pdflang={English}}
\begin{document}

\section*{Homework 9}
\label{sec:org4356776}
\subsection*{9.1}
\label{sec:org94060fd}
\textbf{Watched Eyes}: The function \(gg(w)\) is the maximum number of robot eyes
 (REs) required to protect any \emph{w}-walled station such that each RE can
 be seen by at least one other RE. Please make a conjecture for the value of
 \(gg(w)\).

\textbf{Conjecture}: The number of REs required to protect a
station \(S_w\) where each RE can see at least one other RE, is less than or
equal to \(|V(H)|\), where \(H\) is an irreducible graph homeomorphic to the dual
graph \(D\) of the graph \(C\), and \(C\) is a minimally convex polygonization of \(S_w\)

\textbf{Justification}: The dual graph \(D\) is produced placing a vertex on each
 face of \(C\), where two vertices are connected if the faces share an
 edge. We observe that by placing an RE on each of the vertices of \(D\), the
 station \(S_w\) is protected\footnote{Each vertex is in a convex shape, and so a RE placed in the vertex
can protect that entire shape. We have vertices for each convex shape in the
graph \(S_w\) and so if each shape is protected, \(S_w\) is protected.}. We also observe that if two convex shapes
 share a face, a line can be drawn from some point in the first shape to a
 point in the second shape, and therefore, a RE at some point within the
 first shape can observe an RE at some point in the interior of the second
 shape.

\textbf{Producing the graph \(C\)}: The strategy for producing the minimally convex
 polygonization of \(S_w\) is as follows:
\begin{enumerate}
\item Produce a triangualization \(\nabla S_w\) as explained in class
\item Select an exterior triangle (a triangle that has two exterior edges),
and remove the shared edge if the resulting shape is convex.
\item Continue removing shared edges while possible.
\item Once a convex shape cannot be produced, repeat from step 2
\end{enumerate}
We assume that this algorithm is correct in producing a minimally convex
polygonization. It has done so in repeated tests, but has not formally been
proved to always produce the minimal number of polygons.

\clearpage
\subsection*{9.2}
\label{sec:orga4dbe5d}
\textbf{Rectangular Stations}: Define the function \(\guard_{\perp}(w)\) to be the
 maximum number of REs required to protect a \emph{w}-walled station whose
 interior angles between walls are either \(\ang{90}\) or \(\ang{270}\). Please
 find the value for \(\guard_{\perp}(w)\) and prove the value you find is correct.

\textbf{Claim}: \(\guard_{\perp}(w) = \floor{\frac{w}{4}}\)  

\textbf{Proof}:

Let \(\mathrm{guard}(S_\perp) =\) The number of REs sufficient and necessary to
guard a given orthogonal space station \(S_\perp\)

We define \(\guard_{\perp}(w)\) as follows.
$$\guard_\perp(w) = \max \left\{ \mathrm{guard}(S_\perp): S_\perp \in \{ \mbox{$w$-walled orthogonal stations}\}\right\}$$

We define the algorithm \emph{guard} as follows:

\noindent\rule{\textwidth}{0.5pt}
\textbf{Procedure}: \(\textsc{Guard}\) \\
\emph{Input}: \emph{w}-walled orthogonal station \(S_\perp\) \\
\emph{Output}:  A placement of \(\floor{\frac{w}{4}}\) REs which guard \(S_\perp\)
\begin{enumerate}
\item Create a convex quadrilateralization \(Q\) of \(S_\perp\)
\item Properly 4-color the graph \(Q\)
\item Place a RE at each of the corners where a least-used color appears
\end{enumerate}

\noindent\rule{\textwidth}{0.5pt}

Notice that due to the definition of a station (constructed from Jordan
curves, where no walls cross), \(|V(S_\perp)| = |W(S_\perp)|\). Therefore we
know the algorithm above proves \(\guard_\perp(S) \leq \floor{\frac{w}{4}}\)\footnote{We colored each vertex with one of four colors, and selected the
least-used color to place the REs on. The maximum value for the number of
vertices colored with the least-used color is \(\floor{\frac{w}{4}}\), because if
the value was any greater than an even division of the number of vertices, it
wouldn't be the least used color.}.

Two assumptions were made in our algorithm. First, that a convex
quadrilateralization \(Q\) can be created for any orthogonal station, and
second, that a convex quadrilateralization \(Q\) is 4-colorable. Proofs for
these assumptions will be shown later.

We must now show that there are some orthogonal stations where it is
necessary to have \(\floor{\frac{w}{4}}\) REs. To do this, we consider the
orthogonal analogue to the comb station. It is obvious that the station
requires at least \(\floor{\frac{w}{4}}\) REs. Therefore \(\guard_\perp(s) \geq \floor{\frac{w}{4}}\)

Because we have shown \(\guard_\perp(S) \leq \floor{\frac{w}{4}}\) and
\(\guard_\perp(S) \geq \floor{\frac{w}{4}}\), it follows that
\(\guard_\perp(S) = \floor{\frac{w}{4}}\)

\clearpage
\subsection*{9.3}
\label{sec:org47e265e}
\textbf{Watched Eyes in Rectangular Stations}: Define the function
 \(gg_\perp(w)\) to be the maximum number of REs to protect a
 \emph{w}-walled rectangular station such that each RE can be seen by at least one
 other RE. Please determine the value for \(gg_\perp(w)\) and prove
 the value is correct.

\textbf{Claim}: \(gg_\perp(w) = \floor{\frac{w}{3}}\)
\end{document}