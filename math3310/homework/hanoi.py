#!/usr/bin/env python3
from typing import List
import sys

# possible directions to move. Represents
# directed edges between verticies for a graph
# of the tower
directions = [(0,1), (1,0), (1,2), (2,1)]

def fill(n):
    # we define our three pegs as a tuple containing three lists
    return ([],[x for x in range(1, n+1)][::-1], [])

def movePossible(tower, pfrom, pto):
    return len(tower[pfrom]) != 0 and (
        len(tower[pto]) == 0 or tower[pfrom][-1] < tower[pto][-1])

def isComplete(tower, n):
    return len(tower[1]) == 0 and (len(tower[0]) == n or len(tower[2]) == n)

# helper function to replace tuple element at index
def treplace(tup, ix, val):
    return tup[:ix] + (val,) + tup[ix+1:]

def move(tower, pfrom, pto):
    last = tower[pfrom][-1]
    return treplace(
        treplace(tower, pfrom, tower[pfrom][:-1]),
        pto,
        tower[pto]+[last])

def step(prev, tower, n, m):
    if isComplete(tower, n):
        return (tower, m)
    for d in directions:
        if movePossible(tower, d[0], d[1]) and move(tower, d[0], d[1]) != prev:
            return step(tower, move(tower, d[0], d[1]), n, m+1)

def help():
    return """
    hanoi.py

    Prints the number of moves required to move n disks to a new peg

    Usage: hanoi.py <n>

    OPTIONS

    n      number of disks
    """

def main(argc: int, argv: List[str]):
    n = 0
    if argc > 1:
        n = int(argv[1])
        print(step((), fill(n), n, 0))
    else:
        print(help())
    pass

if __name__ == "__main__":
    main(len(sys.argv), sys.argv)
