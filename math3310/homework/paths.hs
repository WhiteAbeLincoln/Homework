import Data.Tree
type Point = (Int, Int)
-- data Tree a = Branch a [Tree a] deriving (Show, Eq)
data Direction = DLeft | DRight | DUp deriving (Show, Eq)
-- Action can be thought of the Direction taken, and the resulting Point
type Action = (Direction, Point)

right :: Action -> Action
right (_, p) = (DRight, (fst p + 1, snd p))

left :: Action -> Action
left (_, p) = (DLeft, (fst p - 1, snd p))

up :: Action -> Action
up (_, p) = (DUp, (fst p, snd p + 1))

origin :: Tree Action
origin = Node (DUp, (0,0)) []

move :: Tree Action -> Tree Action
--  if we are at a leaf node
move (Node p []) = case p of
                     (DLeft, _) -> Node p [Node (left p) [], Node (up p) []]
                     (DUp, _) -> Node p [Node (left p) [], Node (up p) [], Node (right p) []]
                     (DRight, _) -> Node p [Node (right p) [], Node (up p) []]
-- if we are not at a leaf node, recursively call move over all sub trees
move (Node p t) = Node p $ map move t

moveto :: Tree Action -> Int -> Tree Action
moveto t 0 = t
moveto t n = moveto (move t) $ n-1

paths :: Tree Action -> Int
paths (Node p []) = 1
paths (Node p t) = sum (map paths t)

p_n :: Int -> Int
p_n = paths . moveto origin

printed :: Tree Action -> Tree String
printed = (>>= return . show)

-- printed :: Tree Action -> Tree String
-- printed (Node p []) = Node (show p) []
-- printed (Node p t) = Node (show p) $ map printed t

printTree :: Tree Action -> String
printTree = drawTree . printed
