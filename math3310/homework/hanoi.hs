import Control.Applicative
import Data.Maybe
import System.Environment

data Position = PLeft | PMiddle | PRight deriving (Show, Eq)
data Pole = Pole {position :: Position, disks :: [Int] } deriving (Show, Eq)
data Tower = Tower Pole Pole Pole deriving (Show, Eq)

directions = [(PLeft, PMiddle), (PMiddle, PLeft), (PMiddle, PRight), (PRight, PMiddle)]

fill n = Tower (Pole PLeft [n,n-1..1]) (Pole PMiddle []) (Pole PRight [])

getPole :: Tower -> Position -> Pole
getPole (Tower l m r) p
  | position l == p = l
  | position m == p = m
  | position r == p = r

-- find out how to write this in pointfree style
getDisks t p = disks $ getPole t p

-- a move is possible if there are disks on the from-pole
-- and if the to-pole has no disks or the new disk is smaller in size
movePossible :: Tower -> Position -> Position -> Bool
movePossible t from to =
    length dfrom /= 0 && (length dto == 0 || last dfrom < last dto)
        where
            dfrom = getDisks t from
            dto = getDisks t to

-- a tower is complete if the left pole is empty (since that's always our starting pole),
-- and if any other pole has n disks
isComplete :: Tower -> Int -> Bool
isComplete t n = length left == 0 && (length middle == n || length right == n)
    where
        left = getDisks t PLeft
        middle = getDisks t PMiddle
        right = getDisks t PRight

move :: Tower -> Position -> Position -> Tower
move t@(Tower a b c) from to =
    -- the order we add poles to the tower doesn't matter, since position is contained
    -- in the pole data structure
    -- this fact makes creating a new tower easy, but complicates comparisons (hence the towerEqual function)
    -- also means we could possibly expand to n number of poles without many changes - except our Left Middle Right
    -- structure limits that, so it would actually require a full rewrite
    Tower (Pole from $ dfrom) (Pole to $ dto) (other)
        where disk = last $ getDisks t from
              dfrom = init $ getDisks t from
              dto = getDisks t to ++ [disk]
              -- the third pole will be the one without a position passed to the move function
              other = head $ filter (\p -> position p /= from && position p /= to) [a, b, c]

towerEqual :: Tower -> Tower -> Bool
towerEqual (Tower l1 m1 r1) (Tower l2 m2 r2) =
  -- the list comprehension should result in all the possible comparisons of tower poles
  -- if we have 3 or more poles that were equal, then the towers are equal
  (>=3) $ length $ filter (==True) [i == j | i <- [l1,m1,r1], j <- [l2, m2, r2]]

step :: Tower -> Tower -> Int -> Int -> (Tower, Int)
step prev t n m
  | isComplete t n = (t, m)
  | otherwise      = head $ filter (\tup -> snd tup /= 0) $ map stepIt directions
  where stepIt (from, to)
          | (movePossible t from to) && not (towerEqual (move t from to) prev) =
            step t (move t from to) n (m+1)
          | otherwise = (t, 0)

help :: String
help = "hanoi\n"
       ++ "Prints the number of moves required to move n disks to a new peg\n"
       ++ "Usage: hanoi <n> \n"
       ++ "OPTIONS \n"
       ++ "n      number of disks"

main = do
  args <- getArgs
  case args of
    [sn] -> do
      let n = read sn :: Int
      print $ step (fill 0) (fill n) n 0
    _ -> do
      putStr help
