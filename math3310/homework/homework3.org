#+OPTIONS: toc:nil num:nil
#+LATEX_CLASS_OPTIONS: [10pt,AMS Euler]
#+LATEX_HEADER: \textheight=9.25in \textwidth=7in \topmargin=-0.75in \oddsidemargin=-0.25in \evensidemargin=-0.25in
* Homework 3
** 3.1
Please prove the identity $F_{n+2} = 1 + \sum_{i=0}^{n} F_i$ using mathematical
induction. \\

/Proof/: \\
By induction:

If /n/ = 0, then the relationship is true:
#+BEGIN_EXPORT latex
\begin{align*}
F_2 = 1 &= 1 + \sum_{i=0}^{0} F_i && \\
&= 1 + F_0 && \\
&= 1 + 0 && \\
1 &= 1 &&
\end{align*}
#+END_EXPORT

This is the basis for the induction.
We must now show that if $F_{k+2} = 1 + \sum_{i=0}^{k} F_i$ is true where
$k \geq 0$, then it follows that $F_{k+3} = 1 + \sum_{i=0}^{k+1} F_i$ is true.

We assume the relationship holds for some $k \geq 0$;
that is $F_{k+2} = 1 + \sum_{i=0}^{k} F_i$. To show this
relationship implies $F_{k+3} = 1 + \sum_{i=0}^{k+1} F_i$, consider
$1 + \sum_{i=0}^{k+1} F_i$
#+BEGIN_EXPORT latex
\begin{align*}
1 + \sum_{i=0}^{k+1} F_i &= 1 + \sum_{i=0}^{k} F_i + (F_{k+1}) && \\
 &= F_{k+2} + F_{k+1} && \text{Substitute our assumed relationship} \\
 &= F_{k+3} && \text{Definition of Fibonacci numbers} \\
\end{align*}
#+END_EXPORT

Our first equality follows from the induction assumption, the second from the
definintion of Fibonacci numbers[fn:1]. We know the relationship is true for n = 0,
and for $n = k \geq 0$ implying the truth for $n = k + 1$. Therefore the
relationship is true for all nonnegative integers by the Principle of
Mathematical Induction.
\clearpage
** 3.2
Suppose $c_1,c_2,\cdots,c_j$ are positive integers serving as capacities of
water jugs in some fixed unit. Suppose also that 
$c_1 \leq c_2 \leq \cdots \leq c_j$ and that $\gcd(c_1,\cdots,c_j) = 1$. \\
Use the following theorem to prove that /n/ units of water, where /n/ is an
integer satisfing $0 \leq n \leq c_1 + c_2 + \cdots + c_j$, can be measured
using the jugs and the processes of filling a jug, emptying a jug into another,
and completely emptying a jug. \\
*Theorem* Given the jug capacities as above and an amount $\hat{n}$ of water
with $0 \leq \hat{n} \leq c_j$, the largest jug can be filled with $\hat{n}$
units of water.\\

/Proof/: \\

Consider /c/ to be the sum of the jug quantities $c_1 + c_2 + \cdots + c_j$.
We must prove that /n/ units of water, where /n/ is an integer satisfing
$0 \leq n \leq c$, can be measured using these jugs and mentioned processes. \\

The case where $n = 0$ or $n = c$ is trivial: we either empty all of the jugs,
or fill all of the jugs. Therefore, we will focus on the case where $0 < n < c$ \\

Let us assume that $n = c_1 x_1 + c_2 x_2 + \cdots + c_j x_j$ or that
$n = \sum_{k=1}^j c_k x_k$. Recall Bézout's lemma that states if
$\gcd(a_1,a_2,\cdots,a_n) = d$, then there are integers $x_1, x_2,\cdots,x_n$
that satisfy the equation $d = a_1 x_1 + a_2 x_2 + \cdots + a_n x_n$. We can
apply this to our problem. We know the gcd of $c_1 \cdots c_j$ is 1; $d = 1$,
therefore, by Bézout's lemma, we know that there are integers
$x_1, \cdots, x_j$ that satisfy $1 = \sum_{k=1}^j c_k x_k$. This means, that
through some process of filling and dumping jugs, we can have a final total of
1 unit of water.

** 3.3
Recall that we discussed number systems by writing an ordered triple (X,Y,Z)
where X is a set of things we call 'numbers', Y is the notation for an operation
we call 'addition', and Z is a notation for what we call 'multiplication'. We
can do something analogous with /Logical Systems/: we specify the set of
statements, the function that determines truth, and the logical operations and
operators. For the logical system that we (and essentially everyone) use, lets
use the notation $(\mathcal{M}, \Phi, \implies, \land, \lor, \lnot)$ to mean
that $\mathcal{M}$ is the set of statements we work with, $\Phi$ is the function
that asseses truth, and the others are the logical operations and operator.\\
Please prove or disprove that our logical system 
$(\mathcal{M}, \Phi, \implies, \land, \lor, \lnot)$ can be replaced with
$(\mathcal{M}, \Phi, \nabla)$, where $\nabla$ is defined as follows. For 
$x,y \in \mathcal{M}$, $x \nabla y$ is equivalent to $\lnot(x \lor y)$.
* COMMENT Local Variables
# Local Variables:
# eval: (add-hook 'after-save-hook 'org-latex-export-to-pdf nil t)
# End:

* Footnotes

[fn:1] This is achieved by shifting the indexes of the standard Fibonacci
definintion; $F_n = F_{n-1} + F_{n-2}$ becomes $F_{n+3} = F_{n+2} + F_{n+1}$
