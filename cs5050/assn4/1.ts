type CompareFunction<T> = (a: T, b: T) => boolean;

class BinaryHeap<T> {
  private heap: T[];

  constructor(arr: T[], private comparison: CompareFunction<T>) {
    for (const k of arr) {
      this.insert(k);
    }
  }

  left(i): T {
    return this.heap[this.leftIdx(i)];
  }

  right(i): T {
    return this.heap[this.rightIdx(i)];
  }

  parent(i): T {
    return this.heap[this.parentIdx(i)];
  }

  get(i): T {
    return this.heap[i];
  }

  leftIdx(i: number): number {
    return 2*i + 1;
  }

  rightIdx(i: number): number {
    return 2*i + 2;
  }

  parentIdx(i: number): number {
    return Math.floor((i - 1) / 2);
  }

  private siftUp(i: number) {
    if (this.comparison(this.heap[i], this.heap[this.parentIdx(i)]) && i !== 0) {
      [ this.heap[i], this.heap[this.parentIdx(i)] ] = [ this.heap[this.parentIdx(i)], this.heap[i] ];
      this.siftUp(this.parentIdx(i));
    }
  }

  private siftDown(i: number) {
    while (!this.comparison(this.heap[i], this.heap[this.leftIdx(i)])
        || !this.comparison(this.heap[i], this.heap[this.rightIdx(i)])) {
          const smaller = this.comparison(this.heap[this.leftIdx(i)], this.heap[this.rightIdx(i)])
            ? this.leftIdx(i) : this.rightIdx(i);
          [ this.heap[i], this.heap[smaller] ] = [ this.heap[smaller], this.heap[i] ];
        i = smaller;
    }
  }

  peek(): T {
    return this.heap[0];
  }

  extract(): T {
    const root = this.heap.shift();
    const last = this.heap.pop();
    this.heap.unshift(last);
    this.siftDown(0);
    return root;
  }

  delete(): T {
    throw "Unimplemented";
  }
  
  insert(k: T) {
    const len = this.heap.push(k);
    this.siftUp(len - 1);
  }
  
  merge(other: BinaryHeap<T>): BinaryHeap<T> {
    throw "Unimplemented";
  }
}


const checkMinLess = (h: BinaryHeap<number>, k: number, x: number) => {

  let count = 0;
  const checkNode = (h: BinaryHeap<number>, i: number, k: number, x: number) => {
    if (h.get(i) < x) {
      count++;
      if (count >= k)
        return;
      checkNode(h, h.leftIdx(i), k, x);
      checkNode(h, h.rightIdx(i), k, x);
    }
  }

  checkNode(h, 0, k, x);
  if (count >= k)
    return true;
  return false;
}