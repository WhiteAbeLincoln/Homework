% Created 2018-03-20 Tue 14:54
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\textheight=9.25in \textwidth=7in \topmargin=-0.75in \oddsidemargin=-0.25in \evensidemargin=-0.25in \usepackage{mathtools} \DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\author{Abraham White}
\date{\today}
\title{}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.3.1 (Org mode 9.1.2)}, 
 pdflang={English}}
\begin{document}

\section*{Assignment 4: Data Structure Design}
\label{sec:org0ffe4d5}
\section*{1}
\label{sec:org3c9caf4}
Suppose we have a min-heap with \emph{n} distinct keys that are stored in an array
\(A[1...n]\). Given a value \(x\) and an integer \(k\) with 1 \(\le\) \(k\) \(\le\) \(n\),
design an algorithm to determine whether the \emph{k}-th smallest key in the heap
is smaller than \(x\) (so your answer should be "yes" or "no"). The running time
of your algorithm should be \(O(k)\), independent of the size of the heap.

\subsection*{Algorithm}
\label{sec:org498c5b6}
Perform a depth-first traversal of the tree, incrementing a counter whenever a
value less than \(x\) is encountered. Stop traversing when a value greater than
\(x\) is found. If in the end the counter is greater than or equal to \(k\), then
we know \(x\) is greater than the \emph{k}-th smallest key.
\subsection*{Main Idea}
\label{sec:org6d787d1}
\begin{enumerate}
\item A minimum-heap is partially ordered, so we know that the \emph{k}-th smallest
node will be among the first \emph{k} nodes of the tree.
\item If \(x\) is larger than the \emph{k}-th node, we would traverse at least \(k\)
nodes before finding one larger than \(x\).
\item If \(x\) is smaller than the \emph{k}-th node, then we will not have traversed
\(k\) nodes before we reach the first node larger than \(x\).
\end{enumerate}
\subsection*{Time Analysis}
\label{sec:org3020a63}
The number of visited nodes with values less than \(x\) will be at most \(k\).
Every node that we visit with a value greater than \(x\) will be a child of a
node with a value less than \(x\). In the worst case, we will check every child
of the \(k\) nodes less than \(x\), giving us worst case \(O(2k) = O(k)\) running time.
\subsection*{Pseudocode}
\label{sec:org0745a19}
\begin{verbatim}
const algorithm = (heap, k, x) => {

  let count = 0;
  const checkNode = (idx, k, x) => {
    if (heap.get(idx) < x) {
      count++;
      if (count <= k) return;
      checkNode(heap.leftOf(idx), k, x);
      checkNode(heap.rightOf(idx), k, x);
    }
  }

  checkNode(0, k, x);
  return count >= k;
}
\end{verbatim}
\section*{2}
\label{sec:org0d292d2}
Suppose you are given a binary search tree \(T\) of \(n\) nodes. We assume that no
two keys in \(T\) are equal. Given a value \(x\), the \emph{rank} operation
\(\operatorname{rank}(x)\) to return the \emph{rank} of \(x\) in \(T\), which is defined
to be one plus the number of keys of \(T\) smaller than \(x\). Note that \(x\) may
or may not be a key in \(T\). Let \(h\) be the height of \(T\). Augment \(T\) so that
\emph{rank}, \emph{search}, \emph{insert}, and \emph{delete} all take \(O(h)\) time.

\subsection*{Algorithm}
\label{sec:org007bc95}
Store the size of the subtree in each node as an additional value on the
node. Every operation that modifies the tree (\emph{search}, \emph{insert}, \emph{delete})
must now maintain that, given a node \(v\), \(v\).size = \(v\).left.size +
\(v\).right.size + 1. To count the rank of some node, we perform an inorder
walk of the tree. The rank of \(x\) is the number of nodes that come before \(x\)
in an inorder walk plus one.
\subsection*{Pseudocode}
\label{sec:org7151004}
\begin{verbatim}
const rank = (T, x) => {
  let r = x.left.size + 1;
  let y = x;
  while (y !== T.root) {
     // if y is the right child of its parent
     if (y === y.parent.right) {
       r = r + y.parent.left.size;
     }
     y = y.parent;
  }
  return r;
}
\end{verbatim}
\subsection*{Time Analysis}
\label{sec:org7e4414c}
In worst case, \(x\) is one of the leaf nodes of the tree. Because we only
ascend the tree through a node's parent, we will traverse at most \(O(\log n)\)
nodes before we reach the root, giving a worst case time of \(O(h)\).
\subsection*{Time Analysis for search insert delete}
\label{sec:org858dc44}
If we maintain a balanced tree, the time for \emph{search}, \emph{insert}, and \emph{delete}
will not change. This is because of the theorem given in class:
\begin{quote}
A field \(f\) on a node \(v\) in a balanced tree can be maintained in \(O(\log n)\)
time if \(f\) can be computed using only information contained in \(v\) and \(v\)'s
left and right children.
\end{quote}
Because our size augmentation only uses information contained in the left and
right children, we can perform updates in \(O(\log n)\) or \(O(h)\) time.

\section*{3}
\label{sec:orgfc81694}
\subsection*{Algorithm}
\label{sec:org4feece8}
The algorithm is simple for a sorted list: perform a binary search for \(x_1\)
and \(x_2\) and return all elements between the two indexes returned by the search.
However, it is a bit more difficult with a structure like a binary search
tree. \\

On each node \(v\) of the tree, store a count of the number of nodes in the tree
with values less than the value of \(v\). Maintaining this can be done in
\(O(\log n)\) time for a balanced tree. Find the node containing a value \(x_1\).
Get the count of the number o
\section*{4}
\label{sec:org6e5fbcc}
\subsection*{Algorithm}
\label{sec:org266d00f}
Maintain a an interval on each node of the tree containing the minimum
and maximum values stored in it's sub trees: i.e, given a node \(v\) with leaf
children having values 1 and 6, the range on \(v\) would be [1, 6]. This can be
maintained in \(O(\log n)\) time because the minimum and maximum values can be
determined from the ranges stored on a node \(v\)'s children. \\

We can now traverse the tree, summing the values where the current node's
intervals overlap with the requested interval.
\subsection*{Pseudocode}
\label{sec:org57fbb5d}
\begin{verbatim}
const algorithm = (v, x1, x2) => {
  if (!rangeOverlaps(v.range, [x1, x2]) {
    return 0;
  } else if (firstRangeWithinSecond(v.range, [x1, x2])) {
    return v.value;
  } else {
    return algorithm(v.left, x1, x2) + algorithm(v.right, x1, x2);
  }
}
\end{verbatim}
\subsection*{Time Analysis}
\label{sec:orgc4fc94e}
This takes \(O(\log n) = O(h)\) time. Proof: \\
We have two cases when querying a node with this algorithm:
\begin{enumerate}
\item When querying at some node, we recurse to only one child. Therefore, the
number of nodes processed is 1 + number of nodes to process in subtree.
This is at most \(O(\log n)\).
\item When querying some node, we recurse to both children because the range
contains both children. \ldots{}
\end{enumerate}
\end{document}