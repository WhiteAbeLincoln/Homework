const head = (a) => a[0];
const tail = (a) => a.slice(1);

/**
 * 
 * @param {Array<number>} a 
 * @returns {[Array<number>, number]}
 */
const sort_count = (a) => {
  if (a.length === 1) return [a, 0]
  else {
    const left = (xs) => splitAt(Math.floor(xs.length / 2))(xs)[0]
    const right = (xs) => splitAt(Math.floor(xs.length / 2))(xs)[1]
    const [L, nl] = sort_count(left(a))
    const [R, nr] = sort_count(right(a))
    const [S, ns] = merge_count_split(L, R)
    return [S, nl + nr + ns];
  }
}

/**
 * 
 * @param {Array<number>} a 
 * @param {Array<number>} b 
 * @returns {[Array<number>, number]}
 */
const merge_count_split = (a, b) => {
  if (!a.length) return [b, 0];
  if (!b.length) return [a, 0];

  const [x, ...xs] = a;
  const [y, ...ys] = b;

  if (x <= y) {
    const [arr, num] = merge_count_split(xs, b);
    return [[x, ...arr], 0+num]
  } else {
    console.log('split: ', y);
    const [arr, num] = merge_count_split(a, ys);
    return [[y, ...arr], a.length+num]
  }
}

/**
 * 
 * @param {Array<any>} a 
 * @param {Array<any>} b 
 * @param {(a, b) => boolean} pred
 * @returns {Array<number>}
 */
const merge = (a, b, pred = (x, y) => x <= y) => {
  if (a.length === 0) {
    return b;
  }

  if (b.length === 0) {
    return a;
  }

  const [x, ...xs] = a;
  const [y, ...ys] = b;

  if (pred(x, y)) {
    console.log(x)
    return [x, ...merge(xs, b, pred)]
  } else {
    // when this branch is called, we had an inversion
    console.log('split: ', y)
    return [y, ...merge(a, ys, pred)]
  }
}

/**
 * 
 * @param {number} idx 
 */
const splitAt = (idx) =>
  /**
   * @param {Array<any>} xs
   * @returns {[Array<any>, Array<any>]}
   */
  (xs) => [xs.slice(0, idx), xs.slice(idx)]

/**
 * 
 * @param {*} xs 
 */
const mergesort = (xs, pred) => {
  if (xs.length === 0) return [];
  if (xs.length === 1) return xs;
  const halved = splitAt(Math.floor(xs.length / 2))(xs)
  const left = mergesort(halved[0], pred)
  const right = mergesort(halved[1], pred)
  if (pred)
    return merge(left, right, pred)
  else
    return merge(left, right)
}

/**
 * 
 * @param {Array<number>} arr 
 */
const findInversions = (arr) => {
  let num = 0;
  console.log(merge([1, 3, 5], [2, 4, 6]))
  console.log('-----------------------\n');
  console.log(merge_count_split([1, 3, 5], [2, 4, 6]))
  console.log('-----------------------\n');
  console.log(sort_count([4,2,9,1,7]))
}

findInversions([6, 5, 4, 3, 2, 1])