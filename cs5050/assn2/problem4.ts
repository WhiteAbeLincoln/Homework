#! /usr/bin/env ts-node

const head = <T>(a: T[]) => a[0];
const tail = <T>(a: T[]) => a.slice(1);

const left = <T>(xs: T[]): T[] => splitAt(Math.floor(xs.length / 2))(xs)[0]
const right = <T>(xs: T[]): T[] => splitAt(Math.floor(xs.length / 2))(xs)[1]

const splitAt = (idx: number) => <T>(xs: T[]): [T[], T[]] => [xs.slice(0, idx), xs.slice(idx)]

const getX = <T>(p: (a: T, b: T) => boolean) => (A: T[]): T => {
  if (A.length === 1) return A[0];
  const m_1 = getX(p)(left(A));
  const m_2 = getX(p)(right(A));

  return p(m_1, m_2) ? m_1 : m_2
}

const getMax = getX((x: number, y) => x > y)
const getMin = getX((x: number, y) => x < y)

const getX2 = <T>(p: (a: T, b: T) => boolean) => (A: T[]) => {
  let m = A[0];

  for (let i = 1; i < A.length; i++) {
    if (p(A[i], m)) m = A[i]
  }

  return m;
}

const getMax2 = getX2((x: number, y) => x > y)
const getMin2 = getX2((x: number, y) => x < y)

const getMinIdx = getX((x: {v: number, i: number}, y) => x.v < y.v)
const getMaxIdx = getX((x: {v: number, i: number}, y) => x.v > y.v)

const find_max_diff = (A: number[]): number => {
 /* Main idea: If we find the maximum on the right subtree and
  * the minimum on the left, this will give us the maximum difference
  *
  */
  if (A.length === 1) return 0;

  // find the maximum difference in the left half
  const left_diff = find_max_diff(left(A))
  // find the maximum difference in the right half
  const right_diff = find_max_diff(right(A))

  // for finding when split over the left and right
  const min = getMin(left(A))
  const max = getMax(right(A))
  const split_diff = max - min;

  return Math.max(split_diff, left_diff, right_diff)
}

const max = <T>(pred: (a: T, b: T) => boolean) => (...values: T[]): T => {
  if (values.length === 1) return values[0];

  const l = max(pred)(...left(values));
  const r = max(pred)(...right(values));

  return pred(l, r) ? l : r;
}

const find_max_diff_idx = (A: {v: number, i: number}[]): {diff: number, sell: number, buy: number} => {
  if (A.length === 1) return {
    diff: 0, sell: A[0].i, buy: A[0].i
  }

  const left_diff = find_max_diff_idx(left(A))
  const right_diff = find_max_diff_idx(right(A))

  const minL = getMinIdx(left(A))
  const maxR = getMaxIdx(right(A))
  const split_diff = {diff: maxR.v - minL.v, sell: maxR.i, buy: minL.i}

  return max((a: {diff: number, sell: number, buy: number}, b) => a.diff > b.diff)(split_diff, left_diff, right_diff)
}

const start = Date.now()
const to_indexed = <T>(A: T[]) => A.map((v, i) => ({v, i}))
console.log(
  max(({v,i}: {v: number, i: number}, b) => v > b.v)(
    {v: 1, i: 1},
    {v: 2, i: 2},
    {v: 8, i: 2},
    {v: 9, i: 2}
  )
)
console.log(find_max_diff_idx(
  to_indexed([5, 4, 3, 2, 1])
))
console.log(find_max_diff_idx(
  to_indexed([9, 1, 5, 4, 7])
))
// console.log(Date.now() - start);