const head = (a) => a[0];
const tail = (a) => a.slice(1);

const left = (xs) => splitAt(Math.floor(xs.length / 2))(xs)[0]
const right = (xs) => splitAt(Math.floor(xs.length / 2))(xs)[1]

const flip = (f) => (a) => (b) => f(b)(a)

/**
 * 
 * @param {number[]} arr 
 * @param {number} r 
 * @param {number} l 
 * @param {number} x 
 * @returns {number}
 */
const binarySearch = (arr, r, l, x) => {
  if (r >= l) {
    const mid = l + (r - l)/2;

    if (arr[mid] === x)
      return mid;

    if (arr[mid] > x)
      // We haven't reached it yet
      return 1 + binarySearch(arr, l, mid-1, x);

    return binarySearch(arr, mid+1, r, x);
  }

  return -1;
}

/**
 * 
 * @param {number} idx 
 */
const splitAt = (idx) => (xs) => [xs.slice(0, idx), xs.slice(idx)]

/**
 * 
 * @param {number[]} A 
 * @param {number} b 
 */
const find_less_than = (A, b) => {
  if (A.length === 1) return A[0] < b ? 1 : 0
  else return find_less_than(left(A), b) + find_less_than(right(A), b)
}

/**
 * 
 * @param {number[]} A 
 * @param {number[]} B 
 */
const find_less = (A, B) => {
  return B.map(b => [b, find_less_than(A, b)])
}

console.log(find_less([30,20,100,60,90,10,40,50,80,70], [60,35,73]))