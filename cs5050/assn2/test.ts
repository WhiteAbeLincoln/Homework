#! /usr/bin/env ts-node

const head = <T>(a: T[]) => a[0];
const tail = <T>(a: T[]) => a.slice(1);

const left = <T>(xs: T[]): T[] => splitAt(Math.floor(xs.length / 2))(xs)[0]
const right = <T>(xs: T[]): T[] => splitAt(Math.floor(xs.length / 2))(xs)[1]

const splitAt = (idx: number) => <T>(xs: T[]): [T[], T[]] => [xs.slice(0, idx), xs.slice(idx)]

type Predicate<T> = (x: T, y: T) => boolean;

const merge = <T>(A: T[], B: T[], pred: Predicate<T>): T[] => {
  if (A.length === 0) return B;
  if (B.length === 0) return A;

  const [x, ...xs] = A;
  const [y, ...ys] = B;

  if (pred(x, y)) {
    return [x, ...merge(xs, B, pred)]
  } else {
    return [y, ...merge(A, ys, pred)]
  }
}

const sort_funcs = (A: Array<(n: number) => number>): typeof A => {
  if (A.length === 0 || A.length === 1) return A;

  const left_arr = sort_funcs(left(A));
  const right_arr = sort_funcs(right(A));

  return merge(left_arr, right_arr, (f, g) => {
    for (let i = 0 ;; i++) {
      if (f(i) === g(i)) continue;
      if (f(i) < g(i)) return true;
      if (f(i) > g(i)) return false;
    }
  });
}

console.log(sort_funcs([
  (x) => x+4,
  (x) => x+1,
  (x) => x+3,
  (x) => x+2,
]).map(f => f(0)))