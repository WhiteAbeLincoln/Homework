* Scrum 
A software engineering development framework
- Lightweight
- Easy to understand
- Simple to use
** Scrum Framework
*** Roles
**** Product owner
- Defines features
- Decides on release date and content
- Provides features according to market value
- Adjust features and priority each iteration as needed
- Represents customers users stakeholders
**** Scrum Master
- Represents management to project
- Responsible for enacting values and practices
- Removes impediments
- Ensures that team is fully functional
- Enables cooperation accross roles and functions
- Shields the team from external interferences
**** Team Members
- Cross-functional, all of skills necessary to create product
- No titles other than "Developer", regardless of actual work
- May have specialized skills, but whole team is accountable
- No sub teams
- Optimal size: 4-6 members
*** Events
**** Sprint
- Heart of scrum
- Time box of 1 month or less during which a "Done", usable and potentially-releasable product increment is created
- Consistent durations throughout development
- New sprint starts immediately afer previous sprint
***** During Sprint
- No changes are made that will affect goal
- Team composition remains constant
**** Sprint planning
- Held at beginning of each sprint
- Meeting where work to be performed is planned
***** Parts
****** What will be delivered?
****** How will work get done?
- Develoment team decides how to build functionality into "Done" product increment
- *Product Backlog* items are select for Sprint, plus plan for delivering is called *Sprint Backlog*
- Develoment team self-organizes the *Sprint Backlog* by estimating time it will take to complete each item
**** Daily Scrum Meeting
- 15 minute time boxed event to synchronize activites, create plan for next 24 hours
- Held at same time and same place each day
- 3 Questions: What did you do since last meeting? What do you hope to accomplish?
***** Advantages
- Improves communication among team members
- Eliminates need for other meetings
- Identifies and removes impediments
- Improves the Develomemnt Team's level of project knowledge
- Promotes accountability for members' work
**** Sprint Review Meeting
- Time when Develompent Team presents what it accomplished during the sprint
- Takes the form of a demo of new features or concrete progress
- Informal - little prep time
- Entire team participates
**** Sprint Retrospective Meeting
- Opportunity for Scrum Team to reflect and create plan for improvements
***** Purpose
- Think about how the last sprint went for People, relationships, process, tools
- Identify major items that went well and potential improvements
- Create plan for implementing improvements to the way the scrum team does its work
*** Artifacts
**** Project Charter
- Problem Statement: Short and succinct
- Project Objectives
- Stakehodlers
- Delivereables
**** Product Backlog
- Ordered list of everything that might be needed in the product and single source of *Requirements* for any changes to be made
- Product Owner is repsonsible for backlog: content availability, ordering
- Never complete
- Lists all features, functions, requirements, enhancements, and fixes that constitute future changes to product
- Backlog Items are sometimes called "User Stories"
**** Sprint Backlog
- Selects items from product backlog to complete during a given sprint
- Team itentifies tasks to complete these and time for each is estimated
***** Managing the sprint backlog
- Individuals sign up for work of their choosing (Never assigned)
- Estimated work remainting is updated daily
- Any team member can modify backlog
* Design
** Use Case Diagrams
- Describe what a systemd does from the standpoint of an external observer
- Emphasis is on /what/ a system does, not /how/
*** Actor
- Outside or external to the system (Human, peripheral device)
- Labeled with descriptive noun
**** Identifing Actors
- What
*** Tasks
- Verb
*** Relationships
**** Include
**** Extend
- Represents extension of a use case
**** Generalization
- Represented by line and hollow arrow from child to parent
**** Boundaries
** UML and Class Diagrams
- A standard graphical language for modeling object oriented software
*** Use Case Diagrams
- Describe functionality of system from external perspective
*** Class Diagrams
- Describe classes and their relationships
*** Interaction
- Show behabior of systems in terms of how objects interact with each other
*** State
- Show how systems behave internally
*** Component and deployment
- Show how the various components of systems are arranged logically and physically
*** UML Features
- Detailed semantics
*** What constitutes a good model?
- Uses standard notation
- Understandable by all stakeholders
- help software engineers develop insights
- provide abstraction
*** Essentials of UML
**** Class
- Box with name of class inside
- May also show operations and attributes
**** Associations and Multiplicity
- Show how two classes are related
- Solid line from two classes
- Symbols representing multiplicity [0-9,*,..] are show at each end of association
- Each association can be labeled to make explicit the nature of association
***** Many-to-one
***** Many-to-many
***** One-to-one
***** Reflexive association
- Connects class to itself
***** Generalization
- Generalization set is a labelled group of generalizations with common superclass
- label (called discriminator) describes the criteria used in specialization
***** Aggregation
- Represent part-whole relationships
***** Composition
- Strong aggregation.
- If aggregate is destroyed, then parts are destroyed as well
**** Notes and Descriptive text
***** Note
- Small block of text embeded in uml diagram
- Acts like a comment
***** Descriptive text
- Embed diagrams in a larger document
- Text can explain aspects of system using any notation
- Highlight and expand on important features, give rationale
* Development
+ Make sure you have good hardware that won't impair your process
+ Test on the hardware the customer is likely to use
** Development Environment
+ Invest in good software to support process
+ Use Source Code Control
+ Use tools that speed process
  - Profilers
  - Testing Tools
  - Source code formatters
  - Debuggers
+ Invest in Training
** Development Best Practices
+ Establish coding standards - formatting, naming conventions
+ Reuse code
+ Write for people, not computers
  - Meaningful variable names
  - Indent nicely
  - Use comments
  - Spell correctly
+ Write self-documenting code
  - Use descriptive names for classes, methods, properties
  - No magic numbers - use constands or enumerated types
+ Always validate results
  - Murphy's law: "Anything that can go wrong will go wrong"
+ Use Exceptions - handle error cases
** Prototyping
+ Low fidelity prototype :: Sketch on napkin, whiteboard
+ High fidelity prototype :: Powerpoint, Photoshop
*** Paper Prototypes
+ Cheap way to design alternatives
+ Encourages reflection
+ Document and communicate design
  - Makes ideas explicit
  - Stakeholders can see, hold, interact
+ Start with wireframe, then proceed to more detail
* Testing
** Effective and Efficient Testing
+ To test effectively, you moust use a strategy that uncovers as many
  defects as possible
+ To test efficiently, you must find the largest possible number of defects
  using the fewest possible tests
** Equivalence classes
+ Don't test by brute force, using every possible input value
+ Divide inputs into groups
+ Make sure at least one test is run with every possible equivalence class
*** Testing at boundaries of equivalence classes
+ A lot of errors occur at boundaries of equivalence classes
+ Should specifically test values at the extremes of each equivalence class
+ E.g. if valid input is month (1-12)
  - Test 0,1,2,11,12,13
** Black-box testing
** White-box testing
+ Testers have access to system design
  - Can examine code, documentation
** Formal Testing
+ Test case :: an explicit set of instructions designed to get results for
               equivalence classes
+ Test plan :: document that contains a complete set of test cases for a system
*** Information to include in test case
**** Identifictation and Classifictaion
+ Number, title
+ System, subsystem or module being tested
+ Importance of test case
**** Instructions
+ Tell tester exactly what to do
+ The tester should not normally have to refer to any documentation in order to
  execute the instructions
**** Expected result
+ Tells the tester what the system should do in response to the instructions
+ The tester reports a failure if the desired result is not recieved
*** Levels of importance in test cases
**** Level 1
+ Critical
+ Designed to verify system runs and is safe
**** Level 2
+ General test cases
+ Verify that day-to-day functions correctly
**** Level 3
*** Determining test cases
It is important that the test cases test every aspect of the requirements
** Strategies for large systems
+ System testing :: Take the entire system and test as unit
+ Integration testing :: Test each individual subsystem in isolation. Continue
     testing as you add more subsystems to the final project
** Test-fix-test cycle
 When failure occurs during testing
+ Each failure report is enetered into a failure traking system
+ Screened and assigned priority
+ Low-priority failures can be put on known-bugs list
+ Some failure reports can be merged if appear to be same cause
+ Somebody is assigned to investigate a failure
+ That person traks down the defect and fix it
+ Finally a new version of the system is created, ready to be tested again
** Ripple Effect
There is a high probability that the efforts to remove the defects may have
actually added new defects
+ The maintainer tries to fix problems without fully understanding the
  ramifications of the changes
+ The maintainer makes ordinary human errors
The system /regresses/ into a more and more failure-prone state
** Unit testing
+ Verifies the correctness of a specific piece of code
+ Usually apply to methods
+ Usually conducted by developer
+ Automated testing uses this
** Regression Testing
+ Too expensive to re-run every single test case every time a change is made to
  software
+ Only a subset of the previously-successful test casees is actually re-run
+ Carefully selected to cover as much of system as possible
** People involved in testing
+ Test Lead :: Oversees development of test cases and verifies test plans have
               been executed
+ Testers :: Writes test plans, cases and procedures; Executes tests
+ Developers :: Builds automated unit tests
** Testing performed by users and clients
+ Alpha testing ::
+ Beta testing ::
+ Acceptance testing ::
* COMMENT Local Variables
  # Local Variables:
  # eval: (add-hook 'after-save-hook 'org-latex-export-to-pdf nil t)
  # End:
