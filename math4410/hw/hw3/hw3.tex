% Created 2020-02-11 Tue 02:05
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../../preamble.tex}
\author{Abraham White}
\date{\today}
\title{Homework 3}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Homework 3},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{1}
\label{sec:orgf383153}
Find a closed formula for each of the following recurrences
\subsection*{a}
\label{sec:org0fff0db}
\[ R_n = R_{n-1} + 2n - 2, \text{ for } n>0,~~R_0 = 1 \]
\begin{answer}
Begin by defining a generating function \(A(x)\) such that \(R_n\) is the coefficient
on \(x^n\).
\[ A(x) = \sum_{n \geq 0} R_n x^n \]
Since \(R_n\) only works for \(n > 0\), we need to redefine it to work for all \(n \in \NN\).
First we say that \(R_n = 0\) for all \(n < 0\). Next we remove the explicit definition
\(R_0 = 1\) by adding an Iverson function.
\[ R_n = R_{n-1} + 2n - 2 + 3[n=0] \]
We've defined \(A(x)\) to be \(\sum_{n \geq 0} R_n x^n\), so substituting our definition
of \(R_n\) gives us
\begin{align*}
A(x) &= \sum_{n \geq 0} R_{n-1} x^n - 2\sum_{n\geq 0}nx^n - 2\sum{n\geq 0} x^n + 3 \sum{n\geq 0}[n=0]x^n \\
     &= \sum_{n \geq 0} R_{n-1} x^n - 2\sum_{n\geq 0}nx^n - 2\sum{n\geq 0} x^n + 3
     \shortintertext{Substitute the known generating functions}
     &= \sum_{n \geq 0} R_{n-1} x^n - 2\left(\frac{x}{(1-x)^2}\right) - 2\left(\frac{1}{1-x}\right) + 3
     \shortintertext{Shift the summation for the first term involving $R_{n-1}$, relying on the fact that $R_n=0$ for $n<0$}
     &= x\sum_{n \geq 0} R_n x^n - 2\left(\frac{x}{(1-x)^2}\right) - 2\left(\frac{1}{1-x}\right) + 3 \\
     &= xA(x) - 2\left(\frac{x}{(1-x)^2}\right) - 2\left(\frac{1}{1-x}\right) + 3 \\
     \shortintertext{Solve for $A(x)$}
     A(x)(1-x) &= \frac{-2x}{(1-x)^2} - \frac{2}{1-x} + 3 \\
     A(x) &= \frac{-2x}{(1-x)^3} - \frac{2}{(1-x)^2} + \frac{3}{1-x} \\
\end{align*}
Now we have a generating function for \(A(x)\).\\
Next we need to find \(R_n\) by getting the coefficient on \(x^n\) for each of these generating functions.
We already know that \([x^n]\{\frac{1}{1-x}\} = 1\), so \([x^n]\{\frac{3}{1-x}\} = 3\). Recall that in general,
\[ \frac{1}{(1-x)^k} =  \sum_{n\geq 0} \binom{n+k-1}{n} x^n \]
Using this we can determine \([x^n]\{\frac{2}{(1-x)^2}\} = 2\binom{n+1}{n}\). Finally,
\(\frac{x}{(1-x)^3} = \sum_{n\geq 0} \binom{n+2}{n} x^{n+1}\). Since we want the coefficient on \(x^n\), we
can shift back the index and say \(\frac{x}{(1-x)^3} = \sum_{n\geq 0} \binom{n+1}{n-1} x^{n}\), so
\([x^n]\{\frac{2x}{(1-x)^3}\} = 2\binom{n+1}{n-1}\). Putting this all together gives us
\[ R_n = 3 - 2\binom{n+1}{n} + 2\binom{n+1}{n-1} \]

Evaluating for \(n\) from 0 to 5 on WolframAlpha gives us \((1,1,3,7,13,21)\), which matches the initial conditions and the
original recurrence.
\end{answer}
\subsection*{b}
\label{sec:org39c43f5}
\[ a_n = 4a_{n-1} - a_{n-2} -6a_{n-3}, \text{ for } n>2,~~a_0 = 1, a_1=4, a_2=15 \]
\begin{answer}
Define \(A(x) = \sum_{n\geq 0} a_n x^n\). We redefine \(a_n\) as \(a_n = 4a_{n-1} - a_{n-2} -6a_{n-3} + [n=0]\), where
\(a_n = 0\) when \(n<0\).
Substituting for \(a_n\) in \(A(x)\) gives us:
\begin{align*}
A(x) &= 4 \sum_{n\geq 0} a_{n-1} x^n - \sum_{n\geq 0} a_{n-2} x^n - 6\sum_{n\geq 0} a_{n-3} x^n + \sum_{n\geq 0} [n=0] x^n \\
     &= 4x A(x) - x^2A(x) - 6x^3A(x) + 1 \\
     &= \frac{1}{(3x-1)(2x-1)(x-1)} \\
     \intertext{Split up the fraction using partial fraction decomposition}
     &= \frac{9}{4}\left(\frac{1}{1-3x}\right) - \frac{4}{3}\left(\frac{1}{1-2x}\right) + \frac{1}{12}\left(\frac{1}{1+x}\right)
\end{align*}
Recall that in general, \[ \frac{1}{1-kx} = \sum_{n\geq 0} k^n x^n \]
So finding the coefficient on \(x^n\) for these generating functions means
\[ a_n = \frac{9}{4}3^n - \frac{4}{3}2^n + \frac{1}{12}(-1)^n \]

Evaluating for \(n\) from 0 to 5 on WolframAlpha gives us \((1,4,15,50,161,504)\), which matches the initial conditions and the
original recurrence.
\end{answer}
\subsection*{c}
\label{sec:org8dca74c}
\[ a_n = 2a_{n-3} - a_{n-2} - 2a_{n-1}, \text{ for } n>2,~~a_0 = 1, a_1=3, a_2=5 \]
\begin{answeri}
Define \(A(x) = \sum_{n\geq 0} a_n x^n\). Redefine \(a_n\) as \(a_n = 2a_{n-3} -a_{n-2} + 2a_{n-1} + [n=0] + [n=1]\) and \(a_{n < 0} = 0\).
Substituting in \(A(x)\) results in:
\begin{align*}
A(x) &= 2 x^3 A(x) - x^2 A(x) + 2x A(x) + x + 1 \\
     &= \frac{x+1}{-2x^3 + x^2 - 2x + 1} \\
     &= \frac{3}{5} \left(\frac{x}{1+x^2}\right) - \frac{1}{5} \left(\frac{1}{1+x^2}\right) + \frac{6}{5} \left(\frac{1}{1-2x}\right)
\end{align*}
We know what the \(x^n\) coefficient is for \(\frac{1}{1-2x}\), but the other two generating functions are unknown. However, the
generating function in the first term seems to just be the generating function in the second \(\left(\frac{1}{1+x^2}\right)\)
shifted by one. Therefore, solving \([x^n]\{\frac{1}{1+x^2}\}\) seems to be a good place to start. \\

\noindent We begin by substituting \(-(x^2)\) for \(x\) in the fundamental generating function, resulting in
\[\frac{1}{1+x^2} = \sum_{n\geq 0} (-1)^n x^{2n} = 1x^0 + -1x^2 + 1x^4 + \cdots \]
Adding back in the zero coefficients indicates that this generating function creates the sequence \\
\((1,0,-1,0,1,0,-1,\ldots)\). Notice that when these points are plotted on the plane, they appear to be the discrete points
of a sinusoidal function with period 4. Based on the graph appearance, and since the function is 1 when \(n=0\), we can guess
that it is a cosine. The matching function then is \(\cos \left(\frac{\pi n}{2} \right)\), which would be the coefficient on
\(x^n\). We could just stop there, but it seems that there should be a function better suited to the discrete nature of the problem,
and a more rigourous way of finding it.

Because this is a cosine function and because of the alternating nature of the sequence, it seems that the imaginary
unit \(i\) should be involved somehow. Write out the sequence generated by \(\sum_{n\geq 0} i^n x^n\).
\[ (1,i,-1,-i,1,\ldots) \]
This is remarkably close to the sequence that we want. We just have to get rid of all of the \(i\). We can do this by adding
a similar sequence where all of the elements involving \(i\) are negated. This sequence should be what we want
\((1,-i,-1,i,1,\ldots)\). Notice that it is only every evenly indexed element that is negated, i.e. only the coefficients on
\(x^{2n}\) are negated. We know that \(\sum_{n\geq 0} (-1)^n x^n\) generates a sequence that does the same thing, so
multiplying the two should give us the correct sequence \(\sum_{n\geq 0} (-1)^n i^n x^n \xleftrightarrow{GF} (1,-i,-1,i,1,\ldots)\).
Adding these two generating functions removes the elements involving \(i\), but all of the other elements are doubled. However,
dividing by two easily fixes this. Therefore the final generating function for \((1,0,-1,0,1,\ldots)\) is 
\[\sum_{n\geq 0} \frac{(-i)^n + i^n}{2} x^n\]

Now that we know \([x^n]\{\frac{1}{1+x^2}\} = \frac{(-i)^n + i^n}{2}\), all that remains is to find \([x^n]\{\frac{x}{1+x^2}\}\).
This generating function is equivalent to the previous shifted by one term, \(\frac{x}{1+x^2} = x\sum_{n\geq 0} \frac{(-i)^n + i^n}{2} x^n\),
so by shifting terms we can say \(\frac{x}{1+x^2} = \sum_{n\geq 0} \frac{(-i)^{n-1} + i^{n-1}}{2} x^n\), and
\[[x^n]\{\frac{x}{1+x^2}\} = \frac{(-i)^{n-1} + i^{n-1}}{2}\]

\noindent Putting this all together results in
\[a_n = [x^n]\{A(x)\} = \frac{3}{10} ((-i)^{n-1} + i^{n-1}) - \frac{1}{10} ((-i)^n + i^n) + \frac{6}{5} 2^n \]

Evaluating for \(n\) from 0 to 5 on WolframAlpha gives us the sequence \((1,3,5,9,19,39)\), which jives with our initial
conditions and the original recurrence.
\end{answeri}
\subsection*{d}
\label{sec:orgf363bab}
\[ L_n = L_{n-1} + L_{n-2}, \text{ for } n>1,~~L_0 = 2, L_1=1 \]
\begin{answer}
Redefine \(L_n\) as \(L_n = L_{n-1} + L_{n-2} + 2[n=0] - [n=1]\), where \(L_{n<0} = 0\).
Define \(A(x)\) so that \([x^n]\{A(x)\} = L_n\).
\begin{align*}
A(x) &= \sum_{x\geq 0} L_n x^n \\
     &= \sum_{x\geq 0} \left(L_{n-1} + L_{n-2} + 2[n=0] - [n=1]\right) x^n \\
     &= xA(x) + x^2A(x) + 2 - x \\
     &= \frac{x-2}{x^2 + x - 1}
\end{align*}
After doing some complicated algebra, we can find a partial fraction decomposition.
\[ A(x) = \frac{1}{1 - \varphi x} +  \frac{1}{1 - \phi x},~~\varphi = \frac{1+\sqrt{5}}{2}, \phi = \frac{1-\sqrt{5}}{2} \]
Since \(\frac{1}{1-kx} = \sum_{n\geq 0} k^n x^n\), we know that
\[A(x) = \sum_{n\geq 0} \varphi^n x^n + \sum_{n\geq 0} \phi^n x^n = \sum_{n\geq 0} (\varphi^n + \phi^n) x^n\]
Therefore, \[L_n = [x^n]\{A(x)\} = \varphi^n + \phi^n \]
\end{answer}
\section*{2}
\label{sec:orgcdd96a4}
The infamous \emph{Combinatorial Bank Robber}, nicknamed Ardish, holds up banks and not only
demands money (usually, and strangely, \$500 in specific denominations) but also demands to
know the number of ways the cashier can give her the money. During her latest caper, Ardish
demanded \$500 in tens and twenties. Determine a generating function \(M(x)\) for which this
number is \([x^{500}]\{M(x)\}\) and a more compact one, \(\hat{M}(x)\) for which \([x^{50}]\{\hat{M}(x)\}\)
is this number. Determine this number using partial fractions (and the GFs of course).
\begin{answeri}
We begin by letting \(M_n\) be the number of ways of making \(n\) dollars with tens and twenties.
Let \(T(x)\) be a generating function where \([x^n]\{T(x)\}\) is the number of ways we can make \(n\) dollars
with only tens and \(U(x)\) be a generating function where \([x^n]\{T(x)\}\) is the number of ways we
can make \(n\) dollars with twenties.

Let us think about how \(T(x)\) and \(U(x)\) should behave.
\begin{enumerate}
\item We know that the amounts of money we can make out of \$10 bills must be multiples of 10.
Similarly, if all we have are \$20 bills, all amounts must be multiples of 20.
\item For both tens and twenties, there is only one way to make \$0, which is to have no bills at all.
\item Since we only have single bills available to us for \(T(x)\) and \(U(x)\), we know that there is only one
way to make each possible dollar amount.
\end{enumerate}

These facts imply that \(T(x) = x^0 + x^{10} + x^{20} + x^{30} + \cdots\), and \(u(x) = x^0 + x^{20} + x^{40} + x^{60} + \cdots\).
The appropriate generating functions for \(T(x)\) and \(U(x)\) are then \[ T(x) = \frac{1}{1-x^{10}},~~U(x) = \frac{1}{1-x^{20}} \]

We want to combine all "like terms" in these polynomials to get all possible ways of making bills with tens and twenties,
so 
\begin{align*}
M(x) &= T(x)U(x) \\
     &= \left(\frac{1}{1-x^{10}}\right)\left(\frac{1}{1-x^{20}}\right) \\
     &= \frac{1}{(1-x^{10})(1-x^{20})}
     \shortintertext{Find a partial fraction decomposition}
\frac{1}{(1-x^{10})(1-x^{20})} &= \frac{A}{1-x^{10}} + \frac{B}{1-x^{20}} \\
1 &= A(1-x^{20}) + B(1-x^{10}) \\
\end{align*}

Using a CAS like Maxima indicates that \([x^{500}]{M(x)} = 26\).
\end{answeri}
\section*{3}
\label{sec:org699c20b}
In how many ways can a \(2 \times 2 \times n\) pillar be built out of \(2 \times 1 \times 1\) bricks?
\begin{answeri}
\end{answeri}
\section*{4}
\label{sec:org39173b1}
Evaluate the sum \(\sum_{k=1}^{n-1} \frac{1}{k(n-k)}\) by
\subsection*{(1)}
\label{sec:org77048bf}
Using a partial fraction decomposition
\begin{answer}
\end{answer}
\subsection*{(2)}
\label{sec:orgc107205}
Treating the sum as a convolution and using generating functions
\begin{answer}
\end{answer}
\section*{5}
\label{sec:orga04f142}
A form of DNA found stuck to a space probe has been analyzed and found to consist of five
different molecules, call them \(a,b,c,d,e\). Research shows that the pairs \(cd,ce,ed\), and
\(ee\) cannot occur consecutively in a string of this mysterious DNA, but any string without
these \emph{forbidden pairs} can occur. So, for example \(bbcda\) is not possible, but \(bbdca\) is.
How many different mysterious DNA strings of length \(n\) are possible? Note that when \(n=2\),
there are 21 different strings because the left and right ends of the string are
distinguishable.
\begin{answeri}
\end{answeri}
\end{document}