% Created 2020-04-29 Wed 19:50
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../../preamble.tex} \DeclareMathOperator{\lcm}{lcm}
\author{Abraham White}
\date{\today}
\title{Homework 6}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Homework 6},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle

\section*{1}
\label{sec:org95a5a60}
Find the smallest positive integer \(x\) such that
\begin{align*}
x &\equiv 11 \mod{24}, \text{ and} \\
x &\equiv 16 \mod{35}
\end{align*}
\begin{answer}
Recall that given integers \(a,b,m\), the modulus congruence relation \(a \equiv b \mod{m}\) is equivalent to saying
\(a - b = mq\) for some \(q \in \ZZ\). Using this we can set up two equations:
\begin{align}
x &\equiv 11 \mod{24} \iff x - 11 = 24 q_1 \iff x = 24 q_1 + 11 \nonumber \\
x &\equiv 16 \mod{35} \iff x - 16 = 35 q_2 \iff x = 35 q_2 + 16 \nonumber
\shortintertext{Set these equations as equivalent}
24 q_1 + 11 &= 35 q_2 + 16 \nonumber \\
11 - 16 &= 35 q_2 - 24 q_1 \nonumber \\
-5 &= 35 q_2 - 24 q_1 \label{eq:1}
\end{align}
We know that \(24 \perp 35\). This means that by Bézout's Identity, \(1 = 24u + 35v\) for some \(u,v \in \ZZ\).
Multiply this equation by -5.
\[ -5 = (-5)24u + (-5)35v \]
We can set this equal to Eq. \eqref{eq:1} and solve for \(q_1\) and \(q_2\) in terms of \(u\) and \(v\).
\begin{align*}
35 q_2 - 24 q_1 &= (-5)24u + (-5)35v \\
-24 q_1 + 35 q_2 &= (-5)24u + (-5)35v \\
q_1 &= 5u \\
q_2 &= -5v
\end{align*}
Now all that remains is to solve for \(u\) and \(v\) using the Euclidean Algorithm.
\begin{minted}[]{python}
def gcd(a, b):
    """solves bezouts identity gcd(a, b) = a*u + b*v, returning (gcd, u, v)"""
    u0, u1 = 0, 1
    v0, v1 = 1, 0
    print("||{}|{}|{}|".format(b,u0,v0))
    print("||{}|{}|{}|".format(a,u1,v1))
    while a != 0:
        (q, a), b = divmod(b, a), a
        v0, v1 = v1, v0 - q*v1
        u0, u1 = u1, u0 - q*u1
        print("|{}|{}|{}|{}|".format(q,a,u0,v0))
    return (b,u0,v0)

print("|$q$|$r$|$u$|$v$|")
print("|---+---+---+---|")
_, u, v = gcd(24, 35)
print("We have found that $u={}, v={}$".format(u, v))
\end{minted}

\begin{center}
\begin{tabular}{rrrr}
\(q\) & \(r\) & \(u\) & \(v\)\\
\hline
 & 35 & 0 & 1\\
 & 24 & 1 & 0\\
1 & 11 & 1 & 0\\
2 & 2 & -1 & 1\\
5 & 1 & 3 & -2\\
2 & 0 & -16 & 11\\
\end{tabular}
\end{center}
We have found that \(u=-16, v=11\)

Now substitute for \(q_1\) in \(x = 24q_1 + 11\) using the relations \(q_1 = 5u\).
\begin{align*}
x &= 24q_1 + 11 \\
x &= 24(5u) + 11 \\
x &= 24 \cdot 5 \cdot -16 + 11 \\
x &= -1909
\end{align*}
Now we need to find this as a positive integer. Do do this we mod -1909 by the least common
multiple of 24 and 35, which happens to be \(24\cdot 35\), since \(24 \perp 35\).
This gives us \(x = -1909 \bmod (24\cdot 35) = 611\).

Finally, we check that this \(x\) is valid for the two congruences.
\begin{align*}
611 \bmod 24 &= 11 \\
611 \bmod 35 &= 16
\end{align*}
\end{answer}
\section*{2}
\label{sec:org6629c44}
Compute \(32^{11} \bmod 899\). Please do not use any technology; your write-up should show lots of
reductions to arithmetic that is totally brain-able.
\begin{answeri}
Note that 899 is equal to \(29 \cdot 31\), the product of two prime numbers. We can deduce that \(x \equiv 32^{11} \mod 899\)
if and only if \(x \equiv 32^{11} \mod 29\) and \(x \equiv 32^{11} \mod 31\).

One of the properties of congruence arithmetic is that \(a \equiv b \mod m \implies a^n \equiv b^n \mod m\), and congruence
is symmetric and transitive. We know that \(32 \equiv 1 \mod 31\). Using these facts, we can say \(32^{11} \equiv 1^{11} \mod 32\).

So now we know that \(x \equiv 1 \mod 31\).

Next, consider \(32 \bmod 29\). We know that \(32 \equiv 3 \mod 29\), so \(32^{11} \equiv 3^{11} \mod 29\). Decompose \(3^{11}\) into a product:
\(3^{11} = 3^3 \cdot 3^4 \cdot 3^4\). Now \(3^3 \equiv -2 \mod 29\), and we can find \(3^4 \bmod 29\) pretty easily using some arithmetic:
\begin{align*}
3^4 &= 3^2 \cdot 3^2 \\
&= 81 \\
81 &= 29(3) - 6 \iff 81 \equiv -6 \mod 29 \\
3^4 &\equiv -6 \mod 29
\end{align*}
This implies that \(3^{11} \equiv -2 \cdot -6 \cdot -6 \mod 29\). We can further simplify by finding \((-6)^2 \bmod 29\).
\begin{align*}
(-6)^2 &= 36 \\
36 &= 29(1) + 7 \iff (-6)^2 \equiv 7 \mod 29
\end{align*}
Now we know that \(3^{11} \equiv -2 \cdot 7 \mod 29\).
\begin{align*}
3^{11} &\equiv -2 \cdot 7 \mod 29 \\
       &\equiv -14 \mod 29 \\
       &\equiv 15 \mod 29
\end{align*}
This all implies that \[ 32^{11} \equiv 15 \mod 29 \]

This gives us two congruences to determine \(x\):
\begin{align*}
x &\equiv 15 \mod 29 \text{, and} \\
x &\equiv 1 \mod 31
\end{align*}
We can use Bézout's Identity again to determine \(x\), as in Problem 1. 
\begin{align}
x &\equiv 15 \mod 29 \iff x = 29 s + 15 \nonumber \\
x &\equiv 1 \mod 31 \iff x = 31 t + 1 \nonumber
\shortintertext{Subtract the two equations}
0 &= 29 s + 15 - (31 t + 1) \nonumber \\
-14 &= 29 s - 31 t \nonumber \\
14 &= 31 t - 29 s \label{eq:2.1}
\end{align}

Find \(u\) and \(v\) such that \(1 = 31u + 29v\).
\begin{center}
\begin{tabular}{rrrr}
\(q\) & \(r\) & \(u\) & \(v\)\\
\hline
 & 29 & 0 & 1\\
 & 31 & 1 & 0\\
0 & 29 & 1 & 0\\
1 & 2 & 0 & 1\\
14 & 1 & 1 & -1\\
2 & 0 & -14 & 15\\
\end{tabular}
\end{center}
We have found that \(u=-14, v=15\), meaning \begin{equation} 1 = 31(-14)+29(15) \label{eq:2.2} \end{equation}
Solve for \(t\) and \(s\) by multiplying Eq. \eqref{eq:2.1} by 14 and setting it equal to \eqref{eq:2.2}.
\begin{align*}
14 \cdot 31 \cdot -14 + 14 \cdot 29 \cdot 15 &= 31 t - 29 s \\
31 (14 \cdot -14) + 29(14 \cdot 15) &= 31 t - 29 s \\
t &= 14 \cdot -14 \\
s &= 14 \cdot -15
\end{align*}
Finally, substitute for \(t\) in \(x = 31 t + 1\) and simplify
\[ x = 31 \cdot 14 \cdot -14 + 1 \]
\begin{align*}
14 \cdot -14 &= -(10 + 4)(10 + 4) \\
&= -(100 + 40 + 40 + 16) \\
&= -196 \\
31 \cdot -196 &= -(30 + 1)(100 + 40 + 40 + 16) \\
&= -(3000 + 1200 + 1200 + 480 + 100 + 40 + 40 + 16) \\
&= -(5400 + 580 + 80 + 16) \\
&= -(5980 + 96) \\
&= -6076 \\
-6076 + 1 &= -6075 = x \\
\end{align*}
Now we need to find \(x \bmod 899\). First we find \(6075 \bmod 899\). 899 is almost 1000, so we expect it would fit in 6075
around 6 times.
\begin{align*}
899 \cdot 6 &= 5394
\shortintertext{Another 899 puts us past 6075, so 6 is the proper factor. Now find the remainder.}
6075 - 5394 &= 681
\end{align*}
This implies that \(-6075 \equiv -681 \mod 899\), or \(-6075 \equiv 899-681 \mod 899\). The final answer is then
\[ x \equiv 218 \mod 899 \]
\end{answeri}
\section*{3}
\label{sec:orgb55e070}
Use the Principle of Inclusion-Exclusion to show that \(\varphi(n) = n \prod_{p \divides n} \left(1 - \frac{1}{p}\right)\).
\begin{answer}
\end{answer}
\section*{4}
\label{sec:org150829a}
Prove Euler's Theorem, which generalizes Fermat's Little Theorem to nonprime moduli: If \(n \perp m, n,m \in \ZZ^{+}\),
then \(n^{\varphi(m)} \equiv 1 \mod{m}\).
\begin{answer}
\end{answer}
\end{document}