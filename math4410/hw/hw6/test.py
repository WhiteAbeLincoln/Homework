def gcd(a, b):
    """solves bezouts identity gcd(a, b) = a*u + b*v, returning (gcd, u, v)"""
    u0, u1 = 0, 1
    v0, v1 = 1, 0
    print("||{}|{}|{}|".format(b,u0,v0))
    print("||{}|{}|{}|".format(a,u1,v1))
    while a != 0:
        (q, a), b = divmod(b, a), a
        v0, v1 = v1, v0 - q*v1
        u0, u1 = u1, u0 - q*u1
        print("|{}|{}|{}|{}|".format(q,a,u0,v0))
    return (b,u0,v0)

print("|$q$|$r$|$u$|$v$|")
print("|---+---+---+---|")
_, u, v = gcd(31, 29)
print("We have found that $u={}, v={}$".format(u, v))

