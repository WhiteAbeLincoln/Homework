const euclid = n => {
  if (n < 0) return 'INVALID'
  if (n === 0) return 1
  let prod = 1;
  for (let i = 0; i < n; ++i) {
    prod = prod * euclid(i)
  }
  return prod + 1
}

const main = argv => {
  const [,,args] = argv

  if (args.length != 1) {
    console.error('Invalid args')
    process.exit(-1)
  }

  const n = parseInt(args[0], 10)
  const v = euclid(n)
  console.log(v)
}

main(process.argv)
