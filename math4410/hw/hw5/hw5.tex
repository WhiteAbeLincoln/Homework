% Created 2020-04-03 Fri 17:01
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../../preamble.tex}
\author{Abraham White}
\date{\today}
\title{Homework 5}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Homework 5},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle

\section*{1}
\label{sec:orga6e358b}
Story: I took a class on Ancient Greek Philosophy in college, and in the midst of one lecture the professor (extemporaneously)
walked the class through Euclid's proof that \(\mathcal{P}\) is infinite. Kudos to the prof for this, since their speciality had
nothing to do with Math. But they said "\ldots{} the proof also gives way to construct primes". I didn't bother to correct this,
because I'm not one of those students who does that.

Define the \textbf{Euclid number} (actually a variant of Sylvester's Sequence, see \url{https://oeis.org/A129871}) \(e_n\) recursively
(inspired by the proof of Theorem 13.4) as follows:
\[\begin{cases}
  e_n = e_0 e_1 e_2 \cdots e_{n-1} + 1 & \text{for $n \geq 1$} \\
  e_0 = 1
  \end{cases}
  \]
\subsection*{a}
\label{sec:org20b1517}
Are the Euclid numbers prime?
\begin{answer}
No. At first glance it seems that the Euclid numbers are prime, since the first four numbers (excluding \(e_0\)) are prime. 
However, the fifth number in the sequence \(e_5 = 1807\) can be factored to \(13 \cdot 139\).
\end{answer}
\subsection*{b}
\label{sec:org5d1d1c6}
Show that \(e_m \perp e_n\) for \(m \neq n\)
\begin{answer}
Recall the division algorithm: 
\begin{theorem*}{13.2}
Suppose $m,n \in \ZZ$ with $n \neq 0$; then there exist unique integers $q$ and $r$ such that $m = nq + r$,
where $0 \leq r < |n|$
\end{theorem*}

Now if we have an Euclid number \(e_m\) and \(e_n\), where \(m < n\), we know that \(e_n\) will be of the form
\[ e_0 \cdots e_m e_{m+1}\cdots e_{n-1} + 1 \] We can apply the division algorithm, saying that \(e_n = e_m q + r\),
where \(q = e_0 \cdots e_{m+1}\cdots e_{n-1}\) and \(r = 1\). Thus, \(e_m\) divides \(e_n\) with remainder 1, or
\(e_n \equiv 1 \pmod{e_m}\).

Next, remember the Euclidean algorithm for calculating the greatest common denominator:
\begin{theorem*}{13.3}
For integers $a$ and $b$, satisfying $0 \leq a < b$, $\gcd(b, a) = \gcd(b \bmod a, a)$
\end{theorem*}
Applying this to \(e_n\) and \(e_m\), we have \(\gcd(e_n, e_m) = \gcd(e_n \bmod e_m, e_m)\), and since \(e_n \equiv 1 \pmod{e_m}\),
this is equivalent to \(\gcd(1, e_m)\). Because the only common denominator between 1 and a number is 1, \(\gcd(e_n, e_m) = 1\) and 
so \(e_m\) and \(e_n\) are relatively prime. The same argument applies if \(n < m\).
\end{answer}
\section*{2}
\label{sec:org723bc89}
A \textbf{Mersenne number} is a number of the form \(2^p - 1\), where \(p \in \mathcal{P}\). The Mersenne numbers that are prime occur for
certain primes, but not all. As of 1998, the largest prime yielding a \emph{Mersenne prime} was 2976221; now the largest Mersenne
prime occurs with \(p = 43112609\) (cf \url{https://oeis.org/A000043}, OEIS A000043). Please prove that \(2^K - 1 \not\in \mathcal{P}\) if
\(K\) is composite (not prime).
\begin{answer}
Suppose we have a composite \(K\). This means that we can represent \(K\) as the product of two numbers \(a\) and \(b\), \(K = ab\).
Now we can construct a number of the form \(2^K - 1\):
\begin{align*}
2^K - 1 &= 2^{ab} - 1 \\
        &= (2^a)^b - (1^a)^b
\end{align*}
Let \(n = 2^a - 1\). We can then say that \(2^a \equiv 1 \pmod{n}\). Recall from Proposition 14.2 that exponentiation preserves
congruence in modular arithmetic, i.e. if \(a \equiv b \pmod{m}\) then \(a^n \equiv b^n \pmod{m}\). If we raise both sides of
\(2^a \equiv 1 \pmod{n}\) to the power of \(b\), we get \(2^{ab} \equiv 1^b \pmod{n}\). From this we can conclude that
\(n = 2^a - 1\) divides \(2^{ab} - 1\).

Alternatively, there is a special case for factoring polynomials of the form \(x^n - y^n\). These polynomials can be factored
to \((x - y)(x^{n-1}y^0 + x^{n-2}y^1 + \cdots + x^{1}y^{n-2} + x^{0}y^{n-1})\). Our number is in the form \((2^a)^b - (1^a)^b\),
so we can factor it as \((2^a - 1^a)((2^a)^{b-1}\cdot 1^0 + (2^a)^{b-2}\cdot 1^1 + \cdots + (2^a)^{1}\cdot 1^{b-2} + (2^a)^{0}\cdot 1^{b-1})\).
Thus, \(2^a - 1\) divides it and so \(2^K - 1\) is not prime if \(K\) is composite.
\end{answer}
\section*{3}
\label{sec:org832c577}
Recall that \(\epsilon_p(n!) = \max\{ m : p^m \divides n! \}\), where \(p \in \mathcal{P}\) (Legendre's formula).
\subsection*{a}
\label{sec:org06d34c1}
Please prove \(\epsilon_p(n!) = \sum_{k\geq 1} \left\lfloor \frac{n}{p^k} \right\rfloor\).
\begin{answeri}
Recall that \(\left \lfloor \frac{N}{q} \right \rfloor\) counts the number of positive integers less than or equal to \(N\) that are
multiples of \(q\). This proof is not given in the notes, but follows from the division algorithm:

\begin{proof}
By the divison algorithm, given integers $m,n \in \ZZ$ with $n \neq 0$
\begin{equation} \exists q,r \in \ZZ, \text{ s.t. } m = nq + r, \text{ where } 0 \leq r < |n| \label{eq:div} \end{equation}
Since $r$ must be less than $n$, the largest possible multiple of $n$ less than $m$ is $nq$. We divide
both sides of \eqref{eq:div} by $n$:
\begin{equation} \frac{m}{n} = q + \frac{r}{n}, \text{ where } 0 \leq \frac{r}{n} < 1 \label{eq:div1} \end{equation} We can list
all of the multiples of $n$ up to $m$ as $n,2n,3n,\cdots,qn$, so it seems that $q$ counts the number of multiples of $n$ less than $m$.
We can get an equality for $q$ by taking the floor of \eqref{eq:div1}, since $0 \leq \frac{r}{n} < 1$:
\[ \left \lfloor \frac{m}{n} \right \rfloor = \left \lfloor q + \frac{r}{n} \right \rfloor = q \]
Thus $q = \left \lfloor \frac{m}{n} \right \rfloor$ counts the number of multiples of $n$ up to $m$.
\end{proof}

The factorial of \(n\) is the product of the numbers from 1 to \(n\), so \(n! = 1 \times 2 \times \ldots \times (n-1) \times n\).
The Fundamental Theorem of Arithmetic states that every composite number can be factored into a product of primes, i.e.
\[ n = p_1^{e_1} p_2^{e_2} \cdots p_r^{e_r} \] Therefore, any number \(0 < m \leq n\) that occurs in the factorial may
be divided by \(p\), and could possibly be divided multiple times by \(p\), i.e \(m\) is divisible by \(p^{e\geq 1}\).

It is a little hard to see how summing the the number of multiples of powers of \(p\) up to \(n\) helps us find the largest power
that divides \(n!\). It may help to write out the process step by step and then reason backwards to prove why it works.

First, suppose we have \(p = 3\) and \(n = 14\). We want to find \(\epsilon_3(14!)\). Write out 14! and highlight all of the multiples
of \(p\).
\[ 1 \cdot 2 \cdot (3) \cdot 4 \cdot 5 \cdot (6) \cdot 7 \cdot 8 \cdot (9) \cdot 10 \cdot 11 \cdot (12) \cdot 13 \cdot 14 \]
It appears that there are 4 multiples of \(p\) in 14!. However, that is not the number of times \(p\) occurs as a factor in 14!.
Multiples of \(p^2\) only got counted once.
\[ 1 \cdot 2 \cdot 3 \cdot 4 \cdot 5 \cdot 6 \cdot 7 \cdot 8 \cdot (9) \cdot 10 \cdot 11 \cdot 12 \cdot 13 \cdot 14 \]
From this we can reason that \(p\) occurs 5 times in \(14!\), or the largest power of \(p\) that divides \(14!\) is 5.

Now let us group these multiples and factor them into primes, so that we can directly count the number of times \(p\) occurs, or in
other words, the largest \(m\) such that \(p^m \divides n!\).
\begin{align*}
1 \cdot 2 \cdot (3) \cdot 4 \cdot 5 \cdot (6) \cdot 7 \cdot 8 \cdot (9) \cdot 10 \cdot 11 \cdot (12) \cdot 13 \cdot 14 \\
(3 \cdot 6 \cdot 9 \cdot 12) \cdot 1 \cdot 2 \cdot 4 \cdot 5 \cdot 7 \cdot 8 \cdot 10 \cdot 11 \cdot 13 \cdot 14 \\
(3 \cdot (3 \cdot 2) \cdot (3 \cdot 3) \cdot (3 \cdot 2 \cdot 2)) \cdot 1 \cdot 2 \cdot 4 \cdot 5 \cdot 7 \cdot 8 \cdot 10 \cdot 11 \cdot 13 \cdot 14 \\
(3 \cdot 3 \cdot 3 \cdot 3 \cdot 3 \cdot (2 \cdot 2 \cdot 2)) \cdot 1 \cdot 2 \cdot 4 \cdot 5 \cdot 7 \cdot 8 \cdot 10 \cdot 11 \cdot 13 \cdot 14
\end{align*}
We can easily see that the multiplicity of 3 in 14! is 5, or that 3\textsuperscript{5} divides 14!.

Returning to the formula, we see that for the first step of the summation, we count all members of the factorial that are divided by
\(p\). The first term is \(\epsilon_3(14!) = \left \lfloor \frac{14}{3^1} \right \rfloor + \cdots\). However, we omitted
members that may be divided multiple times by \(p\), i.e. are divided by \(p^2\). We can rectify this by incrementing \(k\) and adding another
term: \(\epsilon_3(14!) = \left \lfloor \frac{14}{3^1} \right \rfloor + \left \lfloor \frac{14}{3^2} \right \rfloor + \cdots\). But we may have
missed members that are divided by \(p^3\). Let us add another term:
\(\epsilon_3(14!) = \left \lfloor \frac{14}{3^1} \right \rfloor + \left \lfloor \frac{14}{3^2} \right \rfloor + \left \lfloor \frac{14}{3^3} \right \rfloor\).
It turns out that since 3\textsuperscript{3} is larger than 14, \(\left \lfloor \frac{14}{3^3} \right \rfloor = 0\), and all subsequent terms will also be 0.
Therefore we have counted all the times 3 occurs in 14!. Multiplying all of those occurences together gets us \(3^5\), which shows that 5 is
the largest power of \(p\) that divides 14!. \\

We can continue this same argument to more formally prove that the equation works. In order to count an integer multiple in \(n!\) which is
divisible by \(p^j\), we must count exactly \(j\) times, because \(p^j\) is simply \(p\) multiplied \(j\) times. Each term of
\(\left \lfloor \frac{n}{p^k} \right \rfloor\) only counts \(p\) once for each integer less than \(m\) that it divides. This is why we have to
include terms where \(k > 1\). Summing these terms counts the number of times \(p\) is a factor in \(n!\), which is also the largest power \(m\) 
such that \(p^m\) divides \(n!\).
\end{answeri}
\subsection*{b}
\label{sec:orgadf6f44}
Please prove the approximation and upper bound \(\epsilon_p(n!) < \frac{n}{p-1}\).
\begin{answer}
We know that \(\epsilon_p(n!) = \sum_{k\geq 1} \left \lfloor \frac{n}{p^k} \right \rfloor\). By the definition of the floor
function \(\left \lfloor x \right \rfloor \leq x\) for some \(x\), so we can reason that
\[ \sum_{k\geq 1} \left \lfloor \frac{n}{p^k} \right \rfloor \leq \sum_{k\geq 1} \frac{n}{p^k} \]
We must massage \(\sum_{k\geq 1} \frac{n}{p^k}\) until it becomes \(\frac{n}{p - 1}\).
\begin{align*}
\sum_{k\geq 1} \frac{n}{p^k} &= n \sum_{k\geq 1} \frac{1}{p^k} \\
                             &= n \sum_{k\geq 1} \left( \frac{1}{p} \right)^k
                             \shortintertext{This is a geometric series with $r = \frac{1}{p}$, and so converges}
                             &= \frac{n}{1 - \frac{1}{p}}
                             \shortintertext{Multiply by $p$, since $p$ is nonzero}
                             &= \frac{n}{p - 1}
\end{align*}
Since \(p^k\) cannot evenly divide \(n\) for all \(k \geq 1\), there must be a fractional term in the sum \(\sum_{k\geq 1} \frac{n}{p^k}\).
This means that \(\epsilon_p(n!)\) is strictly less than \(\sum_{k\geq 1} \frac{n}{p^k}\) and cannot be equal.
Consequently, we have found that \(\epsilon_p(n!) < \frac{n}{p - 1}\).
\end{answer}
\section*{4}
\label{sec:orgf3ba97b}
Show that if \(n^{(m-1)} \equiv 1 \mod m\) and \(n^{(m-1)/p} \not\equiv 1 \mod m\), for all primes \(p\) such that \(p \divides (m - 1)\), then
\(m\) is prime.
\begin{answer}
We begin with a claim: \(n^{m-1} \equiv 1 \pmod{m}\) implies that \(n \perp m\).
\begin{proof}
Since we have $n^{m-1} \equiv 1 \pmod{m}$, we can state that $n^{m-1} - 1 = mq$, where $q$ is an integer.
Rearranging this gives us $1 = n^{m-1} + m(-q)$. Pull out an $n$ from $n^{m-1}$:
\[ 1 = n(n^{m-2}) + m(-q) \]
By Bézout's Identity, we know that $n \perp m$ since we can express 1 as a linear combination of $n$ and $m$.
\end{proof}
Now we consider the numbers \(n \bmod m, n^2 \bmod m, \ldots, n^{m-1} \bmod m\). We want to show that these numbers are distinct.
This is because by Theorem 14.6, if these numbers are distinct then the numbers are \(1, 2, \ldots, m - 1\). We will do this by
way of contradiction.
\begin{proof}
Suppose $n \bmod m, n^2 \bmod m, \ldots, n^{m-1} \bmod m$ are not distinct. In other words, 
\begin{align*}
  n^k & \equiv n^j \pmod m \text{ for some } j, k \in \ZZ \text{ s.t. } 1 \leq j < k \leq m - 1
  \shortintertext{By Proposition 14.2, we can multiply the equivalence by $n^{-j}$}
  n^{k - j} & \equiv 1 \pmod m
\end{align*}
Therefore there exists some $k'$ such that $n^{k'} \equiv 1 \pmod m$. Choose $k'$ to be the smallest such number,
i.e. $1 \leq k' < m-1$. We claim that $k' \divides m - 1$.
\begin{subproof}[Subproof]
Suppose $k' \notdivides m - 1$. Then $\forall q \in \ZZ, m-1 \neq k'q$. By the division algorithm,
$m - 1 = k'q + r,~0 < r < k'$. We know that $n^{m-1} \equiv 1 \pmod{m}$.
\begin{align*}
n^{m-1} &\equiv 1 \pmod{m} \\
1 &\equiv n^{k'q + r} && \text{Substitute our division algorithm equality for $m - 1$.} \\
&\equiv n^{k'q}n^r \\
&\equiv \left (n^{k'} \right)^q n^r \\
&\equiv \left (1 \right)^q n^r && \text{Apply the $n^{k'} \equiv 1 \pmod{m}$ equivalence.} \\
&\equiv n^r
\end{align*}
So $n^r \equiv 1 \pmod{m}$, but we picked $k'$ to be the smallest number that is equivalent to $1 \pmod{m}$. The inequality
in the division algorithm states that $r < k'$, so this contradicts our choice of $k'$. Therefore, we conclude that
$k' \divides m - 1$.
\end{subproof}
Since we have concluded that $k' \divides m - 1$, we know that there exists some $q' \in \ZZ$ such that $m - 1 = k'q'$.
Let us pick a prime $p$ that divides $m-1$, such that $\frac{m-1}{p'} = k'q$ for some $q \in \ZZ$.
\begin{align*}
n^{(m-1)/p} &\equiv n^{k'q'} \\
            &\equiv \left (n^{k'} \right )^{q'} \\
            &\equiv (1)^{q'} \\
            &\equiv 1
\end{align*}
This contradicts the assumption made in the problem statement, that $n^{(m-1)/p} \not\equiv 1 \pmod{m}$, for all primes
$p$ such that $p \divides (m - 1)$. Therefore we can conclude that the numbers $n \bmod m, n^2 \bmod m, \ldots, n^{m-1} \bmod m$
are distinct and so by Theorem 14.6 are equal to 1 through $m - 1$.
\end{proof}

Since \(n \perp m\), we know that \(n^2 \perp m, n^3 \perp m, \ldots, n^{m-1} \perp m\). Using the relationship between the numbers
\(1, 2, \ldots, m - 1\) and \(n \bmod m, n^2 \bmod m, \ldots, n^{m-1} \bmod m\), we can then state that the numbers 1 through \(m-1\)
are relatively prime to \(m\). The only way for this to be possible is if \(m\) itself is prime.
\end{answer}
\end{document}