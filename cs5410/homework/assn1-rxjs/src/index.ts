import './styles.css';
import 'rxjs/add/observable/defer';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
import { Scheduler, Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { equals } from 'ramda';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

interface Event {
  name: string;
  interval: number;
  count: number;
  activeFor: number;
  render?: boolean;
  startTime: number;
}

interface State {
  events: Event[];
}

interface Clock {
  previousTime: number;
  elapsedTime: number;
  fps: string;
}

/**
 * Returns an Observable that emits clock objects on the animationFrame scheduler
 */
const clockStream = () => {
  const state: Clock = {
    previousTime: performance.now(),
    elapsedTime: 0,
    fps: ''
  };

  return Observable
    .interval(16, Scheduler.animationFrame)
    .scan<number, Clock>(previous => {
      const currentTime = performance.now();
      const elapsedTime = currentTime - previous.previousTime;
      const fps = (1/(elapsedTime/1000)).toFixed(2);
      return {
        ...previous,
        previousTime: currentTime,
        elapsedTime,
        fps
      };
    }, state);
};

/**
 * Returns an Observable that emits new Event objects when the Add Event button is clicked
 */
const inputStream = () => {
  const state: Event = {
    name: '',
    interval: 0,
    count: 0,
    activeFor: 0,
    startTime: 0,
  };

  const addBtn = document.querySelector('#btnAdd') as HTMLButtonElement;

  return Observable
    .fromEvent(addBtn, 'click')
    .map(() => {
      return {
        name: (document.querySelector('#eventName') as HTMLInputElement).value,
        interval: parseInt((document.querySelector('#eventInterval') as HTMLInputElement).value || '0', 10),
        count: parseInt((document.querySelector('#eventCount') as HTMLInputElement).value || '0', 10),
        activeFor: 0,
        startTime: performance.now(),
      }
    }, state).distinctUntilChanged();
};

/**
 * Merges the event and clock observables, emitting on the clock Observable. If the event Observable doesn't have a new value it emits [Clock, null]
 * @param inputStream The Event Stream
 * @param clockStream The Clock Stream
 * @returns Observable of [Clock, Event | null]
 */
const gameEventStream = (inputStream: Observable<Event>, clockStream: Observable<Clock>) => {
  const subject = new Subject<[Clock, Event | null]>();
  let lastVal: Event | null = null;

  inputStream.subscribe({
    next: event => {
      if (lastVal === null) {
        lastVal = event;
      }
    },
    error: subject.error,
    complete: subject.complete
  });

  clockStream.subscribe({
    next: c => {
      subject.next([c, lastVal]);
      lastVal = null;
    },
    error: subject.error,
    complete: subject.complete
  });

  return subject.asObservable();
}

const gameFactory = () => {
  const initialState: State = {
    events: []
  };

  const clock = clockStream();
  const input = inputStream();

  const gameEvents = gameEventStream(input, clock);

  const eventUpdater = gameEvents.map(([clock, input]) => (state: State) => {
    const newInput: Event[] = input ? [input] : [];
    return {
      ...state,
      events: [...state.events, ...newInput].map(e => {
        const newEvent = { ...e };
        newEvent.activeFor = newEvent.activeFor + clock.elapsedTime;

        if (newEvent.render) {
          newEvent.render = !newEvent.render;
        }

        if (newEvent.activeFor >= newEvent.interval) {
          newEvent.activeFor = 0;
          newEvent.count = newEvent.count - 1;
          newEvent.render = true;
        }

        return newEvent;
      }).filter(e => e.count > 0)
    }
  });

  const state = Observable
    .merge(eventUpdater)
    .startWith<any>(initialState)
    .scan<(s:State) => State, State>((state, reducer) => reducer(state));
  
  return state;
}

const render = (state: State) => {
  const canvas = document.getElementById('left')!;
  const detail = document.getElementById('json')!;
  const eventStrings = state.events.filter(e => e.render).map(e => `<p class="event">EVENT: ${e.name} (${e.count} remaining)</p>`)
  const height = canvas.offsetHeight;

  canvas.innerHTML += eventStrings.join(' ');
  canvas.scrollTop = canvas.scrollHeight;
  // we know each paragraph takes up 20px (we set it in the css)
  // if the number of paragraph tags exceeds those that will fit in the 
  // container height, remove the extras to increase performance
  const numparagraphs = Math.floor(height / 20);
  while (canvas.childElementCount > numparagraphs) {
    canvas.removeChild(canvas.firstChild as any);
  }

  detail.innerHTML = JSON.stringify(state, null, 4);
  return state;
}

const main = () => {
  
  gameFactory().subscribe(state => {
    render(state);
  });
}

main();