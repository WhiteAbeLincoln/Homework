/**
 * The Event Object
 * @typedef {Object} Event
 * @property {string} name
 * @property {number} interval
 * @property {number} count
 * @property {number} activeFor
 * @property {?boolean} render
 */

/**
 * The state object
 * @typedef {Object} State
 * @property {number} previousTime
 * @property {Array.<Event>} events
 * @property {?number} elapsedTime
 */

let newEvents = [];

/**
 * Renders the state to the DOM
 * @param {State} state 
 * @returns {State} An unchanged state object
 */
const render = state => {
  const canvas = document.getElementById('left');
  const fps = document.getElementById('fps');
  const detail = document.getElementById('json');
  const eventStrings = state.events.filter(e => e.render).map(e => `<p class="event">EVENT: ${e.name} (${e.count} remaining)</p>`)
  const height = canvas.offsetHeight;

  canvas.innerHTML += eventStrings.join(' ');
  canvas.scrollTop = canvas.scrollHeight;
  // we know each paragraph takes up 20px (we set it in the css)
  // if the number of paragraph tags exceeds those that will fit in the 
  // container height, remove the extras to increase performance
  const numparagraphs = Math.floor(height / 20);
  while (canvas.childElementCount > numparagraphs) {
    canvas.removeChild(canvas.firstChild);
  }

  fps.innerHTML = state.fps;
  detail.innerHTML = JSON.stringify(state, null, 4);
  return state;
}

// This may not be the most performant code, especially with all the Object.assigns
// It's just an experiment with a more functional style game loop
/**
 * Updates the state object
 * @param {State} state the previous state
 * @returns {State} the state object
 */
const update = state => {
  return Object.assign(state, {
    // First, increment the amount of time the event has been active
    events: state.events.map(e => Object.assign(e, {
      activeFor: e.activeFor + state.elapsedTime
    })).map(e => {
      let newEvent = e;
      // if we rendered last round, reset
      if (newEvent.render) {
        newEvent = Object.assign(newEvent, {
          render: !newEvent.render
        });
      }

      // if the activeTime is greater than interval, we want to render
      if (newEvent.activeFor >= newEvent.interval) {
        newEvent = Object.assign(newEvent, {
          activeFor: 0,
          count: newEvent.count - 1,
          render: true
        });
      }

      return newEvent;
    })
    // filter events that have exceeded their count
    .filter(e => e.count > 0)
  });
}

/**
 * Adds new events to the state object 
 * @param {State} state the previous state
 * @returns {State} the state object
 */
const processInput = state => {
  const newState = Object.assign(state, {
    events: [...state.events, ...newEvents]
  });
  newEvents = [];
  return newState;
}

/**
 * Runs the main game loop
 * @param {State} initialState the initial state 
 */
const gameLoop = (initialState) => (tFrame) => {
  const currentTime = performance.now();
  const elapsedTime = currentTime - initialState.previousTime;
  const fps = (1/(elapsedTime/1000)).toFixed(2);
  const state = Object.assign(initialState, {
    previousTime: currentTime,
    elapsedTime, // add elapsed time to the state object
    fps
  });

  // elapsed time is passed to update as part of the state object
  const newState = render(
    update(
      processInput(state)
    )
  );

  requestAnimationFrame(gameLoop(newState));
}

document.getElementById('btnAdd').addEventListener('click', () => {
  const name = document.getElementById('eventName').value;
  const interval = parseInt(document.getElementById('eventInterval').value, 10) || 0;
  const count = parseInt(document.getElementById('eventCount').value, 10) || 0;
  newEvents.push({
    name,
    interval,
    count,
    activeFor: 0,
  });
});

gameLoop({
  previousTime: performance.now(),
  events: []
})(performance.now());