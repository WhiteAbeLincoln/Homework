import { AABB, Circle, Plane, aabbCirc, closestPointOnBounds, Point } from './Collision';
import { GameObject } from './GameObject';
import { collidingWithBrick, Ball } from './Ball';
import { magnitude } from './util';

export type BrickColor = 'yellow' | 'orange' | 'blue' | 'green';

export interface Brick extends GameObject, AABB {
  points: number;
  color: BrickColor;
  // We don't want to remove a brick when broken, otherwise that would
  // change our row calculations. just mark it as broken
  broken: boolean;
}

// Brick area is upper half of the gameplay area starting a quarter of the way down
// i.e. 2nd quarter of the gameplay area
/*
0,0-----------------+
|                   |
|                   |
|-------------------|
|    Brick Area     |
|                   |
|-------------------|
|                   |
|               .   |
|  .                |
|                   |
|        ___        |
+-------------------+
*/
export const getBrickArea = ({height}: Plane) =>
  ({start: height / 6, end: height / 2.5});

export const breakBrick = (b: Brick) => ({
  ...b,
  broken: true,
});

export const getColorPoints = (color: BrickColor) => {
  switch (color) {
    case 'yellow': return 1;
    case 'orange': return 2;
    case 'blue': return 3;
    case 'green': return 5;
  }
};

// this isn't a pure function (we mutate the bs array)
// but we only use it in a single location where I guarantee it is passed
// a copy of the array
const breakBrickInArray = (bs: Brick[]) => (idx: number) => {
  bs.splice(idx, 1, {
    ...bs[idx],
    broken: true,
  });

  return bs;
};

export const breakBricks = (bs: Brick[]) => (idxs: number[]) =>
  idxs.reduce((bricks, idx) => breakBrickInArray(bricks)(idx), [...bs]);

export const colorIntervals = (bs: Brick[]) => {
  const map = {
    yellow: { start: 0, end: 0 },
    orange: { start: 0, end: 0 },
    blue: { start: 0, end: 0 },
    green: { start: 0, end: 0 },
  };

  if (bs.length !== 0) map[bs[0].color].start = 0;
    else return map;

  for (let i = 0; i < bs.length; i++) {
    // if the color changed
    if (bs[i - 1] && bs[i - 1].color !== bs[i].color) {
      map[bs[i - 1].color].end = i - 1;
      map[bs[i].color].start = i;
    }
  }

  map[bs[bs.length - 1].color].end = bs.length - 1;

  return map;
};

export const lineWasCleared = (collisions: number[]) => (bricks: Brick[]) => {
  const intervals = colorIntervals(bricks);
  const brokenBricks = breakBricks(bricks)(collisions);

  const collisionsInClearedLines: number[] = [];
  for (const c of collisions) {
    const brick = bricks[c];
    const interval = intervals[brick.color];
    const halfway = Math.floor((interval.end + interval.start) / 2);
    let allLeft = true;
    let allRight = true;

    for (let i = interval.start; i <= halfway; i++) {
      // if all the bricks in the interval are broken and the newly broken
      // brick was in the interval, we just broke a line
      allLeft = allLeft && brokenBricks[i].broken
        && c >= interval.start && c <= halfway;
    }

    for (let i = halfway + 1; i <= interval.end; i++) {
      allRight = allRight && brokenBricks[i].broken
        && c >= halfway + 1 && c <= interval.end;
    }

    // if allRight or allLeft, we broke a line
    if ((allRight || allLeft) && !collisionsInClearedLines.includes(c))
      collisionsInClearedLines.push(c);
  }

  return collisionsInClearedLines.length;
};

export const inBrickArea = (ga: Plane) => (b: Circle) => {
  const brickArea = getBrickArea(ga);

  return aabbCirc({
    type: 'AABB',
    pos: {
      type: 'Point',
      x: 0,
      y: brickArea.start,
    },
    width: ga.width,
    height: brickArea.end - brickArea.start,
  })(b);
};

const lineIntersects = (p1: Point, p2: Point, q1: Point, q2: Point): boolean => {
  // from https://stackoverflow.com/a/24392281
  const { x: r, y: s } = q2;
  const { x: p, y: q } = q1;
  const { x: c, y: d } = p2;
  const { x: a, y: b } = p1;
  // tslint:disable-next-line:one-variable-per-declaration
  let det, gamma, lambda;

  det = (c - a) * (s - q) - (r - p) * (d - b);
  if (det === 0) {
    return false;
  } else {
    lambda = ((s - q) * (r - a) + (p - r) * (s - b)) / det;
    gamma = ((b - d) * (r - a) + (c - a) * (s - b)) / det;
    return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
  }
};

export const getCollidingEdge = (br: Brick) => (b: Ball, dt: number) => {
  const prev = {
    ...b.pos,
    x: b.pos.x - b.velocity.x * (dt / 1000),
    y: b.pos.y - b.velocity.y * (dt / 1000),
  };

  let y = 1;
  let x = 1;

  if (b.velocity.y < 0) {
    // // traveling up test upper edge of ball with bottom of brick
    const brickStart = { ...br.pos, y: br.pos.y + br.height };
    const brickEnd = { ...brickStart, x: brickStart.x + br.width };
    const ball = {...b.pos, y: b.pos.y - b.radius };
    const prevBall = {...prev, y: prev.y - b.radius };
    if (lineIntersects(ball, prevBall, brickStart, brickEnd)) {
      y = -1;
    }
    //   y = -1;
    // }

  } else if (b.velocity.y > 0) {
    // traveling down test lower edge of ball with top of brick
    const ball = {...b.pos, y: b.pos.y + b.radius };
    const prevBall = {...prev, y: prev.y + b.radius };
    const brickStart = br.pos;
    const brickEnd = { ...br.pos, x: brickStart.x + br.width };
    // if (lineIntersect(b.pos, prev, brickStart, brickEnd)) {
    //   y = -1;
    // }
    if (lineIntersects(ball, prevBall, brickStart, brickEnd)) {
      y = -1;
    }

  }

  if (b.velocity.x < 0) {
    // travelling left test left edge of ball with right of brick
    const ball = {...b.pos, x: b.pos.x - b.radius };
    const prevBall = {...prev, y: prev.x - b.radius };
    const brickStart = { ...br.pos, x: br.pos.x + br.width };
    const brickEnd = { ...brickStart, y: brickStart.y + br.height };
    // if (lineIntersect(b.pos, prev, brickStart, brickEnd)) {
    //   x = -1;
    // }
    if (lineIntersects(ball, prevBall, brickStart, brickEnd)) {
      x = -1;
    }

  } else if (b.velocity.x > 0) {
    // travelling right test right edge of ball with left of brick
    const ball = {...b.pos, y: b.pos.y + b.radius };
    const prevBall = {...prev, y: prev.y + b.radius };
    const brickStart = { ...br.pos, x: br.pos.x + br.width };
    const brickEnd = { ...brickStart, y: brickStart.y + br.height };
    // if (lineIntersect(b.pos, prev, brickStart, brickEnd)) {
    //   x = -1;
    // }
    if (lineIntersects(ball, prevBall, brickStart, brickEnd)) {
      x = -1;
    }
  }

  return { type: 'Point', x, y };
};

export const getCollidingBricks = (bricks: Brick[]) => (b: Ball, dt: number) => {
  const collisions: Array<{ idx: number, push: Point }> = [];

  for (let i = 0; i < bricks.length; i++) {
    if (bricks[i].broken) continue;
    if (collidingWithBrick(b)(bricks[i])) {
      collisions.push({ idx: i, push: getCollidingEdge(bricks[i])(b, dt) as Point});
    }
  }

  return collisions;
};

export const newBrick = (x: number, y: number, width: number, height: number, color: BrickColor): Brick => ({
  type: 'AABB',
  width,
  height,
  points: getColorPoints(color),
  color,
  broken: false,
  pos: {
    type: 'Point',
    x,
    y,
  },
  velocity: { x: 0, y: 0},
  angle: 0,
});

const createBrickRow = (color: BrickColor, x: number, y: number, offset: number,
                        width: number, height: number, num: number) => {
  const bricks: Brick[] = [];
  for (let i = 0; i < num; i++) {
    bricks.push(newBrick(x, y, width, height, color));
    x += width + offset;
  }

  return bricks;
};

export const newBrickList = (start: {x: number, y: number}, width: number, height: number) => {
  const offset = 5;
  const numBricks = 14;
  let brickWidth = width / numBricks;
  const numOffsets = numBricks - 1;

  while (numBricks * brickWidth + offset * numOffsets > width) {
    brickWidth--;
  }

  const brickHeight = Math.floor(brickWidth / 2);

  const x = start.x + offset / 2;
  let y = start.y;
  let bricks: Brick[] = [];
  for (const color of ['green', 'blue', 'orange', 'yellow'] as BrickColor[]) {
    bricks = bricks.concat(createBrickRow(color, x, y, offset, brickWidth, brickHeight, numBricks));
    y += brickHeight + offset;
    bricks = bricks.concat(createBrickRow(color, x, y, offset, brickWidth, brickHeight, numBricks));
    y += brickHeight + offset;
  }
  return bricks;
};

export default Brick;
