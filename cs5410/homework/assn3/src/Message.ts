import { State, StateReducer } from './state';
import * as P from './Paddle';
import { clearKeyHandlers, registerKeyHandlers, handleEscape } from '.';

export type Action =  'LEFT'
                    | 'RIGHT'
                    | 'ESCAPE';

export type MessageKey = Action | 'TOGGLE_PAUSE' | 'RESIZE' | 'CONTROLS_CHANGED' | 'QUIT';

export interface Message {
  type: MessageKey;
  data?: any;
}

const paddleLeft = (s: State) => { s.paddle = P.moveLeft(s.paddle); return s; };
const paddleRight = (s: State) => { s.paddle = P.moveRight(s.paddle); return s; };

const reducerForMessage = (message: Message): StateReducer => {
  switch (message.type) {
    case 'LEFT': return paddleLeft;
    case 'RIGHT': return paddleRight;
    case 'TOGGLE_PAUSE': return s => { s.game.paused = !s.game.paused; return s; };
    case 'CONTROLS_CHANGED': return s => { clearKeyHandlers(); registerKeyHandlers(s); return s; };
    case 'QUIT': return s => { s.game.over = true; return s; };
    case 'ESCAPE': return handleEscape;
    default: return s => s; // identity function
  }
};

// Should we do something more here? like insert reducer functions
// instead of just the message? That would move some of the logic out of
// our update function
export const processInput = (messages: Message[]) => (state: State): State => {
  state.reducers = messages.map(reducerForMessage);
  return state;
};
