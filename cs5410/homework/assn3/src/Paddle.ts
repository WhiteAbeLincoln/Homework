import { AABB, Point, Plane, Circle } from './Collision';

export interface Paddle extends AABB {
  removedBricks: number;
  hasShrunk: boolean;
}

export const moveLeft = (p: Paddle): Paddle => ({
  ...p,
  pos: {
    type: 'Point',
    x: p.pos.x - 5,
    y: p.pos.y,
  },
});

export const moveRight = (p: Paddle): Paddle => ({
  ...p,
  pos: {
    type: 'Point',
    x: p.pos.x + 5,
    y: p.pos.y,
  },
});

export const shrink = (p: Paddle): Paddle => ({
  ...p,
  width: Math.floor(p.width / 2),
  hasShrunk: true,
});

export const addRemovedBricks = (p: Paddle) => (num: number) => ({
  ...p,
  removedBricks: p.removedBricks + num,
});

export const newPaddle = (width: number, height: number, x: number, y: number): Paddle => ({
  type: 'AABB',
  width,
  height,
  pos: {
    type: 'Point',
    x,
    y,
  },
  removedBricks: 0,
  hasShrunk: false,
});

export const baseSize = ({width}: Plane) => ({
  // the starting paddle width will be a quarter of the game area width
  width: Math.floor(width / 4),
  // the starting paddle height will be 1/4 the paddle width
  height: Math.floor(width / 16),
});

export const getXVelocity = (br: Paddle, b: Circle) => {
  const x = (b.pos.x - (br.pos.x + br.width / 2)) / (br.width / 2);
  return x;
};
