import { Point } from './Collision';
import { Vec2 } from './GJK';

export type Arity1<A, B> = (a: A) => B;

// tslint:disable:max-line-length
export function pipe<A, B>(f: Arity1<A, B>): Arity1<A, B>;
export function pipe<A, B, C>(f: Arity1<A, B>, g: Arity1<B, C>): Arity1<A, C>;
export function pipe<A, B, C, D>(f: Arity1<A, B>, g: Arity1<B, C>, h: Arity1<C, D>): Arity1<A, D>;
export function pipe<A, B, C, D, E>(f: Arity1<A, B>, g: Arity1<B, C>, h: Arity1<C, D>, i: Arity1<D, E>): Arity1<A, E>;
export function pipe<A, B, C, D, E, F>(f: Arity1<A, B>, g: Arity1<B, C>, h: Arity1<C, D>, i: Arity1<D, E>, j: Arity1<E, F>): Arity1<A, F>;
export function pipe<A, B, C, D, E, F, G>(f: Arity1<A, B>, g: Arity1<B, C>, h: Arity1<C, D>, i: Arity1<D, E>, j: Arity1<E, F>, k: Arity1<F, G>): Arity1<A, G>;
export function pipe<A, B, C, D, E, F, G, H>(f: Arity1<A, B>, g: Arity1<B, C>, h: Arity1<C, D>, i: Arity1<D, E>, j: Arity1<E, F>, k: Arity1<F, G>, l: Arity1<G, H>): Arity1<A, H>;
export function pipe<A, B, C, D, E, F, G, H, I>(f: Arity1<A, B>, g: Arity1<B, C>, h: Arity1<C, D>, i: Arity1<D, E>, j: Arity1<E, F>, k: Arity1<F, G>, l: Arity1<G, H>, m: Arity1<H, I>): Arity1<A, I>;
export function pipe<A, B, C, D, E, F, G, H, I, J>(f: Arity1<A, B>, g: Arity1<B, C>, h: Arity1<C, D>, i: Arity1<D, E>, j: Arity1<E, F>, k: Arity1<F, G>, l: Arity1<G, H>, m: Arity1<H, I>, n: Arity1<I, J>): Arity1<A, J>;
export function pipe<A, B, C, D, E, F, G, H, I, J, K>(f: Arity1<A, B>, g: Arity1<B, C>, h: Arity1<C, D>, i: Arity1<D, E>, j: Arity1<E, F>, k: Arity1<F, G>, l: Arity1<G, H>, m: Arity1<H, I>, n: Arity1<I, J>, o: Arity1<J, K>): Arity1<A, K>;
export function pipe<A, B>(...args: Array<Arity1<any, any>>): Arity1<A, B>;
export function pipe<A, B>(...args: Array<Arity1<any, any>>): Arity1<A, B> {
  return (init: A): B => args.reduce((s, f) => f(s), init);
}

export function compose<A, B>(f: Arity1<A, B>): Arity1<A, B>;
export function compose<A, B, C>(g: Arity1<B, C>, f: Arity1<A, B>): Arity1<A, C>;
export function compose<A, B, C, D>(h: Arity1<C, D>, g: Arity1<B, C>, f: Arity1<A, B>): Arity1<A, D>;
export function compose<A, B, C, D, E>(i: Arity1<D, E>, h: Arity1<C, D>, g: Arity1<B, C>, f: Arity1<A, B>): Arity1<A, E>;
export function compose<A, B, C, D, E, F>(j: Arity1<E, F>, i: Arity1<D, E>, h: Arity1<C, D>, g: Arity1<B, C>, f: Arity1<A, B>): Arity1<A, F>;
export function compose<A, B, C, D, E, F, G>(k: Arity1<F, G>, j: Arity1<E, F>, i: Arity1<D, E>, h: Arity1<C, D>, g: Arity1<B, C>, f: Arity1<A, B>): Arity1<A, G>;
export function compose<A, B, C, D, E, F, G, H>(l: Arity1<G, H>, k: Arity1<F, G>, j: Arity1<E, F>, i: Arity1<D, E>, h: Arity1<C, D>, g: Arity1<B, C>, f: Arity1<A, B>): Arity1<A, H>;
export function compose<A, B, C, D, E, F, G, H, I>(m: Arity1<H, I>, l: Arity1<G, H>, k: Arity1<F, G>, j: Arity1<E, F>, i: Arity1<D, E>, h: Arity1<C, D>, g: Arity1<B, C>, f: Arity1<A, B>): Arity1<A, I>;
export function compose<A, B, C, D, E, F, G, H, I, J>(n: Arity1<I, J>, m: Arity1<H, I>, l: Arity1<G, H>, k: Arity1<F, G>, j: Arity1<E, F>, i: Arity1<D, E>, h: Arity1<C, D>, g: Arity1<B, C>, f: Arity1<A, B>): Arity1<A, J>;
export function compose<A, B, C, D, E, F, G, H, I, J, K>(o: Arity1<J, K>, n: Arity1<I, J>, m: Arity1<H, I>, l: Arity1<G, H>, k: Arity1<F, G>, j: Arity1<E, F>, i: Arity1<D, E>, h: Arity1<C, D>, g: Arity1<B, C>, f: Arity1<A, B>): Arity1<A, K>;
export function compose<A, B>(...args: Array<Arity1<any, any>>): Arity1<A, B>;
export function compose<A, B>(...args: Array<Arity1<any, any>>): Arity1<A, B> {
  return (init: A): B => args.reduceRight((s, f) => f(s), init);
}

export const flip = <A, B, C>(f: Arity1<A, Arity1<B, C>>): Arity1<B, Arity1<A, C>> => (b: B) => (a: A) => f(a)(b);

export const getRandom = (min: number, max: number) => Math.random() * (max - min) + min;
export const getRandomInt = (min: number, max: number) => {
  min = Math.ceil(min);
  max = Math.ceil(max);
  return Math.floor(Math.random() * (max - min)) + min;
};
export const not = (b: boolean) => !b;
export const isEven = (n: number) => n % 2 === 0;
export const isOdd = compose(not, isEven);
export const flatten = <T>(a: T[][]) => a.reduce((a, c) => a.concat(c));
export const magnitude = (p: Point) => Math.sqrt(p.x * p.x + p.y * p.y);
