import { State, Area } from './state';
import { Point, AABB, Circle } from './Collision';
import { Player } from './Player';
import { Paddle, newPaddle } from './Paddle';
import { flatten } from './util';

// The browser probably already caches this, but I don't see any
// harm in being careful
const imageCache: Record<string, HTMLImageElement> = {};

/*
Particle animation
Particle:
  - Lifetime
  - Velocity
    - Speed, Direction
  - Acceleration
  - Curve/Path
  - Size
  - Spin/Rotation (spin about its central axis)
  - Color
  - Texture / alternately wireframe

Particle System
 - Update function on the system
 - createEffect(FIRE, ...) Adds a predefined effect to the system
 - render method? or ParticleSystemRenderer that takes a system and renders it
*/

const drawImage = (ctx: CanvasRenderingContext2D) =>
  (url: string, x: number, y: number, width: number, height: number) => {
  if (!(url in imageCache)) {
    const imageObj = new Image();
    imageCache[url] = imageObj;
    imageObj.src = url;
    imageObj.onload = () => ctx.drawImage(imageObj, x, y, width, height);
  } else {
    ctx.drawImage(imageCache[url], x, y, width, height);
  }
};

const drawArea = (ctx: CanvasRenderingContext2D, area: Area) => {
  const { color, width, height, image, children, pos: { x, y } } = area;
  if (color) {
    ctx.fillStyle = color;
    ctx.fillRect(x, y, width, height);
  }

  if (image) {
    drawImage(ctx)(image, x, y, width, height);
  }

  if (children) {
    children.forEach(a => drawArea(ctx, a));
  }
};

const drawRect = (ctx: CanvasRenderingContext2D, color: string, rect: AABB) => {
  const { width, height } = rect;
  const { x, y } = rect.pos;
  ctx.fillStyle = color;
  ctx.fillRect(x, y, width, height);
};

const drawCirc = (ctx: CanvasRenderingContext2D, color: string, circ: Circle) => {
  const { radius, pos: { x, y } } = circ;
  ctx.fillStyle = color;
  ctx.beginPath();
  ctx.arc(x, y, radius, 0, 2 * Math.PI);
  ctx.fill();
};

const drawStats = (ctx: CanvasRenderingContext2D, player: Player, area: Area) => {
  // draw paddles on left
  const { width, height } = area;
  const gap = 5;
  let offset = 0;
  const rectarea = Math.floor(width / 2);
  const rectWidth = Math.floor(rectarea / 3 - rectarea / (gap * 2));
  const rectHeight = Math.floor(rectWidth / 4);
  for (let i = player.lives; i > 0; i--) {
    drawRect(ctx, 'green',
      {
        type: 'AABB',
        width: rectWidth,
        height: rectHeight,
        pos: {
          type: 'Point',
          x: area.pos.x + offset,
          y: area.pos.y + height / 2 + height / 4,
        },
    });
    offset += rectWidth + gap;
  }
  // draw score on right
  ctx.font = '30px Arial';
  ctx.fillStyle = 'white';
  const score = `Score: ${player.score}`;
  const length = ctx.measureText(score);
  ctx.fillText(score, area.pos.x + width - length.width, area.pos.y + height / 2 + height / 4);
};

const findStatsAreas = (a: Area): Area[] => {
  const areas: Area[] = [];
  if (a.name === 'statsArea')
    areas.push(a);

  if (a.children) {
    return areas.concat(flatten(a.children.map(findStatsAreas)));
  }

  return areas;
};

const centeredText = (ctx: CanvasRenderingContext2D, text: string, area: Area) => {
  ctx.font = '60px Arial';
  ctx.fillStyle = 'white';
  const length = ctx.measureText(text);
  const halfwayX = area.pos.x + area.width / 2;
  const halfwayY = area.pos.y + area.height / 2;

  ctx.fillText(text, halfwayX - length.width / 2, halfwayY);
};

// TODO: Finish the render function
export const render = (state: State): State => {
  const { game: { rootArea }, context: ctx } = state;
  if (!ctx) return state;

  if (state.game.over) {
    centeredText(ctx, 'Game Over!', rootArea);

    return state;
  }

  ctx.clearRect(0, 0, rootArea.width, rootArea.height);

  // draw background
  drawArea(ctx, state.game.rootArea);
  // draw stats area
  // draw stats
  const statsAreas = findStatsAreas(rootArea);
  if (statsAreas.length !== 0) {
    statsAreas.forEach(a => drawStats(ctx, state.player, a));
  }

  // draw paddle
  drawRect(ctx, 'green', state.paddle);
  // draw bricks
  state.bricks.filter(b => !b.broken).forEach(b => drawRect(ctx, b.color, b));
  // draw particles
  state.particles.forEach(b => drawCirc(ctx, b.color, b as any));
  // draw balls
  state.balls.forEach(b => drawCirc(ctx, 'purple', b));

  if (state.countdown > 0) {
    centeredText(ctx, (state.countdown / 1000).toFixed(0), rootArea);
  }

  return state;
};
