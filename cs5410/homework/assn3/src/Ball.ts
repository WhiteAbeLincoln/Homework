import { Point, aabbCirc, Circle } from './Collision';
import { GameObject, Velocity } from './GameObject';
import { Brick } from './Brick';
import { Paddle } from './Paddle';
import { getRandom, getRandomInt, isEven } from './util';

export interface Ball extends GameObject, Circle {}

export const increaseSpeed = (ball: Ball): Ball => ({
  ...ball,
  velocity: {
    x: ball.velocity.x + Math.sign(ball.velocity.x) * 2.5,
    y: ball.velocity.y + Math.sign(ball.velocity.y) * 2.5,
  },
});

export const reflectVelocity = (ball: Ball): Ball => ({
  // TODO: figure velocity reflection
  ...ball,
});

export const startingVelocity = (): Velocity => ({
  // x in range [-2, -1), [1, 2)
  // x: isEven(getRandomInt(1, 3)) ? getRandom(-2, -1) : getRandom(1, 2),
  // y in range [-2, -1)
  // y: getRandom(-2, -1),
  x: isEven(getRandomInt(1, 3)) ? getRandom(-20, -15) : getRandom(30, 15),
  y: getRandom(-40, -25),
});

export const newBall = (x: number, y: number, radius: number): Ball => ({
  type: 'Circle',
  pos: {
    type: 'Point',
    x,
    y,
  },
  velocity: startingVelocity(),
  radius,
  angle: 0,
});

export const collidingWithBrick = (ball: Circle) => (brick: Brick) =>
  aabbCirc(brick)(ball);

export const collidingWithPaddle = (ball: Circle) => (paddle: Paddle) =>
  aabbCirc(paddle)(ball);

export default Ball;
