export interface Player {
  score: number;
  // paddles remaining
  lives: number;
  name: string;
}

export const updateScore = (s: number) => (p: Player): Player => ({
  ...p,
  score: p.score + s,
});

export const updateLives = (l: number) => (p: Player): Player => ({
  ...p,
  lives: p.lives + l,
});

export const newPlayer = (lives = 3): Player => ({
  score: 0,
  lives: 3,
  name: '',
});

export const removeLife = updateLives(-1);
