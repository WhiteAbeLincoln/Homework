// tslint:disable:max-classes-per-file

export class Vec2 {
  public static dot(v1: Vec2, v2: Vec2): number {
    return v1.x * v2.x + v1.y + v2.y;
  }

  constructor(public x: number, public y: number) { }

  public normalized() {
    // normally we would use fast inverse square root, but javascript doesn't support the floating point tricks used
    const mag = Math.sqrt(this.x * this.x + this.y * this.y);
    return this.smult(1 / mag);
  }

  public add(v: Vec2) {
    return new Vec2(this.x + v.x, this.y + v.y);
  }

  public smult(n: number) {
    return new Vec2(this.x * n, this.y * n);
  }

  public sub(v: Vec2) {
    return new Vec2(this.x - v.x, this.y - v.y);
  }

}

export class Edge {
  constructor(public distance: number, public normal: Vec2, public index: number) { }
}

interface Shape2D {
  support(dir: Vec2): Vec2;
}

class Point implements Shape2D {
  constructor(public x: number, public y: number) {}

  public support(dir: Vec2): Vec2 {
    throw new Error('Method not implemented.');
  }
}

class Circle implements Shape2D {
  constructor(public center: Vec2, public radius: number) { }

  public support(dir: Vec2): Vec2 {
    return this.center.add(dir.normalized().smult(this.radius));
  }
}

class Polygon implements Shape2D {
  constructor(public vertices: Vec2[]) { }

  public support(dir: Vec2): Vec2 {
    let furthest = Number.NEGATIVE_INFINITY;
    let furthestVertex: Vec2 | null = null;

    for (const v of this.vertices) {
      const dist = Vec2.dot(v, dir);
      if (dist > furthest) {
        furthest = dist;
        furthestVertex = v;
      }
    }

    return furthestVertex as Vec2;
  }
}

function gjk(a: Shape2D, b: Shape2D) {
  const vertices: Vec2[] = [];
  let direction: Vec2;

  function addSupport(a: Shape2D, b: Shape2D, dir: Vec2) {
    const newVertex = a.support(dir).sub(b.support(dir.smult(-1)));
    vertices.push(newVertex);
    return Vec2.dot(dir, newVertex) >= 0;
  }

  function evolveSimplex() {
    switch (vertices.length) {
      case 0: direction = new Vec2(1, 0);
      case 1: direction = direction.smult(-1);
      case 2: {
        const ab = vertices[1].sub(vertices[0]);
        const a0 = vertices[0].smult(-1);

        // direction = tripleProduct(ab, a0, ab);
      }
    }
  }
}
