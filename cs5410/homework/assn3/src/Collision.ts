import { Ball, collidingWithBrick } from './Ball';
import { getBrickArea, Brick } from './Brick';
import { Paddle } from './Paddle';
/*
Use a quad tree to deliniate regions for collision detection
if some area has more than 4 items, split the area into 4 sections
and recheck
https://gamedevelopment.tutsplus.com
  /tutorials/quick-tip-use-quadtrees-to-detect-likely-collisions-in-2d-space--gamedev-374
Collision between frames
Sweep method - create an object and sweep between the before and after points
Incremenent physics engine fast

Breakout collision detection
- Do exhaustive n^2 computation
- Only test once the ball passes into the brick region, then test every row to find which row region.
  Test every brick in the row region
*/

export interface Point {
  type: 'Point';
  x: number;
  y: number;
}

export interface AABB {
  type: 'AABB';
  pos: Point;
  width: number;
  height: number;
}

export interface Circle {
  type: 'Circle';
  pos: Point;
  radius: number;
}

export interface Plane {
  width: number;
  height: number;
}

export const isCircle = (o: Circle | Point | AABB): o is Circle =>
  o.type === 'Circle';
export const isRectangle = (o: Circle | Point | AABB): o is AABB =>
  o.type === 'AABB';
export const isPoint = (o: Circle | Point | AABB): o is Point =>
  o.type === 'Point';

export const pointAABB = ({x, y}: Point) => (rt: AABB) =>
     x < rt.pos.x + rt.width && x > rt.pos.x
  && y < rt.pos.y + rt.height && y > rt.pos.y;

export const pointCircle = ({x, y}: Point) => (c: Circle) => {
  const dx = c.pos.x - x;
  const dy = c.pos.y - y;
  return (dx * dx + dy * dy) < (c.radius * c.radius);
};
export const aabbCirc = (r: AABB) => (c: Circle): boolean => {
  const nearx = Math.max(r.pos.x, Math.min(c.pos.x, r.pos.x + r.width));
  const neary = Math.max(r.pos.y, Math.min(c.pos.y, r.pos.y + r.height));

  return pointCircle({ type: 'Point', x: nearx, y: neary })(c);
};
export const aabbAABB = (r1: AABB) => (r2: AABB): boolean =>
  encompassesOrigin(minkowskiDiffAABB(r1)(r2));

export const circCirc = (c1: Circle) => (c2: Circle): boolean => {
  const dx = c1.pos.x - c2.pos.x;
  const dy = c1.pos.y - c2.pos.y;
  const dist = Math.sqrt(dx * dx + dy * dy);
  return dist < c1.radius + c2.radius;
};

// TODO: make this generic (i.e. accepts AABB and any convex polygon)
export const minkowskiDiffAABB = (a: AABB) => (b: AABB): AABB => ({
  type: 'AABB',
  pos: {
    type: 'Point',
    // m_left = a_left - b_right
    x: a.pos.x - (b.pos.x + b.width),
    // m_top = a_top - b_bottom
    y: a.pos.y - (b.pos.y + b.height),
  },
  // m_width = a_width + b_width
  width: a.width + b.width,
  // m_height = a_height + b_height
  height: a.height + b.height,
});

// if the minkowski difference encompasses the origin, then there was a collision
const encompassesOrigin = (a: AABB): boolean =>
  a.pos.x <= 0 && a.pos.x + a.width >= 0 &&
  a.pos.y <= 0 && a.pos.y + a.height >= 0;

export const closestPointOnBounds = (bounds: AABB) => (point: Point) => {
  let minDist = Math.abs(point.x - bounds.pos.x);
  const boundsPoint: Point = { type: 'Point', x: bounds.pos.x, y: point.y };

  // Very simplistic - we just check each edge segment
  if (Math.abs(bounds.pos.x + bounds.width - point.x) < minDist) {
    minDist = Math.abs(bounds.pos.x + bounds.width - point.x);
    boundsPoint.x = bounds.pos.x + bounds.width;
    boundsPoint.y = point.y;
  }
  if (Math.abs(bounds.pos.y + bounds.height - point.y) < minDist) {
    minDist = Math.abs(bounds.pos.y + bounds.height - point.y);
    boundsPoint.x = point.x;
    boundsPoint.y = bounds.pos.y + bounds.height;
  }
  if (Math.abs(bounds.pos.y - point.y) < minDist) {
    minDist = Math.abs(bounds.pos.y - point.y);
    boundsPoint.x = point.x;
    boundsPoint.y = bounds.pos.y;
  }

  return boundsPoint;
};

export const atLeftWall = ({width}: Plane) => (b: Circle | AABB) => {
  if (isCircle(b)) {
    return b.pos.x - b.radius < 0;
  } else {
    return b.pos.x < 0;
  }
};

export const atRightWall = ({width}: Plane) => (b: Circle | AABB) => {
  if (isCircle(b)) {
    return b.pos.x + b.radius > width;
  } else {
    return b.pos.x + b.width > width;
  }
};

export const atTopWall = ({height}: Plane) => (b: Circle | AABB) => {
  if (isCircle(b)) {
    return b.pos.y - b.radius < 0;
  } else {
    return b.pos.y < 0;
  }
};

export const atBottomWall = ({height}: Plane) => (b: Circle | AABB) => {
  if (isCircle(b)) {
    return b.pos.y + b.radius > height;
  } else {
    return b.pos.y + b.height > height;
  }
};
