import { State, update, initialState } from './state';
import { Message, processInput } from './Message';
import { pipe } from './util';
import { render } from './render';

const initialKeydownFunc = (e: KeyboardEvent) => {
  if (e.defaultPrevented) return;
  if (e.key === 'Escape') {
    // escape handling when not started
    if (scores.classList.contains('show')) {
      // return to main menu
      toggleUIVisibility(scores);
      toggleUIVisibility(menu);
    }

    if (credits.classList.contains('show')) {
      // return to main menu
      toggleUIVisibility(credits);
      toggleUIVisibility(menu);
    }
  }

  e.preventDefault();
};

const messages: Message[] = [];
// function references for clearing the event handlers
let keydownFunc: (e: KeyboardEvent) => void;
let keyupFunc: (e: KeyboardEvent) => void;

const startBtn = document.getElementById('newBtn')!;
const scoresBtn = document.getElementById('scoresBtn')!;
const creditsBtn = document.getElementById('creditsBtn')!;
const quitBtn = document.getElementById('quitBtn')!;
const resumeBtn = document.getElementById('resumeBtn')!;
const resetScoreBtn = document.getElementById('resetScoreBtn')!;

const canvas = document.getElementById('canvas') as HTMLCanvasElement;
const menu = document.getElementById('menu')!;
const scores = document.getElementById('scores')!;
const credits = document.getElementById('credits')!;
const pause = document.getElementById('pause')!;

export const registerKeyHandlers = (state: State) => {
  const map: Record<string, boolean> = {};

  keydownFunc = (e: KeyboardEvent) => {
    if (e.defaultPrevented) return;
    map[e.key] = true;
    const pressedKeys = Object.keys(map).filter(k => map[k]);

    pressedKeys.forEach(k =>
      state.game.controls
              .filter(c => c.key === k)
              .forEach(c => messages.push({ type: c.action })));

    e.preventDefault();
  };

  keyupFunc = (e: KeyboardEvent) => {
    map[e.key] = false;
  };

  window.addEventListener('keydown', keydownFunc, true);
  window.addEventListener('keyup', keyupFunc, true);
};

export const clearKeyHandlers = () => {
  window.removeEventListener('keydown', keydownFunc, true);
  window.removeEventListener('keyup', keyupFunc, true);
};

const registerInitialKeyHandler = () => {
  clearKeyHandlers();
  keydownFunc = initialKeydownFunc;
  document.addEventListener('keydown', keydownFunc, true);
};

const updateTime = (time: number) => (state: State): State => {
  const currentTime = time;
  const elapsedTime = currentTime - state.previousTime;
  state.previousTime = currentTime;
  state.elapsedTime = elapsedTime;

  return state;
};

const getHighScores = (): Array<{name: string, score: number}> => {
  const hs = localStorage.getItem('highScores');
  let highScores: Array<{ name: string, score: number }> = [];

  if (hs) highScores = JSON.parse(hs);

  return highScores;
};

const newHighScore = (s: State) => {
  const scores =  getHighScores();
  return scores.some(sc => sc.score < s.player.score) || (scores.length < 5 && s.player.score !== 0);
};

const writeHighScore = (newScore: { name: string, score: number}) => {
  const current = getHighScores();
  current.push(newScore);
  current.sort((a, b) => a.score > b.score ? -1
                      : a.score < b.score ? 1
                      : 0);
  if (current.length > 5) {
    current.splice(5);
  }
  localStorage.setItem('highScores', JSON.stringify(current));
};

const gameLoop = (prevState: State) => (time: number) => {
  const nextState = pipe(
    updateTime(time),
    processInput(messages),
    // debugState,
    update,
    render,
  )(prevState);
  // reset our message queue without breaking references
  messages.length = 0;

  // register the next frame call
  if (!nextState.game.over) {
    requestAnimationFrame(gameLoop(nextState));
  } else {
    if (newHighScore(nextState)) {
      const text = prompt('Enter your name') || 'ANON';
      writeHighScore({ name: text, score: nextState.player.score });
    }
    toggleUIVisibility(canvas);
    toggleUIVisibility(menu);
    registerInitialKeyHandler();
  }

};

const startGame = () => {
  const init = initialState(500, 792);
  const ctx = canvas.getContext('2d');

  // register our event handlers
  clearKeyHandlers();
  registerKeyHandlers(init);
  // start the game
  init.context = ctx!;
  gameLoop(init)(performance.now());
};

const toggleUIVisibility = (m: HTMLElement) => {
  if (m.classList.contains('show')) {
    m.classList.remove('show');
    m.classList.add('hide');
  } else {
    m.classList.remove('hide');
    m.classList.add('show');
  }
};

const elementVisible = (m: HTMLElement) => m.classList.contains('hide');

export const handleEscape = (s: State) => {
  s.game.paused = !s.game.paused;
  toggleUIVisibility(pause);
  toggleUIVisibility(canvas);

  return s;
};

const loadScores = () => {
  const scoresTxt = document.getElementById('scoresTxt')!;

  // clear existing scores
  scoresTxt.innerHTML = '';
  for (const s of getHighScores()) {
    const cont = document.createElement('div');
    const n = document.createElement('p');
    const score = document.createElement('p');
    cont.classList.add('scoreRow');
    n.innerText = s.name;
    score.innerText = s.score.toString();
    cont.appendChild(n);
    cont.appendChild(score);
    scoresTxt.appendChild(cont);
  }
};

startBtn.addEventListener('click', () => {
  toggleUIVisibility(menu);
  // toggleUIVisibility(canvas);
  // setTimeout(500, () => startGame());
  startGame();
});

resumeBtn.addEventListener('click', () => {
  toggleUIVisibility(pause);
  messages.push({ type: 'TOGGLE_PAUSE' });
});

quitBtn.addEventListener('click', () => {
  toggleUIVisibility(pause);
  messages.push({ type: 'QUIT' });
});

scoresBtn.addEventListener('click', () => {
  toggleUIVisibility(menu);
  toggleUIVisibility(scores);
  loadScores();
});

creditsBtn.addEventListener('click', () => {
  toggleUIVisibility(menu);
  toggleUIVisibility(credits);
});

resetScoreBtn.addEventListener('click', () => {
  localStorage.setItem('highScores', JSON.stringify([]));
  loadScores();
});

registerInitialKeyHandler();
