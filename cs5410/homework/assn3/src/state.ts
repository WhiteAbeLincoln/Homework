import * as Ba from './Ball';
import Ball from './Ball';
import * as Br from './Brick';
import Brick from './Brick';
import { Action, Message } from './Message';
import { Paddle, newPaddle, baseSize, addRemovedBricks, shrink, getXVelocity } from './Paddle';
import { Player, updateLives, removeLife, updateScore, newPlayer } from './Player';
import { atLeftWall, atRightWall, atTopWall, atBottomWall, Point, Plane } from './Collision';
import { pipe, flatten } from './util';
import * as Go from './GameObject';
import { GameObject } from './GameObject';
import { Particle, createParticles, updateParticle } from './Particle';
/*
State management
The game has multiple states
Change the update function depending on
what state it is in. You could keep this as a function under
the object, then run that function every time, instead of a specific update
*/

export interface Area {
  name: string;
  height: number;
  width: number;
  pos: Point;
  color?: string;
  image?: string;
  children?: Area[];
}

interface Input {
  key: string;
  action: Action;
}

interface Game {
  controls: Input[];
  /*
  We should be able to represent our state using
  a JSX style structure

  <area color="black" image="blah">
    <area name="gameArea" height="blah" width="blah">
      <area name="brickArea" height="blah" width="blah">
      </area>
    </area>
    <area name="statsArea">
      <playerLives player={player} />
      <score player={player} />
    </area>
  </area>
  */
  rootArea: Area;
  over: boolean;
  paused: boolean;
}

export type StateReducer = (s: State) => State;

export interface State {
  reducers: StateReducer[];
  game: Game;
  player: Player;
  balls: Ball[];
  paddle: Paddle;
  // bricks will be sorted by color, with yellows coming first.
  // Every row will be 20 bricks wide (for easy rendering if we divide the background into a 100 pt grid)
  // thererfore we can get the row by doing floor(index + 1 / 14)
  bricks: Brick[];
  particles: Particle[];
  previousTime: number;
  elapsedTime: number;
  totalTime: number;
  countdown: number;
  context?: CanvasRenderingContext2D;
}

export const initialState = (width: number, height: number): State => {
  const gameAreaHeight = Math.floor((7 * height) / 8);
  const { width: paddleWidth, height: paddleHeight} = baseSize({width, height});
  const paddleX = Math.floor(width / 2 - paddleWidth / 2);
  const paddleY = Math.floor(gameAreaHeight - paddleHeight);
  const brickArea = Br.getBrickArea({ width, height });

  return {
    reducers: [],
    game: {
      controls: [
        { key: 'ArrowRight', action: 'RIGHT' },
        { key: 'ArrowLeft', action: 'LEFT' },
        { key: 'Escape', action: 'ESCAPE' },
      ],
      rootArea: {
        name: 'bg',
        pos: {
              type: 'Point',
              x: 0,
              y: 0,
        },
        width,
        height,
        color: 'black',
        image: 'https://www.nasa.gov/sites/default/files/images/420970main_M51HST-GendlerMr_full.jpg',
        children: [
          {
            name: 'gameArea',
            width,
            height: gameAreaHeight,
            pos: {
              type: 'Point',
              x: 0,
              y: 0,
            },
            children: [{
              name: 'brickArea',
              width,
              height: brickArea.end - brickArea.start,
              pos: {
                type: 'Point',
                x: 0,
                y: brickArea.start,
              },
            }],
          },
          {
            name: 'statsArea',
            color: 'aquamarine',
            width,
            height: Math.floor(height / 8),
            pos: {
              type: 'Point',
              x: 0,
              y: Math.floor((7 * height) / 8),
            },
          },
        ],
      },
      over: false,
      paused: false,
    },
    player: newPlayer(2),
    particles: [],
    balls: [],
    paddle: newPaddle(paddleWidth, paddleHeight, paddleX, paddleY),
    bricks: Br.newBrickList({ x: 0, y: brickArea.start }, width, brickArea.end),
    previousTime: 0,
    elapsedTime: 0,
    totalTime: 0,
    countdown: performance.now() + 3000,
  };
};

const handleBallCollision = (s: State) => (b: Ball): Ball => {
  if (atLeftWall(s.game.rootArea)(b)) {
    return pipe(
      Go.updatePosition({
        type: 'Point',
        x: b.radius,
        y: b.pos.y,
      }),
      Go.updateVelocity({
        x: -b.velocity.x,
        y: b.velocity.y,
      }),
    )(b) as Ball;
  } else if (atRightWall(s.game.rootArea)(b)) {
    return pipe(
      Go.updateVelocity({
        x: -b.velocity.x,
        y: b.velocity.y,
      }),
      Go.updatePosition({
        type: 'Point',
        x: s.game.rootArea.width - b.radius,
        y: b.pos.y,
      }),
    )(b) as Ball;
  } else if (atTopWall(s.game.rootArea)(b)) {
    return pipe(
      Go.updatePosition({
        type: 'Point',
        x: b.pos.x,
        y: b.radius,
      }),
      Go.updateVelocity({
        x: b.velocity.x,
        y: -b.velocity.y,
      }),
    )(b) as Ball;
  } else if (b.velocity.y > 0 && Ba.collidingWithPaddle(b)(s.paddle)) {
    return pipe(
      Go.updateVelocity({
        x: b.velocity.x * getXVelocity(s.paddle, b),
        y: -b.velocity.y,
      }),
      Go.updatePosition({
        type: 'Point',
        x: b.pos.x,
        y: s.paddle.pos.y - b.radius,
      }),
    )(b) as Ball;
  }

  return b;
};

const updatePosition = (dt: number) => (go: GameObject): GameObject => Go.updatePosition({
  type: 'Point',
  x: go.pos.x + go.velocity.x * (dt / 1000),
  y: go.pos.y + go.velocity.y * (dt / 1000),
})(go);

const updateBalls = (s: State): State => {
  s.balls = s.balls.map(updatePosition(s.elapsedTime)).map(b => handleBallCollision(s)(b as Ball));
  return s;
};

const removeBall = (b: Ball) => (s: State): State => {
  const ballIdx = s.balls.indexOf(b);
  s.balls.splice(ballIdx, 1);

  return s;
};

const reflectCurrentBall = (b: Ball) => (p: Point[]) => (s: State): State => {
  const newBalls = [...s.balls];
  const ballIdx = newBalls.indexOf(b);
  newBalls[ballIdx] = p.reduce((p, c) => ({
    ...p,
    velocity: {
      x: p.velocity.x * c.x,
      y: p.velocity.y * c.y,
    },
  }), b);

  s.balls = newBalls;
  return s;
};

const resetPaddle = (s: State): State => {
  const newPaddleSize = baseSize(s.game.rootArea);

  // const x = Math.floor(s.game.rootArea.width / 2 - newPaddleSize.width / 2);
  // const y = gameAreaHeight - newPaddleSize.height;

  s.paddle = newPaddle(newPaddleSize.width, newPaddleSize.height, s.paddle.pos.x, s.paddle.pos.y);
  return s;
};

const startCountdown = (ms: number) => (s: State): State => {
  s.countdown = ms;
  return s;
};

const gameOverIfNoLives = (s: State): State => {
  if (s.player.lives <= 0) {
    s.game.over = true;
  }
  return s;
};

const handleMissedBall = (b: Ball) => (s: State) =>
  pipe(
    removeBall(b),
    resetPaddle,
    // remove a player life
    (s: State) => { s.player = removeLife(s.player); return s; },
    gameOverIfNoLives,
    addBallIfNone,
    startCountdown(3000),
  )(s);

const handleBrickCollisions = (b: Ball) => (s: State) => {
  const collisions = Br.getCollidingBricks(s.bricks)(b, s.elapsedTime);
  if (collisions.length === 0) return s;

  const numberGreen = s.bricks.filter(b => b.color === 'green').length;
  const justBrokeGreen = collisions.some(i => i.idx >= 0 && i.idx <= Math.floor(numberGreen / 2));

  const shouldShrinkPaddle = justBrokeGreen && !s.paddle.hasShrunk;
  // shrink the paddle when we break green
  // we only shrink the first time a green is broken, or the first time after getting a new paddle
  const newPaddle = shouldShrinkPaddle ? shrink(s.paddle) : s.paddle;
  // update the player's score for each broken brick
  const newPlayer = collisions.reduce((p, c) => updateScore(s.bricks[c.idx].points)(p), s.player);
  newPlayer.score += Br.lineWasCleared(collisions.map(i => i.idx))(s.bricks) ? 25 : 0;

  const newState = reflectCurrentBall(b)(collisions.map(c => c.push))(s);
  newState.particles =
    newState.particles.concat(flatten(collisions.map(i => s.bricks[i.idx]).map(b => createParticles(b, 1))));
  newState.bricks = Br.breakBricks(s.bricks)(collisions.map(i => i.idx));
  newState.player = newPlayer;
  newState.paddle = addRemovedBricks(newPaddle)(collisions.length);

  return newState;
};

const updateStateForCollision = (s: State, b: Ball): State => {
  if (Br.inBrickArea(s.game.rootArea)(b)) {
    return handleBrickCollisions(b)(s);
  } else if (atBottomWall(s.game.rootArea)(b)) {
    return handleMissedBall(b)(s);
  }

  return s;
};

const collision = (s: State): State => {
  // update every ball when it collides with walls or paddle
  if (s.balls.length === 0)
    s = addBall(s);
  const ballState = updateBalls(s);
  // update the rest of the state when any ball collides with a
  // brick or falls through the bottom
  return ballState.balls.reduce(updateStateForCollision, ballState);
};

const addBall = (s: State): State => {
  s.balls.push(Ba.newBall(Math.floor(s.paddle.pos.x + s.paddle.width / 2), s.paddle.pos.y - 5, 5));
  return s;
};

const addBallIfNone = (s: State): State =>
  s.balls.length === 0 ? addBall(s) : s;

let scoreCounter = 0;
const addBallIfScore = (s: State): State => {
  if (s.player.score - scoreCounter > 100) {
    scoreCounter = s.player.score;
    return addBall(s);
  }

  return s;
};

const increaseBallSpeedIfBricksRemoved = (s: State): State => {
  // if we have removed one of these amounts of bricks
  if ([4, 12, 36, 62].includes(s.paddle.removedBricks)) {
    s.balls = s.balls.map(Ba.increaseSpeed);
  }
  return s;
};

const clearReducers = (state: State): State => {
  state.reducers = [];
  return state;
};

const runReducers = (state: State): State =>
  pipe<State, State>(...state.reducers, clearReducers)(state);

const revertPaddle = (paddle: Paddle) => (state: State) => {
  state.paddle = paddle;

  return state;
};

const updateParticles = (state: State): State => {
  // tslint:disable-next-line:prefer-for-of
  for (let i = 0; i < state.particles.length; i++) {
    updateParticle(state.particles[i], state.elapsedTime);
  }

  state.particles = state.particles.filter(p => p.ttl > 0);
  return state;
};

export const update = (state: State): State => {

  // handle pause, etc here
  if (state.game.paused)
    // we run the reducers as normal, and revert any changes from user game action i.e. paddle movement
    return pipe(runReducers, revertPaddle(state.paddle))(state);
  if (state.game.over)
    return pipe(runReducers, revertPaddle(state.paddle))(state);

  if (state.countdown > 0) {
    // do nothing but update countdown if it's active
    const newState = pipe(runReducers, revertPaddle(state.paddle))(state);

    if (!state.game.paused)
      newState.countdown = state.countdown - state.elapsedTime;

    return newState;
  } else {
    // Do State update
    return pipe(
      // the user input reducers
      runReducers,
      updateParticles,
      // calculate changes dealing with collision
      collision,
      // add a new ball if the score is at the correct interval
      addBallIfScore,
      // increase ball speed if the number of removed bricks is at the correct interval
      increaseBallSpeedIfBricksRemoved,
      // s => { s.game.over = !s.game.over && s.bricks.every(b => b.broken); return s; },
    )(state);
  }
};
