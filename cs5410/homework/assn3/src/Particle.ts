import { Vec2 } from './GJK';
import { Brick } from './Brick';
import { getRandom } from './util';

export interface Particle {
  color: string;
  radius: number;
  pos: Vec2;
  ttl: number;
  velocity?: Vec2;
  radiusRate?: number;
  acceleration?: Vec2;
}

export const updateParticle = (particle: Particle, dt: number) => {
  if (particle.velocity) {
    particle.pos = particle.pos.add(particle.velocity.smult(dt / 1000));
  }

  if (particle.acceleration && particle.velocity) {
    particle.velocity.add(particle.acceleration.smult(dt / 1000));
  }

  if (particle.radiusRate) {
    particle.radius = particle.radius + particle.radiusRate * (dt / 1000);
  }

  particle.ttl = particle.ttl - dt;

  return particle;
};

export const createParticles = (b: Brick, radius: number): Particle[] => {
  const minx = b.pos.x;
  const maxx = b.pos.x + b.width;
  const miny = b.pos.y;
  const maxy = b.pos.y + b.height;

  const offset = 2;
  const width = maxx - minx;
  const height = maxy - miny;
  let numHoriz = width / radius;
  let numOffset = numHoriz - 1;
  while (numHoriz * radius + offset * numOffset > width) {
    numHoriz--;
    numOffset--;
  }

  let numVert = height / radius;
  let numYOffset = numVert - 1;
  while (numVert * radius + offset * numYOffset > height) {
    numVert--;
    numYOffset--;
  }
  const extray = height - (numVert * radius + offset * numYOffset);
  const extrax = width - (numHoriz * radius + offset * numOffset);

  let x = minx;
  let y = miny;

  if (extrax > 0) {
    x = Math.floor(x + extrax / 2);
  }

  if (extray > 0) {
    y = Math.floor(y + extray / 2);
  }

  const particles: Particle[] = [];

  for (let i = 0; i < numVert; i++) {
    const origx = x;
    for (let j = 0; j < numHoriz; j++) {
      particles.push({
        pos: new Vec2(x, y),
        radius,
        color: b.color,
        ttl: getRandom(1000, 2500),
        velocity: new Vec2(0, getRandom(15, 20)),
        acceleration: new Vec2(0, getRandom(-15, 5)),
      });

      x += radius + offset;
    }
    y += radius + offset;
    x = origx;
  }

  return particles;
};
