import { Point } from './Collision';

export interface Velocity {
  x: number;
  y: number;
}

export interface GameObject {
  pos: Point;
  velocity: Velocity;
  angle: number; // currently unused
}

// I would like to make this generic, but can't because classes can't spread and they fit
// the <T extends GameObject>
export const updatePosition = (point: Point) => (go: GameObject): GameObject => {
  go.pos = point;

  return go;
};

export const updateVelocity = (velocity: Velocity) => (go: GameObject): GameObject => {
  go.velocity = velocity;

  return go;
};
