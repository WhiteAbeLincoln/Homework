// import { clock$ } from './State'
import { GameMap, Direction } from './StateModel';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/bufferCount';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/scan';

export const animation$: Observable<number> = new Observable(sub => {
  let stop = false;

  const func = (time: number) => {
    sub.next(time);
    if (!stop) requestAnimationFrame(func);
  };

  func(performance.now());

  return () => { stop = true; };
});

export const clock$ = animation$
                          .bufferCount(2, 1)
                          .map(([prev, current]) => current - prev);

export const keypress$ = (elem: HTMLElement): Observable<string[]> =>
  new Observable(sub => {
    const map: { [key: string]: boolean } = {};

    const keydown = (e: KeyboardEvent) => {
      if (e.defaultPrevented) return;
      map[e.key] = true;
      const pressedKeys = Object.keys(map).filter(k => map[k]);

      sub.next(pressedKeys);
    };

    const keyup = (e: KeyboardEvent) => {
      map[e.key] = false;
    };

    elem.addEventListener('keydown', keydown, true);
    elem.addEventListener('keyup', keyup, true);

    return () => {
      elem.removeEventListener('keydown', keydown, true);
      elem.removeEventListener('keyup', keyup, true);
    };
  });

export const click$ = (elem: HTMLElement): Observable<MouseEvent> =>
    Observable.fromEvent(elem, 'click');

export const clickCoordinates$ = (element: HTMLElement) =>
                        click$(element).map(e => {
  const rect = element.getBoundingClientRect();
  const x = e.clientX - rect.left;
  const y = e.clientY - rect.top;
  return { x, y };
});

export const updateSize$ = Observable.fromEvent(window, 'resize').startWith(null);

export const updateMap$: Observable<GameMap> = Observable.of({ width: 50, height: 50 });

export const moveForward$ = clock$.scan((acc, curr) => {
  if (acc > 150) {
    return 0;
  }

  return acc + curr;
}, 0).map(t => t === 0);

type ArrowKey = 'ArrowUp' | 'ArrowDown' | 'ArrowRight' | 'ArrowLeft';

const isArrowKey = (key: string): key is ArrowKey => {
  return key === 'ArrowUp' || key === 'ArrowDown' || key === 'ArrowRight' || key === 'ArrowLeft';
};

export const changeDirection$ = (elem: any): Observable<Direction> =>
  keypress$(elem)
    .map(keys => keys[keys.length - 1])
    .filter(isArrowKey)
    .map(key => {
      switch (key) {
        case 'ArrowUp': return 'up';
        case 'ArrowDown': return 'down';
        case 'ArrowLeft': return 'left';
        case 'ArrowRight': return 'right';
      }
    });
