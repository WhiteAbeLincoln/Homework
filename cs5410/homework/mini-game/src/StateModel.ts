
/* * * * * * * * * * * *
 *                     *
 *      INTERFACE      *
 *     DEFINITIONS     *
 *                     *
 * * * * * * * * * * * */

export interface GameMap {
  width: number;
  height: number;
}

export interface Point {
  x: number;
  y: number;
}

export interface GameObject {
  kind: 'food' | 'obstacle' | 'segment';
  color: string;
  pos: Point;
}

export interface Obstacle extends GameObject {
  kind: 'obstacle';
  color: 'green';
}

export interface Border extends GameObject {
  kind: 'obstacle';
  color: 'red';
}

export interface Food extends GameObject {
  kind: 'food';
  color: 'orange';
}

export type Direction = 'left' | 'right' | 'up' | 'down';

export interface Segment extends GameObject {
  kind: 'segment';
  color: 'white';
  direction: Direction | null;
  head: boolean;
}

export interface Snake {
  turningPoints: {
    point: Point;
    direction: Direction
  };
  segments: Segment[];
}

export interface State {
  elapsed: number;
  map: GameMap;
  objects: Map<string, GameObject>;
  food: Food;
  head: Segment;
  tail: Segment[];
  turningPoints: Array<{
    point: Point;
    direction: Direction;
  }>;
  over: boolean;
  newTailQueue: Segment[];
  countdown: number;
  // tail: Map<string, Segment>;
  // snake: Snake;
}

/* * * * * * * * * * * *
 *                     *
 *   HELPER FUNCTIONS  *
 *                     *
 * * * * * * * * * * * */

export const newBorder = (pos: Point): GameObject => ({
  kind: 'obstacle',
  pos,
  color: 'red',
});

export const newFood = (pos: Point): Food => ({
  kind: 'food',
  pos,
  color: 'orange',
});

export const newObstacle = (pos: Point): GameObject => ({
  kind: 'obstacle',
  pos,
  color: 'green',
});

export const newSegment = (pos: Point): Segment => ({
  kind: 'segment',
  pos,
  color: 'white',
  direction: null,
  head: false,
});

export const newHead = (pos: Point) => {
  const seg = newSegment(pos);
  seg.head = true;
  return seg;
};
