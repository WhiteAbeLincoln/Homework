import { getInitialState, update } from './state';
import { render } from './Render';
import { updateSize$, clock$ } from './Actions';
import 'rxjs/add/observable/fromEvent';
import { Observable } from 'rxjs/Observable';

/**
 * I made the mistake of trying something new with the observables,
 * and so didn't actually start writing working code until I had an
 * hour left... Sorry.
 */

const updateCanvasSize = (canvas: HTMLCanvasElement) => {
  canvas.width = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
};

const canvas = document.getElementById('canvas') as HTMLCanvasElement;

updateSize$.subscribe(() => updateCanvasSize(canvas));

const getHighScores = (): Array<{name: string, score: number}> => {
  const hs = localStorage.getItem('highScores');
  let highScores: Array<{ name: string, score: number }> = [];

  if (hs) highScores = JSON.parse(hs);

  return highScores;
};

const newHighScore = (s: number) => {
  const scores = getHighScores();
  return scores.some(sc => sc.score < s) || (scores.length < 5 && s !== 0);
};

const writeHighScore = (newScore: { name: string, score: number}) => {
  const current = getHighScores();
  current.push(newScore);
  current.sort((a, b) => a.score > b.score ? -1
                      : a.score < b.score ? 1
                      : 0);
  if (current.length > 5) {
    current.splice(5);
  }
  localStorage.setItem('highScores', JSON.stringify(current));
};

const startGame = () => {
  const state = getInitialState();

  return new Promise<number>((res, rej) => {
    const sub = clock$.subscribe(prev => {
      state.elapsed = prev;
      update(canvas, state);
      render(canvas)(state);

      if (state.over) {
        res(state.tail.length + 1);
        sub.unsubscribe();
      }
    });
  });
};

startGame().then(score => {
  alert(`Game over, score ${score}`);
  if (newHighScore(score)) {
    const text = prompt('Enter your name') || 'ANON';
    writeHighScore({ name: text, score });
  }
});
