import { State, Point, GameMap, GameObject, Direction } from './StateModel';

interface Area {
  width: number;
  height: number;
}

export const convertDim = (src: number, srcMin: number, srcMax: number, resMin: number, resMax: number) =>
                           (src - srcMin) / (srcMax - srcMin) * (resMax - resMin) + resMin;

const convertPoint = (smin: Point, smax: Point) => (dmin: Point, dmax: Point) => (point: Point) => {
  const x = convertDim(point.x, smin.x, smax.x, dmin.x, dmax.x);
  const y = convertDim(point.y, smin.y, smax.x, dmin.y, dmax.y);

  return { x, y };
};

const convertPoint2 = (srcArea: Area) => (destArea: Area) =>
                      convertPoint({ x: 0, y: 0 }, { x: srcArea.width, y: srcArea.height })(
                                   { x: 0, y: 0 }, { x: destArea.width, y: destArea.height });

export const getDrawArea = (m: GameMap, width: number, height: number) => {
  // const aspectRatio = m.width / m.height
  // for now assume our map is always square
  const min = Math.min(width, height);

  const { x, y } = convertPoint({x: 0, y: 0}, {x: m.width, y: m.height})(
                                {x: 0, y: 0}, {x: min, y: min})(
                                {x: m.width, y: m.height});
  return { width: x, height: y };
};

const drawObjects = (objects: GameObject[]) =>
                    (ctx: CanvasRenderingContext2D, srcArea: Area, destArea: Area) => {
  const rectSize = destArea.width / 50;
  for (const object of objects) {
    ctx.fillStyle = object.color;
    const { x, y } = convertPoint2(srcArea)(destArea)(object.pos);
    ctx.fillRect(x, y, rectSize, rectSize);
  }
};

const centeredText = (ctx: CanvasRenderingContext2D, text: string, area: Area) => {
  ctx.font = '60px Arial';
  ctx.fillStyle = 'white';
  const length = ctx.measureText(text);
  const halfwayX = area.width / 2;
  const halfwayY = area.height / 2;

  ctx.fillText(text, halfwayX - length.width / 2, halfwayY);
};

const getColor = (t: { direction: Direction }) => {
  switch (t.direction) {
    case 'left': return 'lavender';
    case 'right': return 'rebeccapurple';
    case 'up': return 'turquoise';
    case 'down': return 'darksalmon';
  }
};

export const render = (canvas: HTMLCanvasElement) => (state: State) => {
  const ctx = canvas.getContext('2d');
  if (!ctx) throw new Error('Couldn\'t get 2d context');

  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = 'blue';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  const drawArea = getDrawArea(state.map, canvas.width, canvas.height);
  const mapArea = state.map;

  const renderFuncs = [];

  renderFuncs.push(drawObjects([...state.objects.values()]));
  renderFuncs.push(drawObjects([...state.tail, state.head]));
  renderFuncs.push(drawObjects([state.food]));
  renderFuncs.push(drawObjects(state.turningPoints.map(t => {
    return ({
      kind: 'obstacle',
      pos: t.point,
      color: getColor(t),
    }) as GameObject;
  })));

  renderFuncs.forEach(f => f(ctx, mapArea, drawArea));

  if (state.countdown > 0) {
    centeredText(ctx, (state.countdown / 1000).toFixed(0), drawArea);
  }
};
