import { State, GameObject, GameMap, newBorder, newObstacle, newFood, Food, newHead, Direction } from './StateModel';
import { getRandomInt } from './util';
import { changeDirection$, moveForward$ } from './Actions';
import 'rxjs/add/operator/distinctUntilChanged';

const genBorders = (m: GameMap) => {
  const borders = new Map<string, GameObject>();
  for (let i = 0; i < m.width; i++) {
    borders.set(`${i},0`, newBorder({ x: i, y: 0 }));
    borders.set(`${i},${m.height - 1}`, newBorder({ x: i, y: m.height - 1 }));
  }

  for (let i = 0; i < m.height; i++) {
    borders.set(`0,${i}`, newBorder({ x: 0, y: i }));
    borders.set(`${m.width - 1},${i}`, newBorder({ x: m.width - 1, y: i }));
  }

  return borders;
};

const genObstacles = (m: Map<string, GameObject>) => {
  const obj = new Map([...m]);
  let obstacleCount = 0;

  while (obstacleCount < 16) {
    const x = getRandomInt(1, 50);
    const y = getRandomInt(1, 50);
    const key = `${x},${y}`;
    if (!obj.has(key)) {
      obj.set(key, newObstacle({ x, y }));
      obstacleCount++;
    }
  }

  return obj;
};

const genFood = (m: Map<string, GameObject>) => {
  const obj = new Map([...m]);

  while (true) {
    const x = getRandomInt(1, 50);
    const y = getRandomInt(1, 50);
    const key = `${x},${y}`;
    if (!obj.has(key)) {
      return newFood({ x, y });
    }
  }
};

const genHead = (m: Map<string, GameObject>, food: Food) => {
  const obj = new Map([...m]);

  while (true) {
    const x = getRandomInt(1, 50);
    const y = getRandomInt(1, 50);
    const key = `${x},${y}`;
    if (!obj.has(key) && food.pos.x !== x && food.pos.y !== y) {
      return newHead({ x, y });
    }
  }
};

export const getInitialState = (): State => {
  const map: GameMap = { width: 50, height: 50 };
  const borders = genBorders(map);
  const obstacles = genObstacles(borders);
  const firstFood = genFood(obstacles);
  const head = genHead(obstacles, firstFood);

  return {
    map,
    objects: new Map([...borders, ...obstacles]),
    food: firstFood,
    head,
    tail: [],
    turningPoints: [],
    over: false,
    elapsed: 0,
    newTailQueue: [],
    countdown: performance.now() + 3000,
  };
};

const colliding = (g1: GameObject, g2: GameObject) => {
  return g1.pos.x === g2.pos.x && g1.pos.y === g2.pos.y;
};

let currentDir: null | Direction = null;
let moveForward = false;
moveForward$.subscribe(m => { moveForward = m; });

const oppositeDir = (d: Direction) => {
  switch (d) {
    case 'left': return 'right';
    case 'up': return 'down';
    case 'right': return 'left';
    case 'down': return 'up';
  }
};

const isValidDirection = (dir1: Direction | null, dir2: Direction | null): dir1 is Direction => {
  if (!dir1 || !dir2) return false;
  if (dir1 === dir2) return false;
  if (oppositeDir(dir1) === dir2 || dir1 === oppositeDir(dir2)) return false;
  return true;
};

const addTurningPoint = (state: State, dir: Direction | null) => {
  if (!dir) return;
  if (!state.head.direction) {
    state.turningPoints.push({ point: { ...state.head.pos }, direction: dir });
  }

  if (isValidDirection(state.head.direction, dir)) {
    state.turningPoints.push({ point: { ...state.head.pos }, direction: dir });
  }
};

const changeDirection = (state: State) => {
  let tps = state.turningPoints;
  const last = state.tail[state.tail.length - 1] || state.head;
  for (const obj of [state.head, ...state.tail]) {
    const points = tps.filter(tp => tp.point.x === obj.pos.x && tp.point.y === obj.pos.y);
    const notpoints = tps.filter(tp => !(tp.point.x === obj.pos.x && tp.point.y === obj.pos.y));

    if (!points.length) continue;
    const point = points[0];
    obj.direction = point.direction;

    if (obj === last && state.newTailQueue.length === 0) {
      tps = notpoints;
    }
  }

  state.turningPoints = tps;
};

const advance = (state: State, move: boolean) => {
  if (move) {
    for (const obj of [...state.tail, state.head]) {
      if (state.newTailQueue.length > 0) {
        const [next, ...rest] = state.newTailQueue;
        state.tail.push(next);
        state.newTailQueue = rest;
      }
      switch (obj.direction) {
        case 'left':
          obj.pos.x--;
          break;
        case 'right':
          obj.pos.x++;
          break;
        case 'up':
          obj.pos.y--;
          break;
        case 'down':
          obj.pos.y++;
          break;
      }
    }
  }
};

const getPreviousPos = (g: GameObject, d: Direction) => {
  const pos = {...g.pos};
  switch (d) {
    case 'left':
      pos.x--;
    case 'right':
      pos.x++;
    case 'up':
      pos.y++;
    case 'down':
      pos.y--;
  }

  return pos;
};

const checkCollision = (state: State) => {
  const head = state.head;
  for (const obj of [...state.tail, ...state.objects.values(), state.food]) {
    if (colliding(head, obj) && obj.kind !== 'food') {
      return true;
    } else if (colliding(head, obj) && obj.kind === 'food') {
      const last = state.tail[state.tail.length - 1] || state.head;
      const next = { ...last, pos: getPreviousPos(last, last.direction!) };
      const nextnext = { ...next, pos: getPreviousPos(next, last.direction!) };
      const snake = [...state.tail, state.head]
                      .map(g => ([`${g.pos.x},${g.pos.y}`, g as GameObject])) as Array<[string, GameObject]>;
      state.food = genFood(new Map([...state.objects, ...snake]));
      state.newTailQueue.push({ ...last, pos: { ...last.pos } });
      state.newTailQueue.push(next);
      state.newTailQueue.push(nextnext);
      return false;
    }
  }

  return false;
};

export const update = (elem: HTMLElement, state: State) => {
  if (state.countdown > 0) {
    state.countdown = state.countdown - state.elapsed;
    return;
  }
  const sub = changeDirection$(window).distinctUntilChanged().subscribe(c => {
    currentDir = c;
  });
  addTurningPoint(state, currentDir);
  changeDirection(state);
  advance(state, moveForward);
  const collided = checkCollision(state);
  if (collided) {
    state.over = true;
  }
  // sub.unsubscribe();
};
