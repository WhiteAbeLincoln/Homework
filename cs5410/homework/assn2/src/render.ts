import { Graph, isConnected } from './Graph';
import { Cell } from './maze';
import { hasRight, getRight, hasDown, getDown } from './gridMaze';
import { Maze, State } from './state';

const getWallWidth = (width: number) => width / 100;
const getWallHeight = (height: number) => height / 100;
const getCellWidth = (n: number) => (width: number) => (width - (n-1) * getWallWidth(width)) / n;
const getCellHeight = (n: number) => (height: number) => (height - (n-1) * getWallHeight(height)) / n;
export const getMazeDimension = (m: Maze) => Math.sqrt(m.graph.nodes.length);

const drawMaze = (ctx: CanvasRenderingContext2D) => (state: State) => {
  const { maze, player } = state;
  const {
    graph,
    bg: { size: [width, height], color: bgColor },
    wallColor,
    endColor,
  } = maze;
  // we assume a square matrix for now
  const n = getMazeDimension(maze);
  // we have n-1 cell borders of 5 px each
  // we divide the space into a 100 by 100 grid
  const ww = getWallWidth(width);
  const wh = getWallHeight(height);

  const cw = getCellWidth(n)(width);
  const ch = getCellHeight(n)(height);

  for (let i = 0; i < graph.nodes.length; i++) {
    // draw the rectangle
    ctx.fillStyle = i === graph.nodes.length - 1 ? endColor : bgColor;
    ctx.fillRect(0, 0, cw, ch);

    const right = getRight(n, n)(i);
    const down = getDown(n, n)(i);

    if (right) {
      // draw wall
      if (!isConnected(graph)(i, right)) {
        ctx.fillStyle = wallColor;
        ctx.fillRect(cw, -wh, ww, ch + (down ? 2 * wh : wh));
      } else {
        ctx.fillStyle = bgColor;
        ctx.fillRect(cw, 0, ww, ch);
      }
    }

    if (down) {
      if (!isConnected(graph)(i, down)) {
        ctx.fillStyle = wallColor;
        ctx.fillRect(-ww, ch, cw + (right ? 2 * ww : ww), wh);
      } else {
        ctx.fillStyle = bgColor;
        ctx.fillRect(0, ch, cw, wh);
      }
    }

    if (right) {
      ctx.translate(cw + ww, 0);
    } else {
      ctx.translate((cw + ww) * -(n - 1), ch + wh);
    }
  }

  ctx.translate(0, (ch + wh) * -n);
};

const drawPlayer = (ctx: CanvasRenderingContext2D) => (state: State) =>
                    drawBox(ctx)(state)(state.player.position, '#efefef');

const drawBox = (ctx: CanvasRenderingContext2D) => (state: State) => (position: number, color: string) => {
  const { maze } = state;
  const n = getMazeDimension(maze);
  const ww = getWallWidth(maze.bg.size[0]);
  const wh = getWallHeight(maze.bg.size[1]);

  const cw = getCellWidth(n)(maze.bg.size[0]);
  const ch = getCellHeight(n)(maze.bg.size[1]);

  const row = (Math.floor(position / n));
  const column = (Math.floor(position % n));
  const boxw = cw / 2;
  const boxh = ch / 2;
  const x = column * (cw + ww) + (cw / 2) - boxw / 2;
  const y = row * (ch + wh) + (ch / 2) - boxh / 2;

  ctx.fillStyle = color;
  ctx.fillRect(x, y, boxw, boxh);
};

const drawBg = (ctx: CanvasRenderingContext2D) => (state: State) => {
  const { color, size: [width, height] } = state.bg;
  ctx.fillStyle = color;
  ctx.rect(0, 0, width, height);
  ctx.fill();
};

const drawStats = (ctx: CanvasRenderingContext2D) => (state: State) => {
  ctx.font = '20px monospace';
  ctx.fillStyle = 'white';
  const text = `Score: ${state.player.score} Won: ${state.player.won} Time: ${state.totalTime}`;
  ctx.fillText(text, 10, state.maze.bg.size[1] + 30, state.bg.size[0]);
};
const drawCrumbs = (ctx: CanvasRenderingContext2D) => (state: State) =>
  state.player.past_cells.forEach(i => drawBox(ctx)(state)(i, '#4321af'));
const drawPath = (ctx: CanvasRenderingContext2D) => (state: State) =>
  state.maze.graph.path.forEach(i => drawBox(ctx)(state)(i, '#6e7f80'));

const drawHint = (ctx: CanvasRenderingContext2D) => (state: State) => { /**/ };

export const render = (ctx: CanvasRenderingContext2D) => (state: State) => {
  ctx.clearRect(0, 0, state.bg.size[0], state.bg.size[1]);
  drawBg(ctx)(state);
  if (state.render.score) {
    drawStats(ctx)(state);
  }
  drawMaze(ctx)(state);
  if (state.render.hint) {
    drawHint(ctx)(state);
  }
  if (state.render.crumbs) {
    drawCrumbs(ctx)(state);
  }
  if (state.render.path) {
    drawPath(ctx)(state);
  }
  drawPlayer(ctx)(state);

  return state;
};
