import { range } from './util';

export interface Graph<T> {
  adjacency_matrix: number[][];
  nodes: T[];
}

export default Graph;

const genFullMatrix = (n: number) => range(n).map(e => range(n).fill(1));

export const genConnectedGraph = (n: number): Graph<number> => {
  const matrix = genFullMatrix(n)
                  .map((r, i) => r.map((c, j) => (i === j) ? 0 : c));

  return {
    adjacency_matrix: matrix,
    nodes: range(n),
  };
};

export const isConnected = (g: Graph<any>) => (from: number, to: number) => g.adjacency_matrix[from][to] !== 0;
export const connectedTo = (g: Graph<any>) => (index: number) =>
    g.adjacency_matrix[index].filter((v, i) => isConnected(g)(index, i)).map((v, i) => i);

export const shortestPath = (g: Graph<any>) => (root: number, v: number) => {
  return [];
};
