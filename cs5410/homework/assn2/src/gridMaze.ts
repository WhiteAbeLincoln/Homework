import { range, shuffle, getUnique } from './util';
import { Wall } from './maze';

// given a mxn sized grid, stored as a flat list, navigates the list by returning indexes of the next
// item in the list
export const getLeft  = (m: number, n: number) => (i: number) => hasLeft(m, n)(i)  ? i - 1 : null;
export const getRight = (m: number, n: number) => (i: number) => hasRight(m, n)(i) ? i + 1 : null;
export const getDown  = (m: number, n: number) => (i: number) => hasDown(m, n)(i)  ? i + n : null;
export const getUp    = (m: number, n: number) => (i: number) => hasUp(m, n)(i)    ? i - n : null;

export const hasLeft  = (m: number, n: number) => (i: number) => i % n !== 0;
export const hasRight = (m: number, n: number) => (i: number) => (i + 1) % n !== 0 && i !== m * n - 1;
export const hasDown  = (m: number, n: number) => (i: number) => (i + n) < m * n;
export const hasUp    = (m: number, n: number) => (i: number) => (i - n) >= 0;

export const getNumWalls = (m: number, n: number) => m * (m - 1) + n * (n - 1);

export const genWallList = (m: number, n: number) => {
  const walls: Wall[] = [];

  const rightOf = getRight(m, n);
  const downOf = getDown(m, n);
  for (let i = 0; i < m * n; i++) {
    if (hasRight(m, n)(i))
      walls.push({
        divides: [i, rightOf(i) as number],
      });
    if (hasDown(m, n)(i))
      walls.push({
        divides: [i, downOf(i) as number],
      });
  }

  return walls;
};

export const genCellList = (m: number, n: number) => {
  return range(m * n).map(e => [{ id: e }]);
};
