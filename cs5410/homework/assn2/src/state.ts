import { Graph, genConnectedGraph, isConnected } from './Graph';
import { Cell, randomKruskal, GraphMaze } from './maze';
import { hasLeft, getLeft, getDown, getRight, getUp, genCellList, genWallList } from './gridMaze';
import { getMazeDimension } from './render';

export type Action =  'LEFT'
                    | 'RIGHT'
                    | 'DOWN'
                    | 'UP'
                    | 'TOGGLE_CRUMBS'
                    | 'TOGGLE_PATH'
                    | 'TOGGLE_SCORE'
                    | 'TOGGLE_HINT';

export type MessageKey = Action | 'RESIZE' | 'NEW_MAZE';

export interface Message {
  type: MessageKey;
  data?: any;
}

export interface RenderInfo {
  color: string;
}

export interface Background {
  color: string;
  size: [number, number];
}

export interface Maze {
  graph: GraphMaze<Cell>;
  wallColor: string;
  bg: Background;
  endColor: string;
}

export interface Input {
  key: string;
  action: Action;
}

export interface Game {
  controls: Input[];
}

export interface Player {
  score: number;
  position: number;
  past_cells: number[];
  won: boolean;
}

export interface RenderToggles {
  'crumbs': boolean;
  'path': boolean;
  'score': boolean;
  'hint': boolean;
}

export interface State {
  bg: Background;
  maze: Maze;
  game: Game;
  messages: Message[];
  previousTime: number;
  elapsedTime: number;
  player: Player;
  render: RenderToggles;
  totalTime: number;
}

export const initialState: State = {
  bg: {
    color: 'black',
    size: [500, 500],
  },
  game: {
    controls: [
      { key: 'ArrowUp', action: 'UP' },
      { key: 'ArrowDown', action: 'DOWN' },
      { key: 'ArrowRight', action: 'RIGHT' },
      { key: 'ArrowLeft', action: 'LEFT' },

      { key: 'w', action: 'UP' },
      { key: 's', action: 'DOWN' },
      { key: 'd', action: 'RIGHT' },
      { key: 'a', action: 'LEFT' },

      { key: 'i', action: 'UP' },
      { key: 'k', action: 'DOWN' },
      { key: 'l', action: 'RIGHT' },
      { key: 'j', action: 'LEFT' },

      { key: 'h', action: 'TOGGLE_HINT' },
      { key: 'b', action: 'TOGGLE_CRUMBS' },
      { key: 'p', action: 'TOGGLE_PATH' },
      { key: 'y', action: 'TOGGLE_SCORE' },
    ],
  },
  maze: {
    graph: genConnectedGraph(5) as any,
    bg: {
      color: 'blue',
      size: [400, 400],
    },
    wallColor: 'red',
    endColor: 'green',
  },
  messages: [
    { type: 'NEW_MAZE', data: [5, 5] },
  ],
  previousTime: 0,
  elapsedTime: 0,
  totalTime: 0,
  player: {
    score: 0,
    position: 0,
    past_cells: [],
    won: false,
  },
  render: {
    crumbs: false,
    path: false,
    score: true,
    hint: false,
  },
};

const move = (mFunc: typeof getLeft) => (state: State) => {
  const n = getMazeDimension(state.maze);
  const next = mFunc(n, n)(state.player.position);
  if (next === null)
    return state;

  if (!isConnected(state.maze.graph)(state.player.position, next))
    return state;

  const previous = state.player.past_cells[state.player.past_cells.length - 2] === next
                    ? state.player.past_cells : [ ...state.player.past_cells, next];
  return {
    ...state,
    player: {
      ...state.player,
      position: next,
      past_cells: previous,
      won: state.player.won || next === state.maze.graph.nodes.length - 1,
    },
  };
};

const newMaze = (state: State) => ([width, height]: [number, number]): State => {
  const cells = genCellList(width, height);
  const walls = genWallList(width, height);
  const maze = randomKruskal(cells, walls);

  return {
    ...state,
    totalTime: 0,
    player: {
      ...initialState.player,
    },
    maze: {
      ...state.maze,
      graph: maze,
    },
  };
};

const resize = (state: State) => ([width, height]: [number, number]): State => ({
    ...state,
    bg: {
      ...state.bg,
      size: [width, height],
    },
    maze: {
      ...state.maze,
      bg: {
        ...state.maze.bg,
        size: [width - width * (10 / 100), height - height * (10 / 100)],
      },
    },
});

const toggle = (type: 'path' | 'crumbs' | 'score' | 'hint') => (state: State): State => ({
  ...state,
  render: {
    ...state.render,
    [type]: !state.render[type],
  },
});

const updateMessage = (state: State) => (message: Message) => {
  switch (message.type) {
    case 'LEFT': return move(getLeft)(state);
    case 'RIGHT': return move(getRight)(state);
    case 'UP': return move(getUp)(state);
    case 'DOWN': return move(getDown)(state);
    case 'TOGGLE_CRUMBS': return toggle('crumbs')(state);
    case 'TOGGLE_PATH': return toggle('path')(state);
    case 'TOGGLE_SCORE': return toggle('score')(state);
    case 'TOGGLE_HINT': return toggle('hint')(state);
    case 'RESIZE': return resize(state)(message.data);
    case 'NEW_MAZE': return newMaze(state)(message.data);
  }
};

const applyMessages = (messages: Message[], state: State): State => {
  if (messages.length === 0) return state;
  const [x, ...rest] = messages;
  console.log('got message', x);
  return applyMessages(rest, updateMessage(state)(x));
};

export const update = (state: State): State => {
  // console.log(state.player.position);
  const newState = applyMessages(state.messages, state);
  return {
    ...newState,
    totalTime: state.player.won ? state.totalTime : state.totalTime + state.elapsedTime,
    messages: [],
  };
};
