export const even = (n: number) => n % 2 === 0;
export const odd = (n: number) => !even(n);

export const getUnique = <T>(arr: T[]) => arr.filter((v, i, s) => s.indexOf(v) === i);

const randomInt2 = (n: number) => Math.floor(Math.random() * n);
export const randomInt = (n: number, m?: number) => {
  if (typeof m === 'undefined')
    return randomInt2(n);

  const min = Math.ceil(n);
  const max = Math.floor(m);
  return Math.floor(Math.random() * (max - min)) + min;
};

const range2 = (n: number): number[] => Array.apply(null, {length: n}).map(Number.call, Number);
export const range = (start: number, end?: number, step = 1) => {
  if (typeof end === 'undefined')
    return range2(start);

  if (end < start && step > 0)
    step = -step;

  const max = Math.max(start, end);
  const min = Math.min(start, end);

  return range2(Math.ceil((max - min) + 1) / Math.abs(step))
          .map(e => (e * step) + start);
};

export const shuffle = <T>(xs: T[]) => {
  const randomized = [...xs];
  for (let i = xs.length - 1; i > 0; i--) {
    const j = randomInt(0, i + 1);
    // es6 swap
    [randomized[i], randomized[j]] = [randomized[j], randomized[i]];
  }

  return randomized;
};

export function* pairwise<T>(xs: T[]) {
  for (let i = 1; i < xs.length; i++) {
    yield [xs[i - 1], xs[i]];
  }
}

// Uses Heap's algorithm
export function* permutations<T>(xs: T[], n?: number): IterableIterator<T[]> {
  if (typeof n === 'undefined') {
    n = xs.length;
  }

  if (n === 1) {
    yield xs;
  } else {
    for (let i = 0; i < n - 1; i++) {
      yield *permutations(xs, n - 1) as any;
      const j = even(n) ? i : 0;
      [xs[j], xs[n - 1]] = [xs[n - 1], xs[j]];
    }

    yield *permutations(xs, n - 1) as any;
  }
}
