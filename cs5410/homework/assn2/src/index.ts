import { genCellList, genWallList} from './gridMaze';
import { shuffle } from './util';
import { randomKruskal } from './maze';
import { render } from './render';
import { State, Action, MessageKey, Message, initialState, update } from './state';

const canvas = document.getElementById('canvas') as HTMLCanvasElement;
const ctx = canvas.getContext('2d')!;

let messages: Message[] = [];

const registerKeyHandlers = (state: State) => {
  const map: {[name: string]: boolean} = {};
  window.addEventListener('keydown', (event) => {
    if (event.defaultPrevented) {
      return;
    }

    map[event.key] = true;

    const pressedKeys = Object.keys(map).filter(k => map[k]);

    pressedKeys.forEach(k =>
      state.game.controls
              .filter(c => c.key === k)
              .forEach(c => messages.push({ type: c.action })));

    event.preventDefault();
  }, true);

  window.addEventListener('keyup', e => {
    map[e.key] = false;
  }, true);
};

const doResize = () => {
    const canvas = document.getElementById('canvas') as HTMLCanvasElement;
    const newSize = Math.min(canvas.offsetHeight, canvas.offsetWidth);
    canvas.width = newSize;
    canvas.height = newSize;
    messages.push({ type: 'RESIZE', data: [newSize, newSize]});
};

const registerResize = () => {
  window.addEventListener('resize', doResize);
};

const changeMazeSize = (n: number) => () => {
  messages.push({ type: 'NEW_MAZE', data: [n, n]});
  doResize();
};

const registerButtons = () => {
  document.getElementById('btn5')!.addEventListener('click', changeMazeSize(5));
  document.getElementById('btn10')!.addEventListener('click', changeMazeSize(10));
  document.getElementById('btn15')!.addEventListener('click', changeMazeSize(15));
  document.getElementById('btn20')!.addEventListener('click', changeMazeSize(20));
};

const processInput = (state: State): State => {
  const newState = {
    ...state,
    messages: [...messages, ...state.messages],
  };
  messages = [];
  return newState;
};

const gameLoop = (initialState: State) => (time: number) => {
  const currentTime = time;
  const elapsedTime = currentTime - initialState.previousTime;
  const state = { ...initialState, previousTime: currentTime, elapsedTime };

  const newState = render(ctx)( update( processInput(state) ) );

  requestAnimationFrame(gameLoop(newState));
};

registerKeyHandlers(initialState);
registerResize();
registerButtons();
doResize();
gameLoop(initialState)(performance.now());
