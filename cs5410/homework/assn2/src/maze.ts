import { shuffle, getUnique, permutations, pairwise } from './util';
import { Graph, genConnectedGraph, shortestPath } from './Graph';

export interface Wall {
  divides: [number, number];
}

export interface Cell {
  id: number;
}

interface WallCellMaze {
  cells: Cell[];
  walls: Wall[];
}

export type GraphMaze<T> = Graph<T> & { path: number[] };

export const findCellIndex = (cells: Cell[][]) => (cell: number) => {
  let inner = -1;
  let outer = -1;
  cells.forEach((v, i) => {
    inner = v.findIndex(c => c.id === cell);
    if (inner !== -1)
      outer = i;
  });

  return [outer, inner];
};

const mazeToGraph = (m: WallCellMaze): GraphMaze<Cell> => {
  const graph = genConnectedGraph(m.cells.length);
  m.walls.forEach(wall => {
    for (const p of permutations(wall.divides)) {
      for (const [i, j] of pairwise(p))
        graph.adjacency_matrix[i][j] = 0;
    }
  });

  const maze = {
    ...graph,
    nodes: m.cells.sort((a, b) => a.id < b.id ? -1 : a.id > b.id ? 1 : 0),
  };

  return {
    ...maze,
    path: shortestPath(maze)(0, m.cells.length - 1),
  };
};

export const randomKruskal = (cells: Cell[][], walls: Wall[]) => {
  const shuffled = shuffle(walls);
  let newCells = [...cells];

  const newWalls = shuffled.map(wall => {
    const indexes = wall.divides.map(d => findCellIndex(newCells)(d)[0]);
    // if the cells divided by this wall belong to distinct sets
    if (indexes.length === getUnique(indexes).length && indexes.every(i => i !== -1)) {
      // join the sets of the formerly divided cells
      newCells[indexes[0]] = indexes.map(i => newCells[i])
                                    .reduce((p, c) => [...p, ...c]);
      indexes.slice(1).forEach(i => newCells[i] = []);
      newCells = newCells.filter(c => c.length !== 0);

      // remove the current wall
      // if a wall doesn't partition anything, is it really a wall?
      return {...wall, divides: []};
    }

    return wall;
  }).filter(w => w.divides.length > 0) as Wall[];

  return mazeToGraph({
    // cells should be a single element array of arrays of numbers
    // [ number[] ]
    cells: newCells[0],
    walls: newWalls,
  });
};
