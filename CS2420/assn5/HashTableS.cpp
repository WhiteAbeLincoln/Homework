//
// Created by abe on 3/21/16.
//

#include <iostream>
#include "HashTableS.h"
#include "ComputeHelper.h"

int HashTableS::Hash(std::string zip) {
    int asciiSum = 0;
    for (char c : zip) {
        asciiSum += static_cast<int>(c);
    }

    return asciiSum % m;
}

void HashTableS::Insert(string zip, double lat, double lng) {
    int k = Hash(zip);
    Element *old = T[k];
    Element *newElem = new Element(zip, lat, lng, old);

    T[k] = newElem;
}

void HashTableS::Remove(string zip) {
    int k = Hash(zip);
    Element *toDelete = T[k];

    if (toDelete != NULL) {
        while (toDelete->key != zip && toDelete->next != NULL) {
            toDelete = toDelete->next;
        }

        Element *next = toDelete->next;

        delete toDelete;
        T[k] = next;
    }
}

bool HashTableS::Search(string zip) {
    int k = Hash(zip);
    Element *elem = T[k];

    if (elem != NULL) {
        while (elem->key != zip && elem->next != NULL) {
            elem = elem->next;
        }

        return elem->key == zip; // should always be true, unless we hit the elem->next != NULL condition
                                 // in which case zip doesn't exist in table
    }

    return false;
}

Element *HashTableS::Find(std::string zip) {
    int k = Hash(zip);
    Element *elem = T[k];

    if (elem != NULL) {
        while (elem->key != zip && elem->next != NULL) {
            elem = elem->next;
        }

        if (elem->key == zip) return elem;
    }

    return nullptr;
}

double HashTableS::ComputeDistance(string zip1, string zip2) {

    Element * elem1 = Find(zip1);
    Element * elem2 = Find(zip2);

    if (elem1 && elem2) {
        return ComputeHelper::haversine(elem1, elem2);
    }

    return -1; // can't ever have negative distance, so this is perfect for representing error
}

int HashTableS::CountTableEntry(int i) {
    int count = 0;

    Element *curr = T[i];

    if (curr != NULL) {
        count = 1;

        while (curr->next != NULL) {
            curr = curr->next;
            count++;
        }
    }

    return count;
}

void HashTableS::PrintTableEntry() {
    for (int i = 0; i < m; i++) {
        Element *curr = T[i];

        if (curr != NULL) {
            std::cout << curr->key << ", " << curr->lat << ", " << curr->lng << endl;

            while (curr->next != NULL) {
                curr = curr->next;
                std::cout << curr->key << ", " << curr->lat << ", " << curr->lng << endl;
            }
        }
    }
}

HashTableS::HashTableS(int size) {
    m = size;
    T = new Element*[m];
    for (int i = 0; i < size; i++) {
        T[i] = NULL;
    }
}
