#include "Element.h"
#include <string>

using namespace std;

Element::Element(string key_input, double lat, double lng, Element *next_input, elem_flag flag) {
    this->key = key_input;
    this->lat = lat;
    this->lng = lng;
    this->next = next_input;
    this->flag = flag;
}
