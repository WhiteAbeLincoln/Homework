//
// Created by abe on 3/21/16.
//

#include "HashTableQ.h"
#include "ComputeHelper.h"
#include <iostream>

using namespace std;

HashTableQ::HashTableQ(int size) {
    m = size;
    T = new Element[m];
    for (int i = 0; i < m; i++) {
        T[i] = Element();
    }
}

void HashTableQ::Insert(string zip, double lat, double lng) {
    int i = 0;
    while (i < m) {
        int k = QuadraticProbe(zip, i);
        if (T[k].flag != elem_flag::ACTIVE) {
            T[k] = Element(zip, lat, lng, NULL, elem_flag::ACTIVE);
            return;
        } else i++;
    }

    cout << "The table is full";
}

void HashTableQ::Remove(string zip) {
    int i = 0;
    while (i < m) {
        int k = QuadraticProbe(zip, i);
        if (T[k].flag == elem_flag::EMPTY) return;
        else if (T[k].key == zip) {
            T[k].flag = elem_flag::DELETED;     // We don't actually delete the values, that would be pointless.
                                                // By setting the flag, we never access this one again, and allow
                                                // overwriting if needed. (much like how operating systems delete files)
            return;
        } else i++;
    }
}

bool HashTableQ::Search(string zip) {
    int i = 0;
    while (i < m) {
        int k = QuadraticProbe(zip, i);
        if (T[k].flag == elem_flag::EMPTY) return false;
        else if (T[k].flag == elem_flag::ACTIVE && T[k].key == zip) {
            return true;
        } else i++;
    }
    return false;
}

Element *HashTableQ::Find(string zip) {
    int i = 0;
    while (i < m) {
        int k = QuadraticProbe(zip, i);
        if (T[k].flag == elem_flag::EMPTY) return nullptr;
        else if (T[k].flag == elem_flag::ACTIVE && T[k].key == zip) {
            return &T[k];
        } else i++;
    }
    return nullptr;
}

double HashTableQ::ComputeDistance(string zip1, string zip2) {

    Element * elem1 = Find(zip1);
    Element * elem2 = Find(zip2);

    if (elem1 && elem2) {
        return ComputeHelper::haversine(elem1, elem2);
    }

    return -1; // can't ever have negative distance, so this is perfect for representing error
}

void HashTableQ::PrintTableEntry() {
    for (int i = 0; i < m; i++) {
        Element curr = T[i];

        if (curr.flag == elem_flag::ACTIVE) {
            std::cout << curr.key << ", " << curr.lat << ", " << curr.lng << endl;
        }
    }
}

int HashTableQ::Hash(string zip) {
    int asciiSum = 0;
    for (char c : zip) {
        asciiSum += static_cast<int>(c);
    }

    return asciiSum % m;
}

int HashTableQ::QuadraticProbe(string zip, int i) {
    return (Hash(zip) + i*i) % m;
}

