//
// Created by abe on 3/21/16.
//

#include "ComputeHelper.h"
#include <cmath>

double ComputeHelper::haversine(Element *elem1, Element *elem2) {
    double medianLat = (get_radians(elem2->lat) - get_radians(elem1->lat)) / 2;
    double medianLng = (get_radians(elem2->lng) - get_radians(elem1->lng)) / 2;

    double innerRoot = pow(sin(medianLat), 2) + cos(get_radians(elem1->lat)) * cos(get_radians(elem2->lat)) * pow(sin(medianLng), 2);

    double d = EARTH_RADIUS * 2 * asin(sqrt(innerRoot));

    return d;
}

double ComputeHelper::get_radians(double degree) {
    return degree * (M_PI / 180);
}
