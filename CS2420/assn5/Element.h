#ifndef ELEMENT_H
#define ELEMENT_H
#include <string>

enum elem_flag {
    EMPTY,
    ACTIVE,
    DELETED
};

class Element {
    public:
        elem_flag flag;
        std::string key;
        double lat;
        double lng;
        Element *next;
        Element(std::string key_input="", double lat=0.0, double lng=0.0, Element *next_input=NULL, elem_flag flag=elem_flag::EMPTY);
};

#endif
