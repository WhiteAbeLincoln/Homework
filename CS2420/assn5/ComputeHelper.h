//
// Created by abe on 3/21/16.
//

#ifndef COMPUTEHELPER_H
#define COMPUTEHELPER_H


#include "Element.h"

class ComputeHelper {
private:
    ComputeHelper() {}; // don't allow this class to initialize, since it is static
    const static int EARTH_RADIUS = 6373/*KM*/;
public:
    static double haversine(Element * elem1, Element * elem2);
    static double get_radians(double degree);
};


#endif //ASSN5_COMPUTEHELPER_H
