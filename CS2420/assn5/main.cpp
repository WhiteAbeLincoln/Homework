//
// Created by abe on 3/21/16.
//

#include "HashTableS.h"
#include "HashTableQ.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

using namespace std;


float l_factors[] = {0.3, 0.4, 0.5, 2};
const string SEARCHZIP_PATH = "searchzip.txt";
const string ZIPCODES_PATH = "zipcodes.csv";
const string VALIDZIP_PATH = "validzip.txt";

// split function taken from http://stackoverflow.com/a/236803
// uses stringstream and getline to split by arbitrary token
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

template <typename T>
T demo_public() {
    T tableS = T(11);
    tableS.Insert("pizza", 12.95, 10.3);
    tableS.Insert("chocolate", 14.21, 12.4);
    tableS.Insert("pizza1", 12.95, 10.3);
    tableS.Insert("choco1", 14.21, 12.4);
    tableS.Insert("lasagna", 12.95, 10.3);
    tableS.Insert("choco2", 14.21, 12.4);
    tableS.Insert("pie", 324.04, 210.13);
    tableS.Insert("raspberry", 18.38, -67.18);
    tableS.Insert("cake", 18.16, -66.72);

    cout << "Printing Hash Table entries\n"
         << "---------------------------\n";
    tableS.PrintTableEntry();
    cout << "\n";

    printf("Table contains pie: %i\n", tableS.Search("pie"));

    cout << "Deleting pie" << endl;
    tableS.Remove("pie");

    printf("Table contains pie: %i\n", tableS.Search("pie"));
    printf("Distance between raspberry and cake: %f.4 km\n", tableS.ComputeDistance("raspberry", "cake"));

    return tableS;
}

template <typename T>
T construct_table(int size) {
    T table = T(size);

    ifstream fin;
    fin.open(ZIPCODES_PATH);

    string temp = "";
    while(getline(fin, temp)) {
        vector<string> v1 = split(temp, ',');
        // inserts into table using zip as key
        table.Insert(v1[0], stod(v1[1]), stod(v1[2]));
    }

    fin.close();

    return table;
}

vector<int> calc_size(int numElems) {
    vector<int> sizes;
    for(int i = 0; i < 4; i++) {
       sizes.push_back((int)(numElems/l_factors[i]));
    }

    return sizes;
}

double avg_length(int size, HashTableS table) {
    double sum = 0;
    for (int i = 0; i < size; i++) {
        sum += table.CountTableEntry(i);
    }

    return sum/size;
}

template <typename T>
string read_searchzip(T table, string append) {
    string savePath = append.append(VALIDZIP_PATH);
    ifstream fin;
    fin.open(SEARCHZIP_PATH);

    ofstream fout;
    fout.open(savePath);

    string temp;
    while(getline(fin, temp)) {
        if (table.Search(temp)) {
            fout << temp << endl;
        }
    }

    fout.close();
    fin.close();

    return savePath;
}

template <typename T>
void compute_distance(T table, string file) {
    ifstream fin;
    fin.open(file);

    string odd;
    string even;
    int count = 0;

    string temp;
    while(getline(fin, temp)) {
        count++;
        if (count % 2 == 1)
            odd = temp;
        else {
            even = temp;
            table.ComputeDistance(odd, even);
            // cout << table.ComputeDistance(odd, even) << endl;
        }
    }

    fin.close();
}

template <typename T>
T write_summary(int i, vector<int> sizes) {
    // print out the load factor and its corresponding hash table size
    printf("load factor: %.2f, hash size: %i\n", l_factors[i], sizes[i]);
    // create a hash table using computed hash table size
    T hashTable = construct_table<T>(sizes[i]);

    // calculate time to search
    string append = to_string(rand() % 100).append(to_string(l_factors[i]));
    clock_t start = clock();
        string file = read_searchzip<T>(hashTable, append);
    double duration = (clock() - start) / (double) CLOCKS_PER_SEC;
    printf("search time: %f\n", duration);

    // calculate time to compute distance
    start = clock();
        compute_distance<T>(hashTable, file);
    duration = (clock() - start) / (double) CLOCKS_PER_SEC;
    printf("compute distance time: %f\n", duration);

    return hashTable;
}

int main(int argc, char** argv) {
    /***********************
     * H A S H T A B L E S *
     ***********************/
    HashTableS tableS = demo_public<HashTableS>();
    printf("Elements at index 7: %i\n", tableS.CountTableEntry(7));
    cout << "\n\n";


    /***********************
     * H A S H T A B L E Q *
     ***********************/
    demo_public<HashTableQ>();
    cout << "\n\n";



    ifstream fin;
    fin.open(ZIPCODES_PATH);

    if (fin.fail()) {
        cout << "error opening zipcodes.csv" << endl;
        return 500; // 500 internal server error
    }

    // 2. Read zipcodes.csv file to know the total number of entries
    string temp = "";
    int count = 0;
    while (getline(fin, temp)) {
        count++;
    }

    // 3. Choose 4 sizes for HashTableS
    // calculates load factor - this should stay the same if the number of entries in csv is the same,
    // so I only have to calculate once
    vector<int> sizes = calc_size(count);


    cout << "HashTableS summary\n"
         << "------------------\n";
    for (int i = 0; i < 4; i++) {
        HashTableS hashS = write_summary<HashTableS>(i, sizes);
        printf("avg length: %.2f\n", avg_length(sizes[i], hashS));
        cout << "-----------" << endl;
    }


    cout << "\n\n";

    cout << "HashTableQ summary\n"
         << "------------------\n";
    for (int i = 0; i < 3; i++) {
        write_summary<HashTableQ>(i, sizes);
        cout << "-----------" << endl;
    }

    cout <<"\n\nI recommend using HashTableS" << endl;
    return 0;
}
