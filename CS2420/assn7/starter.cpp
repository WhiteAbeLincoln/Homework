#include<iostream>
#include<fstream>
#include<string>
#include "MaxHeap.h"
using namespace std;

// Function protototypes

// helper functions
void copy_array(int*,int*,int,int);
void merge(int*,int,int,int);
int partition(int*,int,int);

// Read the file and return the number of elements in the file which contains all integers
// Make sure to close the file before return the number of elements in the file
int FindNumberofElements1(const string & fileName) ;

// Sequnetially read the data in the file and load them one-by-one in arrayPtr
// Make sure to close the file after reading the data
void GetInputfromFile1(int *arrayPtr, const string & fileName)  ;

// Read the file and return the number of elements in the file which contains all words
// Make sure to close the file before return the number of elements in the file
int FindNumberofElements2(const string & fileName) ;

// Sequnetially read the data in the file
// Convert the data into nonnegative integers using the strategy mentioned in assignment 5
// Load nonnegative integers one-by-one in arrayPtr
// Make sure to close the file after reading the data
void GetInputfromFile2(int *arrayPtr, const string & fileName)  ;

// Perform various sorting algorithms to sort the data in the ascending order
void HeapSort(int *arrayPtr, int size) ;
void MergeSort(int *arrayPtr, int low, int high) ;
void QuickSort(int *arrayPtr, int low, int high) ;

// Other function prototypes go here


int main()
{
	int *A ;
	int n ; // the number of elements stored in A
	string inputFileName1 = "Assign7Data.txt" ; // the name of the input file 
		
	n = FindNumberofElements1(inputFileName1) ;
	cout << "the number of data in the input file is " << n << endl ;

	// dynamically allocate n spaces in A to hold the n numbers
	// Add your code here ... ... 
    A = new int[n];
	

	// read the numbers from the input file to array A 
	GetInputfromFile1(A, inputFileName1) ; 

    // call the heap sort procedure
	HeapSort(A, n) ; 

    // print the numbers of A out on the screen/console 
	for (int i = 0 ; i < n ; i++)
		cout << A[i] << "  " ; 

	cout << endl ; 

	// read the numbers from the input file to array A 
	GetInputfromFile1(A, inputFileName1) ; 

    // call the merge sort procedure
	MergeSort(A, 0, n-1) ; 

    // print the numbers of A out on the screen/console 
	for (int i = 0 ; i < n ; i++)
		cout << A[i] << "  " ; 

	cout << endl ; 


	// read the numbers from the input file to array A 
	GetInputfromFile1(A, inputFileName1) ; 

    // call the quick sort procedure
	QuickSort(A, 0, n-1) ; 

    // print the numbers of A out on the screen/console 
	for (int i = 0 ; i < n ; i++)
		cout << A[i] << "  " ; 

	cout << endl ; 

	delete [] A ;
	A = NULL ;

	cout << "*******************************************************************************************" << endl ;
	cout << "*******************************************************************************************" << endl ;


	string inputFileName2 = "dictionary.txt" ; // the name of the input file

	n = FindNumberofElements2(inputFileName2) ;
	cout << "the number of data in the input file is " << n << endl ;

	// dynamically allocate n spaces in A to hold the n numbers
	// Add your code here ... ... 
    A = new int[n];	

	// read the numbers from the input file to array A 
	GetInputfromFile2(A, inputFileName2) ; 

    // call the heap sort procedure
	HeapSort(A, n) ; 

    // print the numbers of A out on the screen/console 
	for (int i = 0 ; i < n ; i++)
		cout << A[i] << "  " ; 

	cout << endl ; 

	// read the numbers from the input file to array A 
	GetInputfromFile2(A, inputFileName2) ; 

    // call the merge sort procedure
	MergeSort(A, 0, n-1) ; 

    // print the numbers of A out on the screen/console 
	for (int i = 0 ; i < n ; i++)
		cout << A[i] << "  " ; 

	cout << endl ; 


	// read the numbers from the input file to array A 
	GetInputfromFile2(A, inputFileName2) ; 

    // call the quick sort procedure
	QuickSort(A, 0, n-1) ; 

    // print the numbers of A out on the screen/console 
	for (int i = 0 ; i < n ; i++)
		cout << A[i] << "  " ; 

	cout << endl ; 

	delete [] A ;
	A = NULL ;
	
	system("pause") ;

	return 0;
}

// Add your code for each function

// Read the file and return the number of elements in the file containing integers
int FindNumberofElements1(const string & fileName) 
{
    std::ifstream fin(fileName);
    int temp;
    int i;

    for (i = 0; fin >> temp; ++i);
    fin.close();
    return i;
}


// Sequnetially read the data in the file and load them one-by-one in arrayPtr
void GetInputfromFile1(int *arrayPtr, const string & fileName) 
{
    std::ifstream fin(fileName);

    int i = 0;
    int temp = 0;
    while (fin >> temp) {
        arrayPtr[i] = temp;
        i++;
    }

    fin.close();
}

// Read the file and return the number of elements in the file containing words
int FindNumberofElements2(const string & fileName) 
{
    std::ifstream fin(fileName);
    std::string temp;
    int i;

    for (i = 0; std::getline(fin, temp); ++i);
    fin.close();
    return i;
}


// Sequnetially read the data in the file, convert them to integers, and load integers one-by-one in arrayPtr
void GetInputfromFile2(int *arrayPtr, const string & fileName) 
{
    std::ifstream fin(fileName);
    std::string temp;

    for (int i = 0; std::getline(fin, temp); ++i) {
        int tot = 0;
        for (unsigned j = 0; j < temp.size(); j++) {
            tot += temp[j];
        }
        arrayPtr[i] = tot;
    }
    fin.close();
}

// Perform heap sort and sort the data in ascending order
void HeapSort(int *arrayPtr, int size)
{
    MaxHeap<int> heap(arrayPtr, size, size+1);
    copy_array(arrayPtr, heap.HeapSort(false), size, size);
}

// Perform merge sort and sort the data in ascending order
void MergeSort(int *arrayPtr, int low, int high)
{
    if (low >= high)
        return;
    else {
        int mid = low + (high-low)/2; // avoids overlflow for large low, high
        MergeSort(arrayPtr, low, mid);
        MergeSort(arrayPtr, mid+1, high);
        merge(arrayPtr, low, mid, high);

    }
}

// Perform quick sort and sort the data in ascending order
void QuickSort(int *arrayPtr, int low, int high)
{
    if (low < high) {
        int p = partition(arrayPtr, low, high);

        QuickSort(arrayPtr, low, p - 1);
        QuickSort(arrayPtr, p+1, high);
    }
}


// Implementation of other functions

// merge helper function
void merge(int *arr, int low, int mid, int high)
{
    int idx_b, idx_c, count;
    int len_b = mid - low + 1;
    int len_c =  high - mid;
 
    int B[len_b];
    int C[len_c];
 
    // fill the temp arrays
    for (int i = 0; i < len_b; i++)
        B[i] = arr[low + i];
    for (int j = 0; j < len_c; j++)
        C[j] = arr[mid + 1+ j];
 
    idx_b = 0;
    idx_c = 0;
    count = low;
    while (idx_b < len_b && idx_c < len_c)
    {
        if (B[idx_b] <= C[idx_c])
        {
            arr[count] = B[idx_b];
            idx_b++;
        }
        else
        {
            arr[count] = C[idx_c];
            idx_c++;
        }
        count++;
    }
 
    // copy the rest of B to A
    while (idx_b < len_b)
    {
        arr[count] = B[idx_b];
        idx_b++;
        count++;
    }
 
    // copy the rest of C to A
    while (idx_c < len_c)
    {
        arr[count] = C[idx_c];
        idx_c++;
        count++;
    }
}

void copy_array(int *A, int *B, int end_a, int end_b) {
    for (int i=0, j=0; i<end_a && j<end_b; i++, j++) {
        A[i] = B[i];
    }
}

// Lomuto partition scheme - last element
int partition(int* A, int low, int high) {
    int pivot = A[high];
    int i = low;

    for (int j = low; j < high; j++) {
        if (A[j] < pivot) {
            int temp = A[j];
            A[j] = A[i];
            A[i] = temp;

            i++;
        }
    }

    int temp = A[i];
    A[i] = A[high];
    A[high] = temp;

    return i;
}
