#ifndef ASSN6_MAXHEAP_H_
#define ASSN6_MAXHEAP_H_

template <typename T>
class MaxHeap
{
    private:
        int arraySize ;
        // the size of the array, index from 0 to array_size-1
        int heapSize ;
        // number of elements in the heap; heapSize is smaller than arraySize
        T * H ;
        // elements of heap are in H[1]...H[heapSize], cell at index 0 is not used
        int Left(int i) ;
        // return the index of the left child of node i
        int Right(int i) ; // return the index of the right child of node i
        int Parent(int i) ; // return the index of the parent of node i
        void PercolateDown(int) ; // DownHeap method. It will be called in MaxHeap and DeleteMax
        void PercolateDown(T*,int,int);
        int recurse_print_tree(int elem, bool is_left, int offset, int depth, char s[20][255]);
        T * build_array(int n, int idx, int elem, T* (arr));
        void print_array(T*,int);
    public:
        MaxHeap(int arraySize=30) ; // Generate an empty heap with the default array size of 30.
        MaxHeap(T *A, int heapSize, int arraySize) ; // A contains a sequence of elements
        ~MaxHeap() ;
        void Insert(const T &a) ; // Insert a new element containing word and its weight
        T DeleteMax(); // Find, return, and remove the element with the maximum weight
        void PrintHeap(); // Print the heap in tree structure; each node containing word and weight
        void Merge(const MaxHeap &newHeap) ; // Merge with another heap to form a larger heap
        T * FindTopMatches(int count) ; // return top “count” matching words based on weights
        T * HeapSort(bool print);
        T * GetArray(T*);
};

#endif // ASSN6_MAXHEAP_H_
