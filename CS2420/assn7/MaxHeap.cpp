#include <iostream>
#include "MaxHeap.h"

template <typename T>
int MaxHeap<T>::Left(int i) {
    return 2*i;
}

template <typename T>
int MaxHeap<T>::Right(int i) {
    return 2*i + 1;
}

template <typename T>
int MaxHeap<T>::Parent(int i) {
    return i / 2;
}

template <typename T>
void MaxHeap<T>::PercolateDown(int i) {
    int left = Left(i);
    int right = Right(i);
    int largest = i;

    if (left <= heapSize && H[left] > H[largest]) {
        largest = left;
    }

    if (right <= heapSize && H[right] > H[largest]) {
        largest = right;
    }

    if (largest != i) {
        T temp = H[i];
        H[i] = H[largest];
        H[largest] = temp;

        PercolateDown(largest);
    }
}

template <typename T>
void MaxHeap<T>::PercolateDown(T* a, int start, int end) {
    int root = start;

    while (Left(root) <= end) {
        int child = Left(root);
        int swap = root;

        if (a[swap] < a[child])
            swap = child;

        if (child+1 <= end && a[swap] < a[child+1])
            swap = child+1;

        if (swap == root)
            return;
        else {
            T temp = a[root];
            a[root] = a[swap];
            a[swap] = temp;

            root = swap;
        }
    }
}

template <typename T>
MaxHeap<T>::MaxHeap(int arraySize) {
    this->arraySize = arraySize;
    this->heapSize = 0;
    H = new T[arraySize];
}

template <typename T>
MaxHeap<T>::MaxHeap(T *A, int heapSize, int arraySize) {
    this->arraySize = arraySize;
    this->heapSize = heapSize;
    H = new T[arraySize];
    for (int i = 1; i <= heapSize; i++) {
        H[i] = A[i-1];
    }

    for (int i = heapSize / 2; i > 0; i--) {
        PercolateDown(i);
    }
}

template <typename T>
MaxHeap<T>::~MaxHeap() {
    delete[] H;
}

template <typename T>
void MaxHeap<T>::Insert(const T &a) {
    heapSize++;
    H[heapSize] = a;
    int i = heapSize;

    while (i > 1 && a > H[Parent(i)]) {
        T temp = H[i];
        H[i] = H[Parent(i)];
        H[Parent(i)] = temp;
        i = Parent(i);
    }

    H[i] = a;

    /*for (int i = 1; i <= heapSize; i++) {
        std::cout << H[i].value << " ";
    }
    std::cout << "\n";*/
}

template <typename T>
T MaxHeap<T>::DeleteMax() {
    T temp = H[1];
    H[1] = H[heapSize];
    heapSize--;
    PercolateDown(1);
    return temp;
}

// Max depth of tree is 20
// this print function struggles when the output becomes too wide
// TODO: refactor to use less horizontal space
template <typename T>
void MaxHeap<T>::PrintHeap() {
    // we build the outputted string in this character array
    char string_builder[20][255];
    for (int i = 0; i < 20; i++)
        sprintf(string_builder[i], "%80s", " ");

    recurse_print_tree(1, 0, 0, 0, string_builder);

    for (int i = 0; i < 20; i++) {
        if (std::string(string_builder[i]).find_first_not_of(' ') != std::string::npos)
            printf("%s\n", string_builder[i]);
    }
    printf("\n\n");
}

// implementation of http://stackoverflow.com/a/13755911 modified for heaps and commented
template <typename T>
int MaxHeap<T>::recurse_print_tree(int elem, bool is_left, int offset, int depth, char s[20][255]) {
    char b[20];
    int width = 8;

    // our exit condition - if the index overflows the heapSize, return
    if (elem > heapSize) return 0;

    // visualization of a node ' (node_value) '
    sprintf(b, "[%6s]", (std::to_string(H[elem])).c_str());

    // recurses through tree, getting the total offset of the left branch
    int left = recurse_print_tree(Left(elem), true, offset, depth + 1, s);
    // gets the offset from the right branch, using left offset and width as the minimum separator
    int right = recurse_print_tree(Right(elem), false, offset + left + width, depth + 1, s);

    // stores the node visualization in our built string
    for (int i = 0; i < width; i++)
        s[depth][offset + left + i] = b[i];

    if (depth && is_left) {
        // builds the edges between nodes for the left branch
        for (int i = 0; i < width + right; i++)
            s[depth - 1][offset + left + width/2 + i] = '-';

        // connector character between node and edge
        s[depth - 1][offset + left + width/2] = '.';
    } else if (depth && ! is_left) {

        // builds the edges for the right branch
        for (int i = 0; i < width + right; i++)
            s[depth - 1][offset - width/2 + i] = '-';

        s[depth - 1][offset + left + width/2] = '.';
    }

    return left + width + right;
}

template <typename T>
void MaxHeap<T>::Merge(const MaxHeap &newHeap) {
    int tot_arr_size = arraySize + newHeap.arraySize;
    int tot_heap_size = heapSize + newHeap.heapSize;

    T * old = H;
    H = new T[tot_arr_size];

    for (int i = 1; i <= heapSize; i++) {
        H[i] = old[i];
    }

    for (int i = 1; i <= newHeap.heapSize; i++) {
        H[heapSize + i] = newHeap.H[i];
    }

    heapSize = tot_heap_size;
    arraySize = tot_arr_size;

    // Run our BuildHeap algorithm
    for (int i = heapSize / 2; i > 0; i--) {
        PercolateDown(i);
    }

    delete[] old;
}

/*T * MaxHeap::build_array(int n, int idx, int elem, T* (arr)) {
    if (idx <= n) {
        arr = build_array(n, idx+1, Left(elem), arr);
        arr[idx] = H[elem];
        arr = build_array(n, idx+2, Right(elem), arr);
    }
    return arr;
}*/

template <typename T>
T * MaxHeap<T>::FindTopMatches(int n) {
    // since heaps are already partially sorted, just return the first n elements
    T * returned = new T[n];
    for (int i = 0; i < heapSize && i < n; i++) {
        returned[i] = H[i+1];
    }

    return returned;
   // return build_array(n, 0, 1, returned);
}

template <typename T>
void MaxHeap<T>::print_array(T* a, int size) {
    std::cout << "[";
    for (int i = 1; i <= size; i++)
        std::cout << a[i] << " ";
    std::cout << "]\n";
}

template <typename T>
T * MaxHeap<T>::HeapSort(bool print) {
    T* old = this->H;

    int end = this->arraySize - 1;
    while (end > 1) {
        T temp = old[end];
        old[end] = old[1];
        old[1] = temp;

        end = end - 1;

        if (print) {
            std::cout << "\tswap: ";
            print_array(old, this->heapSize);
        }

        PercolateDown(old, 1, end);

        if (print) {
            std::cout << "downheap: ";
            print_array(old, this->heapSize);
        }
    }

    return GetArray(old);
}

template <typename T>
T * MaxHeap<T>::GetArray(T* arr) {
    T * newArr = new T[this->heapSize];

    for (int i = 1; i <= this->heapSize; i++) {
        newArr[i-1] = arr[i];
    }

    return newArr;
}

template class MaxHeap<int>;
