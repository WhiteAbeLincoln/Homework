#include <stdio.h>
#include "MaxHeap.h"

void print_array(int* array, int size) {
    printf("[");
    for (int i = 0; i < size; i++) {
        printf("%d ", array[i]);
    }
    printf("]\n");
}

void sort_bubble(int* array, int size) {
    int temp;
    bool swapped;
    int tot_size = size;

    do {
        swapped = false;
        for (int count = 0; count < (size-1); count++) {
            if (array[count]>array[count+1]) {
                temp = array[count];
                array[count] = array[count+1];
                array[count+1] = temp;
                swapped = true;
            }
        }
        size--;
        print_array(array, tot_size);
    } while (swapped);
}

void sort_select(int* array, int size) {
    int start_scan, min_idx, min_val;

    for (start_scan=0; start_scan < (size-1); start_scan++) {
        min_idx = start_scan;
        min_val = array[start_scan];
        for (int i = start_scan+1; i < size; i++) {
            if (array[i] < min_val) {
                min_val = array[i];
                min_idx = i;
            }
        }
        array[min_idx] = array[start_scan];
        array[start_scan] = min_val;

        print_array(array, size);
    }
}

void sort_insert(int* array, int size) {
    int j, temp;
    for (int i = 1; i <= size-1; i++) {
        temp = array[i];
        j = i - 1;
        while (j >= 0 && temp < array[j]) {
            array[j+1] = array[j];
            j--;
        }
        array[j+1] = temp;

        print_array(array, size);
    }
}

void sort_heap(int* array, int size) {
    MaxHeap<int> heap(array, size, size+1);
    print_array(heap.HeapSort(true), size);
}

int main() {
    printf("bubble\n");
    int a[] = {17, 23, 5, 11, 2};
    sort_bubble(a, 5);
    printf("\n");

    printf("selection\n");
    int b[] = {17, 23, 5, 11, 2};
    sort_select(b, 5);
    printf("\n");

    printf("insertion\n");
    int c[] = {17, 23, 5, 11, 2};
    sort_insert(c, 5);
    printf("\n");

    printf("heap\n");
    int d[] = {17, 23, 5, 11, 2};
    sort_heap(d, 5);
    printf("\n");
    return 0;
}
