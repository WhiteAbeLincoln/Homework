#include "Element.h"

Element::Element(std::string value, int weight) {
    this->weight = weight;
    this->value = value;
}

Element::Element() {
    this->weight = 0;
    this->value = "";
}
