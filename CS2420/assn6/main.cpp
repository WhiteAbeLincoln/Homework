#include "MaxHeap.h"
#include "Element.h"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

// Helper functions, since C++'s stl library is crap
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

std::string trim(std::string& str)
{
    size_t first = str.find_first_not_of(' ');
    if (first == std::string::npos)
        return "";
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last-first+1));
}

void demo() {
    MaxHeap heap(11);
    Element a ("blaasdf", 0);
    heap.Insert(a);
    heap.PrintHeap();
    for (int i = 1; i < 7; i++) {
        Element b (std::to_string(i), i);
        heap.Insert(b);
        heap.PrintHeap();
    }

    heap.DeleteMax();
    printf("Deleting max\n");
    heap.PrintHeap();
    heap.DeleteMax();
    printf("Deleting max\n");
    heap.PrintHeap();


    printf("printing top matches\n");
    Element * elements = heap.FindTopMatches(5);
    for (int i = 0; i < 5; i++) {
        printf("%s ", elements[i].value.c_str());
    }
    printf("\n");

    Element * elArray = new Element[6];
    for (int i = 0; i < 5; i++) {
        std::string a = "el" + std::to_string(i);
        Element el0 = Element(a, 12+i);
        elArray[i] = el0;
    }

    MaxHeap heap2(elArray, 5, 6);
    printf("printing heap2\n");
    heap2.PrintHeap();

    heap2.DeleteMax();
    printf("Deleting max from heap2\n");
    heap2.PrintHeap();

    heap2.DeleteMax();
    printf("Deleting max from heap2\n");
    heap2.PrintHeap();

    printf("printing top matches for heap2\n");
    elements = heap2.FindTopMatches(5);
    for (int i = 0; i < 5; i++) {
        printf("%s ", elements[i].value.c_str());
    }
    printf("\n");

    printf("merging heaps\n");
    heap.Merge(heap2);
    heap.PrintHeap();

    printf("printing top matches for merged heap\n");
    elements = heap.FindTopMatches(5);
    for (int i = 0; i < 5; i++) {
        printf("%s ", elements[i].value.c_str());
    }
    printf("\n");
}

int r_binary_search(Element arr[], std::string target, int first, int last) {
    int str_len = target.length();
    if (last < first)
        return -1;
    else {
        int mid = (first + last) / 2;

        if (arr[mid].value.substr(0, str_len) == target)
            return mid;

        if (arr[mid].value.substr(0, str_len) < target)
            return r_binary_search(arr, target, mid+1, last);
        else
            return r_binary_search(arr, target, first, mid - 1);

    }
}

int BinarySearch(Element a[], int arraySize, std::string target) {
    return r_binary_search(a, target, 0, arraySize - 1);
}

int FindFirstIndex(Element a[], int arraySize, int foundIndex, std::string target) {
    int str_len = target.length();
    int h = foundIndex;
    int prev_match = foundIndex;
    int l = 0;

    while (h >= l) {
        // calculate midpoint
        int m = (l + h) / 2;
        if (a[m].value.substr(0, str_len) == target) {
            if (m > 0 && a[m-1].value.substr(0, str_len) == target) {
                // if we haven't reached the beginning, and there is a value to the left of our found index that matches,
                // we haven't found the first occurance
                prev_match = m;
                h = m-1;
            } else // we found the first instance where there isn't a matching one before it
                return m;
        } else if (a[m].value.substr(0, str_len) < target) {
            // if we have moved behind our target - current comes alphebetically before
            l = m;
            h = prev_match;
        } else
            h = m - 1;
    }

    return -1;
}

int FindLastIndex(Element a[], int arraySize, int foundIndex, std::string target) {
    int str_len = target.length();
    int h = arraySize-1; // high bound
    int l = foundIndex; // low bound
    int prev_match = foundIndex;

    while (l <= h) {
        int m = (l+h) / 2;

        if (a[m].value.substr(0, str_len) == target) {
            if (m < arraySize-1 && a[m+1].value.substr(0, str_len) == target) {
                // if we haven't reached the end of the arrya, and there is a value to the right that matches,
                // we haven't found the first occurance
                prev_match = m;
                l = m+1;
            } else // we found the first instance where there isn't a matching one after it
                return m;
        } else if (a[m].value.substr(0, str_len) > target)  {
            // we have moved past our target - current comes alphabetically after
            h = m; // set our high bound to be the current
            l = prev_match; // set the low bound to be the last known valid
        } else
            // we haven't moved past the target, but we haven't seen the target yet - it must be to the right
            l = m + 1; // increment the lower bound
    }

    return -1;
}

std::vector<std::string> get_prefixes() {
    std::string prefix;
    std::string matches;
    printf("Please input prefixes: ");
    std::getline(std::cin, prefix);
    printf("Please input number of matches: ");
    std::getline(std::cin, matches);

    std::vector<std::string> prefixes = split(prefix, '&');

    for (int i = 0; i < (int)prefixes.size(); i++) {
        // trim the possible whitespace
        prefixes[i] = trim(prefixes[i]);
    }
    prefixes.push_back(matches);

    return prefixes;
}

void do_main(std::vector<std::string> prefixes, Element* array, int array_size) {
    int prefix_length = prefixes.size() - 1;
    int num_matches = std::stoi(prefixes[prefix_length]);


    MaxHeap * heap_arr = new MaxHeap[prefix_length];
    for (int i = 0; i < prefix_length; i++) {
        int found_idx = BinarySearch(array, array_size, prefixes[i]);
        int first = FindFirstIndex(array, array_size, found_idx, prefixes[i]);
        int last = FindLastIndex(array, array_size, found_idx, prefixes[i]);

        Element * small_array = new Element[last-first+1];
        for (int k = first, j = 0; k <= last; k++, j++) {
            small_array[j] = array[k];
        }

        MaxHeap * heap = new MaxHeap(small_array, last-first+1, last-first+2);
        heap_arr[i] = (*heap);
    }

    if (prefix_length > 1) {
        for (int i = prefix_length-1; i > 0; i--) {
            heap_arr[i-1].Merge(heap_arr[i]);
        }
    }

    Element * out_arr = heap_arr[0].FindTopMatches(num_matches);
    for (int i = 0; i < num_matches; i++) {
        printf("\t%s\n", out_arr[i].value.c_str());
    }

}

int main(int argc, char** argv) {
    demo();
    printf("\n\n=============================\n");

    std::ifstream fin("SortedWords.txt");

    std::string temp;
    std::getline(fin, temp);

    int num_entries = std::stoi(temp);

    Element * array = new Element[num_entries];

    int i = 0;
    while(std::getline(fin, temp)) {
        std::stringstream ss(temp);
        std::string word;
        int weight;
        ss >> word;
        ss >> weight;

        Element a(word, weight);
        array[i] = a;
        i++;
    }

    std::string input;
    int option = 0;
    do {
        printf("1. Search words with prefix\n");
        printf("0. Exit application\n");
        printf("\n");
        printf("Selection: ");
        std::getline(std::cin, input);

        std::stringstream my_stream(input);
        if (!(my_stream >> option))
            option = -1;

        switch (option) {
            case 0:
                printf("\nbye");
                break;
            case 1:
                do_main(get_prefixes(), array, num_entries);
                break;
            default:
                printf("Invalid Option, try again\n");
                break;
        }
    } while (option != 0);

    return 0;
}
