#ifndef ASSN6_ELEMENT_H_
#define ASSN6_ELEMENT_H_

#include <string>

class Element {
    public:
        std::string value;
        int weight;
        Element();
        Element(std::string value, int weight);
};

#endif // ASSN6_ELEMENT_H_
