#ifndef _Set_H
#define _Set_H
// Create a data type for the linked list node
#include <iostream>
class ListNode
{
    public:
        ListNode(); // default constructor
        ListNode(int s); // constructor with one parameter
        int value;
        ListNode *next;
};
class Set
{
    private:
        ListNode *head;
        int count ; // the number of elements in the list
    public:
        Set(); // Constructor: Create the initial linked list with two nodes
        Set(Set &obj); // Copy constructor
        ~Set(); // Destructor
        //Functions
        void Insert(int v); // Insert a new value in order. Duplicated element will not be inserted
        void Delete(int v); // Delete a value

        // could not use many of my functions because they weren't declared const.
        // any method that doesn't change the object should be const
        void Print() const; // Display list in order
        // compiler complained until I made Find const
        // which is correct, because it doesn't modify the object.
        bool Find(int v) const; // Search v in the list. Return true if v is found; otherwise, return false
        // Overloaded operators
        // all operators except = should be const, since they do not modify the original Set
        bool operator<(const Set &obj) const; // proper subset
        Set operator^(const Set &obj) const; // intersection
        Set operator+(const Set &obj) ; // union
        Set operator-(const Set &obj) ; // set difference
        Set & operator=(const Set &obj); // assignment
        int operator[](const int index) const ; // index

        // ostream operator for convenience
        friend std::ostream& operator<<(std::ostream &out, const Set &obj);
};
#endif
