//
// Created by abe on 2/8/16.
//
#include "Set.h"
#include <iostream>

using namespace std;

int main(int argc, char** argv) {
    Set set1;
    Set set2;

    set1.Insert(1);
    set1.Insert(2);
    set1.Insert(2);
    set1.Insert(4);
    set1.Insert(5);

    set2.Insert(1);
    set2.Insert(2);
    set2.Insert(4);
    set2.Insert(8);

    cout << "printing 1:" << endl;
    set1.Print();

    cout << "printing 2:" << endl;
    set2.Print();

    cout << "bracket operator: " << endl;
    cout << set1[3] << endl;

    cout << "Find method: " << endl;
    cout << set1.Find(3) << endl;
    cout << set1.Find(9) << endl;

    cout << "subset operator: " << endl;
    cout << (set2 < set1) << endl;

    cout << "intersection operator: "<< endl;
    cout << (set2 ^ set1) << endl;

    cout << "union operator: " << endl;
    cout << (set2 + set1) << endl;

    cout << "assignment operator" << endl;
    Set set3 = set2;
    cout << set3 << endl;

    cout << "diff operator: " << endl;
    cout << (set2 - set1) << endl;

    cout << "Delete method: " << endl;
    set2.Delete(2);
    set2.Delete(8);
    cout << set2 << endl;
}
