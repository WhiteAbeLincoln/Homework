//
// Created by abe on 2/8/16.
//
#include "Set.h"
#include <iostream>

using namespace std;

Set::Set() {
    head = new ListNode();
    count = 0;
};

ListNode::ListNode() {
    value = 0;
    next = NULL;
}

ListNode::ListNode(int s) {
    value = s;
    next = NULL;
}

Set::Set(Set &obj) {
    head = new ListNode();
    count = 0;

    ListNode * here = obj.head->next;

    while (here != NULL) {
        this->Insert(here->value);
        here = here->next;
    }

}

Set::~Set() {
    ListNode * here = head->next;
    ListNode * prev = head;

    while (here->next != NULL) {
        delete prev;

        prev = here;
        here = here->next;
    }
}

void Set::Insert(int v) {
    ListNode * newnode = new ListNode(v);

    ListNode * here = head->next;
    ListNode * prev = head;


    while (here != NULL && v > here->value) {


        prev = here;
        here = here->next;
    }

    // sets only contain unique values.
    if (here != NULL && here->value == newnode->value)
        return;
    prev->next = newnode;
    newnode->next = here;
    count++;
}

void Set::Delete(int v) {
    ListNode * here = head->next;
    ListNode * prev = head;

    while (here != NULL && v != here->value) {
        prev = here;
        here = here->next;
    }

    if (here != NULL) {
        prev->next = here->next;
        delete here;
    }
}

void Set::Print() const {
    ListNode * here = head->next;       //first real node, since the head is always 0

    while(here != NULL) {
        cout << here->value << endl;
        here = here->next;
    }
}

int Set::operator[](const int index) const {
    // we don't check for an out of bounds index, because C/C++ never do for any of their arrays
    ListNode * here = this->head->next;
    int count = 0;

    while (count < index && here->next != NULL) {
        here = here->next;
        count++;
    }

    return here->value;
}

bool Set::Find(const int v) const {
    // we know the list is already sorted.
    // We should use a binary search, but we don't know how many elements we have or any way to access an arbitrary element without starting from the beginning
    int min = 0;
    int max = this->count-1;
    // cout <<"max: " << max << endl;


    while (min <= max) {
        int mid = (min+max)/2;

        if ((*this)[mid] == v) {
            return true;
        } else if ((*this)[mid] < v) {
            min = mid +1;
        } else {
            max = mid - 1;
        }
    }

   /* ListNode * here = head->next;
    ListNode * prev = head;

    while (here->next != NULL) {
        if (here->value == v)
            return true;
        prev = here;
        here = here->next;
    } */

    return false;
}

// subset: all elements of set A (lhs, this) must also be elements of B (rhs, obj)
bool Set::operator<(const Set &obj) const {
    //Set rhs = obj;

    // if the sets point to the same address (they are the same)
    if (this == &obj) {
        // any set is a subset of itself
        return true;
    }

    // subset: all elements of set A (lhs) must also be elements of B (rhs)
    ListNode * here = this->head->next;

    while (here->next != NULL) {
        if (!obj.Find(here->value)) {
            return false;
        }

        here = here->next;
    }

    return true;
}

Set Set::operator^(const Set &obj) const {
    Set newset;         // create a blank Set

    ListNode * here = obj.head->next;

    // the new set will always have a max size of the smallest set
    // if obj (here) happens to be the smaller set this is a cheaper operation
    // (we stop after iterating over the smaller set)
    // otherwise we are repeatedly testing values that we have already seen, but that's ok
    //
    // the intersection occurs if an element of one set is found in the other set
    while (here->next != NULL) {
        // cout << "val: " << here->value << endl;
        if (this->Find(here->value))
            newset.Insert(here->value);
        here = here->next;
    }


    return newset;
}

Set Set::operator+(const Set &obj) {
    // cout << "hi from +" << endl;
    Set newset(*this);            // copy lhs into a new set, since we don't want to modify it

    ListNode * here = obj.head->next;

    while (here != NULL) {
        // insert the current value into the new set, and move to the next element
        // insert method handles duplicates for us
        newset.Insert(here->value);
        here = here->next;
    }

    return newset;
}

Set Set::operator-(const Set &obj) {
    Set newset;
    // Set rhs = Set(obj);

    ListNode * here = this->head->next;

    while (here != NULL) {
        if (!obj.Find(here->value)) {
            newset.Insert(here->value);
        }

        here = here->next;
    }

    return newset;
}

Set &Set::operator=(const Set &obj) {
    cout << "hi" << endl;
    if (this == NULL) {
        //cout
    }
    if (this == &obj) {
        return *this;
    } else {
        //delete this;
        ListNode * here = head->next->next;
        ListNode * prev = head->next;

        while (here != NULL) {
            cout << "prev: " << prev << endl;
            cout << "here: " << here << endl;
            delete prev;

            prev = here;
            here = here->next;
        }

        for (int i = 0; i < obj.count; i++) {
            this->Insert(obj[i]);
        }

    }

    return (*this);
}

ostream& operator<<(ostream &out, const Set &obj) {
    ListNode * here = obj.head->next;       //first real node, since the head is always 0

    while(here != NULL) {
        out << here->value << endl;
        here = here->next;
    }

    return out;
}


