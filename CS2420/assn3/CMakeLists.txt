cmake_minimum_required(VERSION 3.3)
project(assn3)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -g")

set(SOURCE_FILES
    BSTree.h BSTree.cpp main.cpp)

set( CMAKE_EXPORT_COMPILE_COMMANDS 1 )

add_executable(assn3 ${SOURCE_FILES})
