//
// Created by abe on 2/22/16.
//

#include "BSTree.h"

TreeNode::TreeNode() {
    //    TreeNode::TreeNode("");
    this->word = "";
    this->ptrLeft = NULL;
    this->ptrRight = NULL;
}

TreeNode::TreeNode(string s) {
    this->word = s;
    this->ptrLeft = NULL;
    this->ptrRight = NULL;
}

TreeNode::~TreeNode() {
    delete ptrLeft;
    delete ptrRight;
}


BSTree::BSTree() {
    root = NULL;
}

BSTree::BSTree(const BSTree& tree) {
    if (tree.root == NULL) {
        this->root = NULL;
    } else
        copyTree(this->root, tree.root);
}

BSTree::~BSTree() {
    // recursively deletes each node in the tree because of the TreeNode destructor
    delete this->root;
}

int BSTree::Size() {
    return rSize(root);
}

int BSTree::Height() {
    return rHeight(root);
}

void BSTree::Delete(string s) {
    rDelete(s, this->root);
}

string BSTree::Traverse() {
    return preorder(this->root, "");
}

void BSTree::Insert(string s) {
    iInsert(s, root);
}

bool BSTree::Find(string s) {
    return iSearch(s, this->root);
}

int BSTree::rSize(TreeNodeptr &ptr) {
    if (ptr != NULL) {
        return rSize(ptr->ptrLeft) + 1 + rSize(ptr->ptrRight);
    }
    return 0;
}

int BSTree::rHeight(TreeNodeptr &ptr) {
    if (ptr != NULL) {
        int left = rHeight(ptr->ptrLeft);
        int right = rHeight(ptr->ptrRight);
        return left > right ? left+1 : right+1;
    }
    return 0;
}

bool BSTree::iSearch(string key, TreeNodeptr &ptr) {
    TreeNodeptr* current = &ptr;
    while (*current != NULL) {
        if (key == (*current)->word) {
            return true;
        } else if (key < (*current)->word) {
            // cout << "finding left" << endl;
            current = &(*current)->ptrLeft;
        } else {
            // cout << "finding right" << endl;
            current = &(*current)->ptrRight;
        }
    }
    return false;
}

void BSTree::rInsert(string s, TreeNodeptr& root) {
    if (root == NULL) {
        root = new TreeNode(s);
    } else if (s <= root->word) {
        //    cout << "going left" << endl;
        rInsert(s, root->ptrLeft);
    } else {
        //   cout << "going right" << endl;
        rInsert(s, root->ptrRight);
    }
}

void BSTree::iInsert(string s, TreeNodeptr &ptr) {
    TreeNodeptr *current = &ptr;

    while(*current != NULL) {

        if(s <= (*current)->word)
            current = &(*current)->ptrLeft;
        else
            current = &(*current)->ptrRight;
    }
    *current = new TreeNode(s);

}

string BSTree::preorder(TreeNodeptr ptr, string s) {
    if (ptr != NULL) {
        s += ptr->word;
        s += " ";
        // cout << ptr->word << " ";
        s = preorder(ptr->ptrLeft, s);
        s = preorder(ptr->ptrRight, s);
    }
    return s;
}

void BSTree::copyTree(TreeNodeptr &thisptr, const TreeNodeptr &srcptr) {
    if (srcptr == NULL) {
        thisptr = NULL;
    } else {
        thisptr = new TreeNode(srcptr->word);
        copyTree(thisptr->ptrLeft, srcptr->ptrLeft);
        copyTree(thisptr->ptrRight, srcptr->ptrRight);
    }
}

void BSTree::rDelete(string s, TreeNodeptr &ptr) {
    if (ptr != NULL) {
        if (s==ptr->word) {
            delNode(ptr);
        } else if (s <= (ptr->word)) {
            rDelete(s, ptr->ptrLeft);
        } else {
            rDelete(s, ptr->ptrRight);
        }
    }

}

void BSTree::delNode(TreeNodeptr &ptr) {
    if (ptr->ptrLeft == NULL && ptr->ptrRight == NULL) {
        delete ptr;
        ptr = NULL;
    } else if (ptr->ptrLeft == NULL) {
        TreeNodeptr here = ptr;
        ptr = ptr->ptrRight;
        delete here;
    } else if (ptr->ptrRight == NULL) {
        TreeNodeptr here = ptr;
        ptr = ptr->ptrLeft;
        delete here;
    } else {
        rDelete(ptr->word, ptr);
    }
}
