//
// Created by abe on 2/22/16.
//
#include "BSTree.h"
#include <fstream>
#include <vector>
// for the random shuffle, as suggested
#include <algorithm>

using namespace std;

int main(int argc, char** argv) {
    string dictionaryPath = "dictionary.txt";
    string letterPath = "letter.txt";
    vector<string> dictlist;

    BSTree tree;
    tree.Insert("test");
    tree.Insert("good");
    tree.Insert("score");

    cout << tree.Traverse() << endl;
    if (tree.Find("good")) {
        cout << "good found" << endl;
    }
    ifstream fin;
    ofstream fout;

    fin.open(dictionaryPath);
    if (!fin)
        cout << "Error opening dictionary" << endl;

    string tempText;
    while (getline(fin, tempText)) {
        // strip the windows newlines (carriage returns), because they cause issues.
        tempText.erase(std::remove(tempText.begin(), tempText.end(), '\r'), tempText.end());
        dictlist.push_back(tempText);
    }

    fin.close();

    // shuffle the function using <algorithm>.random_shuffle
    std::random_shuffle(dictlist.begin(), dictlist.end());


    cout << endl;
    cout << "TESTING PUBLIC FUNCTIONS" << endl;
    cout << endl;

    // test the public member functions
    BSTree testTree;
    for (int i = 0; i < 10; i++) {
        testTree.Insert(dictlist[i]);
    }

    cout << "Tree Height: " << testTree.Height() << endl;
    cout << "Tree Size: " << testTree.Size() << endl;
    // note that if dictionary.txt has carriage returns, then this will cause problems on linux
    cout << "Preorder Traverse: " << testTree.Traverse() << endl;
    cout << "Find - " << dictlist[5] << " exists: " << testTree.Find(dictlist[5]) << endl;


    cout << endl;
    cout << "READING LETTER" << endl;
    cout << endl;


    BSTree dictTree;
    // using a lambda and foreach, because I don't like writing for loops
    ::for_each(dictlist.begin(), dictlist.end(), [&dictTree](string s) {
       dictTree.Insert(s);
    });

    fin.open(letterPath);
    if (!fin)
        cout << "Error opening letter" << endl;

    tempText = "";
    // letter doesn't contain any punctuation, except for the period in Dr. , which isn't even in our dictionary
    // Also note that the dictionary doesn't contain:
    // joker
    // programming
    // prestige
    // program
    //
    // this concerned me at first until I manually searched the dictionary file
    while (fin >> tempText) {
        // remove our carriage returns
        tempText.erase(std::remove(tempText.begin(), tempText.end(), '\r'), tempText.end());
        // use the same erase, remove method to get rid of punctuation
        tempText.erase (std::remove_if(tempText.begin(), tempText.end(), ::ispunct), tempText.end());
        // lowercase the words
        std::transform(tempText.begin(), tempText.end(), tempText.begin(), ::tolower);
        // cout << "the word: " << tempText << endl;

        if (!dictTree.Find(tempText)) {
            cout <<  tempText << endl;
        }
    }
}
