#!/usr/bin/env python
import sys

class DisjointSetArray(object):
    def __init__(self, n):
        self.n = n
        self.S = []
        for i in range(0, n-1):
            self.S.append(i)

    def find(self, x):
        return self.S[x]

    def union(self, x, y):
        if self.find(x) == self.find(y):
            return
        else:
            for idx, elem in enumerate(self.S):
                if elem == self.S[x]:
                    self.S[idx] = self.S[y]

class DisjointSetTree(object):
    def __init__(self, n):
        self.n = n
        self.S = []
        for i in range(0, n-1):
            self.S.append(-1)

    def iterate_find(self, x):
        while self.S[x] >= 0:
            x = self.S[x]
        return x

    def find(self, x):
        if self.S[x] < 0:
            return x
        else:
            return self.find(self.S[x])

    def union(self, x, y):
        rootx = self.find(x)
        rooty = self.find(y)
        if rootx == rooty:  # already same set
            return
        else:
            self.S[rootx] = rooty


def main(argv=None):
    if argv is None:
        argv = sys.argv

    set1 = DisjointSetArray(5)
    print(set1.find(1))
    print(set1.find(2))
    print()
    set1.union(1,2)
    print(set1.find(1))
    print(set1.find(2))
    print(set1.find(3))
    print()
    set1.union(2,3)
    print(set1.find(1))
    print(set1.find(2))
    print(set1.find(3))

    print()
    print()
    set2 = DisjointSetTree(5)
    print(set2.find(1))
    print(set2.find(2))
    print()
    set2.union(1,2)
    print(set2.find(1))
    print(set2.find(2))
    print(set2.find(3))
    print()
    set2.union(2,3)
    print(set2.find(1))
    print(set2.find(2))
    print(set2.find(3))

if __name__ == "__main__":
    sys.exit(main())
