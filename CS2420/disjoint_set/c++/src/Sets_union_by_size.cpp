#include "Sets.h"

void Sets::Union(int i, int j) {
    int rootx = Find(i);
    int rooty = Find(j);

    if (rootx == rooty) return;

    int sum = this->p[rootx] + this->p[rooty];

    if (-this->p[rootx] > -this->p[rooty]) {
        // make rootx as parent of rooty
        this->p[rooty] = rootx;
        this->p[rootx] = sum;
    } else {
        // make rooty as parent of rootx
        this->p[rootx] = rooty;
        this->p[rooty] = sum;
    }
}
