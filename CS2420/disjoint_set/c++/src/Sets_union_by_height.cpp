#include "Sets.h"

void Sets::Union(int i, int j) {
    int rootx = Find(i);
    int rooty = Find(j);

    if (rootx == rooty) return;

    if (-this->p[rootx] > -this->p[rooty]) {
        // rootx is higher
        this->p[rooty] = rootx;       // don't change the tree height
    }

    if (-this->p[rooty] > -this->p[rootx]) {
        // rooty is higher
        this->p[rootx] = rooty;
    }

    if (-this->p[rootx] == -this->p[rooty]) {
        // same height
        this->p[rootx] = rooty;
        // increase tree height by one
        this->p[rooty]--;
    }
}
