#include "Sets.h"

void Sets::Union(int i, int j) {
    int rooti = Find(i);
    int rootj = Find(j);
    
    if (rooti == rootj) return;
    else
        this->p[rooti] = rootj;
}
