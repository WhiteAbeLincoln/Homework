#ifndef DISJOINTSETS_SETS_H_
#define DISJOINTSETS_SETS_H_

class Sets {
    private:
        int* p;
        int n;
    public:
        Sets(int size) {
            this->n = size;
            this->p = new int[n+1];
            for (int i = 0; i <= n; i++) {
                this->p[i] = -1;
            }
        }

        ~Sets() {
            delete[] p;
        }

        void Union(int i, int j);
        int Find(int i);
};

#endif //DISJOINTSETS_SETS_H_
