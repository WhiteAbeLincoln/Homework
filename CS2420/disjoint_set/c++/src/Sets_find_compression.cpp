#include "Sets.h"

int Sets::Find(int i) {
    if (this->p[i] < 0) return i;
    else {
        this->p[i] = Find(this->p[i]);
        return this->p[i];
    }
}
