#include "Sets.h"
#include <cstdio>

int main() {
    Sets set1 = Sets(5000);
    Sets set2 = Sets(5000);

    printf("Find %d: %d\n", 1, set1.Find(1));
    printf("Find %d: %d\n", 2, set1.Find(2));
    printf("\n");
    set1.Union(1, 2);
    set2.Union(2, 1);
    printf("Find %d: %d\n", 1, set1.Find(1));
    printf("Find %d: %d\n", 2, set1.Find(2));
    printf("Find %d: %d\n", 3, set1.Find(3));
    printf("\n");
    set1.Union(2, 3);
    printf("Find %d: %d\n", 1, set1.Find(1));
    printf("Find %d: %d\n", 2, set1.Find(2));
    printf("Find %d: %d\n", 3, set1.Find(3));
    printf("\n");
    set2.Union(2, 3);
    printf("Find %d: %d\n", 1, set2.Find(1));
    printf("Find %d: %d\n", 2, set2.Find(2));
    printf("Find %d: %d\n", 3, set2.Find(3));


}
