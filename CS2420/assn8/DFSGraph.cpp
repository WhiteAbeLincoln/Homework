#include "DFSGraph.h"
#include <iostream>

DFSGraph::DFSGraph(int n_input) {
    this->n = n_input;
    this->adj = new Vertex*[n_input];
    this->color = new colorType[n_input];
    this->pre = new int[n_input];
    this->d = new int[n_input];
    this->f = new int[n_input];
    this->time = 0;
    for (int i = 0; i < n_input; i++) {
        this->adj[i] = NULL;
    }
}

DFSGraph::~DFSGraph() {
    delete[] adj;
    delete pre;
    delete color;
    delete d;
    delete f;
}

void DFSGraph::SetAdjLists(int *adjM) {
    for (int i = 0; i < this->n; i++) {
        for (int j = this->n-1; j >= 0; j--) {
            if (adjM[i*this->n+j] == 1) {
                Vertex * v = new Vertex(j);
                v->next = this->adj[i];
                this->adj[i] = v;
            }
        }
    }
}

void DFSGraph::PrintAdjLists() {
    // I'm guessing this is supposed to print the traversal order, since there is no mention of PrintAdjLists in the instructions
    DFS();
    std::cout << "Traversal Order\n";
    std::cout << "---------------\n";
    for (unsigned long i = 0; i < this->traverse.size(); i++) {
        std::cout << this->traverse[i] << " ";
    }
    std::cout << "\n---------------\n";
}

void DFSGraph::DFS(int id) {
    for (int i = 0; i < this->n; i++) {
        this->color[i] = colorType::WHITE;
        this->d[i] = 2*this->n+1;
        this->f[i] = 2*this->n+1;
        this->pre[i] = n;
    }
    this->time = 0;
    DFSVisit(id);
    for (int i = 0; i < this->n; i++) {
        if (this->color[i] == colorType::WHITE) {
            DFSVisit(id);
        }
    }
}

void DFSGraph::DFSVisit(int id) {
    this->color[id] = colorType::GRAY;
    this->traverse.push_back(id);
    this->time++;
    this->d[id] = time;
    Vertex * v = this->adj[id];
    while (v != NULL) {
        if (this->color[v->id] == colorType::WHITE) {
            this->pre[v->id] = id;
            DFSVisit(v->id);
        }
        v = v->next;
    }
    this->color[id] = colorType::BLACK;
    this->time++;
    this->f[id] = time;
}

void DFSGraph::PrintReachableNodes(int source) {
    std::cout << "Reachable Nodes from " << source << std::endl;;
    std::cout << "---------------\n";
    // iterate through all of the vertices
    for (int i = 0; i < this->n; i++) {
        // if a path exists between the two, output that.
        if (pathExists(source, i)) {
            std::cout << i << " ";
        }
    }
    std::cout << "\n---------------\n";
}

bool DFSGraph::pathExists(int from, int to) {
    DFS(from);
    // if the parent has been set for any one of these, then it is connected to the root (from)
    return this->pre[to] != this->n;
}
