#ifndef ASSN8_DFSGRAPH_H_
#define ASSN8_DFSGRAPH_H_
#include "Vertex.h"
#include "ColorType.h"
#include <vector>
class DFSGraph
{
    private:
        colorType * color ; // record the colors of the vertices during DFS
        int * pre ;
        // record the predecessors during DFS
        int * d;
        // record the discovery times during DFS
        int * f;
        // record the finish times during DFS
        std::vector<int> traverse;
        int time;
        bool pathExists(int from, int to);
    public:
        int n ;
        // the number of vertices, the ids of the vertices are from 0 to n-1
        Vertex ** adj ; // adj[i] points the head of the adjacency list of vertex i
        DFSGraph(int n_input);
        ~DFSGraph();
        // constructor
        void SetAdjLists(int * adjM); // build the adjacency lists from the adjacency matrix adjM
        void PrintAdjLists();
        // print the adjacency lists of the graph
        // the following two functions are for the DFS traversal as we discussed in class
        void DFS(int id = 0) ; // DFS traversal, id is the source vertex, with default 0
        void DFSVisit(int id ); // actually does DFS, search a connected component from id
        void PrintReachableNodes(int source) ; // Print all nodes that can be reached by source
};
#endif  // ASSN8_DFSGRAPH_H_
