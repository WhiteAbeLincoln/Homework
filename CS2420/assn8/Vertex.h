#ifndef ASSN8_VERTEX_H_
#define ASSN8_VERTEX_H_
#include <cstddef>
class Vertex {
    public:
        int id;
        Vertex *next;
        Vertex(int input_id, Vertex* input_next = NULL) {
            this->id = input_id;
            this->next = input_next;
        }
};
#endif      // ASSN8_VERTEX_H_
