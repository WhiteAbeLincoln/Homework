#include "BFSGraph.h"
#include <iostream>
#include <string>

BFSGraph::BFSGraph(int n_input) {
    this->n = n_input;
    this->adj = new Vertex*[n_input];
    this->color = new colorType[n_input];
    this->pre = new int[n_input];
    this->dis = new int[n_input];
    this->front = this->rear = 0;
    for (int i = 0; i < n_input; i++) {
        this->adj[i] = NULL;
    }
}

void BFSGraph::SetAdjLists(int *adjM) {
    for (int i = 0; i < this->n; i++) {
        for (int j = this->n-1; j >= 0; j--) {
            if (adjM[i*this->n+j] == 1) {
                Vertex * v = new Vertex(j);
                v->next = this->adj[i];
                this->adj[i] = v;
            }
        }
    }
}

BFSGraph::~BFSGraph() {
    delete[] adj;
    delete pre;
    delete dis;
    delete color;
}


void BFSGraph::PrintAdjLists() {
    // I'm guessing this is supposed to print the traversal order, since there is no mention of PrintAdjLists in the instructions
    BFS();
    std::cout << "Traversal Order\n";
    std::cout << "---------------\n";
    for (unsigned long i = 0; i < this->traverse.size(); i++) {
        std::cout << this->traverse[i] << " ";
    }
    std::cout << "\n---------------\n";
}


void BFSGraph::BFS(int id) {
    for (int i = 0; i < this->n; i++) {   // initialize n vertices from vertex 0 to vertex n-1
        this->color[i] = WHITE;             // record the colors of the vertices during BFS
        this->dis[i] = this->n;             // record the shortest path distances during BFS, assign it to any number larger than n-1
        this->pre[i] = this->n;             // record the predecessors during BFS, assign it to any number larger than n-1
        this->front = this->rear = 0;
    }

    BFSVisit(id);

    for (int i = 0; i < this->n; i++) {
        if (color[i] == colorType::WHITE)
            BFSVisit(i);
    }
}

void BFSGraph::BFSVisit(int id) {
    this->color[id] = colorType::GRAY;
    this->dis[id] = 0;
    int * Q = new int[this->n];
    enqueue(Q, id);
    while (this->rear != this->front) {
        int u = dequeue(Q);
        Vertex * v = this->adj[u];
        while (v != NULL) {
            if (this->color[v->id] == colorType::WHITE) {
                this->color[v->id] = colorType::GRAY;
                this->dis[v->id] = this->dis[u] + 1;
                this->pre[v->id] = u;
                enqueue(Q, v->id);
            }
            v = v->next;
        }
        this->color[u] = colorType::BLACK;
        this->traverse.push_back(u);
    }
}

void BFSGraph::enqueue(int*array, int i) {
    array[this->rear++] = i;
}

int BFSGraph::dequeue(int* array) {
    return array[this->front++];
}

void BFSGraph::PrintSP(int source, int v) {
    BFS(source);
    std::cout << "Shortest Path from " << source << " to " << v << std::endl;
    std::cout << "-------------\n";
    std::string buff = std::to_string(v) + " ";
    while (this->pre[v] != source) {
        v = this->pre[v];
        buff += (std::to_string(v) + " ");
    }
    // prints it out in reverse order, since the while loop above starts from bottom
    std::cout << source;
    for (int i = buff.length()-1; i >= 0; i--) {
        std::cout << buff[i];
    }
    std::cout << std::endl;
    std::cout << "distance: " << this->dis[v] << std::endl;
    std::cout << "-------------\n";
}
