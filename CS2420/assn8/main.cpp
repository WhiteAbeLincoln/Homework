#include "BFSGraph.h"
#include "DFSGraph.h"
#include <fstream>
#include <iostream>

int main() {
    std::cout << "BFS" << std::endl;
    std::ifstream fin("Assign8BFSInput.txt");
    int len = 0;
    // get number of vertices in graph
    fin >> len;
    // init adjacency matrix
    int * M1 = new int[len*len];

    // populate adjacency matrix
    for(int n, i = 0; fin >> n; i++)
        M1[i] = n;

    fin.close();

    BFSGraph bfs1(len);
    bfs1.SetAdjLists(M1);

    bfs1.PrintAdjLists();
    for (int i = 1; i <= len-1; i++) {
        bfs1.PrintSP(0, i);
        std::cout << std::endl;
    }

    std::cout << std::endl;
    std::cout << "DFS" << std::endl;
    fin.open("Assign8DFSInput.txt");
    fin >> len;
    int * M2 = new int[len*len];

    for(int n, i = 0; fin >> n; i++)
        M2[i] = n;

    fin.close();

    DFSGraph dfs1(len);
    dfs1.SetAdjLists(M2);

    dfs1.PrintAdjLists();
    dfs1.PrintReachableNodes(0);
    dfs1.PrintReachableNodes(4);
}
