#include "Graph.h"
#include <cstdlib>
#include <iostream>
#include <vector>
#include <stack>

Graph::Graph(int n_input) {
    this->n = n_input;
    this->adj = new Vertex*[n_input];
    this->adjM = new int[n_input*n_input];
    this->pre = new int[n_input];
    this->dis = new int[n_input];

    for (int i = 0; i < n_input; i++) {
        this->adj[i] = NULL;
    }
}

void Graph::SetAdjLists(int *adjM) {
    this->adjM = adjM;
    for (int i = 0; i < this->n; i++) {
        for (int j = this->n-1; j >= 0; j--) {
            if (adjM[i*this->n+j] != 0) {
                Vertex * v = new Vertex(j, adjM[i*this->n+j]);
                v->next = this->adj[i];
                this->adj[i] = v;
            }
        }
    }
}

void Graph::PrintAdjLists() {
    for (int i = 0; i < this->n; i++) {

        Vertex * temp = this->adj[i];
        std::cout << std::to_string(i) << " ";

        while (temp != NULL) {
            std::printf("->%d", temp->id);
            temp = temp->next;
        }

        std::cout << std::endl;
    }
}

void Graph::PrintAdjListsWeight() {
    for (int i = 0; i < this->n; i++) {

        Vertex * temp = this->adj[i];
        std::cout << std::to_string(i) << " ";

        while (temp != NULL) {
            std::printf("->%d(%d)", temp->id, temp->weight);
            temp = temp->next;
        }

        std::cout << std::endl;
    }
}

bool Graph::has_incident(int num) {
    for (int i = 0; i < this->n; i++) {
        Vertex * temp = adj[i];
        while (temp != NULL) {
            if (temp->id == num)
                return true;

            temp = temp->next;
        }
    }

    return false;
}

void Graph::TopologicalSort() {
    std::stack<int> S;
    std::vector<int> L;

    for (int i = 0; i < this->n; i++) {
        if (!has_incident(i)) {
            S.push(i);
        }
    }

    while (!S.empty()) {
        int idx_v = S.top();
        S.pop();
        L.push_back(idx_v);

        Vertex * v = adj[idx_v];
        adj[idx_v] = NULL;
        while (v != NULL) {
            if (!has_incident(v->id)) {
                S.push(v->id);
            }

            v = v->next;
        }
    }

    for (unsigned long i = 0; i < L.size(); i++) {
        std::cout << L[i] << " ";
    }

    std::cout << std::endl;
}

void Graph::Dijkstra(int s) {

}

void Graph::PrintShortestPath(int source, int v) {

}

int Graph::GetShortestPathDis(int v) {
    return 0;
}
