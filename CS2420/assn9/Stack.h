#ifndef ASSN9_STACK_H_
#define ASSN9_STACK_H_
#include <cstdlib>
#include <stdexcept>
#include "ListNode.h"

using namespace std;

class IsEmptyException: public runtime_error {
    public:
        IsEmptyException():runtime_error("Stack is Empty") {}
};

template <typename T>
class Stack {
    private:
        ListNode<T> *top;
        int count;

    public:
        Stack() {
            top = NULL;
            count = 0;
        }

        ~Stack() {
            ListNode<T> *temp = top;
            while (temp != NULL) {
                top = temp->next;
                delete temp;
                temp = top;
            }
        }

        void Push(T item) {
            ListNode<T>* temp = new ListNode<T>;
            temp->value = item;
            temp->next = top;
            top = temp;
            count++;
        }

        T Pop() {
            if (count == 0) {
                throw IsEmptyException();
            }

            ListNode<T> *temp = top;
            T item = temp->value;
            top = temp->next;
            count--;
            delete temp;
            return item;
        }

        T Top() {
            if (count == 0) {
                throw IsEmptyException();
            }

            return top->value;
        }
};

#endif  //ASSN9_STACK_H_
