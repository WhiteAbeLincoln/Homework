#ifndef ASSN9_LISTNODE_H_
#define ASSN9_LISTNODE_H_
#include <cstdlib>

template <typename T>
class ListNode {
    public:
        T value;
        ListNode *next;
        ListNode(T input_value = NULL, ListNode* input_next = NULL) {
            value = input_value;
            next = input_next;
        }

};

#endif  // ASSN9_LISTNODE_H_
