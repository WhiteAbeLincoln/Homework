#include "Graph.h"
#include <fstream>
#include <iostream>

int main() {
    std::cout << "BFS" << std::endl;
    std::ifstream fin("Assign9TopologicalInput.txt");
    int len = 0;
    // get number of vertices in graph
    fin >> len;
    // init adjacency matrix
    int * M1 = new int[len*len];

    // populate adjacency matrix
    for(int n, i = 0; fin >> n; i++)
        M1[i] = n;

    fin.close();

    Graph dag(len);
    dag.SetAdjLists(M1);

    std::cout << "DAG Adjacency List" << std::endl;
    dag.PrintAdjLists();
    std::cout << "DAG Topological Sort" << std::endl;
    dag.TopologicalSort();

    std::cout << std::endl;

    std::cout << "DFS" << std::endl;
    fin.open("Assign9ShortestPathInput.txt");
    fin >> len;
    int * M2 = new int[len*len];

    for(int n, i = 0; fin >> n; i++)
        M2[i] = n;

    fin.close();

    Graph weighted(len);
    weighted.SetAdjLists(M2);

    std::cout << "Weighted Adjacency List" << std::endl;
    weighted.PrintAdjListsWeight();
    std::cout << "Dijkstra" << std::endl;
    weighted.Dijkstra(0);
    for (int i = 0; i < len; i++) {
        std::cout << "Shortest Path " << i << std::endl;
        weighted.PrintShortestPath(0, i);
    }
    std::cout << "Shortest Path Dist" << std::endl;
    std:: cout << weighted.GetShortestPathDis(0) << std::endl;
}
