#include <string>
#include <algorithm>
#include <fstream>
#include <vector>
#include <sstream>
#include "AvlTree.h"
#include "BSTree.h"

using namespace std;

// convenience function to split string
// see http://stackoverflow.com/questions/236129/split-a-string-in-c
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

// using templates so I avoid repeating myself
template <typename T>
void loadInstructions(T* &tree, string path) {
    ifstream fin;
    fin.open(path);
    if (!fin)
        cout << "Error opening " + path;

    string tempText;
    while (getline(fin, tempText)) {
        vector<string> tokens = split(tempText, ' ');
        if (tokens[0] == "insert") {
            tree->Insert(tokens[1]);
        } else if (tokens[0] == "remove") {
            tree->Delete(tokens[1]);
        }
    }
    fin.close();

    cout << "the preorder traversal list is: " << endl;
    cout << tree->Traverse(0) << endl;
    cout << "the descending traversal list is: " << endl;
    cout << tree->Traverse(3) << endl;
    cout << "the word in the root is: " << tree->GetRoot() << endl;
    cout << "the height of the tree is: " << tree->Height() << endl;
    cout << "the number of nodes in the tree is: " << tree->Size() << endl;
    cout << "the number of nodes with 2 children is: " << tree->CountTwoChildren() << endl;
    cout << "the number of nodes with 1 child is: " << tree->CountOneChild() << endl;
    cout << "the number of leaf nodes is: " << tree->CountLeaves() << endl;

    // print tree for fun
    // tree->Print();
}

template <typename T>
void loadDictionary(T* &tree, vector<string> dictlist) {
    // read our dictlist into the trees
    ::for_each(dictlist.begin(), dictlist.end(), [&tree](string s) {
       tree->Insert(s);
    });

    cout << "Tree size after reading dictionary: " << tree->Size() << endl;
}

template<typename T>
double loadLetter(T* &tree, string path) {
    fstream fin;
    fin.open(path); if (!fin)
        cout << "Error opening letter" << endl;

    string tempText = "";
    int count = 0;
    int comparisons = 0;
    // letter doesn't contain any punctuation, except for the period in Dr. , which isn't even in our dictionary
    // Also note that the dictionary doesn't contain:
    // joker
    // programming
    // prestige
    // program
    //
    // this concerned me at first until I manually searched the dictionary file
    while (fin >> tempText) {
        // remove our carriage returns
        tempText.erase(std::remove(tempText.begin(), tempText.end(), '\r'), tempText.end());
        // use the same erase, remove method to get rid of punctuation
        tempText.erase (std::remove_if(tempText.begin(), tempText.end(), ::ispunct), tempText.end());
        // lowercase the words
        std::transform(tempText.begin(), tempText.end(), tempText.begin(), ::tolower);
        // cout << "the word: " << tempText << endl;

        comparisons += tree->FindComparisons(tempText);
        if (!tree->Find(tempText)) {
            cout <<  tempText << endl;
        }
        count++;
    }
    fin.close();

    return (double)(comparisons / count);
}

void loadLetterComparisons(BSTree* &btree, AvlTree* &atree, string path) {
    fstream fin;
    fin.open(path);
    if (!fin)
        cout << "Error opening letter" << endl;

    string tempText = "";
    // letter doesn't contain any punctuation, except for the period in Dr. , which isn't even in our dictionary
    // Also note that the dictionary doesn't contain:
    // joker
    // programming
    // prestige
    // program
    //
    // this concerned me at first until I manually searched the dictionary file
    while (fin >> tempText) {
        // remove our carriage returns
        tempText.erase(std::remove(tempText.begin(), tempText.end(), '\r'), tempText.end());
        // use the same erase, remove method to get rid of punctuation
        tempText.erase (std::remove_if(tempText.begin(), tempText.end(), ::ispunct), tempText.end());
        // lowercase the words
        std::transform(tempText.begin(), tempText.end(), tempText.begin(), ::tolower);
        // cout << "the word: " << tempText << endl;

        cout << tempText << endl;
        cout << "avl: " << atree->FindComparisons(tempText) << " | bst: " << btree->FindComparisons(tempText)<< endl;
    }
    fin.close();
}

int main(int argc, char** argv) {
    string DICT_PATH= "dictionary.txt";
    string LETTER_PATH = "letter.txt";
    string INSTRUCT_PATH = "hw4_input.txt";

    AvlTree * dictAVLTree = new AvlTree;
    BSTree * dictBSTree = new BSTree;
    AvlTree * testAVLTree = new AvlTree;
    BSTree * testBSTree = new BSTree;
    vector<string> dictlist;
    fstream fin;

    fin.open(DICT_PATH);
    if (!fin)
        cout << "Error opening dictionary" << endl;

    string tempText = "";
    while (getline(fin, tempText)) {
        // strip the windows newlines (carriage returns), because they cause issues.
        tempText.erase(std::remove(tempText.begin(), tempText.end(), '\r'), tempText.end());
        dictlist.push_back(tempText);
    }
    fin.close();

    // shuffle the function using <algorithm>.random_shuffle
    std::random_shuffle(dictlist.begin(), dictlist.end());



    cout << "Testing AVL tree with " << INSTRUCT_PATH << endl;
    loadInstructions(testAVLTree, INSTRUCT_PATH);

    cout << endl;
    cout << "loading avl dictionary" << endl;
    loadDictionary(dictAVLTree, dictlist);

    cout << endl;
    cout << "reading avl letter" << endl;
    cout << "==================" << endl;
    double avlAvg = loadLetter(dictAVLTree, LETTER_PATH);

    cout << endl;
    cout << endl;

    cout << "Testing BST tree with " << INSTRUCT_PATH << endl;
    loadInstructions(testBSTree, INSTRUCT_PATH);

    cout << endl;
    cout << "loading bst dictionary" << endl;
    loadDictionary(dictBSTree, dictlist);

    cout << endl;
    cout << "reading bst letter" << endl;
    cout << "==================" << endl;
    double bstAvg = loadLetter(dictBSTree, LETTER_PATH);

    cout << endl;

    cout << "root\t AVL: " << dictAVLTree->GetRoot() << " | BST: " << dictBSTree->GetRoot() << endl;
    cout << "size\t AVL: " << dictAVLTree->Size() << " | BST: " << dictBSTree->Size() << endl;
    cout << "height\t AVL: " << dictAVLTree->Height() << " | BST: " << dictBSTree->Height() << endl;
    cout << "num 2 child\t AVL: " << dictAVLTree->CountTwoChildren() << " | BST: " << dictBSTree->CountTwoChildren() << endl;
    cout << "num 1 child\t AVL: " << dictAVLTree->CountOneChild() << " | BST: " << dictBSTree->CountOneChild() << endl;
    cout << "leaf nodes\t AVL: " << dictAVLTree->CountLeaves() << " | BST: " << dictBSTree->CountLeaves() << endl;
    cout << endl;
    cout << "AVL vs BST Comparisons for letter" << endl;
    cout << endl;
    loadLetterComparisons(dictBSTree, dictAVLTree, LETTER_PATH);

    cout << endl;
    cout << "AVL vs BST average comparisons" << endl;
    printf("AVL: %.4f | BST %.4f \n", avlAvg, bstAvg);
}
