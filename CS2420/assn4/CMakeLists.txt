cmake_minimum_required(VERSION 3.3)
project(assn4)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -g")

set(SOURCE_FILES
    AvlTree.cpp
    AvlTree.h
    main.cpp)


add_executable(assn4 ${SOURCE_FILES})