//
// Created by abe on 2/22/16.
//

#include "BSTree.h"

TreeNode::TreeNode() {
    //    TreeNode::TreeNode("");
    this->word = "";
    this->ptrLeft = NULL;
    this->ptrRight = NULL;
}

TreeNode::TreeNode(string s) {
    this->word = s;
    this->ptrLeft = NULL;
    this->ptrRight = NULL;
}

TreeNode::~TreeNode() {
    delete ptrLeft;
    delete ptrRight;
}


BSTree::BSTree() {
    root = NULL;
}

BSTree::BSTree(const BSTree& tree) {
    if (tree.root == NULL) {
        this->root = NULL;
    } else
        copyTree(this->root, tree.root);
}

BSTree::~BSTree() {
    // recursively deletes each node in the tree because of the TreeNode destructor
    delete this->root;
}

int BSTree::Size() {
    return rSize(root);
}

int BSTree::Height() {
    return rHeight(root) - 1;
}

void BSTree::Delete(string s) {
    rDelete(s, this->root);
}

string BSTree::Traverse() {
    return rev_inorder(this->root, "");
}

string BSTree::Traverse(int n) {
    if (n == 0) return preorder(this->root, "");
    else if (n == 1) return inorder(this->root, "");
    else if (n == 2) return postorder(this->root, "");
    else return rev_inorder(this->root, "");
}

void BSTree::Insert(string s) {
    iInsert(s, root);
}

void BSTree::Print() {
    print_t(this->root);
}

bool BSTree::Find(string s) {
    return iSearch(s, this->root);
}

int BSTree::FindComparisons(string s) {
    return rComparisons(this->root, s);
}

int BSTree::CountOneChild() {
    return countChild(root, 1);
}

int BSTree::CountTwoChildren() {
    return countChild(root, 2);
}

int BSTree::CountLeaves() {
    return countChild(root, 0);
}

// returns a 0 based height (leaf nodes have a height of 0) as the professor specified
int BSTree::GetHeight(TreeNodeptr v) {
    if (v == NULL)
        return -1;
    else
        return rHeight(v) - 1;
    // return rHeight(v);
}

string BSTree::GetRoot() {
    return root->word;
}


// private functions

// printing for debugging
// taken from http://stackoverflow.com/questions/801740/c-how-to-draw-a-binary-tree-to-the-console and modified for strings
int BSTree::_print_t(TreeNodeptr tree, int is_left, int offset, int depth, char s[20][255])
{
    char b[20];
    int width = 5;

    if (!tree) return 0;

    sprintf(b, "(%03s)", tree->word.c_str());

    int left  = _print_t(tree->ptrLeft,  1, offset,                depth + 1, s);
    int right = _print_t(tree->ptrRight, 0, offset + left + width, depth + 1, s);

#ifdef COMPACT
    for (int i = 0; i < width; i++)
        s[depth][offset + left + i] = b[i];

    if (depth && is_left) {

        for (int i = 0; i < width + right; i++)
            s[depth - 1][offset + left + width/2 + i] = '-';

        s[depth - 1][offset + left + width/2] = '.';

    } else if (depth && !is_left) {

        for (int i = 0; i < left + width; i++)
            s[depth - 1][offset - width/2 + i] = '-';

        s[depth - 1][offset + left + width/2] = '.';
    }
#else
    for (int i = 0; i < width; i++)
        s[2 * depth][offset + left + i] = b[i];

    if (depth && is_left) {

        for (int i = 0; i < width + right; i++)
            s[2 * depth - 1][offset + left + width/2 + i] = '-';

        s[2 * depth - 1][offset + left + width/2] = '+';
        s[2 * depth - 1][offset + left + width + right + width/2] = '+';

    } else if (depth && !is_left) {

        for (int i = 0; i < left + width; i++)
            s[2 * depth - 1][offset - width/2 + i] = '-';

        s[2 * depth - 1][offset + left + width/2] = '+';
        s[2 * depth - 1][offset - width/2 - 1] = '+';
    }
#endif

    return left + width + right;
}

int BSTree::print_t(TreeNodeptr tree)
{
    char s[20][255];
    for (int i = 0; i < 20; i++)
        sprintf(s[i], "%80s", " ");

    _print_t(tree, 0, 0, 0, s);

    for (int i = 0; i < 20; i++)
        printf("%s\n", s[i]);
}


int BSTree::rComparisons(TreeNodeptr ptr, string s) {
    if (ptr != NULL) {
        if (s == ptr->word)
            return 0;
        else if (s < ptr->word)
            return 1 + rComparisons(ptr->ptrLeft, s);
        else
            return 1 + rComparisons(ptr->ptrRight, s);
    }
    return 0;
}

int BSTree::countChild(TreeNodeptr ptr, int numToCount) {
    int count = 0;
    int twoCount = 0;
    int oneCount = 0;
    if (ptr != NULL) {
        // if node has 2 children
        if (ptr->ptrRight != NULL && ptr->ptrLeft != NULL) {
            twoCount = 1;
        // if node has 1 child
        } else if ((ptr->ptrRight != NULL || ptr->ptrLeft != NULL) && !(ptr->ptrRight != NULL && ptr->ptrLeft != NULL)) {
            oneCount = 1;
        // if node has 0 children
        } else if (ptr->ptrRight == NULL && ptr->ptrLeft == NULL) {
            count = 1;
        }

        count = numToCount == 2 ? twoCount :
                numToCount == 1 ? oneCount :
                                    count;

        return count + countChild(ptr->ptrRight, numToCount) + countChild(ptr->ptrLeft, numToCount);
    }
    return count;
}

int BSTree::rSize(TreeNodeptr &ptr) {
    if (ptr != NULL) {
        return rSize(ptr->ptrLeft) + 1 + rSize(ptr->ptrRight);
    }
    return 0;
}

int BSTree::rHeight(TreeNodeptr &ptr) {
    if (ptr != NULL) {
        int left = rHeight(ptr->ptrLeft);
        int right = rHeight(ptr->ptrRight);
        return left > right ? left+1 : right+1;
    }
    return 0;
}

bool BSTree::iSearch(string key, TreeNodeptr &ptr) {
    TreeNodeptr* current = &ptr;
    while (*current != NULL) {
        if (key == (*current)->word) {
            return true;
        } else if (key < (*current)->word) {
            // cout << "finding left" << endl;
            current = &(*current)->ptrLeft;
        } else {
            // cout << "finding right" << endl;
            current = &(*current)->ptrRight;
        }
    }
    return false;
}

void BSTree::rInsert(string s, TreeNodeptr& root) {
    if (root == NULL) {
        root = new TreeNode(s);
    } else if (s <= root->word) {
        //    cout << "going left" << endl;
        rInsert(s, root->ptrLeft);
    } else {
        //   cout << "going right" << endl;
        rInsert(s, root->ptrRight);
    }
}

void BSTree::iInsert(string s, TreeNodeptr &ptr) {
    TreeNodeptr *current = &ptr;

    while(*current != NULL) {

        if(s <= (*current)->word)
            current = &(*current)->ptrLeft;
        else
            current = &(*current)->ptrRight;
    }
    *current = new TreeNode(s);

}

string BSTree::rev_inorder(TreeNodeptr ptr, string s) {
    if (ptr != NULL) {
        s = rev_inorder(ptr->ptrRight, s);
        s += ptr->word;
        s += " ";
        s = rev_inorder(ptr->ptrLeft, s);
    }
    return s;
}

string BSTree::inorder(TreeNodeptr ptr, string s) {
    if (ptr != NULL) {
        s = inorder(ptr->ptrLeft, s);
        s += ptr->word;
        s += " ";
        s = inorder(ptr->ptrRight, s);
    }
    return s;
}

string BSTree::postorder(TreeNodeptr ptr, string s) {
    if (ptr != NULL) {
        s = postorder(ptr->ptrLeft, s);
        s = postorder(ptr->ptrRight, s);
        s += ptr->word;
        s += " ";
    }
    return s;
}

string BSTree::preorder(TreeNodeptr ptr, string s) {
    if (ptr != NULL) {
        s += ptr->word;
        s += " ";
        // cout << ptr->word << " ";
        s = preorder(ptr->ptrLeft, s);
        s = preorder(ptr->ptrRight, s);
    }
    return s;
}

void BSTree::copyTree(TreeNodeptr &thisptr, const TreeNodeptr &srcptr) {
    if (srcptr == NULL) {
        thisptr = NULL;
    } else {
        thisptr = new TreeNode(srcptr->word);
        copyTree(thisptr->ptrLeft, srcptr->ptrLeft);
        copyTree(thisptr->ptrRight, srcptr->ptrRight);
    }
}

void BSTree::rDelete(string s, TreeNodeptr &ptr) {
    if (ptr != NULL) {
        if (s==ptr->word) {
            delNode(ptr);
        } else if (s <= (ptr->word)) {
            rDelete(s, ptr->ptrLeft);
        } else {
            rDelete(s, ptr->ptrRight);
        }
    }

}

void BSTree::delNode(TreeNodeptr &ptr) {
    if (ptr->ptrLeft == NULL && ptr->ptrRight == NULL) {
        delete ptr;
        ptr = NULL;
    } else if (ptr->ptrLeft == NULL) {
        TreeNodeptr here = ptr;
        ptr = ptr->ptrRight;
        here->ptrRight = NULL;
        delete here;
    } else if (ptr->ptrRight == NULL) {
        TreeNodeptr here = ptr;
        ptr = ptr->ptrLeft;
        here->ptrLeft = NULL;
        delete here;
    } else {
        rDelete(ptr->word, ptr);
    }
}
