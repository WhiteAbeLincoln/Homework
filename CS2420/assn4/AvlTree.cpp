/* Abraham White
 * A02178955
 */

#define COMPACT
// #define DEBUG
#include "AvlTree.h"
#include <string>


AvlNode::AvlNode() {
    //    AvlNode::AvlNode("");
    this->word = "";
    this->height = 0;
    this->ptrLeft = NULL;
    this->ptrRight = NULL;
}

AvlNode::AvlNode(string s) {
    this->word = s;
    this->height = 0;
    this->ptrLeft = NULL;
    this->ptrRight = NULL;
}

AvlNode::~AvlNode() {
    delete ptrLeft;
    delete ptrRight;
}



AvlTree::AvlTree() {
    root = NULL;
}

AvlTree::AvlTree(const AvlTree &tree) {
    cout << "calling copy constructor" << endl;
    if (tree.root == NULL) {
        this->root = NULL;
    } else
        copyTree(this->root, tree.root);
}

AvlTree::~AvlTree() {
    delete this->root;
}

int AvlTree::Size() {
   return rSize(root);
}

// returns a 0 based height (leaf nodes have a height of 0)
int AvlTree::Height() {
    return rHeight(root) - 1;
}

// returns a 0 based height (leaf nodes have a height of 0) as the professor specified
int AvlTree::GetHeight(AvlNodeptr v) {
    if (v == NULL)
        return -1;
    else
        return v->height;
    // return rHeight(v);
}

string AvlTree::GetRoot() {
    return root->word;
}

AvlNodeptr AvlTree::GetRootPtr() {
    return root;
}

string AvlTree::Traverse() {
    return rev_inorder(this->root, "");//preorder(this->root, "");
}
// for debugging
string AvlTree::Traverse(int n) {
    if (n == 0) return preorder(this->root, "");
    else if (n == 1) return inorder(this->root, "");
    else if (n == 2) return postorder(this->root, "");
    else return rev_inorder(this->root, "");
}

void AvlTree::Delete(string s) {
    rDelete(s, root);
    balance(root);
}

int AvlTree::CountOneChild() {
    return countChild(root, 1);
}

int AvlTree::CountTwoChildren() {
    return countChild(root, 2);
}

int AvlTree::CountLeaves() {
    return countChild(root, 0);
}

void AvlTree::Insert(string s) {
    return rInsert(root, s);
}

bool AvlTree::Find(string s) {
    return rSearch(this->root, s);
}

int AvlTree::FindComparisons(string s) {
    return rComparisons(this->root, s);
}

void AvlTree::Print() {
    print_t(this->root);
}

// private methods


// printing for debugging
// taken from http://stackoverflow.com/questions/801740/c-how-to-draw-a-binary-tree-to-the-console and modified for strings
int AvlTree::_print_t(AvlNodeptr tree, int is_left, int offset, int depth, char s[20][255])
{
    char b[20];
    int width = 5;

    if (!tree) return 0;

    sprintf(b, "(%3s)", tree->word.c_str());

    int left  = _print_t(tree->ptrLeft,  1, offset,                depth + 1, s);
    int right = _print_t(tree->ptrRight, 0, offset + left + width, depth + 1, s);

#ifdef COMPACT
    for (int i = 0; i < width; i++)
        s[depth][offset + left + i] = b[i];

    if (depth && is_left) {

        for (int i = 0; i < width + right; i++)
            s[depth - 1][offset + left + width/2 + i] = '-';

        s[depth - 1][offset + left + width/2] = '.';

    } else if (depth && !is_left) {

        for (int i = 0; i < left + width; i++)
            s[depth - 1][offset - width/2 + i] = '-';

        s[depth - 1][offset + left + width/2] = '.';
    }
#else
    for (int i = 0; i < width; i++)
        s[2 * depth][offset + left + i] = b[i];

    if (depth && is_left) {

        for (int i = 0; i < width + right; i++)
            s[2 * depth - 1][offset + left + width/2 + i] = '-';

        s[2 * depth - 1][offset + left + width/2] = '+';
        s[2 * depth - 1][offset + left + width + right + width/2] = '+';

    } else if (depth && !is_left) {

        for (int i = 0; i < left + width; i++)
            s[2 * depth - 1][offset - width/2 + i] = '-';

        s[2 * depth - 1][offset + left + width/2] = '+';
        s[2 * depth - 1][offset - width/2 - 1] = '+';
    }
#endif

    return left + width + right;
}

int AvlTree::print_t(AvlNodeptr tree)
{
    char s[20][255];
    for (int i = 0; i < 20; i++)
        sprintf(s[i], "%80s", " ");

    _print_t(tree, 0, 0, 0, s);

    for (int i = 0; i < 20; i++)
        printf("%s\n", s[i]);
}

void AvlTree::leftRotate(AvlNodeptr &k2) {
    //std::cout<<"left rotate\n";
    AvlNodeptr k1 = k2->ptrRight;
    k2->ptrRight = k1->ptrLeft;
    k1->ptrLeft = k2;

    k2->height = /* rHeight(k2); // */  1 + max(GetHeight(k2->ptrRight), GetHeight(k2->ptrLeft));

    k1->height = 1 + max(GetHeight(k1->ptrRight), GetHeight(k1->ptrLeft));
    k2 = k1;

    #ifdef DEBUG
    cout << "after left rotate" << endl;
    this->Print();
    #endif
}

void AvlTree::rightRotate(AvlNodeptr &k2) {
    AvlNodeptr k1 = k2->ptrLeft;
    k2->ptrLeft = k1->ptrRight;
    k1->ptrRight = k2;

    k2->height = 1 + max(GetHeight(k2->ptrLeft), GetHeight(k2->ptrRight));

    k1->height = 1 + max(GetHeight(k1->ptrLeft), GetHeight(k1->ptrRight));
    k2 = k1;

    #ifdef DEBUG
    cout << "after right rotate" << endl;
    this->Print();
    #endif
}

void AvlTree::doubleRightLeftRotate(AvlNodeptr &ptr) {
    rightRotate(ptr->ptrRight);
    leftRotate(ptr);
}

void AvlTree::doubleLeftRightRotate(AvlNodeptr &ptr) {
    leftRotate(ptr->ptrLeft);
    rightRotate(ptr);
}

void AvlTree::balance(AvlNodeptr &ptr) {
    if (ptr == NULL) return;

    if ((GetHeight(ptr->ptrLeft) - GetHeight(ptr->ptrRight)) > 1) {
        if (GetHeight(ptr->ptrLeft->ptrLeft) >= GetHeight(ptr->ptrLeft->ptrRight)) // left-left case
            rightRotate(ptr);
        else // left-right case
            doubleLeftRightRotate(ptr);
    }

    if ((GetHeight(ptr->ptrRight) - GetHeight(ptr->ptrLeft)) > 1) {
        if (GetHeight(ptr->ptrRight->ptrRight) >= GetHeight(ptr->ptrRight->ptrLeft)) // right-right case
            leftRotate(ptr);
        else // right-left case
            doubleRightLeftRotate(ptr);
    }

    ptr->height = 1 + max(GetHeight(ptr->ptrLeft), GetHeight(ptr->ptrRight));
}

void AvlTree::rInsert(AvlNodeptr &ptr, string s) {
    if (ptr == NULL) {
       ptr = new AvlNode(s);
    } else {
        if (s == ptr->word)
            return;
        else if (s < ptr->word)
            rInsert(ptr->ptrLeft, s);
        else
            rInsert(ptr->ptrRight, s);
    }

    #ifdef DEBUG
    cout << "after insert " << s << endl;
    this->Print();
    #endif

    balance(ptr);
}

bool AvlTree::rSearch(AvlNodeptr ptr, string s) {
    if (ptr != NULL ) {
        if (s == ptr->word)
            return true;
        else if (s < ptr->word)
            return rSearch(ptr->ptrLeft, s);
        else
            return rSearch(ptr->ptrRight, s);
    }
    return false;
}

int AvlTree::rComparisons(AvlNodeptr ptr, string s) {
    if (ptr != NULL) {
        if (s == ptr->word)
            return 0;
        else if (s < ptr->word)
            return 1 + rComparisons(ptr->ptrLeft, s);
        else
            return 1 + rComparisons(ptr->ptrRight, s);
    }
    return 0;
}

int AvlTree::countChild(AvlNodeptr ptr, int numToCount) {
    int count = 0;
    int twoCount = 0;
    int oneCount = 0;
    if (ptr != NULL) {
        // if node has 2 children
        if (ptr->ptrRight != NULL && ptr->ptrLeft != NULL) {
            twoCount = 1;
        // if node has 1 child
        } else if ((ptr->ptrRight != NULL || ptr->ptrLeft != NULL) && !(ptr->ptrRight != NULL && ptr->ptrLeft != NULL)) {
            oneCount = 1;
        // if node has 0 children
        } else if (ptr->ptrRight == NULL && ptr->ptrLeft == NULL) {
            count = 1;
        }

        count = numToCount == 2 ? twoCount :
                numToCount == 1 ? oneCount :
                                    count;

        return count + countChild(ptr->ptrRight, numToCount) + countChild(ptr->ptrLeft, numToCount);
    }
    return count;
}

void AvlTree::rDelete(string s, AvlNodeptr &ptr) {
    if (ptr != NULL) {
        if (s==ptr->word) {
            delNode(ptr);
        } else if (s < (ptr->word)) {
            rDelete(s, ptr->ptrLeft);
        } else {
            rDelete(s, ptr->ptrRight);
        }
        #ifdef DEBUG
        cout << "after delete " << s << endl;
        this->Print();
        #endif
        balance(ptr);
    }
}

void AvlTree::delNode(AvlNodeptr &ptr) {
    if (ptr->ptrLeft == NULL && ptr->ptrRight == NULL) {
        delete ptr;
        ptr = NULL;
    } else if (ptr->ptrLeft == NULL) {
        AvlNodeptr here = ptr;
        ptr = ptr->ptrRight;
        here->ptrRight = NULL;
        delete here;
    } else if (ptr->ptrRight == NULL) {
        AvlNodeptr here = ptr;
        ptr = ptr->ptrLeft;

        // we explictly set the pointer to null on the node to be deleted
        // so that my recursive delete doesn't break our ptr->left that will get elevated
        here->ptrLeft = NULL;
        delete here;
    } else {
        AvlNodeptr temp = ptr->ptrRight;
        while(temp->ptrLeft != NULL) temp = temp->ptrLeft;
        ptr->word = temp->word;
        rDelete(temp->word, ptr->ptrRight);
    }
}

int AvlTree::rSize(AvlNodeptr ptr) {
    if (ptr != NULL) {
        return rSize(ptr->ptrLeft) + 1 + rSize(ptr->ptrRight);
    }
    return 0;
}

// returns a 1 based height (leaf nodes have a height of 1)
int AvlTree::rHeight(AvlNodeptr &ptr) {
    if (ptr != NULL) {
        int left = rHeight(ptr->ptrLeft);
        int right = rHeight(ptr->ptrRight);
        return left > right ? left+1 : right+1;
    }
    return 0;
}

string AvlTree::postorder(AvlNodeptr ptr, string s) {
    if (ptr != NULL) {
        s = postorder(ptr->ptrLeft, s);
        s = postorder(ptr->ptrRight, s);
        s += ptr->word;
        s += " ";
    }
    return s;
}

string AvlTree::preorder(AvlNodeptr ptr, string s) {
    if (ptr != NULL) {
        s += ptr->word;
        s += " ";
        // cout << ptr->word << " ";
        s = preorder(ptr->ptrLeft, s);
        s = preorder(ptr->ptrRight, s);
    }
    return s;
}

string AvlTree::rev_inorder(AvlNodeptr ptr, string s) {
    if (ptr != NULL) {
        s = rev_inorder(ptr->ptrRight, s);
        s += ptr->word;
        s += " ";
        s = rev_inorder(ptr->ptrLeft, s);
    }
    return s;
}

string AvlTree::inorder(AvlNodeptr ptr, string s) {
    if (ptr != NULL) {
        s = inorder(ptr->ptrLeft, s);
        s += ptr->word;
        s += " ";
        s = inorder(ptr->ptrRight, s);
    }
    return s;
}

void AvlTree::copyTree(AvlNodeptr &thisptr, const AvlNodeptr &srcptr) {
    if (srcptr == NULL) {
        thisptr = NULL;
    } else {
        thisptr = new AvlNode(srcptr->word);
        copyTree(thisptr->ptrLeft, srcptr->ptrLeft);
        copyTree(thisptr->ptrRight, srcptr->ptrRight);
    }
}
