#ifndef AVLTREE_H
#define AVLTREE_H
#include <string>
#include <iostream>
using namespace std ;
class AvlNode
{
    public:
        string word;
        int height;
        AvlNode *ptrLeft, *ptrRight;
        AvlNode(); // Default constructor
        ~AvlNode();
        AvlNode(string s); // Constructor with one parameter
};

typedef AvlNode * AvlNodeptr;

class AvlTree
{
private:
    AvlNodeptr root;
public:
    AvlTree();                       // Initializes root to NULL.
    AvlTree(const AvlTree &tree);   // Copy constructor
    ~AvlTree ();                    // Destructor.
    int Size ();                    // Return the number of nodes in the tree.
    int Height();                   // Return the path length from the root node to a deepest leaf node in the tree.
    void Delete(string s);          // Delete a string.
    string Traverse();              // return a string containing all strings stored in the binary search tree in
    string Traverse(int);
                                            // the descending order and print the strings in pre-order
    void Insert (string s);         // Insert a string into the binary search tree.
    bool Find (string s);           // search s in the list. Return true if s is found; otherwise, return false.
    int FindComparisons (string s); // search s in the list and return the number of comparisons to
                                            // determine whether s is in the tree.
    int CountTwoChildren();         // return the number of nodes with two children
    int CountOneChild();            // return the number of nodes with one child
    int CountLeaves();              // return the number of leave nodes
    int GetHeight(AvlNodeptr v);    // return the height of node v
    string GetRoot();               // return the word contained in the root node
    AvlNodeptr GetRootPtr();
    int rHeight(AvlNodeptr &ptr);
    void Print();
private:
                                    // You need to add the following private functions to implement the rebalance operations.
                                    // You may need to add other private functions you feel appropriate
    void balance(AvlNodeptr &);             // make the tree balanced after insert and delete
    void rightRotate (AvlNodeptr &);    // Right rotation
    void leftRotate (AvlNodeptr &);     // Left rotation
    void doubleLeftRightRotate (AvlNodeptr &);  // Left right double rotation
    void doubleRightLeftRotate (AvlNodeptr &);  // Right left double rotation

    int _print_t(AvlNodeptr tree, int is_left, int offset, int depth, char s[20][255]);
    int print_t(AvlNodeptr tree);

    void rInsert(AvlNodeptr &ptr, string s);
    bool rSearch(AvlNodeptr ptr, string s);
    int rSize(AvlNodeptr ptr);
    void copyTree(AvlNodeptr &thisptr, const AvlNodeptr &srcptr);
    string preorder(AvlNodeptr ptr, string s);
    string inorder(AvlNodeptr ptr, string s);
    string rev_inorder(AvlNodeptr ptr, string s);
    string postorder(AvlNodeptr ptr, string s);
    void delNode(AvlNodeptr &ptr);
    int countChild(AvlNodeptr ptr, int numC);
    void rDelete(string s, AvlNodeptr &ptr);
    int rComparisons(AvlNodeptr ptr, string s);
} ;
#endif
