#ifndef BSTREE_H
#define BSTREE_H
#include <string>
#include <iostream>
using namespace std;
class TreeNode {
    public:
        string word ;
        TreeNode *ptrLeft, *ptrRight ;
        TreeNode() ;  // Default constructor.
        TreeNode(string s) ;  // Constructor with one parameter
        ~TreeNode();
};
typedef TreeNode* TreeNodeptr ;
class BSTree {
    private:
        TreeNodeptr root ;
    public:
        BSTree() ;  // Initializes root to NULL.
        BSTree(const BSTree &);  //Copy constructor
        ~BSTree() ;  // Destructor.
        int Size ();  // Return the number of nodes in the tree.
        int Height(); // Return the path length from the root node to a deepest leaf node in the tree.
        void Delete(string s) ; // Delete a string.
        string Traverse() ;  // return a string containing all strings stored in the binary search tree in                                             // the descending order.
        string Traverse(int n);

        void Insert (string s) ;  // Insert a string into the binary search tree.
        bool Find (string s) ;   // search s in the list.  Return true if s is found; otherwise, return false.
        int FindComparisons(string s);
        int CountTwoChildren();         // return the number of nodes with two children
        int CountOneChild();            // return the number of nodes with one child
        int CountLeaves();              // return the number of leave nodes
        int GetHeight(TreeNodeptr v);    // return the height of node v
        string GetRoot();               // return the word contained in the root node
        void Print();

    private:  // You need to add private functions to implement the recursive solutions.
        void rDelete(string s, TreeNodeptr& ptr);
        void delNode(TreeNodeptr& ptr);

        int _print_t(TreeNodeptr tree, int is_left, int offset, int depth, char s[20][255]);
        int print_t(TreeNodeptr tree);
        string preorder(TreeNodeptr ptr, string s);
        string inorder(TreeNodeptr ptr, string s);
        string rev_inorder(TreeNodeptr ptr, string s);
        string postorder(TreeNodeptr ptr, string s);
        int rComparisons(TreeNodeptr ptr, string s);
        int countChild(TreeNodeptr ptr, int numC);
        int rSize(TreeNodeptr &ptr);
        int rHeight(TreeNodeptr &ptr);
        bool iSearch(string key, TreeNodeptr &ptr);
        void iInsert(string s, TreeNodeptr &ptr);
        // for testing
        void rInsert(string s, TreeNodeptr &root);
        void copyTree(TreeNodeptr &thisptr, const TreeNodeptr &srcptr);
};
#endif
