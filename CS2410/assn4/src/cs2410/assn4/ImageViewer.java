/*
 * Created by Abraham White
 * 2015-02-18
 * A02178955
 * CS 2410-001
 */
package cs2410.assn4;

// IMPORTS {{{
    import java.io.FileNotFoundException;
    import java.io.FileReader;
    import java.io.PrintWriter;
    import java.net.MalformedURLException;
    import java.net.URL;
    import java.util.ArrayList;
    import java.util.List;
    import java.util.Scanner;

    import java.awt.*;
    import java.awt.event.ActionEvent;
    import java.awt.event.ActionListener;
    import java.awt.event.WindowEvent;
    import java.awt.event.WindowListener;

    import javax.swing.*;
//}}}

/**
 * Allows one to view images from the internet, specified by url in a file
 *
 * @author Abe
 */
public class ImageViewer {
// GLOBALS {{{
    private JLabel imageLabel;
    private ImageIcon currImage;
    private JTextField txtAdd;
    private JButton btnNext;
    private JButton btnPrev;
    private JButton btnDel;
    private JButton btnAdd;
    private ActionListener delListener;
    private ActionListener addListener;
    private ActionListener nextListener;
    private ActionListener prevListener;

    private String listLocation;
    private List<String> urls;
    private int currIndex;
// }}}

    List<String> readFile() {
        List<String> urlList = new ArrayList<String>();

        try {
            Scanner inFile = new Scanner(new FileReader(this.listLocation));
            while(inFile.hasNextLine()) {
                urlList.add(inFile.nextLine());
            }

            inFile.close();

        } catch (FileNotFoundException e) {
            System.err.println("Cannot open " + this.listLocation + " for reading");
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        return urlList;
    }

    void saveFile() {
        try {
            PrintWriter outFile = new PrintWriter(this.listLocation);
            for (int i = 0; i < urls.size(); i++) {
                outFile.println(urls.get(i));
            }
            outFile.close();
        } catch (FileNotFoundException e) {
            System.err.println("Cannot open " + this.listLocation + " for reading");
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * loads the image into the label
     */
    void setImage() {
        // if the last image in the list was deleted, the index will equal the size.
        // display the previous image in that case.
        if (currIndex >= urls.size()) {
            currIndex--;
        }

        if (urls.size() < 1) {
            imageLabel.setIcon(null);
            imageLabel.setText("No Image");
            // disable all buttons except for add
            btnNext.setEnabled(false);
            btnDel.setEnabled(false);
            btnPrev.setEnabled(false);
            return;
        }

        if (! btnNext.isEnabled()) {
            btnNext.setEnabled(true);
            btnDel.setEnabled(true);
            btnPrev.setEnabled(true);
        }

        try {
            System.out.println("viewing: " + this.urls.get(currIndex));

            // this is really messy just to resize the image to 250x250
            // do I really have to create another object?
            currImage = new ImageIcon(new ImageIcon(new URL(this.urls.get(currIndex))).getImage().getScaledInstance(250,250, Image.SCALE_SMOOTH));
            imageLabel.setIcon(currImage);
        } catch (MalformedURLException e) {
            System.err.println("Cannot open " + this.urls.get(currIndex));
            System.err.println(e.getMessage());
        }
    }

    /**
     * initializes the delete listener function
     */
    private void initDelListener() {
        delListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("deleting: " + urls.get(currIndex));
                urls.remove(currIndex);
                setImage();
            }
        };
    }

    /**
     * initializes the next listener function
     */
    private void initNextListener() {
        nextListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currIndex++;
                if (currIndex >= urls.size()) {
                    currIndex = 0;
                }
                setImage();
            }
        };
    }

    /**
     * initializes the previous listener function
     */
    private void initPrevListener() {
        prevListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currIndex--;
                if (currIndex <= 0) {
                    currIndex = urls.size() - 1;
                }
                setImage();
            }
        };
    }

    /**
     * initializes the add listener function
     */
    private void initAddListener() {
        addListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String addCache = txtAdd.getText();
                if (! addCache.isEmpty()) {

                    // remove the first slash to toggle insert functionality

                    //*
                    // DEFAULT: insert at end of list, and move to that index
                        urls.add(addCache);
                        currIndex = urls.size() - 1;
                    /*/
                     //insert at current index, and shift everything after
                        currIndex++;
                        urls.add(currIndex, addCache);
                    //*/
                    System.out.println("adding: " + addCache);
                    txtAdd.setText("");
                    setImage();
                }
            }
        };
    }

    /**
     * creates the GUI and shows it.
     */
    private void createGUI() {
        JFrame frame = new JFrame("ImageViewer");
        // we leave 100 px for the controls
        frame.setSize(250, 350);
        Container pane = frame.getContentPane();
        pane.setLayout(null);

        // initialize my listeners
        initDelListener();
        initAddListener();
        initNextListener();
        initPrevListener();

        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}

            @Override
            public void windowClosing(WindowEvent e) {
                // save the url list
                saveFile();
            }

            @Override
            public void windowClosed(WindowEvent e) {}

            @Override
            public void windowIconified(WindowEvent e) {}

            @Override
            public void windowDeiconified(WindowEvent e) {}

            @Override
            public void windowActivated(WindowEvent e) {}

            @Override
            public void windowDeactivated(WindowEvent e) {}
        });

        btnNext = new JButton("->");
        btnPrev = new JButton("<-");
        btnAdd = new JButton("+");
        btnDel = new JButton("-");
        txtAdd = new JTextField();

        // load the inital image
        imageLabel = new JLabel();
        setImage();
        imageLabel.setBounds(0,0,250,250);

        // attach the listeners
        btnNext.addActionListener(nextListener);
        btnPrev.addActionListener(prevListener);
        btnAdd.addActionListener(addListener);
        btnDel.addActionListener(delListener);

        // set the button positions
        btnDel.setBounds(0, 300, 50, 50);
        btnAdd.setBounds(200, 300, 50, 50);
        txtAdd.setBounds(50, 300, 150, 50);
        btnPrev.setBounds(0, 250, 50, 50);
        btnNext.setBounds(200, 250, 50, 50);

        pane.add(imageLabel);
        pane.add(btnPrev);
        pane.add(btnNext);
        pane.add(txtAdd);
        pane.add(btnAdd);
        pane.add(btnDel);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public ImageViewer() {
        this.listLocation = "data/assn3data.txt";
        this.urls = readFile();
        // start at the first image
        this.currIndex = 0;

        // Creates the GUI. Most of the code is in this function
        createGUI();
    }

    public static void main(String[] argv) {
        System.out.println("pwd: " + System.getProperty("user.dir"));
        new ImageViewer();
    }

}
// vim: fdm=marker
