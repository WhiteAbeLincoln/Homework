<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Java Language](#java-language)
  - [JDK & JRE](#jdk-&-jre)
  - [Process of java program](#process-of-java-program)
  - [Javadoc](#javadoc)
  - [Formal vs Actual Parameters](#formal-vs-actual-parameters)
  - [Keyworks & Reserved Words](#keyworks-&-reserved-words)
  - [Primitive Data Types](#primitive-data-types)
    - [Autoboxing](#autoboxing)
- [Classes](#classes)
  - [Access Levels](#access-levels)
  - [Inheritance](#inheritance)
  - [Polymorphism](#polymorphism)
  - [Abstract](#abstract)
  - [Misc](#misc)
- [Interfaces](#interfaces)
  - [Default Methods](#default-methods)
- [GUIs](#guis)
  - [Important Components](#important-components)
  - [Layouts](#layouts)
    - [Types of Layout Mangers](#types-of-layout-mangers)
      - [Grid Layout](#grid-layout)
      - [Box Layout](#box-layout)
      - [Border Layout](#border-layout)
      - [GridBag Layout](#gridbag-layout)
- [Important Libraries](#important-libraries)
  - [Java Collections Framework](#java-collections-framework)
- [Threads](#threads)
  - [Definitions](#definitions)
  - [Thread States](#thread-states)
  - [Using Threads](#using-threads)
  - [Synchronization](#synchronization)
    - [Methods](#methods)
    - [Statements](#statements)
  - [SwingWorker](#swingworker)
  - [Timer](#timer)
  - [Event Dispatch Thread](#event-dispatch-thread)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Java Language

## JDK & JRE
+ JDK is the development kit - contains javac
+ JRE is the runtime - the vm to run the program

## Process of java program
1. Edit
2. Compile
3. Load
4. Verify
5. Interpret

## Javadoc
Starts with `/**`

## Formal vs Actual Parameters
Formal parameters are found in the function definition.  
Actual parameters are what is passed when called

## Keyworks & Reserved Words
+ `abstract` - prevents instantiation, or allows deferring implementation
+ `assert` - used in testing
+ `boolean` - type
+ `break` - end execution of current loop
+ `byte` - sequence of 8 bits
+ `case` - statement in switch block
+ `catch` - try-catch, contains code for when exception is thrown
+ `char` - type
+ `class` - type that defines implementation of object
+ `const` - not used
+ `continue` - resume execution at end of loop
+ `default` - statement in switch block, used if no matching case
+ `do` - part of do-while loop
+ `double` - type
+ `else` - part of if-else statement
+ `enum` - keyword to declare enumerated type, extends class Enum
+ `extends` - used to specify super\* in interface or class
+ `final` - defines an entity to be constant
+ `finally` - try-catch, executed always, after catch block
+ `float` - type
+ `for` - specifies for loop
+ `goto` - not used
+ `if` - specifies if statement
+ `implements` - used in class to specify one or more interfaces
+ `import` - used to specify classes or packages to include
+ `instanceof` - evaluates to true if object is instance of other
+ `int` - type
+ `interface` - declares special class, only abstract methods, constants, and static interfaces
+ `long` - type
+ `native` - used in methods to specify method comes from other lang
+ `new` - create an instance of class or array
+ `package` - group of types
+ `private` - keyword
+ `protected` - keyword
+ `public` - keyword
+ `return` - used to finish execution of method
+ `short` - type
+ `static` - used to declare field, method, inner class as class member
+ `strictfp` - restricts precision and rounding of fp calculations
+ `super` - used to access members of the parent class
+ `switch` - part of switch statement
+ `synchronized` - gets a lock on an object; for threading
+ `this` - represents instance of the class
+ `throw` - causes declared exception instance to be thrown
+ `throws` - used in method declarations to specify which exceptions aren't handled, but passed on
+ `transient` - delcares instance to be not part of serialized form of an object
+ `try` - part of try-catch block
+ `void` - used in method declaration, show method doesn't return value
+ `volatile` - used in field to specify var is modified asynchronously by concurrent threads
+ `while` - used to create while loop
+ `false` - boolean literal
+ `true` - boolean literal
+ `null` - reference literal

## Primitive Data Types

Primitive | Wrapper Class
----------|--------------
boolean   | Boolean
byte      | Byte
char      | Character
float     | Float
int       | Integer
long      | Long
short     | Short
double    | Double

### Autoboxing

+ Autoboxing - automatic conversion of primitive to wrapper class
+ Autounboxing - automatic conversion of wrapper to primitive

----------------------------------------------------

# Classes

## Access Levels

Modifier  |Class|Package|Subclass|World|
----------|:---:|:-----:|:------:|:---:|
public    | Y   |   Y   |   Y    | Y   |
protected | Y   |   Y   |   Y    | N   |
none(pkg) | Y   |   Y   |   N    | N   |
private   | Y   |   N   |   N    | N   |

## Inheritance
+ No multiple inheritance
+ keyword 'extends'
+ 'is-a' type of relationship
+ new class is a sub-class/child class
+ don't override variables (shadow variables)
+ method binding
  + java uses late binding

## Polymorphism
+ means many forms
+ subclass can be treated as any of its superclasses
+ use 'instanceof' to determine type of object
+ can cast an object from one of its types to another

## Abstract
+ abstract methods must be defined to be used
  + usually through inheritance
+ class must be declared abstract if abstract method exists
+ class can be abstract
+ abstract class can't be instantiated.

## Misc

Scanner Class: reads from some input - file or stdin

Math uses static,final in class definition, private on constructor to prevent instantiation

Typically overridden
+ `toString()`
+ `equals()`

Abstract methods must be defined in subclasses

----------------------------------------------------

# Interfaces

Interfaces contain only abstract methods and constants

+ All methods are public abstract (don't need to specify)
+ implementing class must define every method
+ polymorphism applies
+ can extend other interfaces
+ a class can implement multiple interfaces

## Default Methods

Enables adding new functionality to an interface but ensure compatibility with code written for older versions of interface

example:

```java
public interface IMyInterface {
    void setSomething(int variable, int minute, int second);
    void getSomething(String something);
    default getSomethingNew(String something) {
        return something+"blah";
    }
}
```

----------------------------------------------------

# GUIs

## Important Components
+ [**JFrame**](http://docs.oracle.com/javase/7/docs/api/javax/swing/JFrame.html)
  + `setVisible(bool b)` - last method called, displays frame
  + `setDefaultCloseOperation(int operation)` - sets behavior on close
  + `getContentPane()` - gets the content pane, usually JPanel
+ [**Container**](http://docs.oracle.com/javase/7/docs/api/java/awt/Container.html) - a component that can contain other components
+ [**JLabel**](http://docs.oracle.com/javase/7/docs/api/javax/swing/JLabel.html) - a display area for short text string and/or image 
+ [**JTextField**](https://docs.oracle.com/javase/8/docs/api/index.html?javax/swing/JTextField.html) - allows editing of single line of text
+ [**JButton**](http://docs.oracle.com/javase/7/docs/api/javax/swing/JButton.html) - action button, can be clicked
+ [**JPanel**](http://docs.oracle.com/javase/7/docs/api/javax/swing/JPanel.html) - generic lightweight container
+ [**JOptionPane**](http://docs.oracle.com/javase/7/docs/api/javax/swing/JOptionPane.html) - easy to make standard dialog box
+ [**JMenuBar**](https://docs.oracle.com/javase/8/docs/api/index.html?javax/swing/JMenuItem.html) - contains the JMenu s. Add with JFrame.setJMenuBar(bar)
+ [**JMenu**](https://docs.oracle.com/javase/8/docs/api/index.html?javax/swing/JMenuItem.html) - drop down list. Contains the JMenuItems
+ [**JMenuItem**](https://docs.oracle.com/javase/8/docs/api/index.html?javax/swing/JMenuItem.html) - individual, clickable item. Can add key alias
+ [**JTextArea**](https://docs.oracle.com/javase/8/docs/api/index.html?javax/swing/JTextArea.html) - multi-line area, displays plain text
+ [**JComboBox**](http://docs.oracle.com/javase/7/docs/api/javax/swing/JComboBox.html) - combines a button or field and a drop-down list
+ [**JCheckBox**](http://docs.oracle.com/javase/7/docs/api/javax/swing/JCheckBox.html) - check box. can be selected or deselected and shows state
+ [**JRadioButton**](http://docs.oracle.com/javase/7/docs/api/javax/swing/JRadioButton.html) - radio button. only one at a time can be selected. Use with ButtonGroup
+ [**ButtonGroup**](http://docs.oracle.com/javase/7/docs/api/javax/swing/ButtonGroup.html) - Multiple-exclusion for a set of buttons. Only one button in set can be "on" at a time
+ [**Font**](https://docs.oracle.com/javase/8/docs/api/index.html?java/awt/Font.html) - Represents fonts, can render text.
+ [**MIDI**](http://docs.oracle.com/javase/7/docs/api/javax/sound/midi/package-summary.html) - Midi package. See [SoundDemo.java](/MidiSounds/src/cs2410/demo/SoundDemo.java)

## Layouts

Layout managers do two basic things
  + Calculate min/preferred/max sizes for a container
  + Lay out the containers children

### Types of Layout Mangers

#### Grid Layout
Makes a bunch of components equal in size and displays them in the requested number of rows and columns

**Important Methods**  
`GridLayout(int rows, int cols)`  
`GridLayout(int rows, int cols, int hgap, int vgap)`  
`void setVgap(int vgap)`  
`void setHgap(int hgap)`  

#### Box Layout
Puts components in a single row or column, specified in constructor

**Important Methods:**  
`BoxLayout(Container c, int axis)`  
  where axis is one of X\_AXIS, Y\_AXIS, LINE\_AXIS, PAGE\_AXIS

#### Border Layout
Default Layout.
Places components in up to five areas: **PAGE**\_**START**, **PAGE**\_**END**, **LINE**\_**START**, **LINE**\_**END**, **CENTER**

**Important Methods**  
using Container pane  
`pane.add(Component c, int constraints)`  
  where constraints is one of the previously listed areas

#### GridBag Layout
Aligns components by placing them within a grid of cells. Components can span more than one cell.
Use the `GridBagConstraints` class to configure layout

**Important Methods**  
using Container pane  
`pane.add(Component c, Object constraint)`  
  where the constraint is an object of the GridBagConstraints class

set the following instance variables  
  `GridBagConstraints.gridx`, `GridBagContstraints.gridy`
  `GridBagConstraints.gridwidth, `GridBagConstraints.gridheight`
  `GridBagConstraints.fill

--------------------------------------------------------------------

# Important Libraries

## Java Collections Framework

A library of classes and interfaces.
derived from `java.util.Collection` interface
most found in `java.util.*`

Contains:
+ Lists - `java.util.List`
+ Stacks - `java.util.Stack`
+ Queues - `java.util.Queue`
+ Sets - `java.util.Set`
+ Iterator

# Threads

## Definitions

**Concurrency:**
  fake threading. rapid switching between processes

**Parallelism:**
  processes run on separate cores/CPUs

## Thread States
+ **New**
+ **Runnable**
+ **Waiting**
+ **Timed Waiting**
+ **Terminated**
+ **Blocked**

## Using Threads
Use any class that implements `Runnable` - lambdas will work. Call `start()` to start the thread

## Synchronization

Uses the Java Monitor. Monitor handles the threads, and determines blocking or cooperation
Mutual exclusion uses object locks (see method and statements below)

### Methods
Add the `synchronized` keyword to a method declaration.  
It has 2 effects
1. While a synchronized method is being called, all other threads with synchronized methods block until first thread is done
2. When method exits, establishes _happens-before_ relationship with subsequent invocations. Changes to state are visible to all threads

### Statements
Specify the object that provides lock.
```java
public void addName(String name) {
    synchronized(this) {
        lastName = name;
        nameCount++;
    }
    nameList.add(name);
}
```
Uses an object to indicate lock on a block of code

## SwingWorker
See [**SwingWorker**](http://docs.oracle.com/javase/7/docs/api/javax/swing/SwingWorker.html)  
3 important methods:
+ `doInBackground()` - where all background activities occur. Main code area
+ `done()` - callback. Called when process is finished
+ `execute()` - Schedules the `SwingWorker` for the execution on a separate _worker_ thread. Can only call once

## Timer
See [**Timer**](https://docs.oracle.com/javase/8/docs/api/index.html?java/util/Timer.html)  
3 important methods:
+ `Timer()` - creates a new timer
  + `Timer(boolean isDaemon)
  + `Timer(String name)
  + `Timer(String name, boolean is Daemon)
+ [`TimerTask`](http://docs.oracle.com/javase/7/docs/api/java/util/TimerTask.html) - task that can be scheduled for one-time or repeated execution by a Timer
+ `cancel()` - terminates the timer, discarding any scheduled tasks

## Event Dispatch Thread
Event handling code runs on the Event Dispatch Thread. Swing methods aren't "thread safe", so they must use this thread.

Start a GUI using the following:
```java
SwingUtilities.invokeLater(new Runnable() {
    public void run() {
        blahBlah()
    }
});

// OR

EventQueue.invokeLater(() -> {
    // this is a lambda
    blahBlah()
});
```
