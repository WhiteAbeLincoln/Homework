package cs2410.assn7.components;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.prefs.Preferences;

import static java.lang.Math.ceil;
import static java.lang.Math.sqrt;

/**
 * Created by abe on 4/7/16.
 * Grid to hold Color Panels
 *
 * @see cs2410.assn7.components.GridPanel
 * @author abe
 */
public class GridPanel extends JPanel implements SequenceListener, StartListener {
    private int square;
    private Preferences prefs;

    private boolean playerTurn = false;
    private Color[] colors;
    private List<ColorPanel> panelList = new ArrayList<>();
    private List<ColorPanel> orderList = new ArrayList<>();
    // does it make sense to have multiple score panels for a single grid?
    private IScoreKeeper scorePanel;
    private Iterator<ColorPanel> iter;

    /**
     * Creates a new grid with the default number of panels
     * @param scorePanel ScoreKeeper associated
     */
    public GridPanel(IScoreKeeper scorePanel) {
        this(new Color[] {Color.YELLOW, Color.BLUE, Color.RED, Color.GREEN}, scorePanel);
    }

    /**
     * Creates a new grid with the specified number of panels from Color array
     * @param colors the Color array
     * @param scorePanel associated ScoreKeeper
     */
    public GridPanel(Color[] colors, IScoreKeeper scorePanel) {
        this.prefs = Preferences.userRoot().node(PrefKeys.MAIN_NAME);
        this.scorePanel = scorePanel;
        this.colors = colors;
        this.square = (int)ceil(sqrt(colors.length));
        this.setLayout(new GridLayout(square, square, 3, 3));
        this.setBackground(Color.GRAY);

        for (Color color : colors) {
            ColorPanel panel = new ColorPanel(color);
            panel.addListener(this);
            this.panelList.add(panel);
            this.add(panel);
        }
    }

    /**
     * returns the colors making up the grid
     * @return Color array
     */
    public Color[] getColors() {
        return colors;
    }

    @Override
    public void panelClicked(ColorPanel panel) {
        if (!playerTurn) return;
        if (!isCorrectClick(panel)) {
            this.gameOver();
        } else if (!iter.hasNext()) {
            this.roundWon();
            this.computerTurn();
        }
    }

    /**
     * tells score keeper to increment on win
     */
    private void roundWon() {
        scorePanel.incrementScore();
    }

    /**
     * tells score keeper to end game on loose
     */
    private void gameOver() {
        scorePanel.endGame();
    }

    /**
     * Determines if a click was correct
     * @param panel the panel clicked
     * @return true if the sequence was correct
     */
    private boolean isCorrectClick(ColorPanel panel) {
        return iter.next().equals(panel);
    }

    /**
     * runs the computers turn in a separate thread
     */
    private void computerTurn() {

        new Thread(() -> {

            playerTurn = false;

            if (prefs.getBoolean(PrefKeys.REVERSE, false))
                reverse();

            // brief pause between computer and human rounds
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            addPanel();

            iter = orderList.iterator();

            while (iter.hasNext()) {
                ColorPanel temp = iter.next();

                temp.sequence(getSpeed());

                try {
                    Thread.sleep(getSpeed());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (prefs.getBoolean(PrefKeys.REVERSE, false))
                reverse();

            if (prefs.getBoolean(PrefKeys.SCRAMBLE, false))
                shuffle();

            iter = orderList.iterator();
            playerTurn = true;
        }).start();
    }

    /**
     * reverses the order for Reverse mode
     */
    private void reverse() {
        Collections.reverse(orderList);
    }

    /**
     * shuffles the grid for Scramble mode
     */
    private void shuffle() {
        panelList.forEach(this::remove);

        Collections.shuffle(panelList);

        panelList.forEach(this::add);

        this.update(this.getGraphics());
        this.revalidate();
    }

    /**
     * adds a panel to the order list sequence
     */
    private void addPanel() {
        Random rand = new Random();
        orderList.add(panelList.get(rand.nextInt(colors.length)));
    }

    /**
     * gets the speed for panels to blink during computer turn
     * @return speed in milliseconds
     */
    private int getSpeed() {
        if (prefs.getBoolean(PrefKeys.ADAPTIVE, false)) {
            return 1000 / (scorePanel.getScore()+1);
        } else {
            // return saved speed with a default (unset) value of 1000 - slow
            return prefs.getInt(PrefKeys.SPEED, 1000);
        }
    }

    @Override
    public void gameStarted() {
        orderList.clear();
        computerTurn();
    }
}
