package cs2410.assn7.components;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

/**
 * Created by abe on 4/7/16.
 * Implementation of IScoreKeeper to keep track and display of the scores
 *
 * @see cs2410.assn7.components.IScoreKeeper
 * @author abe
 */
public class ScorePanel extends JPanel implements IScoreKeeper {
    private final int JOKER_LOW = 0;
    private final int KNIGHT_LOW = 4;
    private final int KING_LOW = 10;
    private final String JOKER_TEXT = "Joker";
    private final String KNIGHT_TEXT = "Knight";
    private final String KING_TEXT = "King";

    private int score = 0;
    private int highScore = 0;
    private boolean newHighScore = false;
    private JLabel lblCurrScore = new JLabel();
    private JLabel lblHighScore = new JLabel();
    private List<StartListener> listeners = new ArrayList<>();

    /**
     * Creates an new ScorePanel
     */
    public ScorePanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        JButton btnStart = new JButton("Start");
        btnStart.addActionListener(actionEvent -> {
                    resetScore();
                    listeners.forEach(StartListener::gameStarted);
                }
        );
        this.add(lblCurrScore);
        this.add(btnStart);
        this.add(lblHighScore);
        this.updateView();
    }

    /**
     * resets the score
     */
    private void resetScore() {
        score = 0;
        newHighScore = false;
        updateView();
    }

    /**
     * updates the view, loading the highscore and current score
     */
    private void updateView() {
        lblHighScore.setText("High Score: " + highScore);
        lblCurrScore.setText("Score: " + score);
        this.update(this.getGraphics());
    }

    /**
     * Adds a StartListener
     * @param toAdd StartListener that wants to subscribe
     */
    public void addListener(StartListener toAdd) {
        listeners.add(toAdd);
    }

    /**
     * Removes a StartListener
     * @param toRemove StartListener that wants to unsubscribe
     */
    public void removeListener(StartListener toRemove) {
        listeners.remove(toRemove);
    }

    /**
     * Saves the game record to disk
     */
    private void saveGame() {
        String homeDir = System.getProperty("user.home");
        String filePath = homeDir + "/" + ".simonish_games";
        String entry = Integer.toString(score) + System.lineSeparator();
        File file = new File(filePath);
        try {
            if (!file.exists())
                file.createNewFile();
            Files.write(file.toPath(), entry.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Static method to read the high score CSV
     * @param p the path to the csv
     * @return A list of tuples of each entry
     */
    static List<Map.Entry<Integer,String>> readScoreCSV(Path p) {
        List<Map.Entry<Integer, String>> entries = new ArrayList<>();
        try {
            Files.readAllLines(p).forEach(line -> {
                String[] cols = line.split(",");
                int score = Integer.parseInt(cols[0]);
                String entryName = cols[1];
                entries.add(new AbstractMap.SimpleEntry<>(score, entryName));
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return entries;
    }

    /**
     * Saves the current high score to disk
     * @param name player name to save with
     */
    private void saveHighScore(String name) {
        String homeDir = System.getProperty("user.home");
        String filePath = homeDir + "/" + ".simonish_scores";
        File file = new File(filePath);
        String separator = System.lineSeparator();

        try {
            if (!file.exists())
                file.createNewFile();

            List<Map.Entry<Integer,String>> entries = readScoreCSV(file.toPath());

            entries.add(new AbstractMap.SimpleEntry<>(this.highScore, name));

            Collections.sort(entries, (entry1, entry2) -> {
                if (entry1.getKey() < entry2.getKey())
                    return -1;
                else if (entry1.getKey().equals(entry2.getKey()))
                    return 0;
                else
                    return 1;
            });

            String toWrite = "";

            for (int i = 0; i < 10; i++) {
                if (i >= entries.size())
                    break;
                toWrite += entries.get(i).getKey().toString() + "," + entries.get(i).getValue() + separator;
            }

            Files.write(file.toPath(), toWrite.getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void incrementScore() {
        this.score++;
        if (score > highScore) {
            highScore = score;
            newHighScore = true;
        }
        updateView();
    }

    @Override
    public void endGame() {
        String message = "Rank: ";
        if (score > JOKER_LOW && score < KNIGHT_LOW) {
            message = message + JOKER_TEXT;
        } else if (score > KNIGHT_LOW && score < KING_LOW) {
            message = message + KNIGHT_TEXT;
        } else if (score > KING_LOW) {
            message = message + KING_TEXT;
        }

        message = message + "\nFinal Score: " + score;

        if (newHighScore) {
            message = message + "\nCongrats! You set a new High Score!";
        }

        saveGame();

        JOptionPane.showMessageDialog(this, message, "GAME OVER", JOptionPane.INFORMATION_MESSAGE);

        if (newHighScore) {
            String name = JOptionPane.showInputDialog(this, "Enter Name");
            saveHighScore(name);
        }
        resetScore();
    }

    @Override
    public int getScore() {
        return score;
    }
}
