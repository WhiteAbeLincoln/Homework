package cs2410.assn7.components;

/**
 * Created by abe on 4/7/16.
 * Interface to listen for a new game
 *
 * @author abe
 */
interface StartListener {
    void gameStarted();
}
