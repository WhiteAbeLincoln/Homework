package cs2410.assn7.components;

/**
 * Static class that contains preference keys
 *
 * @author abe
 */
public final class PrefKeys {
    public final static String SCRAMBLE = "scramble";
    public final static String REVERSE = "reverse";
    public final static String SOUNDS = "sounds";
    public final static String IMAGES = "images";
    public final static String SPEED = "speed";
    public final static String SIZE = "size";
    public final static String MAIN_NAME = "simonish_advanced";
    public final static String ADAPTIVE = "adaptive";
    private PrefKeys() {}

}
