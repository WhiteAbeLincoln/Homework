package cs2410.assn7.components;

import cs2410.assn7.game.SimonishGame;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.List;
import java.util.prefs.Preferences;

/**
 * Created by abe on 4/8/16.
 * Menu, manages setting preferences
 * @author abe
 */
public class MenuPanel extends JMenuBar implements ActionListener {
    private static final int SPEED_SLOW = 1000;
    private static final int SPEED_FAST = 200;
    private static final int SPEED_MED = 500;
    private Preferences prefs;
    private SimonishGame parent;

    private JMenuItem itmColor;
    private JMenuItem itmScore;
    private JMenuItem itmHistory;
    private JMenuItem itmAbout;
    private JMenuItem itmRules;
    private JCheckBoxMenuItem itmScramble;
    private JCheckBoxMenuItem itmReverse;
    private JRadioButtonMenuItem itmFast;
    private JRadioButtonMenuItem itmMed;
    private JRadioButtonMenuItem itmSlow;
    private JRadioButtonMenuItem itmAdaptive;
    private JCheckBoxMenuItem itmImages;
    private JCheckBoxMenuItem itmSounds;
    private JRadioButtonMenuItem itmSize2;
    private JRadioButtonMenuItem itmSize3;
    private JRadioButtonMenuItem itmSizeCustom;

    /**
     * Creates a new Simonish MenuPanel
     * @param parent the parent window (JFrame)
     */
    public MenuPanel(SimonishGame parent) {
        this.parent = parent;
        prefs = Preferences.userRoot().node(PrefKeys.MAIN_NAME);
        int sizeDefault = prefs.getInt(PrefKeys.SIZE, 0);
        int speedDefault = prefs.getInt(PrefKeys.SPEED, SPEED_SLOW);
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        ButtonGroup speedGroup = new ButtonGroup();
        ButtonGroup sizeGroup = new ButtonGroup();

        JMenu menuSettings = new JMenu("SETTINGS");
        menuSettings.setMnemonic(KeyEvent.VK_S);
        JMenu menuStats = new JMenu("STATS");
        menuStats.setMnemonic(KeyEvent.VK_T);
        JMenu menuHelp = new JMenu("HELP");
        menuHelp.setMnemonic(KeyEvent.VK_H);

        JMenu itmMenuGPlay = new JMenu("Game Settings");
        JMenu itmMenuMode = new JMenu("Mode");
        JMenu itmMenuSpeed = new JMenu("Speed");
        JMenu itmMenuAnnoyances = new JMenu("Annoyances");
        JMenu itmMenuSize = new JMenu("Size");

        itmColor = new JMenuItem("Choose Color");
        itmScore = new JMenuItem("High Scores");
        itmHistory = new JMenuItem("Play History");
        itmAbout = new JMenuItem("About");
        itmRules = new JMenuItem("Rules");
        itmScramble = new JCheckBoxMenuItem("Scramble", prefs.getBoolean(PrefKeys.SCRAMBLE, false));
        itmReverse = new JCheckBoxMenuItem("Reverse", prefs.getBoolean(PrefKeys.REVERSE, false));
        itmFast = new JRadioButtonMenuItem("Fast", speedDefault == SPEED_FAST && !prefs.getBoolean(PrefKeys.ADAPTIVE, false));
        itmMed = new JRadioButtonMenuItem("Medium", speedDefault == SPEED_MED && !prefs.getBoolean(PrefKeys.ADAPTIVE, false));
        itmSlow = new JRadioButtonMenuItem("Slow", speedDefault == SPEED_SLOW && !prefs.getBoolean(PrefKeys.ADAPTIVE, false));
        itmAdaptive = new JRadioButtonMenuItem("Adaptive", prefs.getBoolean(PrefKeys.ADAPTIVE, false));
        itmImages = new JCheckBoxMenuItem("Images", prefs.getBoolean(PrefKeys.IMAGES, false));
        itmSounds = new JCheckBoxMenuItem("Sounds", prefs.getBoolean(PrefKeys.SOUNDS, false));
        itmSize2 = new JRadioButtonMenuItem("2x2", sizeDefault == 0);
        itmSize3 = new JRadioButtonMenuItem("3x3", sizeDefault == 1);
        itmSizeCustom = new JRadioButtonMenuItem("Custom", sizeDefault != 0 && sizeDefault != 1);
        itmSizeCustom.addActionListener(this);
        itmSize3.addActionListener(this);
        itmSize2.addActionListener(this);
        itmSounds.addActionListener(this);
        itmImages.addActionListener(this);
        itmAdaptive.addActionListener(this);
        itmSlow.addActionListener(this);
        itmMed.addActionListener(this);
        itmFast.addActionListener(this);
        itmReverse.addActionListener(this);
        itmScramble.addActionListener(this);
        itmRules.addActionListener(this);
        itmAbout.addActionListener(this);
        itmHistory.addActionListener(this);
        itmScore.addActionListener(this);
        itmColor.addActionListener(this);

        speedGroup.add(itmFast);
        speedGroup.add(itmMed);
        speedGroup.add(itmSlow);
        speedGroup.add(itmAdaptive);

        sizeGroup.add(itmSize2);
        sizeGroup.add(itmSize3);
        sizeGroup.add(itmSizeCustom);

        itmMenuGPlay.add(itmMenuMode);
        itmMenuGPlay.add(itmMenuSpeed);
        itmMenuGPlay.add(itmMenuSize);
        itmMenuGPlay.add(itmMenuAnnoyances);

        itmMenuMode.add(itmScramble);
        itmMenuMode.add(itmReverse);

        itmMenuSpeed.add(itmSlow);
        itmMenuSpeed.add(itmMed);
        itmMenuSpeed.add(itmFast);
        itmMenuSpeed.add(itmAdaptive);

        itmMenuSize.add(itmSize2);
        itmMenuSize.add(itmSize3);
        itmMenuSize.add(itmSizeCustom);

        itmMenuAnnoyances.add(itmSounds);
        itmMenuAnnoyances.add(itmImages);

        menuSettings.add(itmColor);
        menuSettings.add(itmMenuGPlay);

        menuStats.add(itmScore);
        menuStats.add(itmHistory);

        menuHelp.add(itmAbout);
        menuHelp.add(itmRules);

        this.add(menuSettings);
        this.add(menuStats);
        this.add(menuHelp);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem temp = (JMenuItem)e.getSource();

        if (temp.equals(itmColor)) {
            showColor();
        } else if (temp.equals(itmScore)) {
            showScores();
        } else if (temp.equals(itmHistory)) {
            showHistory();
        } else if (temp.equals(itmAbout)) {
            showAbout();
        } else if (temp.equals(itmRules)) {
            showRules();
        } // end of level one options
        // Game Settings -> Mode
        else if (temp.equals(itmScramble)) {
            prefs.putBoolean(PrefKeys.SCRAMBLE, itmScramble.getState());
        } else if (temp.equals(itmReverse)) {
            prefs.putBoolean(PrefKeys.REVERSE, itmReverse.getState());
        }
        // Game Settings -> Annoyances
        else if (temp.equals(itmSounds)) {
            prefs.putBoolean(PrefKeys.SOUNDS, itmSounds.getState());
        } else if (temp.equals(itmImages)) {
            prefs.putBoolean(PrefKeys.IMAGES, itmImages.getState());
        }
        // Game Settings -> Speed
        else if (temp.equals(itmFast)) {
            prefs.putInt(PrefKeys.SPEED, SPEED_FAST);
            prefs.putBoolean(PrefKeys.ADAPTIVE, false);
        } else if (temp.equals(itmMed)) {
            prefs.putInt(PrefKeys.SPEED, SPEED_MED);
            prefs.putBoolean(PrefKeys.ADAPTIVE, false);
        } else if (temp.equals(itmSlow)) {
            prefs.putInt(PrefKeys.SPEED, SPEED_SLOW);
            prefs.putBoolean(PrefKeys.ADAPTIVE, false);
        } else if (temp.equals(itmAdaptive)) {
            prefs.putBoolean(PrefKeys.ADAPTIVE, !prefs.getBoolean(PrefKeys.ADAPTIVE, false));
        }
        // Game Settings -> Size
        else if (temp.equals(itmSize2)) {
            System.out.println("size 2");
            prefs.putInt(PrefKeys.SIZE, 0);
            parent.setColors(SimonishGame.genColors(2));
        } else if (temp.equals(itmSize3)) {
            System.out.println("size 3");
            prefs.putInt(PrefKeys.SIZE, 1);
            parent.setColors(SimonishGame.genColors(3));
        } else if (temp.equals(itmSizeCustom)) {
            System.out.println("size custom");
            customSize();
        }

    }

    /**
     * sets a custom sized grid
     */
    private void customSize() {
        int panelSize = getPanelSize();
        prefs.putInt(PrefKeys.SIZE, panelSize);
        parent.setColors(SimonishGame.genColors(panelSize));
    }

    /**
     * gets the users desired grid size
     * @return int representing one side of the square grid
     */
    private int getPanelSize() {
        String s = (String)JOptionPane.showInputDialog(parent, "Input an integer between 4 and 10 inclusive", "Select Size", JOptionPane.PLAIN_MESSAGE, null, null, "4");

        if (!tryParseInt(s) || Integer.parseInt(s) < 4) {
            JOptionPane.showMessageDialog(parent, "Input must be an integer greater than 3, and is recommended to be below 11 to avoid repeating colors", "ERROR", JOptionPane.ERROR_MESSAGE);
            return getPanelSize();
        }

        return Integer.parseInt(s);
    }

    /**
     * gets if the string will parse correctly
     * @param s string to parse
     * @return true if able to parse
     */
    private boolean tryParseInt(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Reads game history from save file
     * @return List of previous scores
     */
    private List<Integer> readHistory() {
        String homeDir = System.getProperty("user.home");
        String filePath = homeDir + "/" + ".simonish_games";
        List<Integer> list = new ArrayList<>();
        File file = new File(filePath);
        if (!file.exists()) {
            System.out.println("History file not found");
            return list;
        }

        try {
            Files.readAllLines(file.toPath()).forEach(line -> list.add(Integer.parseInt(line)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    /**
     * Reads previous high scores from save file
     * @return List of tuples (previous score)
     */
    private List<Map.Entry<Integer, String>> readScores() {
        String homeDir = System.getProperty("user.home");
        String filePath = homeDir + "/" + ".simonish_scores";
        List<Map.Entry<Integer, String>> list = new ArrayList<>();
        File file = new File(filePath);
        if (!file.exists()) {
            System.out.println("Scores file not found");
            return list;
        }

        return ScorePanel.readScoreCSV(file.toPath());

    }

    /**
     * deletes the game history file
     */
    private void delHistory() {
        String homeDir = System.getProperty("user.home");
        String filePath = homeDir + "/" + ".simonish_games";
        File file = new File(filePath);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * deletes the high score history file
     */
    private void delScores() {
        String homeDir = System.getProperty("user.home");
        String filePath = homeDir + "/" + ".simonish_scores";
        File file = new File(filePath);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * provides a dialog to change a color
     */
    private void showColor() {
        Color[] colors = parent.getColors();
        JDialog dialog = new JDialog(parent, "Pick Colors");
        Container pane = dialog.getContentPane();
        pane.setLayout(new BoxLayout(pane, BoxLayout.PAGE_AXIS));
        JComboBox<Color> cmbColor = new JComboBox<>(colors);
        cmbColor.setSelectedIndex(0);

        JButton btnColor = new JButton("Choose Color");
        JButton btnCancel = new JButton("Done");

        btnColor.addActionListener(e -> {
            Color temp = JColorChooser.showDialog(dialog, "Choose a color", (Color)cmbColor.getSelectedItem());

            if (temp != null) {
                System.out.println("setting color");
                int idx = cmbColor.getSelectedIndex();
                Color current = (Color)cmbColor.getSelectedItem();
                cmbColor.insertItemAt(temp, idx);
                cmbColor.removeItem(current);
                cmbColor.setSelectedItem(temp);
            }
        });
        btnCancel.addActionListener((e) -> {
            ComboBoxModel<Color> model = cmbColor.getModel();
            Color[] newColors = new Color[model.getSize()];
            for (int i = 0; i < model.getSize(); i++) {
                newColors[i] = model.getElementAt(i);
            }
            parent.setColors(newColors);
            dialog.dispatchEvent(new WindowEvent(dialog, WindowEvent.WINDOW_CLOSING));
        });

        pane.add(cmbColor);
        pane.add(btnColor);
        pane.add(btnCancel);

        dialog.pack();
        dialog.setLocationRelativeTo(parent);
        dialog.setVisible(true);

    }

    /**
     * shows the previous high scores
     */
    private void showScores() {
        JDialog dialog = new JDialog(parent, "High Scores");
        Container pane = dialog.getContentPane();
        pane.setLayout(new BoxLayout(pane, BoxLayout.PAGE_AXIS));
        pane.add(new JLabel("Top 10 High Scores"));

        List<Map.Entry<Integer, String>> entries = readScores();
        // reverse so the list is in big to little scores order
        Collections.reverse(entries);
        entries.forEach(entry -> {
            pane.add(new JLabel(entry.getValue() + ": " + entry.getKey()));
        });

        JButton btnScores = new JButton("Delete Scores");
        JButton btnClose = new JButton("Close");

        btnScores.addActionListener(e -> {
            delScores();
            dialog.dispatchEvent(new WindowEvent(dialog, WindowEvent.WINDOW_CLOSING));
        });
        btnClose.addActionListener((e) -> dialog.dispatchEvent(new WindowEvent(dialog, WindowEvent.WINDOW_CLOSING)));

        pane.add(btnScores);
        pane.add(btnClose);

        dialog.pack();
        dialog.setLocationRelativeTo(parent);
        dialog.setVisible(true);
    }

    /**
     * shows the previous game history
     */
    private void showHistory() {
        List<Integer> history = readHistory();
        int numGames = history.size();
        double avg = 0;

        for (Integer aHistory : history) {
            avg += aHistory;
        }

        if (numGames != 0)
            avg /= numGames;

        JDialog panel = new JDialog(parent, "History");
        Container pane = panel.getContentPane();
        pane.setLayout(new BoxLayout(pane, BoxLayout.PAGE_AXIS));

        JLabel lblGames = new JLabel("Num Games: " + numGames);
        JLabel lblAvg = new JLabel("Average: " + avg);

        pane.add(lblGames);
        pane.add(lblAvg);

        JButton btnHistory = new JButton("Delete History");
        JButton btnClose = new JButton("Close");

        btnHistory.addActionListener((e) -> {
            delHistory();
            panel.dispatchEvent(new WindowEvent(panel, WindowEvent.WINDOW_CLOSING));
        });
        btnClose.addActionListener((e) -> panel.dispatchEvent(new WindowEvent(panel, WindowEvent.WINDOW_CLOSING)));

        pane.add(btnHistory);
        pane.add(btnClose);

        panel.pack();
        panel.setLocationRelativeTo(parent);
        panel.setVisible(true);
    }

    /**
     * shows about panel
     */
    private void showAbout() {
        String message;
        message = "Simonish Advanced\n";
        message += "(c) Abe 2016\n";
        message += "\tA memory game with advanced features, including:\n";
        message += "\t\t* sounds\n";
        message += "\t\t* images\n";
        message += "\t\t* adaptive speed\n";
        message += "\t\t* new game modes - shuffle and reverse\n";
        JOptionPane.showMessageDialog(this, message, "About", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * shows rules panel
     */
    private void showRules() {
        String message;
        message = "How to play:\n";
        message += "Click on the colored squares in the order that the computer highlighted them\n";
        message += "The round is over when an incorrect square is clicked\n";
        message += "A new square will be added to the sequence at the end of a successful round";
        JOptionPane.showMessageDialog(this, message, "Rules", JOptionPane.INFORMATION_MESSAGE);
    }

}
