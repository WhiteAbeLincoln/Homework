package cs2410.assn7.components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

/**
 * Created by abe on 3/25/16.
 *
 * Custom panel that handles click events
 *
 * @author abe
 */

class ColorPanel extends JPanel implements MouseListener {
    private Color mainColor;
    private Preferences prefs;
    // using diamond operator from java 7
    private List<SequenceListener> listenerList = new ArrayList<>();
    private SoundThread redThread;
    private SoundThread blueThread;
    private SoundThread greenThread;
    private int clickCount = 0;
    private String urls[] = new String[] {
            "http://www.unixstickers.com/image/cache/data/stickers/haskell/Haskell.sh-600x600.png",
            "https://pbs.twimg.com/profile_images/426420605945004032/K85ZWV2F_400x400.png",
            "http://www.unixstickers.com/image/cache/data/stickers/python/python.sh-600x600.png",
            "http://d13yacurqjgara.cloudfront.net/users/46200/screenshots/799814/cpp-logo-dribbble_1x.png",
            "http://ih3.redbubble.net/image.15741788.5796/sticker,375x360.u1.png",
            "https://www.unixstickers.com/image/cache/data/stickers/js/js.sh-600x600.png"
    };
    private JLabel imageLabel;

    /**
     * Initializes a color panel
     * @param mainColor background color for the panel
     */
    ColorPanel(Color mainColor) {
        prefs = Preferences.userRoot().node(PrefKeys.MAIN_NAME);
        this.mainColor = mainColor;

        this.addMouseListener(this);
            imageLabel = new JLabel();

            imageLabel.setBounds(0,0,100,100);
            imageLabel.setVisible(prefs.getBoolean(PrefKeys.IMAGES, false));
            this.add(imageLabel);

        this.setBackground(mainColor);
        this.setVisible(true);
    }

    /**
     * sleeps and then updates the color
     */
    private void sleep() {
        sleep(1000);
    }

    /**
     * sleeps and then updates the color
     * @param i milliseconds to sleep
     */
    private void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * sets the image when Image Annoyance is turned on
     */
    private void setImage() {
        System.out.println("Setting image");
        clickCount++;
        try {
            ImageIcon image = new ImageIcon(new ImageIcon(new URL(urls[clickCount % urls.length])).getImage().getScaledInstance(100,100, Image.SCALE_SMOOTH));
            imageLabel.setIcon(image);
            imageLabel.setVisible(true);
            this.update(this.getGraphics());
        } catch (MalformedURLException e) {
            System.err.println("Cannot open " + urls[clickCount % urls.length]);
            e.printStackTrace();
        }
    }

    /**
     * Plays a sound unique to this color, and stops it after an amount of time
     * @param time time in milliseconds to play the sound
     */
    private void playSound(int time) {
        playSound();
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.schedule(this::stopSound,time,TimeUnit.MILLISECONDS);
    }

    /**
     * Plays a sine wave composed of the frequencies computed from each component of the RGB color
     */
    private void playSound() {
        double redFreq = (Math.log(mainColor.getRed()+1) / Math.log(2)) * 256;
        double greenFreq = (Math.log(mainColor.getGreen()+2) / Math.log(2)) * 256;
        double blueFreq = (Math.log(mainColor.getBlue()+3) / Math.log(2)) * 256;

        redThread = new SoundThread(redFreq);
        greenThread = new SoundThread(greenFreq);
        blueThread = new SoundThread(blueFreq);

        new SwingWorker<Void,Void>(){
            @Override
            protected Void doInBackground() throws Exception {
                redThread.run();
                return null;
            }
        }.execute();
        new SwingWorker<Void,Void>(){
            @Override
            protected Void doInBackground() throws Exception {
                greenThread.run();
                return null;
            }
        }.execute();
        new SwingWorker<Void,Void>(){
            @Override
            protected Void doInBackground() throws Exception {
                blueThread.run();
                return null;
            }
        }.execute();
    }

    /**
     * Stops the running sound threads
     */
    private void stopSound() {
        redThread.exit();
        blueThread.exit();
        greenThread.exit();
    }

    /**
     * returns a color to its main state
     */
    private void restoreColor() {
        this.setBackground(this.mainColor);
        this.update(this.getGraphics());
    }


    /**
     * Adds a listener to the SequenceListener Observer Pattern
     * @param toAdd the class implementing SequenceListener
     */
    void addListener(SequenceListener toAdd) {
        listenerList.add(toAdd);
    }

    /**
     * Brightens the panel in the sequence
     */
    void sequence(int speed) {
        this.setBackground(brighter());
        this.update(this.getGraphics());
        sleep(speed);
        this.restoreColor();
    }

    /**
     * Brightens a color by converting it to HSL and back
     * @return brighter color
     */
    private Color brighter() {
        float[] hsl = ColorConvert.fromRGB(this.mainColor);

        // try to increase lightness
        hsl[2] += 0.5 * hsl[2];
        if (hsl[2] >= 100f) hsl[2] = 100f;

        return ColorConvert.toRGB(hsl[0], hsl[1], hsl[2], 1.0f);
    }

    /**
     * Calls the panelClicked event for each listener on a mouse click
     * @param mouseEvent unused
     */
    public void mouseClicked(MouseEvent mouseEvent) {
        if (prefs.getBoolean(PrefKeys.IMAGES, false))
            setImage();
        else
            imageLabel.setVisible(false);
        listenerList.parallelStream().forEach((listener) -> listener.panelClicked(this));
    }

    public void mousePressed(MouseEvent mouseEvent) {
        this.setBackground(brighter());
        this.update(this.getGraphics());
        if (prefs.getBoolean(PrefKeys.SOUNDS, false))
            playSound();
    }

    public void mouseReleased(MouseEvent mouseEvent) {
        this.restoreColor();
        if (prefs.getBoolean(PrefKeys.SOUNDS, false))
            stopSound();
    }

    public void mouseEntered(MouseEvent mouseEvent) {

    }

    public void mouseExited(MouseEvent mouseEvent) {

    }

}
