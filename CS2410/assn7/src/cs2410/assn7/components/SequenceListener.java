package cs2410.assn7.components;

/**
 * Created by abe on 3/25/16.
 *
 * Listens for a clicked panel
 */
interface SequenceListener {
    void panelClicked(ColorPanel panel);
}

