package cs2410.assn7.components;

import javax.sound.sampled.*;
import java.nio.ByteBuffer;

/**
 * Starts a thread which plays a sine wave at a certain frequency until the thread is killed
 * @author abe
 */
public class SoundThread extends Thread {
    private final static int SAMPLING_RATE = 44100;
    private final static int SAMPLE_SIZE = 2;
    private final static double BUFFER_DURATION = 0.1;
    private final static int PACKET_SIZE = (int)(BUFFER_DURATION*SAMPLING_RATE*SAMPLE_SIZE);

    private SourceDataLine line;
    private boolean exitThread = false;

    private double freq = 440;

    SoundThread(double freq) {
        this.freq = freq;
    }

    private int getSampleCount() {
        return line.getBufferSize() - line.available();
    }

    @Override
    public void run() {
        double cyclePos = 0;

        try {
            AudioFormat format = new AudioFormat(SAMPLING_RATE, 16, 1, true, true);
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, format, PACKET_SIZE*2);

            if (!AudioSystem.isLineSupported(info))
                throw new LineUnavailableException();

            line = (SourceDataLine)AudioSystem.getLine(info);
            line.open(format);
            line.start();
        } catch (LineUnavailableException e) {
            System.out.println("Line is not available");
            e.printStackTrace();
        }

        ByteBuffer buff = ByteBuffer.allocate(PACKET_SIZE);

        while (!exitThread) {
            double cycleInc = freq / SAMPLING_RATE;
            buff.clear();

            for (int i =0; i < PACKET_SIZE/SAMPLE_SIZE; i++) {
                buff.putShort((short)(Short.MAX_VALUE * Math.sin(2*Math.PI*cyclePos)));

                cyclePos += cycleInc;
                if (cyclePos > 1)
                    cyclePos -= 1;
            }

            line.write(buff.array(), 0, buff.position());

            try {
                while (getSampleCount() > PACKET_SIZE) {
                    Thread.sleep(1);
                }
            } catch (InterruptedException e) {}
        }

        line.drain();
        line.close();
    }

    /**
     * Kills the thread
     */
    public void exit() {
        exitThread = true;
    }

}
