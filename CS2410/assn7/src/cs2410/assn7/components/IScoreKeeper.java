package cs2410.assn7.components;

/**
 * Created by abe on 4/7/16.
 * Interface for a ScoreKeeper - ScorePanel is an implementation of this
 *
 * @see cs2410.assn7.components.ScorePanel
 */
public interface IScoreKeeper {
    /**
     * should increment the current score
     */
    void incrementScore();

    /**
     * should set an end game
     */
    void endGame();

    /**
     * should return the current score
     * @return current score
     */
    int getScore();
}
