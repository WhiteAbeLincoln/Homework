package cs2410.assn7.game;

// import cs2410.assn7.game.Simonish;

import cs2410.assn7.components.GridPanel;
import cs2410.assn7.components.MenuPanel;
import cs2410.assn7.components.PrefKeys;
import cs2410.assn7.components.ScorePanel;

import javax.swing.*;
import java.awt.*;
import java.util.prefs.Preferences;


/**
 * Created by abe on 3/25/16.
 * Main Game Frame
 *
 * @author abe
 */
public class SimonishGame extends JFrame {
    Preferences prefs;

    GridPanel gridPanel;
    ScorePanel scorePanel;

    /**
     * initializes the frame
     */
    public SimonishGame() {
        prefs = Preferences.userRoot().node(PrefKeys.MAIN_NAME);
        int numColors =
                prefs.getInt(PrefKeys.SIZE, 0) == 0
                    ? 2
                : prefs.getInt(PrefKeys.SIZE, 0) == 1
                    ? 3
                : prefs.getInt(PrefKeys.SIZE, 0);


        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("Something erred while setting look and feel");
            e.printStackTrace();
        }
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container pane = this.getContentPane();

        pane.setPreferredSize(new Dimension(400, 500));
        pane.setSize(50, 50);
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        MenuPanel menuPanel = new MenuPanel(this);
        scorePanel = new ScorePanel();
        gridPanel = new GridPanel(genColors(numColors), scorePanel);
        scorePanel.addListener(gridPanel);

        this.setJMenuBar(menuPanel);
        this.add(scorePanel);
        this.add(gridPanel);

        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    /**
     * gets the gridPanels current colors
     * @return color array
     */
    public Color[] getColors() {
        return gridPanel.getColors();
    }

    /**
     * Sets the colors and reloads the gridPanel
     * @param colors colors to set
     */
    public void setColors(Color[] colors) {
        scorePanel.removeListener(gridPanel);
        this.remove(gridPanel);
        gridPanel = new GridPanel(colors, scorePanel);
        scorePanel.addListener(gridPanel);
        this.add(gridPanel);
        this.pack();
    }

    /**
     * gets the bit at position k of an integer
     * @param n the integer
     * @param k the position
     * @return the bit (1 or 0)
     */
    private static int getBit(int n, int k) {
        return (n >> k) & 1;
    }

    /**
     * Generates a color table
     * @param n square root of the number of colors
     * @return an array of Colors
     */
    private static Color[] buildColors(int n) {
        Color[] colors = new Color[n*n];
        int counter = 1;

        for (int i = 0; i<colors.length; i++) {

            int p0 = getBit(counter, 0);
            int p1 = getBit(counter, 1);
            int p2 = getBit(counter, 2);

            int mult = (i%16 + 1)*16 - 1;
            System.out.printf("[%d,%d,%d]\n", p2*mult+p2, p1*mult+p1, p0*mult+p0);
            if (p2*mult+p2 == 256 && p1*mult+p0 == 256 && p0*mult+p0 == 256)
                System.out.println(i + ", " + n);

            colors[i] = new Color(p2*mult, p1*mult, p0*mult);

            if ((i% 16+1)*16 >= 256)
                counter++;
        }

        return colors;
    }

    /**
     * Generates the color table
     * @param n square root of the desired number of colors
     * @return an array of Colors
     */
    public static Color[] genColors(int n) {
        if (n == 2)
            return new Color[] {Color.YELLOW, Color.BLUE, Color.RED, Color.GREEN};
        else if (n == 3)
            return new Color[] {Color.YELLOW, Color.BLUE, Color.RED, Color.GREEN, Color.CYAN, Color.MAGENTA, Color.PINK, Color.WHITE, Color.ORANGE};
        else
            return buildColors(n);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(SimonishGame::new);
    }

}
