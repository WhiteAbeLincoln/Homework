

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import cs2410.demo.component.ColorPanel;
import cs2410.demo.component.ScorePanel;

/**
 * @author abe
 *
 */
public class MainGame implements MouseListener, ActionListener {
	private JFrame frame = new JFrame("Simonish");
	private Color[] colors = {Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW};
	private ColorPanel[] panels = new ColorPanel[4];
	private ScorePanel scorePanel = new ScorePanel();
	private ArrayList<ColorPanel> compSeq = new ArrayList<ColorPanel>();
	private Random rand = new Random();
	private Iterator<ColorPanel> iter;
	boolean playerTurn = false;
	
	private MainGame() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel pane = (JPanel)frame.getContentPane();
		pane.setLayout(new BorderLayout());
		
		JPanel gameArea = new JPanel();
		gameArea.setLayout(new GridLayout(2, 2));
		gameArea.setPreferredSize(new Dimension(400, 400));
		
		for (int i = 0; i < panels.length; i++) {
			panels[i] = new ColorPanel(colors[i]);
			panels[i].addMouseListener(this);
			gameArea.add(panels[i]);
		}
		
		scorePanel.addStartListener(this);
		
		pane.add(gameArea);		
		pane.add(scorePanel, BorderLayout.NORTH);
		
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	private void reset() {
		scorePanel.disableStart();
		
		for (ColorPanel p : panels) {
			p.reset();
		}
		
		scorePanel.resetScore();
		compSeq.clear();
	}
	
	private void compTurn() {
		playerTurn = false;
		
		//pause briefly between rounds
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		compSeq.add(panels[rand.nextInt(4)]);
		
		iter = compSeq.iterator();
		
		while(iter.hasNext()) {
			ColorPanel tmp = iter.next();
			tmp.pressed();
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			tmp.released();
			
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		iter = compSeq.iterator();
		playerTurn = true;
	}
	
	private boolean isCorrectClick(ColorPanel panel) {
		return iter.next() == panel;
	}
	
	private void roundWon() {
		scorePanel.incrScore();
	}

	private void gameOver() {
		playerTurn = false;
		String msg = "Game Over\n" + "Your Score was " + scorePanel.getScore();
		if (scorePanel.isNewHighScore()) {
			msg = msg + "\nCongratulations! You set a new high score!";
		}
		JOptionPane.showMessageDialog(frame, msg);
		scorePanel.enableStart();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){

			@Override
			public void run() {
				new MainGame();				
			}});
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		//do nothing
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (!playerTurn) return;
		ColorPanel tmp = (ColorPanel)e.getSource();
		tmp.pressed();
		if (!isCorrectClick(tmp)) {
			this.gameOver();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (!playerTurn) return;

		((ColorPanel)e.getSource()).released();
		if (!iter.hasNext()) {
			this.roundWon();
			this.compTurn();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// do nothing
	}

	@Override
	public void mouseExited(MouseEvent e) {
		//do nothing		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.reset();
		this.compTurn();
	}

}
