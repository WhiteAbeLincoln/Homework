package cs2410.assn9.components;

import javax.swing.JComponent;
import java.awt.Graphics2D;

class Canvas extends JComponent {
    private Graphics2D g;
    private int currX, currY, oldX, oldY;

    Canvas() {
        this.setDoubleBuffered(false);
    }
}
