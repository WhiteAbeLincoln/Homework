package cs2410.assn3.helper;

/**The Day class is a helper class for Assignment #3.
 * Creates and stores a valid date
 *
 * @author Chad Mano
 * @version 1.0
 *
 */
public class Day {

    /**
     * Value of the year. If not set, then defaults to 2015
     */
    private int year;
    /**
     * Value of the month, 1-12. Set to 1 for invalid values
     */
    private int month;
    /**
     * Value of day, 1-31. Set to 1 for invalid values
     */
    private int day;

    /**Constructor with default year of 2015.
     *
     * @param month int value of the month (1-12)
     * @param day int value of the day (1-31)
     */
    public Day(int month, int day) {
        setYear(2015);
        setMonth(month);
        setDay(day);
    }

    /**getter method for the year
     *
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**setter method for the year. Invalid years are set to 2016
     *
     * @param year value of the year
     */
    public void setYear(int year) {
        if (year < 0) {
            year = 2016;
        }
        this.year = year;
    }

    /**getter method for the month
     *
     * @return value of the month
     */
    public int getMonth() {
        return month;
    }

    /**setter method for the month. Defaults to 1 for invalid input
     *
     * @param month value of the month (1-12)
     */
    public void setMonth(int month) {
        if (month < 1 || month > 12) {
            month = 1;
        }
        this.month = month;
    }

    /**getter method for the day.
     *
     * @return value of the day
     */
    public int getDay() {
        return day;
    }

    /**setter method for the day. Defaults to 1 for invalid input
     *
     * @param day value of the day
     */
    public void setDay(int day) {
        if (day < 1 || day > 31) {
            day = 1;
        }
        this.day = day;
    }

    /**Creates a string of the date in the format MM/DD/YYYY
     *
     * @return String value of the full date
     */
    @Override
    public String toString() {
        return month + "/" + day + "/" + year;
    }


}
