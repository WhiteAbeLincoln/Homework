package cs2410.assn3.mtable;

/**
 * Created by abe on 2/7/16.
 * Helps the neighborhood kids study.
 */
public class MultiplicationTable {
    int start;
    int end;

    /**
     * Constructs the Multiplication table
     * @param start the starting value
     * @param end the ending value
     */
    public MultiplicationTable(int start, int end) {
        this.start = start;
        this.end = end;
    }

    /**
     * Prints the table in a nice format
     */
    void printTable() {
        System.out.println("Number Squared Cubed");
        System.out.println("------ ------- -----");

        int count = start;

        while (count <= end) {
            int squared = count*count;
            int cubed = squared*count;

            System.out.printf("%6d %7d %5d\n", count, squared, cubed);
            count++;
        }
    }

}
