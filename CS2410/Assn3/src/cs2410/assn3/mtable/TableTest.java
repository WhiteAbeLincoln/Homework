package cs2410.assn3.mtable;

import java.util.Scanner;

/**
 * Created by abe on 2/7/16.
 */
public class TableTest {
    public static void main(String[] argv) {
        int start = 0;
        int end = 0;

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the starting value for your table: ");
        String starting = sc.next();

        System.out.print("Enter the ending value for your table: ");
        String ending = sc.next();

        try {
            start = Integer.parseInt(starting);
            end = Integer.parseInt(ending);
        } catch (NumberFormatException e) {
            System.out.println("Invalid number format");
            System.out.println(e.getMessage());
            System.exit(34);
        }

        if (end < start) {
            //throw new Exception("Starting value is greater than ending value");
            System.out.println("Starting value is greater than ending value");
            System.exit(34);
        }

        if (end < 0 || start < 0) {
            System.out.println("Start or end value is less than 0");
            System.exit(34);
        }

        /*for (int i = 0; i < 1000; i += 10) {
            //System.out.println("Column1 Col3 Column3");
            //System.out.println("------- ---- -------");
            System.out.printf("%s\t%s\t%s", "Column1", "Column2", "Column3\n");
            System.out.printf("%7d %7d %7d\n", i, i*i,i*i*i);

        }*/

        MultiplicationTable table = new MultiplicationTable(start, end);
        table.printTable();
    }
}
