package cs2410.assn3.rocks;

import cs2410.assn3.helper.Day;

/**
 * Created by abe on 2/7/16.
 */
public class Order {
    private final double rockPrice = 6.75;
    private final double largePrice = 2.0;
    private final double smallPrice = 1.2;
    private final int largeQuantity = 10;
    private final int smallQuantity = 5;

    int totalRocks = 0;
    int largeBoxes = 0;
    int smallBoxes = 0;
    int leftoverRocks = 0;
    Day deliveryDay;

    public double getRockPrice() { return rockPrice; }

    public double getLargeBPrice() { return largePrice; }

    public double getSmallBPrice() { return smallPrice; }

    public Order(int totalRocks, Day deliveryDay) {
        this.totalRocks = totalRocks;
        this.deliveryDay = deliveryDay;

        largeBoxes = totalRocks / largeQuantity;
        totalRocks %= largeQuantity;
        smallBoxes = totalRocks / smallQuantity;
        totalRocks %= smallQuantity;
        leftoverRocks = totalRocks;

        if (leftoverRocks > 0) { // if there are any rocks left over, put them in a new box;
            smallBoxes += 1;
        }
    }

    public int getTotalRocks() {
        return totalRocks;
    }

    public double getTotalBagPrice() {
        return totalRocks * rockPrice;
    }

    public int getLargeBoxes() {
        return largeBoxes;
    }

    public double getLargePrice() {
        return largeBoxes * largePrice;
    }

    public int getSmallBoxes() {
        return smallBoxes;
    }

    public double getSmallPrice() {
        return smallBoxes * smallPrice;
    }

    public double getTotalPrice() {
        return getSmallPrice() + getLargePrice() + rockPrice*totalRocks;
    }

    public Day getDeliveryDay() {
        return deliveryDay;
    }

    public int getLargeBoxBagCount() {
        return largeBoxes * largeQuantity;
    }

    public int getSmallBoxBagCount() {
        if (smallBoxes == 1 && leftoverRocks > 0) {
            return leftoverRocks;
        } else return smallBoxes * smallQuantity + leftoverRocks;
    }

}
