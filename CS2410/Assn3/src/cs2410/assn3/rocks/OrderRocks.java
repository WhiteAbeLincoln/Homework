package cs2410.assn3.rocks;

import cs2410.assn3.helper.Day;

import javax.swing.*;

/**
 * Created by abe on 2/7/16.
 */
public class OrderRocks {
    public static void main(String[] argv) {
        int rocks = 0;
        Day deliveryDate = new Day(1,1);

        String totalRocks = (String)JOptionPane.showInputDialog(null, "How many rocks would you like to purchase?");
        String month = (String)JOptionPane.showInputDialog(null, "What month (1-12) would you like your rocks delivered?");
        String day = (String)JOptionPane.showInputDialog(null, "What day (1-31) would you like your rocks delivered?");
        String year = (String)JOptionPane.showInputDialog(null, "What year would you like your rocks delivered?");

        try {
            rocks = Integer.parseInt(totalRocks);
            deliveryDate.setYear(Integer.parseInt(year));
            deliveryDate.setMonth(Integer.parseInt(month));
            deliveryDate.setDay(Integer.parseInt(day));

        } catch (NumberFormatException e) {
            System.out.println("Illegal number format");
            System.out.println(e.getMessage());
        }

        Order od = new Order(rocks, deliveryDate);
        String receipt =
                "Thank you for your order. Here is your receipt"    + "\n"
                + "Number of rocks ordered: "
                + od.getTotalRocks()                                + "\n"
                + "Price per rock: "
                + od.getRockPrice()                                 + "\n"
                + "Price for all rocks: "
                + od.getTotalBagPrice()                             + "\n"
                                                                    + "\n"
                + "Your shipment will arrive grouped together"
                + "in the smallest number of boxes:"                + "\n"
                + od.getLargeBoxes()
                + "(" + od.getLargeBoxBagCount() + ") Large @ "
                + od.getLargeBPrice() + "/box = "
                + od.getLargePrice()                                + "\n"
                + od.getSmallBoxes()
                + "(" + od.getSmallBoxBagCount() + ") Small @ "
                + od.getSmallBPrice() + "/box = "
                + od.getSmallPrice()                                + "\n"
                                                                    + "\n"
                + "Total Cost: "
                + od.getTotalPrice()                                + "\n"
                + "Delivery Date: " + od.getDeliveryDay();

        JOptionPane.showConfirmDialog(null, receipt);
    }
}
