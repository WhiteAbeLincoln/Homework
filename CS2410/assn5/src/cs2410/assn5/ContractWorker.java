package cs2410.assn5;

import java.util.Random;

/**
 * Created by abe on 3/5/16.
 */
public class ContractWorker extends Smarty {

    double payPerContract;
    int numContracts;
    String name;

    /**
     * Creates a default ContractWorker
     */
    public ContractWorker() {
        this(5, 1500.50, "Generic Contract", 120);
    }

    /**
     * Creates a ContractWorker
     * @param numContracts number of contracts completed
     * @param payPerContract amount paid per contract
     * @param name name of the contract worker
     * @param IQ the worker's IQ
     */
    public ContractWorker(int numContracts, double payPerContract, String name, int IQ) {
        this.IQ = IQ;
        this.payPerContract = payPerContract;
        this.numContracts = numContracts;
        this.name = name;
    }

    @Override
    public double getIncome() {
        return payPerContract * numContracts;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String doMath() {
        // can do division

        // making the phrases a bit more fun
        int[] a = {1,2,3,4,5,6,7,8,9,10};
        Random ran = new Random();
        int idx1 = ran.nextInt(10); // [0,10)
        int idx2 = ran.nextInt(10); // [0,10)

        // can use multiplication
        return "I can divide! " +a[idx1]+ " / " +a[idx2]+ " = " + ((double)a[idx1]/a[idx2]);
    }

    @Override
    public String saySomethingSmart() {
        // knows about animals
        String[] a = {
                "Neutron stars can spin at a rate of 600 rotations per second"
                , "99% of our solar system's mass is the Sun"
                , "Saturn's rings are arguably the flattest structure known to " +
                    "man - 300,000 km long, but with a height of 10 meters"
                , "The Sun burns 600 million tons of Hydrogen a second"
                , "There is a black hole millions of times the mass of the Sun at the center of our galaxy"
                , "Some pulsars rotate several hundred times per second"
                , "Earth is the only discovered planet that has plate tectonics!"
        };

        // I want to mix this up a bit. A single animal phrase is boring
        Random ran = new Random();
        int quoth = ran.nextInt(7); // [0,7)

        return a[quoth];
    }

    /**
     * Gets the number of contracts completed
     * @return a string stating the number of contracts
     */
    public String getContractsCompleted() {
        return "I have " + numContracts + " contracts";
    }
}
