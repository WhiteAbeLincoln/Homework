package cs2410.assn5;

import java.util.Random;

public class HourlyWorker extends Smarty {

    int hours;
    double wage;
    String name;

    /**
     * Creates a default HourlyWorker
     */
    public HourlyWorker() {
       this(40, 10, "Generic Hourly", 100);
    }

    /**
     * Creates an HourlyWorker
     * @param hours number of hours worked
     * @param wage a double representing pay per hour
     * @param name the worker's name
     * @param IQ the worker's IQ
     */
    public HourlyWorker(int hours, double wage, String name, int IQ) {
        this.IQ = IQ;
        this.name = name;
        this.hours = hours;
        this.wage = wage;
    }

    @Override
    public double getIncome() {
        return hours * wage;
    }

    /**
     * Gets the number of hours a HourlyWorker worked
     * @return a string stating the number of hours
     */
    public String getHoursWorked() {
        return "I worked " + hours + " hours";
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String doMath() {
        // making the phrases a bit more fun
        int[] a = {0,1,2,3,4,5,6,7,8,9};
        Random ran = new Random();
        int idx1 = ran.nextInt(10);
        int idx2 = ran.nextInt(10);

        // can use multiplication
        return "I can multiply! " +a[idx1]+ " x " +a[idx2]+ " = " + a[idx1]*a[idx2];
    }

    @Override
    public String saySomethingSmart() {
        // knows about animals

        // I want to mix this up a bit. A single animal phrase is boring
        Random ran = new Random();
        int quoth = (ran.nextInt(6) % 2);

        if (quoth == 0) {
            return "Hippos are dangerous!";
        } else {
            return "Ravens quoth \"Nevermore\"";
        }

    }
}
