package cs2410.assn5;

public abstract class Simpleton {
    /**
     * Gets the Simpleton's name
     * @return a string stating the name
     */
    public abstract String getName();

    /**
     * Gets an example of the Simpleton's math
     * @return a string containing some math operation
     */
    public abstract String doMath();

    /**
     * Gets a fact from the Simpleton's repository of knowledge
     * @return a string with some fact about a subject
     */
    public abstract String saySomethingSmart();
}
