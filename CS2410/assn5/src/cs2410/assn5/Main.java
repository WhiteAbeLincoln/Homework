package cs2410.assn5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by abe on 3/5/16.
 */
public class Main {
    private JFrame frame = new JFrame("Polyabstraheritance");
    private Container pane;
    private JComboBox<String> comboActions;
    private JTextArea txtaOutput;
    private String[] actionArr = {"Math", "Income", "Hours", "IQ", "Say", "Carrots", "Contracts"};
    private ActionListener comboListener;
    private ArrayList<Simpleton> list;

    /**
     * Initializes our GUI
     */
    public Main() {
        list = new ArrayList<Simpleton>();
        list.add(new Hobbit(5, "Frodo"));
        list.add(new Hobbit(30, "Sam"));
        list.add(new HourlyWorker(40,10.80, "Bob", 80));
        list.add(new HourlyWorker(50,15.60, "James", 100));
        list.add(new ContractWorker(10, 1032.4, "Sally", 115));
        list.add(new ContractWorker(2, 1600000, "Barack", 120));

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pane = frame.getContentPane();
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        comboListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox tmp = (JComboBox)e.getSource();
                if (tmp.getSelectedIndex() != -1) {
                    // we know it will always be a string, since that is our only combobox
                    txtaOutput.setText("");
                    runAction(tmp.getSelectedItem().toString());
                }
            }
        };

        comboActions = new JComboBox<String>(actionArr);
        comboActions.addActionListener(comboListener);
        comboActions.setSelectedIndex(-1);

        txtaOutput = new JTextArea();

        //JPanel a = new JPanel();
        pane.setPreferredSize(new Dimension(500,500));

        pane.add(comboActions);
        pane.add(txtaOutput);

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * Executes an action for each Simpleton
     * @param s string describing the action to execute
     */
    private void runAction(String s) {
        System.out.println(s);
        // here is my for each statement
        for (Simpleton person : list) {
            switch (s) {
                case "Math":
                    txtaOutput.append(person.getName() + ": " + person.doMath() + "\n");
                    break;
                case "Income":
                    if (person instanceof Smarty)
                        txtaOutput.append(person.getName() + ": " + ((Smarty)person).getIncome() + "\n");
                    break;
                case "Hours":
                    if (person instanceof HourlyWorker)
                        txtaOutput.append(person.getName() + ": " + ((HourlyWorker)person).getHoursWorked() + "\n");
                    break;
                case "IQ":
                    if (person instanceof Smarty)
                        txtaOutput.append(person.getName() + ": " + ((Smarty)person).getIQ() + "\n");
                    break;
                case "Say":
                    txtaOutput.append(person.getName() + ": " + person.saySomethingSmart() + "\n");
                    break;
                case "Carrots":
                    if (person instanceof Hobbit)
                        txtaOutput.append(person.getName() + ": " + ((Hobbit)person).getNumCarrots() + "\n");
                    break;
                case "Contracts":
                    if (person instanceof ContractWorker)
                        txtaOutput.append(person.getName() + ": " + ((ContractWorker)person).getContractsCompleted() +"\n");
                    break;
                default:
                    txtaOutput.append("Something terrible happened\n");
                    break;
            }
        }

    }

    public static void main(String[] argv) {
        new Main();
    }
}
