package cs2410.assn5;

import java.util.Random;

/**
 * Created by abe on 3/5/16.
 */
public class Hobbit extends Simpleton {

    String name;
    int numCarrots;

    /**
     * Default constructor for a Hobbit
     */
    public Hobbit() {
        this(5, "Generic Frodo");
    }

    /**
     * Constructor for a Hobbit
     * @param numCarrots the number of carrots picked
     * @param name the hobbits name
     */
    public Hobbit(int numCarrots, String name) {
        this.name = name;
        this.numCarrots = numCarrots;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String doMath() {
        // making the phrases a bit more fun
        int[] a = {0,1,2,3,4,5,6,7,8,9};
        Random ran = new Random();
        int idx1 = ran.nextInt(10);
        int idx2 = ran.nextInt(10);

        // can use multiplication
        return "I can add! " +a[idx1]+ " breakfast(s) + " +a[idx2]+ " breakfast(s) = " + (a[idx1]+a[idx2]) + " Breakfasts!";
    }

    @Override
    public String saySomethingSmart() {// knows about animals
        String[] a = {
                "Sunflowers are actually made of 1,000-2,000 individual flowers"
                , "Farmers plant crops across the slope of a hill to conserve water and protect soil. " +
                    "This is called contour farming"
                , "Lettuce is a member of the sunflower family"
                , "Blueberries are a good source of Vitamin C and fiber"
                , "There are at least 10,000 varieties of tomatoes"
                , "Cucumbers have the highest water content of any vegetable"
                , "Broccoli and cauliflower are the only vegetables that are also flowers"
        };

        // I want to mix this up a bit. A single animal phrase is boring
        Random ran = new Random();
        int quoth = ran.nextInt(7); // [0,7)

        return a[quoth];
    }

    /**
     * Gets the number of carrots picked by a Hobbit
     * @return a string stating the number of carrots
     */
    public String getNumCarrots() {
        return "I have " + numCarrots + " carrots";
    }

}
