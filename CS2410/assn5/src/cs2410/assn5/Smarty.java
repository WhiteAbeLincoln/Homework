package cs2410.assn5;

public abstract class Smarty extends Simpleton {
    int IQ;

    /**
     * Gets the IQ of a Smarty
     * @return a string stating the Smarty's IQ
     */
    public String getIQ() {
        return "My IQ is " + IQ;
    }

    /**
     * Gets the Income of a Smarty
     * @return a double representing the Smarty's income
     */
    public abstract double getIncome();
}
