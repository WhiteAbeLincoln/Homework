package cs2410.assn8.view;

import cs2410.assn8.controller.IScoreKeeper;
import cs2410.assn8.controller.StartListener;

import java.awt.*;
import java.util.List;
import java.util.ArrayList;

import javax.swing.*;

/**
 * Created by abe on 4/27/16.
 *
 * Implementation of {@link cs2410.assn8.controller.IScoreKeeper IScoreKeeper} to handle score keeping and displaying
 *
 * @see cs2410.assn8.controller.IScoreKeeper
 */
class ScorePanel extends JPanel implements IScoreKeeper {
    private List<StartListener> startListeners = new ArrayList<>();
    private JLabel lblTimer = new JLabel();
    private int secondsPassed;
    private Timer timer;

    /**
     * Constructs a score panel
     */
    ScorePanel() {
        Font font = new Font("Courier New", Font.PLAIN, 15);
        secondsPassed = 0;
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        JButton btnStart = new JButton("New Game");
        btnStart.setFont(font);
        lblTimer.setFont(font);
        btnStart.addActionListener(actionEvent -> {
            resetTimer();
            startListeners.forEach(StartListener::gameStarted);
        });

        this.add(lblTimer);
        this.add(Box.createRigidArea(new Dimension(50, 0)));
        this.add(btnStart);
        updateView();
        this.initTimer();
    }

    /**
     * Updates the label
     */
    private void updateView() {
        lblTimer.setText("TIME: " +  Integer.toString(secondsPassed));
        this.update(this.getGraphics());
    }

    /**
     * resets the counting up timer
     */
    private void resetTimer() {
        timer.stop();
        secondsPassed = 0;
        updateView();
        initTimer();
    }

    /**
     * initializes the timer
     */
    private void initTimer() {
        timer = new Timer(1000, actionEvent -> {
            secondsPassed++;
            System.out.println(secondsPassed);
            updateView();
        });
    }

    @Override
    public void startTimer() {
        System.out.println("Timer Started");
        timer.start();
    }

    @Override
    public void incrementScore() {

    }

    /**
     * Subscribes a listener to StartListener events
     * @param toAdd listener to add
     */
    void addStartListener(StartListener toAdd) {
        startListeners.add(toAdd);
    }

    /**
     * Unsubscribes a listener from StartListener events
     * @param toRemove listener to remove
     */
    void removeStartListener(StartListener toRemove) {
        startListeners.remove(toRemove);
    }

    @Override
    public void endGame(boolean won) {
        timer.stop();
        String message = "Time: " + Integer.toString(secondsPassed);
        if (won) {
            message = "You Win - " + message;
            JOptionPane.showMessageDialog(this, message, "You Won", JOptionPane.INFORMATION_MESSAGE);
        } else {
            message = "You Lose";
            JOptionPane.showMessageDialog(this, message, "Game Over", JOptionPane.INFORMATION_MESSAGE);
        }

        secondsPassed = 0;
    }

    @Override
    public int getScore() {
        return 0;
    }
}
