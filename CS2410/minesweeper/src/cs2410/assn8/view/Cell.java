package cs2410.assn8.view;

import cs2410.assn8.controller.ICellContainer;
import cs2410.assn8.controller.Flag;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by abe on 4/27/16.
 *
 * An individual cell on the grid
 * May contain a mine
 */
public class Cell extends JPanel implements MouseListener {
    private JLabel lblCurrentFlag;
    private boolean mine;
    private boolean cleared = false;
    private int numAdjMines = 0;
    private Flag currentFlag;
    private ICellContainer gridPanel;
    private Color clearColor = new Color(142,142,142);
    private Color defaultColor = new Color(115, 142, 122);
    private Font font;

    Cell(ICellContainer grid) {
        this(grid, false);
    }

    /**
     * Constructs a new Cell
     * @param grid the cell's containing object
     * @param isMine whether the cell contains a mine
     */
    private Cell(ICellContainer grid, boolean isMine) {
        this.mine = isMine;
        this.gridPanel = grid;
        this.currentFlag = Flag.NONE;

        this.addMouseListener(this);
        this.lblCurrentFlag = new JLabel();

        this.lblCurrentFlag.setBounds(0,0,64,64);
        this.lblCurrentFlag.setVisible(false);
        this.add(lblCurrentFlag);

        this.setBackground(defaultColor);
        this.setVisible(true);
    }

    /**
     * Gets whether the cell contains a mine
     * @return true if cell contains mine
     */
    public boolean isMine() {
        return mine;
    }

    /**
     * Toggles a flag
     */
    private void toggleFlag() {
        switch (currentFlag) {
            case NONE:
                currentFlag = Flag.FLAGGED;
                break;
            case FLAGGED:
                currentFlag = Flag.UNSURE;
                break;
            default:
                currentFlag = Flag.NONE;
                break;
        }
        updateIcon();
    }

    /**
     * Updates the current icon to match the flag
     */
    private void updateIcon() {
        switch (currentFlag) {
            case NONE:
                lblCurrentFlag.setVisible(false);
                if (mine) {
                    gridPanel.decrementFound();
                }
                break;
            case FLAGGED:
                setImage("images/flag.png");
                if (mine) {
                    gridPanel.incrementFound();
                }
                break;
            case UNSURE:
                setImage("images/question.png");
                if (mine) {
                    gridPanel.decrementFound();
                }
                break;
            default:
                lblCurrentFlag.setVisible(false);
                if (mine) {
                    gridPanel.decrementFound();
                }
                break;
        }
        this.update(this.getGraphics());
    }

    /**
     * Gets the minimum dimension of a cell for setting an image
     * @return smaller of the cell height and the cell width or 16
     */
    private int getCellSize() {
        int size = this.getWidth() < this.getHeight() ? this.getWidth() - 16 : this.getHeight() - 16;     // pick the smaller dimension
        if (size < 16) size = 16;

        return size;
    }

    /**
     * Sets the image to the label
     * @param path path of image
     */
    private void setImage(String path) {
        int size = getCellSize();

        ImageIcon image = new ImageIcon(new ImageIcon(path).getImage().getScaledInstance(size,size, Image.SCALE_REPLICATE));
        lblCurrentFlag.setIcon(image);
        lblCurrentFlag.setVisible(true);
        this.update(this.getGraphics());
    }

    /**
     * Sets whether the cell contains a mine
     * @param b boolean indicating presence of mine
     */
    public void setMine(boolean b) {
        mine = b;
    }

    /**
     * Private function to fit font to grid size
     */
    private void setFont() {
        font = new Font("monospace", Font.BOLD, 1);
        FontMetrics fm = this.getFontMetrics(font);
        do {
            font = new Font("monospace", Font.BOLD, font.getSize() + 1);
            fm = this.getFontMetrics(font);
        } while (fm.stringWidth("6") < this.getWidth() - 15 && fm.getHeight() < this.getHeight() - 15);
    }

    /**
     * Sets the label to contain a number indicating adjacent mines
     */
    private void setNumber() {
        this.clearCell();
        String str = Integer.toString(this.numAdjMines);
        setFont();

        lblCurrentFlag.setFont(font);

        lblCurrentFlag.setIcon(null);
        lblCurrentFlag.setText(str);
        lblCurrentFlag.setVisible(true);
        this.update(this.getGraphics());
    }

    /**
     * Clears the cell
     */
    private void clearCell() {
        cleared = true;
        this.setBackground(clearColor);
        this.update(this.getGraphics());
        gridPanel.incrementCleared();
    }

    /**
     * Shows all the mines
     */
    public void showMines() {
        List<Cell> all = gridPanel.getCells();

        cleared = true;
        if (this.isMine() && this.isFlagged()) {
            clearCell();
            this.setBackground(Color.GREEN);
            setImage("images/mine.png");
        } else if (this.isMine()) {
            clearCell();
            this.setBackground(Color.RED);
            setImage("images/mine.png");
        } else if (this.isFlagged()) {
            clearCell();
            this.setBackground(Color.YELLOW);
        }

        all.stream().filter(cell -> !cell.isCleared()).forEach(Cell::showMines);
    }

    /**
     * Gets whether the cell has a Flag set
     * @return current flag is Flag.FLAGGED
     */
    public boolean isFlagged() {
        return currentFlag == Flag.FLAGGED;
    }

    /**
     * Gets whether the cell has been cleared
     * @return if the cell has been cleared
     */
    public boolean isCleared() {
        return cleared;
    }

    /**
     * Gets whether the cell has adjacent mines
     * @return number of adjacent mines is greater than 0
     */
    public boolean hasAdjacent() {
        return numAdjMines > 0;
    }

    /**
     * Recursively checks a cell and its adjacents
     */
    public void checkCell() {
        List<Cell> adjacent = gridPanel.getAdjacent(this);

        adjacent.stream().filter(Cell::isMine).forEach(cell -> numAdjMines++);

        if (numAdjMines == 0) {
            clearCell();
            adjacent.stream().filter(cell -> !cell.isFlagged() && !cell.isCleared() && !cell.hasAdjacent()).forEach(Cell::checkCell);
        } else if (!this.isCleared()){
            setNumber();
        }
    }

    /**
     * explodes a mine on incorrect click.
     * Ends game
     */
    private void explode() {
        clearCell();
        setImage("images/mine.png");
        this.setBackground(Color.RED);
        this.update(this.getGraphics());
        showMines();
        gridPanel.endGame(false);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
            gridPanel.cellClicked();
            toggleFlag();
        }

        if (mouseEvent.getButton() == MouseEvent.BUTTON1) {
            gridPanel.cellClicked();
            if (currentFlag != Flag.FLAGGED) {
                if (mine) explode();
                else checkCell();
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
