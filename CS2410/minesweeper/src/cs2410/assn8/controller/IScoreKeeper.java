package cs2410.assn8.controller;

/**
 * Created by abe on 4/27/16.
 *
 * Interface for any object that wishes to keep track of score
 */
public interface IScoreKeeper {
    /**
     * increments the score
     */
    void incrementScore();

    /**
     * Called on game over
     * @param won whether game was won
     */
    void endGame(boolean won);

    /**
     * gets the current score
     * @return number indicating score
     */
    int getScore();

    /**
     * starts a timer
     */
    void startTimer();
}
