package cs2410.assn8.controller;

/**
 * Created by abe on 4/27/16.
 *
 * Unused
 *
 * TODO: Add menubar and preferences
 */
public final class PrefKeys {
    public final static String MAIN_NAME = "minesweeperish";
    public final static String NUMBER_CELLS = "numCells";
    public final static String NUMBER_MINES = "numMines";
}
