/**
 * @author Abraham White A02178955
 * @version 1.0
 * @since 2016-01-19
 */
public class Fizzy {
    /**
     * Loop counter
     */
    private static int counter;
    
    /**
     * checks if val is a Fizz
     * 
     * @param val integer to check
     * @return boolean indicating whether val is a Fizz
     */
    private static boolean isFizz(int val) {
        return val % 3 == 0;
    }

    /**
     * checks if val is a Buzz
     *
     * @param val integer to check
     * @return boolean indicating whether val is a Buzz
     */
    private static boolean isBuzz(int val) {
        return val % 5 == 0;
    }

    public static void main(String[] argv) {
        counter = 1;
        while (counter < 101) {
            if (isFizz(counter) && isBuzz(counter)) {
                System.out.println("FizzBuzz");
            } else if (isFizz(counter)) {
                System.out.println("Fizz");
            } else if (isBuzz(counter)) {
                System.out.println("Buzz");
            } else {
                System.out.println(counter);
            }
            counter++;
        } 
    }
}
