package cs2410.assn6.game;

import cs2410.assn6.components.SimonishFrame;

/**
 * Created by abe on 3/25/16.
 * Main function - creates frames
 *
 * @author abe
 */
public class Simonish {
    public Simonish() {
        // new SimonishFrame(new Color[] {Color.BLUE, Color.RED, Color.GREEN, Color.GRAY, Color.CYAN, Color.MAGENTA, Color.PINK});
        new SimonishFrame();
    }

    public static void main(String[] argv) {
        Simonish game = new Simonish();
    }

}
