package cs2410.assn6.components;

// import cs2410.assn6.game.Simonish;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Math.ceil;
import static java.lang.Math.sqrt;

/**
 * Created by abe on 3/25/16.
 *
 * @author abe
 */
public class SimonishFrame extends JFrame implements SequenceListener {
    private final int JOKER_LOW = 0;
    private final int KNIGHT_LOW = 4;
    private final int KING_LOW = 10;
    private final String JOKER_TEXT = "Joker";
    private final String KNIGHT_TEXT = "Knight";
    private final String KING_TEXT = "King";

    private Color[] colors;
    // using diamond operator from java 7
    private List<Integer> orderList = new ArrayList<>();
    private List<ColorPanel> panelList = new ArrayList<>();
    private int currPanel;
    private int currScore;
    private int highScore;
    private int numClicked;
    private ActionListener startListener;
    private JLabel lblHighScore;
    private JLabel lblCurrScore;
    private boolean loop;

    public SimonishFrame() {
        this(new Color[] {Color.YELLOW, Color.BLUE, Color.RED, Color.GREEN});
    }

    /**
     * initializes the frame
     * @param colors list of colors to put in the grid
     */
    public SimonishFrame(Color[] colors) {
        EventQueue.invokeLater(() -> {
            this.colors = colors;
            loop = false;
            for (int i = 0; i < colors.length; i++) {
                ColorPanel panel = new ColorPanel(colors[i], this);
                panel.addListener(this);
                panelList.add(panel);
            }

            try {
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (Exception e) {
                System.err.println("Something erred while setting look and feel");
                e.printStackTrace();
            }
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            Container pane = this.getContentPane();

            pane.setPreferredSize(new Dimension(400, 500));
            pane.setSize(50, 50);
            pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

            initStartListener();

            pane.add(setUpScoreBoard());
            pane.add(setUpGrid());

            this.pack();
            this.setLocationRelativeTo(null);
            this.setVisible(true);
        });
    }

    /**
     * sets up the score board section of the frame
     * @return jpanel containing the scoreboard
     */
    private JPanel setUpScoreBoard() {
        JPanel scorePanel = new JPanel();
        scorePanel.setLayout(new BoxLayout(scorePanel, BoxLayout.LINE_AXIS));

        lblCurrScore = new JLabel("Score: 0");
        lblHighScore = new JLabel("High Score: 0");
        JButton btnStartGame = new JButton("Start");
        btnStartGame.addActionListener(startListener);

        scorePanel.add(lblCurrScore);
        scorePanel.add(btnStartGame);
        scorePanel.add(lblHighScore);

        return scorePanel;
    }

    /**
     * sets up the grid section of the frame
     * @return jpanel containing the color grid
     */
    private JPanel setUpGrid() {
        JPanel gridPanel = new JPanel();
        int square = (int)ceil(sqrt(colors.length));
        gridPanel.setLayout(new GridLayout(square,square));
        gridPanel.setBackground(Color.GRAY);

        ((GridLayout)gridPanel.getLayout()).setHgap(3);
        ((GridLayout)gridPanel.getLayout()).setVgap(3);

        panelList.forEach(gridPanel::add);

        return gridPanel;
    }

    /**
     * Button listener for the Start button
     */
    private void initStartListener() {
        // Lambdas are pretty cool
        startListener = actionEvent -> {
            orderList.clear();
            currScore = 0;
            currPanel = 0;
            lblCurrScore.setText("Score: 0");
            loop = true;
            addPanel();
        };
    }

    /**
     * gets the color array used to initialize the grid
     * @return array of colors
     */
    public Color[] getColors() {
       return colors;
    }

    /**
     * gets the list of panel indexes in the order to be clicked
     * @return list of integers
     */
    List<Integer> getOrderList() {
        return orderList;
    }

    /**
     * gets the list of color panels
     * @return list of color panels
     */
    List<ColorPanel> getPanelList() {
        return panelList;
    }

    /**
     * adds a panel to the order list sequence
     */
    public void addPanel() {
        System.out.println("add panel");

        currPanel = 0;
        numClicked = 0;

        Random rand = new Random();
        orderList.add(rand.nextInt(colors.length));

        panelList.get(orderList.get(0)).sequence(1);

    }

    /**
     * sets the new score
     */
    public void setScores() {
        System.out.println("set scores");

        // if the user hasn't clicked an incorrect panel
        if (loop) {
            currScore = this.getOrderList().size(); // set the new score

            // if the user has passed the previous high score
            if (currScore > highScore) {
                highScore = currScore;      // set the high score to current score
            }

            this.updateLabels();

            // wait before calling the next panel to let the click color time out
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            this.addPanel();
        }
    }

    /**
     * updates the labels
     */
    public void updateLabels() {
        lblCurrScore.setText("Score: " + currScore);
        lblHighScore.setText("High Score: " + highScore);
    }

    /**
     * called on out of order panel click
     */
    @Override
    public void invalidClick() {
        System.out.println("invalid click");
        System.out.println(orderList.toString());

        loop = false;
        int score = orderList.size() - 1;
        String message = "Rank: ";
        if (score > JOKER_LOW && score < KNIGHT_LOW) {
            message = message + JOKER_TEXT;
        } else if (score > KNIGHT_LOW && score < KING_LOW) {
            message = message + KNIGHT_TEXT;
        } else if (score > KING_LOW) {
            message = message + KING_TEXT;
        }

        message = message + "\nFinal Score: " + score;

        JOptionPane.showMessageDialog(this, message, "GAME OVER", JOptionPane.INFORMATION_MESSAGE);
        this.currScore = 0;
        updateLabels();
    }


    /**
     * called on panel click
     */
    @Override
    public void panelClicked() {
        numClicked++;

        currPanel = numClicked;

        System.out.println("panel clicked: " + currPanel);

        panelList.parallelStream().forEach((panel) -> panel.incrementClicks(numClicked));

        if (currPanel >= this.getOrderList().size()) {
            System.out.printf("Current: %s List: %s\n", currPanel, this.orderList.size());
            setScores();
        }
    }

}