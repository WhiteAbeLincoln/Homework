package cs2410.assn6.components;

/**
 * Created by abe on 3/25/16.
 *
 * Interface that follows the Observer pattern
 */
interface SequenceListener {
    void invalidClick();
    void panelClicked();
}

