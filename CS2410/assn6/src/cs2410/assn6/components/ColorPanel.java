package cs2410.assn6.components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abe on 3/25/16.
 *
 * Custom panel that handles click events and throws custom events on invalid clicks
 *
 * @author abe
 */

class ColorPanel extends JPanel implements MouseListener {
    private Color mainColor;
    private boolean shouldClick;
    private int numberClicked;
    private SimonishFrame parent;
    // using diamond operator from java 7
    private List<SequenceListener> listenerList = new ArrayList<>();

    /**
     * Initializes a color panel
     * @param mainColor background color for the panel
     * @param parent containing JFrame
     */
    ColorPanel(Color mainColor, SimonishFrame parent) {
        super();
        this.mainColor = mainColor;
        this.shouldClick = false;
        this.numberClicked = 0;
        this.parent = parent;

        this.addMouseListener(this);

        this.setBackground(mainColor);
        this.setVisible(true);
    }

    /**
     * sleeps and then updates the color
     */
    private void sleep() {
        sleep(1000);
    }

    /**
     * sleeps and then updates the color
     * @param i milliseconds to sleep
     */
    private void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.updateColor();
    }

    /**
     * returns a color to its main state
     */
    private void updateColor() {
        this.setBackground(this.mainColor);
        this.update(this.getGraphics());
    }


    /**
     * Adds a listener to the SequenceListener Observer Pattern
     * @param toAdd
     */
    void addListener(SequenceListener toAdd) {
        listenerList.add(toAdd);
    }

    /**
     * Brightens a chain of panels in the sequence
     * @param i index of the next panel in the sequence
     */
    void sequence(int i) {
        this.setBackground(brighter());
        this.update(this.getGraphics());
        this.shouldClick = true;
        sleep();
        sleep(500);

        // call the next panel in the sequence
        if (i < parent.getOrderList().size())
            parent.getPanelList().get(parent.getOrderList().get(i)).sequence(i+1);
    }

    /**
     * sets the current number of panels clicked
     * @param i number of panels
     */
    void incrementClicks(int i) {
        numberClicked = i;
        System.out.println("increment: " + i);
    }

    /**
     * Brightens a color by converting it to HSL and back
     * @return brighter color
     */
    private Color brighter() {
        float[] hsl = ColorConvert.fromRGB(this.mainColor);

        // try to increase lightness
        hsl[2] += 0.5 * hsl[2];
        if (hsl[2] >= 100f) hsl[2] = 100f;

        return ColorConvert.toRGB(hsl[0], hsl[1], hsl[2], 1.0f);

    }

    /**
     * Event called on mouse click
     * @param mouseEvent
     */
    public void mouseClicked(MouseEvent mouseEvent) {
        listenerList.forEach(SequenceListener::panelClicked);

        System.out.println("GOOD - Clicked: " + numberClicked + " order: " + parent.getOrderList().toString());

        if (!this.shouldClick || !this.mainColor.equals(parent.getColors()[parent.getOrderList().get(numberClicked - 1)])) {
            System.out.println("Clicked: " + numberClicked + " order: " + parent.getOrderList().toString());
            listenerList.forEach(SequenceListener::invalidClick);
        }
    }

    public void mousePressed(MouseEvent mouseEvent) {
        this.setBackground(brighter());
        this.update(this.getGraphics());
    }

    public void mouseReleased(MouseEvent mouseEvent) {
        this.setBackground(this.mainColor);
        this.update(this.getGraphics());
    }

    public void mouseEntered(MouseEvent mouseEvent) {

    }


    public void mouseExited(MouseEvent mouseEvent) {

    }

}
