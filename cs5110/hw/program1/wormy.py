#!/usr/bin/env python3
# Wormy (a Nibbles clone)
# By Al Sweigart al@inventwithpython.com
# http://inventwithpython.com/pygame
# Released under a "Simplified BSD" license

import random, pygame, sys, math
import pygame.locals as P
import state as S
from constants import *

def main():
    global FPSCLOCK, DISPLAYSURF, BASICFONT, STEPPER, DOSTEP

    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    BASICFONT = pygame.font.Font('freesansbold.ttf', 18)
    STEPPER = False
    DOSTEP = False
    pygame.display.set_caption('Snakes on a plane')

    showStartScreen()
    while True:
        runGame()
        showGameOverScreen()

def runGame():
    global STEPPER, DOSTEP
    state = S.initialState(len(PLAYERS))

    # Steps of the game loop
    # 1. apply inputs
    # 2. update projectiles and check if they hit
    # 3. Remove invalid worms (that have hit obstacles)
    # 4. Grow worms that are on apples
    # 5. Move worms and projectiles based on direction

    while True: # main game loop
        for event in pygame.event.get(): # event handling loop
            if event.type == P.QUIT:
                terminate()
            elif event.type == P.KEYDOWN:
                if event.key == P.K_ESCAPE:
                    terminate()

                if event.key == P.K_q:
                    STEPPER = not STEPPER
                if STEPPER and event.key == P.K_z:
                    DOSTEP = True

                # Step 1
                state = S.handleKeys(event, state)

        if DOSTEP or not STEPPER:
            # Step 2
            state = S.updateProjectiles(state)

            # Step 3 
            state['worms'] = S.checkWorms(state)

            # end game if all worms are gone
            if not any([c for _, c in state['worms']]):
                return

            # Step 4
            state = S.growWorms(state)

            # Step 5
            state['worms'] = S.moveWorms(state['worms'])
            state['projectiles'] = S.moveProjectiles(state['projectiles'])

            DISPLAYSURF.fill(BGCOLOR)
            drawGrid()

            lastScorePos = None
            for p, (_, wormCoords) in zip(PLAYERS, state['worms']):
                drawBox(wormCoords, p['color'])
                lastScorePos = drawScore(len(wormCoords), lastScorePos)

            for apple in state['apples']:
                drawApple(apple)

            drawBox(state['obstacles'], (DARKGRAY, GRAY))

            for _, p in state['projectiles']:
                drawBox([p], (RED, GRAY))

            DOSTEP = False

        pygame.display.update()
        FPSCLOCK.tick(FPS)

def drawPressKeyMsg():
    pressKeySurf = BASICFONT.render('Press a key to play.', True, YELLOW)
    pressKeyRect = pressKeySurf.get_rect()
    pressKeyRect.topleft = (WINDOWWIDTH - 200, WINDOWHEIGHT - 30)
    DISPLAYSURF.blit(pressKeySurf, pressKeyRect)


def checkForKeyPress():
    if len(pygame.event.get(P.QUIT)) > 0:
        terminate()

    keyUpEvents = pygame.event.get(P.KEYUP)
    if len(keyUpEvents) == 0:
        return None
    if keyUpEvents[0].key == P.K_ESCAPE:
        terminate()
    return keyUpEvents[0].key


def showStartScreen():
    titleFont = pygame.font.Font('freesansbold.ttf', 100)
    titleSurfaces = [(titleFont.render(t, True, fg, bg), d, i) for (t, fg, bg, d, i) in TITLES]

    while True:
        DISPLAYSURF.fill(BGCOLOR)

        for idx, (surface, deg, incr) in enumerate(titleSurfaces):
            rotatedSurf = pygame.transform.rotate(surface, deg)
            rotatedRect = rotatedSurf.get_rect()
            rotatedRect.center = (math.floor(WINDOWWIDTH / 2), math.floor(WINDOWHEIGHT / 2))
            DISPLAYSURF.blit(rotatedSurf, rotatedRect)
            # increase rotation each frame
            titleSurfaces[idx] = (surface, deg + incr, incr)

        drawPressKeyMsg()

        if checkForKeyPress():
            pygame.event.get() # clear event queue
            return
        pygame.display.update()
        FPSCLOCK.tick(FPS)


def terminate():
    pygame.quit()
    sys.exit()


def showGameOverScreen():
    gameOverFont = pygame.font.Font('freesansbold.ttf', 150)
    gameSurf = gameOverFont.render('Game', True, WHITE)
    overSurf = gameOverFont.render('Over', True, WHITE)
    gameRect = gameSurf.get_rect()
    overRect = overSurf.get_rect()
    gameRect.midtop = (math.floor(WINDOWWIDTH / 2), 10)
    overRect.midtop = (math.floor(WINDOWWIDTH / 2), gameRect.height + 10 + 25)

    DISPLAYSURF.blit(gameSurf, gameRect)
    DISPLAYSURF.blit(overSurf, overRect)
    drawPressKeyMsg()
    pygame.display.update()
    pygame.time.wait(500)
    checkForKeyPress() # clear out any key presses in the event queue

    while True:
        if checkForKeyPress():
            pygame.event.get() # clear event queue
            return


def drawScore(score, ypos):
    if ypos is None:
        ypos = 10
    # don't display players who haven't joined yet
    if score == 0:
        return ypos
    scoreSurf = BASICFONT.render('Score: %s' % (score), True, WHITE)
    scoreRect = scoreSurf.get_rect()
    scoreRect.topleft = (WINDOWWIDTH - 120, ypos)
    DISPLAYSURF.blit(scoreSurf, scoreRect)
    return ypos + 20


def drawBox(wormCoords, color):
    for coord in wormCoords:
        x = coord[0] * CELLSIZE
        y = coord[1] * CELLSIZE
        wormSegmentRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
        pygame.draw.rect(DISPLAYSURF, color[0], wormSegmentRect)
        wormInnerSegmentRect = pygame.Rect(x + 4, y + 4, CELLSIZE - 8, CELLSIZE - 8)
        pygame.draw.rect(DISPLAYSURF, color[1], wormInnerSegmentRect)


def drawApple(coord):
    x = coord[0] * CELLSIZE
    y = coord[1] * CELLSIZE
    xcenter = x + math.floor(CELLSIZE/2)
    ycenter = y + math.floor(CELLSIZE/2)
    #appleRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
    #pygame.draw.rect(DISPLAYSURF, RED, appleRect)
    pygame.draw.circle(DISPLAYSURF, RED,(xcenter,ycenter),RADIUS)

def drawGrid():
    for x in range(0, WINDOWWIDTH, CELLSIZE): # draw vertical lines
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (x, 0), (x, WINDOWHEIGHT))
    for y in range(0, WINDOWHEIGHT, CELLSIZE): # draw horizontal lines
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (0, y), (WINDOWWIDTH, y))


if __name__ == '__main__':
    main()

