# Wormy
## Setup
1. Install a virtual environment with `python3 -m venv .venv`
2. Enter the virtual environment with `source .venv/bin/activate`
3. Install pygame with `pip install pygame`

## Running
1. Source the virtual environment with `source .venv/bin/activate`
2. Start the game with `./wormy.py` or `python3 wormy.py`

This code has not been tested on Windows, and the I cannot provide any guarantee that it will work
as expected. Please use a Unix environment if possible.

## Keybindings
Players can jump in at any time by hitting a key. Players other than Player 1 are not playing or listed
in the scores until they press a key.

If a player has died during a round, they can get back in with
the initial amount of points as long as there is still one player in the game.

The game has keybindings defined for three players by default:
1.
  **Movement**:
    Arrow Keys
  **Fire**: Left Shift
2.
  **Movement**:
    Left: `h`, Right: `l`, Up: `k`, Down: `j`
  **Fire**: `i`
3.
  **Movment**:
    WASD
  **Fire**: Space

Additionally, players can be added or removed by editing the `PLAYERS` variable in the `constants.py` file.
The required format is `{'keys': (UP, DOWN, LEFT, RIGHT, FIRE), 'color': (PRIMARY, SECONDARY)}`.

The keypad controls all present players at the same time, but will not bring new players in to the game.

## Debugging and Testing
Since it is difficult to test features such as firing with multiple snakes and one person, I added a stepper
testing feature. While playing, hit the `q` key to enter stepper mode. While in this mode, an input
will still be received, but the state will be paused until the `z` key is hit. Press `z` to increment the
state by one iteration, and press `q` again to exit stepper mode.

As an added bonus, this acts as a pause feature.

