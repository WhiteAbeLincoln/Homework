import math
import pygame.locals as P

FPS = 15
WINDOWWIDTH = 1280
WINDOWHEIGHT = 960
CELLSIZE = 20
RADIUS = math.floor(CELLSIZE/2.5)
assert WINDOWWIDTH % CELLSIZE == 0, "Window width must be a multiple of cell size."
assert WINDOWHEIGHT % CELLSIZE == 0, "Window height must be a multiple of cell size."
CELLWIDTH = int(WINDOWWIDTH / CELLSIZE)
CELLHEIGHT = int(WINDOWHEIGHT / CELLSIZE)

#             R    G    B
WHITE     = (255, 255, 255)
BLACK     = (  0,   0,   0)
RED       = (255,   0,   0)
GREEN     = (  0, 255,   0)
DARKGREEN = (  0, 155,   0)
DARKGRAY  = ( 40,  40,  40)
GRAY  =     ( 80,  80,  80)
YELLOW    = (255, 255,   0)
BLUE      = (0,     0, 255)
BGCOLOR = BLACK

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'

HEAD = 0 # syntactic sugar: index of the worm's head
# titles: (text, foreground color, background color, initial degress, increment degrees)
TITLES = [('WORMS', WHITE, DARKGREEN, 0, 3), ('SNAKES', GREEN, None, 0, 7), ('USU USU USU', BLUE, None, 180, 45)]

# keys is tuple in form (UP,DOWN,LEFT,RIGHT,FIRE)
# color is tuple in form (outer, inner)
PLAYERS = [
    {'keys': (P.K_UP, P.K_DOWN, P.K_LEFT, P.K_RIGHT, P.K_RSHIFT), 'color': (YELLOW, GREEN)},
    {'keys': (P.K_k, P.K_j, P.K_h, P.K_l, P.K_i), 'color': (BLUE, GREEN)},
    {'keys': (P.K_w, P.K_s, P.K_a, P.K_d, P.K_SPACE), 'color': (RED, GREEN)}
]

KEYPAD_KEYS = (P.K_KP8, P.K_KP2, P.K_KP4, P.K_KP6)
