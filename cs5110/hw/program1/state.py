import random, math
from constants import *
import pygame.locals as P

def handleKeys(event, state):
    state['worms'] = handleDirections(event.key, state['worms'])
    state['projectiles'] += handleFire(event.key, state['worms'])
    return state

def handleFire(key, worms):
    projectiles = []

    for p, w in zip(PLAYERS, worms):
        if not w[1]:
            continue

        # we start the projectile two positions out so the player doesnt run into it
        if key == p['keys'][4]:
            proj = (w[0], applyDir(w[0], w[1][HEAD]))
            if not outOfBounds(proj[1]):
                projectiles.append(proj)

    return projectiles

def handleDirections(key, worms):
    # keypad controls all
    if (key in KEYPAD_KEYS):
        # don't have the keypad bring in players who havent started yet or are dead
        return [(setDirection(KEYPAD_KEYS, key, w[0]), w[1]) if w[1] else w for w in worms]

    newWorms = []
    # only works because we've ensured state['worms'] is stable
    for p, w in zip(PLAYERS, worms):
        newW = (setDirection(p['keys'], key, w[0]), w[1])
        # allow player to jump in if dead. they have to change direction so we know a key was hit
        # but when they die we set direction to the empty string, so any key should work
        if not newW[1] and newW[0] != w[0]:
            newW = initialWorm()
        newWorms.append(newW)
    return newWorms

def setDirection(keys, key, direction):
    if (key == keys[2]) and direction != RIGHT:
        return LEFT
    elif (key == keys[3]) and direction != LEFT:
        return RIGHT
    elif (key == keys[0]) and direction != DOWN:
        return UP
    elif (key == keys[1]) and direction != UP:
        return DOWN

    return direction

def initialWorm():
    startx = random.randint(5, CELLWIDTH - 6)
    starty = random.randint(5, CELLHEIGHT - 6)
    return (RIGHT,
            [(startx,     starty),
             (startx - 1, starty),
             (startx - 2, starty)]
           )

def getRandomLocation():
    return (random.randint(0, CELLWIDTH - 1), random.randint(0, CELLHEIGHT - 1))

# generates a list of items using the given function, ensuring 
# that each item is unique
# WARNING: will loop infinitely if genfn returns a constant value
def uniqueList(genfn, n, arr = None):
    if arr is None:
        arr = []
    l = list(arr)
    for _ in range(0, n):
        r = genfn()
        while r in l:
            r = genfn()
        l.append(r)

    return l

def initialState(nw = 1, na = 3):
    player1 = initialWorm()
    # we initially set all players but player 1 to empty
    worms = [player1] + [('', []) for w in range(0, nw - 1)]
    # Start the apple in a random place.
    apples = uniqueList(getRandomLocation, na)
    o1 = getRandomLocation()
    return {'worms': worms, 'apples': apples, 'projectiles': [], 'obstacles': [o1, applyDir(RIGHT, o1)]}

def outOfBounds(head):
    return head[0] <= -1 or head[0] >= CELLWIDTH or head[1] <= -1 or head[1] >= CELLHEIGHT

def hitObstacle(head, obs):
    return head in obs

def hitWorm(worm, worms):
    # we don't want to compare the head of the current worm to itself
    return any((worm[HEAD] in (worm[1:] if w is worm else w) for _, w in worms))

# given a list of worms, returns a new list of worms
def checkWorms(state):
    worms = state['worms']
    obs = state['obstacles']
    # w[1] check is to exclude dead players
    return [('', []) if not w[1] or outOfBounds(w[1][HEAD]) or hitWorm(w[1], worms) or hitObstacle(w[1][HEAD], obs) else w for w in worms]

def split_on_condition(seq, condition):
    a, b = [], []
    for item in seq:
        (a if condition(item) else b).append(item)
    return a, b

def shootWorm(p, w):
    # we want to always return true after the first instance
    # there should be a better way of doing this
    split = False
    def cond(b):
        nonlocal split
        split = split or b == p
        return split
    return split_on_condition(w, cond)

def updateProjectiles(state):
    state['projectiles'] = [p for p in state['projectiles'] if not outOfBounds(p[1])]
    obstacles = []
    hitprojs = []

    for p in state['projectiles']:
        for i, w in enumerate(state['worms']):
            if p in hitprojs:
                continue
            obs, body = shootWorm(p[1], w[1])
            state['worms'][i] = (w[0], body)
            if obs:
                obstacles.extend(obs)
                hitprojs.append(p)

    state['obstacles'] += obstacles
    state['projectiles'] = [p for p in state['projectiles'] if p not in hitprojs]
    return state

def growWorms(state):
    # we can't remove apples until all worms have been checked
    # or a new apple may overlap with a worm head and they would
    # grow this turn
    # we have multiple apples, so we cant just shrink for
    # each apple eaten
    removed = []
    for _, wormCoords in state['worms']:
        # handle dead players
        if not wormCoords:
            continue

        for apple in state['apples']:
            if wormCoords[HEAD] == apple:
                removed.append((apple,wormCoords))

    # shrink the worms that didn't eat apples
    wormsAte = [w for _, w in removed]
    for _, wormCoords in state['worms']:
        # handle dead player
        if not wormCoords:
            continue
        if wormCoords not in wormsAte:
            del wormCoords[-1] # remove worm's tail segment

    # remove the apples that were eaten and add new ones
    removedApples = [a for a, _ in removed]
    state['apples'] = uniqueList(getRandomLocation,
                                 len(removedApples),
                                 [a for a in state['apples'] if a not in removedApples]
                                )
    return state

def applyDir(direction, pos):
    if direction == UP:
        return (pos[0], pos[1] - 1)
    elif direction == DOWN:
        return (pos[0], pos[1] + 1)
    elif direction == LEFT:
        return (pos[0] - 1, pos[1])
    elif direction == RIGHT:
        return (pos[0] + 1, pos[1])
    return pos

def moveWorms(worms):
    # move the worm by adding a segment in the direction it is moving
    # have already removed the last segment in growWorms
    return [(d, wc if not wc else [applyDir(d, wc[HEAD])] + wc) for (d, wc) in worms]

def moveProjectiles(projs):
    return [(d, applyDir(d, p)) for (d, p) in projs]
