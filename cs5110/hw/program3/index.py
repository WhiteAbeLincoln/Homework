import sys
import csv
from functools import reduce
from copy import deepcopy

def pairwise(cs):
    from itertools import combinations
    return combinations(cs, 2)

def partition(pred, iterable):
    from itertools import tee, filterfalse
    'Use a predicate to partition entries into false entries and true entries'
    # partition(is_odd, range(10)) --> 0 2 4 6 8   and  1 3 5 7 9
    t1, t2 = tee(iterable)
    return filter(pred, t2), filterfalse(pred, t1)

def majgraph(g, render, f):
    from graphviz import Digraph
    dot = Digraph(name='Majority')

    ks = g.keys()
    out = g.copy()
    for k in ks:
        dot.node(k)
        out[k] = []

    for c1, c2 in pairwise(ks):
        c1votes = g[c1]
        c2votes = g[c2]

        pref_c1_c2 = 0

        for (v1, v2) in zip(c1votes, c2votes):
            if v1 < v2:
                pref_c1_c2 += 1

        pref_c2_c1 = len(c1votes) - pref_c1_c2

        if pref_c1_c2 > pref_c2_c1:
            dot.edge(c1, c2)
            out[c1].append(c2)
        elif pref_c1_c2 < pref_c2_c1:
            dot.edge(c2, c1)
            out[c2].append(c1)

    dot.attr(label='Majority Graph',labelloc='top',labeljust='left')
    if render:
        dot.render(f'{f}.dot', view=True)
    return out

def slater(g):
    pass

def get_choice(choice, votes):
    return ([(1 if p == choice else 0) for p in pref] for pref in votes)

def majority(choices, num_votes):
    cnt = list(reduce(lambda acc, pref: [sum(pair) for pair in zip(acc, pref)], choices, [0]*num_votes))
    maj, losers = partition(lambda t: t[1] >= num_votes/2, enumerate(cnt))
    return next(map(lambda t: t[0], maj), None), cnt, maj, losers

def runoff(csv):
    candidates, votes = deepcopy(csv)
    max_iter = len(candidates)
    n = 0
    removed = []
    while n < max_iter:
        n += 1
        num_votes = len(votes)
        num_candidates = len(candidates)
        if num_candidates == 1:
            print(f'{candidates[0]} wins by elimination')
            removed.extend(candidates)
            return removed[::-1]
        mw, first_cnt, maj, losers = majority(get_choice(1, votes), num_votes)
        if mw is not None:
            print(f'{candidates[mw]} wins by majority')
            removed.extend(candidates[i] for i, v in losers)
            removed.extend(candidates[i] for i, v in maj if i != mw)
            removed.append(candidates[mw])
            return removed[::-1]
        # remove lowest scoring candidate(s)
        min_val = min(first_cnt)
        to_remove = [i for i,v in enumerate(first_cnt) if v == min_val]
        if len(to_remove) == num_candidates:
            print(f'Tie between {", ".join(candidates)}')
            removed.extend(candidates)
            return removed[::-1]
        # remove in reverse order so as to not throw off following indexes
        for i in sorted(to_remove, reverse=True):
            adjusted = []
            for idx in range(num_votes):
                # if this candidate was the first choice for any of the voters
                if votes[idx][i] == 1:
                    adjusted.append(str(idx))
                    votes[idx] = [v-1 for v in votes[idx]]
                del votes[idx][i]
            print(f'Eliminated {candidates[i]}')
            if adjusted:
                print(f'Adjusted preferences for voters {", ".join(adjusted)}')
            else:
                print(f'No preferences adjusted')
            removed.append(candidates[i])
            del candidates[i]

def bucklin(csv):
    candidates, votes = csv
    num_votes = len(votes)
    num_candidates = len(candidates)

    k = 1
    choices = []
    while k < num_candidates+1:
        choices.extend(get_choice(k, votes))
        maj, *_ = majority(choices, num_votes)
        if maj is not None:
            print(f'{candidates[maj]} won with first {k} preferences')
            return (k, candidates[maj])
        k += 1

    return None

def parse_csv(file):
    from collections import defaultdict

    reader = csv.reader(file)
    try:
        candidates = next(reader, None)
        if not candidates:
            raise SyntaxError('csv must have a header listing candidates')
        n_c = len(candidates)
        graph = defaultdict(list)
        votes = []
        for row in reader:
            if len(row) != n_c:
                raise SyntaxError('each voter must vote for all candidates (row must have same number of entries as header)')
            prefs = []
            for i, val in enumerate(row):
                k = candidates[i]
                v = int(val)
                if v in prefs:
                    raise SyntaxError('vote preferences must be unique')
                if v > n_c or v < 1:
                    raise SyntaxError('vote preference must be between 1 and the number of candidates')
                graph[k].append(v)
                prefs.append(v)
            votes.append(prefs)
        if not votes:
            raise SyntaxError('csv must have votes')

        return graph, (candidates, votes)

    except csv.Error as e:
        sys.exit('file {}, line {}: {}'.format(filename, reader.line_num, e))

def help(n):
    print("""{name} - Compare voting methods
USAGE:
    {name} -h
    {name} [-srb] [FILES...]
OPTIONS:
    -h          Show this
    -s          Enable Slater
    -r          Enable Instant Runoff
    -b          Enable Bucklin
    -m          Disable Majority graph
    FILES...    Elections specified as csv files. Read from stdin if no filenames provided
""".format(name=n))

def main(argv):
    from collections import defaultdict
    import getopt

    graphs = []
    do_slater = False
    do_runoff = False
    do_bucklin = False
    do_majority = True

    try:
        opts, files = getopt.getopt(argv[1:], 'hsrbm', ['help'])
        for o, a in opts:
            if o in ('-h', '--help'):
                help(argv[0])
                sys.exit()
            elif o == '-s':
                do_slater = True
            elif o == '-r':
                do_runoff = True
            elif o == '-b':
                do_bucklin = True
            elif o == '-m':
                do_majority = False
            else:
                assert False, 'unhandled option'

        if len(files) == 0:
            with sys.stdin as csvfile:
                graphs.append((parse_csv(csvfile), 'stdin'))
        else:
            for f in files:
                with open(f, newline='') as csvfile:
                    graphs.append((parse_csv(csvfile), f))

    except getopt.GetoptError as err:
        print(err)
        help(argv[0])
        sys.exit(2)

    for (graph, data), f in graphs:
        slater_res = None
        bucklin_res = None
        runoff_res = None
        nc = len(data[0])
        print(f'Election: {f}')
        mg = majgraph(graph, do_majority, f)
        if do_slater:
            print('Slater:')
            slater(mg)
        if do_runoff:
            print('Instant Runoff:')
            runoff_res = runoff(data)
        if do_bucklin:
            print('Bucklin:')
            bucklin_res = bucklin(data)

        with open(f'{f}.out.csv', 'w', newline='') as outfile:
            bucklin_name=''
            fieldnames=['ranking']
            if slater_res:
                fieldnames.append('slater')
            if bucklin_res:
                bucklin_name=f'bucklin k={bucklin_res[0]}'
                fieldnames.append(bucklin_name)
            if runoff_res:
                fieldnames.append('runoff')
            writer = csv.DictWriter(outfile, fieldnames=fieldnames)
            writer.writeheader()
            for i in range(nc):
                d = {'ranking': i+1}
                if slater_res:
                    d['slater'] = slater_res[i]
                if bucklin_res:
                    if i == 0:
                        d[bucklin_name] = bucklin_res[1]
                if runoff_res:
                    d['runoff'] = runoff_res[i]
                writer.writerow(d)

if __name__ == '__main__':
    main(sys.argv)
