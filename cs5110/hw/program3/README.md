I wasn't able to complete Slater ranking in time, since instant runoff proved to be more
difficult than I expected. Slater would involve finding the minimum number of edges to
change to produce a DAG from a digraph, which would also take a large amout of time.

# Running
Install `graphviz` with pip or by running `pip install -r requirements.txt`.
I used a python 3.8.1 virtualenv, but I don't believe I used any 3.8.1 features.

Run with `python index.py`. Get help with `python index.py --help`.  

Votes are provided in csv format, with the candidates in the header.
Voters must rank all candidates.

# Output
Results are stored in a file having the same filename as the input data but with `.out.csv`
appended.


