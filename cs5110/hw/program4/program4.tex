% Created 2020-04-29 Wed 16:54
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../../preamble.tex} \DeclareMathOperator{\cube}{cube}
\author{Abraham White}
\date{\today}
\title{Vicrey Clark Grove}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Vicrey Clark Grove},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
I couldn't record a video to demonstrate the program, but this document goes in enough
detail in the Results section that I don't feel it would be necessary. The video would
just be me running some scenarios and talking about the results, which can be done as well
(if not better) in text form.

\section*{Setup}
\label{sec:orgdd011ca}
First we need to set up our python environment.
\begin{minted}[]{bash}
python3 -m venv .venv
\end{minted}
We activate the environment and install the required packages.
\begin{minted}[]{bash}
source ./.venv/bin/activate
pip install docopt
\end{minted}

\section*{Implementation}
\label{sec:orgfeb238a}
We start with program configuration through command-line arguments.
\begin{minted}[]{python}
"""Vickrey Clark Grove Auctions.

Usage:
  vcg.py [options]

Options:
  -h --help    Show this text
  -b BIDS      Specify the bids per click as space-seperated list of floats
  -s SLOTS     Specify the clicks per week for ad slots as list of integers
"""
from docopt import docopt
\end{minted}

\begin{minted}[]{python}
if __name__ == '__main__':
    args = docopt(__doc__)
    args['-b'] = list(map(float, args['-b'].split()))
    args['-s'] = list(map(int, args['-s'].split()))
    if not args['-b'] or not args['-s']:
        print(__doc__)
        sys.exit(1)
    main(args)
\end{minted}

Our first step is to determine which bidder wins which advertising slot. We are assuming that bidders
are willing to pay for any slot, so this is simply matching the highest bidders with the slots having
the most clicks. We label each bid and each slot by index before sorting so that the results will match
the input.
\begin{minted}[]{python}
def main(args):
    bids = list(enumerate(args['-b']))
    slots = list(enumerate(args['-s']))
    snd = lambda p: p[1]
    bids.sort(key=snd, reverse=True)
    slots.sort(key=snd, reverse=True)
    
    winners = list(zip(bids, slots))
    for (b,s) in winners:
        print(f"Bidder {b[0]} won slot {s[0]} ({s[1]} clicks) with bid of {b[1]}")
\end{minted}

Now that we have the winning bidders, we need to calculate the price paid by the winners.
First we need to find the bidders that were displaced by each winning bidder, along with
the slots that each bidder would have won had the winning bidder not been present. We do this
by zipping the lower bids with the ad slots if the current bidder was not present,
and the ad slots given the current configuration.

Since we want the current utility to be zero when a bidder has not won any slots, we pad the
end of the slots list with zeros. Then we iterate over the list of lists, multiplying the
price a displaced bidder would have paid by the difference between their current winnings
and what could have been. The sum of this lost utility is the price that the bidder pays.
\begin{minted}[]{python}
    displaced = ((i, zip(map(snd, bids[i+1:]),
                         map(snd, slots[i:]),
                         padby(map(snd, slots[i+1:]), lambda: 0)
                        )) for i in range(len(winners)))
    vals = ((i, sum(p*(new - curr) for (p,new,curr) in l)) for (i,l) in displaced)
    for (i,pay) in vals:
        b = bids[i]
        print(f"Bidder {b[0]} pays {pay}")
\end{minted}
\section*{Running}
\label{sec:orgc6259ec}
Source the virtual environment
\begin{minted}[]{bash}
source ./.venv/bin/activate
\end{minted}
Run with \texttt{python3 bandit.py}
\section*{Results}
\label{sec:orgc2d1802}
First we run with the example given in the assignment:
\begin{minted}[]{bash}
python vcg.py -s "500 300 100" -b ".40 .50 .30 .20 .10"
\end{minted}

\begin{verbatim}
Bidder 1 won slot 0 (500 clicks) with bid of 0.5
Bidder 0 won slot 1 (300 clicks) with bid of 0.4
Bidder 2 won slot 2 (100 clicks) with bid of 0.3
Bidder 1 pays 160.0
Bidder 0 pays 80.0
Bidder 2 pays 20.0
\end{verbatim}

\noindent The results match the data given in the assignment example. \\

We can have another configuration where two advertising slots have
the same number of clicks.
\begin{minted}[]{bash}
python vcg.py -s "500 300 300 100" -b ".40 .50 .30 .20 .10"
\end{minted}

\begin{verbatim}
Bidder 1 won slot 0 (500 clicks) with bid of 0.5
Bidder 0 won slot 1 (300 clicks) with bid of 0.4
Bidder 2 won slot 2 (300 clicks) with bid of 0.3
Bidder 3 won slot 3 (100 clicks) with bid of 0.2
Bidder 1 pays 130.0
Bidder 0 pays 50.0
Bidder 2 pays 50.0
Bidder 3 pays 10.0
\end{verbatim}

\noindent Notice that though Bidder 0 and Bidder 2 bid differently,
they both end up paying the same amount for the same good. \\

Another interesting configuration would be if two bidders bid the same.
\begin{minted}[]{bash}
python vcg.py -s "500 300 100" -b ".40 .40 .30 .20 .10"
\end{minted}

\begin{verbatim}
Bidder 0 won slot 0 (500 clicks) with bid of 0.4
Bidder 1 won slot 1 (300 clicks) with bid of 0.4
Bidder 2 won slot 2 (100 clicks) with bid of 0.3
Bidder 0 pays 160.0
Bidder 1 pays 80.0
Bidder 2 pays 20.0
\end{verbatim}

\noindent Notice that this result is the same as the problem given in the assignment.
Since what matters for determining price paid is the loss incurred by the other bidders,
changing the highest bidder's valuation to the next highest does not change the results. \\

What if we changed a lower bidder to tie with another lower bidder, rather than reducing the
highest bid?
\begin{minted}[]{bash}
python vcg.py -s "500 300 100" -b ".50 .30 .30 .20 .10"
\end{minted}

\begin{verbatim}
Bidder 0 won slot 0 (500 clicks) with bid of 0.5
Bidder 1 won slot 1 (300 clicks) with bid of 0.3
Bidder 2 won slot 2 (100 clicks) with bid of 0.3
Bidder 0 pays 140.0
Bidder 1 pays 80.0
Bidder 2 pays 20.0
\end{verbatim}

\noindent The price paid by the highest bidder (Bidder 0) decreases, but the others stay the same.
This is because the price paid by the highest bidder depends on the valuations by lower bidders.
Since the valuations of lower bidders decreased, we should expect the price paid by higher bidders 
to decrease. \\

What if we have the same number of bidders as items? Since the price paid is determined by the loss
of lower bidders, we would expect that the bidder at the bottom wouldn't pay anything, as there
was no harm caused to lower bidders.
\begin{minted}[]{bash}
python vcg.py -s "600 500 400 300 200" -b ".50 .30 .30 .20 .10"
\end{minted}

\begin{verbatim}
  Bidder 0 won slot 0 (600 clicks) with bid of 0.5
  Bidder 1 won slot 1 (500 clicks) with bid of 0.3
  Bidder 2 won slot 2 (400 clicks) with bid of 0.3
  Bidder 3 won slot 3 (300 clicks) with bid of 0.2
  Bidder 4 won slot 4 (200 clicks) with bid of 0.1
  Bidder 0 pays 90.0
  Bidder 1 pays 60.0
  Bidder 2 pays 30.0
  Bidder 3 pays 10.0
  Bidder 4 pays 0
\end{verbatim}
\noindent This matches with our expectations. In this case an auctioneer may want to set a minimum payment
to prevent losing money on items. This is different than setting a minimum bid. For instance,
if we said the minimum bid was 0.20, then the lowest bidder would still pay 0.\\
\begin{minted}[]{bash}
python vcg.py -s "600 500 400 300 200" -b ".50 .30 .30 .20 .20"
\end{minted}

\begin{verbatim}
  Bidder 0 won slot 0 (600 clicks) with bid of 0.5
  Bidder 1 won slot 1 (500 clicks) with bid of 0.3
  Bidder 2 won slot 2 (400 clicks) with bid of 0.3
  Bidder 3 won slot 3 (300 clicks) with bid of 0.2
  Bidder 4 won slot 4 (200 clicks) with bid of 0.2
  Bidder 0 pays 100.0
  Bidder 1 pays 70.0
  Bidder 2 pays 40.0
  Bidder 3 pays 20.0
  Bidder 4 pays 0
\end{verbatim}

What if all the items were the same? We would expect that all of the winning bidders
should pay the same price, since the only loss incured by lower bidders is that of the
losers. 
\begin{minted}[]{bash}
python vcg.py -s "400 400 400 400" -b ".50 .40 .30 .20 .10"
\end{minted}

\begin{verbatim}
Bidder 0 won slot 0 (400 clicks) with bid of 0.5
Bidder 1 won slot 1 (400 clicks) with bid of 0.4
Bidder 2 won slot 2 (400 clicks) with bid of 0.3
Bidder 3 won slot 3 (400 clicks) with bid of 0.2
Bidder 0 pays 40.0
Bidder 1 pays 40.0
Bidder 2 pays 40.0
Bidder 3 pays 40.0
\end{verbatim}


Similarly, all bidders should pay 0 if we have the same number of bidders as
items and all items are the same. 
\begin{minted}[]{bash}
python vcg.py -s "400 400 400 400" -b ".50 .40 .30 .20"
\end{minted}

\begin{verbatim}
Bidder 0 won slot 0 (400 clicks) with bid of 0.5
Bidder 1 won slot 1 (400 clicks) with bid of 0.4
Bidder 2 won slot 2 (400 clicks) with bid of 0.3
Bidder 3 won slot 3 (400 clicks) with bid of 0.2
Bidder 0 pays 0.0
Bidder 1 pays 0.0
Bidder 2 pays 0.0
Bidder 3 pays 0
\end{verbatim}


Finally, we want to see if VCG can be manipulated. Assume that one of the bidders can control multiple bids.
We want to see if this manipulation is actually beneficial. Assume we have three items up for bid, \(\{A,B,C\}\).
The false bidder values these items equally at 0.5, but doesn't want to pay their actual valuation.
By placing multiple lower bids, can the false bidder obtain one of the items at a lower price? What happens if the
bidder places three bids for 0.3 rather than a truthful bid for 0.5?
\begin{minted}[]{bash}
python vcg.py -s "500 400 300" -b ".8 .3 .3 .3 .6 .2"
\end{minted}

\begin{verbatim}
Bidder 0 won slot 0 (500 clicks) with bid of 0.8
Bidder 4 won slot 1 (400 clicks) with bid of 0.6
Bidder 1 won slot 2 (300 clicks) with bid of 0.3
Bidder 0 pays 180.0
Bidder 4 pays 120.0
Bidder 1 pays 90.0
\end{verbatim}

\noindent The bidder won slot 2 and paid 90.

Now what happens if he bids truthfully?
\begin{minted}[]{bash}
python vcg.py -s "500 400 300" -b ".8 .5 .6 .2"
\end{minted}

\begin{verbatim}
Bidder 0 won slot 0 (500 clicks) with bid of 0.8
Bidder 2 won slot 1 (400 clicks) with bid of 0.6
Bidder 1 won slot 2 (300 clicks) with bid of 0.5
Bidder 0 pays 170.0
Bidder 2 pays 110.0
Bidder 1 pays 60.0
\end{verbatim}

\noindent The bidder wins the same slot (slot 2), but he pays 60 rather than 90. In this
case it would have been better to bid truthfully, which matches with the theory.

\section*{Appendix}
\label{sec:orgdb4ad06}
\subsection*{Pad By}
\label{sec:org1a47c63}
Creates an infinite iterable using the given lambda function, with a prefix consisting of the given list 
\begin{minted}[]{python}
def padby(lst, fn):
    yield from lst
    while True: 
        yield fn()
\end{minted}
\subsection*{Full Source}
\label{sec:orga3688d2}
See \texttt{vcg.py}.
\end{document}