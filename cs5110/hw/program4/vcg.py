"""Vickrey Clark Grove Auctions.

Usage:
  vcg.py [options]

Options:
  -h --help    Show this text
  -b BIDS      Specify the bids per click as space-seperated list of floats
  -s SLOTS     Specify the clicks per week for ad slots as list of integers
"""
from docopt import docopt
import sys

def padby(lst, fn):
    yield from lst
    while True: 
        yield fn()

def main(args):
    bids = list(enumerate(args['-b']))
    slots = list(enumerate(args['-s']))
    snd = lambda p: p[1]
    bids.sort(key=snd, reverse=True)
    slots.sort(key=snd, reverse=True)
    
    winners = list(zip(bids, slots))
    for (b,s) in winners:
        print(f"Bidder {b[0]} won slot {s[0]} ({s[1]} clicks) with bid of {b[1]}")
    displaced = ((i, zip(map(snd, bids[i+1:]),
                         map(snd, slots[i:]),
                         padby(map(snd, slots[i+1:]), lambda: 0)
                        )) for i in range(len(winners)))
    vals = ((i, sum(p*(new - curr) for (p,new,curr) in l)) for (i,l) in displaced)
    for (i,pay) in vals:
        b = bids[i]
        print(f"Bidder {b[0]} pays {pay}")

if __name__ == '__main__':
    args = docopt(__doc__)
    args['-b'] = list(map(float, args['-b'].split()))
    args['-s'] = list(map(int, args['-s'].split()))
    if not args['-b'] or not args['-s']:
        print(__doc__)
        sys.exit(1)
    main(args)
