% Created 2020-04-22 Wed 18:28
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../../preamble.tex} \DeclareMathOperator{\cube}{cube}
\author{Abraham White}
\date{\today}
\title{K-Armed Bandit}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={K-Armed Bandit},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle

\section*{Introduction}
\label{sec:orgcf9a231}
As agents pursue their goals in an open environment, they may encounter other agents.
It may be advantageous for these agents to cooperate and form a coalition, but it is also
possible that it would be advantageous for one of the agents to sabotage the other.

In many systems, there is the assumption that encountered agents will share a common goal and cooperate.
These types of systems are called ad-hoc -- agents cooperate, but are missing information on the capabilities
and state of the other agents (hereafter referred to as opponents), and must figure out how best to work
together through modeling or other strategies. Stone and Kraus give an example of a soda can gathering robot
which has to cooperate with an older model to optimally gather the most cans from beaches [4]. 

In other systems, there is the assumption that agents will be competing in a game or for some resource.
In this case the agents are selfish rather than cooperating, and sometimes may be actively hostile.
Poker is an ideal example of such a game. Multiple agents must deal with deception, risk management, and
model the opponents in order to win the game [2].

However, in the real world we can encounter agents of both types, and need to be effective in dealing
with them. For instance, the ”hitchhiking robot” hitchBOT encountered cooperative agents until its demise
in Philadelphia in 2015. A way of modeling the behavior and type of the agents it encountered may have
enabled it to last longer.

Is it possible to devise one model which can handle both selfish agents and cooperative agents, while
accounting for the lack of information about the behaviour, motivations, or capabilities of the
encountered agent?
\section*{Previous Work}
\label{sec:orgcc0d1de}
First we need to come up with a game or problem on which we should apply our agents.

There is a common problem in agent systems called the k-armed bandit problem. In this problem, a
learning (not reactive) agent is given a goal - maximize the payoff over a set amount of time.
The agent pulls one of k arms (such as on a slot machine) at each time step, which each give
payment according to a different random distribution. This problem exemplifies the exploration vs exploitation
dilemma - should the agent explore the other arms looking for one with a higher average payoff, or should they stick
with an already known arm that gives a high payoff?

Stone and Kraus introduce a variation of this problem in “To teach or not to teach?: decision making under uncertainty
in ad hoc teams” [4]. As summarized by Stone et al., the variation on the problem involves two players, a teacher, and
a learner. The learner has less information than the teacher, and the teacher must make the decision to pull the arm
with the highest payoff, or teach the learner by pulling a different arm. This is an example of an ad-hoc multi-agent
system, and Stone et al mention that this problem forms a great base for the theoretical analysis of collaborative ad
hoc teams [5]. We will build a game upon this problem for this paper.

The next question is how should one agent model the behavior of the other? Albrecht and Stone have created a great summary
of different modelling methods [1]:
\begin{description}
\item[{Policy Reconstruction}] Attempts to reconstruct the opponent’s decision making by starting
with an idealized model and then fitting parameters based on observed behavior.
\item[{Type-Based Reasoning}] Assumes that agents come in one of several different types,
and then attempts to fit the observed behaviors with previously encountered types which have associated models.
\item[{Classification}] Attempts to define agents by behavior into more general categories,
such as “aggressive” or “defensive”. Often uses machine-learning classification tools such as k-means or SVM classifiers.
\item[{Plan Recognition}] Identifies the goals of the agent based on observation. From this end
goal possible plans for achieving are reverse engineered.
\item[{Recursive Reasoning}] Accounts for the opposing agents also modelling the behavior of the
agent in question. Think of the scene in The Princess Bride, where the Man in Black and Vizzini have their battle of
wits. Vizzini attempts to model the Man in Black’s thought process, but then he also recursively accounts for the Man
in Black doing the same for Vizzini. This can regress infinitely, but is usually terminated at a predetermined depth.
Osten, Kirley, and Miller introduce a similar technique for opponent modelling in multi-agent systems which they call
Multi-agent Theory of Mind (MToM) [6].
\item[{Graphical Models}] Makes dependencies between state, agent decisions, and utility explicit by modeling the system as a graph.
\item[{Group Modelling}] Most of the other methods create models for a single agent and assume that they will be used independently
for each agent when agents are acting as a group. Group modelling instead models the behavior of the group as a whole
instead of a collection of individual agents.
\end{description}

We do have to be careful with the modelling method, as some ways of discovering the behavior of the opponent may also influence
the opponent, causing the model to not be accurate [3]. Out of these options, Policy Reconstruction and Type-Based Reasoning will most
likely be the best methods, since we don’t have to worry about groups of agents, don’t have to care about the opponent doing any
reasoning about the agent, and already know the general plan of the opponent (to get as many points as possible).
\section*{Contribution}
\label{sec:org694523b}
This paper sets up an empirical experiment to compare the performance of different algorithms
for the Bernoulli K-Armed Bandit problem. We also evaluate the behavior when two agents are involved,
and arms can only be pulled once per turn. We simulate the teaching problem proposed by Stone and Kraus,
showing how a "teacher" agent with more information can support a "learner" by sharing information. 
Finally, we compare the performance of an agent when the other agent lies about information.

\section*{Experimental Design}
\label{sec:org6563aec}
We create a simulation consisting of two agents (Agent A and Agent B) pulling arms of a Bernoulli
\emph{k}-armed bandit. A Bernoulli bandit is a \emph{k}-armed bandit where rather than each arm giving out
rewards from a probability distribution, each arm \(i\) has an associated probability \(p_i\) of
giving a reward of 1, and probability \(1 - p_i\) of giving no reward or 0. At each time tick the
agents can pull an arm, but not the same arm. In order to prevent the first agent hogging a good arm,
we alternate the order of play, i.e. the agent who pulled first at time \(t\) will pull second at time \(t+1\),
and vice versa. 

The agents use two basic algorithms for determining which arm to pull, \(\epsilon\)-Greedy and Thompson
Sampling. We evaluate the performance of the algorithm using the accumulated score, and a measure
called \textbf{cumulative regret}. Cumulative regret is defined as \[ n \mu^{*} - \sum_{t=1}^n R_t \]
where \(n\) is the number of iterations in the game, \(\mu^{*}\) is the maximal probability in the bandit,
and \(R_t\) is the probability of the arm pulled at time \(t\).

Agent B can choose to share the results of pulling an arm with Agent A with probability \(1-p\), or to
lie about the results with probability \(p\). Additionally, Agent B can also stay silent, providing no
information.

Agent A must determine whether to trust the information provided by Agent B, as using the information
when Agent B lies with high probability will cause Agent A to make incorrect decisions when selecting
an arm, greatly increasing the cumulative regret. Agent A does this by keeping track of the information
provided by Agent B. When this information deviates significantly from Agent A's own observations,
Agent A determines the information provided by Agent B to be invalid and doesn't use it.

\section*{Results}
\label{sec:org010c27d}
\subsection*{Single Agent, \(\epsilon\)-Greedy vs Single Agent, Thompson Sampling}
\label{sec:org328a10e}
This experiment compares the performance of the \(\epsilon\)-Greedy algorithm vs the 
Thompson Sampling algorithm. We perform 10,000 iterations with 20 arms. The \(\epsilon\)
parameter is set to 0.1, meaning the agent will choose a lever at random 1\% of the time,
otherwise choosing the lever with the best known probability.
\begin{minted}[]{bash}
   source ./.venv/bin/activate
   python bandit.py -i 10000 -k 20 -s 1 -a none -b greedy > single_epsilon.json
   python bandit.py -i 10000 -k 20 -s 1 -a none -b thompson > single_thompson.json
\end{minted}
We plot the time step vs cumulative regret.
\begin{center}
\includegraphics[width=.9\linewidth]{single-regret.png}
\end{center}

\begin{table}[htbp]
\caption{Bandit probabilties vs final estimates}
\centering
\begin{tabular}{rrr}
Actual & Epsilon & Thompson\\
\hline
0.5118216247002567 & 0.48404255319148937 & 0.5\\
0.9504636963259353 & 0.9500113352981183 & 0.9434416365824309\\
0.14415961271963373 & 0.11290322580645161 & 0\\
0.9486494471372439 & 0.9380530973451328 & 0.9486831387455879\\
0.31183145201048545 & 0.32075471698113206 & 0\\
0.42332644897257565 & 0.631578947368421 & 0.25\\
0.8277025938204418 & 0.803921568627451 & 0.8222222222222222\\
0.4091991363691613 & 0.4807692307692308 & 0.5\\
0.5495936876730595 & 0.49019607843137253 & 0.3333333333333333\\
0.027559113243068367 & 0.01639344262295082 & 0\\
0.7535131086748066 & 0.6153846153846154 & 0.75\\
0.5381433132192782 & 0.58 & 0.25\\
0.32973171649909216 & 0.3076923076923077 & 0.4\\
0.7884287034284043 & 0.8181818181818182 & 0.6428571428571429\\
0.303194829291645 & 0.18181818181818182 & 0\\
0.4534978894806515 & 0.3953488372093023 & 0.4\\
0.13404169724716475 & 0.1724137931034483 & 0\\
0.40311298644712923 & 0.423728813559322 & 0\\
0.20345524067614962 & 0.2807017543859649 & 0.25\\
0.2623133404418495 & 0.28 & 0.25\\
\end{tabular}
\end{table}

\subsection*{Agent, \(\epsilon\)-Greedy and Agent, Thompson Sampling, no sharing}
\label{sec:org9939a0e}
\subsection*{Agent, \(\epsilon\)-Greedy and Agent, Thompson Sampling, lying}
\label{sec:orgc6a5595}
\subsection*{Agent, Thompson Sampling and Agent, Thompson Sampling, no alternation}
\label{sec:org568333e}
\section*{Future Work}
\label{sec:org5bbce58}
Agent A can also observe the levers that Agent B pulls. If Agent B tells Agent A that a lever
does not give output but still continues to pull the lever, either Agent B is lying, or Agent B
is irrational, both cases in which Agent A should ignore the information given.
\end{document}