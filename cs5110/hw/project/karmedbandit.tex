% Created 2020-04-22 Wed 16:34
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../../preamble.tex} \DeclareMathOperator{\cube}{cube}
\author{Abraham White}
\date{\today}
\title{K-Armed Bandit Implementation}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={K-Armed Bandit Implementation},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Setup}
\label{sec:org4cb700e}
First we need to set up our python environment.
\begin{minted}[]{bash}
python3 -m venv .venv
\end{minted}
We activate the environment and install the required packages.
\begin{minted}[]{bash}
source ./.venv/bin/activate
pip install numpy docopt
\end{minted}

\section*{\emph{k}-Armed Bandit}
\label{sec:orgf4f2ce8}
We need to define the \emph{k}-armed bandit (a slot machine). Rather than having each of the arms
return a real value based on a probability distribution, we will implement a variant called the
Bernoulli multi-armed bandit. This variation has each arm \(i\) issue a reward of 1 with some
probability \(p_i\), and 0 with probability \(1-p_i\).
\begin{minted}[]{python}
def gen_bandit(rng, k: int) -> Tuple[List[float], Callable[[int], int]]:
    probs = rng.random((k,))
    return (probs, lambda i: 1 if rng.random() < probs[i] else 0)
\end{minted}

\section*{Agents}
\label{sec:org498cfdb}
An agent has a standard interface.
\begin{minted}[]{python}
class Agent:
    def __init__(self, k: int, bandit: Callable[[int], int], rng):
        self.k = k
        self.bandit = bandit
        self.score = 0
        self.time = 0
        self.npulls = [0]*k
        self.payouts = [0]*k
        self.rng = rng
        
    def get_lever(self) -> int:
        pass
    
    def update(self, i: int, r: int):
        self.time += 1
        self.npulls[i] += 1
        self.payouts[i] += r
        self.score += r
        return (i, r)
        
    def run(self):
        i = self.get_lever()
        r = self.bandit(i)
        return self.update(i, r)
\end{minted}

We have to account for the fact that a lever cannot be pulled twice per time-step, yet we
have two agents. We define a wrapping function that modifies an agent's state, removing the 
locked lever, and restores it after the turn.
\begin{minted}[]{python}
def exclusive_run(lever, agent):
    if lever == -1:
        return agent.run()
    oldk = agent.k
    oldnpull = agent.npulls[lever]
    oldpayout = agent.payouts[lever]
    agent.k -= 1
    del agent.npulls[lever]
    del agent.payouts[lever]
    i = agent.get_lever()
    if i >= lever:
        i += 1
    agent.k += 1
    agent.npulls.insert(lever, oldnpull)
    agent.payouts.insert(lever, oldpayout)
    r = agent.bandit(i)
    return agent.update(i, r)
\end{minted}

\subsection*{Algorithms}
\label{sec:org9ce67ba}
We start with algorithms developed for playing the Bernoulli multi-armed bandit problem with
a single agent. 
\subsubsection*{\(\epsilon\)-Greedy Algorithm}
\label{sec:orgca1e5e1}
According to the \(\epsilon\)-Greedy algorithm, the best lever should be selected with probability \(1-\epsilon\),
while a uniformly random lever is selected with probability \(\epsilon\). This allows for choosing a known good
lever most of the time, while also allowing for random exploration to find better levers.
\begin{minted}[]{python}
class EpsilonGreedy(Agent): 
    def __init__(self, k: int, bandit: Callable[[int], int], rng, eps: float):
        super().__init__(k, bandit, rng)
        self.eps = eps
    
    def get_lever(self):    
        return (self.rng.integers(0, self.k) if self.rng.random() < self.eps
                else max(range(self.k), key=lambda i: (0. if self.npulls[i] == 0
                                                       else self.payouts[i] / self.npulls[i])))
\end{minted}
\subsubsection*{Thompson Sampling}
\label{sec:org1d282f9}
For a Bernoulli bandit, we can assume that the expected payout for a specific lever 
follows a Beta distribution. Our parameters \(\alpha\) and \(\beta\) correspond to the number
of times a reward was granted or withheld respectively. For every possible lever \(i\),
we sample an expected reward from the probability distribution
\(\operatorname{Beta}(\alpha_i, \beta_i)\), and then select the best action among that set.
The Beta distribution is updated after the lever is pulled and the true value found.
\begin{minted}[]{python}
class ThompsonSampling(Agent):
    def get_lever(self):
        distribs = [self.rng.beta(alpha + 1, (npull - alpha) + 1)
                    for (npull, alpha) in zip(self.npulls, self.payouts)]
        return max(range(self.k), key=lambda i: distribs[i])
\end{minted}

\subsection*{Agent B}
\label{sec:orgbf21f79}
Agent B has a single goal - to maximize points received by pulling levers. However,
the agent can come in two variations: an adversarial agent, who tries to get the most points
and does not help other agents also trying to pull levers; and a cooperative agent, who
shares information with other agents in order to maximize the total points collected by both.
Agent B doesn't attempt any modeling of the other agent. To make the agent adversarial or cooperative,
we allow Agent B to share information with Agent A, or to lie, saying that the arm just pulled did
not return a value. Lying would cause Agent A to underestimate the probability of the arm, while
sharing truthfully would allow Agent A to gain information about the probability of each arm without
having to do exploration.

To do this, we add a configuration parameter for Agent B indicating its probability of lying.
Information is shared with Agent A through a \texttt{shareinfo} method on a class that handles updating
Agent A depending on the determination made about Agent B and Agent A's configured behavior. Agent A
can choose to accept or deny the information depending on how it is configured.
\begin{minted}[]{python}
def share(prob: float, rng, sharer, ib: int, rb: int):
    if rng.random() < prob:
        sharer.shareinfo(ib, 1-rb)
    else:
        sharer.shareinfo(ib, rb)
\end{minted}
In this code block, \texttt{ib} is the index of the lever just pulled by Agent B, \texttt{rb} is the
result of pulling the lever, and \texttt{prob} is the probability of Agent B lying about
the result.
\subsection*{Agent A}
\label{sec:orgc255650}
We design a Sharer class that handles providing information to Agent A when shared by
Agent B. Agent A can make a determination on whether Agent B is telling the truth about
provided information by how correlated the estimated probabilites observed by Agent A
are with the estimated probabilties as shared by Agent B. If Agent B is lying or is
irrational, there will be a high negative correlation, and if Agent B is telling the 
truth there will be a high positive correlation.
\begin{minted}[]{python}
def get_est(npulls: List[int], payouts: List[int]) -> List[float]:
    return [(0. if npull == 0 else payout / npull)
            for (npull, payout) in zip(npulls, payouts)]
class Sharer:
    def __init__(self, agenta, mode, corr_thresh):
        self.agent = agenta
        self.mode = mode
        self.corr = 0
        self.thresh = corr_thresh
        self.npulls = [0]*self.agent.k
        self.payouts = [0]*self.agent.k
        self.accepted_npulls = [0]*self.agent.k
        self.accepted_payouts = [0]*self.agent.k
        
    def update_corr(self):
        npulls_a = [a - b for a,b in zip(self.agent.npulls, self.accepted_npulls)]
        payouts_a = [a - b for a,b in zip(self.agent.payouts, self.accepted_payouts)]
        npulls_b = self.npulls
        payouts_b = self.payouts
        est_b = get_est(npulls_b, payouts_b)
        est_a = get_est(npulls_a, payouts_a)
        m = corrcoef(est_a, est_b)
        self.corr = m[0, 1]
        
    def accept(self, idx, result):
        self.accepted_npulls[idx] += 1
        self.accepted_payouts[idx] += result
        self.agent.npulls[idx] += 1
        self.agent.payouts[idx] += result
        
    def update_agent(self, idx, result):
        if self.mode == 'accept':
            self.accept(idx, result)
        elif self.mode == 'threshold':
            if self.corr >= self.thresh:
                self.accept(idx, result)
        
    def shareinfo(self, idx, result):
        self.npulls[idx] += 1
        self.payouts[idx] += result
        self.update_corr()
        self.update_agent(idx, result)
\end{minted}
\section*{Driver Code}
\label{sec:orgbe03061}
We start with program configuration through command-line arguments.
\begin{minted}[]{python}
"""K-Armed Bandit.

Usage:
  bandit.py [options]

Options:
  -h --help    Show this text
  -i N         Set number of iterations [default: 1000]
  -k K         Set number of arms [default: 10]
  -s SEED      Set the random seed
  -l P         Probability of B lying. Float between [0,1], or -1 to not share [default: -1]
  --eps-a=EPS  Epsilon when Agent A uses the epsilon-greedy algorithm [default: 0.1]
  --eps-b=EPS  Epsilon when Agent B uses the epsilon-greedy algorithm [default: 0.1]
  -a STRATA    Base strategy for Agent A. One of 'none', 'thompson', 'greedy' [default: thompson]
  -b STRATB    Base strategy for Agent B. One of 'thompson', 'greedy' [default: thompson]
  -m MODE      Analysis for Agent A. One of 'threshold', 'accept', 'deny' [default: accept]
  -t THRESH    Threshold for accepting information from Agent B [default: 0.6]
  --a-starts   Agent A starts the first round
  --no-alter   Don't alternate order of play
"""
from docopt import docopt
\end{minted}

\begin{minted}[]{python}
if __name__ == '__main__':
    args = docopt(__doc__)
    args['--eps-a'] = float(args['--eps-a'])
    args['--eps-b'] = float(args['--eps-b'])
    if args['-s'] is not None:
      args['-s'] = int(args['-s'])
    args['-k'] = int(args['-k'])
    args['-i'] = int(args['-i'])
    args['-l'] = float(args['-l'])
    args['-t'] = float(args['-t'])
    if ((args['-a'] not in ['none', 'thompson', 'greedy']) or
            (args['-b'] not in ['thompson', 'greedy']) or
            (args['-m'] not in ['threshold', 'accept', 'deny']) or
            (args['-l'] > 1 or (args['-l'] < 0 and args['-l'] != -1))): 
        print(__doc__)
        sys.exit(1)
    main(args)
\end{minted}

Next we initialize the agents and the bandit. We create a new random number generator for
each agent using the same seed. This ensures that if we change operational mode of one 
agent while keeping the same seed, the other agent will continue to act the same.
\begin{minted}[]{python}
def main(args):
    rng = default_rng(args['-s'])
    rng_a = default_rng(args['-s'])
    rng_b = default_rng(args['-s'])
    k = args['-k']
    probs, bandit = gen_bandit(rng, k)
    best_prob = max(probs)
    
    agenta = None
    if args['-a'] == 'thompson':
        agenta = ThompsonSampling(k, bandit, rng_a)
    elif args['-a'] == 'greedy':
        agenta = EpsilonGreedy(k, bandit, rng_a, args['--eps-a'])
        
    sharer = None
    if agenta:
        sharer = Sharer(agenta, args['-m'], args['-t'])
        
    agentb = None
    if args['-b'] == 'thompson':
        agentb = ThompsonSampling(k, bandit, rng_b)
    elif args['-b'] == 'greedy':
        agentb = EpsilonGreedy(k, bandit, rng_b, args['--eps-b'])
\end{minted}

Next, we run the simulation. We want to keep track of each agent's cumulative regret,
estimates of the bandit probabilities, and current score for each time tick.

Cumulative Regret is defined as \[ n \mu^{*} - \sum_{t=1}^n R_t \] where \(n\) is the number of iterations,
\(\mu^{*}\) is the maximal probability in the bandit, and \(R_t\) is the probability of the arm pulled at time \(t\).

We start with an agent going first, and then alternate each round. So if agent A starts the first round,
then agent B will start the next round, and agent A starts the round after that. If the 
strategy for agent A is set to \texttt{none}, we treat this as a single player K-Armed bandit simulation,
only running agent B.

\begin{minted}[]{python}
    regrets_a = []
    estimates_a = []
    score_a = []
    shared_corr = []
    regrets_b = []
    estimates_b = []
    score_b = []
    iter = 0
    afirst = args['--a-starts']
    while iter < args['-i']:
        iter += 1
        (fst, snd) = (agenta, agentb) if afirst else (agentb, agenta)
        if agenta:
            (i_fst, r_fst) = exclusive_run(-1, fst)
            (i_snd, r_snd) = exclusive_run(i_fst, snd)
            (ia, ra, ib, rb) = (i_fst, r_fst, i_snd, r_snd) if afirst else (i_snd, r_snd, i_fst, r_fst)
            
            if args['-l'] != -1:
              share(args['-l'], rng, sharer, ib, rb)
            
            regrets_a.append(best_prob - probs[ia])
            regrets_b.append(best_prob - probs[ib])
            estimates_a.append(get_est(agenta.npulls, agenta.payouts))
            estimates_b.append(get_est(agentb.npulls, agentb.payouts))
            score_a.append(agenta.score)
            score_b.append(agentb.score)
            shared_corr.append(sharer.corr)
        else:
            i, r = agentb.run()
            regrets_b.append(best_prob - probs[i])
            estimates_b.append(get_est(agentb.npulls, agentb.payouts))
            score_b.append(agentb.score)
        if not args['--no-alter']:
          afirst = not afirst
\end{minted}

Finally, we output the results as json on the standard output stream.
\begin{minted}[]{python}
    regret_a = cumsum(regrets_a)
    regret_b = cumsum(regrets_b)
    json.dump({'regret_a': regret_a.tolist(), 'estimates_a': estimates_a, 'scores_a': score_a,
               'regret_b': regret_b.tolist(), 'estimates_b': estimates_b, 'scores_b': score_b,
               'shared_corr': shared_corr, 'bandit': probs.tolist()}, sys.stdout)
\end{minted}
\section*{Running}
\label{sec:orgb42453a}
Run with \texttt{python3 bandit.py}
\section*{Appendix}
\label{sec:orgc3d5bc6}
\subsection*{Full Source}
\label{sec:orgc66c5cd}
See \texttt{bandit.py}.
\end{document}