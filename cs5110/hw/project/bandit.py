"""K-Armed Bandit.

Usage:
  bandit.py [options]

Options:
  -h --help    Show this text
  -i N         Set number of iterations [default: 1000]
  -k K         Set number of arms [default: 10]
  -s SEED      Set the random seed
  -l P         Probability of B lying. Float between [0,1], or -1 to not share [default: -1]
  --eps-a=EPS  Epsilon when Agent A uses the epsilon-greedy algorithm [default: 0.1]
  --eps-b=EPS  Epsilon when Agent B uses the epsilon-greedy algorithm [default: 0.1]
  -a STRATA    Base strategy for Agent A. One of 'none', 'thompson', 'greedy' [default: thompson]
  -b STRATB    Base strategy for Agent B. One of 'thompson', 'greedy' [default: thompson]
  -m MODE      Analysis for Agent A. One of 'threshold', 'accept', 'deny' [default: accept]
  -t THRESH    Threshold for accepting information from Agent B [default: 0.6]
  --a-starts   Agent A starts the first round
  --no-alter   Don't alternate order of play
"""
from docopt import docopt
from numpy.random import default_rng
from numpy import cumsum, corrcoef
from typing import List, Callable, Tuple
import sys
import json

def gen_bandit(rng, k: int) -> Tuple[List[float], Callable[[int], int]]:
    probs = rng.random((k,))
    return (probs, lambda i: 1 if rng.random() < probs[i] else 0)

class Agent:
    def __init__(self, k: int, bandit: Callable[[int], int], rng):
        self.k = k
        self.bandit = bandit
        self.score = 0
        self.time = 0
        self.npulls = [0]*k
        self.payouts = [0]*k
        self.rng = rng
        
    def get_lever(self) -> int:
        pass
    
    def update(self, i: int, r: int):
        self.time += 1
        self.npulls[i] += 1
        self.payouts[i] += r
        self.score += r
        return (i, r)
        
    def run(self):
        i = self.get_lever()
        r = self.bandit(i)
        return self.update(i, r)

class EpsilonGreedy(Agent): 
    def __init__(self, k: int, bandit: Callable[[int], int], rng, eps: float):
        super().__init__(k, bandit, rng)
        self.eps = eps
    
    def get_lever(self):    
        return (self.rng.integers(0, self.k) if self.rng.random() < self.eps
                else max(range(self.k), key=lambda i: (0. if self.npulls[i] == 0
                                                       else self.payouts[i] / self.npulls[i])))

class ThompsonSampling(Agent):
    def get_lever(self):
        distribs = [self.rng.beta(alpha + 1, (npull - alpha) + 1)
                    for (npull, alpha) in zip(self.npulls, self.payouts)]
        return max(range(self.k), key=lambda i: distribs[i])

def exclusive_run(lever, agent):
    if lever == -1:
        return agent.run()
    oldk = agent.k
    oldnpull = agent.npulls[lever]
    oldpayout = agent.payouts[lever]
    agent.k -= 1
    del agent.npulls[lever]
    del agent.payouts[lever]
    i = agent.get_lever()
    if i >= lever:
        i += 1
    agent.k += 1
    agent.npulls.insert(lever, oldnpull)
    agent.payouts.insert(lever, oldpayout)
    r = agent.bandit(i)
    return agent.update(i, r)

def get_est(npulls: List[int], payouts: List[int]) -> List[float]:
    return [(0. if npull == 0 else payout / npull)
            for (npull, payout) in zip(npulls, payouts)]
class Sharer:
    def __init__(self, agenta, mode, corr_thresh):
        self.agent = agenta
        self.mode = mode
        self.corr = 0
        self.thresh = corr_thresh
        self.npulls = [0]*self.agent.k
        self.payouts = [0]*self.agent.k
        self.accepted_npulls = [0]*self.agent.k
        self.accepted_payouts = [0]*self.agent.k
        
    def update_corr(self):
        npulls_a = [a - b for a,b in zip(self.agent.npulls, self.accepted_npulls)]
        payouts_a = [a - b for a,b in zip(self.agent.payouts, self.accepted_payouts)]
        npulls_b = self.npulls
        payouts_b = self.payouts
        est_b = get_est(npulls_b, payouts_b)
        est_a = get_est(npulls_a, payouts_a)
        m = corrcoef(est_a, est_b)
        self.corr = m[0, 1]
        
    def accept(self, idx, result):
        self.accepted_npulls[idx] += 1
        self.accepted_payouts[idx] += result
        self.agent.npulls[idx] += 1
        self.agent.payouts[idx] += result
        
    def update_agent(self, idx, result):
        if self.mode == 'accept':
            self.accept(idx, result)
        elif self.mode == 'threshold':
            if self.corr >= self.thresh:
                self.accept(idx, result)
        
    def shareinfo(self, idx, result):
        self.npulls[idx] += 1
        self.payouts[idx] += result
        self.update_corr()
        self.update_agent(idx, result)

def share(prob: float, rng, sharer, ib: int, rb: int):
    if rng.random() < prob:
        sharer.shareinfo(ib, 1-rb)
    else:
        sharer.shareinfo(ib, rb)

def main(args):
    rng = default_rng(args['-s'])
    rng_a = default_rng(args['-s'])
    rng_b = default_rng(args['-s'])
    k = args['-k']
    probs, bandit = gen_bandit(rng, k)
    best_prob = max(probs)
    
    agenta = None
    if args['-a'] == 'thompson':
        agenta = ThompsonSampling(k, bandit, rng_a)
    elif args['-a'] == 'greedy':
        agenta = EpsilonGreedy(k, bandit, rng_a, args['--eps-a'])
        
    sharer = None
    if agenta:
        sharer = Sharer(agenta, args['-m'], args['-t'])
        
    agentb = None
    if args['-b'] == 'thompson':
        agentb = ThompsonSampling(k, bandit, rng_b)
    elif args['-b'] == 'greedy':
        agentb = EpsilonGreedy(k, bandit, rng_b, args['--eps-b'])
    regrets_a = []
    estimates_a = []
    score_a = []
    shared_corr = []
    regrets_b = []
    estimates_b = []
    score_b = []
    iter = 0
    afirst = args['--a-starts']
    while iter < args['-i']:
        iter += 1
        (fst, snd) = (agenta, agentb) if afirst else (agentb, agenta)
        if agenta:
            (i_fst, r_fst) = exclusive_run(-1, fst)
            (i_snd, r_snd) = exclusive_run(i_fst, snd)
            (ia, ra, ib, rb) = (i_fst, r_fst, i_snd, r_snd) if afirst else (i_snd, r_snd, i_fst, r_fst)
            
            if args['-l'] != -1:
              share(args['-l'], rng, sharer, ib, rb)
            
            regrets_a.append(best_prob - probs[ia])
            regrets_b.append(best_prob - probs[ib])
            estimates_a.append(get_est(agenta.npulls, agenta.payouts))
            estimates_b.append(get_est(agentb.npulls, agentb.payouts))
            score_a.append(agenta.score)
            score_b.append(agentb.score)
            shared_corr.append(sharer.corr)
        else:
            i, r = agentb.run()
            regrets_b.append(best_prob - probs[i])
            estimates_b.append(get_est(agentb.npulls, agentb.payouts))
            score_b.append(agentb.score)
        if not args['--no-alter']:
          afirst = not afirst
    regret_a = cumsum(regrets_a)
    regret_b = cumsum(regrets_b)
    json.dump({'regret_a': regret_a.tolist(), 'estimates_a': estimates_a, 'scores_a': score_a,
               'regret_b': regret_b.tolist(), 'estimates_b': estimates_b, 'scores_b': score_b,
               'shared_corr': shared_corr, 'bandit': probs.tolist()}, sys.stdout)

if __name__ == '__main__':
    args = docopt(__doc__)
    args['--eps-a'] = float(args['--eps-a'])
    args['--eps-b'] = float(args['--eps-b'])
    if args['-s'] is not None:
      args['-s'] = int(args['-s'])
    args['-k'] = int(args['-k'])
    args['-i'] = int(args['-i'])
    args['-l'] = float(args['-l'])
    args['-t'] = float(args['-t'])
    if ((args['-a'] not in ['none', 'thompson', 'greedy']) or
            (args['-b'] not in ['thompson', 'greedy']) or
            (args['-m'] not in ['threshold', 'accept', 'deny']) or
            (args['-l'] > 1 or (args['-l'] < 0 and args['-l'] != -1))): 
        print(__doc__)
        sys.exit(1)
    main(args)
