import math
from models import Color, Dir
import pygame.locals as P
from typing import Literal, Optional, Union, Tuple

FPS = 20
WINDOWWIDTH = 500
WINDOWHEIGHT = 500
CELLSIZE = 20
RADIUS = math.floor(CELLSIZE/2.5)
assert WINDOWWIDTH % CELLSIZE == 0, "Window width must be a multiple of cell size."
assert WINDOWHEIGHT % CELLSIZE == 0, "Window height must be a multiple of cell size."
CELLWIDTH = int(WINDOWWIDTH / CELLSIZE)
CELLHEIGHT = int(WINDOWHEIGHT / CELLSIZE)

#             R    G    B
WHITE     = (255, 255, 255)
BLACK     = (  0,   0,   0)
RED       = (255,   0,   0)
GREEN     = (  0, 255,   0)
DARKGREEN = (  0, 155,   0)
DARKGRAY  = ( 40,  40,  40)
GRAY      = ( 80,  80,  80)
YELLOW    = (255, 255,   0)
BLUE      = (0,     0, 255)
BROWN     = (165, 103,  41)
BGCOLOR = WHITE

HEAD = 0 # syntactic sugar: index of the worm's head
# titles: (text, foreground color, background color, initial degress, increment degrees)
TITLES = [('Roomba', WHITE, DARKGREEN, 0, 3)]

num_cells = CELLWIDTH * CELLHEIGHT
print(num_cells)

# (minimum, maximum) or constant
# if set to a range, uses rand.randint(min, max) to pick a value
# uniformly distributed between min and max while generating the initial state
STATIC_OBS_SIZE: Union[int, Tuple[int, int]] = (10, 30)
NUM_STATIC_OBS: Union[int, Tuple[int, int]] = (4, 8)
NUM_MOVING_OBS: Union[int, Tuple[int, int]] = (2, 4)
DIRT_CAPACITY: Union[int, Tuple[int, int]] = (1, 10)
NUM_DIRT: Union[int, Tuple[int, int]] = (30, 50)
NUM_ROOMBA: Union[int, Tuple[int, int]] = 4
NUM_CHARGER: Union[int, Tuple[int, int]] = 3
# should be a percentage based on room size
ROOMBA_CHARGE: Union[int, Tuple[int, int]] = int(num_cells / NUM_CHARGER)
MOVING_WAIT_CHANCE = 0.7
RAND_SEED: Optional[int] = 2

ROOMBA_COLOR: Color = (GRAY, GREEN)
CHARGER_COLOR: Color = (GREEN, GREEN)
MOVING_OBS_COLOR: Color = (BLUE, GRAY)
OBS_COLOR = (BLACK, GRAY)
DIRT_COLOR = (BLACK, BROWN)
