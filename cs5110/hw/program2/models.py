from typing import Tuple, List
from enum import Enum, auto
from dataclasses import dataclass

class Dir(Enum):
    UP = 'up'
    DOWN = 'down'
    LEFT = 'left'
    RIGHT = 'right'

class Behavior(Enum):
    RETURN_TO_CHARGE = auto()
    CHARGE = auto()
    FIND_DIRT = auto()
    VACUUM_DIRT = auto()

RGB = Tuple[int, int, int]
Color = Tuple[RGB, RGB]
Pos = Tuple[int, int]

@dataclass
class WorldObject():
    pos: Pos

@dataclass
class Roomba(WorldObject):
    charge: int
    behavior: Behavior
    full_charge: int

@dataclass
class Dirt(WorldObject):
    quantity: int

@dataclass
class Obstacle(WorldObject):
    pass

@dataclass
class WorldEdge(WorldObject):
    pass

@dataclass
class MObstacle(WorldObject):
    pass

@dataclass
class Charger(WorldObject):
    pass

@dataclass
class Empty(WorldObject):
    pass

@dataclass
class State():
    roombas: List[Roomba]
    dirt: List[Dirt]
    obs: List[Obstacle]
    moving_obs: List[MObstacle]
    charger: List[Charger]
