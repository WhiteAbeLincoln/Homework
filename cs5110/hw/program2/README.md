# Roomba
## Setup
1. Install a virtual environment with `python3 -m venv .venv`
2. Enter the virtual environment with `source .venv/bin/activate`
3. Install pygame with `pip install pygame`

This program requires python 3.8 (because I'm using dataclasses)

## Running
1. Source the virtual environment with `source .venv/bin/activate`
2. Start the game with `./roomba.py` or `python3 roomba.py`

This code has not been tested on Windows, and the I cannot provide any guarantee that it will work
as expected. Please use a Unix environment if possible.

You can change program parameters in the constants.py file
