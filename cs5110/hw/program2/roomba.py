#!/usr/bin/env python3
import random
from rules import obstacle_react, roomba_react
import pygame
import sys
import math
from typing import List, cast
import pygame.locals as P
import state as S
from constants import *
from models import Color, Pos, WorldObject
from state import adjacent_loc, find_by_pos, get_adjacent_wobj, get_world_objs, remove_overlapping
from random import seed

seed(RAND_SEED)

def main():
    global FPSCLOCK, DISPLAYSURF, BASICFONT, STEPPER, DOSTEP

    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    BASICFONT = pygame.font.Font('freesansbold.ttf', 18)
    STEPPER = False
    DOSTEP = False
    pygame.display.set_caption('Roomba')

    showStartScreen()
    while True:
        runGame()
        showGameOverScreen()


def runGame():
    global STEPPER, DOSTEP
    state = S.init_state()

    # Steps of the game loop
    # 1. apply inputs
    # 2. update projectiles and check if they hit
    # 3. Remove invalid worms (that have hit obstacles)
    # 4. Grow worms that are on apples
    # 5. Move worms and projectiles based on direction

    while True:  # main game loop
        for event in pygame.event.get():  # event handling loop
            if event.type == P.QUIT:
                terminate()
            elif event.type == P.KEYDOWN:
                if event.key == P.K_ESCAPE:
                    terminate()

                if event.key == P.K_q:
                    STEPPER = not STEPPER
                if STEPPER and event.key == P.K_z:
                    DOSTEP = True

        if DOSTEP or not STEPPER:
            DISPLAYSURF.fill(BGCOLOR)
            draw_grid()

            world_objs = get_world_objs(state)

            for o in state.moving_obs:
                adj = get_adjacent_wobj(o, world_objs)
                obstacle_react(o, adj)

            for o in state.roombas:
                adj = get_adjacent_wobj(o, world_objs)
                print('before', o)
                roomba_react(o, state.charger, remove_overlapping(find_by_pos(o.pos, world_objs), False), adj)
                if any(o.pos == c.pos for c in state.charger):
                    o.charge += 1
                elif o.charge > 0:
                    o.charge -= 1
                print('after', o)

            if all(r.charge <= 0 for r in state.roombas):
                return

            state.dirt = [o for o in state.dirt if o.quantity > 0]

            draw_boxes(cast(List[WorldObject], state.dirt), DIRT_COLOR)
            draw_boxes(cast(List[WorldObject], state.charger), CHARGER_COLOR)
            draw_boxes(cast(List[WorldObject], state.obs), OBS_COLOR)
            draw_boxes(cast(List[WorldObject], state.moving_obs), MOVING_OBS_COLOR)
            draw_boxes(cast(List[WorldObject], state.roombas), ROOMBA_COLOR)

            DOSTEP = False

        pygame.display.update()
        FPSCLOCK.tick(FPS)


def drawPressKeyMsg():
    pressKeySurf = BASICFONT.render('Press a key to play.', True, YELLOW)
    pressKeyRect = pressKeySurf.get_rect()
    pressKeyRect.topleft = (WINDOWWIDTH - 200, WINDOWHEIGHT - 30)
    DISPLAYSURF.blit(pressKeySurf, pressKeyRect)


def checkForKeyPress():
    if len(pygame.event.get(P.QUIT)) > 0:
        terminate()

    keyUpEvents = pygame.event.get(P.KEYUP)
    if len(keyUpEvents) == 0:
        return None
    if keyUpEvents[0].key == P.K_ESCAPE:
        terminate()
    return keyUpEvents[0].key


def showStartScreen():
    titleFont = pygame.font.Font('freesansbold.ttf', 100)
    titleSurfaces = [(titleFont.render(t, True, fg, bg), d, i)
                     for (t, fg, bg, d, i) in TITLES]

    while True:
        DISPLAYSURF.fill(BGCOLOR)

        for idx, (surface, deg, incr) in enumerate(titleSurfaces):
            rotatedSurf = pygame.transform.rotate(surface, deg)
            rotatedRect = rotatedSurf.get_rect()
            rotatedRect.center = (math.floor(
                WINDOWWIDTH / 2), math.floor(WINDOWHEIGHT / 2))
            DISPLAYSURF.blit(rotatedSurf, rotatedRect)
            # increase rotation each frame
            titleSurfaces[idx] = (surface, deg + incr, incr)

        drawPressKeyMsg()

        if checkForKeyPress():
            pygame.event.get()  # clear event queue
            return
        pygame.display.update()
        FPSCLOCK.tick(FPS)


def terminate():
    pygame.quit()
    sys.exit()


def showGameOverScreen():
    gameOverFont = pygame.font.Font('freesansbold.ttf', 150)
    gameSurf = gameOverFont.render('Game', True, WHITE)
    overSurf = gameOverFont.render('Over', True, WHITE)
    gameRect = gameSurf.get_rect()
    overRect = overSurf.get_rect()
    gameRect.midtop = (math.floor(WINDOWWIDTH / 2), 10)
    overRect.midtop = (math.floor(WINDOWWIDTH / 2), gameRect.height + 10 + 25)

    DISPLAYSURF.blit(gameSurf, gameRect)
    DISPLAYSURF.blit(overSurf, overRect)
    drawPressKeyMsg()
    pygame.display.update()
    pygame.time.wait(500)
    checkForKeyPress()  # clear out any key presses in the event queue

    while True:
        if checkForKeyPress():
            pygame.event.get()  # clear event queue
            return


def draw_score(score, ypos):
    if ypos is None:
        ypos = 10
    # don't display players who haven't joined yet
    if score == 0:
        return ypos
    scoreSurf = BASICFONT.render('Score: %s' % (score), True, WHITE)
    scoreRect = scoreSurf.get_rect()
    scoreRect.topleft = (WINDOWWIDTH - 120, ypos)
    DISPLAYSURF.blit(scoreSurf, scoreRect)
    return ypos + 20


def draw_box(coord: WorldObject, color: Color):
    x = coord.pos[0] * CELLSIZE
    y = coord.pos[1] * CELLSIZE
    wormSegmentRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
    pygame.draw.rect(DISPLAYSURF, color[0], wormSegmentRect)
    wormInnerSegmentRect = pygame.Rect(
        x + 4, y + 4, CELLSIZE - 8, CELLSIZE - 8)
    pygame.draw.rect(DISPLAYSURF, color[1], wormInnerSegmentRect)


def draw_boxes(coords: List[WorldObject], color: Color):
    for c in coords:
        draw_box(c, color)


def draw_grid():
    for x in range(0, WINDOWWIDTH, CELLSIZE):  # draw vertical lines
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (x, 0), (x, WINDOWHEIGHT))
    for y in range(0, WINDOWHEIGHT, CELLSIZE):  # draw horizontal lines
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (0, y), (WINDOWWIDTH, y))


if __name__ == '__main__':
    main()
