import random
from constants import *
import pygame.locals as P
from typing import Tuple, TypedDict, List, Union, cast
from models import *


def get_val(conf: Union[int, Tuple[int, int]]) -> int:
    return random.randint(conf[0], conf[1]) if not isinstance(conf, int) else conf


def get_random_loc() -> Pos:
    return (random.randint(0, CELLWIDTH - 1), random.randint(0, CELLHEIGHT - 1))


def get_world_objs(state: State) -> List[WorldObject]:
    return (state.roombas  # type: ignore
            + state.dirt
            + state.obs
            + state.moving_obs
            + state.charger)


def all_occupied_cells(state: State) -> List[Pos]:
    return [r.pos for r in get_world_objs(state)]


def get_rand_unoccupied_loc(occupied: List[Pos]) -> Pos:
    """
    Generates a random location that's not already occupied
    Takes longer the more cells are occupied, and will loop infinitely if all are occupied
    """
    loc = get_random_loc()
    while loc in occupied:
        loc = get_random_loc()

    return loc

# generates a list of items using the given function, ensuring
# that each item is unique
# WARNING: will loop infinitely if genfn returns a constant value


def uniqueList(genfn, n, arr=None):
    if arr is None:
        arr = []
    l = list(arr)
    for _ in range(0, n):
        r = genfn()
        while r in l:
            r = genfn()
        l.append(r)

    return l

def is_occupiable(adj: WorldObject):
    # roombas can only overlap with empty cells, chargers, or dirt
    return isinstance(adj, Empty) or isinstance(adj, Charger) or isinstance(adj, Dirt)

def is_overlapping(m: WorldObject):
    return isinstance(m, MObstacle) or isinstance(m, Roomba)


def out_of_bounds(head: Pos) -> bool:
    return head[0] <= -1 or head[0] >= CELLWIDTH or head[1] <= -1 or head[1] >= CELLHEIGHT


def adjacent_loc(p: Pos) -> Tuple[Pos, Pos, Pos, Pos]:
    return ((p[0] + 1, p[1]), (p[0] - 1, p[1]), (p[0], p[1] + 1), (p[0], p[1] - 1))


def get_open_adj(p: Pos, occupied: List[Pos]) -> List[Pos]:
    return [l for l in adjacent_loc(p) if not (l in occupied or out_of_bounds(l))]


def find_by_pos(p: Pos, wo: List[WorldObject]) -> List[WorldObject]:
    l = [obj for obj in wo if obj.pos == p]
    return l if l else [cast(WorldObject, Empty(p))]

def remove_overlapping(wo: List[WorldObject], top = True) -> WorldObject:
    if len(wo) == 1:
        return wo[0]

    fst = wo[0]
    snd = wo[1]

    # there should be a way to reduce this, but i'm too tired
    if top:
        if is_overlapping(fst):
            return fst
        if is_overlapping(snd):
            return snd
    else:
        if not is_overlapping(fst):
            return fst
        if not is_overlapping(snd):
            return snd

    return fst


def get_adjacent_wobj(o: WorldObject, world_objs: List[WorldObject]) -> Tuple[WorldObject, WorldObject, WorldObject, WorldObject]:
    p = o.pos

    return cast(Tuple[WorldObject, WorldObject, WorldObject, WorldObject],
                tuple(WorldEdge(l) if out_of_bounds(l) else remove_overlapping(find_by_pos(l, world_objs))
                      for l in adjacent_loc(p)))


def obstacle_gen(occupied: List[Pos], size: int = 10) -> List[Pos]:
    body = [get_rand_unoccupied_loc(occupied)]

    open_adj: List[Pos] = []
    for b in body:
        open_adj += get_open_adj(b, body + occupied)

    for _ in range(size):
        new_loc = random.choice(open_adj)
        body.append(new_loc)
        open_adj.remove(new_loc)
        # calculate the newly open positions
        open_adj += get_open_adj(new_loc, body + occupied)

    return body


def init_state() -> State:
    occupied: List[Pos] = []
    obstacles: List[Obstacle] = []
    moving_obstacles: List[MObstacle] = []
    dirt: List[Dirt] = []
    chargers: List[Charger] = []

    # first generate obstacles
    for _ in range(get_val(NUM_STATIC_OBS)):
        o = obstacle_gen(occupied, get_val(STATIC_OBS_SIZE))
        occupied += o
        obstacles += [Obstacle(p) for p in o]

    # next generate the dirt
    for _ in range(get_val(NUM_DIRT)):
        o = get_rand_unoccupied_loc(occupied)
        occupied.append(o)
        dirt.append(Dirt(o, get_val(DIRT_CAPACITY)))

    # next generate the initial positions for the moving obstacles
    for _ in range(get_val(NUM_MOVING_OBS)):
        o = get_rand_unoccupied_loc(occupied)
        occupied.append(o)
        moving_obstacles.append(MObstacle(o))

    # finally generate the charger and the roombas

    num_roomba = get_val(NUM_ROOMBA)

    roombas: List[Roomba] = []
    for _ in range(get_val(NUM_CHARGER)):
        pos = get_rand_unoccupied_loc(occupied)
        occupied.append(pos)
        chargers.append(Charger(pos))
        if len(roombas) < num_roomba:
            charge = get_val(ROOMBA_CHARGE)
            roombas.append(Roomba(pos, charge, Behavior.FIND_DIRT, charge))

    remaining = num_roomba - len(roombas)
    for _ in range(remaining):
        pos = get_rand_unoccupied_loc(occupied)
        occupied.append(pos)
        charge = get_val(ROOMBA_CHARGE)
        roombas.append(Roomba(pos, charge, Behavior.FIND_DIRT, charge))

    return State(roombas, dirt, obstacles, moving_obstacles, chargers)


def apply_dir(direction: Dir, pos: Pos) -> Pos:
    if direction == Dir.UP:
        return (pos[0], pos[1] - 1)
    elif direction == Dir.DOWN:
        return (pos[0], pos[1] + 1)
    elif direction == Dir.LEFT:
        return (pos[0] - 1, pos[1])
    elif direction == Dir.RIGHT:
        return (pos[0] + 1, pos[1])
    return pos
