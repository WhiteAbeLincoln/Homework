from math import sqrt
from constants import MOVING_WAIT_CHANCE
from models import Behavior, Charger, Dir, Dirt, Empty, MObstacle, Pos, Roomba, WorldObject
from typing import Optional, Tuple, List
from random import choice, random
from state import is_occupiable

# A roomba knows their charge, position, charger position, and the
# contents of the 4 adjacent cells and the current dirt cell, if any
# the roomba has no memory (reactive only), so we can't create a good rule
# for finding dirt like to not go back to a previous cell

def manhattan_dist(p1: Pos, p2: Pos):
    return abs(p2[0] - p1[0]) + abs(p2[1] - p1[1])

def obstacle_react(obstacle: MObstacle, adj: Tuple[WorldObject, WorldObject, WorldObject, WorldObject]):
    if random() <= MOVING_WAIT_CHANCE:
        return
    # move to a random open cell
    open_cells = [d for d in adj if is_occupiable(d)]
    if not open_cells:
        return
    obstacle.pos = choice(open_cells).pos

def roomba_react(roomba: Roomba, chargers: List[Charger], occupied: WorldObject, adj: Tuple[WorldObject, WorldObject, WorldObject, WorldObject]):
    # if the roomba is dead don't do anything
    if roomba.charge <= 0:
        return

    shortest_dist_to_charger = min(manhattan_dist(roomba.pos, c.pos) for c in chargers)
    # return if the roomba's charge below the optimal path to the charger,
    # i.e. there would be no way to make it back
    if roomba.charge < shortest_dist_to_charger + 3:
        roomba.behavior = Behavior.RETURN_TO_CHARGE

    # if we are vacuuming dirt, stay on the cell until dirt quantity is 0
    # handles decrementing the dirt quantity
    # consumes the roomba's action for this turn
    if roomba.behavior == Behavior.VACUUM_DIRT:
        if isinstance(occupied, Dirt):
            print('dirt quantity', occupied.quantity)
            if occupied.quantity > 0:
                occupied.quantity -= 1
                return
        else:
            roomba.behavior = Behavior.FIND_DIRT

    if roomba.behavior == Behavior.RETURN_TO_CHARGE:
        copy=chargers.copy()
        copy.sort(key=lambda c: manhattan_dist(roomba.pos, c.pos))
        closest_charger = copy[0]
        if isinstance(occupied, Charger):
            roomba.behavior = Behavior.CHARGE
        else:
            open_cells = [d for d in adj if is_occupiable(d)]
            # out of the empty cells, we pick the one that has the shortest distance to
            # the charger location and move to it
            open_cells.sort(key = lambda c: manhattan_dist(c.pos, closest_charger.pos))
            # if the roomba has no open cells then we wait
            # this will only happen if a moving obstacle moved into the roombas last position
            if not open_cells:
                return
            roomba.pos = open_cells[0].pos
            return

    if roomba.behavior == Behavior.CHARGE:
        if roomba.charge >= roomba.full_charge:
            roomba.behavior = Behavior.FIND_DIRT
        else:
            return

    if roomba.behavior == Behavior.FIND_DIRT:
        adj_dirt = [d for d in adj if isinstance(d, Dirt)]
        # if the roomba is finding dirt and there's a dirt adjacent, we move to that cell and change our behavior
        # uses an action
        if adj_dirt:
            roomba.pos = choice(adj_dirt).pos
            roomba.behavior = Behavior.VACUUM_DIRT
            return
        else:
            # pick an open cell and move to it
            # roombas can only overlap with empty cells, chargers, or dirt
            open_cells = [d for d in adj if is_occupiable(d)]
            if not open_cells:
                return
            roomba.pos = choice(open_cells).pos
            return
