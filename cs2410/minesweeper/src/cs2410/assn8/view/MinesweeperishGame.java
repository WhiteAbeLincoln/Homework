package cs2410.assn8.view;

import cs2410.assn8.controller.PrefKeys;

import javax.swing.*;
import java.awt.*;
import java.util.prefs.Preferences;

/**
 * Created by abe on 4/29/16.
 * Main Game Frame
 *
 * @author abe
 */
public class MinesweeperishGame extends JFrame {
    Preferences prefs;
    GridPanel cellContainer;
    ScorePanel scoreKeeper;

    public MinesweeperishGame() {
        // prefs = Preferences.systemRoot().node(PrefKeys.MAIN_NAME);

        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("Something erred while setting look and feel");
            e.printStackTrace();
        }

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container pane = this.getContentPane();

        pane.setPreferredSize((new Dimension(400, 500)));
        pane.setSize(50, 50);
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        scoreKeeper = new ScorePanel();
        cellContainer = new GridPanel(scoreKeeper);
        scoreKeeper.addStartListener(cellContainer);

        this.add(scoreKeeper);
        this.add(cellContainer);

        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(MinesweeperishGame::new);
    }
}
