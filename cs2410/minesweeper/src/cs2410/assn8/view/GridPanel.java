package cs2410.assn8.view;

import cs2410.assn8.controller.ICellContainer;
import cs2410.assn8.controller.IScoreKeeper;
import cs2410.assn8.controller.StartListener;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.prefs.Preferences;

import static java.lang.Math.ceil;
import static java.lang.Math.sqrt;

/**
 * Created by abe on 4/27/16.
 * Grid to hold {@link cs2410.assn8.view.Cell Cells}
 *
 * @see cs2410.assn8.view.Cell
 * @see cs2410.assn8.controller.ICellContainer
 * @author abe
 */
class GridPanel extends JPanel implements ICellContainer, StartListener {
    private Preferences prefs;
    private IScoreKeeper scorePanel;
    private List<Cell> cellList = new ArrayList<>();
    private int sideLength;
    private int numCells;
    private int numMines;
    private int prevFoundMines = 0;
    private int foundMines = 0;
    private int clearedCells = 0;
    private boolean firstCell = false;

    /**
     * Constructs a grid panel
     * @param scorePanel {@link cs2410.assn8.controller.IScoreKeeper ScoreKeeper} associated with the panel
     */
    GridPanel(IScoreKeeper scorePanel) {
        // this.prefs = Preferences.userRoot().node(PrefKeys.MAIN_NAME);
        // this.numCells = prefs.getInt(PrefKeys.NUMBER_CELLS, 576);
        // this.numMines = prefs.getInt(PrefKeys.NUMBER_MINES, 100);
        this.numCells = 576;
        this.numMines = 100;
        this.sideLength = (int)ceil(sqrt(numCells));
        this.scorePanel = scorePanel;

        this.setLayout(new GridLayout(sideLength, sideLength, 3, 3));
        this.setBackground(Color.GRAY);
        initGrid();

    }

    /**
     * Initializes a grid
     */
    private void initGrid() {
        for (int i = 0; i < numCells; i++) {
            Cell cell = new Cell(this);

            if (i < numMines) {
                cell.setMine(true);
                cellList.add(cell);
            } else {
                cellList.add(cell);
            }

            this.add(cell);
        }
        shuffle();
    }

    /**
     * Shuffles the grid
     */
    private void shuffle() {
        cellList.forEach(this::remove);
        Collections.shuffle(cellList);
        cellList.forEach(this::add);

        this.update(this.getGraphics());
        this.revalidate();
    }

    @Override
    public List<Cell> getAdjacent(Cell srcCell) {
        int currIdx = cellList.indexOf(srcCell);

        List<Cell> adjacent = new ArrayList<>();
        boolean hasLeft = true, hasRight = true, hasTop = true, hasBottom = true;

        if ((currIdx + 1) % sideLength == 0) {
            hasRight = false;
        }

        if (currIdx % sideLength == 0) {
            hasLeft = false;
        }

        if (currIdx < sideLength) {
            hasTop = false;
        }

        if (currIdx + sideLength >= cellList.size()) {
            hasBottom = false;
        }

        if (hasTop && hasLeft) {
            adjacent.add(cellList.get(currIdx - sideLength - 1));
        }

        if (hasTop) {
            adjacent.add(cellList.get(currIdx - sideLength));
        }

        if (hasTop && hasRight) {
            adjacent.add(cellList.get(currIdx - sideLength + 1));
        }

        if (hasRight) {
            adjacent.add(cellList.get(currIdx + 1));
        }

        if (hasBottom && hasRight) {
            adjacent.add(cellList.get(currIdx + sideLength + 1));
        }

        if (hasBottom) {
            adjacent.add(cellList.get(currIdx + sideLength));
        }

        if (hasBottom && hasLeft) {
            adjacent.add(cellList.get(currIdx + sideLength - 1));
        }

        if (hasLeft) {
            adjacent.add(cellList.get(currIdx - 1));
        }

        return adjacent;
    }

    /**
     * Resets state variables
     */
    private void resetVars() {
        firstCell = false;
        foundMines = 0;
        clearedCells = 0;
        prevFoundMines = 0;
    }

    /**
     * Checks if winning conditions have been met
     */
    private void checkIfWon() {
        System.out.printf("Found Mines: %s\nnumMines: %s\nclearedCells: %s\nnumCells-foundMines: %s\n", foundMines, numMines, clearedCells, numCells-foundMines);
        if (foundMines == numMines && numCells - foundMines == clearedCells) {
            endGame(true);
        }
    }

    @Override
    public List<Cell> getCells() {
        return cellList;
    }

    @Override
    public void cellClicked() {
        if (!firstCell) {
            System.out.println("First cell clicked");
            firstCell = true;
            scorePanel.startTimer();
        }
    }

    @Override
    public void endGame(boolean won) {
        if (won)
            cellList.get(0).showMines();
        scorePanel.endGame(won);
        resetVars();
    }

    @Override
    public void incrementFound() {
        System.out.println("Found Mine");
        prevFoundMines = foundMines;
        foundMines++;
        checkIfWon();
    }

    @Override
    public void decrementFound() {
        System.out.println("Changed Mind");
        foundMines = prevFoundMines;
        if (foundMines < 0) foundMines = 0;
        checkIfWon();
    }

    @Override
    public void incrementCleared() {
        System.out.println("cleared cell");
        clearedCells++;
        checkIfWon();
    }

    @Override
    public void gameStarted() {
        cellList.forEach(this::remove);
        cellList.clear();

        initGrid();
    }
}
