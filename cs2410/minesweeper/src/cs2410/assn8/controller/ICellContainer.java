package cs2410.assn8.controller;

import cs2410.assn8.view.Cell;

import java.util.List;

/**
 * Created by abe on 4/27/16.
 *
 * Interface to for any object that holds {@link cs2410.assn8.view.Cell Cells}
 */
public interface ICellContainer {
    /**
     * Gets adjacent cells - Cells do not have to be in a grid, this is up to chosen implementation
     * @param srcCell source cell
     * @return List of adjacent Cells
     */
    List<Cell> getAdjacent(Cell srcCell);

    /**
     * Gets all cells
     * @return List of all Cells
     */
    List<Cell> getCells();

    /**
     * called when cell is clicked
     */
    void cellClicked();

    /**
     * Called on game end
     * @param won whether game was won
     */
    void endGame(boolean won);

    /**
     * Increment found cells
     */
    void incrementFound();

    /**
     * Decrement found cells
     */
    void decrementFound();

    /**
     * Increment cleared cells
     */
    void incrementCleared();
}
