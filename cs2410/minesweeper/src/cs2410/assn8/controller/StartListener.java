package cs2410.assn8.controller;

/**
 * Listener interface for game start
 */
public interface StartListener {

    /**
     * called on game start
     */
    void gameStarted();
}
