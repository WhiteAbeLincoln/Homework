# Abraham White
# CS3810
# 2015-02-17

.data
	array: .word 9,8,7,6,5,4,3,2,1,0			# bubblesort worst case
	length: .word 40							# multiply length by 4 b/c of word offset
	comma: .asciiz ","
.text
main:
	la $s0, array								# load array address
	li $s1, 0									# var done = false
	srl $t0, $s2, 4

	whilebranch:
	beq $s1, 1, end								# while (! done)  -- end if 1

		#whilebody:
		li $s1, 1									# done = true

		li $s2, 4									# reset i = 1
		forbranch:
		lw $t1, length
		slt $t0, $s2, $t1					# if i < length
		bne $t0, 1, whilebranch				# else return to while loop
											# then goto for loop
			#forbody:
			# srl $t0, $s2, 2						# i - 1
			addi $t0, $s2, -4
			add $t0, $t0, $s0					# pointer to element at i - 1
			lw $t1, ($t0)						# element at i - 1
		
			add $t2, $s2, $s0					# pointer to element at i
			lw $t3, ($t2)						# element at i
		
			# addi $s2, $s2, 4					# i++
			addi $s2, $s2, 4
			
			#ifbranch
			slt $t4, $t3, $t1					# if array[i] < array[i-1]
			bne $t4, 1, forbranch				# if not, return to for branch
		
				#ifbody
				li $s1, 0							# done = false
				add $t4, $t1, 0						# var tmp = array[i-1] -- loads element at i-1 to temp
				sw $t3,($t0)						# array[i-1] = array[i]
				sw $t4,($t2)						# array[i] = tmp
			
		j forbranch
		# end for loop
		
	j whilebranch
	# end while loop	

end:

printarray:
	li $t0, 0
	lw $t1, length
printloop:
	li $v0, 1
	lw $a0 array($t0)
	syscall
	
	li $v0, 4
	la $a0, comma
	syscall
	
	addi $t0, $t0, 4
	slt $t2, $t0, $t1
	bne $t2, $zero, printloop	

li $v0, 10
syscall
