% Created 2020-03-31 Tue 23:04
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../preamble.tex} \newcommand{\Ringr}{\mathbf{R}} \newcommand{\Rings}{\mathbf{S}} \newcommand{\Field}{\mathbf{F}}
\newcommand*{\Ph}{\hphantom{)}} \usepackage{booktabs} \usepackage{array} \usepackage{polynom}
\author{Abraham White}
\date{\today}
\title{Midterm}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Midterm},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\begin{itemize}
\item Due By Wednesday April 1 at 12:30 pm
\item Provide clear and justified answers
\item Reference theorems, lemmas, and corollaries
\item \(\Field\) denotes a field, and \(\Ringr\), \(\Rings\) are rings.
\end{itemize}

\section*{1}
\label{sec:orgcdcf000}
For the ring on page 46 compute \(3rt^3 + 2s^2 + z\)
\begin{answer}
For reference, the mentioned ring is defined by the set \(T = \{r,s,t,z\}\) equipped
with addition and multiplication defined by the following tables:
\begin{center}
\begin{tabular}{l|llll}
+ & \(z\) & \(r\) & \(s\) & \(t\)\\
\hline
\(z\) & \(z\) & \(r\) & \(s\) & \(t\)\\
\(r\) & \(r\) & \(z\) & \(t\) & \(s\)\\
\(s\) & \(s\) & \(t\) & \(z\) & \(r\)\\
\(t\) & \(t\) & \(s\) & \(r\) & \(z\)\\
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{l|llll}
\(\cdot\) & \(z\) & \(r\) & \(s\) & \(t\)\\
\hline
\(z\) & \(z\) & \(z\) & \(z\) & \(z\)\\
\(r\) & \(z\) & \(z\) & \(r\) & \(r\)\\
\(s\) & \(z\) & \(z\) & \(s\) & \(s\)\\
\(t\) & \(z\) & \(z\) & \(t\) & \(t\)\\
\end{tabular}
\end{center}

\begin{align*}
3rt^3 + 2s^2 + z &= 3rt^3 + 2s^2 + z
\shortintertext{$z$ is the additive identity}
&= 3rt^3 + 2s^2
\shortintertext{We treat scalar multiplication of an integer as successive addition or subtraction}
&= (rt^3 + rt^3 + rt^3) + (s^2 + s^2)
\shortintertext{We treat exponentation to an integer power as successive multiplication}
&= (r(t \cdot t \cdot t) + r(t \cdot t \cdot t) + r(t \cdot t \cdot t)) + ((s \cdot s) + (s \cdot s))
\shortintertext{$t$ and $s$ are their own multiplicative identities}
&= (rt + rt + rt) + (s + s)
\shortintertext{$rt = r$ by the multiplication table}
&= (r + r + r) + (s + s)
\shortintertext{By the table, each element is its own additive inverse}
&= z + z \\
&= z
\end{align*}
\end{answer}
\section*{2}
\label{sec:orgec861ee}
Let \(\Ringr\) be a ring and \(b \in \Ringr\) any FIXED single element. Show that
\[ T = \{ rb \mid r \in \Ringr \} \] is a subring of \(\Ringr\)
\begin{answer}
We can use Theorem 3.6 to show that \(T\) is a subring of \(\Ringr\).
\begin{theorem*}{3.6}
Let $S$ be a nonempty subset of a ring $R$ such that
\begin{itemize}
\item $S$ is closed under subtraction ($a,b \in S \implies a - b \in S$)
\item $S$ is closed under multiplication ($a,b \in S \implies ab \in S$)
\end{itemize}
Then $S$ is a subring of $R$.
\end{theorem*}

We know that \(T\) is a subset of \(\Ringr\), because \(\Ringr\) is a ring and so must be closed
under multiplication. This means that for any fixed \(b \in \Ringr\) and all elements
\(r \in \Ringr\), \(rb \in \Ringr\). If \(\Ringr\) is nonempty then \(T\) will also be nonempty. \\

Now we show that \(T\) is closed under multiplication. \\
Suppose we have \(x = r_1 b\) and \(y = r_2 b\), both elements of \(T\). We want to show that
\(xy\) still has the proper form to be an element of \(T\).
\begin{align*}
xy &= r_1 b \cdot r_2 b \\
   &= (r_1 b r_2) b && \text{Multiplication in rings is associative}
   \shortintertext{$r_1, b, r_2 \in \Ringr$, so we can let $r = r_1 b r_2$ be an element of $\Ringr$}
   &= rb
   \shortintertext{This fits the requirements to be a member of $T$, therefore $T$ is closed under multiplication}
\end{align*}

We show that \(T\) is closed under subtraction. \\
Suppose we have \(x = r_1 b\) and \(y = r_2 b\), both elements of \(T\). We want to show that
\(x - y\) still has the proper form to be an element of \(T\).
\begin{align*}
x - y &= r_1 b - r_2 b \\
      &= (r_1 - r_2) b && \text{Multiplication in rings is distributive}
\shortintertext{$r_1, r_2 \in \Ringr$, so we can let $r = r_1 - r_2$ be an element of $\Ringr$}
      &= r b
\shortintertext{This fits the requirements to be a member of $T$, therefore $T$ is closed under subtraction}
\end{align*}

Since \(T\) is a nonempty subset of \(\Ringr\) closed under both subtraction and multiplication,
by Theorem 3.6 \(T\) is a subring of \(\Ringr\).
\end{answer}
\section*{3}
\label{sec:org3e185f4}
Let \(f : \Ringr \to \Rings\) be a ring homomorphism. The kernel of \(f\) is defined to be
\[ \ker f = \{ r \in \Ringr \mid f(r) = 0_{\Rings} \} \] where \(0_{\Rings}\) is the zero
element of \(\Rings\). Show (using the fact that \(f\) is a homomorphism) that \(\ker f\) is
a subring of \(\Ringr\).
\begin{answer}
We know that \(\ker f\) is nonempty, since by Theorem 3.10 \(f(0_{\Ringr}) = 0_{\Rings}\), and
so \(0_{\Ringr}\) is an element of \(\ker f\). By definition, \(\ker f\) is a subset of \(\Ringr\).

We also can determine that the elements of \(\ker f\) are closed under multiplication.
Let \(a,b \in \ker f\). By the definition of a homomorphism, \(f(ab) = f(a)f(b)\).
Since \(f(a) = 0_{\Rings} = f(b)\), we know \(f(ab) = 0_{\Rings}\) and so \(ab \in \ker f\).

Similarly, we can show that the elements of \(\ker f\) are closed under subtraction.
Let \(a,b \in \ker f\). By Theorem 3.10, \(f(a - b) = f(a) - f(b)\). By the definition of a kernel,
\(f(a) = f(b) = 0_{\Rings}\). Therefore, \(f(a - b) = 0_{\Rings}\) and \(a - b \in \ker f\).

By Theorem 3.6, since \(\ker f\) is a nonempty subset of \(\Ringr\) and is closed under multiplication
and subtraction, \(\ker f\) is a subring of \(\Ringr\).
\end{answer}
\section*{4}
\label{sec:orgb00529e}
Show \(\ZZ\) is not isomorphic to \(\ZZ \times \ZZ\). Here \(\ZZ\) is the integers.
\begin{answer}
For two rings \(\Ringr\), \(\Rings\) to be isomorphic, there must be an isomorphism \(f : \Ringr \to \Rings\).
An isomorphism must be bijective and \(f(a + b) = f(a) + f(b)\) and \(f(ab) = f(a)f(b)\) for all \(a,b \in \Ringr\). \\

Suppose that there is an isomorphism \(f : \ZZ \to \ZZ \times \ZZ\). Since \(f\) is a bijection, this means that we can find
two elements, \(a,b \in \ZZ\) such that \(f(a) = \opair{1,0}\) and \(f(b) = \opair{0, 1}\). Multiplying \(f(a)f(b)\) gives us
\(\opair{1, 0} \cdot \opair{0,1} = \opair{0, 0}\). However for \(f\) to be an isomorphism, \(f(ab)\) must be equivalent.

Recall that we can treat multiplication of integers as repeated addition.
\begin{align*}
f(ab) &= f(b + b + \cdots + b) \\
&= f(b) + f(b) + \cdots + f(b) \\
&= af(b) \\
&= a \cdot \opair{0,1} \\
&= \opair{0,1} + \opair{0,1} + \cdots + \opair{0,1} \\
&= \opair{0,a}
\end{align*}
Since \(\ZZ\) is commutative, \(ab = ba\). Now we compute \(f(ab)\) in another way.
\begin{align*}
f(ab) &= f(ba) \\
&= f(a + a + \cdots + a) \\
&= f(a) + f(a) + \cdots + f(a) \\
&= bf(a) \\
&= b \cdot \opair{1,0} \\
&= \opair{b,0}
\end{align*}
The only way for \(f(ab) = f(ba) = f(a)f(b)\) is for \(a = b = 0\). However this would mean that 
\(f(0)\) maps to \(\opair{1,0}, \opair{0,1}\) and \(\opair{0,0}\), which is not valid.
Therefore by contradiction there is no isomorphism between \(\ZZ\) and \(\ZZ \times \ZZ\).
\end{answer}
\section*{5}
\label{sec:org6ccd293}
Is \(f : \ZZ \to \ZZ\) given by \(f(n) = -n\) an isomorphism? Prove or give a counterexample.
\begin{answer}
An isomorphism must be a bijective homomorphic function. This function is obviously bijective,
but we must also prove that \(f(a+b) = f(a) + f(b)\) and \(f(ab) = f(a)f(b)\) for all \(a,b \in \ZZ\).
\begin{align*}
\shortintertext{Let $a,b \in \ZZ$}
f(a+b) &= f(a) + f(b) \\
-(a + b) &= -a + -b \\
-a + -b  &= -a + -b \\ \\
f(ab) &= f(a)f(b) \\
-ab   &= -a \cdot -b \\
\shortintertext{By Theorem 3.5, $(-a)(-b) = ab$}
-ab &\neq ab
\end{align*}
The function \(f\) doesn't distribute over multiplication, and so \(f\) is not an isomorphism.
\end{answer}
\section*{6}
\label{sec:orgacaf882}
Is \(\ZZ_2 \times \ZZ_2\) isomorphic to \(\ZZ_4\)? Prove or disprove.
\begin{answer}
Suppose that there is an isomorphism \(f : \ZZ_2 \times \ZZ_2 \to \ZZ_4\). By Theorem 3.10,
\(f(\opair{0,0}) = 0\) and \(f(\opair{1,1}) = 1\). If we define \(f\) with the table
below things seem to work out. All values in \(\ZZ_4\) are mapped by values in \(\ZZ_2 \times \ZZ_2\),
and \(f(\opair{1,1}) = f(\opair{1,0} + \opair{0,1}) = f(\opair{1,0}) + f(\opair{0,1})\).
\begin{center}
\begin{tabular}{lr}
\(x\) & \(f(x)\)\\
\hline
\(\opair{0,0}\) & 0\\
\(\opair{1,1}\) & 1\\
\(\opair{0,1}\) & 2\\
\(\opair{1,0}\) & 3\\
\end{tabular}
\end{center}

However, look at the case of \(f(\opair{1,0} + \opair{1,1})\). \(\opair{1,0} + \opair{1,1} = \opair{0,1}\), but
\(f(\opair{0,1}) \neq f(\opair{1,0}) + f(\opair{1,1})\).

\begin{align*}
f(\opair{1,0} + \opair{1,1}) &= f(\opair{1,0}) + f(\opair{1,1}) \\
f(\opair{0,1}) &= f(\opair{1,0}) + f(\opair{1,1}) \\
2 &= 3 + 1 \\
2 &\neq 0
\end{align*}

Since the behavior of \(f\) is fixed for inputs \(\opair{0,0}\) and \(\opair{1,1}\) by Theorem 3.10, the only thing that we
can change to fix this issue is swap the mappings for \(\opair{0,1}\) and \(\opair{1,0}\). However, this results
in the same behavior. Therefore there cannot be an isomorphism between \(\ZZ_2 \times \ZZ_2\) and \(\ZZ_4\).
\end{answer}
\section*{7}
\label{sec:org32d6a35}
For the polynomials \(a(x), b(x) \in \ZZ_5[x]\) given by
\[ a(x) = 3x^3 + 4x^2 + x + 2 ~~\text{and}~~ b(x) = 2x^2 + 1 \]
\subsection*{a}
\label{sec:org520efb4}
Compute the quotient and remainder of \(a(x)\) divided by \(b(x)\) (don't forget \(\ZZ_5[x]\)).
\begin{answer}
\(\begin{array}{r r@{} l@{} l@{} l@{}}
   & & & 4x &{}+ 2 \\
   \cmidrule{2-5}
   2x^2 + 1~\big\rvert & 3x^3  &{}+ 4x^2 &{}+ x  &{}+ 2 \\
                       &-(3x^3 &{}       &{}+ 4x) \\
   \cmidrule{2-4}
                       &       &{}\hphantom{+} 4x^2  &{}- 3x &{}+ 2 \\
                       &       &-(4x^2             &{}     &{}+ 4) \\
   \cmidrule{3-5}
                       &       &{}                   &{}  3x &{}- 2 \\
   \end{array}\) \\
So the quotient is \(4x + 2\) and the remainder is \(3x - 2\).
\end{answer}
\subsection*{b}
\label{sec:org5f11dfb}
Using the \textbf{Euclidean Algorithm} compute the \(\gcd(a(x), b(x))\) (again in \(\ZZ_5[x]\)).
\begin{answer}
\begin{align*}
3x^3 + 4x^2 + x + 2 &= (2x^2 + 1) \cdot (4x + 2) + (3x - 2) \\
2x^2 + 1 &= (3x - 2) \cdot (4x + 1) + 3 \\
3x - 2 &= 3 \cdot (x + 1) + 0
\end{align*}
So \(\gcd(a(x), b(x)) = 3\)
\end{answer}
\section*{8}
\label{sec:orgf7b2848}
Write \(x^4 - 4\) in \(\ZZ_5[x]\) as a product of irreducibles. State why things are irreducible.
\begin{answer}
First, we can reduce \(x^4 - 4\) into \((x^2 + 2)(x^2 - 2)\). By Corollary 4.19, we know that
if these two factors have no roots then they are irreducible. 

First we can test the function \(f(x)\) induced by \(x^2 - 2\)
\begin{center}
\begin{tabular}{rr}
x & f(x)\\
\hline
0 & -2\\
1 & -1\\
2 & 2\\
3 & 2\\
4 & -1\\
\end{tabular}
\end{center}
There are no roots for the function \(f\), so \(x^2 - 2\) is irreducible.

Now we test the function \(g(x)\) induced by \(x^2 + 2\)
\begin{center}
\begin{tabular}{rr}
x & g(x)\\
\hline
0 & 2\\
1 & 3\\
2 & 1\\
3 & 1\\
4 & 3\\
\end{tabular}
\end{center}
There are no roots for the function \(g\), so \(x^2 + 2\) is irreducible.
\end{answer}
\section*{9}
\label{sec:orgc23ed48}
Let \(p(x) \in \Field[x]\) be of degree \(n \geq 0\), and suppose \(a(x) \in \Field[x]\), is of degree \(n\)
and that \(a(x)\) divides \(p(x)\). Show that \(a(x)\) is an associate of \(p(x)\).
\begin{answer}
Recall that \(f(x)\) is an associate of \(g(x)\) in \(\Field[x]\) if and only if \(f(x) = cg(x)\) for some nonzero c \(\in\) \(\Field\). \\

We know that \(a(x)\) divides \(p(x)\). Therefore, \(p(x) = a(x) \cdot q(x)\). Since \(p(x)\) and \(a(x)\) both have the same degree, we know by
Theorem 4.2 that \(q(x)\) must have degree 0 and be a constant polynomial \(c\). Since \(p(x) \neq 0\), we also know that \(c\) must be nonzero.
This is because by Corollary 4.3, since \(\Field\) is an integral domain, \(\Field[x]\) is also an integral domain, and one of the properties
of an integral domain is that for a product of two elements \(ab \neq 0, a \neq 0\) and \(b \neq 0\).

All of this means that we have some nonzero \(c \in \Field\), such that \(p(x) = ca(x)\). Therefore \(a(x)\) is an associate of \(p(x)\).
\end{answer}
\section*{10}
\label{sec:org45a685e}
For what values of \(k\) is \(x + 3\) a factor (divisor) of \(x^8 + 2x^3 + 3x^2 + kx + 1\) in \(\ZZ_5[x]\)?
\begin{answer}
By Theorem 4.16, we know that \(x - a\) is a factor of a polynomial \(f(x) \in F[x]\) if and only if \(a\) is a root of \(f(x)\). \\

Let \(f(x) = x^8 + 2x^3 + 3x^2 + kx + 1\) In this case \(a = -3\). We must find all values of \(k\) such that \(f(a) = 0\).

First we can evaluate the polynomial for \(x = a\), giving us a new function \(g\) in terms of \(k\).
\begin{align*}
f(a) &= a^8 + 2a^3 + 3a^2 + ka + 1 \\
     &= (-3)^8 + 2(-3)^3 + 3(-3)^2 + k(-3) + 1 \\
     &= 1 + 2(-2) + 3(4)  -3k + 1 \\
     &= 1 - 4 + 2 -3k + 1 \\
g(k) &= -3k
\end{align*}

Now all values \(k\) such that \(g(k) = 0\) are values for which \(x + 3\) is a factor of \(f(x)\) (in other words, the kernel \(\ker g\)).
The only possible value is \(k = 0\).
\end{answer}
\end{document}