% Created 2020-04-25 Sat 20:41
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../preamble.tex} \newcommand{\Ringr}{\mathbf{R}} \newcommand{\Rings}{\mathbf{S}} \newcommand{\Field}{\mathbf{F}}
\newcommand*{\Ph}{\hphantom{)}} \usepackage{booktabs} \usepackage{array} \usepackage{polynom}
\author{Abraham White}
\date{\today}
\title{Final}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Final},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle

\section*{1}
\label{sec:orgf92d6fa}
Let \(S \in M_{2\times 2}(\RR)\) be the subring of \(2 \times 2\) real matrices with matrix addition and matrix multiplication
defined by \[ S = \left \{ \mat[p]{a&b\\0&c} \mid a,b,c \in \RR \right \} \]
\subsection*{a}
\label{sec:org60c955d}
Is \(S\) a commutative ring? Prove or give a counter example. Does \(S\) have an identity?
\begin{answer}
Using Theorem 3.6, we can show that \(S\) is a ring by showing that it is closed under subtraction and multiplication.
Let \(A = \mat[p]{a&b\\0&c}, B = \mat[p]{d&e\\0&f} \in S\).

\textbf{Subtraction}:
\begin{align*}
\mat[p]{a&b\\0&c} - \mat[p]{d&e\\0&f} &= \mat[p]{a-d&b-e\\0&c-f} \\
\end{align*}
Since \(a,b,c,d,e,f \in \RR\), we know that \(\mat[p]{a-d&b-e\\0&c-f} \in S\) as defined above. Therefore \(S\) is closed under subtraction.

\textbf{Multiplication}:
\begin{align*}
AB &= \mat[p]{a&b\\0&c} \mat[p]{d&e\\0&f} \\
&= \mat[p]{ad + b(0)&ae+bf\\0d+c(0)&0e+cf} \\
&= \mat[p]{ad&ae+bf\\0&cf}
\end{align*}
Since \(a,b,c,d,e,f \in \RR\), we know that \(\mat[p]{ad&ae+bf\\0&cf} \in S\) as defined above. Therefore \(S\) is closed under multiplication,
and by Theorem 3.6, \(S\) is a ring.

Now we must show that multiplication is commutative in \(S\). To do this, we must show that \(\forall a,b \in S, ab = ba\).
We already know the result of \(AB\). Let us show that \(BA = AB\).
\begin{align*}
BA &= \mat[p]{d&e\\0&f} \mat[p]{a&b\\0&c} \\
&= \mat[p]{da + e(0)&bd + ce\\0a + f(0)&0b+fc}
\shortintertext{Multiplication is commutative in $\RR$ so we can reorder.}
&= \mat[p]{ad&ae+bf\\0&cf} = AB
\end{align*}
We have shown that multiplication is commutative for all \(a,b \in S\), so by defintion \(S\) is a commutative ring.

Now we need to find if \(S\) has an identity. A ring \(R\) with \textbf{identity} is one that contains an element \(1_R\) statisfying
\[ a1_R = a = 1_Ra \text{ for all } a \in R \]

We know that \(I = \mat[p]{1&0\\0&1}\) is the identity for \(2 \times 2\) real matrices. This matrix also happens to exist in the subset
\(S\), if we let \(a=1, b=0, c=1\) in \(\mat[p]{a&b\\0&c}\). All that remains is to verify that \(IA = A = AI\).
\begin{align*}
IA &= \mat[p]{1&0\\0&1} \mat[p]{a&b\\0&c} \\
&= \mat[p]{1a + 0&1b+c(0)\\0a + 0&0b + 1c} \\
&= \mat[p]{a&b\\0&c} = A \\
AI &= \mat[p]{a&b\\0&c} \mat[p]{1&0\\0&1} \\
   &= \mat[p]{a&b\\0&c} = A
\end{align*}
Therefore, \(S\) does have an identity.
\end{answer}
\subsection*{b}
\label{sec:orgf7f737a}
Let \(f : S \to \RR\) be given by \[ f \mat[p]{a&b\\0&c} = b \] Is \(f\) a homomorphism to the real numbers \(\RR\) with the usual
addition and multiplication on \(\RR\)? If not give a counter example.
\begin{answer}
A homomorphism is defined as a function \(f: R \to S\) (\(R\) and \(S\) being rings) where \(f(a + b) = f(a) + f(b)\) and \(f(ab) = f(a)f(b)\)
for all \(a,b \in R\).

Let \(A = \mat[p]{a&b\\0&c}, B = \mat[p]{d&e\\0&f} \in S\).

First we show that \(f(A+B) = f(A) + f(B)\).
\begin{align*}
f(A+B) &= f\left(\mat[p]{a&b\\0&c} + \mat[p]{d&e\\0&f}\right) \\
&= f\mat[p]{a+d&b+e\\0&c+f} \\
&= b+e \\
f(A) + f(B) &= b + e = f(A + B)
\end{align*}

Next we show that \(f(ab) = f(a)f(b)\)
\begin{align*}
f(AB) &= f\left(\mat[p]{a&b\\0&c} \mat[p]{d&e\\0&f}\right) \\
&= f\mat[p]{ad&ae+bf\\0&cf} \\
&= ae + bf \\
f(A)f(B) &= be \neq f(AB)
\end{align*}
Therefore, \(f\) is not a homomorphism. We can also show this with a counterexample: Let \(I = \mat[p]{1&0\\0&1}\), \(C = \mat[p]{5&6\\0&3}\)
\begin{align*}
f(IC) &= f(C) = 6 \\
f(I)f(C) &= 0 \cdot 6 = 0 \\
6 &\neq 0
\end{align*}
\end{answer}
\subsection*{c}
\label{sec:org198e96e}
Let \(g: \RR \to S\) be \[ g(t) = \mat[p]{0&t\\0&0} \] Is \(g\) a homomorphism from the real numbers \(\RR\) with the usual addition
and multiplication on \(\RR\)? If not give a counter example
\begin{answer}
Let \(a,b \in \RR\).
\begin{align*}
g(a + b) &= \mat[p]{0&a+b\\0&0} \\
g(a) + g(b) &= \mat[p]{0&a\\0&0} + \mat[p]{0&b\\0&0} \\
&= \mat[p]{0&a+b\\0&0} = g(a + b)
\end{align*}

\begin{align*}
g(ab) &= \mat[p]{0&ab\\0&0} \\
g(a)g(b) &= \mat[p]{0&a\\0&0}\mat[p]{0&b\\0&0} \\
&= \mat[p]{0&0\\0&0} \neq g(ab)
\end{align*}
Therefore \(g\) is not a homomorphism. We can also show this with a counterexample. Let \(a = 5, b = 11\).
\begin{align*}
g(ab) &= g(55) = \mat[p]{0&55\\0&0} \\
g(a)g(b) &= \mat[p]{0&5\\0&0}\mat[p]{0&11\\0&0} \\
&= \mat[p]{0&0(11) + 5(0)\\0&0} = \mat[p]{0&0\\0&0}
\end{align*}
\end{answer}
\subsection*{d}
\label{sec:orgd24982e}
Here we work with the ring \(S\) and consider the subset \(T \subset S\) given by \[ T = \left\{\mat[p]{0&a\\0&0} \mid a \in \RR \right\} \]
Is \(T\) an ideal in \(S\) (NOT asking if \(T\) is an ideal in \(M_{2\times 2}(\RR)\))? If not give a counter example. \textbf{Bonus}: Is \(T\) an ideal
in \(M_{2\times 2}(\RR)\)?
\begin{answer}
We can use Theorem 6.1 to show that \(T\) is an ideal in \(S\).
\begin{theorem*}{6.1}
A nonempty subset $I$ of a ring $R$ is an ideal if an only if it has these properties:
\begin{itemize}
\item if $a,b \in I$, then $a-b \in I$.
\item if $r \in R$ and $a \in I$, then $ra \in I$ and $ar \in I$.
\end{itemize}
\end{theorem*}
We know that \(T\) is nonempty, because \(\RR\) is nonempty.

We show that the first property holds:

Let \(A = \mat[p]{0&a\\0&0},B = \mat[p]{0&b\\0&0} \in T\).
\begin{align*}
A - B &= \mat[p]{0&a\\0&0} - \mat[p]{0&b\\0&0} \\
      &= \mat[p]{0&a-b\\0&0}
\end{align*}
Because \(a-b \in \RR\), \(T\) must contain \(\mat[p]{0&a-b\\0&0}\).

Next, we show that the second property holds:

Let \(A = \mat[p]{0&a\\0&0} \in T\). Let \(R = \mat[p]{b&c\\0&d} \in S\).
\begin{align*}
AR &= \mat[p]{0&a\\0&0}\mat[p]{b&c\\0&d} \\
   &= \mat[p]{0&ad\\0&0} \\
RA &= \mat[p]{b&c\\0&d}\mat[p]{0&a\\0&0} \\
   &= \mat[p]{0&ab\\0&0}
\end{align*}
Both \(ad\) and \(ab\) are in \(\RR\), therefore \(AR\) and \(RA\) both fit the form of \(T\).

Since both properties hold, \(T\) is an ideal in \(S\) by Theorem 6.1.

\textbf{Bonus}
We only have to prove the second property, as the first still applies.

Let \(A = \mat[p]{0&a\\0&0} \in T\). Let \(R = \mat[p]{b&c\\d&e} \in M_{2\times 2}(\RR)\).
\begin{align*}
AR &= \mat[p]{0&a\\0&0}\mat[p]{b&c\\d&e} \\
&= \mat[p]{0b+ad&0c+ae\\0b+0d&0c+0e} \\
&= \mat[p]{ad&ae\\0&0}
\end{align*}
\(T\) is not an ideal in \(M_{2\times 2}(\RR)\), because \(AR \not \in T\).
\end{answer}
\section*{2}
\label{sec:orgdc0167e}
Let \(p(x) = x^3 + 1 \in \ZZ_2[x]\).
\subsection*{a}
\label{sec:orgc617a13}
Factor \(p(x)\) in \(\ZZ_2[x]\) into a product of irreducible polynomials and show why each term is irreducible by referencing a result in
the book.
\begin{answer}
We begin by listing all possible factors of lesser degree that may be irreducible polynomials. This is feasible because
we are in \(\ZZ_2[x]\).

\begin{align*}
&x \\
&x + 1 \\
&x^2 \\
&x^2 + 1 \\
&x^2 + x \\
&x^2 + x + 1
\end{align*}

We want a polynomial of degree 3, so the only possible combinations are:
\(a \in \{x, x+1\}\) multiplied by \(b \in \{x^2, x^2+1,x^2+x,x^2+x+1\}\).
Additionally, since we need a constant term in the result,
we know that both factors must also have a constant term. This further reduces our combinations to:
\(x+1\) multiplied by \(b \in \{x^2+1, x^2+x+1\}\). We can also use Theorem 4.16 to verify that
\(x+1\) is a factor of \(p(x)\). Since 1 is a root of \(p(x)\), \(x-1\) is a factor of \(p(x)\) by Theorem 4.16.
In \(\ZZ_2\), \(-1 \equiv 1\), so \(x+1\) is a factor of \(p(x)\).

\begin{align*}
(x+1)(x^2+1) &= x^3 + x + x^2 + 1
\end{align*}
This is not \(x^3 + 1\), so we try the other combination.
\begin{align*}
(x+1)(x^2+x+1) &= x^3 + x^2 + x + x^2 + x + 1 \\
               &= x^3 + 2x^2 + 2x + 1 \\
               \shortintertext{$2 \equiv 0 \mod{2}$}
               &= x^3 + 1
\end{align*}
Therefore \(x+1\) and \(x^2+x+1\) are our two factors. Now we must show that these polynomials are
irreducible. We know that \(x+1\) is irreducible because it is a polynomial of degree 1. To
show that \(x^2 + x + 1\) is irreducible, we can use Corollary 4.19:
\begin{corollary*}{4.19}
Let $F$ be a field and let $f(x) \in F[x]$ be a polynomial of degree 2 or 3. Then $f(x)$ is
irreducible in $F[x]$ if and only if $f(x)$ has no roots in $F$.
\end{corollary*}

Let \(f: \ZZ_2 \to \ZZ_2\) be the function induced by \(x^2 + x + 1\).
Because the domain \(f\) is \(\ZZ_2\), we only have to check if 0 or 1 are roots.
\begin{align*}
f(0) &= 0^2 + 0 + 1 = 1 \\
f(1) &= 1^2 + 1 + 1 = 1
\end{align*}
Therefore, \(x^2 + x + 1\) is irreducible by Theorem 4.19.
\end{answer}
\subsection*{b}
\label{sec:orgd720151}
Use part (a) to find the units and zero divisors in \(\ZZ_2[x]/(p(x))\). \textbf{Hint}: Theorem 5.9.
\begin{answeri}
We know that \(\ZZ_2[x]/(p(x))\) consists of 8 congruence classes:
\[ [0], [1], [x], [x+1], [x^2], [x^2 + 1], [x^2 + x], [x^2 + x + 1] \]
To find the units, we can use Theorem 5.9
\begin{theorem*}{5.9}
Let $F$ be a field and $p(x)$ a nonconstant polynomial in $F[x]$. If $f(x) \in F[x]$ and
$f(x)$ is relatively prime to $p(x)$, then $[f(x)]$ is a unit in $F[x]/(p(x))$.
\end{theorem*}
Two polynomials are relatively prime if their GCD is 1. We know by Theorem 4.16 that if
\(a\) is a root of a polynomial \(f(x) \in F[x]\), then \(x - a\) is a factor of \(f(x)\).
Therefore, if two reducible polynomials share a root \(a\), then they will both have \(x-a\) as a
factor, meaning 1 is not their greatest common divisor. We can use this fact to reduce 
the congruence classes that we have to check as units.

We know that \(p(x)\) has a root of 1. \(x^2 + 1\), and \(x^2 + x\) both also have roots of 1, and so
cannot be relatively prime to \(p(x)\) because they share roots. That leaves us to consider
\[ [x], [x+1], [x^2], [x^2 + x + 1] \]

We know that the factors of \(p(x)\) are \(x+1\) and \(x^2 + x + 1\), and by Theorem 4.14 this is a unique
factorization. This excludes \(x+1\) and \(x^2+x+1\) as relatively prime to \(p(x)\), because \(\gcd(a(x),b(x))\) is
defined as the monic polynomial of highest degree that divides both \(a(x)\) and \(b(x)\). The divisors of \(x+1\)
are \(x+1\) and \(1\). The divisors of \(x^3+1\) are \(\{x+1, x^2+x+1, x^3+1, 1\}\). Out of the divisors in common (\(\{1,x+1\}\)),
the one with greatest degree is \(x+1\). This same argument can be used to show that
\(\gcd(x^2+x+1, x^3+1) = x^2 + x+ 1\). Therefore, the only classes left to consider as units are:
\[ [x], [x^2] \]
The divisors of \(x\) are \(\{x,1\}\). The divisors of \(x^2\) are \(\{x, x^2, 1\}\). The only common divisor with
\(x^3+1\) (whose divisors are \(\{x+1, x^2+x+1, x^3+1, 1\}\)) is 1. Therefore both \(x\) and \(x^2\) are relatively prime
with \(x^3+1\). By Theorem 5.9, these are unit in \(\ZZ_2[x]/(p(x))\). \\

Since \(p(x)\) is reducible, \(\ZZ_2[x]/(p(x))\) may not be an integral domain, and therefore may have zero divisors.
A zero divisor is an element \(a\) of a ring where \(a \neq 0\), and there exists a nonzero element \(c\) in the ring
such that \(ac = 0\) or \(ca = 0\). We will brute force finding the zero divisors by constructing a multiplication table 
for a subset of the congruence classes in \(\ZZ_2[x]/(p(x))\).

\begin{center}
\begin{tabular}{l|llllll}
\(\times\) & \(x\) & \(x+1\) & \(x^2\) & \(x^2+1\) & \(x^2+x\) & \(x^2+x+1\)\\
\hline
\(x\) & \(x^2\) & \(x^2+x\) & 1 & \(x+1\) & \(x^2+1\) & \(x^2+x+1\)\\
\(x+1\) & \(x^2+x\) & \(x^2+1\) & \(x^2+1\) & \(x^2+x\) & \(x+1\) & 0\\
\(x^2\) & 1 & \(x^2+1\) & \(x\) & \(x^2+x\) & \(x+1\) & \(x^2+x+1\)\\
\(x^2+1\) & \(x+1\) & \(x^2+x\) & \(x^2+x\) & \(x+1\) & \(x^2+1\) & 0\\
\(x^2+x\) & \(x^2+1\) & \(x+1\) & \(x+1\) & \(x^2+1\) & \(x^2+x\) & 0\\
\(x^2+x+1\) & \(x^2+x+1\) & 0 & \(x^2+x+1\) & 0 & 0 & \(x^2+x+1\)\\
\end{tabular}
\end{center}

From this we can deduce that \(x^2+x+1\), \(x+1\), \(x^2+1\), and \(x^2+x\) are all zero divisors in \(\ZZ_2[x]/(p(x))\).
\end{answeri}
\section*{3}
\label{sec:orge0af4a3}
Let \(I\) and \(J\) be ideals in a ring \(R\). Prove that \(I \cap J\) is an ideal.
\begin{answer}
The definition of an ideal is: a subring \(I\) of a ring \(R\) where whenever \(r \in R\) and \(a \in I\),
then \(ra \in I\) and \(ar \in I\). \\

We have two ideals \(I\) and \(J\). This means that given an \(r \in R\), multiplication of any element in \(I\) by
\(r\) results in an element of \(I\), and multiplication of any element in \(J\) results in an element of \(J\). \\

We know that \(I\) and \(J\) are not disjoint, since as subrings they must contain the zero element of \(R\).
We can use Theorem 6.1 to show that \(I \cap J\) is an ideal in \(R\). \\

Suppose we have two elements \(a,b \in I \cap J\). By the definition of set intersection,
\(a,b \in I\) and \(a,b \in J\). \\

Since \(a\) and \(b\) are elements of both \(I\) and \(J\), \(a - b \in I\) and \(a - b \in J\). So \(a-b \in I \cap J\) and
\(I \cap J\) is closed under subtraction. \\

Let \(r \in R\). Since \(I\) is an ideal and \(a \in I\), \(ra,ar \in I\). But \(J\) is also an ideal, and \(a \in J\).
So \(ra,ar\) must also be in \(J\). It follows that \(ra,ar \in I \cap J\) and so by Theorem 6.1 \(I \cap J\) must
be an ideal in \(R\).
\end{answer}
\section*{4}
\label{sec:org0cda39b}
Let \(2x^3 + x + 1 \in \ZZ_3[x]\)
\subsection*{a}
\label{sec:org63ad6a8}
Prove that \(\ZZ_3[x]/(2x^3 + x + 1)\) is a field.
\begin{answer}
Let \(p(x) = 2x^3 + x + 1 \in \ZZ_3[x]\).
By Theorem 5.10, \(p(x)\) being irreducible implies that \(\ZZ_3[x]/(p(x))\) is a field.

We can use Corollary 4.19 to show that \(p\) is irreducible. Because \(p(x)\) is in \(\ZZ_3[x]\),
we only need to check for roots in \(\{0,1,2\}\).
\begin{align*}
p(0) &= 2(0)^3 + 0 + 1 = 1 \neq 0 \\ 
p(1) &= 2(1)^3 + 1 + 1 = 1 \neq 0 \\ 
p(2) &= 2(2)^3 + 2 + 1 = 1 \neq 0
\end{align*}
Since \(p(x)\) has no roots, by Corollary 4.19 it is irreducible. Since \(p(x)\) is irreducible,
by Theorem 5.10 \(\ZZ_3[x]/(p(x))\) is a field.
\end{answer}
\subsection*{b}
\label{sec:org31aa57d}
Find \([2x + 1]^{-1}\) in the form \([ax^2 + bx + c]\) where \(a,b,c \in \{0,1,2\} = \ZZ_3\).
\begin{answeri}
We want to find a polynomial \(a(x)\) in the form \([ax^2 + bx + c]\) such that \(a(x) [2x + 1] = 1\).
Let \(p(x) = 2x^3 + x + 1 \in \ZZ_3[x]\).

This inverse \(a(x)\) will only exist if \(2x + 1\) is unit in \(\ZZ_3[x]/(p(x))\), since by definition a unit
is an element that has an inverse. By Theorem 5.9, \(2x + 1\) is unit because it is relatively prime
to \(2x^3 + x + 1\) (because \(2x^3 + x + 1\) is irreducible, its only divisors are itself and 1. \(2x + 1\) is
has lesser degree than \(2x^3 + x + 1\), so the only common divisor can be 1). 

We can use the Division Algorithm to find \(a(x)\), the inverse of \([2x+1]\).

\begin{align*}
2x^3 + x + 1 &= (2x+1)(x^2+x) + 1 \\
1 &= 2x^3 + x + 1 - (2x+1)(x^2+x) \\
&= 2x^3 + x + 1 + (2x+1)(-x^2-x)
\shortintertext{$-1 \equiv 2 \pmod{3}$}
&= 2x^3 + x + 1 + (2x+1)(2x^2+2x)
\shortintertext{$2x^3+x+1 \equiv 0 \pmod{2x^3 + x + 1}$}
1 &= (2x+1)(2x^2+2x)
\end{align*}
Therefore, \([2x^2 + 2x]\) is the inverse of \([2x+1]\).
\end{answeri}
\section*{5}
\label{sec:org6254854}
Find a prime number \(p\) such that \(x - 2\) is a factor of \(x^4 + x^3 + x + 1\) in \(\ZZ_p[x]\).
\begin{answer}
By Theorem 4.16 (The Factor Theorem), given a field \(F\), \(f(x) \in F[x]\), and \(a \in F\), if \(x-a\) is a
factor of \(f(x)\), then \(a\) is a root of \(f(x)\).

Therefore, we want to find a prime \(p\) such that \(f(x) = x^4 + x^3 + x + 1 \in \ZZ_p[x]\) has a root in
\(x = 2\). Start by applying \(g(x) = x^4 + x^3 + x + 1 \in \ZZ[x]\) to \(2\).
\[ g(2) = 2^4 + 2^3 + 2 + 1 = 27 \]
We want a prime \(p\) such that \(27 \equiv 0 \mod{p}\). 27 is divisible by 1, 3, 9, and 27. The only prime
number in that set is 3, therefore we want \(p = 3\).
\end{answer}
\section*{6}
\label{sec:orgaef4516}
Find all solutions to \(x^3 + 2x + 1 = 0\) in \(\ZZ_5\).
\begin{answer}
We are in \(\ZZ_5\), so we only have to check for roots in the set of congruence classes \(\{[0],[1],[2],[3],[4]\}\).
Let \(f(x) = x^3 + 2x + 1\).
\begin{align*}
f(0) &= 0^3 + 2(0) + 1 = 1 \neq 0 \\
f(1) &= 1^3 + 2(1) + 1 = 4 \neq 0 \\
f(2) &= 2^3 + 2(2) + 1 = 3 \neq 0 \\
f(3) &= 3^3 + 2(3) + 1 = 4 \neq 0 \\
f(4) &= 4^3 + 2(4) + 1 = 3 \neq 0
\end{align*}
Therefore, \(f\) has no roots and no solutions to \(x^3 + 2x + 1 = 0\) in \(\ZZ_5\).
\end{answer}
\end{document}