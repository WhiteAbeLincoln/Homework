#include <iostream>
#include <mpi.h> // this includes the mpi library
#include <unistd.h>
#include <stdlib.h>
#define MCW MPI_COMM_WORLD
int main(int argc, char** argv) {
  int rank, size;
  // this is required at the beginning of all MPI programs
  MPI_Init(&argc, &argv);

  // determines the rank of the calling process in the communcator value is set to rank variable
  MPI_Comm_rank(MCW, &rank);
  // determines the size of the communicator group - value is assigned at runtime by console
  // value is set to size - I hate output paramters
  MPI_Comm_size(MCW, &size);
MPI_Send( &rank/*buffer*/
        , 1/*count*/
        , MPI_INT/*datatype*/
        , (rank+1) % size/*destination*/
        , 0/*tag*/
        , MCW/*communicator*/);
int data;
MPI_Recv( &data/*buffer*/
        , 1/*count*/
        , MPI_INT/*datatype*/
        , MPI_ANY_SOURCE/*source*/
        , 0/*tag*/
        , MCW/*communicator*/
        , MPI_STATUS_IGNORE/*status*/);
  std::cout << "I am " << rank << " of " << size << "; got a message from " << data << std::endl;

  // all MPI programs should end with this
  MPI_Finalize();
  return 0;
} // end main
