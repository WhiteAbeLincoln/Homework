% Created 2020-02-20 Thu 17:31
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../../preamble.tex} \DeclareMathOperator{\cube}{cube}
\author{Abraham White}
\date{\today}
\title{Homework 5 - Parallel Mandelbrot}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Homework 5 - Parallel Mandelbrot},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section{Description}
\label{sec:orga993e7c}
Write a serial program that creates a 512x512-pixel mandelbrot image in ppm format and run it on one of the nodes in the cluster.
Your program should accept three doubles as input: \(Re_0\), \(Im_0\), and \(Re_1\). \(Im_1\) should be calculated as
\(Im_0 + (Re_1-Re_0)\) (i.e. you should generate a square mandelbrot image). Pick a nice color scheme for your Mandelbrot images,
and look at a few of them, just for fun. Get timing results from running your program.
\section{Implementation}
\label{sec:orgb2d223b}
\subsection{Serial Mandelbrot}
\label{sec:orgd93d739}
\begin{quote}
The Mandelbrot set is the set of complex numbers \(c\) for which the function \(f_{c}(z)=z^{2}+c\) does not diverge when
iterated from \(z=0\), i.e., for which the sequence \(f_{c}(0), f_{c}(f_{c}(0))\), etc., remains bounded in absolute value. \\
\url{https://en.wikipedia.org/wiki/Mandelbrot\_set}
\end{quote}

A good starting point will then be to define this mandelbrot function\footnote{See the appendix for the definitions of the \texttt{Complex} struct and the \texttt{cadd} and \texttt{cmult} functions.}:
\begin{minted}[]{c}
Complex mandelbrot_fn(Complex z, Complex c) {
  return c_add(c_mult(z, z), c);
}
\end{minted}

We also need a function that knows when to stop iteration, either after a set maximum number of iterations, or after the 
value has escaped. We know that the value escapes when the its magnitude is greater than or equal to 2, i.e. when
\(\lvert z \rvert \geq 2\).

When drawing the Mandelbrot set, we don't actually care about the value of \(z\) or \(c\), rather, we use the number of iterations
to determine the color of a pixel. Therefore, we want a function that iterates until the stop condition is reached, and then
returns the number of iterations that occured.

\begin{minted}[]{c}
int iterate_until_escape(Complex c, int max_iter) {
  Complex z = { 0, 0 };
  int i = 0;
  // magnitude returns the magnitude squared to avoid
  // using a sqrt, so compare with 4 instead of 2
  while (c_mag(z) < 4 && i < max_iter) {
    z = mandelbrot_fn(z, c);
    i++;
  }
  
  return i;
}
\end{minted}

Now that we have a way to get iteration values, we need to think about how we will draw them. Recall the definition of the the Mandelbrot set
as the numbers \(c\) where the Mandelbrot function does not diverge when iterated. If we want to draw a continuous section of the Mandelbrot set,
it would be good to define the values for \(c\) as a subset of the complex plane, meaning we need two complex numbers to define the bounds.
These two numbers, \(Re_0, Im_0\) and \(Re_1, Im_1\) mentioned in the assignment description, are taken as input to the program.

However, we are drawing the image through calculating the iterations for each discrete pixel in an image with default size 512x512.
Because of this, we also need a way of representing the plane that we draw the image on, and a way of converting between a pixel in our
image plane to a point in our complex plane.

For both of these purposes we can define a \texttt{Plane} struct.
\begin{minted}[]{c}
typedef struct Plane {
  double xmin;
  double xmax;
  double ymin;
  double ymax;
} Plane;
  
double p_width(Plane p) {
  return p.xmax - p.xmin;
}
double p_height(Plane p) {
  return p.ymax - p.ymin;
}
double p_area(Plane p) {
  return p_width(p) * p_height(p);
}
\end{minted}

Now we need a way to transform a point in our image plane (or the real plane) to a complex point in the complex plane.
Since our image plane has an integral width and height, and the minimum for x and y are 0, we don't have to worry about
transforming from an image to a real viewbox. We do the transformation by multiplying the \(x\) coordinate by the ratio
between the widths of the plane and adding that to the minimum \(x\) value for the target plane. We do the same thing for
the \(y\) coordinate. 
\begin{minted}[]{c}
Complex scale_to_plane(Plane imageP, Plane compP, int x, int y) {
  return (Complex){ .r = compP.xmin + x * (p_width(compP) / p_width(imageP)),
                    .i = compP.ymin + y * (p_height(compP) / p_height(imageP)) };
}
\end{minted}

Now we need a way to get a color to draw for a single pixel, a way to get a color\footnote{See the appendix for a data structure to represent RGB colors.} from a number of iterations, and a way to print out
a list of these colors as a ppm file.

First we define our \texttt{color\_for\_pixel} function, which given pixel coordinates \(x,y\), first computes the number of iterations
until escaping for the mandelbrot function, with the scaled pixel coordinates as the value for \(c\). This iteration number is then
given to \texttt{color\_for\_iteration}, which returns a final color value. We pass in configuration data using the \texttt{ProgData} struct.
\begin{minted}[]{c}
Color color_for_pixel(ProgData data, int x, int y) {
  int iterations = iterate_until_escape(scale_to_plane(data.image_plane, data.comp_plane, x, y),
                                        data.max_iter);
  return color_for_iter(data.color_start, data.color_end, iterations, data.max_iter);
}
\end{minted}
Now we define the color from iteration function, which performs a linear interpolation between two colors.
\begin{minted}[]{c}
float lerp(float v0, float v1, float t) {
  return (1-t) * v0 + t * v1;
}
Color color_for_iter(Color c1, Color c2, int iter, int max_iter) {
  fprintf(stderr, "Getting color for %d\n", iter);
  return (Color){
    .r = lerp(c1.r, c2.r, iter/(float)max_iter),
    .g = lerp(c1.g, c2.g, iter/(float)max_iter),
    .b = lerp(c1.b, c2.b, iter/(float)max_iter),
  };
}
\end{minted}
Finally we must print out an array of colors (the result of running \texttt{color\_for\_pixel} on each pixel in the image). We use the ppm image format
for this, which represents pixels as a map of RGB triplets. The image data is printed to standard output.
\begin{minted}[]{c}
void print_ppm(int width, int height, Color* colors) {
  // print the header
  printf("P3\n%d %d\n255\n", width, height);
  int rows = 0;
  for (int i = 0; i < width*height; ++i) {
    Color c = colors[i];
    printf("%4d%4d%4d  ", c.r, c.g, c.b);
    rows++;
    if (rows == width) {
      printf("\n");
      rows = 0;
    }
  }
}
\end{minted}

This is everything we need for a serial Mandelbrot program. However, what we want is a parallel program.

\subsection{Parallel Mandelbrot}
\label{sec:org344fc15}
Our strategy for the parallel mandelbrot could be to divide the image into \(n\) rows, where \(n\) is the
number of processors available to the program. However, this will lead to slowdowns because
the difficulty of processing is not evenly distributed among the image rows. A different strategy might
be a threadpool-like setup, where we divide the work into an arbitrary amount of rows and hand new work
to the processors as they finish their initial assignments. This should ensure that a processor that
takes a long time on some work will not do the majority, since other processors that finish quickly will
be able to start new jobs. The downside to this is that the glorious leader process won't do any
work other than waiting for results.

The leader process needs a way to assign jobs. We will track the rows that are being worked on or complete by the
\texttt{jobs} array. We track the current row assignments by process with the \texttt{assignments} array. To assign a row
to be worked on, we set the process index in the \texttt{assignments} array to the current job, set the row index
in the \texttt{jobs} array to 1, and then send the row to the desired process.
\begin{minted}[]{c}
void assn_job(int job, int process, int* jobs, int* assignments) {
  assignments[process] = job;
  // mark assignment as taken
  jobs[job] = 1;
  MPI_Send(&job, 1, MPI_INT, process, SEND_JOB, MCW);
}
\end{minted}

The next thing the leader needs is a way to receive finished results from the processes. We do this using \texttt{MPI\_Irecv},
which requires some extra machinations. The \texttt{waiting} array tracks processes that have pending receives, the \texttt{flag}
array tests whether a receive was successful, and the \texttt{rq} array stores receive requests for MPI. We store the
results in the \texttt{iter\_data} multidimensional array and increment the \texttt{num\_finished} variable to keep track of the
finished rows. First the leader processes initiate a receive if there isn't one pending. Next the leader uses
\texttt{MPI\_Test} to check if a receive completed, and if it has, unassigns the process, increments the number of finished
tasks, and removes the process from the pending receive array. 
\begin{minted}[]{c}
void recv_job(int node,
              int width,
              int* assignments,
              int* waiting,
              int* flag,
              MPI_Request* rq,
              int** iter_data,
              int* num_finished) {
  // if we haven't already initiated a recv
  if (waiting[node] == 0) {
    // initiate it
    MPI_Irecv(iter_data[assignments[node]], width, MPI_INT, node, JOB_FINISHED, MCW, &rq[node]);
    waiting[node] = 1;
  }

  MPI_Test(rq + node, &flag[node], MPI_STATUS_IGNORE);
  if (flag[node]) {
    assignments[node] = -1;
    waiting[node] = 0;
    (*num_finished)++;
  }
}
\end{minted}

Here we begin the glorious leader's dispatch function, where workers are assigned jobs. Each job is simply
a single row of the image. We start with some required variable declarations and initializations.
\begin{minted}[]{c}
void dispatch(ProgData data, int size) {
  int height = p_height(data.image_plane);
  int width = p_width(data.image_plane);
  int* jobs = malloc(height * sizeof(int));
  int* assignments = malloc((size - 1) * sizeof(int));
  int* waiting = malloc((size - 1) * sizeof(int));
  int* flag = malloc((size - 1) * sizeof(int));
  MPI_Request* rq = malloc((size - 1) * sizeof(MPI_Request));
  int** iter_data = malloc(height * sizeof(int*));
  int num_finished = 0;
  
  for (int i = 0; i < height; ++i) {
    iter_data[i] = malloc(width*sizeof(int));
    jobs[i] = 0;
  }

  // keep track of which process is working on which row
  for (int i = 1; i < size; ++i) {
    assignments[i] = -1;
    waiting[i] = 0;
    flag[i] = 0;
  }
\end{minted}

Here is the dispatch part of the dispatch function. While we have incomplete rows, we loop over the other processes,
initiating a job receive if the process has an assignment, otherwise assigning the first available job to that
process.
\begin{minted}[]{c}
  while (num_finished != height) {
    for (int node=1; node < size; ++node) {
      // if the node has a job
      if (assignments[node] != -1) {
        recv_job(node, width, assignments, waiting,
                 flag, rq, iter_data, &num_finished);
      } else {
        // find an open job
        for (int i = 0; i < height; ++i) {
          if (jobs[i] == 0) {
            assn_job(i, node, jobs, assignments);
            break;
          }
        }
      }  
    }
  }
\end{minted}

After all jobs have been completed, we have to let the other processes know that they can
stop waiting for new requests. We do this by sending the special value -1 as a job assignment,
since -1 is not a valid row id.
\begin{minted}[]{c}
  int termval = -1;
  for (int node=1; node < size; ++node) {
    MPI_Send(&termval, 1, MPI_INT, node, SEND_JOB, MCW);
  }
\end{minted}

Finally, the master process collects all of the iteration arrays into a color data array,
which is then printed as a ppm to standard out.
\begin{minted}[]{c}
  Color* color_data = malloc((width * height) * sizeof(Color));
  for (int x = 0; x < width; ++x) {
    for (int y = 0; y < height; ++y) {
      color_data[width * x + y] = color_for_iter(data.color_start,
                                                  data.color_end,
                                                  iter_data[y][x],
                                                  data.max_iter);
    }
  }
  
  print_ppm(width, height, color_data);
}
\end{minted}

Here is the definition of a worker job. The worker first receives a row to work on from the leader
process. If the row is valid, the worker then gets the number of iterations until escape for every
pixel in that row. Finally the worker sends back the list of iterations to the leader.
\begin{minted}[]{c}
bool do_job(ProgData data, int rank) {
  int y;
  int width = p_width(data.image_plane);
  int* iter_arr = malloc(width * sizeof(int));
  MPI_Recv(&y, 1, MPI_INT, 0, SEND_JOB, MCW, MPI_STATUS_IGNORE);
  if (y == -1) return false;
  fprintf(stderr, "Process %d got job %d\n", rank, y);
  
  for (int x = 0; x < width; ++x) {
    iter_arr[x] = iterate_until_escape(
                       scale_to_plane(data.image_plane, data.comp_plane, x, y),
                       data.max_iter
                    );
  }
  
  fprintf(stderr, "Process %d finished job %d\n", rank, y);
  MPI_Send(iter_arr, width, MPI_INT, 0, JOB_FINISHED, MCW);
  
  return true;
}
\end{minted}

In the main function we decide on what role a process gets by the rank. The worker process runs the
job function until it is told to terminate by the leader process.
\begin{minted}[]{c}
if (rank == 0) {
  dispatch(data, size);
} else {
  bool incomplete = true;
  while (incomplete) {
    incomplete = do_job(data, rank);
  }
  fprintf(stderr, "Process %d terminating\n", rank);
}
\end{minted}

\section{Building and Running}
\label{sec:org5525565}
Build a file with \texttt{mpicc -Wall -Wextra -Werror -o mandelbrot mandelbrot.c -lm}, and run with \\
\texttt{mpirun -np <num\_processes> ./mandelbrot <Re0> <Re1> <Im0>}
\begin{minted}[]{sh}
    mpicc -Wall -Wextra -Werror -o mandelbrot ./mandelbrot.c -lm
    mpirun -np 8 --use-hwthread-cpus ./mandelbrot 1 1 -2
\end{minted}

\section{Appendix}
\label{sec:orgff56373}
\subsection{Source Code}
\label{sec:orgeeaa984}
\subsubsection{Complex}
\label{sec:org57a8c9d}
Defines the complex struct and mathematical functions operating on it.
\begin{minted}[]{c}
typedef struct Complex {
  double r;
  double i;
} Complex;
  
Complex c_add(Complex x, Complex y) {
  return (Complex){ .r = x.r + y.r, .i = x.i + y.i };
}

Complex c_mult(Complex x, Complex y) {
  return (Complex){ .r = x.r*y.r - x.i*y.i, .i = x.r*y.i + x.i*y.r };
}

// returns the magnitude squared
double c_mag(Complex x) {
  return x.r*x.r + x.i*x.i;
}
\end{minted}
\subsubsection{Plane}
\label{sec:org0c45ead}
Defines a data structure for planes and related functions.
\begin{minted}[]{c}
typedef struct Plane {
  double xmin;
  double xmax;
  double ymin;
  double ymax;
} Plane;
  
double p_width(Plane p) {
  return p.xmax - p.xmin;
}
double p_height(Plane p) {
  return p.ymax - p.ymin;
}
double p_area(Plane p) {
  return p_width(p) * p_height(p);
}
\end{minted}
\subsubsection{Color}
\label{sec:orge6b0b9c}
Defines a data structure for RGB colors.
\begin{minted}[]{c}
typedef struct Color {
  int r;
  int g;
  int b;
} Color;
\end{minted}
\subsubsection{ProgData}
\label{sec:org014c3b9}
A data structure for convenient passing of configuration data.
\begin{minted}[]{c}
typedef struct ProgData {
  Plane image_plane;
  Plane comp_plane;
  Color color_start;
  Color color_end;
  int max_iter;
} ProgData;
\end{minted}
\subsubsection{Messages}
\label{sec:org0e7f80c}
Enum for job pool messages
\begin{minted}[]{c}
typedef enum Messages {
  JOB_FINISHED,
  SEND_JOB,
} Messages;
\end{minted}
\subsubsection{Help}
\label{sec:org695429f}
Function that prints out help string and exits.
\begin{minted}[]{c}
void help(char* name) {
  printf(
    "Mandelbrot\n"
    "Usage:\t %s -h|--help\n"
    "\t %s [options]\n"
    "Options:\n"
    "\t-h\tShow this screen\n"
    "\t-i <iter>\tNumber of iterations\n"
    "\t-s <r> <g> <b>\tStart Color\n"
    "\t-e <r> <g> <b>\tEnd Color\n"
    "\t-c <r1> <i1> <r2> [i2]\tDefine complex plane\n"
    "\t-d <xmax> [ymax]\tDefine image dimensions\n",
    name, name);
}
\end{minted}
\subsubsection{Program Configuration}
\label{sec:orge0ccd0e}
\begin{minted}[]{c}
  Complex c1 = { .r = -3, .i = -3 };
  Complex c2 = { .r = 0, .i = 0 };
  Plane image = { .xmin = 0, .xmax = 512, .ymin = 0, .ymax = 512 };
  Color color_start = {0, 0, 0};
  Color color_end = {255, 255, 255};
  int iter_max = 30;

  int opt;
  while ((opt = getopt(argc, argv, "hi:s:e:c:d:")) != -1) {
    switch (opt) {
      case 'h':
        help(argv[0]);
        return 0;
      case 'i':
        iter_max = atoi(optarg);
        break;
      case 's': {
        int r,g,b;
        int read = sscanf(optarg, "%u %u %u", &r, &g, &b);
        if (read < 3) {
          fprintf(stderr, "Invalid color\n");
          exit(EXIT_FAILURE);
        }
        color_start.r = r;
        color_start.g = g;
        color_start.b = b;
        break;
      }
      case 'e': {
        int r,g,b;
        int read = sscanf(optarg, "%u %u %u", &r, &g, &b);
        if (read < 3) {
          fprintf(stderr, "Invalid color\n");
          exit(EXIT_FAILURE);
        }
        color_end.r = r;
        color_end.g = g;
        color_end.b = b;
        break;
      }
      case 'c': {
        double r1, i1, r2, i2;
        int read = sscanf(optarg, "%lf %lf %lf %lf", &r1, &i1, &r2, &i2);
        if (read < 3) {
          fprintf(stderr, "Invalid complex coordinates\n");
          exit(EXIT_FAILURE);
        }
        c1.r = r1;
        c1.i = i1;
        c2.r = r2;
        c2.i = (read < 4) ? c1.i + (c2.r - c1.r) : i2;
        break;
      }
      case 'd': {
        int xmax;
        int ymax;
        int read = sscanf(optarg, "%u %u", &xmax, &ymax);
        if (read < 1) {
          fprintf(stderr, "Invalid image dimensions\n");
          exit(EXIT_FAILURE);
        }
        image.xmax = xmax;
        image.ymax = (read == 1) ? xmax : ymax;
        break;
      }
      default:
        help(argv[0]);
        exit(EXIT_FAILURE);
    }
  }
  
  Plane complex = { .xmin = c1.r, .xmax = c2.r - c1.r, .ymin = c1.i, .ymax = c2.i - c1.i };
  ProgData data = {image, complex, color_start, color_end, iter_max};
\end{minted}
\subsubsection{Includes}
\label{sec:org6428b73}
\begin{minted}[]{c}
#include <stdlib.h>
#include <unistd.h> /* unix stdlib, provides access to posix api */
#include <stdio.h> /* standard input output */
#include <stdbool.h> /* c doesn't import booleans by default. I could just use 1 */
#include <math.h>
#include <string.h>
\end{minted}
\subsubsection{mandelbrot.c}
\label{sec:org7ba10ac}
\begin{minted}[]{c}
#include <stdlib.h>
#include <unistd.h> /* unix stdlib, provides access to posix api */
#include <stdio.h> /* standard input output */
#include <stdbool.h> /* c doesn't import booleans by default. I could just use 1 */
#include <math.h>
#include <string.h>
#include <mpi.h>

#define MCW MPI_COMM_WORLD

typedef struct Complex {
  double r;
  double i;
} Complex;
  
Complex c_add(Complex x, Complex y) {
  return (Complex){ .r = x.r + y.r, .i = x.i + y.i };
}

Complex c_mult(Complex x, Complex y) {
  return (Complex){ .r = x.r*y.r - x.i*y.i, .i = x.r*y.i + x.i*y.r };
}

// returns the magnitude squared
double c_mag(Complex x) {
  return x.r*x.r + x.i*x.i;
}
typedef struct Plane {
  double xmin;
  double xmax;
  double ymin;
  double ymax;
} Plane;
  
double p_width(Plane p) {
  return p.xmax - p.xmin;
}
double p_height(Plane p) {
  return p.ymax - p.ymin;
}
double p_area(Plane p) {
  return p_width(p) * p_height(p);
}
typedef struct Color {
  int r;
  int g;
  int b;
} Color;
typedef struct ProgData {
  Plane image_plane;
  Plane comp_plane;
  Color color_start;
  Color color_end;
  int max_iter;
} ProgData;
typedef enum Messages {
  JOB_FINISHED,
  SEND_JOB,
} Messages;

Complex mandelbrot_fn(Complex z, Complex c) {
  return c_add(c_mult(z, z), c);
}
int iterate_until_escape(Complex c, int max_iter) {
  Complex z = { 0, 0 };
  int i = 0;
  // magnitude returns the magnitude squared to avoid
  // using a sqrt, so compare with 4 instead of 2
  while (c_mag(z) < 4 && i < max_iter) {
    z = mandelbrot_fn(z, c);
    i++;
  }
  
  return i;
}
Complex scale_to_plane(Plane imageP, Plane compP, int x, int y) {
  return (Complex){ .r = compP.xmin + x * (p_width(compP) / p_width(imageP)),
                    .i = compP.ymin + y * (p_height(compP) / p_height(imageP)) };
}
float lerp(float v0, float v1, float t) {
  return (1-t) * v0 + t * v1;
}
Color color_for_iter(Color c1, Color c2, int iter, int max_iter) {
  fprintf(stderr, "Getting color for %d\n", iter);
  return (Color){
    .r = lerp(c1.r, c2.r, iter/(float)max_iter),
    .g = lerp(c1.g, c2.g, iter/(float)max_iter),
    .b = lerp(c1.b, c2.b, iter/(float)max_iter),
  };
}
Color color_for_pixel(ProgData data, int x, int y) {
  int iterations = iterate_until_escape(scale_to_plane(data.image_plane, data.comp_plane, x, y),
                                        data.max_iter);
  return color_for_iter(data.color_start, data.color_end, iterations, data.max_iter);
}
void print_ppm(int width, int height, Color* colors) {
  // print the header
  printf("P3\n%d %d\n255\n", width, height);
  int rows = 0;
  for (int i = 0; i < width*height; ++i) {
    Color c = colors[i];
    printf("%4d%4d%4d  ", c.r, c.g, c.b);
    rows++;
    if (rows == width) {
      printf("\n");
      rows = 0;
    }
  }
}
void help(char* name) {
  printf(
    "Mandelbrot\n"
    "Usage:\t %s -h|--help\n"
    "\t %s [options]\n"
    "Options:\n"
    "\t-h\tShow this screen\n"
    "\t-i <iter>\tNumber of iterations\n"
    "\t-s <r> <g> <b>\tStart Color\n"
    "\t-e <r> <g> <b>\tEnd Color\n"
    "\t-c <r1> <i1> <r2> [i2]\tDefine complex plane\n"
    "\t-d <xmax> [ymax]\tDefine image dimensions\n",
    name, name);
}

void recv_job(int node,
              int width,
              int* assignments,
              int* waiting,
              int* flag,
              MPI_Request* rq,
              int** iter_data,
              int* num_finished) {
  // if we haven't already initiated a recv
  if (waiting[node] == 0) {
    // initiate it
    MPI_Irecv(iter_data[assignments[node]], width, MPI_INT, node, JOB_FINISHED, MCW, &rq[node]);
    waiting[node] = 1;
  }

  MPI_Test(rq + node, &flag[node], MPI_STATUS_IGNORE);
  if (flag[node]) {
    assignments[node] = -1;
    waiting[node] = 0;
    (*num_finished)++;
  }
}
void assn_job(int job, int process, int* jobs, int* assignments) {
  assignments[process] = job;
  // mark assignment as taken
  jobs[job] = 1;
  MPI_Send(&job, 1, MPI_INT, process, SEND_JOB, MCW);
}
void dispatch(ProgData data, int size) {
  int height = p_height(data.image_plane);
  int width = p_width(data.image_plane);
  int* jobs = malloc(height * sizeof(int));
  int* assignments = malloc((size - 1) * sizeof(int));
  int* waiting = malloc((size - 1) * sizeof(int));
  int* flag = malloc((size - 1) * sizeof(int));
  MPI_Request* rq = malloc((size - 1) * sizeof(MPI_Request));
  int** iter_data = malloc(height * sizeof(int*));
  int num_finished = 0;
  
  for (int i = 0; i < height; ++i) {
    iter_data[i] = malloc(width*sizeof(int));
    jobs[i] = 0;
  }

  // keep track of which process is working on which row
  for (int i = 1; i < size; ++i) {
    assignments[i] = -1;
    waiting[i] = 0;
    flag[i] = 0;
  }
  while (num_finished != height) {
    for (int node=1; node < size; ++node) {
      // if the node has a job
      if (assignments[node] != -1) {
        recv_job(node, width, assignments, waiting,
                 flag, rq, iter_data, &num_finished);
      } else {
        // find an open job
        for (int i = 0; i < height; ++i) {
          if (jobs[i] == 0) {
            assn_job(i, node, jobs, assignments);
            break;
          }
        }
      }  
    }
  }
  int termval = -1;
  for (int node=1; node < size; ++node) {
    MPI_Send(&termval, 1, MPI_INT, node, SEND_JOB, MCW);
  }
  Color* color_data = malloc((width * height) * sizeof(Color));
  for (int x = 0; x < width; ++x) {
    for (int y = 0; y < height; ++y) {
      color_data[width * x + y] = color_for_iter(data.color_start,
                                                  data.color_end,
                                                  iter_data[y][x],
                                                  data.max_iter);
    }
  }
  
  print_ppm(width, height, color_data);
}

bool do_job(ProgData data, int rank) {
  int y;
  int width = p_width(data.image_plane);
  int* iter_arr = malloc(width * sizeof(int));
  MPI_Recv(&y, 1, MPI_INT, 0, SEND_JOB, MCW, MPI_STATUS_IGNORE);
  if (y == -1) return false;
  fprintf(stderr, "Process %d got job %d\n", rank, y);
  
  for (int x = 0; x < width; ++x) {
    iter_arr[x] = iterate_until_escape(
                       scale_to_plane(data.image_plane, data.comp_plane, x, y),
                       data.max_iter
                    );
  }
  
  fprintf(stderr, "Process %d finished job %d\n", rank, y);
  MPI_Send(iter_arr, width, MPI_INT, 0, JOB_FINISHED, MCW);
  
  return true;
}

int main(int argc, char** argv) {
    Complex c1 = { .r = -3, .i = -3 };
    Complex c2 = { .r = 0, .i = 0 };
    Plane image = { .xmin = 0, .xmax = 512, .ymin = 0, .ymax = 512 };
    Color color_start = {0, 0, 0};
    Color color_end = {255, 255, 255};
    int iter_max = 30;
  
    int opt;
    while ((opt = getopt(argc, argv, "hi:s:e:c:d:")) != -1) {
      switch (opt) {
        case 'h':
          help(argv[0]);
          return 0;
        case 'i':
          iter_max = atoi(optarg);
          break;
        case 's': {
          int r,g,b;
          int read = sscanf(optarg, "%u %u %u", &r, &g, &b);
          if (read < 3) {
            fprintf(stderr, "Invalid color\n");
            exit(EXIT_FAILURE);
          }
          color_start.r = r;
          color_start.g = g;
          color_start.b = b;
          break;
        }
        case 'e': {
          int r,g,b;
          int read = sscanf(optarg, "%u %u %u", &r, &g, &b);
          if (read < 3) {
            fprintf(stderr, "Invalid color\n");
            exit(EXIT_FAILURE);
          }
          color_end.r = r;
          color_end.g = g;
          color_end.b = b;
          break;
        }
        case 'c': {
          double r1, i1, r2, i2;
          int read = sscanf(optarg, "%lf %lf %lf %lf", &r1, &i1, &r2, &i2);
          if (read < 3) {
            fprintf(stderr, "Invalid complex coordinates\n");
            exit(EXIT_FAILURE);
          }
          c1.r = r1;
          c1.i = i1;
          c2.r = r2;
          c2.i = (read < 4) ? c1.i + (c2.r - c1.r) : i2;
          break;
        }
        case 'd': {
          int xmax;
          int ymax;
          int read = sscanf(optarg, "%u %u", &xmax, &ymax);
          if (read < 1) {
            fprintf(stderr, "Invalid image dimensions\n");
            exit(EXIT_FAILURE);
          }
          image.xmax = xmax;
          image.ymax = (read == 1) ? xmax : ymax;
          break;
        }
        default:
          help(argv[0]);
          exit(EXIT_FAILURE);
      }
    }
    
    Plane complex = { .xmin = c1.r, .xmax = c2.r - c1.r, .ymin = c1.i, .ymax = c2.i - c1.i };
    ProgData data = {image, complex, color_start, color_end, iter_max};
  int rank, size;
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);
  if (rank == 0) {
    dispatch(data, size);
  } else {
    bool incomplete = true;
    while (incomplete) {
      incomplete = do_job(data, rank);
    }
    fprintf(stderr, "Process %d terminating\n", rank);
  }
  
  MPI_Finalize();
  return 0;
}
\end{minted}

\subsubsection{serialMandelbrot.c}
\label{sec:org725cd0e}
\begin{minted}[]{c}
#include <stdlib.h>
#include <unistd.h> /* unix stdlib, provides access to posix api */
#include <stdio.h> /* standard input output */
#include <stdbool.h> /* c doesn't import booleans by default. I could just use 1 */
#include <math.h>
#include <string.h>

typedef struct Complex {
  double r;
  double i;
} Complex;
  
Complex c_add(Complex x, Complex y) {
  return (Complex){ .r = x.r + y.r, .i = x.i + y.i };
}

Complex c_mult(Complex x, Complex y) {
  return (Complex){ .r = x.r*y.r - x.i*y.i, .i = x.r*y.i + x.i*y.r };
}

// returns the magnitude squared
double c_mag(Complex x) {
  return x.r*x.r + x.i*x.i;
}
typedef struct Plane {
  double xmin;
  double xmax;
  double ymin;
  double ymax;
} Plane;
  
double p_width(Plane p) {
  return p.xmax - p.xmin;
}
double p_height(Plane p) {
  return p.ymax - p.ymin;
}
double p_area(Plane p) {
  return p_width(p) * p_height(p);
}
typedef struct Color {
  int r;
  int g;
  int b;
} Color;
typedef struct ProgData {
  Plane image_plane;
  Plane comp_plane;
  Color color_start;
  Color color_end;
  int max_iter;
} ProgData;
typedef enum Messages {
  JOB_FINISHED,
  SEND_JOB,
} Messages;

Complex mandelbrot_fn(Complex z, Complex c) {
  return c_add(c_mult(z, z), c);
}
int iterate_until_escape(Complex c, int max_iter) {
  Complex z = { 0, 0 };
  int i = 0;
  // magnitude returns the magnitude squared to avoid
  // using a sqrt, so compare with 4 instead of 2
  while (c_mag(z) < 4 && i < max_iter) {
    z = mandelbrot_fn(z, c);
    i++;
  }
  
  return i;
}
Complex scale_to_plane(Plane imageP, Plane compP, int x, int y) {
  return (Complex){ .r = compP.xmin + x * (p_width(compP) / p_width(imageP)),
                    .i = compP.ymin + y * (p_height(compP) / p_height(imageP)) };
}
float lerp(float v0, float v1, float t) {
  return (1-t) * v0 + t * v1;
}
Color color_for_iter(Color c1, Color c2, int iter, int max_iter) {
  fprintf(stderr, "Getting color for %d\n", iter);
  return (Color){
    .r = lerp(c1.r, c2.r, iter/(float)max_iter),
    .g = lerp(c1.g, c2.g, iter/(float)max_iter),
    .b = lerp(c1.b, c2.b, iter/(float)max_iter),
  };
}
Color color_for_pixel(ProgData data, int x, int y) {
  int iterations = iterate_until_escape(scale_to_plane(data.image_plane, data.comp_plane, x, y),
                                        data.max_iter);
  return color_for_iter(data.color_start, data.color_end, iterations, data.max_iter);
}
void print_ppm(int width, int height, Color* colors) {
  // print the header
  printf("P3\n%d %d\n255\n", width, height);
  int rows = 0;
  for (int i = 0; i < width*height; ++i) {
    Color c = colors[i];
    printf("%4d%4d%4d  ", c.r, c.g, c.b);
    rows++;
    if (rows == width) {
      printf("\n");
      rows = 0;
    }
  }
}
void help(char* name) {
  printf(
    "Mandelbrot\n"
    "Usage:\t %s -h|--help\n"
    "\t %s [options]\n"
    "Options:\n"
    "\t-h\tShow this screen\n"
    "\t-i <iter>\tNumber of iterations\n"
    "\t-s <r> <g> <b>\tStart Color\n"
    "\t-e <r> <g> <b>\tEnd Color\n"
    "\t-c <r1> <i1> <r2> [i2]\tDefine complex plane\n"
    "\t-d <xmax> [ymax]\tDefine image dimensions\n",
    name, name);
}

int main(int argc, char** argv) {
    Complex c1 = { .r = -3, .i = -3 };
    Complex c2 = { .r = 0, .i = 0 };
    Plane image = { .xmin = 0, .xmax = 512, .ymin = 0, .ymax = 512 };
    Color color_start = {0, 0, 0};
    Color color_end = {255, 255, 255};
    int iter_max = 30;
  
    int opt;
    while ((opt = getopt(argc, argv, "hi:s:e:c:d:")) != -1) {
      switch (opt) {
        case 'h':
          help(argv[0]);
          return 0;
        case 'i':
          iter_max = atoi(optarg);
          break;
        case 's': {
          int r,g,b;
          int read = sscanf(optarg, "%u %u %u", &r, &g, &b);
          if (read < 3) {
            fprintf(stderr, "Invalid color\n");
            exit(EXIT_FAILURE);
          }
          color_start.r = r;
          color_start.g = g;
          color_start.b = b;
          break;
        }
        case 'e': {
          int r,g,b;
          int read = sscanf(optarg, "%u %u %u", &r, &g, &b);
          if (read < 3) {
            fprintf(stderr, "Invalid color\n");
            exit(EXIT_FAILURE);
          }
          color_end.r = r;
          color_end.g = g;
          color_end.b = b;
          break;
        }
        case 'c': {
          double r1, i1, r2, i2;
          int read = sscanf(optarg, "%lf %lf %lf %lf", &r1, &i1, &r2, &i2);
          if (read < 3) {
            fprintf(stderr, "Invalid complex coordinates\n");
            exit(EXIT_FAILURE);
          }
          c1.r = r1;
          c1.i = i1;
          c2.r = r2;
          c2.i = (read < 4) ? c1.i + (c2.r - c1.r) : i2;
          break;
        }
        case 'd': {
          int xmax;
          int ymax;
          int read = sscanf(optarg, "%u %u", &xmax, &ymax);
          if (read < 1) {
            fprintf(stderr, "Invalid image dimensions\n");
            exit(EXIT_FAILURE);
          }
          image.xmax = xmax;
          image.ymax = (read == 1) ? xmax : ymax;
          break;
        }
        default:
          help(argv[0]);
          exit(EXIT_FAILURE);
      }
    }
    
    Plane complex = { .xmin = c1.r, .xmax = c2.r - c1.r, .ymin = c1.i, .ymax = c2.i - c1.i };
    ProgData data = {image, complex, color_start, color_end, iter_max};
      
  int width = p_width(image);
  int height = p_height(image);
  Color* color_data = malloc((width * height) * sizeof(Color));
  for (int x = 0; x < width; ++x) {
    for (int y = 0; y < height; ++y) {
      color_data[width * x + y] = color_for_pixel(data, x, y);
    }
  }
  
  print_ppm(width, height, color_data);
  return 0;
}
\end{minted}
\end{document}