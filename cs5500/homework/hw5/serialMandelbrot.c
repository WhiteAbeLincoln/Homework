#include <stdlib.h>
#include <unistd.h> /* unix stdlib, provides access to posix api */
#include <stdio.h> /* standard input output */
#include <stdbool.h> /* c doesn't import booleans by default. I could just use 1 */
#include <math.h>
#include <string.h>

typedef struct Complex {
  double r;
  double i;
} Complex;
  
Complex c_add(Complex x, Complex y) {
  return (Complex){ .r = x.r + y.r, .i = x.i + y.i };
}

Complex c_mult(Complex x, Complex y) {
  return (Complex){ .r = x.r*y.r - x.i*y.i, .i = x.r*y.i + x.i*y.r };
}

// returns the magnitude squared
double c_mag(Complex x) {
  return x.r*x.r + x.i*x.i;
}
typedef struct Plane {
  double xmin;
  double xmax;
  double ymin;
  double ymax;
} Plane;
  
double p_width(Plane p) {
  return p.xmax - p.xmin;
}
double p_height(Plane p) {
  return p.ymax - p.ymin;
}
double p_area(Plane p) {
  return p_width(p) * p_height(p);
}
typedef struct Color {
  int r;
  int g;
  int b;
} Color;
typedef struct ProgData {
  Plane image_plane;
  Plane comp_plane;
  Color color_start;
  Color color_end;
  int max_iter;
} ProgData;
typedef enum Messages {
  JOB_FINISHED,
  SEND_JOB,
} Messages;

Complex mandelbrot_fn(Complex z, Complex c) {
  return c_add(c_mult(z, z), c);
}
int iterate_until_escape(Complex c, int max_iter) {
  Complex z = { 0, 0 };
  int i = 0;
  // magnitude returns the magnitude squared to avoid
  // using a sqrt, so compare with 4 instead of 2
  while (c_mag(z) < 4 && i < max_iter) {
    z = mandelbrot_fn(z, c);
    i++;
  }
  
  return i;
}
Complex scale_to_plane(Plane imageP, Plane compP, int x, int y) {
  return (Complex){ .r = compP.xmin + x * (p_width(compP) / p_width(imageP)),
                    .i = compP.ymin + y * (p_height(compP) / p_height(imageP)) };
}
float lerp(float v0, float v1, float t) {
  return (1-t) * v0 + t * v1;
}
Color color_for_iter(Color c1, Color c2, int iter, int max_iter) {
  fprintf(stderr, "Getting color for %d\n", iter);
  return (Color){
    .r = lerp(c1.r, c2.r, iter/(float)max_iter),
    .g = lerp(c1.g, c2.g, iter/(float)max_iter),
    .b = lerp(c1.b, c2.b, iter/(float)max_iter),
  };
}
Color color_for_pixel(ProgData data, int x, int y) {
  int iterations = iterate_until_escape(scale_to_plane(data.image_plane, data.comp_plane, x, y),
                                        data.max_iter);
  return color_for_iter(data.color_start, data.color_end, iterations, data.max_iter);
}
void print_ppm(int width, int height, Color* colors) {
  // print the header
  printf("P3\n%d %d\n255\n", width, height);
  int rows = 0;
  for (int i = 0; i < width*height; ++i) {
    Color c = colors[i];
    printf("%4d%4d%4d  ", c.r, c.g, c.b);
    rows++;
    if (rows == width) {
      printf("\n");
      rows = 0;
    }
  }
}
void help(char* name) {
  printf(
    "Mandelbrot\n"
    "Usage:\t %s -h|--help\n"
    "\t %s [options]\n"
    "Options:\n"
    "\t-h\tShow this screen\n"
    "\t-i <iter>\tNumber of iterations\n"
    "\t-s <r> <g> <b>\tStart Color\n"
    "\t-e <r> <g> <b>\tEnd Color\n"
    "\t-c <r1> <i1> <r2> [i2]\tDefine complex plane\n"
    "\t-d <xmax> [ymax]\tDefine image dimensions\n",
    name, name);
}

int main(int argc, char** argv) {
    Complex c1 = { .r = -3, .i = -3 };
    Complex c2 = { .r = 0, .i = 0 };
    Plane image = { .xmin = 0, .xmax = 512, .ymin = 0, .ymax = 512 };
    Color color_start = {0, 0, 0};
    Color color_end = {255, 255, 255};
    int iter_max = 30;
  
    int opt;
    while ((opt = getopt(argc, argv, "hi:s:e:c:d:")) != -1) {
      switch (opt) {
        case 'h':
          help(argv[0]);
          return 0;
        case 'i':
          iter_max = atoi(optarg);
          break;
        case 's': {
          int r,g,b;
          int read = sscanf(optarg, "%u %u %u", &r, &g, &b);
          if (read < 3) {
            fprintf(stderr, "Invalid color\n");
            exit(EXIT_FAILURE);
          }
          color_start.r = r;
          color_start.g = g;
          color_start.b = b;
          break;
        }
        case 'e': {
          int r,g,b;
          int read = sscanf(optarg, "%u %u %u", &r, &g, &b);
          if (read < 3) {
            fprintf(stderr, "Invalid color\n");
            exit(EXIT_FAILURE);
          }
          color_end.r = r;
          color_end.g = g;
          color_end.b = b;
          break;
        }
        case 'c': {
          double r1, i1, r2, i2;
          int read = sscanf(optarg, "%lf %lf %lf %lf", &r1, &i1, &r2, &i2);
          if (read < 3) {
            fprintf(stderr, "Invalid complex coordinates\n");
            exit(EXIT_FAILURE);
          }
          c1.r = r1;
          c1.i = i1;
          c2.r = r2;
          c2.i = (read < 4) ? c1.i + (c2.r - c1.r) : i2;
          break;
        }
        case 'd': {
          int xmax;
          int ymax;
          int read = sscanf(optarg, "%u %u", &xmax, &ymax);
          if (read < 1) {
            fprintf(stderr, "Invalid image dimensions\n");
            exit(EXIT_FAILURE);
          }
          image.xmax = xmax;
          image.ymax = (read == 1) ? xmax : ymax;
          break;
        }
        default:
          help(argv[0]);
          exit(EXIT_FAILURE);
      }
    }
    
    Plane complex = { .xmin = c1.r, .xmax = c2.r - c1.r, .ymin = c1.i, .ymax = c2.i - c1.i };
    ProgData data = {image, complex, color_start, color_end, iter_max};
      
  int width = p_width(image);
  int height = p_height(image);
  Color* color_data = malloc((width * height) * sizeof(Color));
  for (int x = 0; x < width; ++x) {
    for (int y = 0; y < height; ++y) {
      color_data[width * x + y] = color_for_pixel(data, x, y);
    }
  }
  
  print_ppm(width, height, color_data);
  return 0;
}
