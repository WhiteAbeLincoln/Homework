#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <time.h>
typedef unsigned int uint;
bool VERBOSE = false;
// see https://stackoverflow.com/a/29378380
static unsigned long int strtosubrange(const char* s,
                                       char** endptr,
                                       int base,
                                       unsigned long int min,
                                       unsigned long int max) {
  unsigned long int y = strtoul(s, endptr, base);
  if (y > max) {
    errno = ERANGE;
    return max;
  }
  if (y < min) {
    errno = ERANGE;
    return min;
  }
  return y;
}
uint strtoui(const char* s, char** endptr, int base) {
  #if UINT_MAX == ULONG_MAX
    return (uint) strtol(s, endptr, base);
  #else
    return (uint) strtosubrange(s, endptr, base, 0, UINT_MAX);
  #endif
}
// https://stackoverflow.com/a/1157217
int msleep(long msec) {
  struct timespec ts;
  int res;

  if (msec < 0) {
      errno = EINVAL;
      return -1;
  }

  ts.tv_sec = msec / 1000;
  ts.tv_nsec = (msec % 1000) * 1000000;

  do {
      res = nanosleep(&ts, &ts);
  } while (res && errno == EINTR);

  return res;
}
void help(const char* name) {
  printf(
    "Game of Life\n"
    "Usage:\t %s [OPTIONS]\n"
    "Options:\n"
    "\t-h\tShow this screen\n"
    "\t-s <uint>\tRandom seed [default: time(0)]\n"
    "\t-d <xmax> [ymax]\tDefine board dimensions [default: 1024]\n"
    "\t-i <uint> \tMaximum number of generations [default: -1]\n"
    "\t-n \tDisable game board overwrite [default: false]\n"
    "\t-f <FILE> \tRead initial board from a file. Uses stdin if FILE is - [default: NULL]\n"
    "\t-t <uint> \tTime between ticks in milliseconds. [default: 1000]\n"
    "\t-m\tEnable timing metrics. Metrics are output on stderr [default: false]\n",
    name);
}
typedef struct Env {
  bool n;
  bool ne;
  bool e;
  bool se;
  bool s;
  bool sw;
  bool w;
  bool nw;
} Env;
uint num_live(Env e) {
  return (uint)e.n
       + (uint)e.ne
       + (uint)e.e
       + (uint)e.se
       + (uint)e.s
       + (uint)e.sw
       + (uint)e.w
       + (uint)e.nw;
}
int get_x(int v, int w) { return v % w; }
int get_y(int v, int w) { return v / w; }
int get_idx(uint x, uint y, uint w) { return x + y*w; }
Env get_neighbors(uint i, bool* cells, uint width, uint height) {
  int x = get_x(i, width); int y = get_y(i, width);
  
  int L  = x-1;
  uint R  = x+1;
  int U  = y-1;
  uint D  = y+1;
  
  return (Env){ .n  =               U < 0       ? false : cells[get_idx(x, U, width)],
                .s  =               D >= height ? false : cells[get_idx(x, D, width)],
                .e  = R >= width                ? false : cells[get_idx(R, y, width)],
                .w  = L < 0                     ? false : cells[get_idx(L, y, width)],
                .ne = R >= width || U < 0       ? false : cells[get_idx(R, U, width)],
                .se = R >= width || D >= height ? false : cells[get_idx(R, D, width)],
                .nw = L < 0      || U < 0       ? false : cells[get_idx(L, U, width)],
                .sw = L < 0      || D >= height ? false : cells[get_idx(L, D, width)]};
}
double rand_dbl() { return rand() / (double)RAND_MAX; }
bool rand_prob(double range) { return rand_dbl() <= range ? true : false; }
bool rules(bool cell, Env env) {
  int n = num_live(env);
  if (cell && (n == 2 || n == 3)) return true;
  if (!cell && n == 3) return true;
  return false;
}
bool fprint(bool* grid, uint width, uint height, bool overwrite, FILE* fp) {
  uint dead = 0;
  for (uint y = 0; y < height; ++y) {
    for (uint x = 0; x < width; ++x) {
      bool val = grid[get_idx(x, y, width)];
      if (val) fprintf(fp, "*");
      else {
        fprintf(fp, ".");
        dead++;
      }
    }
    fprintf(fp, "\n");
  }
  if (overwrite) {
    // use terminal escapes to move back to beginning
    fprintf(fp, "\33[%dA\r", height);
  } else {
    fprintf(fp, "\n");
  }
  return dead == width*height;
}
bool print(bool* grid, uint width, uint height, bool overwrite) {
  return fprint(grid, width, height, overwrite, stdout);
}
bool eprint(bool* grid, uint width, uint height) {
  return fprint(grid, width, height, false, stderr);
}
bool is_live(const char v) { return v == '*' || v == '1'; }
void fill_line(const char* line, bool* grid, uint y, uint w, ssize_t nread) {
  if (nread - 1 != w) {
    fprintf(stderr, "input line of length %zu is invalid", nread);
    exit(EXIT_FAILURE);
  }
  for (uint x = 0; x < w; ++x) {
    grid[get_idx(x, y, w)] = is_live(line[x]);
  }
}
bool* read_initial(const char* fname, uint* width, uint* height) {
  FILE* fp = NULL;
  char* line = NULL;
  size_t len = 0;
  ssize_t nread;
  
  fp = strcmp(fname, "-") == 0 ? stdin : fopen(fname, "r");
  if (!fp) {
    perror("failed to open file");
    exit(EXIT_FAILURE);
  }
  
  // read a single line to check for width and height
  nread = getline(&line, &len, fp);
  if (nread == -1) {
    fprintf(stderr, "Unexpected end of input");
    exit(EXIT_FAILURE);
  }
  uint w;
  uint h;
  int n = 0;
  sscanf(line, "%u %u %n", &w, &h, &n);
  // if the line is longer than n then this is a safe access.
  // if the line is smaller, then n will not have changed and n > 0 will fail
  // checks if this is a line specifing width and height or a board line
  bool valid = n > 0 && line[n] == '\0';
  if (!valid) {
    w = *width;
    h = *height;
  }
  bool* grid = malloc(w*h * sizeof(bool));
  if (!valid) {
    fill_line(line, grid, 0, w, nread);
  }
  
  for (uint y = (uint)!valid; y < h; ++y) {
    nread = getline(&line, &len, fp);
    if (nread == -1) {
      fprintf(stderr, "Unexpected end of input");
      exit(EXIT_FAILURE);
    }
    fill_line(line, grid, y, w, nread);
  }
  
  free(line); line = NULL;
  if (fp != stdin) fclose(fp);
  (*width) = w;
  (*height) = h;
  return grid;
}
bool* run(bool* grid, uint width, uint height) {
  bool* next = malloc((width*height) * sizeof(bool));
  
  for (uint i = 0; i < width*height; ++i) {
    next[i] = rules(grid[i], get_neighbors(i, grid, width, height));
  }
  free(grid); grid = NULL;
  return next;
}

int main(int argc, char** argv) {
  int opt;
  uint width = 1024;
  uint height = 1024;
  uint seed = time(0);
  int max_gen = -1;
  bool overwrite = true;
  uint sleept = 1000;
  const char* fname = NULL;
  bool metrics = false;
  while ((opt = getopt(argc, argv, "hd:s:i:nf:vt:m")) != -1) {
    switch (opt) {
      case 'h':
        help(argv[0]);
        return 0;
      case 'd': {
        int read = sscanf(optarg, "%u %u", &width, &height);
        if (read < 1) {
          fprintf(stderr, "Invalid dimensions\n");
          exit(EXIT_FAILURE);
        }
        height = (read == 1) ? width : height;
        break;
      }
      case 's': {
        seed = strtoui(optarg, NULL, 0);
        if (errno) {
          perror("Invalid seed");
          exit(EXIT_FAILURE);
        }
        break;
      }
      case 'i': {
        max_gen = strtoui(optarg, NULL, 0);
        if (errno) {
          perror("Invalid number of generations");
          exit(EXIT_FAILURE);
        }
        break;
      }
      case 'n': {
        overwrite = false;
        break;
      }
      case 'f': {
        fname = strcmp(optarg, "-") == 0 || strcmp(optarg, "") == 0 ? "-" : optarg;
        break;
      }
      case 'v': {
        VERBOSE = true;
        break;
      }
      case 't': {
        sleept = strtoui(optarg, NULL, 0);
        if (errno) {
          perror("Invalid sleep time");
          exit(EXIT_FAILURE);
        }
        break;
      }
      case 'm': {
        metrics = true;
        break;
      }
      default:
        help(argv[0]);
        exit(EXIT_FAILURE);
    }
  }
  bool* grid = NULL;
  if (fname) {
    grid = read_initial(fname, &width, &height);
  } else {
    srand(seed);
    grid = malloc((width * height) * sizeof(bool));
    for (uint i = 0; i < width * height; ++i) {
      grid[i] = rand_prob(.2);
    }
  }
  bool dead = false;
  uint gen = 0;
  while (!dead && (int)gen != max_gen) {
    dead = print(grid, width, height, overwrite); 
    if (dead) { continue; }
    clock_t begin = clock();
    grid = run(grid, width, height);
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    if (metrics) {
      fprintf(stderr, "%f seconds to run generation %d\n", time_spent, gen+1);
    }
    msleep(sleept);
    gen++;
  }
  free(grid); grid = NULL;
  return 0;
}
