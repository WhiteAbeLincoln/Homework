% Created 2020-02-28 Fri 22:09
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../../preamble.tex} \DeclareMathOperator{\cube}{cube}
\author{Abraham White}
\date{\today}
\title{Homework 6 - Game of Life}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Homework 6 - Game of Life},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section{Description}
\label{sec:org1fc1985}
Make a game of life. Create a 'world' of 1024 by 1024 cells, without edge connections
(i.e., the world is 'flat' with 'edges' that organisms can 'fall off' of).

The rules for the game of life are:
\begin{itemize}
\item every organism that has 0 or 1 neighbors dies of lonelines.
\item every organism that has 2 or 3 neighbors survives to the next day.
\item every organism that has 4 or more neighbors dies of overcrowding.
\item every cell that has exactly three neighbors gives birth to a new organism.
\end{itemize}

Make your program run as quickly as possible. Provide timing information on a 'per day' basis,
and chart your program versus the number of processors used. 
\section{Implementation}
\label{sec:orgf54de3b}
\subsection{Serial Game of Life}
\label{sec:org4d2ad83}
Our first step is to define a rules function, which when given a cell and each of its neighbors,
returns the new state of that cell. We represent the neighbors using a struct so as to simplify
passing parameters\footnote{See the appendix for the definition of the \texttt{Env} struct which stores the neighbors of a cell.}. The rules are:
\begin{enumerate}
\item Any live cell with two or three neighbors survives.
\item Any dead cell with three live neighbors becomes a live cell.
\item All other live cells die in the next generation. Similarly, all other dead cells stay dead.
\end{enumerate}
We represent cells in the environment using boolean values indicating live or dead. C allows 
treating booleans (defined in \texttt{stdbool.h}) as integers 1 and 0, so we can easily  add up all
neighbors to find out how many live ones there are.
\begin{minted}[]{c}
bool rules(bool cell, Env env) {
  int n = num_live(env);
  if (cell && (n == 2 || n == 3)) return true;
  if (!cell && n == 3) return true;
  return false;
}
\end{minted}

We need a function to populate an Env struct when given the index of a cell and the board
array. We index the two-dimensional grid into a one-dimensional block using the formula \(y*w + x\),
where \(w\) is the width of a row. This means we can get back the \(x\) and \(y\) coordinates
through these relationships: \\
Let \(v = y*w + x\).
\begin{itemize}
\item \(x \equiv v \mod w\)
\item \(y = v \backslash w\), where \(\backslash\) is integer division
\end{itemize}
After getting the \(x\) and \(y\) coordinates, we can find the indexes of the neighbor cells by combining 
movements to the left, right, up, or down. We can find whether a new position has escaped the grid
by making sure the new coordinates are within the bounds: \(0 \geq x < w\) and \(0 \geq y < h\).
\begin{minted}[]{c}
int get_x(int v, int w) { return v % w; }
int get_y(int v, int w) { return v / w; }
int get_idx(uint x, uint y, uint w) { return x + y*w; }
Env get_neighbors(uint i, bool* cells, uint width, uint height) {
  int x = get_x(i, width); int y = get_y(i, width);
  
  int L  = x-1;
  uint R  = x+1;
  int U  = y-1;
  uint D  = y+1;
  
  return (Env){ .n  =               U < 0       ? false : cells[get_idx(x, U, width)],
                .s  =               D >= height ? false : cells[get_idx(x, D, width)],
                .e  = R >= width                ? false : cells[get_idx(R, y, width)],
                .w  = L < 0                     ? false : cells[get_idx(L, y, width)],
                .ne = R >= width || U < 0       ? false : cells[get_idx(R, U, width)],
                .se = R >= width || D >= height ? false : cells[get_idx(R, D, width)],
                .nw = L < 0      || U < 0       ? false : cells[get_idx(L, U, width)],
                .sw = L < 0      || D >= height ? false : cells[get_idx(L, D, width)]};
}
\end{minted}

Next we need some way to populate the initial grid. We want have a cell be live with probability 20\%.
We can do this by defining a rand function that returns a number between 0 and 1, and returning true 
if the random number is less than 0.2.
\begin{minted}[]{c}
double rand_dbl() { return rand() / (double)RAND_MAX; }
bool rand_prob(double range) { return rand_dbl() <= range ? true : false; }
\end{minted}
Now we can allocate the initial grid and populate it. The user can optionally specify the initial grid
using a file, so we read from the file instead of random generation if that is the case.
\begin{minted}[]{c}
if (fname) {
  grid = read_initial(fname, &width, &height);
} else {
  srand(seed);
  grid = malloc((width * height) * sizeof(bool));
  for (uint i = 0; i < width * height; ++i) {
    grid[i] = rand_prob(.2);
  }
}
\end{minted}

With this we can define our serial state update function. This function will run for every tick
(a tick depends on how fast you want the state to update), and takes the previous state as a
grid array, frees it, and returns a new grid.
\begin{minted}[]{c}
bool* run(bool* grid, uint width, uint height) {
  bool* next = malloc((width*height) * sizeof(bool));
  
  for (uint i = 0; i < width*height; ++i) {
    next[i] = rules(grid[i], get_neighbors(i, grid, width, height));
  }
  free(grid); grid = NULL;
  return next;
}
\end{minted}
We need a method to print out the grid. We do this simply by iterating over the grid
and printing out a \texttt{*} when a cell is live, and a \texttt{.} when the cell is dead. We use the boolean parameter
\texttt{overwrite} to control whether we use terminal escape codes to overwrite the old print or just
print a new board separated by a newline.
\begin{minted}[]{c}
bool fprint(bool* grid, uint width, uint height, bool overwrite, FILE* fp) {
  uint dead = 0;
  for (uint y = 0; y < height; ++y) {
    for (uint x = 0; x < width; ++x) {
      bool val = grid[get_idx(x, y, width)];
      if (val) fprintf(fp, "*");
      else {
        fprintf(fp, ".");
        dead++;
      }
    }
    fprintf(fp, "\n");
  }
  if (overwrite) {
    // use terminal escapes to move back to beginning
    fprintf(fp, "\33[%dA\r", height);
  } else {
    fprintf(fp, "\n");
  }
  return dead == width*height;
}
bool print(bool* grid, uint width, uint height, bool overwrite) {
  return fprint(grid, width, height, overwrite, stdout);
}
bool eprint(bool* grid, uint width, uint height) {
  return fprint(grid, width, height, false, stderr);
}
\end{minted}
Finally, in our main method we need to loop, getting the new grid and printing
it until the maximum number of generations is reached or there are no live cells left.
\begin{minted}[]{c}
bool dead = false;
uint gen = 0;
while (!dead && (int)gen != max_gen) {
  dead = print(grid, width, height, overwrite); 
  if (dead) { continue; }
  clock_t begin = clock();
  grid = run(grid, width, height);
  clock_t end = clock();
  double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  if (metrics) {
    fprintf(stderr, "%f seconds to run generation %d\n", time_spent, gen+1);
  }
  msleep(sleept);
  gen++;
}
free(grid); grid = NULL;
\end{minted}
\subsection{Parallel Game of Life}
\label{sec:org70251fe}
For the parallel version we will use the same method as with the parallel mandelbrot:
a job queue where each process receives a row and then sends it back to the leader to
combine and print.

First we need a job assignment function. The leader has a list of currently assigned jobs
and a list of jobs marked as completed or incomplete. The leader assigns the job to the
process and then marks the job as being worked on. Finally the leader sends the assigned row
to the worker process along with the two adjacent rows.
\begin{minted}[]{c}
void assn_job(uint job, int process, int* jobs, int* assignments,
              bool* grid, uint width, uint height) {
  assignments[process] = (int)job;
  jobs[job] = 1;
  bool* surrounding = malloc((width*3) * sizeof(bool));
  for (uint x = 0; x < width; ++x) {
    surrounding[get_idx(x, 0, width)] = ((int)job - 1 < 0) ? false : grid[get_idx(x, job-1, width)];
    surrounding[get_idx(x, 1, width)] = grid[get_idx(x, job, width)];
    surrounding[get_idx(x, 2, width)] = (job + 1 >= height) ? false : grid[get_idx(x, job+1, width)];
  }
  if (VERBOSE) {
    fprintf(stderr, "Leader assigning row %d to %d\n", job, process);
    eprint(grid, width, 3);
  }
  MPI_Send(surrounding, width*3, MPI_C_BOOL, process+1, SEND_JOB, MCW);
  free(surrounding); surrounding = NULL;
}
\end{minted}

The worker receives the assignment from the leader. This is similar to the serial run function,
but we only iterate over a single row. The worker check the number of received items in our list
so that they can know when to terminate. After finding the new, the worker sends back the row.
\begin{minted}[]{c}
bool worker_job(uint width, int rank) {
  bool* grid = malloc((width*3)*sizeof(bool));
  bool ret = false;
  MPI_Status status;
  int count;
  
  MPI_Recv(grid, width*3, MPI_C_BOOL, 0, SEND_JOB, MCW, &status);
  MPI_Get_count(&status, MPI_C_BOOL, &count);
  
  if (count == (int)width*3) {
    bool* next = malloc(width * sizeof(bool));
    
    if (VERBOSE) {
      fprintf(stderr, "Worker %d got assignment size %d\n", rank, count);
      eprint(grid, width, 3);
    }
    for (uint x = 0; x < width; ++x) {
      int i = get_idx(x, 1, width);
      next[x] = rules(grid[i], get_neighbors(i, grid, width, 3));
    }
    if (VERBOSE) {
      fprintf(stderr, "Worker %d sending completed row\n", rank);
      eprint(next, width, 1);
    }
    MPI_Send(next, width, MPI_C_BOOL, 0, JOB_FINISHED, MCW);
    
    free(next); next = NULL;
    ret = true;
  }
  
  free(grid); grid = NULL;
  return ret;
}
\end{minted}

Next we need a way for the leader process to receive the finished results from the worker. The
function returns an integer indicating whether the worker had an assigned job that was completed.
\begin{minted}[]{c}
uint recv_job(int node, uint width, int* assignments, int* waiting,
              int* flag, MPI_Request* rq, bool* next) {
  if (waiting[node] == 0) {
    bool* recvptr = next + get_idx(0, assignments[node], width);
    MPI_Irecv(recvptr, width, MPI_C_BOOL, node+1, JOB_FINISHED, MCW, &rq[node]);
    if (VERBOSE) {
      fprintf(stderr, "Leader got list from worker %d\n", node);
      eprint(recvptr, width, 1);
    }
    waiting[node] = 1;
  }
  
  MPI_Test(rq + node, &flag[node], MPI_STATUS_IGNORE);
  if (flag[node]) {
    assignments[node] = -1;
    waiting[node] = 0;
    return 1;
  }
  
  return 0;
}
\end{minted}

Now we have the parallel implementation of the run function. This function is run for every tick by
the leader process. It takes the previous state, assigns the workers to compute a new state,
frees the old state once complete, and returns the new state.
\begin{minted}[]{c}
bool* run(bool* grid, uint height, uint width, int size) {
  int NUM_WORKERS = size - 1;
  int* jobs = malloc(height * sizeof(int));
  int* assignments = malloc(NUM_WORKERS * sizeof(int));
  int* waiting = malloc(NUM_WORKERS * sizeof(int));
  int* flag = malloc(NUM_WORKERS * sizeof(int));
  MPI_Request* rq = malloc(NUM_WORKERS * sizeof(MPI_Request));
  bool* next = malloc(width*height * sizeof(bool));
  uint num_finished = 0;
  for (uint i = 0; i < height; ++i) {
    jobs[i] = 0;
  }
  for (int i = 0; i < NUM_WORKERS; ++i) {
    assignments[i] = -1;
    waiting[i] = 0;
    flag[i] = 0;
  }
\end{minted}

While we have incomplete rows, we check if a process has an assignment,
assigning the first available if not, otherwise initiating an asynchronous receive.
\begin{minted}[]{c}
  while (num_finished != height) {
    for (int node=0; node < NUM_WORKERS; ++node) {
      if (assignments[node] != -1) {
        num_finished += recv_job(node, width, assignments, waiting,
                                 flag, rq, next);
      } else {
        for (uint i = 0; i < height; ++i) {
          if (jobs[i] == 0) {
            assn_job(i, node, jobs, assignments, grid, width, height);
            break;
          }
        }
      }
    }
  }
\end{minted}

After all jobs are complete, we have to let the other processes know that they can
stop waiting for new assignments. We send a sentinel value which will be read
as the first element of the received array. Since the length of the received array
will be less than the expected size, the worker can disambiguate between a regular
job assignment and a termination message. Finally we free the old grid and return
the next state.
\begin{minted}[]{c}
  bool termval = false;
  for (int node=0; node < NUM_WORKERS; ++node) {
    MPI_Send(&termval, 1, MPI_C_BOOL, node+1, SEND_JOB, MCW);
  }

  free(grid); grid = NULL;
  free(jobs); jobs = NULL;
  free(assignments); assignments = NULL;
  free(waiting); waiting = NULL;
  free(flag); flag = NULL;
  free(rq); rq = NULL;
  return next;
}
\end{minted}

Finally, in the main function we need to have the leader process print out the last grid state,
let the other processes know if the grid is dead, use the run function to calculate a new state in parallel, 
and then sleep and block. The worker processes receive the broadcast grid liveness message, and loop asking
for jobs until the new state is calculated.
\begin{minted}[]{c}
bool dead = false;
uint gen = 0;
while (!dead && (int)gen != max_gen) {
  if (rank == 0) {
    dead = print(grid, width, height, overwrite); 
    MPI_Bcast(&dead, 1, MPI_C_BOOL, 0, MCW);
    if (dead) { continue; }
    if (VERBOSE) fprintf(stderr, "Leader updating state\n");
    clock_t begin = clock();
    grid = run(grid, width, height, size);
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    if (metrics) {
      fprintf(stderr, "%f seconds to run generation %d\n", time_spent, gen+1);
    }
    msleep(sleept);
  } else {
    MPI_Bcast(&dead, 1, MPI_C_BOOL, 0, MCW);
    if (dead) { continue; }
    if (VERBOSE) fprintf(stderr, "Worker %d beginning loop\n", rank);
    bool incomplete = true;
    while (incomplete) {
      incomplete = worker_job(width, rank);
    }
    if (VERBOSE) fprintf(stderr, "Worker %d terminated\n", rank);
  }
  gen++;
  MPI_Barrier(MCW);
}
free(grid); grid = NULL;
\end{minted}

\section{Building and Running}
\label{sec:org8611cea}
Build a file with \texttt{mpicc -Wall -Wextra -Werror -o gol gol.c -lm}, and run with \\
\texttt{mpirun -np <num\_processes> -{}-use-hwthread-cpus ./gol}
\begin{minted}[]{sh}
    mpicc -Wall -Wextra -Werror -o gol ./gol.c -lm
    mpirun -np 8 --use-hwthread-cpus ./gol -d16 -s1 -i1
\end{minted}

\begin{verbatim}
  .....*..........
  ..*.*.*.*.**....
  ................
  *..**...**.*....
  .......*........
  .*..............
  ........*.......
  ..*...*.**......
  ......*.*.*.*.*.
  *.......**.**...
  ..**...*.*.**..*
  *........*......
  ..*...........*.
  .*........*.....
  ...*.......*....
  .....*.*.....*..
\end{verbatim}

\section{Performance}
\label{sec:org0955358}
\subsection{Serial}
\label{sec:orgb260bff}
First we compare the serial program with different sized grids, taking the average of 100 generations.
\begin{minted}[]{sh}
./serial_gol -d"$n" -s1 -i100 -t1 -n -m 2>&1 >/dev/null | awk -F' ' '{s+=$1}END{printf "%f", s/NR}'
\end{minted}

For convenience, we test our table for powers of two from 16 to 1024

\begin{center}
\begin{tabular}{rr}
size & results\\
\hline
16 & 5.6e-05\\
32 & 0.000214\\
64 & 0.000781\\
128 & 0.00159\\
256 & 0.004953\\
512 & 0.019521\\
1024 & 0.078401\\
\end{tabular}
\end{center}
\subsection{Parallel}
\label{sec:org656c0e3}
\begin{minted}[]{sh}
mpirun -np "$p" --use-hwthread-cpus ./gol -d"$n" -s1 -i100 -t1 -n -m 2>&1 >/dev/null | awk -F' ' '{s+=$1}END{printf "%f", s/NR}'
\end{minted}

We will test with 2, 4, and 8 processors.

\subsubsection{2 processors}
\label{sec:org51fb718}
\begin{center}
\begin{tabular}{rr}
size & results\\
\hline
16 & 7.2e-05\\
32 & 0.000197\\
64 & 0.000727\\
128 & 0.002675\\
256 & 0.010206\\
512 & 0.040081\\
1024 & 0.158797\\
\end{tabular}
\end{center}

\subsubsection{4 processors}
\label{sec:org7c4903c}
\begin{center}
\begin{tabular}{rr}
size & results\\
\hline
16 & 3.1e-05\\
32 & 8.1e-05\\
64 & 0.000262\\
128 & 0.00063\\
256 & 0.002414\\
512 & 0.009403\\
1024 & 0.036805\\
\end{tabular}
\end{center}

\subsubsection{8 processors}
\label{sec:org9e37769}
\begin{center}
\begin{tabular}{rr}
size & results\\
\hline
16 & 2.8e-05\\
32 & 7.1e-05\\
64 & 0.000215\\
128 & 0.000894\\
256 & 0.002954\\
512 & 0.011197\\
1024 & 0.04601\\
\end{tabular}
\end{center}

\section{Appendix}
\label{sec:org9dd6796}
\subsection{Complete Source}
\label{sec:org757623e}
See \texttt{serial\_gol.c} and \texttt{gol.c}
\subsection{Auxiliary Functions}
\label{sec:org7b6be25}
\subsubsection{Env}
\label{sec:org3a2edd0}
Defines a struct representing the neighbors of a cell
\begin{minted}[]{c}
typedef struct Env {
  bool n;
  bool ne;
  bool e;
  bool se;
  bool s;
  bool sw;
  bool w;
  bool nw;
} Env;
uint num_live(Env e) {
  return (uint)e.n
       + (uint)e.ne
       + (uint)e.e
       + (uint)e.se
       + (uint)e.s
       + (uint)e.sw
       + (uint)e.w
       + (uint)e.nw;
}
\end{minted}
\subsubsection{\texttt{strtoui}}
\label{sec:orgd241c84}
Converts a string to an unsigned integer, since this functionality is missing
from the C standard library.
\begin{minted}[]{c}
// see https://stackoverflow.com/a/29378380
static unsigned long int strtosubrange(const char* s,
                                       char** endptr,
                                       int base,
                                       unsigned long int min,
                                       unsigned long int max) {
  unsigned long int y = strtoul(s, endptr, base);
  if (y > max) {
    errno = ERANGE;
    return max;
  }
  if (y < min) {
    errno = ERANGE;
    return min;
  }
  return y;
}
uint strtoui(const char* s, char** endptr, int base) {
  #if UINT_MAX == ULONG_MAX
    return (uint) strtol(s, endptr, base);
  #else
    return (uint) strtosubrange(s, endptr, base, 0, UINT_MAX);
  #endif
}
\end{minted}
\subsubsection{\texttt{help}}
\label{sec:org012d366}
Prints out help message for command-line arguments
\begin{minted}[]{c}
void help(const char* name) {
  printf(
    "Game of Life\n"
    "Usage:\t %s [OPTIONS]\n"
    "Options:\n"
    "\t-h\tShow this screen\n"
    "\t-s <uint>\tRandom seed [default: time(0)]\n"
    "\t-d <xmax> [ymax]\tDefine board dimensions [default: 1024]\n"
    "\t-i <uint> \tMaximum number of generations [default: -1]\n"
    "\t-n \tDisable game board overwrite [default: false]\n"
    "\t-f <FILE> \tRead initial board from a file. Uses stdin if FILE is - [default: NULL]\n"
    "\t-t <uint> \tTime between ticks in milliseconds. [default: 1000]\n"
    "\t-m\tEnable timing metrics. Metrics are output on stderr [default: false]\n",
    name);
}
\end{minted}
\subsubsection{\texttt{read\_initial}}
\label{sec:orgd3cf0aa}
Reads initial board from a file
\begin{minted}[]{c}
bool is_live(const char v) { return v == '*' || v == '1'; }
void fill_line(const char* line, bool* grid, uint y, uint w, ssize_t nread) {
  if (nread - 1 != w) {
    fprintf(stderr, "input line of length %zu is invalid", nread);
    exit(EXIT_FAILURE);
  }
  for (uint x = 0; x < w; ++x) {
    grid[get_idx(x, y, w)] = is_live(line[x]);
  }
}
bool* read_initial(const char* fname, uint* width, uint* height) {
  FILE* fp = NULL;
  char* line = NULL;
  size_t len = 0;
  ssize_t nread;
  
  fp = strcmp(fname, "-") == 0 ? stdin : fopen(fname, "r");
  if (!fp) {
    perror("failed to open file");
    exit(EXIT_FAILURE);
  }
  
  // read a single line to check for width and height
  nread = getline(&line, &len, fp);
  if (nread == -1) {
    fprintf(stderr, "Unexpected end of input");
    exit(EXIT_FAILURE);
  }
  uint w;
  uint h;
  int n = 0;
  sscanf(line, "%u %u %n", &w, &h, &n);
  // if the line is longer than n then this is a safe access.
  // if the line is smaller, then n will not have changed and n > 0 will fail
  // checks if this is a line specifing width and height or a board line
  bool valid = n > 0 && line[n] == '\0';
  if (!valid) {
    w = *width;
    h = *height;
  }
  bool* grid = malloc(w*h * sizeof(bool));
  if (!valid) {
    fill_line(line, grid, 0, w, nread);
  }
  
  for (uint y = (uint)!valid; y < h; ++y) {
    nread = getline(&line, &len, fp);
    if (nread == -1) {
      fprintf(stderr, "Unexpected end of input");
      exit(EXIT_FAILURE);
    }
    fill_line(line, grid, y, w, nread);
  }
  
  free(line); line = NULL;
  if (fp != stdin) fclose(fp);
  (*width) = w;
  (*height) = h;
  return grid;
}
\end{minted}
\subsubsection{\texttt{msleep}}
\label{sec:org2006703}
A sleep function allowing for milliseconds
\begin{minted}[]{c}
// https://stackoverflow.com/a/1157217
int msleep(long msec) {
  struct timespec ts;
  int res;

  if (msec < 0) {
      errno = EINVAL;
      return -1;
  }

  ts.tv_sec = msec / 1000;
  ts.tv_nsec = (msec % 1000) * 1000000;

  do {
      res = nanosleep(&ts, &ts);
  } while (res && errno == EINTR);

  return res;
}
\end{minted}
\subsubsection{Messages}
\label{sec:orgb64542c}
Enum for job pool messages
\begin{minted}[]{c}
typedef enum Messages {
  JOB_FINISHED,
  SEND_JOB,
} Messages;
\end{minted}
\subsubsection{Program Configuration}
\label{sec:orgfd681dd}
Handles getting command-line arguments
\begin{minted}[]{c}
int opt;
uint width = 1024;
uint height = 1024;
uint seed = time(0);
int max_gen = -1;
bool overwrite = true;
uint sleept = 1000;
const char* fname = NULL;
bool metrics = false;
while ((opt = getopt(argc, argv, "hd:s:i:nf:vt:m")) != -1) {
  switch (opt) {
    case 'h':
      help(argv[0]);
      return 0;
    case 'd': {
      int read = sscanf(optarg, "%u %u", &width, &height);
      if (read < 1) {
        fprintf(stderr, "Invalid dimensions\n");
        exit(EXIT_FAILURE);
      }
      height = (read == 1) ? width : height;
      break;
    }
    case 's': {
      seed = strtoui(optarg, NULL, 0);
      if (errno) {
        perror("Invalid seed");
        exit(EXIT_FAILURE);
      }
      break;
    }
    case 'i': {
      max_gen = strtoui(optarg, NULL, 0);
      if (errno) {
        perror("Invalid number of generations");
        exit(EXIT_FAILURE);
      }
      break;
    }
    case 'n': {
      overwrite = false;
      break;
    }
    case 'f': {
      fname = strcmp(optarg, "-") == 0 || strcmp(optarg, "") == 0 ? "-" : optarg;
      break;
    }
    case 'v': {
      VERBOSE = true;
      break;
    }
    case 't': {
      sleept = strtoui(optarg, NULL, 0);
      if (errno) {
        perror("Invalid sleep time");
        exit(EXIT_FAILURE);
      }
      break;
    }
    case 'm': {
      metrics = true;
      break;
    }
    default:
      help(argv[0]);
      exit(EXIT_FAILURE);
  }
}
\end{minted}
\end{document}