% Created 2020-03-13 Fri 20:47
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../../preamble.tex} \DeclareMathOperator{\cube}{cube}
\author{Abraham White}
\date{\today}
\title{Homework 7 - Load Balancing}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Homework 7 - Load Balancing},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section{Description}
\label{sec:orgdb28ee8}
Implement Sender-Initiated Distributed Random Dynamic Load Balancing with Dual-Pass Ring Termination.

\section{Implementation}
\label{sec:orge68c363}
A task is represented by an integer \(i\) in the range \([1, 1024]\). The work that a process must do for a
task is to increment this integer \(i^2\) times. We define a function for performing the work for a single
task.
\begin{minted}[]{c}
int do_work(int task) {
  int lim = task*task;
  for (int i = 0; i < lim; ++i) {
    task++;
  }
 return task;
}
\end{minted}
We also define a convenience function for generating a new task in the correct range.
\begin{minted}[]{c}
int gen_task() {
  return rand_range(TASK_MIN, TASK_MAX);
}
\end{minted}

Each process loops, performing the following actions until termination:
\begin{itemize}
\item check to see if any new work has arrived\\
We use \texttt{MPI\_Iprobe} to test whether there are messages waiting to avoid blocking
by a receive with no data.
\begin{minted}[]{c}
int work_flag;
do {
  MPI_Iprobe(MPI_ANY_SOURCE, 0, MCW, &work_flag, MPI_STATUS_IGNORE);
  if (work_flag) {
    int work[2];
    MPI_Recv(&work, 2, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
    enqueue(q, Node(work[0]));
    enqueue(q, Node(work[1]));
  }
} while (work_flag);
\end{minted}
\item if the number of tasks in the queue exceeds 16, then we send two tasks to random destinations.
\begin{minted}[]{c}
// never a valid value for host so we can use as sentinel
int host = size;
if (q->n > 16) {
  int t1 = dequeue(q);
  int t2 = dequeue(q);
  int sendbuff[2] = {t1, t2};
  int host = random_dest(rank, size);
  printf("%f: Process %d sending work to %d\n", MPI_Wtime(), rank, host);
  MPI_Send(&sendbuff, 2, MPI_INT, host, 0, MCW);
}
\end{minted}
We define a convenience function for getting a random destination.
\begin{minted}[]{c}
int random_dest(int rank, int size) {
  int c;
  do {
    c = rand_range(0, size-1);
  } while (c == rank);
  return c;
}
\end{minted}
\item the process completes a task, performing a single unit of work
\begin{minted}[]{c}
if (!is_queue_empty(q)) {
  int task = dequeue(q);
  int complete = do_work(task);
  printf("%f: Process %d completed work %d from task %d\n", MPI_Wtime(), rank, complete, task);
}
\end{minted}
\item finally, if the number of tasks generated so far is less than a predetermined random number in
the range \([1024, 2048]\), we generate 1 to 3 new tasks and place them at the end of the queue.
\begin{minted}[]{c}
  if (n_tasks < task_lim) {
    int to_gen = rand_range(1, 3);
    int diff = task_lim - n_tasks;
    if (to_gen > diff) to_gen = diff;
    for (int i = to_gen; i > 0; i--) {
      int task = gen_task();
      printf("%f: Process %d generated task %d\n", MPI_Wtime(), rank, task);
      enqueue(q, Node(task));
      n_tasks++;
    }
  }
\end{minted}

We also need a way for processes to know when all work has been completed. For this we use
the dual-pass ring termination method.
\end{itemize}

\subsection{Dual-Pass Ring Termination}
\label{sec:org9d88679}
We implement dual-pass ring termination. We use colored tokens, black or white, and also
color processes: a black color means that global termination may have not occured. The
algorithm is as follows:
\begin{enumerate}
\item The root process \(P_0\) becomes white when it has terminated and it generates a white token
that is passed to \(P_1\). We use \texttt{1} to indicate a token being black, \texttt{0} for white, and
\texttt{2} for not present. We also define a helper function for determining if a process has
terminated.
\begin{minted}[]{c}
int token = rank == 0 ? 0 : 2;
// P0 starts off as black and goes to white. all other processes start white
bool black_process = 0;
\end{minted}
\begin{minted}[]{c}
bool has_terminated(queue_t* q) {
  /* int work_flag = 0; */
  /* MPI_Iprobe(MPI_ANY_SOURCE, 0, MCW, &work_flag, MPI_STATUS_IGNORE); */
  return is_queue_empty(q);
}
\end{minted}
\item The token is passed through the ring from one process to the next when each process
has terminated. The color of the token may change. If process \(P_i\) passes a task
to process \(P_j\) where \(j < i\), process \(P_i\) becomes a black process. Otherwise it
is a white process. A black process colors the token black and passes it on. A white
process passes the token in its original color. After process \(P_i\) passes the token,
it becomes a white process.
\begin{minted}[]{c}
if (host < rank) black_process = true;
\end{minted}
\begin{minted}[]{c}
void send_token(int* token, bool* black_process, queue_t* q, int rank, int size) {
  if (has_terminated(q) && (*token) != 2) {
    if (rank == 0) (*token) = 0;
    if (rank != 0 && (*black_process)) (*token) = 1;
    printf("%f: Process %d (color %d) sending token of color %d\n", MPI_Wtime(), rank, *black_process, *token);
    MPI_Send(token, 1, MPI_INT, ring_dest(rank, size), 1, MCW);
    (*token) = 2;
    (*black_process) = false;
  }
}
\end{minted}
\begin{minted}[]{c}
int ring_dest(int rank, int size) {
  return (rank + 1) % size;
}
\end{minted}
\item When a process \(P_0\) receives a black token, it passes on a white token;
if it receives a white token, all processes have terminated.
\begin{minted}[]{c}
bool recv_token(int* token, int rank, int size) {
  int flag = 0;
  MPI_Iprobe(MPI_ANY_SOURCE, 1, MCW, &flag, MPI_STATUS_IGNORE);

  if (flag) {
    MPI_Recv(token, 1, MPI_INT, MPI_ANY_SOURCE, 1, MCW, MPI_STATUS_IGNORE);

    if ((*token) == -1) {
      return true;
    }
    
    printf("%f: Process %d got token with color %d\n", MPI_Wtime(), rank, *token);
  
    if (rank == 0 && (*token) != 2) {
      if ((*token) == 1) {
        int white_tok = 0;
        MPI_Send(&white_tok, 1, MPI_INT, ring_dest(rank, size), 1, MCW);
        (*token) = 2;
      } else {
        printf("%f: Process 0 telling others to terminate\n", MPI_Wtime());
        int terminate = -1;
        for (int i = 1; i < size; ++i) {
          MPI_Send(&terminate, 1, MPI_INT, i, 1, MCW);
        }

        return true;
      }
    }
  }

  return false;
}
\end{minted}
\end{enumerate}

Finally, we need to initialize variables for storing the state of our program. We define
local variables to store jobs, number of tasks, and the limit. Process 0 determines the
total task limit, and the other processes generate tasks until their number of generated
tasks hits the limit. This means that we generate \texttt{task\_lim*size} total
tasks. After that we loop until all processes have completed their tasks and terminated.
\begin{minted}[]{c}
srand(rank);
queue_t* q = Queue();
int n_tasks = 0;
int task_lim = rand_range(LIM_MIN, LIM_MAX);
MPI_Bcast(&task_lim, 1, MPI_INT, 0, MCW);
if (rank == 0) {
  printf("%f: Number of tasks to complete %d\n", MPI_Wtime(), task_lim*size);
}
\end{minted}

\section{Building and Running}
\label{sec:orgd9cec7c}
Build a file with \texttt{mpicc -Wall -Wextra -Werror -o loadb loadb.c -lm}, and run with \\
\texttt{mpirun -np <num\_processes> -{}-use-hwthread-cpus ./loadb}
\begin{minted}[]{sh}
    mpicc -Wall -Wextra -Werror -o loadb ./loadb.c -lm
    mpirun -np 8 --use-hwthread-cpus ./loadb
\end{minted}
\section{Appendix}
\label{sec:orgd8b5419}
\subsection{Helper Code}
\label{sec:org65944c2}
\subsubsection{random range}
\label{sec:orge9b36c6}
Generates a random number in a range. Not a uniform distribution
\begin{minted}[]{c}
int rand_range(int min, int max) {
  return min + rand() % (max - min + 1);
}
\end{minted}
\subsubsection{Queue}
\label{sec:orgcf29784}
An implementation of a queue for integers
\begin{minted}[]{c}
typedef struct node_t {
  int val;
  struct node_t *next;
} node_t;
typedef struct linked_list {
  node_t* head;
  node_t* tail;
  int n;
} queue_t;
node_t* Node(int val) {
  node_t* new_node = malloc(sizeof(node_t));
  new_node->val = val;
  new_node->next = NULL;
  return new_node;
}
queue_t* Queue() {
  queue_t* q = malloc(sizeof(queue_t));
  q->head = NULL;
  q->tail = NULL;
  q->n = 0;
  return q;
}
bool is_queue_empty(queue_t* q) {
  return q->head == NULL;
}
void enqueue(queue_t* q, node_t* n) {
  if (is_queue_empty(q)) {
    q->head = n;
    q->tail = n;
  } else {
    q->tail->next = n;
    q->tail = n;
  }
  q->n += 1;
}
int dequeue(queue_t* q) {
  if (is_queue_empty(q)) {
    errno = EINVAL;
    return INT_MIN;
  }
  int x = q->head->val;
  node_t* temp = q->head;
  q->head = q->head->next;
  free(temp);
  q->n -= 1;
  return x;
}
\end{minted}
\subsection{Complete Source}
\label{sec:org576c08f}
See \texttt{loadb.c}
\end{document}