#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <limits.h>
#include <mpi.h>
#include <time.h>
#define MCW MPI_COMM_WORLD
int TASK_MIN = 1;
int TASK_MAX = 1024;
int LIM_MIN = 1024;
int LIM_MAX = 2048;
typedef struct node_t {
  int val;
  struct node_t *next;
} node_t;
typedef struct linked_list {
  node_t* head;
  node_t* tail;
  int n;
} queue_t;
node_t* Node(int val) {
  node_t* new_node = malloc(sizeof(node_t));
  new_node->val = val;
  new_node->next = NULL;
  return new_node;
}
queue_t* Queue() {
  queue_t* q = malloc(sizeof(queue_t));
  q->head = NULL;
  q->tail = NULL;
  q->n = 0;
  return q;
}
bool is_queue_empty(queue_t* q) {
  return q->head == NULL;
}
void enqueue(queue_t* q, node_t* n) {
  if (is_queue_empty(q)) {
    q->head = n;
    q->tail = n;
  } else {
    q->tail->next = n;
    q->tail = n;
  }
  q->n += 1;
}
int dequeue(queue_t* q) {
  if (is_queue_empty(q)) {
    errno = EINVAL;
    return INT_MIN;
  }
  int x = q->head->val;
  node_t* temp = q->head;
  q->head = q->head->next;
  free(temp);
  q->n -= 1;
  return x;
}
int rand_range(int min, int max) {
  return min + rand() % (max - min + 1);
}
int gen_task() {
  return rand_range(TASK_MIN, TASK_MAX);
}
int random_dest(int rank, int size) {
  int c;
  do {
    c = rand_range(0, size-1);
  } while (c == rank);
  return c;
}
bool has_terminated(queue_t* q) {
  /* int work_flag = 0; */
  /* MPI_Iprobe(MPI_ANY_SOURCE, 0, MCW, &work_flag, MPI_STATUS_IGNORE); */
  return is_queue_empty(q);
}
int ring_dest(int rank, int size) {
  return (rank + 1) % size;
}
void send_token(int* token, bool* black_process, queue_t* q, int rank, int size) {
  if (has_terminated(q) && (*token) != 2) {
    if (rank == 0) (*token) = 0;
    if (rank != 0 && (*black_process)) (*token) = 1;
    printf("%f: Process %d (color %d) sending token of color %d\n", MPI_Wtime(), rank, *black_process, *token);
    MPI_Send(token, 1, MPI_INT, ring_dest(rank, size), 1, MCW);
    (*token) = 2;
    (*black_process) = false;
  }
}
bool recv_token(int* token, int rank, int size) {
  int flag = 0;
  MPI_Iprobe(MPI_ANY_SOURCE, 1, MCW, &flag, MPI_STATUS_IGNORE);

  if (flag) {
    MPI_Recv(token, 1, MPI_INT, MPI_ANY_SOURCE, 1, MCW, MPI_STATUS_IGNORE);

    if ((*token) == -1) {
      return true;
    }
    
    printf("%f: Process %d got token with color %d\n", MPI_Wtime(), rank, *token);
  
    if (rank == 0 && (*token) != 2) {
      if ((*token) == 1) {
        int white_tok = 0;
        MPI_Send(&white_tok, 1, MPI_INT, ring_dest(rank, size), 1, MCW);
        (*token) = 2;
      } else {
        printf("%f: Process 0 telling others to terminate\n", MPI_Wtime());
        int terminate = -1;
        for (int i = 1; i < size; ++i) {
          MPI_Send(&terminate, 1, MPI_INT, i, 1, MCW);
        }

        return true;
      }
    }
  }

  return false;
}
int do_work(int task) {
  int lim = task*task;
  for (int i = 0; i < lim; ++i) {
    task++;
  }
 return task;
}

int main(int argc, char** argv) {
  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);
  
  srand(rank);
  queue_t* q = Queue();
  int n_tasks = 0;
  int task_lim = rand_range(LIM_MIN, LIM_MAX);
  MPI_Bcast(&task_lim, 1, MPI_INT, 0, MCW);
  if (rank == 0) {
    printf("%f: Number of tasks to complete %d\n", MPI_Wtime(), task_lim*size);
  }
  int token = rank == 0 ? 0 : 2;
  // P0 starts off as black and goes to white. all other processes start white
  bool black_process = 0;
  
  while (true) {
    // MPI_Barrier(MCW);
    int work_flag;
    do {
      MPI_Iprobe(MPI_ANY_SOURCE, 0, MCW, &work_flag, MPI_STATUS_IGNORE);
      if (work_flag) {
        int work[2];
        MPI_Recv(&work, 2, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
        enqueue(q, Node(work[0]));
        enqueue(q, Node(work[1]));
      }
    } while (work_flag);
    // never a valid value for host so we can use as sentinel
    int host = size;
    if (q->n > 16) {
      int t1 = dequeue(q);
      int t2 = dequeue(q);
      int sendbuff[2] = {t1, t2};
      int host = random_dest(rank, size);
      printf("%f: Process %d sending work to %d\n", MPI_Wtime(), rank, host);
      MPI_Send(&sendbuff, 2, MPI_INT, host, 0, MCW);
    }
    if (!is_queue_empty(q)) {
      int task = dequeue(q);
      int complete = do_work(task);
      printf("%f: Process %d completed work %d from task %d\n", MPI_Wtime(), rank, complete, task);
    }
      if (n_tasks < task_lim) {
        int to_gen = rand_range(1, 3);
        int diff = task_lim - n_tasks;
        if (to_gen > diff) to_gen = diff;
        for (int i = to_gen; i > 0; i--) {
          int task = gen_task();
          printf("%f: Process %d generated task %d\n", MPI_Wtime(), rank, task);
          enqueue(q, Node(task));
          n_tasks++;
        }
      }
      
    if (host < rank) black_process = true;
    send_token(&token, &black_process, q, rank, size);
    bool terminate = recv_token(&token, rank, size);
    
    if (terminate) {
      printf("%f: Process %d terminating with %d tasks remaining\n", MPI_Wtime(), rank, task_lim - n_tasks);
      break;
    }
  }
  
  MPI_Finalize();
  return 0;
}
