    #include <stdlib.h>
    #include <unistd.h> /* unix stdlib, provides access to posix api */
    #include <stdio.h> /* standard input output */
    #include <stdbool.h> /* c doesn't import booleans by default. I could just use 1 */
    #include <mpi.h>
    
    #define MCW MPI_COMM_WORLD
    
    int main(int argc, char* argv[]) {
      int rank, size;
      MPI_Init(&argc, &argv);
    
      MPI_Comm_rank(MCW, &rank);
      MPI_Comm_size(MCW, &size);
      // allow setting the bomb timer through the command-line
      int timeb = 0;
      if (argc == 2) {
        timeb = atoi(argv[1]);
        if (timeb < 1) {
          printf("Bomb timer must be positive integer\n");
          MPI_Finalize();
          return 1;
        }
      } else {
        // plus 1 ensures the bomb is always positive when received
        timeb = rand() % 100 + 1;
      }
      if (rank == 0) {
        MPI_Send(&timeb, 1, MPI_INT, rand() % size, 0, MCW);
      }
      while (true) {
        int nextp = rand() % size;
        MPI_Recv(&timeb, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
        timeb--;
        // we use -1 as a sentinel value
        if (timeb == -1) {
          break;
        } else if (timeb == 0) {
          printf("Process %d got BOOOM\n", rank);
          exploded = true
          // send terminate message to all MPI processes
          for (int i = 0; i < size; ++i) MPI_Send(&timeb, 1, MPI_INT, i, 0, MCW);
        } else {
          printf("Process %d got bomb (%d); Sending to %d\n", rank, timeb, nextp);
          MPI_Send(&timeb, 1, MPI_INT, nextp, 0, MCW);
        }
      }
    
      MPI_Finalize();
      return 0;
    } // end main
