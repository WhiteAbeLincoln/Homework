% Created 2020-01-24 Fri 20:49
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../../preamble.tex}
\author{Abraham White}
\date{\today}
\title{Homework 2 - Time Bomb}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Homework 2 - Time Bomb},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section{Description}
\label{sec:org01fbd38}
Write a MPI program that does the following:
\begin{enumerate}
\item A process creates a random number (the time bomb)
\item This number is given to a randomly selected process, which subsequently decrements it
\item If the number is now zero, the bomb has exploded, and the current process is declared the loser.
The game ends and all processes terminate.
\item If the number is non-zero, we repeat step 2, sending the number to a random process.
\end{enumerate}
\section{Implementation}
\label{sec:org7de16c7}
See the appendix for full source code without interleaved comments.

First we have the required setup for MPI.
\begin{minted}[]{c}
#include <stdlib.h>
#include <unistd.h> /* unix stdlib, provides access to posix api */
#include <stdio.h> /* standard input output */
#include <stdbool.h> /* c doesn't import booleans by default. I could just use 1 */
#include <mpi.h>

#define MCW MPI_COMM_WORLD

int main(int argc, char* argv[]) {
  int rank, size;
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);
\end{minted}

We generate a random integer for the time bomb. \texttt{RAND\_MAX} is a large number, so for testing
we limit to 1-100.
\begin{minted}[]{c}
  // allow setting the bomb timer through the command-line
  int timeb = 0;
  if (argc == 2) {
    timeb = atoi(argv[1]);
    if (timeb < 1) {
      printf("Bomb timer must be positive integer\n");
      MPI_Finalize();
      return 1;
    }
  } else {
    // plus 1 ensures the bomb is always positive when received
    timeb = rand() % 100 + 1;
  }
\end{minted}

Next the first process sends the time bomb to the randomly selected victim process.
\begin{minted}[]{c}
  if (rank == 0) {
    MPI_Send(&timeb, 1, MPI_INT, rand() % size, 0, MCW);
  }
\end{minted}

We loop forever while waiting to receive a message containing the bomb. We need a while loop because
otherwise a process could not receive the bomb after it sent it once.

At the beginning of the loop we generate the random next process to send to. Note that this is not
a uniformly distributed random variable, but we don't care since there is no such requirement.
\begin{minted}[]{c}
  while (true) {
    int nextp = rand() % size;
\end{minted}

Within the loop, we wait to recieve a message containing the bomb. When the bomb is received, it
is decremented, the value is printed, and it is re-sent. The exploded process resends the bomb to
all other processes, which know to terminate since they received a -1 value.
\begin{minted}[]{c}
    MPI_Recv(&timeb, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
    timeb--;
    // we use -1 as a sentinel value
    if (timeb == -1) {
      break;
    } else if (timeb == 0) {
      printf("Process %d got BOOOM\n", rank);
      exploded = true
      // send terminate message to all MPI processes
      for (int i = 0; i < size; ++i) MPI_Send(&timeb, 1, MPI_INT, i, 0, MCW);
    } else {
      printf("Process %d got bomb (%d); Sending to %d\n", rank, timeb, nextp);
      MPI_Send(&timeb, 1, MPI_INT, nextp, 0, MCW);
    }
  }

  MPI_Finalize();
  return 0;
} // end main
\end{minted}

\section{Building and Running}
\label{sec:org0228f91}
Build a file with \texttt{mpicc -Wall -Wextra -Werror -o timeBomb timeBomb.c}, and run with \texttt{mpirun -np <num\_processes> ./timeBomb [timer]}
\begin{minted}[]{bash}
    mpicc -Wall -Wextra -Werror -o timeBomb ./timeBomb.c
    mpirun -np 4 ./timeBomb 25
\end{minted}

\begin{verbatim}
  Process 3 got bomb (24); Sending to 3
  Process 3 got bomb (23); Sending to 2
  Process 2 got bomb (22); Sending to 3
  Process 3 got bomb (21); Sending to 1
  Process 1 got bomb (20); Sending to 3
  Process 3 got bomb (19); Sending to 3
  Process 3 got bomb (18); Sending to 1
  Process 1 got bomb (17); Sending to 2
  Process 2 got bomb (16); Sending to 2
  Process 2 got bomb (15); Sending to 1
  Process 1 got bomb (14); Sending to 1
  Process 1 got bomb (13); Sending to 3
  Process 3 got bomb (12); Sending to 3
  Process 3 got bomb (11); Sending to 2
  Process 3 got bomb (9); Sending to 0
  Process 2 got bomb (10); Sending to 3
  Process 0 got bomb (8); Sending to 2
  Process 2 got bomb (7); Sending to 1
  Process 1 got bomb (6); Sending to 1
  Process 1 got bomb (5); Sending to 3
  Process 1 got bomb (3); Sending to 2
  Process 2 got bomb (2); Sending to 3
  Process 3 got bomb (4); Sending to 1
  Process 3 got bomb (1); Sending to 1
  Process 1 got BOOOM
\end{verbatim}

\section{Appendix}
\label{sec:org768b777}
\subsection{Source Code}
\label{sec:org3c71447}
\subsubsection{timeBomb.c}
\label{sec:org437e2c2}
\begin{minted}[]{c}
    #include <stdlib.h>
    #include <unistd.h> /* unix stdlib, provides access to posix api */
    #include <stdio.h> /* standard input output */
    #include <stdbool.h> /* c doesn't import booleans by default. I could just use 1 */
    #include <mpi.h>
    
    #define MCW MPI_COMM_WORLD
    
    int main(int argc, char* argv[]) {
      int rank, size;
      MPI_Init(&argc, &argv);
    
      MPI_Comm_rank(MCW, &rank);
      MPI_Comm_size(MCW, &size);
      // allow setting the bomb timer through the command-line
      int timeb = 0;
      if (argc == 2) {
        timeb = atoi(argv[1]);
        if (timeb < 1) {
          printf("Bomb timer must be positive integer\n");
          MPI_Finalize();
          return 1;
        }
      } else {
        // plus 1 ensures the bomb is always positive when received
        timeb = rand() % 100 + 1;
      }
      if (rank == 0) {
        MPI_Send(&timeb, 1, MPI_INT, rand() % size, 0, MCW);
      }
      while (true) {
        int nextp = rand() % size;
        MPI_Recv(&timeb, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
        timeb--;
        // we use -1 as a sentinel value
        if (timeb == -1) {
          break;
        } else if (timeb == 0) {
          printf("Process %d got BOOOM\n", rank);
          exploded = true
          // send terminate message to all MPI processes
          for (int i = 0; i < size; ++i) MPI_Send(&timeb, 1, MPI_INT, i, 0, MCW);
        } else {
          printf("Process %d got bomb (%d); Sending to %d\n", rank, timeb, nextp);
          MPI_Send(&timeb, 1, MPI_INT, nextp, 0, MCW);
        }
      }
    
      MPI_Finalize();
      return 0;
    } // end main
\end{minted}
\end{document}