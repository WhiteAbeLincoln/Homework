    #include <iostream>
    #include <mpi.h>
    #include <unistd.h>
    #include <stdlib.h>
    #define MCW MPI_COMM_WORLD
    int main(int argc, char** argv) {
      int rank, size;
      MPI_Init(&argc, &argv);
    
      MPI_Comm_rank(MCW, &rank);
      MPI_Comm_size(MCW, &size);
      MPI_Send( &rank/*buffer*/
              , 1/*count*/
              , MPI_INT/*datatype*/
              , (rank+1) % size/*destination*/
              , 0/*tag*/
              , MCW/*communicator*/);
      int data;
      MPI_Recv( &data/*buffer*/
              , 1/*count*/
              , MPI_INT/*datatype*/
              , MPI_ANY_SOURCE/*source*/
              , 0/*tag*/
              , MCW/*communicator*/
              , MPI_STATUS_IGNORE/*status*/);
      std::cout << "I am " << rank << " of " << size << "; got a message from " << data << std::endl;
    
      MPI_Finalize();
      return 0;
    } // end main
