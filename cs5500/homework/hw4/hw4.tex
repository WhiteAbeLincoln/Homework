% Created 2020-02-08 Sat 00:45
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../../preamble.tex} \DeclareMathOperator{\cube}{cube}
\author{Abraham White}
\date{\today}
\title{Homework 4 - Bitonic Sort}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Homework 4 - Bitonic Sort},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section{Description}
\label{sec:orgd68d838}
Write a MPI program that performs a bitonic sort using a partial list of numbers.
You may assume that the number of processors is a power of 2.
\section{Modeling our Problem}
\label{sec:org42e660e}
We can model the channels between 2\textsuperscript{n} processors as a hypercube of processors with bitstring labels: \\
\begin{figure}[h]
\centering
\scalebox{0.6}
{
\begin{tikzpicture}[scale=5]
\tikzstyle{vertex}=[circle,minimum size=20pt,inner sep=0pt]
\tikzstyle{selected vertex} = [vertex, fill=red!24]
\tikzstyle{selected edge} = [draw,line width=5pt,-,red!50]
\tikzstyle{edge} = [draw,thick,-,black]
\node[vertex] (v0) at (0,0) {$0000$};
\node[vertex] (v1) at (0,1) {$0001$};
\node[vertex] (v2) at (1,0) {$0010$};
\node[vertex] (v3) at (1,1) {$0011$};
\node[vertex] (v4) at (0.23, 0.4) {$0100$};
\node[vertex] (v5) at (0.23,1.4) {$0101$};
\node[vertex] (v6) at (1.23,0.4) {$0110$};
\node[vertex] (v7) at (1.23,1.4) {$0111$};
\node[vertex] (v8) at (-1,-1) {$1000$};
\node[vertex] (v9) at (-1,2) {$1001$};
\node[vertex] (v13) at (-0.66,2.7) {$1101$};
\node[vertex] (v12) at (-0.66,-0.3) {$1100$};
\node[vertex] (v10) at (2,-1) {$1010$};
\node[vertex] (v14) at (2.34,-0.3) {$1110$};
\node[vertex] (v11) at (2,2) {$1011$};
\node[vertex] (v15) at (2.34,2.7) {$1111$};
\draw[edge] (v0) -- (v1) -- (v3) -- (v2) -- (v0);
\draw[edge] (v0) -- (v4) -- (v5) -- (v1) -- (v0);
\draw[edge] (v2) -- (v6) -- (v7) -- (v3) -- (v2);
\draw[edge] (v4) -- (v6) -- (v7) -- (v5) -- (v4);
\draw[edge] (v8) -- (v9) -- (v13) -- (v12) -- (v8);
\draw[edge] (v0) -- (v4) -- (v12) -- (v8) -- (v0);
\draw[edge] (v1) -- (v9) -- (v13) -- (v5) -- (v1);
\draw[edge] (v2) -- (v10) -- (v14) -- (v6) -- (v2);
\draw[edge] (v8) -- (v10) -- (v14) -- (v12) -- (v8);
\draw[edge] (v3) -- (v11) -- (v15) -- (v7) -- (v3);
\draw[edge] (v10) -- (v11) -- (v15) -- (v14) -- (v10);
\draw[edge] (v9) -- (v11) -- (v15) -- (v13) -- (v9);
\draw[edge] (v0) -- (v2);
\draw[edge] (v2) -- (v6);
\draw[edge] (v6) -- (v4);
\draw[edge] (v4) -- (v5);
\draw[edge] (v5) -- (v13);
\draw[edge] (v13) -- (v12);
\draw[edge] (v12) -- (v14);
\draw[edge] (v14) -- (v15);
\draw[edge] (v15) -- (v7);
\draw[edge] (v7) -- (v3);
\draw[edge] (v3) -- (v1);
\draw[edge] (v1) -- (v9);
\draw[edge] (v9) -- (v11);
\draw[edge] (v11) -- (v10);
\draw[edge] (v10) -- (v8);
\draw[edge] (v8) -- (v0);
\end{tikzpicture}
}
\caption{Hypercube for $n=4$ or 16 processors}
\end{figure}

\noindent We also define functions using these bitstrings:
\begin{itemize}
\item \(\operatorname{getaddr}(n, b)\): returns the given bitstring with the \(n+1\) bit complemented
\item \(\operatorname{cube}(d, n, b)\): sends data \(d\) to the processor with address defined by
\(\operatorname{getaddr}(n, b)\), where \(b\) is the current processor's address
\end{itemize}
These functions allow for choosing specific processors to communicate with, assuming we have labeled our
processors with bitstrings of length \(n\). \\

Next we need to define a bitonic sequence. A bitonic sequence is a sequence \(S\) with elements \(s_0\) to \(s_{n-1}\)
such that
\[ s_0 \leq s_1 \leq \cdots \leq s_{n/2 - 1} \text{ and } s_{n/2} \geq s_{n/2 + 1} \geq \cdots \geq s_{n-1} \]
or alternatively
\[ s_0 \geq s_1 \geq \cdots \geq s_{n/2 - 1} \text{ and } s_{n/2} \leq s_{n/2 + 1} \leq \cdots \leq s_{n-1} \]
In other words, a bitonic sequence is one that increases (or decreases) monotonically until the center, where
it begins decreasing (or increasing) monotonically. A bitonic sorter can only accept bitonic sequences.

Since we want to sort sequences that may not be bitonic, we will need to find a way to turn an arbitrary
sequence into a bitonic sequence. Notice the fact that any two numbers form a bitonic sequence. What we can
do to create a bitonic sequence is to divide our given sequence into \(\frac{n}{2}\) two-element sequences. We sort
those two-element sequences alternately, so that the pairs of these pairs now form a proper bitonic sequence. We
continue this process until we have a single bitonic sequence, after which we can apply the bitonic sort to get
out a list that is monotonically increasing (or decreasing if that is the desire).

Now for the bitonic sort. Given a bitonic sequence of size \(n\), we sort the sequence by successively applying the
cube function. To sort a list of size \(n = 2^k\) in ascending order, we apply \(\cube_{k-1}\), then \(\cube_{k-2}\), then
\(\cube_{k-3}\), down to \(\cube_0\). When applying the \(\cube_n\) operation, the higher number is kept only if the given
processor has a 1 in the bit position \(n\) (i.e. if \texttt{rank \& (1 << n)}).


\section{Implementation}
\label{sec:orgf25f2dc}
First we read in the list of numbers to sort. See the appendix for the code.

Next we need to make the given list a bitonic one. We first give an element of our list to each processor.
Now we have \(\frac{n}{2}\) bitonic sequences split across processors. We can rely on the fact that all
processes receive the same arguments and standard input, and have run the read in.
\begin{minted}[]{c}
int val = array[rank];
\end{minted}

To do the bitonic sort on a list of size \(n = 2^k\), we successively apply the \(\cube_n\) operation. We also
determine whether we should sort in an ascending or descending position by comparing the \(k+1\) bit of rank.
\begin{minted}[]{c}
for (int j = i; j >= 0; j--) {
  val = cube(j, rank, val, !(bool)(rank & (1 << (i + 1))));
}
\end{minted}

However, this bitonic sort must be applied \(k\) times, since the beginning list is potentially not bitonic.
To do this we take the previous code and run it from \(i=0\) to \(i=k\).
\begin{minted}[]{c}
// dimensions of the hypercube, in our case also the power of 2 of the list size
int dimensions = log2(size);

for (int i = 0; i < dimensions; ++i) {
  for (int j = i; j >= 0; j--) {
    val = cube(j, rank, val, !(bool)(rank & (1 << (i + 1))));
  }
}
\end{minted}

Finally we send all of the numbers back to master and print out the result.
\begin{minted}[]{c}
MPI_Barrier(MCW);
MPI_Send(&val, 1, MPI_INT, 0, 0, MCW);

if (rank == 0) {
  for (int i = 0; i < size; ++i) {
    MPI_Recv(array + i, 1, MPI_INT, i, 0, MCW, MPI_STATUS_IGNORE);
  }
  print_array(array, n);
}
\end{minted}

\section{Building and Running}
\label{sec:orgd758b1c}
Build a file with \texttt{mpicc -Wall -Wextra -Werror -o bitonicSort bitonicSort.c -lm}, and run with \\
\texttt{mpirun -np <num\_processes> ./bitonicSort [integers]}
The list of integers must be the same size as the number of processes since I didn't bother allowing
sorting of sub-arrays.
\begin{minted}[]{sh}
    mpicc -Wall -Wextra -Werror -o bitonicSort ./bitonicSort.c -lm
    mpirun -np 8 --use-hwthread-cpus ./bitonicSort 5 2 6 1 4 8 7 3
\end{minted}

\begin{verbatim}
Sorting numbers:  5 2 6 1 4 8 7 3
 1 2 3 4 5 6 7 8
\end{verbatim}

\section{Appendix}
\label{sec:org20b47d7}
\subsection{Source Code}
\label{sec:org651c99c}
\subsubsection{Read list}
\label{sec:orga55f377}
\begin{minted}[]{c}
int n;
int* array;
if (argc > 1) {
  n = argc - 1;
  if (n != size) {
    fprintf(stderr, "the list to sort must be a power of 2 and the same as the number of processors\n");
    MPI_Finalize();
    exit(-1);
  }
  array = malloc(n * sizeof(int));
  check_array(array);

  for (int i = 1; i < argc; ++i) {
    array[i-1] = atoi(argv[i]);
  }
} else {
  n = read_int();
  if (n != size) {
    fprintf(stderr, "the list to sort must be a power of 2 and the same as the number of processors\n");
    MPI_Finalize();
    exit(-1);
  }
  array = malloc(n * sizeof(int));
  check_array(array);

  for (int i = 0; i < n; ++i) {
    array[i] = read_int();
  }
}

if (rank == 0) {
  printf("Sorting numbers: ");
  print_array(array, n);
}
\end{minted}
\subsubsection{\texttt{getaddr}}
\label{sec:org59d3a37}
Gets the address of the next processor based on the cube.
\begin{minted}[]{c}
int getaddr(int n, int rank) {
  return rank ^ (1 << n);
}
\end{minted}
\subsubsection{\texttt{cube}}
\label{sec:org561741f}
Sends the data to the next processor
\begin{minted}[]{c}
int cube(int n, int rank, int data, bool ascending) {
  int nextid = getaddr(n, rank);
  int recv;
  MPI_Send(&data, 1, MPI_INT, nextid, 0, MCW);
  MPI_Recv(&recv, 1, MPI_INT, nextid, 0, MCW, MPI_STATUS_IGNORE);
  
  // this could be made a oneliner/optimized but im crunched for time
  int hi = recv >= data ? recv : data;
  int lo = recv <= data ? recv : data;
  
  if (rank & (1 << n)) {
    return ascending ? hi : lo;
  } else {
    return ascending ? lo : hi;
  }
}
\end{minted}
\subsubsection{\texttt{read\_int}}
\label{sec:org2a05fdb}
\begin{minted}[]{c}
int read_int() {
  int v;
  int err = scanf("%d", &v);

  if (err == EOF) {
    fprintf(stderr, "stdin closed before providing list\n");
    MPI_Finalize();
    exit(-1);
  } else if (err != 1) {
    fprintf(stderr, "user gave incorrect input\n");
    MPI_Finalize();
    exit(-1);
  }

  return v;
}
\end{minted}
\subsubsection{\texttt{print\_array}}
\label{sec:orgf951d76}
\begin{minted}[]{c}
void print_array(int* array, int n) {
  for (int i = 0; i < n; ++i) {
    printf(" %d", array[i]);
  }
  printf("\n");
}
\end{minted}
\subsubsection{\texttt{check\_array}}
\label{sec:org099cd66}
\begin{minted}[]{c}
// we use void pointer since we don't actually care about the data
// just that it is a pointer to some block of memory
void check_array(void* array) {
  if (array == NULL) {
    fprintf(stderr, "malloc failed\n");
    MPI_Finalize();
    exit(-1);
  }
}
\end{minted}
\subsubsection{bitonicSort.c}
\label{sec:org5ccdc80}
\begin{minted}[]{c}
#include <stdlib.h>
#include <unistd.h> /* unix stdlib, provides access to posix api */
#include <stdio.h> /* standard input output */
#include <stdbool.h> /* c doesn't import booleans by default. I could just use 1 */
#include <math.h> // for log2
#include <mpi.h>

#define MCW MPI_COMM_WORLD

int getaddr(int n, int rank) {
  return rank ^ (1 << n);
}
int cube(int n, int rank, int data, bool ascending) {
  int nextid = getaddr(n, rank);
  int recv;
  MPI_Send(&data, 1, MPI_INT, nextid, 0, MCW);
  MPI_Recv(&recv, 1, MPI_INT, nextid, 0, MCW, MPI_STATUS_IGNORE);
  
  // this could be made a oneliner/optimized but im crunched for time
  int hi = recv >= data ? recv : data;
  int lo = recv <= data ? recv : data;
  
  if (rank & (1 << n)) {
    return ascending ? hi : lo;
  } else {
    return ascending ? lo : hi;
  }
}
int read_int() {
  int v;
  int err = scanf("%d", &v);

  if (err == EOF) {
    fprintf(stderr, "stdin closed before providing list\n");
    MPI_Finalize();
    exit(-1);
  } else if (err != 1) {
    fprintf(stderr, "user gave incorrect input\n");
    MPI_Finalize();
    exit(-1);
  }

  return v;
}
void print_array(int* array, int n) {
  for (int i = 0; i < n; ++i) {
    printf(" %d", array[i]);
  }
  printf("\n");
}
// we use void pointer since we don't actually care about the data
// just that it is a pointer to some block of memory
void check_array(void* array) {
  if (array == NULL) {
    fprintf(stderr, "malloc failed\n");
    MPI_Finalize();
    exit(-1);
  }
}

int main(int argc, char* argv[]) {
  int rank, size;
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);
  
  int n;
  int* array;
  if (argc > 1) {
    n = argc - 1;
    if (n != size) {
      fprintf(stderr, "the list to sort must be a power of 2 and the same as the number of processors\n");
      MPI_Finalize();
      exit(-1);
    }
    array = malloc(n * sizeof(int));
    check_array(array);
  
    for (int i = 1; i < argc; ++i) {
      array[i-1] = atoi(argv[i]);
    }
  } else {
    n = read_int();
    if (n != size) {
      fprintf(stderr, "the list to sort must be a power of 2 and the same as the number of processors\n");
      MPI_Finalize();
      exit(-1);
    }
    array = malloc(n * sizeof(int));
    check_array(array);
  
    for (int i = 0; i < n; ++i) {
      array[i] = read_int();
    }
  }
  
  if (rank == 0) {
    printf("Sorting numbers: ");
    print_array(array, n);
  }
  int val = array[rank];
  // dimensions of the hypercube, in our case also the power of 2 of the list size
  int dimensions = log2(size);
  
  for (int i = 0; i < dimensions; ++i) {
    for (int j = i; j >= 0; j--) {
      val = cube(j, rank, val, !(bool)(rank & (1 << (i + 1))));
    }
  }
  MPI_Barrier(MCW);
  MPI_Send(&val, 1, MPI_INT, 0, 0, MCW);
  
  if (rank == 0) {
    for (int i = 0; i < size; ++i) {
      MPI_Recv(array + i, 1, MPI_INT, i, 0, MCW, MPI_STATUS_IGNORE);
    }
    print_array(array, n);
  }
  
  MPI_Finalize();
  return 0;
}
\end{minted}
\end{document}