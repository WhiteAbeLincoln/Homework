#include <stdlib.h>
#include <unistd.h> /* unix stdlib, provides access to posix api */
#include <stdio.h> /* standard input output */
#include <stdbool.h> /* c doesn't import booleans by default. I could just use 1 */
#include <math.h> // for log2
#include <mpi.h>

#define MCW MPI_COMM_WORLD

int getaddr(int n, int rank) {
  return rank ^ (1 << n);
}
int cube(int n, int rank, int data, bool ascending) {
  int nextid = getaddr(n, rank);
  int recv;
  MPI_Send(&data, 1, MPI_INT, nextid, 0, MCW);
  MPI_Recv(&recv, 1, MPI_INT, nextid, 0, MCW, MPI_STATUS_IGNORE);
  
  // this could be made a oneliner/optimized but im crunched for time
  int hi = recv >= data ? recv : data;
  int lo = recv <= data ? recv : data;
  
  if (rank & (1 << n)) {
    return ascending ? hi : lo;
  } else {
    return ascending ? lo : hi;
  }
}
int read_int() {
  int v;
  int err = scanf("%d", &v);

  if (err == EOF) {
    fprintf(stderr, "stdin closed before providing list\n");
    MPI_Finalize();
    exit(-1);
  } else if (err != 1) {
    fprintf(stderr, "user gave incorrect input\n");
    MPI_Finalize();
    exit(-1);
  }

  return v;
}
void print_array(int* array, int n) {
  for (int i = 0; i < n; ++i) {
    printf(" %d", array[i]);
  }
  printf("\n");
}
// we use void pointer since we don't actually care about the data
// just that it is a pointer to some block of memory
void check_array(void* array) {
  if (array == NULL) {
    fprintf(stderr, "malloc failed\n");
    MPI_Finalize();
    exit(-1);
  }
}

int main(int argc, char* argv[]) {
  int rank, size;
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);
  
  int n;
  int* array;
  if (argc > 1) {
    n = argc - 1;
    if (n != size) {
      fprintf(stderr, "the list to sort must be a power of 2 and the same as the number of processors\n");
      MPI_Finalize();
      exit(-1);
    }
    array = malloc(n * sizeof(int));
    check_array(array);
  
    for (int i = 1; i < argc; ++i) {
      array[i-1] = atoi(argv[i]);
    }
  } else {
    n = read_int();
    if (n != size) {
      fprintf(stderr, "the list to sort must be a power of 2 and the same as the number of processors\n");
      MPI_Finalize();
      exit(-1);
    }
    array = malloc(n * sizeof(int));
    check_array(array);
  
    for (int i = 0; i < n; ++i) {
      array[i] = read_int();
    }
  }
  
  if (rank == 0) {
    printf("Sorting numbers: ");
    print_array(array, n);
  }
  int val = array[rank];
  // dimensions of the hypercube, in our case also the power of 2 of the list size
  int dimensions = log2(size);
  
  for (int i = 0; i < dimensions; ++i) {
    for (int j = i; j >= 0; j--) {
      val = cube(j, rank, val, !(bool)(rank & (1 << (i + 1))));
    }
  }
  MPI_Barrier(MCW);
  MPI_Send(&val, 1, MPI_INT, 0, 0, MCW);
  
  if (rank == 0) {
    for (int i = 0; i < size; ++i) {
      MPI_Recv(array + i, 1, MPI_INT, i, 0, MCW, MPI_STATUS_IGNORE);
    }
    print_array(array, n);
  }
  
  MPI_Finalize();
  return 0;
}
