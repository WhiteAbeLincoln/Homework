% Created 2020-01-31 Fri 22:33
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\input{../../../preamble.tex}
\author{Abraham White}
\date{\today}
\title{Homework 3 - Integer Sort}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Homework 3 - Integer Sort},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section{Description}
\label{sec:orgde0c90d}
Write a MPI program that performs an integer sort:
\begin{enumerate}
\item A master process divides the given list of integers into data segments
\item The master sends data segments to the slave processes, which sort the data as they see fit
\item The slaves send the data back to the master, who merges the lists in sorted order
\end{enumerate}
\section{Implementation}
\label{sec:org201b48d}
See the appendix for complete source code without interleaved comments. The code here only
highlights the important sections, leaving out some boilerplate and utility functions. \\

We declare \texttt{array} and \texttt{n} for all processes, which contains the list of numbers and the number
of items in the list respectively.
\begin{minted}[]{c}
int* array = NULL;
unsigned int n;
\end{minted}

Process 0 reads the list of integers from arguments or standard input. If standard input is used, the first
number entered is the length of the list. We use a utility function \texttt{read\_int} (found in appendix) to read
input.
\begin{minted}[]{c}
if (argc > 1) {
  n = argc - 1;
  array = malloc(n * sizeof(int));
  check_array(array);

  for (int i = 1; i < argc; ++i) {
    array[i-1] = atoi(argv[i]);
  }
} else {
  n = read_int();
  array = malloc(n * sizeof(int));
  check_array(array);

  for (unsigned int i = 0; i < n; ++i) {
    array[i] = read_int();
  }
}

printf("Master got integers to sort:");
print_array(array, n);
\end{minted}

Now having received the list of integers, the master process divides the list into \texttt{n} chunks, where \texttt{n}
is the number of processes minus one. Instead of allocating new memory for each chunk, we can instead 
send sections of the data to each process using pointer offsets. We send the size of the array first
and then the data in the array.
\begin{minted}[]{c}
unsigned int chunk_size = n / (size - 1);
unsigned int remainder = n % (size - 1);
for (int i = 1; i < size; ++i) {
  // in the case of size - 1 not diving n evenly,
  // the last process should receive size of chunk_size + remainder
  int num = i == (size - 1) ? chunk_size + remainder : chunk_size;
  MPI_Send(&num, 1, MPI_INT, i, 0, MCW);
  MPI_Send(array + ((i-1) * chunk_size), num, MPI_INT, i, 0, MCW);
}
\end{minted}

The other processes receive first the size of the chunk and then the chunk itself from the master process,
printing out the data.
\begin{minted}[]{c}
MPI_Recv(&n, 1, MPI_INT, 0, 0, MCW, MPI_STATUS_IGNORE);
printf("Process %d got %d integers:", rank, n);
array = malloc(n * sizeof(int));
check_array(array);
MPI_Recv(array, n, MPI_INT, 0, 0, MCW, MPI_STATUS_IGNORE);
print_array(array, n);
\end{minted}

After receiving the chunk of numbers, the other processes sort in place (in this case bubble sort, since
this isn't an algorithms class and we all know how to properly sort lists).
\begin{minted}[]{c}
int nn = n;
do {
  int newn = 0;
  for (int i = 1; i <= nn-1; ++i) {
    if (array[i - 1] > array[i]) {
      int swap = array[i];
      array[i] = array[i-1];
      array[i-1] = swap;
      newn = i;
    }
  }
  nn = newn;
} while (!(nn <= 1));
\end{minted}

After the data is sorted, the other processes then print the sorted data and send back to the master to merge
and output.
\begin{minted}[]{c}
printf("Process %d sorted chunk:", rank);
print_array(array, n);
MPI_Send(array, n, MPI_INT, 0, 0, MCW);
\end{minted}

Back in process 0, the master receives the sorted chunks and merges them. We need to create a
multi-dimensional array because I didn't want to figure out a streaming algorithm to merge \texttt{k} arrays one
at a time. I use a naive solution for merging lists, since that isn't the point of this program.

First we receive the chunks and store them in a array of arrays for later use. We also store the
sizes of the received chunks for later use since this is C and we don't have a convenient length property.
Because we know how the size was determined previously, we can avoid sending it from the slave processes
and just recompute.
\begin{minted}[]{c}
int** chunks = malloc((size-1) * sizeof(int*));
check_array(chunks);
int* chunk_sizes = malloc((size-1) * sizeof(int));;
check_array(chunk_sizes);
for (int i = 1; i < size; ++i) {
  int num = i == (size - 1) ? chunk_size + remainder : chunk_size;
  chunk_sizes[i-1] = num;
  chunks[i-1] = malloc(num * sizeof(int));
  check_array(chunks[i-1]);
  MPI_Recv(chunks[i-1], num, MPI_INT, i, 0, MCW, MPI_STATUS_IGNORE);
}
\end{minted}

Next we use our naive merge algorithm to combine the sorted chunks. This algorithm works by taking the
smallest head of each of the lists and storing it in the final array, until all of the intermediate lists
are empty.

While getting the smallest head we need to both keep track of the index of the chunk that had the
smallest, and have a flag variable for making sure we can set the initial smallest head. Both of
these functions are fulfilled by the \texttt{had\_smallest} variable, which is set to -1 initially to indicate
that we haven't yet compared any heads, and after that set to the chunk index every time we find a new
smallest head. After we find the smallest head, we increment the pointer and decrement the size of its
containing array, effectively dropping the head of the array.

We reset our \texttt{emptylists} counter each iteration of the while loop because if we moved it up a level we could
potentially count an empty chunk multiple times, causing us to end before the lists are fully merged.
\begin{minted}[]{c}
bool sentinel = false;
int idx = 0;

while (!sentinel) {
  int emptylists = 0;
  int smallest;
  int had_smallest = -1;
  for (int k = 0; k < size-1; ++k) {
    int chunk_size = chunk_sizes[k];
    if (chunk_size <= 0) {
      ++emptylists;
      continue;
    }
    int* chunk = chunks[k];
    if (had_smallest == -1 || smallest > chunk[0]) {
      smallest = chunk[0];
      had_smallest = k;
    }
  }
  
  if (had_smallest != -1) {
    chunks[had_smallest]++;
    chunk_sizes[had_smallest]--;
  }
  
  if (emptylists >= size-1) {
    sentinel = false;
    break;
  }
  array[idx++] = smallest;
}
\end{minted}

Finally, the master outputs the sorted list and the program ends.
\begin{minted}[]{c}
print_array(array, n);
\end{minted}

\section{Building and Running}
\label{sec:org18d8704}
Build a file with \texttt{mpicc -Wall -Wextra -Werror -o integerSort integerSort.c}, and run with \\
\texttt{mpirun -np <num\_processes> ./integerSort [timer]}
\begin{minted}[]{bash}
    mpicc -Wall -Wextra -Werror -o integerSort ./integerSort.c
    mpirun -np 4 ./integerSort 10 12 5 2 11 0 88 111 47 30 15
\end{minted}

\begin{verbatim}
Master got integers to sort: 10 12 5 2 11 0 88 111 47 30 15
Process 1 got 3 integers: 10 12 5
Process 1 sorted chunk: 5 10 12
Process 2 got 3 integers: 2 11 0
Process 2 sorted chunk: 0 2 11
Process 3 got 5 integers: 88 111 47 30 15
Process 3 sorted chunk: 15 30 47 88 111
 0 2 5 10 11 12 15 30 47 88 111
\end{verbatim}

\section{Appendix}
\label{sec:org7828bf5}
\subsection{Source Code}
\label{sec:org2236133}
\subsubsection{\texttt{read\_int}}
\label{sec:org09d14f9}
\begin{minted}[]{c}
int read_int() {
  int v;
  int err = scanf("%d", &v);

  if (err == EOF) {
    fprintf(stderr, "stdin closed before providing list\n");
    MPI_Finalize();
    exit(-1);
  } else if (err != 1) {
    fprintf(stderr, "user gave incorrect input\n");
    MPI_Finalize();
    exit(-1);
  }

  return v;
}
\end{minted}
\subsubsection{\texttt{print\_array}}
\label{sec:org2ee2c15}
\begin{minted}[]{c}
void print_array(int* array, int n) {
  for (int i = 0; i < n; ++i) {
    printf(" %d", array[i]);
  }
  printf("\n");
}
\end{minted}
\subsubsection{\texttt{check\_array}}
\label{sec:org379b41e}
\begin{minted}[]{c}
// we use void pointer since we don't actually care about the data
// just that it is a pointer to some block of memory
void check_array(void* array) {
  if (array == NULL) {
    fprintf(stderr, "malloc failed\n");
    MPI_Finalize();
    exit(-1);
  }
}
\end{minted}
\subsubsection{integerSort.c}
\label{sec:orge916ab5}
\begin{minted}[]{c}
#include <stdlib.h>
#include <unistd.h> /* unix stdlib, provides access to posix api */
#include <stdio.h> /* standard input output */
#include <stdbool.h> /* c doesn't import booleans by default. I could just use 1 */
#include <mpi.h>

#define MCW MPI_COMM_WORLD

int read_int() {
  int v;
  int err = scanf("%d", &v);

  if (err == EOF) {
    fprintf(stderr, "stdin closed before providing list\n");
    MPI_Finalize();
    exit(-1);
  } else if (err != 1) {
    fprintf(stderr, "user gave incorrect input\n");
    MPI_Finalize();
    exit(-1);
  }

  return v;
}
void print_array(int* array, int n) {
  for (int i = 0; i < n; ++i) {
    printf(" %d", array[i]);
  }
  printf("\n");
}
// we use void pointer since we don't actually care about the data
// just that it is a pointer to some block of memory
void check_array(void* array) {
  if (array == NULL) {
    fprintf(stderr, "malloc failed\n");
    MPI_Finalize();
    exit(-1);
  }
}

int main(int argc, char* argv[]) {
  int rank, size;
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);
  
  int* array = NULL;
  unsigned int n;
  if (rank == 0) {
    if (argc > 1) {
      n = argc - 1;
      array = malloc(n * sizeof(int));
      check_array(array);
    
      for (int i = 1; i < argc; ++i) {
        array[i-1] = atoi(argv[i]);
      }
    } else {
      n = read_int();
      array = malloc(n * sizeof(int));
      check_array(array);
    
      for (unsigned int i = 0; i < n; ++i) {
        array[i] = read_int();
      }
    }
    
    printf("Master got integers to sort:");
    print_array(array, n);
    unsigned int chunk_size = n / (size - 1);
    unsigned int remainder = n % (size - 1);
    for (int i = 1; i < size; ++i) {
      // in the case of size - 1 not diving n evenly,
      // the last process should receive size of chunk_size + remainder
      int num = i == (size - 1) ? chunk_size + remainder : chunk_size;
      MPI_Send(&num, 1, MPI_INT, i, 0, MCW);
      MPI_Send(array + ((i-1) * chunk_size), num, MPI_INT, i, 0, MCW);
    }
    int** chunks = malloc((size-1) * sizeof(int*));
    check_array(chunks);
    int* chunk_sizes = malloc((size-1) * sizeof(int));;
    check_array(chunk_sizes);
    for (int i = 1; i < size; ++i) {
      int num = i == (size - 1) ? chunk_size + remainder : chunk_size;
      chunk_sizes[i-1] = num;
      chunks[i-1] = malloc(num * sizeof(int));
      check_array(chunks[i-1]);
      MPI_Recv(chunks[i-1], num, MPI_INT, i, 0, MCW, MPI_STATUS_IGNORE);
    }
    bool sentinel = false;
    int idx = 0;
    
    while (!sentinel) {
      int emptylists = 0;
      int smallest;
      int had_smallest = -1;
      for (int k = 0; k < size-1; ++k) {
        int chunk_size = chunk_sizes[k];
        if (chunk_size <= 0) {
          ++emptylists;
          continue;
        }
        int* chunk = chunks[k];
        if (had_smallest == -1 || smallest > chunk[0]) {
          smallest = chunk[0];
          had_smallest = k;
        }
      }
      
      if (had_smallest != -1) {
        chunks[had_smallest]++;
        chunk_sizes[had_smallest]--;
      }
      
      if (emptylists >= size-1) {
        sentinel = false;
        break;
      }
      array[idx++] = smallest;
    }
    print_array(array, n);
  } else {
    MPI_Recv(&n, 1, MPI_INT, 0, 0, MCW, MPI_STATUS_IGNORE);
    printf("Process %d got %d integers:", rank, n);
    array = malloc(n * sizeof(int));
    check_array(array);
    MPI_Recv(array, n, MPI_INT, 0, 0, MCW, MPI_STATUS_IGNORE);
    print_array(array, n);
    int nn = n;
    do {
      int newn = 0;
      for (int i = 1; i <= nn-1; ++i) {
        if (array[i - 1] > array[i]) {
          int swap = array[i];
          array[i] = array[i-1];
          array[i-1] = swap;
          newn = i;
        }
      }
      nn = newn;
    } while (!(nn <= 1));
    printf("Process %d sorted chunk:", rank);
    print_array(array, n);
    MPI_Send(array, n, MPI_INT, 0, 0, MCW);
  }
  MPI_Finalize();
  return 0;
}
\end{minted}
\end{document}