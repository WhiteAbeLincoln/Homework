#include <stdlib.h>
#include <unistd.h> /* unix stdlib, provides access to posix api */
#include <stdio.h> /* standard input output */
#include <stdbool.h> /* c doesn't import booleans by default. I could just use 1 */
#include <mpi.h>

#define MCW MPI_COMM_WORLD

int read_int() {
  int v;
  int err = scanf("%d", &v);

  if (err == EOF) {
    fprintf(stderr, "stdin closed before providing list\n");
    MPI_Finalize();
    exit(-1);
  } else if (err != 1) {
    fprintf(stderr, "user gave incorrect input\n");
    MPI_Finalize();
    exit(-1);
  }

  return v;
}
void print_array(int* array, int n) {
  for (int i = 0; i < n; ++i) {
    printf(" %d", array[i]);
  }
  printf("\n");
}
// we use void pointer since we don't actually care about the data
// just that it is a pointer to some block of memory
void check_array(void* array) {
  if (array == NULL) {
    fprintf(stderr, "malloc failed\n");
    MPI_Finalize();
    exit(-1);
  }
}

int main(int argc, char* argv[]) {
  int rank, size;
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);
  
  int* array = NULL;
  unsigned int n;
  if (rank == 0) {
    if (argc > 1) {
      n = argc - 1;
      array = malloc(n * sizeof(int));
      check_array(array);
    
      for (int i = 1; i < argc; ++i) {
        array[i-1] = atoi(argv[i]);
      }
    } else {
      n = read_int();
      array = malloc(n * sizeof(int));
      check_array(array);
    
      for (unsigned int i = 0; i < n; ++i) {
        array[i] = read_int();
      }
    }
    
    printf("Master got integers to sort:");
    print_array(array, n);
    unsigned int chunk_size = n / (size - 1);
    unsigned int remainder = n % (size - 1);
    for (int i = 1; i < size; ++i) {
      // in the case of size - 1 not diving n evenly,
      // the last process should receive size of chunk_size + remainder
      int num = i == (size - 1) ? chunk_size + remainder : chunk_size;
      MPI_Send(&num, 1, MPI_INT, i, 0, MCW);
      MPI_Send(array + ((i-1) * chunk_size), num, MPI_INT, i, 0, MCW);
    }
    int** chunks = malloc((size-1) * sizeof(int*));
    check_array(chunks);
    int* chunk_sizes = malloc((size-1) * sizeof(int));;
    check_array(chunk_sizes);
    for (int i = 1; i < size; ++i) {
      int num = i == (size - 1) ? chunk_size + remainder : chunk_size;
      chunk_sizes[i-1] = num;
      chunks[i-1] = malloc(num * sizeof(int));
      check_array(chunks[i-1]);
      MPI_Recv(chunks[i-1], num, MPI_INT, i, 0, MCW, MPI_STATUS_IGNORE);
    }
    bool sentinel = false;
    int idx = 0;
    
    while (!sentinel) {
      int emptylists = 0;
      int smallest;
      int had_smallest = -1;
      for (int k = 0; k < size-1; ++k) {
        int chunk_size = chunk_sizes[k];
        if (chunk_size <= 0) {
          ++emptylists;
          continue;
        }
        int* chunk = chunks[k];
        if (had_smallest == -1 || smallest > chunk[0]) {
          smallest = chunk[0];
          had_smallest = k;
        }
      }
      
      if (had_smallest != -1) {
        chunks[had_smallest]++;
        chunk_sizes[had_smallest]--;
      }
      
      if (emptylists >= size-1) {
        sentinel = false;
        break;
      }
      array[idx++] = smallest;
    }
    print_array(array, n);
  } else {
    MPI_Recv(&n, 1, MPI_INT, 0, 0, MCW, MPI_STATUS_IGNORE);
    printf("Process %d got %d integers:", rank, n);
    array = malloc(n * sizeof(int));
    check_array(array);
    MPI_Recv(array, n, MPI_INT, 0, 0, MCW, MPI_STATUS_IGNORE);
    print_array(array, n);
    int nn = n;
    do {
      int newn = 0;
      for (int i = 1; i <= nn-1; ++i) {
        if (array[i - 1] > array[i]) {
          int swap = array[i];
          array[i] = array[i-1];
          array[i-1] = swap;
          newn = i;
        }
      }
      nn = newn;
    } while (!(nn <= 1));
    printf("Process %d sorted chunk:", rank);
    print_array(array, n);
    MPI_Send(array, n, MPI_INT, 0, 0, MCW);
  }
  MPI_Finalize();
  return 0;
}
