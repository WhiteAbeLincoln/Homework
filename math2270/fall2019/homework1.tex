% Created 2019-09-05 Thu 10:21
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\textheight=9.25in \textwidth=7in \topmargin=-0.75in \oddsidemargin=-0.25in \evensidemargin=-0.25in \usepackage{mathtools} \usepackage{pgfplots}
\date{\today}
\title{}
\hypersetup{
 pdfauthor={},
 pdftitle={},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\section{Homework 1}
\label{sec:orge74b32b}
\subsection{2.5.1}
\label{sec:orgd99de31}
Write the following mathematical statements in the form of a conditional "if-then"
or a bidirectional "if and only if". Use complete sentances and correct grammar.
(You don't need to prove the statements, just reword them.)

\begin{enumerate}
\item The derivative of the function \(f(x) = e^{-x^2}\) is \(-2xe^{-x^2}\).\\

if \(f(x) = e^{-x^2}\) then \(f'(x) = -2xe^{-x^2}\)

\item A nonnegative number \(x \in \mathbb{R}\) less than any \(\epsilon > 0\) must satisfy \(x = 0\).\\

There exists an \(x\) in \(\mathbb{R}^+\) such that \(x = 0\), if and only if \(x < \epsilon > 0\), where \(\mathbb{R}^+\) is the nonnegative real numbers.

\item For any \(z, w \in \mathbb{C}\), the product \(zw = 0\) is equivalent to either \(z = 0\) or \(w = 0\).\\

For any \(z, w \in \mathbb{C}\), \(zw = 0\) if and only if \(z = 0\) or \(w = 0\).
\end{enumerate}

\subsection{2.5.2}
\label{sec:orga71b51e}
This problem is concerned with the set \(\mathbb{C}\) of all complex numbers. Recall that if
\(z \in \mathbb{C}\), there exists \(a,b \in \mathbb{R}\) such that \(z = a + ib\), where \(i^2 = -1\).
Here we say that \(a\) is the \emph{real} part of \(z\), written \(a = Re(z)\), and that \(b\) is the \emph{imaginary}
part of \(z\), written \(b = Im(z)\). Every complex number can be regarded as an ordered pair
\((a,b) \in \mathbb{R}^2\), where the \emph{x}-axis represents \(Re(z)\) and the \emph{y}-axis represents \(Im(z)\).
This is why we refer to \(\mathbb{C}\) as the complex \emph{plane}.

\begin{enumerate}
\item Use set builder notation to write \(\mathbb{C}\) as a set.\\

\(\mathbb{C} = \{ a + bi \mid a, b \in \mathbb{R}, i^2 = -1 \}\)
\item The complex numbers form a field. We will show this in several steps. First show that \(\mathbb{C}\)
      is closed under addition and subtraction by showing that if \(z,w \in \mathbb{C}\), then
      \(z \pm w \in \mathbb{C}\). (\emph{Hint}. What are the real and imaginary parts of \(z \pm w\)?)\\

Let \(z,w \in \mathbb{C}\). Addition in \(\mathbb{C}\) is performed component-wise,
adding the real and imaginary parts together, following the rules of addition for the real numbers.
Therefore, if we let \(z = a + bi\), and \(w = c + di\),
\begin{align*}
z + w &= (a + bi) + (c + di) \\
&= (a + c) + (bi + di) \\
&= (a + c) + (b + d)i \\
\shortintertext{let $x = a + c, y = b + d$}
&= x + yi
\end{align*}
By the definition of \(\mathbb{C}\), we know that \(a, b, c, d \in \mathbb{R}\). It follows that
\(x, y \in \mathbb{R}\), because \(\mathbb{R}\) is a field and closed under addition. Therefore
\(x + yi \in \mathbb{C}\), because it fulfills the form
\(\{a + bi \mid a, b \in \mathbb{R}, i^2 = -1 \}\).

A similar procedure can be followed to prove that \(\mathbb{C}\) is closed under subtraction:
\begin{align*}
z - w &= (a + bi) - (c + di) \\
&= (a + bi) + (-c + -di) \\
\shortintertext{let $f = -c, g = -d$}
&= (a + b_zi) + (f + gi)
\end{align*}
Since \(f,g \in \mathbb{R}\), we have reduced this problem to the above proof of closure under
addition. Therefore we can say \(\mathbb{C}\) is also closed under subtraction.

\item Show that \(\mathbb{C}\) is closed under multiplication by showing that \(zw \in \mathbb{C}\) for any
      \(z,w \in \mathbb{C}\). Furthermore, show that \(zw = wz\).\\

Let \(z,w \in \mathbb{C}\). Multiplication in \(\mathbb{C}\) is distributive.
Let \(z = a + bi\), and \(w = c + di\).
\begin{align*}
zw &= (a + bi)(c + di) \\
&= a c + a d i + c b i + b d i^2 \\
&= a c + a d i + c b i - b d && \text{$i^2 = -1$ by definition of $\mathbb{C}$} \\
&= a c - b d + a d i + c b i \\
&= (a c - b d) + (a d + c b)i \\
\intertext{let $x = (a c - b d), y = (a d + c b)$}
&= x + yi \\
\end{align*}
By the definition of \(\mathbb{C}\), we know that \(a, b, c, d \in \mathbb{R}\). It follows that
\(x, y \in \mathbb{R}\), because \(\mathbb{R}\) is a field and closed under addition and multiplication.
Therefore \(x + yi \in \mathbb{C}\), because it fulfills the form
\(\{a + bi \mid a, b \in \mathbb{R}, i^2 = -1 \}\).

Next we prove multiplicative commutativity, \(zw = wz\):
\begin{align*}
zw &= wz \\
(a + bi)(c + di) &= (c + di)(a + bi) \\
a c + a d i + b i c + bi di &= c a + c b i + d i a + di bi \\
\intertext{Multiplication is commutative in $\mathbb{R}$, and $a,b,c,d \in \mathbb{R}$}
a c + a d i + c b i + d b i^2 &= a c + a d i + c b i t+_w b i^2 \\
a c + a d i + c b i - d b &= a c + a d i + c b i - d b \\
a c - b d + a d i + c b i &= a c - b d + a d i + c b i \\
(a c - b d) + (a d + c b)i &= (a c - b d) + (a d + c b)i \\
\intertext{Substitute the earlier definitions of $x = (ac - bd)$ and $y = (ad + cb)$}
x + yi &= x + yi
\end{align*}

\item Show that \(\mathbb{C}\) is closed under divisiton by showing that \(z/w \in \mathbb{C}\) for any
      \(z,w \in \mathbb{C}\) with \(w \ne 0\). (\emph{Hint}. The complex conjugate \(\bar{w}\) will make a showing here.)\\

Let \(z = a + bi, w = c + di\).
\begin{align*}
\frac{z}{w} &= \frac{a + bi}{c + di}
\shortintertext{Multiply by 1, using the conjugate of $w$}
&= \frac{(a + bi)(c - di)}{(c + di)(c - di)} \\
&= \frac{a c - a d i + c b i - b d i^2}{c^2 - d^2i^2} \\
&= \frac{a c - a d i + c b i + b d}{c^2 + d^2} \\
&= \frac{(a c + b d) + (c b - a d) i}{c^2 + d^2} \\
\intertext{Let $r$ be the real number $c^2 + d^2$, $x = a c + b d$, $y = c b - a d$}
&= \frac{x + y i}{r} \\
&= \frac{x}{r} + \frac{y}{r} i
\end{align*}
Note that both \(x/r\) and \(y/r\) are real numbers, since \(w \ne 0\), so \((r = c^2 + d^2) \ne 0\).
Since \(\frac{x}{r} + \frac{y}{r}i\) fits the form \(\{a + bi \mid a, b \in \mathbb{R}, i^2 = -1 \}\), we know
that \(\mathbb{C}\) is closed under division.

\item Show that if \(z \in \mathbb{C}\) is nonzero, there exists a multiplicative inverse \(z^{-1} \in \mathbb{C}\)
      such that \(zz^{-1} = 1 = z^{-1}z\). Hence, \(\mathbb{C}\) is a field. (\emph{Hint}. Use part 4 to pick the real
      and imaginary parts of a complex number \(w \neq 0\) such that \(zw = 1\). By part 3, the multiplication is
      commutative, so what you will do is sufficient to conclude \(w = z^{-1}\)).\\

Let \((w = c + di) \in \mathbb{C} - \{0\}\) such that \(zw = 1\) \\
Show that \(w\) exists and so \(w = z^{-1}\):
\begin{align*}
  1 + 0i &= zw \\
  \shortintertext{Let $z = a + bi$}
  &= (a + bi)(c + di) \\
  &= a c + a d i + c b i + b d i^2 \\
  &= a c + a d i + c b i - b d \\
  &= (a c - b d) + (a d i + c b i) \\
  &= (a c - b d) + (a d + c b)i \\
  \shortintertext{We see that $1 = a c - b d$, and $0 = a d + c b$}
  \shortintertext{Solve for $c$ and $d$ to get a complete representation of $w$}
  1 &= a c - b d \\
  0 &= b c + a d
  \shortintertext{Manipulate the system so that $d$ can be eliminated}
  \shortintertext{Multiply the first equation by $a$, and the second equation by $b$}
  a &= a^2 c - a b d \\
  0 &= b^2 c + a b d
  \shortintertext{Sum the two equations}
  a &= a^2 c + b^2 c
  \shortintertext{Solve for $c$}
  c &= \frac{a}{a^2 + b^2}
  \shortintertext{Solve the first equation for $d$ in terms of $a, b, c$}
  1 &= a c - b d \\
  1 - a c &= - b d \\
  \frac{ac - 1}{b} &= d
  \shortintertext{Substitute $c$ to get $d$ in terms of $a$ and $b$}
  d &= \frac{ac - 1}{b} \\
  &= \frac{1}{b}(a\frac{a}{a^2 + b^2} - 1) \\
  &= \frac{1}{b}(\frac{a^2}{a^2 + b^2} - 1) \\
  &= \frac{1}{b}(\frac{a^2}{a^2 + b^2} - \frac{a^2 + b^2}{a^2 + b^2}) \\
  &= \frac{1}{b}(\frac{a^2 - (a^2 + b^2)}{a^2 + b^2}) \\
  &= \frac{1}{b}(\frac{-b^2}{a^2 + b^2}) \\
  &= \frac{-b}{a^2 + b^2}
  \shortintertext{This gives us $w$}
  w &= \frac{a - bi}{a^2 + b^2}
  \shortintertext{Show that $w$ is a valid complex number}
  w &= \frac{a}{a^2 + b^2} + - \frac{b}{a^2 + b^2}i
  \shortintertext{Let $x = \frac{a}{a^2 + b^2}$ and $y = - \frac{b}{a^2 + b^2}$}
  w &= x + yi
  \shortintertext{$x$ and $y$ are real numbers, and $w$ fits the form $a + bi$, so $w \in \mathbb{C}$}
\end{align*}

\item Show that the set of all integers \(\mathbb{Z}\) is not a field by showing that at least one of the parts
      of the definition fails. Find a concrete counterexample as apposed to a vague statement.\\

For \(\mathbb{Z}\) to be a field, it must be closed under division. Consider two integers \(a, b\). 
The fraction \(a/b\) may not be an integer, e.g. if \(a = 1\) and \(b = 3\), which gives us
a rational number. Therefore, \(\mathbb{Z}\) is not a field, because it is not closed under division.

\item Graph the complex number \(z = 1 + i, w = -2 + 3i\) and their complex conjugates \(\bar{z}\) and \(\bar{w}\).
      Furthermore, graph \(zw\) and \(z/w\).

\begin{tikzpicture}
\begin{axis}[xmin=-5.5,
             xmax=5.5,
             ymin=-5.5,
             ymax=5.5,
             axis equal,
             axis lines=middle,
             xlabel=Re($z$),
             ylabel=Im($z$),
             disabledatascaling]
   \addplot[mark=*] coordinates {(1,1)} node[label=45:{$z$}]{} ;
   \addplot[mark=*] coordinates {(-2,3)} node[label=150:{$w$}]{} ;

   \addplot[mark=*] coordinates {(1,-1)} node[label=-45:{$\bar{z}$}]{} ;
   \addplot[mark=*] coordinates {(-2,-3)} node[label=150:{$\bar{w}$}]{} ;

   \addplot[mark=*] coordinates {(-5,1)} node[label=150:{$zw$}]{} ;
   \addplot[mark=*] coordinates {(1/13,-5/13)} node[label=100:{$z/w$}]{} ;
\end{axis}
\end{tikzpicture}
\end{enumerate}

\subsection{2.5.3}
\label{sec:org542b896}
Consider the set
$$
   \mathbb{Q}[\sqrt{2}] = \{a + b\sqrt{2} \mid a,b \in \mathbb{Q}\}
   $$
This set is read as \(\mathbb{Q}\) \emph{adjoin} \(\sqrt{2}\). Following the steps in parts 2, 3, 4, 5 of the previous
problem, show that \(\mathbb{Q}[\sqrt{2}]\) is a field.

\begin{enumerate}
\item The set is closed under addition and subtraction \\

Let \(z = a + b\sqrt{2}, w = c + d\sqrt{2}\) be elements of the set \(\mathbb{Q}[\sqrt{2}]\). \\

Show that \(z + w \in \mathbb{Q}[\sqrt{2}]\):
\begin{align*}
  z + w &= (a + b\sqrt{2}) + (c + d\sqrt{2})
  \shortintertext{$a,b,c,d \in \mathbb{Q}$ so we can use its properties of commutativity and distributivity}
  &= a + c + b\sqrt{2} + d\sqrt{2} \\
  &= (a + c) + (b + d)\sqrt{2}
  \shortintertext{let $x = a + c, y = b + d$}
  &= x + y\sqrt{2}
  \shortintertext{$z + w \in \mathbb{Q}[\sqrt{2}]$ because the result has the form $x + y\sqrt{2}$ where x,y \in \mathbb{Q}$}
\end{align*}
Show that \(z - w \in \mathbb{Q}[\sqrt{2}]\):
\begin{align*}
  z + w &= (a + b\sqrt{2}) - (c + d\sqrt{2})
  \shortintertext{$a,b,c,d \in \mathbb{Q}$ so we can use its properties of commutativity and distributivity}
  &= a - c + b\sqrt{2} - d\sqrt{2} \\
  &= (a - c) + (b - d)\sqrt{2}
  \shortintertext{let $x = a - c, y = b - d$}
  &= x + y\sqrt{2}
  \shortintertext{$z - w \in \mathbb{Q}[\sqrt{2}]$ because the result has the form $x + y\sqrt{2}$ where x,y \in \mathbb{Q}$}
\end{align*}

\item The set is closed under multiplication and is commutative \\

Let \(z = a + b\sqrt{2}, w = c + d\sqrt{2}\) be elements of the set \(\mathbb{Q}[\sqrt{2}]\). \\

Show that \(zw \in \mathbb{Q}[\sqrt{2}]:\)
\begin{align*}
  zw &= (a + b\sqrt{2})(c + d\sqrt{2}) \\
  &= ac + ad\sqrt{2} + cb\sqrt{2} + 2bd \\
  &= (ac + 2bd) + (ad + cb)\sqrt{2}
  \shortintertext{let $x = ac + 2bd, y = ad + cb$}
  &= x + y\sqrt{2}
  \shortintertext{$zw \in \mathbb{Q}[\sqrt{2}]$ because the result has the form $x + y\sqrt{2}$ where x,y \in \mathbb{Q}$}
\end{align*}

Show that \(zw = wz\):
\begin{align*}
  zw &= wz \\
  (a + b\sqrt{2})(c + d\sqrt{2}) &= (c + d\sqrt{2})(a + b\sqrt{2}) \\
  ac + ad\sqrt{2} + cb\sqrt{2} + 2bd &= ac + ad\sqrt{2} + cb\sqrt{2} + 2bd \\
  (ac + 2bd) + (ad + cb)\sqrt{2} &= (ac + 2bd) + (ad + cb)\sqrt{2} \\
  \shortintertext{Substitute the earlier definitions of $x$ and $y$}
  x + y\sqrt{2} &= x + y\sqrt{2}
  \shortintertext{$zw$ is obviously equal to $wz$ because their representations are equal}
\end{align*}

\item The set is closed under division \\

Let \(z = a + b\sqrt{2}, w = c + d\sqrt{2}\) be elements of the set \(\mathbb{Q}[\sqrt{2}]\). \\

Show that \(z/w \in \mathbb{Q}[\sqrt{2}]:\)
\begin{align*}
  z/w &= \frac{a + b\sqrt{2}}{c + d\sqrt{2}}
  \shortintertext{Multiply by 1 using the conjugate of $w$}
  &= \frac{(a + b\sqrt{2})(c - d\sqrt{2})}{(c + d\sqrt{2})(c - d\sqrt{2})} \\
  &= \frac{ac - ad\sqrt{2} + cb\sqrt{2} - 2bd}{c^2 + 2d^2} \\
  &= \frac{(ac - 2bd) + (cb - ad)\sqrt{2}}{c^2 + 2d^2}
  \shortintertext{let $t = ac - 2bd, u = cb - ad, v = c^2 + 2d^2$}
  &= \frac{t + u\sqrt{2}}{v} \\
  &= \frac{t}{v} + \frac{u}{v}\sqrt{2}
  \shortintertext{$t,u,v \in \mathbb{Q}$, and $\mathbb{Q}$ is closed under division, so we can let $x = t/v, y = u/v$}
  &= x + y\sqrt{2} \\
  \shortintertext{$z/w \in \mathbb{Q}[\sqrt{2}]$ because the result has the form $x + y\sqrt{2}$ where x,y \in \mathbb{Q}$}
\end{align*}

\item There exists a multiplicative inverse: \\

Let \(z = a + b\sqrt{2}, w = c + d\sqrt{2}\) be non-zero elements of the set \(\mathbb{Q}[\sqrt{2}]\). \\

Find the form of \(w\) such that \(zw = 1\):
\begin{align*}
  1 + 0\sqrt{2} &= zw \\
  &= (a + b\sqrt{2})(c + d\sqrt{2}) \\
  &= ac + ad\sqrt{2} + cb\sqrt{2} + 2bd \\
  &= (ac + 2bd) + (ad + cb)\sqrt{2}
  \shortintertext{It is evident that $1 = ac + 2bd$, and $0 = ad + cb$}
  \shortintertext{Solve the system for the components of $w$, $c$ and $d$}
  1 &= ac + 2bd \\
  0 &= bc + ad \\
  \shortintertext{Manipulate the system so that $d$ can be eliminated}
  \shortintertext{Multiply the first equation by $a$, and the second equation by $-2b$}
  a &= a^2c + 2abd \\
  0 &= -2b^2c - 2abd
  \shortintertext{Sum the two equations}
  a &= a^2c - 2b^2c
  \shortintertext{Solve for $c$}
  &= (a^2 - 2b^2)c \\
  c &= \frac{a}{a^2 - 2b^2}
  \shortintertext{Solve the first equation in terms of $a,b,c$}
  1 &= ac + 2bd \\
  1 - ac &= 2bd \\
  \frac{1 - ac}{2b} &= d
  \shortintertext{Substitute $c$ to get $d$ in terms of $a$ and $b$}
  d &= \frac{1 - ac}{2b} \\
  &= \frac{1}{2b}(1 - a\frac{a}{a^2 - 2b^2}) \\
  &= \frac{1}{2b}(\frac{a^2 - 2b^2}{a^2 - 2b^2} - \frac{a^2}{a^2 - 2b^2}) \\
  &= \frac{1}{2b}(\frac{a^2 - 2b^2 - a^2}{a^2 - 2b^2}) \\
  &= \frac{1}{2b}(\frac{- 2b^2}{a^2 - 2b^2}) \\
  &= \frac{-b}{a^2 - 2b^2}
  \shortintertext{This gives us $w$}
  w &= \frac{a}{a^2 - 2b^2} + \frac{-b}{a^2 - 2b^2}\sqrt{2}
  \shortintertext{let $x$ be the rational $\frac{a}{a^2 - 2b^2}$, and $y$ be the rational $\frac{-b}{a^2 - 2b^2}$}
  w &= x + y\sqrt{2}
  \shortintertext{Therefore $w \in \mathbb{Q}[\sqrt{2}]$ because it has the form $x + y\sqrt{2}$ where x,y \in \mathbb{Q}$}
  \shortintertext{This means that $w$ is the multiplicative inverse $z^{-1}$}
\end{align*}
\end{enumerate}

\subsection{2.5.4}
\label{sec:org92fe28c}
\begin{enumerate}
\item Give the definition of a vector space \(V\) over a field \(K\). \\

A vector space \(V\) over a field \(K\) is a set, closed under the operations vector addition and scalar multiplication,
which must satisfy the following:

\begin{enumerate}
\item Vector addition must be: associative, commutative, and have an identity and inverse.
\item Scalar multiplication must be: distributive over field and vector addition, and have an identity.
\end{enumerate}

\item Give three examples of vector spaces. Make sure to identify the ground field \(K\) in
      each example. Explain in complete sentences why the sets you provide are vector spaces.

\begin{enumerate}
\item The zero vector space over any field \(K\). The only vector addition possible is \(\mathbf{0} + \mathbf{0} = \mathbf{0}\),
         which is obviously closed. Since there is only one element in the vector space, addition is associative and
         commutative. The additive inverse of \(\mathbf{0}\) is \(\mathbf{0}\), and \(\mathbf{0} - \mathbf{0} = \mathbf{0}\).
         Multiplication of any scalar \(a \in K\) by the zero vector is just \(\mathbf{0}\), which is closed. Distributivity
         of scalar multiplication is easily verifiable as with vector addition.
\item The vector space \(\mathbb{R}^2\), over the field \(\mathbb{R}\). Vector addition is peformed component-wise: given
         \(\mathbf{x} = (a,b)\) and \(\mathbf{y} = (c,d) \in \mathbb{R}^2\), \(\mathbf{x} + \mathbf{y} = (a + c, b + d)\). It follows
         from \(\mathbb{R}\) that addition is closed, commutative, associative, and has additive inverses. Scalar multiplication
         distributes over the components: given \(c \in \mathbb{R}\), and \(\mathbf{x} = (a,b) \in \mathbb{R}^2\), \(c\mathbf{x} = (ac,bc)\).
         This means that \(\mathbb{R}^2\) is closed under scalar multiplication. The distributivity of scalar multiplication follows
         from vector addition and scalar addition in \(\mathbb{R}\).
\item The nth degree polynomial vector space over \(\mathbb{R}\). Vector addition is the field addition of the components of two
         polynomials of degree less than or equal to \(n\), which yields another polynomial with degree less than or equal to \(n\),
         making the system closed. Scalar multiplication distributes over the coefficients of the polynomial, and so cannot change
         the degree. This means the system remains closed under scalar multiplication. Addition is both commutative and associative,
         because the component parts of the vector are in \(\mathbb{R}\), which has commutative and associative addition. Similarly,
         scalar multiplication distributivity follows from \(\mathbb{R}\).
\end{enumerate}
\end{enumerate}

\subsection{2.5.5}
\label{sec:orgdbbd6f2}
Consider the vector space \(\mathbb{R}^2\) and let \(W = \{[x,y]^T \mid y > 0\} \subset \mathbb{R}^2\)
be the upper half plane. We will show that \(W\) is not a subspace of \(\mathbb{R}^2\).
\begin{enumerate}
\item Show that if \(\mathbf{v_1},\mathbf{v_2} \in W\), then \(\mathbf{v_1} + \mathbf{v_2} \in W\). (This shows that \(W\) is closed
      under vector addition.)
\begin{align*}
\shortintertext{let $\mathbf{v_1} = [a,b]^T$ and $\mathbf{v_2} = [c,d]^T$}
\mathbf{v_1} + \mathbf{v_2} &= [a, b]^T + [c, d]^T \\
&= [a+c, b+d]^T
\shortintertext{Let $x = a+c$, $y=b+d$}
&= [x, y]^T \\
\shortintertext{Because by the definition of $W$, $b, d \in \mathbb{R}_{>0}$. This means $[x, y]^T \in W$}
\end{align*}

\item Show that \(W\) is not closed under scalar multiplication. Hence, \(W\) is not a subspace of \(\mathbb{R}^2\).\\
      (\emph{Hint}. Pick your favorite vector \(v \in \mathbb{W}\).)
\begin{align*}
\shortintertext{let $\mathbf{v} = [a,b]^T$ and $c = \in \mathbb{R}$}
c\mathbf{v} &= c[a, b]^T \\
&= [ca, cb]^T \\
\end{align*}
Suppose \(c\) was a negative number. This means the quantity \(cb \not> 0\), and so \([ca, cb]^T \not\in W\).
Hence, \(W\) is not closed under scalar multiplication.
\end{enumerate}
\end{document}