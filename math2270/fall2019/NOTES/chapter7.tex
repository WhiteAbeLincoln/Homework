% Created 2019-11-18 Mon 19:59
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\input{../../../preamble.tex}
\author{Abraham White}
\date{\today}
\title{Chapter 7}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Chapter 7},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Tensor Product of Covectors}
\label{sec:org4bb8059}
The tensor product of two covectors yields a two argument function, a bilinear form \(V \times V \to K\).
Partially applying this function gives us a function \(V \to V^*\). This is similar to the dot product,
perhaps a generalization. The tensor product is not commutative in general, since
\(\phi \otimes \omega \neq \omega \otimes \phi\) for all \(\phi\) and \(\omega\).

\subsection*{Definitions, Theorems, and Lemmas}
\label{sec:org5160684}
\textbf{Tensor Product}
\begin{equation}
(\omega \otimes \phi)(\vect{v},\vect{w}) = \omega(\vect{v})\phi(\vect{u}) \in K \tag{7.2}
\end{equation}
\begin{definition*}{7.2}
A mapping $g : V \times V \to K$ is called a \textbf{bilinear form} if for all
$\vect{v},\vect{u},\vect{w} \in V$, and for all $c, d \in K$, we have
\[ g(c\vect{v} + d\vect{w}, \vect{u}) = cg(\vect{v}, \vect{u}) + dg(\vect{w}, \vect{u}) \]
\[ g(\vect{v},c\vect{u} + d\vect{w}) = cg(\vect{v}, \vect{u}) + dg(\vect{v}, \vect{w}) \]
\end{definition*}
\begin{definition*}{7.4}
The \textbf{transpose} of a column vector \[ \vect{v} = \mat{c^1 \\ \vdots \\ c^n} \] is the row vector
$\vect{v}^T = \mat{c^1 & \cdots & c^n}$. The transpose of a row vector $\vect{r} = \mat{d_1 & \cdots & d_n}$
is the column vector \[ \vect{r}^T = \mat{d_1 \\ \vdots \\ d_n} \] The transpose of an $m \times n$ matrix
$A = \mat{\vect{v}_1 \ldots \vect{v}_n}$ is the $n \times m$ matrix
\[ A^T = \mat{\vect{v}_1^T \\ \vdots \\ \vect{v}^T_n} \] In terms of the index structure, if $A = [a^i_j]$ is 
the matrix of a linear transformation, the transpose has components $A^T = [a_i^j]$. If $A = [a_{ij}]$ is the
matrix of a bilinear from, the transpose has components $A^T = [a_{ji}]$.
\end{definition*}
\begin{definition*}{7.8}
Let $g_A : V \times V \to K$ be bilinear form. The \textbf{quadratic form} associated to $g_A$ is the
expression \[ Q_A(\vect{v}) = g_A(\vect{v},\vect{v}) \]
\end{definition*}


\section*{Bilinear Form}
\label{sec:org7265e68}
\begin{description}
\item[{Bilinear Form}] A bilinear map \(V \times V \to K\) where \(V\) is a vector space and \(K\) is the scalar field.
In other words, a function of two arguments that is linear in each argument separately.
The tensor product satisifies bilinearity.

\item[{Transpose}] Interchanges the rows and the columns
\end{description}
\subsection*{Definitions, Theorems, and Lemmas}
\label{sec:org9667ec5}
\begin{definition*}{7.2}
A mapping $g : V \times V \to K$ is called a \textbf{bilinear form} if for all
$\vect{v},\vect{u},\vect{w} \in V$, and for all $c, d \in K$, we have
\[ g(c\vect{v} + d\vect{w}, \vect{u}) = cg(\vect{v}, \vect{u}) + dg(\vect{w}, \vect{u}) \]
\[ g(\vect{v},c\vect{u} + d\vect{w}) = cg(\vect{v}, \vect{u}) + dg(\vect{v}, \vect{w}) \]
\end{definition*}
\begin{definition*}{7.4}
The \textbf{transpose} of a column vector \[ \vect{v} = \mat{c^1 \\ \vdots \\ c^n} \] is the row vector
$\vect{v}^T = \mat{c^1 & \cdots & c^n}$. The transpose of a row vector $\vect{r} = \mat{d_1 & \cdots & d_n}$
is the column vector \[ \vect{r}^T = \mat{d_1 \\ \vdots \\ d_n} \] The transpose of an $m \times n$ matrix
$A = \mat{\vect{v}_1 \ldots \vect{v}_n}$ is the $n \times m$ matrix
\[ A^T = \mat{\vect{v}_1^T \\ \vdots \\ \vect{v}^T_n} \] In terms of the index structure, if $A = [a^i_j]$ is 
the matrix of a linear transformation, the transpose has components $A^T = [a_i^j]$. If $A = [a_{ij}]$ is the
matrix of a bilinear from, the transpose has components $A^T = [a_{ji}]$.
\end{definition*}
\begin{definition*}{7.8}
Let $g_A : V \times V \to K$ be bilinear form. The \textbf{quadratic form} associated to $g_A$ is the
expression \[ Q_A(\vect{v}) = g_A(\vect{v},\vect{v}) \]
\end{definition*}

\section*{Transpose}
\label{sec:orgeb81e14}
Partial application of a bilinear form (or currying) gives us a covector. For instance, given a bilinear form
\(g_A : \RR^n \times \RR^n \to \RR\), we can fix one of the inputs, which gives us a covector \(\RR^n \to R\).
Given \(g_A\) and a fixed \(\vect{x} \in \RR^n\), the mapping \[ \omega_x : \RR^n \to \RR \]
\[ \omega_x = g_A(\vect{x},\cdot) \] is a covector. \(g_A\) is defined as \(\vect{x}^TA\vect{y}\), so
\(\omega_x = \vect{x}^T A \in \RR^{n*}\).
\end{document}