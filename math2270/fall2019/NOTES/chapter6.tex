% Created 2019-11-17 Sun 23:05
% Intended LaTeX compiler: pdflatex
\documentclass[10pt,AMS Euler]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\input{../../../preamble.tex}
\author{Abraham White}
\date{\today}
\title{Chapter 6}
\hypersetup{
 pdfauthor={Abraham White},
 pdftitle={Chapter 6},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Linear Functionals and Dual Spaces}
\label{sec:org58fa156}
\begin{description}
\item[{Linear Functional}] A mapping from some vector space \(V\) to it's ground field \(K\). Consider the vector
space \(\RR^3\), which has the field \(\RR\). Multiplying a \(1 \times 3\) vector by a \(3
                         \times 1\) vector will yield a \(1 \times 1\) vector, which is just a scalar in \(\RR\).
This is an example of a linear functional.

\item[{Dual Space}] The collection of all \textbf{linear functionals} on a vector space \(V\). That is, the collection of
all mappings \(V \to K\) for some vector space \(V\) with field \(K\).

\item[{Dual Vector/Covector}] An element \(\omega\) of a \textbf{dual space} \(V^*\)
\end{description}

\subsection*{Definitions, Theorems, and Lemmas}
\label{sec:org16576f7}
\begin{definition*}{6.1}
Let $V$ be a vector space over $K$. A \textbf{linear functional} on $V$ is a linear mapping
$\omega : V \to K$. The collection of all linear functionals on $V$ naturally forms a vector space over $K$,
called the \textbf{dual space} of $V$, and is denoted by $V^*$. We will refer to an element of
$\omega \in V^*$ as a \textbf{dual vector} or a \textbf{covector}.
\end{definition*}
\begin{lemma*}{6.3}
Let $\omega : K^n \to K$ be a linear functional. Then there exists an $1 \times n$ row vector $\vect{r}$ such
that $\omega(\vect{v}) = \vect{r}\vect{v}$ for every $\vect{v} \in K^n$.
\end{lemma*}
\begin{theorem*}{6.4}
The dual space $K^{n*}$ is an n-dimensional vector space over $K$. This vector space structure is the same as
the natural vector space structure on the collection of all $1 \times n$ row vectors.
\end{theorem*}

\section*{Dual Basis and Annihilators}
\label{sec:org5901172}
\begin{description}
\item[{Dual Basis}] The basis of the dual space \(K^{n*}\), which is the set of \textbf{covectors}
 \(\{\epsilon^1,\ldots,\epsilon^n\} \subset K^{n*}\) where the \textit{i}th element of
 \(\epsilon^i\) is 1 and the others are 0. Considered to be the \emph{dual} of the standard
basis of \(K^n\).
\item[{Annihilator}] The subspace consisting of all elements \(\omega\) in the dual space \(V^*\)
(all \textbf{linear functionals}) such that \(\omega(\vect{v}) = 0\) for all vectors \(\vect{v}\) in
\(W \subset V\).
\end{description}

\subsection*{Defintions, Theorems, and Lemmas}
\label{sec:org62a3478}
\begin{definition*}{6.6}
The \textbf{Kronecker delta} $\delta_j^i$ is defined as
\[ \delta_j^i = \begin{cases}1 & i = j \\ 0 & i \neq j\end{cases} \]
For example, the condition for a dual basis is given by
\[ \omega^i(\vect{v}_j) = \delta_j^i \]
and the matrix with components $\delta_j^i$ is the identity matrix $I_n$,
\[ I_n = [\delta_j^i] \]
\end{definition*}
\begin{definition*}{6.8}
Let $W \subseteq V$ be a subspace. Then the \textbf{annihilator} of $W$ is the subspace
\[ \ann(W) = \{ \omega \in V^* \mid \omega(\vect{v}) = 0~~\forall \vect{v} \in W \} \]
\end{definition*}

\section*{Pullback}
\label{sec:org036df9b}
\begin{description}
\item[{Pullback}] A transformation between \textbf{dual spaces}. If we have a linear transformation \(T : V \to W\) for
vector spaces \(V\) and \(W\) over a field \(K\), then the \textbf{pullback} is the mapping
\(T^* : W^* \to V^*\).
\end{description}

\textbf{Note} After choosing a basis for the vector spaces in some transformation \(\Psi = V \to V^*\), the
transpose \(\vect{r} \mapsto \vect{r}^T\) defines a linear isomorphism \(K^{n*} \cong K^n\). We can think of the
transpose \(A^T\) as being representative of the pullback \(T^* : W^* \to V^*A\).
\subsection*{Defintions, Theorems, and Lemmas}
\label{sec:org698d3e8}
\begin{definition*}{6.10}
Let $V$ and $W$ be vector spaces over $K$ and $T : V \to W$ be a linear transformation. Then the
\textbf{pullback} of $T$ is the mapping $T^* : W^* \to V^*$ defined by
\[ (T^*(\omega))(\vect{v}) = \omega(T(\vect{v})) \] for any $\omega \in W^*$ and for all $\vect{v} \in V$.
\end{definition*}
\begin{corollary*}{6.14}
Let $V$ be an n-dimensional vector space over $K$. Then the dual space $V^*$ is also n-dimensional over $K$.
In particular $V \cong V^*$.
\end{corollary*}

\section*{Tensor Product}
\label{sec:orge195a0e}
\begin{description}
\item[{Tensor Product}] A generalization of the outer product between two vector spaces. The tensor product allows
the creation of arbitrary linear transformations \(T : V \to W\) if we use vectors
\(\vect{w} \in W\) and \(\phi \in V^*\) in the product \(\vect{w} \otimes \phi\).
\end{description}
\subsection*{Definitions, Theorems, and Lemmas}
\label{sec:orga8a48cf}
\begin{definition*}{6.17}
Let $\vect{v} \in K^m$ and $\vect{r} \in K^{n*}$. We define the \textbf{tensor product} of $\vect{v}$ and
$\vect{r}$ to be the $m \times n$ matrix given by \[ \vect{v} \otimes \vect{r} = \vect{v}\vect{r} \]
This tensor product naturally defines a linear transformation $T : K^n \to K^m$ by
$T(\vect{x}) = (\vect{v} \otimes \vect{r})\vect{x}$. Similarly, let $V$ and $W$ be finite dimensional spaces
over $K$; let $\vect{w} \in W$ and $\phi \in V^*$. Then we define the tensor product $\vect{w} \otimes \phi$
as a mapping $V \to W$ by \[ (\vect{w} \otimes \phi)(\vect{v}) = \phi(\vect{v})\vect{w} \]
for any $\vect{v} \in V$. In this way, the mapping $T : V \to W$ defined by
\[ T(\vect{v}) = (\vect{w} \otimes \phi)(\vect{v}) \] is naturally a linear mapping.
\end{definition*}
\end{document}