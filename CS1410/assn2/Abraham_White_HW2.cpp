#include <iostream>
#include <fstream>

using namespace std;

class IPRecord {
    int frequency;
    int address;
public:
    IPRecord() {
        frequency = 1;
    }
    IPRecord(int address) {
        this->setAddress(address);
        frequency = 1;
    }
    void incrementFreq() {
        frequency++;
    }
    void setAddress(int address) {
        this->address = address;
    }
    int getFrequency() {
        return frequency;
    }
    int getAddress() {
        return address;
    }
};

class IPTable {
    IPRecord* table;
    int tableCount;

    // because I wanted to make a recursive one
    int binarySearch(IPRecord arr[], int key, int first, int last) {
        // check for empty array
        if (last < first) {
            return -1;
        } else {
            int mid = (first + last) / 2;

            if (arr[mid].getAddress() == key)
                // key has been found
                return mid;

            if (arr[mid].getAddress() < key)
                // key is in the right half of the array
                return binarySearch(arr, key, mid+1, last);
            else
                // key is in the left half of the array
                return binarySearch(arr, key, first, mid - 1);
        }
    }
    void insertionSort(IPRecord arr[]) {
        int j;
        IPRecord temp;

        // starting at element 1 and continuing to the last element,
        for(int i = 1; i < tableCount; i++) {
            j = i;
            // while j is greater than 0 (the first element) and the element previous is greater than the element at j
            while (j > 0 && arr[j-1].getAddress() > arr[j].getAddress()) {
                // swap the two elements
                temp = arr[j];
                arr[j] = arr[j-1];
                arr[j-1] = temp;
                // decrement j
                j--;
            }
        }
    }
    // copypasta from my above insertion sort, with small modifications
    void reverseInsertionSortByFreq(IPRecord arr[]) {
        int j;
        IPRecord temp;

        // starting at element 1 and continuing to the last element,
        for(int i = 1; i < tableCount; i++) {

            j = i;
            // while j is greater than 0 (the first element) and the element previous is less than the element at j
            while (j > 0 && arr[j-1].getFrequency() < arr[j].getFrequency()) {
                // swap the two elements
                temp = arr[j];
                arr[j] = arr[j-1];
                arr[j-1] = temp;
                // decrement j
                j--;
            }
        }
    }
public:
    IPTable() {
        table = new IPRecord[10000];
        tableCount = 0;
    }
    // clean up after ourselves, delete the table pointer in the destructor
    ~IPTable() {
        delete[] table;
    }
    IPRecord* BinarySearch(int address) {
        int location = binarySearch(this->table, address, 0, tableCount - 1);
        if (location == -1)
            return nullptr;
        return &table[location];

    }
    // Really insert should check if the address already exists and handle that,
    // but instructions say to do that in the main function
    void Insert(int address) {
        // insert the record at (tableCount - 1) + 1, and increment tableCount
        table[tableCount++] = IPRecord(address);
        // sort the array
        insertionSort(table);
    }

    void SortByFrequency() {
        reverseInsertionSortByFreq(table);
    }

    void writeOut(ofstream& out) {
        for (int i =0; i < tableCount; i++) {
            out << table[i].getAddress() << "   " << table[i].getFrequency() << endl;
        }
    }
};

int main(int argc, char** argv) {

    // my debugging commandline options
    string in = "input.txt";
    string out = "output.txt";
    // if we have a specified in file
    if (argc >= 2)
        in = argv[1];
    if (argc >= 3)
        // we have an infile and outfile
        out = argv[2];



    IPTable table;

    string ipaddr;
    ifstream fin(in);
    while (fin >> ipaddr) {
        int address = stoi(ipaddr);
        IPRecord* record = table.BinarySearch(address);
        if (record) {
            // we found another, increment the frequency
            record->incrementFreq();
        } else {
            // insert the new address
            table.Insert(address);
        }
    }
    fin.close();

    // sort the table by frequency descending
    table.SortByFrequency();

    ofstream fout(out);
        table.writeOut(fout);
    fout.close();
    return 0;
}
