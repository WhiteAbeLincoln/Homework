#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <regex>

using namespace std;

// given:
int func(int n) {
    if (n == 1) return 0;
    if (n % 2 == 0)
        return func(n/2);
    else
        return func(3*n+1);
}

// list calls invoked when func(7) is called
//
// func(7): n = 7, n is odd, call func(3 * 7+1)
// func(22): n = 22, n is even, call func(22/2)
// func(11): n = 11, n is odd, call func(3*11+1)
// func(34): n = 34, n is even, call func(34/2)
// func(17): n = 17, n is odd, call func(3 * 17+1)
// func(52): n = 52, n is even, call func(52/2)
// func(26): n = 26, n is even, call func(26/2)
// func(13): n = 13, n is odd, call func(3 * 13 + 1)
// func(40): n = 40, n is even, call func(40/2)
// func(20): n = 20, n is even, call func(20/2)
// func(10): n = 10, n is even, call func(10/2)
// func(5): n = 5, n is odd, call func(3*5+1)
// func(16): n = 16, n is even, call func(16/2)
// func(8): n = 8, n is even, call func(8/2)
// func(4): n = 4, n is even, call func(4/2)
// func(2): n = 2, n is even, call func(2/2)
// func(1): n = 1, n == 1, return 0

// b: write a program to compute and print func(n) for each n from 1 to 1000
void printFunc() {
    for (int i = 1; i <= 1000; i++) {
       cout << i << ": " << func(i) << endl;
    }
}
// What do you observe?
// Every one returns 0, since it recurses until it reaches 0


/* ***********
 *   TASK 2:
 * ***********
*/

int g_numStudents;
double* g_gpas;
string* g_students;
bool** g_friendsMatrix;
int* g_path;
int* g_best;
bool* g_used;
int g_studentCount = 0;
int g_gpaCount = 0;
int g_totGpa = 0;

// so that we could possibly change it later
const int TEAM_SIZE = 3;

void defineArray(int n) {
    g_gpas = new double[n];
    g_students = new string[n];
    g_friendsMatrix = new bool*[n];
    g_path = new int[n];
    g_best = new int[n];
    g_used = new bool[n];
    for (int i = 0; i < n; i++) {
        // each friend can have up to n-1 friends (since you can't be friends with yourself)
        g_friendsMatrix[i] = new bool[n];
        for (int j = 0; j < n; j++)
            g_friendsMatrix[i][j] = false;
    }
}

void insertFriendMatrix(int idx1, int idx2) {
    g_friendsMatrix[idx1][idx2] = g_friendsMatrix[idx2][idx1] = true;
}

int insertStudents(string item) {
    g_students[g_studentCount] = item;
    return g_studentCount++;
}

int searchStudents(string student) {
    for (int i = 0; i < g_studentCount; i++) {
        if (g_students[i] == student)
            return i;
    }
    return -1;
}

void loadStudents() {
    ifstream infile("student.txt");
    string line;
    
    // loads the number of students;
    getline(infile, line);
    g_numStudents = stoi(line);

    defineArray(g_numStudents);
    while(getline(infile, line)) {
        string name;
        string maybegpa;
        double gpa;
        bool isFriendPair = false;
        istringstream iss(line);

        getline(iss, name, ' ');
        getline(iss, maybegpa, ' ');

        std::regex aRegex = std::regex("^([0-9.]+)$");
        if (!std::regex_match(maybegpa, aRegex)) {
            isFriendPair = true;
        }
        
        int idx1 = searchStudents(name);

        if (idx1 == -1) {
            idx1 = insertStudents(name);
        }
        
        if (isFriendPair) {
            int idx2 = searchStudents(maybegpa);

            // this shouldn't ever happen, but we're making sure
            if (idx2 == -1) {
               idx2 = insertStudents(maybegpa); 
            }
            
            insertFriendMatrix(idx1, idx2);            
        } else {
            g_gpas[idx1] = stod(maybegpa);
        }
    }

    infile.close();
}

bool isFriend(int idx1, int idx2) {
    return g_friendsMatrix[idx1][idx2];
}

bool areFriends(int first3[]) {
    int friends = 0;
    for (int i = 0; i < TEAM_SIZE; i++) {
        for (int k = 0; k < TEAM_SIZE; k++) {
            // each person must be friends with the other two.
            if (isFriend(first3[i],first3[k]))
                 friends++;
        } 
    }

    cout << "friendCount: " << friends << endl;
    return friends > 3;
}

void checkResult() {
    int totGpa = 0;
    int first3[TEAM_SIZE]; 

    for (int i = 0; i < TEAM_SIZE; i++) {
        first3[i] = g_path[i];
    }

    cout << g_students[first3[0]] << ";" << g_students[first3[1]] << ";" << g_students[first3[2]] << endl;    

    if (areFriends(first3)) {
        for (int i = 0; i < TEAM_SIZE; i++) {
            totGpa += g_gpas[first3[i]];
        }

        if (totGpa > g_totGpa) {
            g_totGpa = totGpa;
            for (int i = 0; i < g_numStudents; i++) 
            g_best[i] = g_path[i];
        }
    }

}

void permute(int i) {
    for (int k = 0; k < g_numStudents; k++) {
		if (!g_used[k]) {
			g_path[i] = k;
			g_used[k] = true;
			
			if (i == g_numStudents-1)
			    checkResult();
			else
			    permute(i+1);

            g_used[k] = false;
		}
	}
}

int main(int argc, char** argv) {
    //printFunc();
    loadStudents();
    permute(0);

    ofstream outfile("team.txt");

    for (int i = 0; i < 3; i++) {
        outfile << g_students[g_best[i]] << " " << g_gpas[g_best[i]] << endl;
    }

    outfile.close();
}
