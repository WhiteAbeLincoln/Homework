//
// Created by abe on 12/2/15.
//

#ifndef ASSN8_STUDENT_H
#define ASSN8_STUDENT_H

// Forward Declarations
class Person;
class Course;
class Student;


#include <string>
#include <vector>
#include "Person.h"
#include "Course.h"

class Student : public Person {
private:
    std::string major;
    std::vector<Course*> courses;
    static double Gtod (const Grade &grade);
    double calculateGPA(const std::vector<Course *> courses);
public:
    const std::vector<Course *> &getCourses() const;
    void setCourses(const std::vector<Course *> &courses);

    const std::string &getMajor() const;
    void setMajor(const std::string &major);

    void printTranscript();
    void addCourse(Course *course);

    void read();
    void write();

    Student();
};


#endif //ASSN8_STUDENT_H
