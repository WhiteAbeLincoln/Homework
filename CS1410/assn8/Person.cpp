//
// Created by abe on 12/2/15.
//

#include <string>
#include <algorithm>
#include <iostream>
#include "Person.h"

using namespace std;

const std::string &Person::getId() const {
    return this->id;
}

void Person::setId(const std::string &id) {
    this->id = id;
}

const std::string &Person::getName() const {
    return this->name;
}

void Person::setName(const std::string &name) {
    this->name = name;
}

const Date &Person::getDate() const {
    return this->dob;
}

void Person::setDate(const Date &date) {
    this->dob = date;
}

const Gender &Person::getGender() const {
    return this->gender;
}

void Person::setGender(const Gender &gender) {
    this->gender = gender;
}

const Address &Person::getAddress() const {
    return this->address;
}

void Person::setAddress(const Address &addr) {
    this->address = addr;
}

Person::Person() {
    this->setId("");
    this->setName("");

    Date date;
    date.day = 1;
    date.month = 1;
    date.year = 1970;
    this->setDate(date);

    this->setGender(Gender::MALE);

    Address address1;
    address1.city = "";
    address1.country = "";
    address1.state = "";
    address1.streetAddr = "";
    address1.zip = 0;
    this->setAddress(address1);
}

void Person::read() {
    std::string input;

    std::cout << "ID: ";
    getline(std::cin, input);
    this->setId(input);

    std::cout << "Name: ";
    getline(std::cin, input);
    this->setName(input);

    std::cout << "* Date of Birth *" << endl;
    Date date;
    std::cout << "Day: ";
    getline(std::cin, input);
    int day = stoi(input);
    date.day = day;
    std::cout << "Month: ";
    getline(std::cin, input);
    day = stoi(input);
    date.month = day;
    std::cout << "Year: ";
    getline(std::cin, input);
    day = stoi(input);
    date.year = day;
    this->setDate(date);

    std::cout << "Gender: ";
    getline(std::cin, input);
    this->setGender(this->stoG(input));

    // for future, implement string to address and string to date.
    std::cout << "* Address *" << endl;
    Address address1;
    std::cout << "Street Address: ";
    getline(std::cin, input);
    address1.streetAddr = input;
    std::cout << "City: ";
    getline(std::cin, input);
    address1.city = input;
    std::cout << "State: ";
    getline(std::cin, input);
    address1.state = input;
    std::cout << "Country: ";
    getline(std::cin, input);
    address1.country = input;
    std::cout << "Zip: ";
    getline(std::cin, input);
    address1.zip = stoi(input);

    this->setAddress(address1);
}

void Person::write() {
    cout << this->id << endl;
    cout << "-----------------------" << endl;
    cout << "Name: " << this->name << "  -  " << Gtos(this->getGender()) << endl;
    cout << "DOB: " << Dtos(this->getDate()) << endl;
    cout << "Address: " << Atos(this->address) << endl;
}

Gender Person::stoG(const std::string &gender) {
    std::string cpgender = gender;
    std::transform(cpgender.begin(), cpgender.end(), cpgender.begin(), ::toupper);

    if (cpgender == "MALE")
        return Gender::MALE;
    else
        return Gender::FEMALE;
}

std::string Person::Gtos(const Gender &gender) {
    switch (gender) {
        case Gender::MALE:
            return "Male";
        case Gender::FEMALE:
            return "Female";
        default:
            return "Something broke :(";
    }
}

std::string Person::Atos(const Address &address) {
    return address.streetAddr + "\n" + address.city + ", " + address.state + " " + to_string(address.zip) + "\n" + address.country;
}

std::string Person::Dtos(const Date &date) {
    return to_string(date.year) + "-" + to_string(date.month) + "-" + to_string(date.day);
}

const std::string &Person::getType() const {
    return this->type;
}

void Person::setType(const std::string &type) {
    this->type = type;
}
