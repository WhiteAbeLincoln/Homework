// Person.h
#ifndef _PERSON_INCLUDED_
#define _PERSON_INCLUDED_

#include <string>

struct Date {
    int month;
    int year;
    int day;
};

struct Address {
    int zip;
    std::string state;
    std::string city;
    std::string country;
    std::string streetAddr;
};


enum Gender { MALE, FEMALE }; //really this is sex, not gender, but whatever.

class Person {
private:
    std::string id;
    std::string name;
    std::string type;
    Date dob;
    Gender gender;
    Address address;

public:
    static Gender stoG(const std::string &gender); // is this really the best place? gender isn't only for a person
    static std::string Gtos(const Gender &gender);
    static std::string Dtos(const Date &date);
    static std::string Atos(const Address &address);

    const std::string &getId() const;
    void setId(const std::string &id);

    const std::string &getName() const;
    void setName(const std::string &name);

    const Date &getDate() const;
    void setDate(const Date &date);

    const std::string &getType() const;
    void setType(const std::string &type);

    const Gender &getGender() const;
    void setGender(const Gender &gender);

    const Address &getAddress() const;
    void setAddress(const Address &addr);

    virtual void read();
    virtual void write();

    Person();
};


#endif
