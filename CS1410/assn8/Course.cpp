#include <string>
#include <iostream>
#include <algorithm>
#include "Course.h"

using namespace std;

Semester Course::stoSemester(const std::string &semester) {
    std::string cpsemester = semester;
    std::transform(cpsemester.begin(), cpsemester.end(), cpsemester.begin(), ::toupper);

    if (cpsemester == "FALL")
        return Semester::FALL;
    else if (cpsemester == "SPRING")
        return Semester::SPRING;
    else
        return Semester::SUMMER;
}

std::string Course::Semestertos(const Semester &semester) {
    switch (semester) {
        case Semester::FALL:
            return "FALL";
        case Semester::SPRING:
            return "SPRING";
        case Semester::SUMMER:
            return "SUMMER";
        default:
            return "Something broke :(";
    }
}
const string &Course::getId() const { return this->id; }
void Course::setId(const string &id) { this->id = id; }

const string &Course::getName() const { return this->name; }
void Course::setName(const string &name) { this->name = name; }

const int &Course::getCredits() const { return this->credits; }
void Course::setCredits(const int &credit) { this->credits = credit; }

const Grade &Course::getGrade() const { return this->curGrade; }
void Course::setGrade(const Grade &curGrade) { this->curGrade = curGrade; }

const Semester &Course::getSemester() const { return this->curSemester; }
void Course::setSemester(const Semester &curSemester) { this->curSemester = curSemester; }

const int &Course::getYear() const { return this->year; }
void Course::setYear(const int &year) { this->year = year; }

Course::Course() {
    this->setId("");
    this->setName("");
    this->setCredits(0);

    // grade, semester and year don't really have empty states, so we use sane defaults
    this->setGrade(Grade::A);
    this->setSemester(Semester::FALL);
    this->setYear(1970); // beginning of time...

    this->initGradeMap();
}

void Course::read() {
    std::string input;
    std::cout << "Id: ";
    getline(std::cin, input);
    this->setId(input);

    std::cout << "Name: ";
    getline(std::cin, input);
    this->setName(input);

    std::cout << "Credits: ";
    getline(std::cin, input);
    this->setCredits(std::stoi(input));

    std::cout << "Grade: ";
    getline(std::cin, input);
    this->setGrade(Course::stoGrade(input));

    std::cout << "Semester: ";
    getline(std::cin, input);
    this->setSemester(Course::stoSemester(input));

    std::cout << "Year: ";
    getline(std::cin, input);
    this->setYear(std::stoi(input));
}

void Course::write() {
    std::cout << this->getId() << "  -  " << this->getName()  << std::endl;
    std::cout << "----------------------------------------------" << std::endl;
    std::cout << this->getSemester() << " " << this->getYear() << std::endl;
    std::cout << "Grade: " << this->getGrade() << " Credits: " << this->getCredits() << std::endl;
}

std::string Course::Gradetos(const Grade &grade) {
    std::map<std::string, Grade>::iterator it;
    string key = "-1";

    for (it = gradeMap.begin(); it != gradeMap.end(); ++it) {
        if (it->second == grade) {
            key = it->first;
            break;
        }
    }

    return key;
}

Grade Course::stoGrade(const std::string &grade) {
    // since get returns a const, we have to copy it to an editable variable
    std::string cpgrade = grade;
    std::transform(cpgrade.begin(), cpgrade.end(), cpgrade.begin(), ::toupper);
    return gradeMap[cpgrade];
}

void Course::initGradeMap() {
    this->gradeMap.insert(std::pair<std::string, Grade>("A+", Grade::A_PLUS));
    this->gradeMap.insert(std::pair<std::string, Grade>("A", Grade::A));
    this->gradeMap.insert(std::pair<std::string, Grade>("A-", Grade::A_MINUS));
    this->gradeMap.insert(std::pair<std::string, Grade>("B+", Grade::B_PLUS));
    this->gradeMap.insert(std::pair<std::string, Grade>("B", Grade::B));
    this->gradeMap.insert(std::pair<std::string, Grade>("B-", Grade::B_MINUS));
    this->gradeMap.insert(std::pair<std::string, Grade>("C+", Grade::C_PLUS));
    this->gradeMap.insert(std::pair<std::string, Grade>("C", Grade::C));
    this->gradeMap.insert(std::pair<std::string, Grade>("C-", Grade::C_MINUS));
    this->gradeMap.insert(std::pair<std::string, Grade>("D+", Grade::D_PLUS));
    this->gradeMap.insert(std::pair<std::string, Grade>("D", Grade::D));
    this->gradeMap.insert(std::pair<std::string, Grade>("D-", Grade::D_MINUS));
    this->gradeMap.insert(std::pair<std::string, Grade>("F", Grade::F));
    this->gradeMap.insert(std::pair<std::string, Grade>("I", Grade::I));
    this->gradeMap.insert(std::pair<std::string, Grade>("X", Grade::X));
}
