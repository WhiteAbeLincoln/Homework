//
// Created by abe on 12/2/15.
//

#include <string>
#include <iostream>
#include "Person.h"
#include "Student.h"

const std::vector<Course *> &Student::getCourses() const {
    return this->courses;
}

void Student::setCourses(const std::vector<Course *> &courses) {
    this->courses = courses;
}

const std::string &Student::getMajor() const {
    return this->major;
}

void Student::setMajor(const std::string &major) {
    this->major = major;
}

void Student::read() {
    std::string input;
    Person::read();

    std::cout << "Major: ";
    getline(std::cin, input);
    this->setMajor(input);
}

void Student::write() {
    Person::write();
    std::cout << "Major: " << this->getMajor() << std::endl;
}

Student::Student() {
    this->setType("student");
    std::vector<Course *> courses;
    this->setCourses(courses);
    this->setMajor("");
}

// There has to be a better way to do this...
double Student::Gtod(const Grade &grade) {
    switch (grade) {
        case Grade::A_PLUS:
            return 4;
        case Grade::A:
            return 4;
        case Grade::A_MINUS:
            return 3.67;
        case Grade::B_PLUS:
            return 3.33;
        case Grade::B:
            return 3;
        case Grade::B_MINUS:
            return 2.67;
        case Grade::C_PLUS:
            return 2.33;
        case Grade::C:
            return 2;
        case Grade::C_MINUS:
            return 1.67;
        case Grade::D_PLUS:
            return 1.33;
        case Grade::D:
            return 1;
        case Grade::D_MINUS:
            return 0.67;
        case Grade::F:
            return 0;
        case Grade::I:
            return 0;
        case Grade::X:
            return 0;
        default:
            return 0;
    }
}

double Student::calculateGPA(const std::vector<Course *> courses) {
    double GPA = 0;
    double gradepoints = 0;
    int credithours = 0;


    for (int i = 0; i < courses.size(); ++i) {
        gradepoints += Gtod(courses[i]->getGrade());
        credithours += courses[i]->getCredits();
    }

    GPA = gradepoints / credithours;

    return GPA;
}

void Student::printTranscript() {
    std::cout << "--------TRANSCRIPT--------" << std::endl;
    this->write();
    std::cout << "GPA: " << this->calculateGPA(this->getCourses()) << std::endl;
    std::cout << ":COURSES:" << std::endl;
    for (int i = 0; i < this->getCourses().size(); ++i) {
        Course* course = this->getCourses()[i];
        course->write();
        std::cout << std::endl;
    }
}

void Student::addCourse(Course *course) {
    // need to use the private member, since get function returns a const
    this->courses.push_back(course);
}
