//
// Created by abe on 12/3/15.
//
#include <vector>
#include <iostream>
#include <sstream>
#include "Person.h"
#include "Professor.h"
#include "Student.h"

using namespace std;

vector<Person *> persons;

Person* findPerson(string ID) {

    for (int i = 0; i < persons.size(); i++) {
        if (persons[i]->getId() == ID) {
            return persons[i];
        }
    }

    return nullptr;
}

// only two types of people, so we use a bool
void addPerson(bool professor) {
   if (professor) {
       Professor * professor1 = new Professor;
       professor1->read();
       persons.push_back(professor1);
   } else {
       Student * student1 = new Student;
       student1->read();
       persons.push_back(student1);
   }
}

void addCourse(string id) {
    Person* person = findPerson(id);

    if (person->getType() == "student") {
        Course * course1 = new Course;
        course1->read();
        ((Student*)person)->addCourse(course1);
    } else {
        std::cout << "This person is a professor" << std::endl;
    }
}

std::string prompt(std::string promptText) {
    string x;
    cout << promptText << endl;
    getline(cin, x);

    return x;
}

int main(int argc, char** argv) {
    cout << "CSV Convert" << endl;
    int option;
    string input;

    do {
        cout << "1. Add a professor" << endl;
        cout << "2. Add a student" << endl;
        cout << "3. Add a course for student" << endl;
        cout << "4. Print a transcript " << endl;
        cout << "5. Print professor information" << endl;
        cout << "0. Exit application" << endl;
        cout << endl;
        cout << "Selection: ";
        getline(cin, input);

        stringstream myStream(input);
        if (!(myStream >> option))
            option = -1;
        switch (option) {
            case 0:
                cout << "\nbye";
                break;
            case 1:
                addPerson(true);
                break;
            case 2:
                addPerson(false);
                break;
            case 3:
                addCourse(prompt("Type Student ID: "));
                break;
            case 4:
                ((Student*)findPerson(prompt("Type Student ID: ")))->printTranscript();
                break;
            case 5:
                ((Professor*)findPerson(prompt("Type Professor ID: ")))->write();
                break;
            default:
                cout << "Invalid Option, try again" << endl;
                break;
        }

     } while (option != 0);
}
