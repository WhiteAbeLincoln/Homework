// Course.h
#ifndef _COURSE_INCLUDED_
#define _COURSE_INCLUDED_

#include <string>
#include <map>

//Forward Declarations
class Course;

enum Semester { SPRING, FALL, SUMMER };
enum Grade { A_PLUS, A, A_MINUS, B_PLUS, B, B_MINUS, C_PLUS, C, C_MINUS, D_PLUS, D, D_MINUS, F, I, X };

class Course
{
    private:
        std::map<std::string, Grade> gradeMap;
        void initGradeMap();
        std::string id;
        std::string name;
        int credits;
        Grade curGrade;
        Semester curSemester;
        int year;
    public:
        const std::string &getId() const;
        void setId(const std::string &id);

        const std::string &getName() const;
        void setName(const std::string &name);

        const int &getCredits() const;
        void setCredits(const int &credit);

        const Grade &getGrade() const;
        void setGrade(const Grade &curGrade);

        const Semester &getSemester() const;
        void setSemester(const Semester &curSemester);

        const int &getYear() const;
        void setYear(const int &year);

        virtual void read();
        virtual void write();

        std::string Gradetos(const Grade &grade);
        Grade stoGrade(const std::string &grade);
        Semester stoSemester(const std::string &semester);
        std::string Semestertos(const Semester &semester);

        Course();
};

#endif
