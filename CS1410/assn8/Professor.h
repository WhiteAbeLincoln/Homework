//
// Created by abe on 12/2/15.
//

#ifndef ASSN8_PROFESSOR_H
#define ASSN8_PROFESSOR_H

// Forward Declarations
class Person;

#include <string>
#include "Person.h"

class Professor : public Person {
private:
    std::string title;
    std::string office;         // office really should be an extension of a class Address called Room.
    double salary;

public:
    const std::string &getTitle() const;
    void setTitle(const std::string &title);

    const std::string &getOffice() const;
    void setOffice(const std::string &office);

    const double &getSalary() const;
    void setSalary(const double &salary);

    void read();
    void write();

    Professor();
};


#endif //ASSN8_PROFESSOR_H
