//
// Created by abe on 12/2/15.
//

#include <string>
#include <iostream>
#include "Professor.h"

const std::string &Professor::getTitle() const {
    return this->title;
}

void Professor::setTitle(const std::string &title) {
    this->title = title;
}

const std::string &Professor::getOffice() const {
    return this->office;
}

void Professor::setOffice(const std::string &office) {
    this->office = office;
}

const double &Professor::getSalary() const {
    return this->salary;
}

void Professor::setSalary(const double &salary) {
    this->salary = salary;
}

Professor::Professor() {
    this->setType("prof");
    this->setTitle("");
    this->setOffice("");
    this->setSalary(0);
}

void Professor::read() {
    std::string input;
    Person::read();
    std::cout << "Job Title: ";
    getline(std::cin, input);
    this->setTitle(input);

    std::cout << "Office: ";
    getline(std::cin, input);
    this->setOffice(input);

    std::cout << "Salary: ";
    getline(std::cin, input);
    this->setSalary(std::stod(input));
}

void Professor::write() {
    Person::write();
    std::cout << "Job Title: " << this->getTitle() << std::endl;
    std::cout << "Office: " << this->getOffice() << std::endl;
    std::cout << "Salary: " << this->getSalary() << std::endl;
}
