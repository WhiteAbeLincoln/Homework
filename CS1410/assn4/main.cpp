#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

vector<string> vsplit(const string &s, char delim) {
    vector<string> elems;
    stringstream ss(s);
    string item;

    while (getline(ss, item, delim)) {
        // checks if the string isn't empty - protects against empty words when given strings like abc:d::e
        if (!item.empty())
            elems.push_back(item);
    }
    return elems;
}

struct coord {
    int row;
    int column;
};

enum Direction {
    NORTH,
    NORTH_EAST,
    EAST,
    SOUTH_EAST,
    SOUTH,
    SOUTH_WEST,
    WEST,
    NORTH_WEST
};

class Robot {
    coord position;
    Direction dir;
    
public:
    Robot(int row, int column, Direction dir) {
        this->position.row = row;
        this->position.column = column;
        this->dir = dir;        
    }
    
};
 
class Map {
    int rows;
    int cols;
    char** map;

public:
    Map(int rows, int cols, char** map) {
        this->rows = rows;
        this->cols = cols;
        this->map = map;
    }
};

class RobotGame {
    Robot robot;
    Map map;
    
    void createMap(ifstream& fin) {
        string size;
        int rows;
        int cols;
        char** map;
        
        getline(fin, size);
        vector<string> instructions = vsplit(size, ' ');
        if (instructions[0] == "MAP" && instructions.size() == 3) {
            rows = stoi(instructions[1]);
            cols = stoi(instructions[2]);
            
            map = new char*[rows];
            for(int i = 0; i < rows; i++) {
                map[i] = new char[cols];
                string row;
                getline(fin, row);
                strcpy(map[i], row);
            }
            
            this->map = new Map(rows, cols, map);
        } else {
            cout << "ERROR: Invalid instructions file" << endl;
            return;
        }
    }
    
    void readFile(char* filename) {
        ifstream fin(filename);
        createMap(fin);
    }

public:
    RobotGame(char* instructions) {
        readFile(instructions);
    }
    ~RobotGame() {
        
    }
};

string* split(const string &s, char delim) {
    int splits = 1; //every string has at least one word
    for (int i = 0; i < s.length(); i++) {
       if (s[i] == delim) splits++; 
    } 
    string* elems = new string[splits+1];
    // first element is the string length
    elems[0] = to_string(splits);
    //cout << splits << endl;

    stringstream ss(s);
    string item;
    splits = 1;

    while (getline(ss, item, delim)) {
        //cout << "elem " << splits << " " << item << endl;
        elems[splits] = item;
        splits++;        
    }
    //cout << "written good" << endl;
    
    return elems;
}

int main(int argc, char** argv) {
    string filename;
    if (argc > 1) {
        filename = argv[1];
        readFile(filename);
    }    
    else {
        cout << "Missing input file" << endl << "Usage: ./program filename.txt" << endl;
        return 1;
    }
    return 0;
}
