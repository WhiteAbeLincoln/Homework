#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

int N = 4;
int* path = new int[N];
int* best = new int[N];
int** cost = new int*[N];
int bestcost = 1000000000;
bool* used = new bool[N];
string* cities = new string[N];
int cityCount = 0;
string startCity;
string endCity;

void defineArr(int n) {
    path = new int[n];
    best = new int[n];
    cost = new int*[n];
    used = new bool[n];
    cities = new string[n];
    for (int i = 0; i < n; i++)
        cost[i] = new int[n];
}

int searchCities(string city) {
    // I don't care about speed...
    // besides, the brute force implementation given to us takes more time than a search 
    for (int i = 0; i < cityCount; i++) {
        if (cities[i] == city)
            return i;
    }
    return -1;
}

int insertCities(string city) {
    cities[cityCount] = city;
    return cityCount++;
}

void setup(string in) {
    for(int i = 0; i < N; i++)
        cost[i] = new int[N];

    ifstream infile(in);    
    string line;
    string numUnique;

    getline(infile, line);
    istringstream strstream(line);

    getline(strstream, numUnique, ' ');
    getline(strstream, startCity, ' ');
    getline(strstream, endCity, ' ');

    //cout << numUnique << " " << startCity << " " << endCity << endl;

    N = stoi(numUnique);

    //cout << N << endl;

    defineArr(N);
 
    while(getline(infile, line)) {
      //  cout << line << endl;
        istringstream iss(line);
        string city1, city2, scost;
        getline(iss, city1, ' ');
        getline(iss, city2, ' ');
        getline(iss, scost, ' ');

     //   cout << "in while" << endl;
     //   cout << city1 << " " << city2 << " " << scost << endl;

        int idx1 = searchCities(city1);
        int idx2 = searchCities(city2); 
        int icost = stoi(scost);

        // if the city doesn't already exist in our array, add it
        if (idx1 == -1) {
            idx1 = insertCities(city1);
        }

        if (idx2 == -1) {
            idx2 = insertCities(city2);
        }

	    cost[idx1][idx2] = cost[idx2][idx1] = icost;
    }
    infile.close();
}

void checkResult() {
	int totalcost = 0;

	cout << "\n Checking this path: ";
			for (int i = 0; i < N; i++)   // printing...
    		    cout << path[i] << ' ';


	if (cost[path[N-1]][path[0]] == 0)  // no path from last city back to first city
	    return;

    for (int i = 0; i < N-1; i++) {
        if (cost[path[i]][path[i+1]] == 0)  // no path from last city back to first city
	    	return;
		totalcost += cost[path[i]][path[i+1]];
	}

	totalcost += cost[path[N-1]][path[0]]; // come back to the first city

    // ABE: this is my only change to the actual travelling salesman code. 
    // if it doesn't produce the correct output, then there was a bug in the original
	if (bestcost > totalcost && cities[path[0]] == startCity && cities[path[N-1]] == endCity) {
        bestcost = totalcost;
        for (int i = 0; i < N; i++)
            best[i] = path[i];

		cout << "\n Found a better solution with total cost of " << totalcost << endl;

		for (int i = 0; i < N; i++)   // printing...
		    cout << path[i] << ' ';

	}
}

void permute(int i) {
	for (int k = 0; k < N; k++) {
		if (!used[k]) {
			path[i] = k;
			used[k] = true;
			
			if (i == N-1)
			    checkResult();
			else
			    permute(i+1);

            used[k] = false;
		}
	}
}

int main() {
	setup("tsp.in");
    permute(0);
//  system("pause"); This isn't cross platform - not everyone has a Windows computer. you can use cin.get() instead

    cout << endl;
    cout << endl;
    for (int i = 0; i < N; i++)
         cout << cities[best[i]] << " ";

    ofstream outfile;
    outfile.open("tsp.out");
    outfile << bestcost << endl;
    for (int i = 0; i < N; i++)
         outfile << cities[best[i]] << endl;
    outfile.close();

    return 0;
}
