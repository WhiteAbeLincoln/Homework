#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

int N;
string* items;
int* path;
bool* used;
int* best;
int itemCount = 0;
int* values;
int valCount = 0;
int bestcost = 1000000000;
int alice = 0;
int bob = 0;

void defineArr(int num) {
    items = new string[num];
    values = new int[num];
    used = new bool[num];
    path = new int[num];
    best = new int[num];
}

int insertItems(string item) {
    items[itemCount] = item;
    return itemCount++;
}

int insertValues(int val) {
    values[valCount] = val;
    return valCount++;

}

void setup(string in) {
    ifstream infile(in);    
    string line;
    string numItems;

    getline(infile, numItems);

    N = stoi(numItems);

    defineArr(N);
 
    while(getline(infile, line)) {
        cout << line << endl;
        istringstream iss(line);
        string item, value;
        getline(iss, item, ' ');
        getline(iss, value, ' ');

        int val = stoi(value);
        insertItems(item);
        insertValues(val);

    }
    infile.close();
}

int abs(int num) {
    if (num < 0)
        return num*-1;
    else
        return num;
}

void checkResult() {
    int alicecost = 0;
    int bobcost = 0;
    int difference = 0;

	cout << "\n Checking this path: ";
			for (int i = 0; i < N; i++)   // printing...
    		    cout << path[i] << ' ';

    // alices items are always the first half of the path
    // add up the total cost of alice and bobs items
//    cout << (int)N/2-1 << endl;
    for (int i = 0; i < N; i++) {
        if (i < N/2) {
		    alicecost += values[path[i]];
        } else {
            bobcost += values[path[i]]; 
        }
	}

//    cout << "alice: " << alicecost << endl;
//    cout << "bob: " << bobcost << endl;

    difference = abs(alicecost - bobcost);
//    cout << difference << endl;

	if (bestcost > difference) {
        bestcost = difference;
        bob = bobcost;
        alice = alicecost;
        for (int i = 0; i < N; i++)
            best[i] = path[i];

		cout << "\n Found a better solution with total cost of " << difference << endl;

		for (int i = 0; i < N; i++)   // printing...
		    cout << path[i] << ' ';

	}
}

void permute(int i) {
	for (int k = 0; k < N; k++) {
		if (!used[k]) {
			path[i] = k;
			used[k] = true;
			
			if (i == N-1)
			    checkResult();
			else
			    permute(i+1);

            used[k] = false;
		}
	}
}

int main() {
    setup("item.txt");
    permute(0);

    ofstream outfile;
    outfile.open("divide.txt");
    outfile << alice << " " << bob << " " << bestcost << endl;
    for (int i = 0; i < N/2-1; i++)
         outfile << items[best[i]] << " ";
    outfile << endl;

    for (int i = N/2-1; i < N; i++)
         outfile << items[best[i]] << " ";

    outfile.close();

    return 0;
}
