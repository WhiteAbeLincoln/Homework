#include <iostream>
#include <cstring>
#include <fstream>
#include <sstream>
#include <vector>
using namespace std;

struct BNKHeader {
    char signature[4]; // {'b','a','n','k'};
    int numberOfAccounts;
    char reserved[24];
};

struct BNKAccount {
    int number;
    char name[20];
    double balance;
    char reserved[96];
};

struct BNKIndex {
    int accountNumber;
    long filePosition;
};

class BankBranch {
    int accountNum;

    fstream file;
    BNKHeader header;
    BNKIndex* indexArr;
    BNKIndex theIndex;

    void writeFile(BNKAccount account, long position) {
        if (position >= 0) {
            file.seekg(position);
            file.write((char*)&account, sizeof(BNKAccount));
        }
    }
    void insertionSort(BNKIndex arr[], int count) {
        int j;
        BNKIndex temp;

        // starting at element 1 and continuing to the last element,
        for(int i = 1; i < count; i++) {
            j = i;
            // while j is greater than 0 (the first element) and the element previous is greater than the element at j
            while (j > 0 && arr[j-1].accountNumber > arr[j].accountNumber) {
                // swap the two elements
                temp = arr[j];
                arr[j] = arr[j-1];
                arr[j-1] = temp;
                // decrement j
                j--;
            }
        }
    }

    int binarySearch(BNKIndex arr[], int key, int first, int last) {
        // check for empty array
        if (last < first) {
            return -1;
        } else {
            int mid = (first + last) / 2;

            if (arr[mid].accountNumber == key)
                // key has been found
                return mid;

            if (arr[mid].accountNumber < key)
                // key is in the right half of the array
                return binarySearch(arr, key, mid+1, last);
            else
                // key is in the left half of the array
                return binarySearch(arr, key, first, mid - 1);
        }
    }
public:
    BankBranch(string in, string accnt) {
        
        file.open(in, ios::in | ios::out | ios::binary);
        if (file.fail()) {
            cout << "Cannot open " << in << endl;
            exit(404);
        }

        file.read((char*)&header, sizeof(BNKHeader));

        if (!(header.signature[0] == 'B' && header.signature[1] == 'A' && header.signature[2] == 'N' && header.signature[3] == 'K')) {
            cout << "Invalid file header" << endl;
            exit(415);
        }

        this->accountNum = stoi(accnt);
        this->indexArr = new BNKIndex[header.numberOfAccounts];
        int count = 0;
        file.seekg(sizeof(BNKHeader) + header.numberOfAccounts * sizeof(BNKAccount));

        // read the index into memory
        for(int i = 0; i < header.numberOfAccounts; i++) {
            BNKIndex index;
            file.read((char*)&index, sizeof(BNKIndex));
            indexArr[i] = index;
        }

        int indexpos = binarySearch(indexArr, this->accountNum, 0, header.numberOfAccounts-1);
        this->theIndex = indexArr[indexpos];
        
    }
    void display() {
        BNKAccount account;
        file.seekg(theIndex.filePosition);
        file.read((char*)&account, sizeof(BNKAccount));

        cout << "Account " << account.number << " - " << account.name << endl;
        cout << "-------------------------------------------------" << endl;
        cout << "Balance: $" << account.balance << endl;

        file.close();
    }
    void deposit() {
        BNKAccount account;
        string inamount;

        file.seekg(theIndex.filePosition);
        file.read((char*)&account, sizeof(BNKAccount));

        cout << "Enter deposit amount: ";
        getline(cin, inamount);
        double deposit = stod(inamount);

        account.balance += deposit;

        writeFile(account, theIndex.filePosition);
        file.close();
    }
    void withdraw() {
        BNKAccount account;
        string inamount;

        file.seekg(theIndex.filePosition);
        file.read((char*)&account, sizeof(BNKAccount));

        cout << "Enter withdrawl amount: ";
        getline(cin, inamount);
        double deposit = stod(inamount);

        account.balance -= deposit;

        writeFile(account, theIndex.filePosition);
        file.close();
    }
};

void fileprompt(string &in, string &accnt, int selector) {
    if (selector != 1) {
        cout << "Enter input filename: ";
        getline(cin, in);
        cout << endl;
    }
    cout << "Enter account number: ";
    getline(cin, accnt);
    cout << endl;
}

int main(int argc, const char* argv[]) {
    
    cout << "CSV Convert" << endl;
    int option;
    string input;

    do {
        cout << "1. Display Account" << endl;
        cout << "2. Deposit to Account" << endl;
        cout << "3. Withdraw from Account" << endl;
        cout << "0. Exit application" << endl;
        cout << endl;
        cout << "Selection: ";
        getline(cin, input);

        stringstream myStream(input);
        if (!(myStream >> option))
            option = -1;
        if (option == 0) {
            cout << "\nbye";
            break;
        }

        string in, accnt;

        if (argc == 2) {
            in = argv[1];
            fileprompt(in, accnt, 1);
        } else if (argc > 2) {
            in = argv[1];
            accnt = argv[2];
        } else {
            fileprompt(in, accnt, 0);
        }

        BankBranch bank(in, accnt);
        switch (option) {
            case 0:
                cout << "\nbye";
                break;
            case 1:
                bank.display();
                break;
            case 2:
                bank.deposit();
                break;
            case 3:
                bank.withdraw();
                break;
            default:
                cout << "Invalid Option, try again" << endl;
                break;
        }

    } while (option != 0);

    return 0;
}
