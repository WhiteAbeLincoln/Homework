#include <iostream>
#include <cstring>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

vector<string> vsplit(const string &s, char delim) {
    vector<string> elems;
    stringstream ss(s);
    string item;

    while(getline(ss, item, delim)) {
        if(!item.empty())
            elems.push_back(item);
    }
    return elems;
}

struct BNKHeader {
    char signature[4]; // {'b','a','n','k'};
    int numberOfAccounts;
    char reserved[24];
};

struct BNKAccount {
    int number;
    char name[20];
    double balance;
    char reserved[96];
};

struct BNKIndex {
    int accountNumber;
    long filePosition;
};

void insertionSort(BNKIndex arr[], int count) {
    int j;
    BNKIndex temp;

    // starting at element 1 and continuing to the last element,
    for(int i = 1; i < count; i++) {
        j = i;
        // while j is greater than 0 (the first element) and the element previous is greater than the element at j
        while (j > 0 && arr[j-1].accountNumber > arr[j].accountNumber) {
            // swap the two elements
            temp = arr[j];
            arr[j] = arr[j-1];
            arr[j-1] = temp;
            // decrement j
            j--;
        }
    }
}

void convertCSV(string in, string out) {
    ifstream csv;
    ofstream bin;    
    csv.open(in);

    if (csv.fail()) {
        cout << "cannot open " << in << endl;
        exit(404);
    }
    bin.open(out, ios::out | ios::binary);
    int lines = -1;     // we start at -1 because we don't want to count the first line (header line)
    string temp;

    // gets the number of lines
    while (getline(csv, temp)) {
        ++lines;        
    }

    // returns the file seeker to the top
    csv.clear();
    csv.seekg(0, ios::beg);

    // create the header and write
    BNKHeader header;
    header.signature[0] = 'B';
    header.signature[1] = 'A'; 
    header.signature[2] = 'N';
    header.signature[3] = 'K';
    header.numberOfAccounts = lines;

    bin.write((char*)&header, sizeof(BNKHeader));

    // dynamic index array
    BNKIndex* records = new BNKIndex[lines];
    int count = 0;

    // writes the account to file
    getline(csv, temp); // read the first line, since it is the header line
    while (getline(csv, temp)) {
        BNKAccount accnt;
        BNKIndex index;
        vector<string> line = vsplit(temp, ',');
        /*for (int i = 0; i < line.size(); i++)
            cout << line[i] << endl;*/
        accnt.number = stoi(line[0]);
        strcpy(accnt.name, line[1].c_str());
        accnt.balance = stod(line[2]);

        index.accountNumber = stoi(line[0]);
        index.filePosition = bin.tellp();

        records[count] = index;
        bin.write((char*)&accnt, sizeof(BNKAccount));
        ++count;
    }

    insertionSort(records, lines);
    for (int i = 0; i < lines; i++) {
        bin.write((char*)(&records[i]), sizeof(BNKIndex));
    }

    bin.close();
    csv.close();
}

void fileprompt() {
    string in;
    string out;

    cout << "Enter input filename: ";
    getline(cin, in);
    cout << endl;
    cout << "Enter output filename: ";
    getline(cin, out);
    cout << endl;

    convertCSV(in, out);
    cout << "File converted" << endl;
}

int main(int argc, const char* argv[]) {
    if (argc > 2) {
        convertCSV(argv[1], argv[2]);
    } else {
        cout << "CSV Convert" << endl;
        int option;
        string input;

        do {
            cout << "1. Convert a file" << endl;
            cout << "0. Exit application" << endl;
            cout << endl;
            cout << "Selection: ";
            getline(cin, input);

            stringstream myStream(input);
            if (!(myStream >> option))
                option = -1;

            switch (option) {
                case 0:
                    cout << "\nbye";
                    break;
                case 1:
                    fileprompt();
                    break;
                default:
                    cout << "Invalid Option, try again" << endl;
                    break;
            }

        } while (option != 0);
    }

    return 0;
}
