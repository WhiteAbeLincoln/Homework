/* ********************************
 * Abraham White
 * A02178955
 *
 *  Classes:
 *      BankAcct
 *          private:
 *              int acctNum
 *              string acctHolder
 *              double balance
 *              bool overdrafted
 *          public:
 *              BankAcct()
 *              BankAcct(int, string)
 *              BankAcct(int, string, double)
 *              double getBalance()
 *              void setBalance(double)
 *              string getHolder()
 *              void setHolder(string)
 *              int getAcctNum()
 *              void setAcctNum(int)
 *              bool isOverdrafted()
 *              void deposit(double)
 *              void withdraw(double)
 *      Bank
 *          private:
 *              vector<BankAcct> db
 *              vector<int> index
 *              void sortDb()
 *              void printBlank()
 *              BankAcct* askForAcct()
 *          public:
 *              Bank()
 *              Bank(vector<BankAcct>)
 *              int addAcct(string)
 *              int addAcct(string, double)
 *              int AddAccount(BankAcct)
 *              BankAcct* findAcct(int)
 *              void printAccts()
 *              void openAcct()
 *              void depositAcct()
 *              void withdrawAcct()
 *              void viewAcct()
 */
#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>

using namespace std;

class BankAccount {
    int acctNum;
    string acctHolder;
    double balance;
    bool overdrafted = false;

public:
    BankAccount() {
        BankAccount(0, "", 0);
    }

    BankAccount(int acctNum, string acctHolder) {
        BankAccount(acctNum, acctHolder, 0);
    }

    BankAccount(int acctNum, string acctHolder, double balance) {
        this->acctNum = acctNum;
        this->acctHolder = acctHolder;
        this->balance = balance;
    }

    double getBalance() {
        return balance;
    }

    void setBalance(double b) {
        balance = b;
    }

    string getHolder() {
        return acctHolder;
    }

    void setHolder(string a) {
        acctHolder = a;
    }

    int getAcctNum() {
        return acctNum;
    }

    // sets the account number
    // DANGEROUS
    // Since the BankBranch class handles the existing account check, we cannot check if we use this method
    // on an account already in the BankBranch database
    void setAcctNum(int n) {
        acctNum = n;
    }

    // returns whether the account is overdrawn
    bool isOverdrafted() {
        return overdrafted;
    }

    // deposits an amount to the account
    void deposit(double v) {
        balance += v;
        overdrafted = balance < 0;
    }

    // withdraws an amount from the account
    void withdraw(double v) {
        balance -= v;
        overdrafted = balance < 0;
    }

    void print() {
        string startCurr = "$";

        cout << "---------------------------" << endl;
        cout << this->acctNum << " - " << this->acctHolder << endl;
        cout << "\t" << "balance: " << startCurr << balance << endl;
        if (isOverdrafted()) cout << "\t" << "OVERDRAFTED" << endl;
        cout << "---------------------------" << endl;
    }
};

class BankBranch {
    // we use vector since we can open an arbitrary number of accounts at any time
    // via the menu, leading to the need for a dynamic, resizeable array
    BankAccount** accounts;
    // we want to use an index, since sorting the actual vector will be prohibitively expensive
    int nAccounts;

    // simple sort, following the examples in class
    void sortDb(BankAccount** arr) {
        int j;
        BankAccount* temp;

        // starting at element 1 and continuing to the last element,
        for(int i = 1; i < nAccounts; i++) {
            j = i;
            // while j is greater than 0 (the first element) and the element previous is greater than the element at j
            while (j > 0 && (*arr[j-1]).getAcctNum() > (*arr[j]).getAcctNum()) {
                // swap the two elements
                temp = arr[j];
                arr[j] = arr[j-1];
                arr[j-1] = temp;
                // decrement j
                j--;
            }
        }
    }

    void printBlank() {
        for (int i = 0; i < 5; i++)
            cout << endl;
    }

    // helper method to ask for a bank account
    BankAccount * askForAcct() {
        string string1;
        int number;
        BankAccount * acct;

        while(true) {
            cout << "Enter account number: ";
            getline(cin, string1);
            stringstream stringstream1(string1);
            if (stringstream1 >> number)
                break;
        }

        return findAcct(number);
    }

public:
    BankBranch() {}
    BankBranch(BankAccount** accounts) {
        this->accounts = accounts;
    }

    // Loads accounts from filename
    void LoadAccounts(string filename) {
        string instring;
        int counter = 0;
        ifstream fin(filename);

        int numAccts;

        //reads the first one (num accounts).
        fin >> numAccts;

        accounts = new BankAccount*[numAccts];

        for (int i = 0; i < numAccts; i++) {
            int acctNum;
            string name;
            double balance;
            fin >> acctNum;
            fin >> name;
            fin >> balance;

            BankAccount* account = new BankAccount(acctNum, name, balance);

            accounts[i] = account;
        }
        fin.close();
    }

    void SaveAccounts(string filename) {
        ofstream fout(filename);

        fout << nAccounts << endl;
        for(int i = 0; i < nAccounts; i++) {
            fout << (*accounts[i]).getAcctNum() << endl;
            fout << (*accounts[i]).getHolder() << endl;
            fout << (*accounts[i]).getBalance() << endl;
        }

        fout.close();
    }

    // returns the created account number
    //
    // creates an account
    int addAcct(string acctHolder) {
        return addAcct(acctHolder, 0);
    }

    // returns the created account number
    int addAcct(string acctHolder, double balance) {
        if (nAccounts > 0) {
            sortDb(accounts);
            // gets acct number of the last element in the sorted database
            int lastNum = (*accounts[nAccounts - 1 ]).getAcctNum();

            // we assign the next account number so there should be no conflicts
            return AddAccount(new BankAccount(lastNum + 1, acctHolder, balance));
        } else {
            return AddAccount(new BankAccount(1, acctHolder, balance));
        }
    }

    // very rarely do we want to use this manually, since we would then have to repeat until we
    // find an account number that doesn't exist
    // The other functions do the work of checking for us
    //
    // returns the created account number
    int AddAccount(BankAccount *acct) {

        if (findAcct((*acct).getAcctNum())) {
            cout << "Account already exists" << endl;
            return -1;
        };

        BankAccount** tempArr = new BankAccount*[nAccounts+1];

        for(int i = 0; i < nAccounts; i++) {
            tempArr[i] = accounts[i];
        }

        delete[] accounts;
        accounts = tempArr;

        tempArr[nAccounts++] = acct;

        sortDb(accounts);

        return (*acct).getAcctNum();
    }

    // we return a pointer so that the single BankAccount may be modified,
    // but we don't want to give public access to the entire database
    BankAccount * findAcct(int acctNum) {
        if (!nAccounts > 0) return nullptr;

        int first = 0, last = nAccounts - 1;

        do {
            // gets the midpoint element
            int mid = (first + last) / 2;

            // if we find it immediately, return the account
            if ((*accounts[mid]).getAcctNum() == acctNum)
                return accounts[mid];

            // if the found number is less than the desired,
            // we search the right half of the array
            // (all elements greater than the current midpoint element)
            if ((*accounts[mid]).getAcctNum() < acctNum)
                first = mid + 1;
            // otherwise, we search the left half
            // (all elements less than the current midpoint element)
            else
                last = mid - 1;
        } while (first <= last);

        return nullptr;
    }

    void printAccts() {
        for (int i = 0; i < nAccounts; i++) {
            (*accounts[i]).print();
        }
    }

    void openAcct() {
        string name = "";
        int deposit = 0;
        cout << "Enter account holder name: ";
        getline(cin, name);

        while(true) {
            cout << "Enter initial deposit: ";
            getline(cin, name);
            stringstream stream1(name);
            if (stream1 >> deposit)
                break;
        }

        // the BankBranch.AddAccount function checks for existing accounts with the same account number.
        // for convenience, we assign the number ourselves to reduce conflicts, but the BankBranch's public methods allow
        // manually assigning an account number
        cout << "New Account number: " << addAcct(name, deposit) << endl;
        printBlank();
    }

    // withdraws a certain amount from an account
    void withdrawAcct() {
        string string1;
        double amount;
        BankAccount * acct = askForAcct();

        if (!acct) {
            cout << "Account not found" << endl;
            return;
        }

        while(true) {
            cout << "Enter amount to withdraw: ";
            getline(cin, string1);
            stringstream stringstream1(string1);
            if (stringstream1 >> amount)
                break;
        }

        acct->withdraw(amount);

        if (acct->isOverdrafted()) {
            cout << "WARNING: Your account is overdrawn" << endl;
        }

        cout << "New balance: " << acct->getBalance() << endl;
        printBlank();
    }

    // deposits a certain amount in the account
    void depositAcct() {
        string string1;
        double amount;
        BankAccount * acct = askForAcct();

        if (!acct) {
            cout << "Account not found" << endl;
            return;
        }

        // asks for a valid amount to deposit
        while(true) {
            cout << "Enter amount to deposit: ";
            getline(cin, string1);
            stringstream stringstream1(string1);
            if (stringstream1 >> amount)
                break;
        }

        acct->deposit(amount);

        if (acct->isOverdrafted()) {
            cout << "WARNING: Your account is overdrawn" << endl;
        }

        cout << "New balance: " << acct->getBalance() << endl;
        printBlank();
    }

    // prints the details for an account using the BankAccount.print method.
    void viewAcct() {
        BankAccount * acct = askForAcct();

        if (!acct) {
            cout << "Account not found" << endl;
            return;
        }

        acct->print();
        printBlank();
    }
};

void populate(BankBranch & bank) {
    int max = rand() % 100;
    for (int i = 0; i < max; i++) {

        bank.addAcct("sample " + to_string(i), rand() % 500);
    }
}

void testBank() {
    cout << "starting" << endl;
    BankBranch bank = BankBranch();
    bank.addAcct("test1");
    bank.addAcct("test2");

    int acctNum = bank.addAcct("Abe White");
    cout << "New Acct: " << acctNum << endl;

    bank.addAcct("test3");

    bank.printAccts();

    bank.findAcct(2)->print();
}

int main() {
    BankBranch bank = BankBranch();
    populate(bank);

    int option;
    // stores the string input before conversion
    string input;

    do {
        cout << "1. Open a new account" << endl;
        cout << "2. Deposit to an existing account" << endl;
        cout << "3. Withdraw from an existing account" << endl;
        cout << "4. View existing account" << endl;
        cout << "5. Load From File" << endl;
        cout << "6. Save to file" << endl;
        cout << "0. Exit application" << endl;
        cout << endl;
        cout << "Selection: ";
        getline(cin, input);

        // converts from string to integer safely
        stringstream myStream(input);
        if (!(myStream >> option))
            option = -1;
            //cout << "worked" << endl;


        switch (option) {
            case 0:
                cout << "\nbye";
                break;
            case 1:
                bank.openAcct();
                break;
            case 2:
                bank.depositAcct();
                break;
            case 3:
                bank.withdrawAcct();
                break;
            case 4:
                bank.viewAcct();
                break;
            case 5:
                string iname;
                cout << "Filename:";
                getline(cin, iname);
                bank.LoadAccounts(iname);
                break;
            case 6:
                string oname;
                cout << "Filename:";
                getline(cin, oname);
                bank.SaveAccounts(oname);
                break;
            default:
                cout << "Invalid Option, try again" << endl;
                break;
        }
    } while (option != 0);

    return 0;
}
