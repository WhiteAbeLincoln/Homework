#include <iostream>
#include <cmath>
using namespace std;

class Vector {
    friend Vector operator+(const Vector& lhs, const Vector& rhs);
    friend Vector operator*(const Vector& lhs, const Vector& rhs);
    protected:
        int size;
        double * array;
    public:
        Vector(int n) {
            array = new double[n];
            for (int i = 0; i < n; i++)
                array[i] = 0;        
            size = n;
        }
        double getElement(int i) {
            if (i < 0 || i >= size)
                throw "Index Out Of Bounds";
            return array[i];
        }

        void setElement(int i, double elem) {
            if (i < 0 || i >= size)
                throw "Index Out Of Bounds";
            array[i] = elem; 
        }

        double getLength() {
            double length = 0;
            for (int i = 0; i < size; i++) {
                length += array[i]*array[i];    
            }
            return sqrt(length);
        }

};

Vector operator+(Vector const& lhs, Vector const& rhs) { 
    if (lhs.size != rhs.size)
        throw "Dimensions Mismatched";
    Vector avector(lhs.size);

    for (int i = 0; i < lhs.size; i++) {
        avector.array[i] = lhs.array[i] + rhs.array[i];
    }

    return avector;
}

Vector operator*(Vector const& lhs, Vector const& rhs) {
    if (lhs.size != rhs.size)
        throw "Dimensions Mismatched";
    Vector avector(lhs.size);

    for (int i = 0; i < lhs.size; i++) {
        avector.array[i] = lhs.array[i] * rhs.array[i];
    }

    return avector;
}

int main(int argc, char** argv) {
    Vector v1(5);
    Vector v2(5);

    for(int i = 0; i < 5; i++) {
        v1.setElement(i, i + 0.5);
        v2.setElement(i, i + 1.25);
    }

    Vector v3 = v1 + v2;
    Vector v4 = v1 * v2;

    for (int i = 0; i < 5; i++) {
        cout << v3.getElement(i) << endl;
    }
    cout << endl;
    for (int i = 0; i < 5; i++) {
        cout << v4.getElement(i) << endl;
    }

    cout << v4.getLength();

    v4.getLength();
    v4.getElement(1);
}
