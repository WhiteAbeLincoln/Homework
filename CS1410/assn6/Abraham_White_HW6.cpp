#include <iostream>
using namespace std;
// Question 1
/* Task 1a
 * 1. gcd is called, a=21, b=35
 * 2. if statement is executed, condition is false, since b != 0
 * 3. else statement is executed: returns a call to gcd with a = b (35), b = 21%35 = 21
 * 4. gcd is called (level 1) with a = 35, b = 21
 * 5. if statement is executed, condition is false, since b != 0
 * 6. else statement is executed: returns gcd with a = b (21), b = 35%21 = 14
 * 7. gcd is called (level 2) with a = 21, b = 14
 * 8. if statement is executed, condition is false, since b != 0
 * 9. else statement is executed: returns gcd with a = b (14), b = 21%14 = 7
 * 10. gcd is called (level 3) with a = 14, b = 7
 * 11. if statement is executed, condition is false, since b != 0
 * 12. else statement is executed: returns gcd with a = b (7), b = 14%7 = 0
 * 13. gcd is called (level 4) with a = 7, b = 0
 * 14. if statement is executed, condition is true, since b == 0: returns a = 7
 */ 
// Task 1b
int gcd(int a, int b) {
    int oldb = b;
    while (b != 0) {
        b = a%b;
        a = oldb;
        oldb = b;
    }
    return a;
}

// recursive version for testing
int rgcd(int a, int b) {
    if (b == 0) return a;
    else return rgcd(b, a%b);
}

// Question 2
// recursive version for testing
int rf(int a[], int x, int y) {
    if (x == y) return a[x];
    int z = (x+y)/2;
    return rf(a, x, z) + rf(a, z+1, y);
}
// Task 2a
// a = {1, 4, 2, 5} f(a, 0, 3)
// Return value of 12
//
// Task 2b
// Computes the sum of the numbers between index x and index y
//
// Task 2c
int f(int a[], int x, int y) {
    int sum = 0;
    for (int i = x; i <= y; i++) {
        sum += a[i];
    }
    return sum;
}

// Question 3
//
void QuickSort(int a[], int l, int r) {
    int pivot = a[(l+r)/2];
    int i = l, j = r;
    while (i < j) {
        while (a[i] < pivot) i++;
        while (a[j] > pivot) j--;
        int t = a[i]; a[i] = a[j]; a[j] = t;
        i++; j--;
    }
    if (l < j) QuickSort(a, l, j);
    if (i < r) QuickSort(a, i, r);
}
// Task 3a
// a = {1, 4, 2, 3} QuickSort(a, 0, 3)
// write description of execution
//
// 1. QuickSort is called with arguments a = {1, 4, 2, 3}, l = 0, r = 3                                         L0  a = {1, 4, 2, 3}, l=0, r=3
// 2. Line 2 is executed. pivot is assigned the element at index (0+3)/2 = index 1 = element value 4            L0  pivot=4
// 3. line 3 is executed. i is assigned the value of 0, j is assigned 3                                         L0  i=0, j=3
// 4. while loop on line 4 is executed, condition is true (0 < 3) - loop continues
// 5. while loop on line 5 is executed, condition is true (1 < 4), so i is incremented                          L0  i=1
// 6. while loop on line 6 is executed, condition is false (3 !> 4), execution continues
// 7. line 7 is executed. t is assigned value of 4, a[1] is changed to 3, a[3] is changed to 4                  L0  t=4, a = {1, 3, 2, 4}
// 8. line 8 is executed. i is incremented, j is decremented.                                                   L0  i=2, j=2
// 9. while loop on line 4 is executed, condition is false (2 !< 2) - loop ends
// 10. if on line 10 is executed, condition is true (0 < 2) - QuickSort is called, args a={1,3,2,4},l=0,r=j(2)  L1  a={1,3,2,4}, l=0, r=2
// 11. line 2 is executed. pivot is assigned element at index (0+2)/2 = index 1 = element value 4               L1  pivot=4
// 12. line 3 is executed. i is assigned 0, j is assigned 2.                                                    L1  i=0, j=2
// 13. while loop on line 4 is executed, condition is true (0 < 2) - loop continues
// 14. while loop on line 5 is executed, condition is true (1 < 4), so i is incremented                         L1  i=1
// 15. while loop on line 6 is executed, condition is false (2 !> 4), execution continues
// 16. line 7 is executed. t is assigned value of 3, a[1] is changed to 2, a[2] is changed to 3                 L1  t=3, a={1,2,3,4}
// 17. line 8 is executed. i is incremented, j is decremented.                                                  L1  i=2, j=1 
// 18. while loop on line 4 is executed, condition is false (2 !< 1) - loop ends
// 19. if on line 10 is executed, condition is true (0 < 1) - QuickSort is called, args a={1,2,3,4},l=0,r=1     L2  a={1,2,3,4},l=0,r=1
// 20. line 2 is executed. pivot is assigned the lement at index (0+1)/2 = index 0 = element value 1            L2  pivot=1
// 21. lien 3 is executed. i is assigned the value of 0, j is assigned the value of 1                           L2  i=0, j=1
// 22. while loop on line 4 is executed, condition is true (0 < 1) - loop continues
// 23. while loop on line 5 is executed, condition is false (1 !< 1) - execution continues
// 24. while loop on line 6 is executed, condition is true (2 > 1) so j is decremented                          L2  j=0
// 25. line 7 is exectued. t is assigned value of 1, a[0] is assigned value of 1, a[0] is assigned value of 1   L2  t=1, a={1,2,3,4}
// 26. line 8 is executed. i is incremented, j decremented                                                      L2  i=1, j=-1
// 27. while loop on line 4 is executed, condition is false (1 !< -1) - loop ends
// 28. if on line 10 is executed, condition is false (0 !< -1) - execution continues
// 29. if on line 11 is executed, condition is false (1 !< 1) - execution continues at stack frame level 1      L1
// 30. if on line 11 is exectued, condition is false (2 !< 2) - execution continues at stack frame level 0      L0
// 31. if on line 11 is executed, condition is true (2 < 3) - QuickSort is called, args a={1,2,3,4},l=2,r=3     L1  a={1,2,3,4},l=2,r=3
// 32. line 2 is executed. pivit is assigned element at index (2+3)/2 = index 2 = element value 3               L1  pivot=3
// 33. line 3 is executed. i is assigned value of 2, j is assigned 3.                                           L1  i=2, j=3
// 34. while loop on line 4 is executed, condition is true (2 < 3) - loop continues
// 35. while loop on line 5 is executed, condition is false (2 !< 2) - execution continues
// 35. while loop on line 6 is executed, condition is true (4 > 2) so j is decremented                          L1  j=2
// 36. line 7 is executed. t is assigned value of 3, a[2] is assigned value of 3, a[2] is assigned value of 3   L1  t=3, a={1,2,3,4} 
// 37. line 8 is executed. i is incremented, j decremented                                                      L1  i=3, j=1
// 38. while loop on line 4 is executed, condition is false (3 !< 1) - execution continues
// 39. if on line 10 is executed, condition is false (2 !< 1) - execution continues
// 40. if on line 11 is executed, condition is false (3 !< 3) - execution continues at stack frame level 0      L0
// 41. function reaches end, exits
//
// Issues: runs through the code twice after the array is sorted

// Task 3b


int main(int argc, char** argv) {
    cout << "rgcd: " << rgcd(21, 35) << endl;    
    cout << "gcd: " << gcd(21, 35) << endl;
    int a[] = {1,4,2,5};
    cout << "rf: " << rf(a, 0, 3) << endl;
    cout << "f: " << f(a, 0, 3) << endl;
    return 0;
}
