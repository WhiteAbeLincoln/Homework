#include <iostream>

using namespace std;

struct TreeNode {
    int item;
    TreeNode *left;
    TreeNode *right;

    TreeNode(int itm) {
        item = itm;
        left = NULL;
        right = NULL; 
    }
};

void treeInsert(TreeNode *&root, int newItem) {
    if (root == NULL) {
        root = new TreeNode(newItem);
        return;
    } else if (newItem < root->item) {
        treeInsert(root->left, newItem);
    } else {
        treeInsert(root->right, newItem);
    }
}

void postorderPrint(TreeNode *root) {
    if (root != NULL) {
        postorderPrint(root->left);
        postorderPrint(root->right);
        cout << root->item << " ";
    }
} 

void inorderPrint(TreeNode *root) {

     if (root != NULL) {
        inorderPrint(root->left);
        cout << root->item << " ";
        inorderPrint(root->right);
     }
}

int main(int argc, char** argv) {
    TreeNode * root = new TreeNode(20);
    treeInsert(root, 5);
    treeInsert(root, 15);
    treeInsert(root, 30);
    treeInsert(root, 40);
    treeInsert(root, 25);
    treeInsert(root, 10);
    treeInsert(root, 12);
    treeInsert(root, 8);
    treeInsert(root, 28);

    cout << "postorder: " << endl;
    postorderPrint(root);
    cout << "inorder: " << endl;
    inorderPrint(root);
}
