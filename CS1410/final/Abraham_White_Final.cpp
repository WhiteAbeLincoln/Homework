#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <stdexcept>

using namespace std;

//***********
//Question 1
//***********

class Node {
     public:
          int key;
          Node *left, *right;

          // Q 1a
          Node(int key, Node *left = NULL, Node *right = NULL) {
               this->key = key;
               this->left = left;
               this->right = right;     
          }
};

class BinarySearchTree {
     int rcount(Node *root) {
          if (root == NULL) {
               return 0; // empty tree 
          } else {
               int count = 1; //counting the current root node

               // branch off two subprocesses, following the left and right trees
               count += rcount(root->left);
               count += rcount(root->right);

               return count;
          }
     }

     Node* rsearch(int key, Node *root) {
          if (root != NULL) {
               if (root->key == key) 
                    return root;
               else if (key < root->key) {
                    return rsearch(key, root->left);
               }
               else return rsearch(key, root->right);

          }
          return NULL;
     }
     public:
     Node *root;
     BinarySearchTree() { root = NULL; }

     // Q 1b
     Node* search(int key) {

          return rsearch(key, root);

          // didn't see the private recursive function instructions before I wrote this
          /* Node* walker;
             walker = root;

             while (true) {
             if (walker == NULL) {
             return NULL;
             }
             else if (key == walker->key) {
             return walker;
             }
             else if (key < walker->key) {
             walker = walker->left;
             }
             else {
             walker = walker->right;
             }
             }
             */
     }

     // Q 1c
     int size() {
          return rcount(root);
     }
};


// Question 1d:
// The following values are inserted sequentially to an empty binary search tree:
//
// 4 2 1 3 5
//
// postorder: 1 3 2 5 4 
// inorder: 1 2 3 4 5
//
//

// *************
// Question 2
// *************

// Q 2a
class Complex {
     protected:
          double x;
          double y;

     public:
          // Q 2b
          // example function description wouldn't compile
          // only thing changed in the following is the function headers. Replace and see for yourself that it doesn't work
          // bool operator==(Complex a, Complex b)
          friend bool operator==(const Complex& a, const Complex& b) {
               return (a.x == b.x && a.y == b.y);
          }

          // Q 2c
          // the example function description didn't want to compile.
          // Complex operator+(Complex a, Complex b)
          friend Complex operator+(const Complex& a, const Complex& b) {
               Complex temp;
               temp.x = a.x + b.y;
               temp.y = a.y + b.y;

               return temp;
          }

          // Q 2d
          // Complex operator*(Complex a, Complex b) {
          friend Complex operator*(const Complex& a, const Complex& b) {
               Complex temp;
               temp.x = a.x * b.y;
               temp.y = a.y * b.y;

               return temp;
          }

          // Q 2a
          Complex() {
               this->x = 0;
               this->y = 0;
          }
          Complex(double x, double y) {
               this->x = x;
               this->y = y;
          } 
};

// ***************
// Question 3
// ***************

class Book {
     protected:
          string title;
          vector<string> authors;
          int pubyear;
          string price;

          // Q 3b
          virtual void display();
     public:
          Book() {
               title = "";
               authors.push_back("");
               pubyear = 0;
               price = "";
          }
          // consider using varargs for the authors list
          Book(string title, int year, string price, vector<string> authors) {
               this->title = title;
               this->pubyear = year;
               this->price = price;
               this->authors = authors;
          }
          Book(string description) {
               if (description.length() == 0) {
                    throw "Invalid String Description";
               }
               stringstream ss(description); 
               string temp;
               getline(ss, temp, ';');              
               this->title = temp;
               getline(ss, temp, ';');
               this->authors.push_back(temp);
               getline(ss, temp, ';');
               this->pubyear = stoi(temp); 
               getline(ss, temp, ';');
               this->price = temp;
          }
};


// Q 3b
void Book::display() {
     cout << "Title: " << this->title << endl;
     cout << "Authors: " << endl;
     for (unsigned long i = 0; i < this->authors.size(); i++) {
          cout << this->authors[i] << endl; 
     }
     cout << "Published in: " << this->pubyear << endl;
     cout << "Price: " << this->price << endl;
}


class EBook : public Book {
     protected:
          string path;
          void display();
     public:
          EBook(string title, int year, string price, vector<string> authors, string path)
               : Book(title, year, price, authors) {
                    this->path = path;
               }
          // calls Book default constructor
          EBook(string description) {
               if (description.length() == 0) {
                    throw "Invalid String Description";
               }
               stringstream ss(description); 
               string temp;
               getline(ss, temp, ';');              
               this->title = temp;

               // we need some way to split multiple authors. for now we will consider them one, since instructions weren't given.
               getline(ss, temp, ';');
               this->authors.push_back(temp);
               getline(ss, temp, ';');
               this->pubyear = stoi(temp); 
               getline(ss, temp, ';');
               this->price = temp;
               getline(ss, temp, ';');
               this->path = temp;
          }

};

// Q 3b
void EBook::display() {
     Book::display();
     cout << "Path: " << this->path << endl; 

     try {
          ifstream book;
          book.open(this->path);

          // prints first 10 lines assuming ebook is text file, not binary or zip format like an epub
          for (int i = 0; i < 10; i++) {
               string temp;
               getline(book, temp);
               cout << temp << endl;
          }
     } catch(exception const& ex) {
          cout << "Error: " << ex.what() << endl; 
     }
}

int main() {
     // do nothing
}
