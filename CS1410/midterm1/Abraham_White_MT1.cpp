#include<iostream>
#include<fstream>
#include<string>

/*
You should submit your solution on Canvas as a .cpp file named as Firstname_Lastname_MT1.cpp
containing all implementation code for the following programming tasks:

Task 1 (15 points). Implement method	"void AddItem(Item* item)" in class Order to add an item to the array 'items' of an order.

Note that, the array 'items' is initially allocated with 10 elements. So, you only re-allocate it when it contains 10 elements or more.

Task 2 (15 points). Implement method	"void RemoveItem(Item* item)" in class Order to remove an item from the array 'items' of an order.

Task 3 (20 points). Implement method	"void SearchItem(string term)" in class Order to find and print items whose descriptions contain the given 'term'.

Task 4 (30 points). Implement method	"void Print()" in class Order to print the order. The following is an example:

ID  Description  		Quantity	Price 	TaxRate
A1  iPhone 6s 64 GB     5           600.0   10%
A2  iPad Pro 128 GB     2           1000.0  10%
B1  Apple Juice         1           2.5     0%

Total items: 8
Total tax: $500.0
Total payment: $5502.5

Task 5 (20 points). Write tests in the main() function to test your program.

*/

using namespace std;

class Item {
public:
    Item(int id, string desc, int quant, double price, double tax) {
        this->id = id;
        this->description = desc;
        this->quantity = quant;
        this->price = price;
        this->taxrate = tax;
    }
	int id;
	string description;
	int quantity;
	double price, taxrate;
    void print() {
        cout << id << "\t\t" << description << "\t\t" << quantity << "\t\t" <<  price << "\t\t" << taxrate << "%" << endl;
    }
};

class Order {
	Item** items;
	int nItems;
    void insertionSort() {
        int j;
        Item* temp;

        for(int i =1; i < nItems; i++) {
            j = i;

            while (j > 0 && (*items[j-1]).id > (*items[j]).id) {
                temp = items[j];
                items[j] = items[j-1];
                items[j-1] = temp;
                j--;
            }
        }
    }
    int binarySearch(Item** arr, int key, int first, int last) {
            if (last < first) {
                return -1;
            } else {
                int mid = (first + last) / 2;

                if (arr[mid]->id == key)
                    return mid;

                if (arr[mid]->id < key)
                    return binarySearch(arr, key, mid+1, last);
                else
                    return binarySearch(arr, key, first, mid-1);
            }
    }
public:
    Order() {
        nItems = 0;
        items = new Item*[10];
	}
    void AddItem(Item* item) {
        if (nItems >= 10) {
            Item** oldarr = items;
            // increments nItems and allocates a new array 
            items = new Item*[++nItems];
            // loops through the old size (nItems -2) and copies data
            for (int i = 0; i < nItems - 1; i++)
                items[i] = oldarr[i];             
            
            delete[] oldarr;
        } else {
            nItems++;
        }
        // adds the new item
        items[nItems - 1] = item;
        insertionSort();
    }    
    // ideally this would return a boolean about whether the item was removed, so you could say while(order.RemoveItem(item)) to remove all instances of an item
    void RemoveItem(Item* item) {
        int idx = binarySearch(items, item->id, 0, nItems -1);    
        for (int i=idx; i < nItems; i++)
            items[i] = items[i+1];
        nItems--;
        insertionSort();

    }
    void SearchItem(string term) {
        for (int i=0; i<nItems; i++) {
            if (items[i]->description.find(term) != std::string::npos) {
                items[i]->print();
            }
        }    
    }
    void Print() {
        double tax = 0;
        double payment = 0;
        int count = 0;
        cout << "ID\t\tDescription\t\tQuantity\t\tPrice\t\tTaxRate" << endl;
        for (int i=0; i<nItems; i++) {
            items[i]->print();
            count += items[i]->quantity; 
            tax += items[i]->taxrate;
            payment += double(items[i]->price * items[i]->quantity * items[i]->taxrate);
        }
        cout << endl;
        cout << "Total items: " << nItems << endl;
        cout << "Total tax: " << tax << endl;
        cout << "Total payment: " << payment << endl;
    }
};

int main() {
    Order order;
    Item item1(1, "computer", 2, 1.00, 3.75), item2(2, "cell phone", 1, 50.00, 1.89), item3(3, "Canadian Syrup Eh!", 50, 40.23, 2.4), item4(4, "Tank", 10, 10002.32, 2.2);


    // the output won't be too pretty, since I can't align the output for different lengths of descriptions
    // I could do this using <iomanip> and setw() setfill(), but I'm not sure if I should go and import another library

    // test item print
    cout << "Testing item print" << endl;
    item1.print();
    cout << endl;
    cout << endl;

    // add items to order (more than 10 to check our reallocating array)
    order.AddItem(&item1);
    order.AddItem(&item2);
    order.AddItem(&item3);
    order.AddItem(&item4);
    order.AddItem(&item2);
    order.AddItem(&item1);
    order.AddItem(&item3);
    order.AddItem(&item4);
    order.AddItem(&item2);
    order.AddItem(&item1);
    order.AddItem(&item3);
    order.AddItem(&item2);
    //Testing order print (note our items will be sorted by id)

    cout << "Testing order print" << endl;
    order.Print();

    // search for canadian
    cout << endl;
    cout << "Searching for the Canadians" << endl;
    order.SearchItem("Canadian");

    // Remove a single item 2 (the cell phone)
    cout << endl;
    cout << "Removing Item 2" << endl;
    order.RemoveItem(&item2);
    order.Print();
}

